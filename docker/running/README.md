# Dockerized Cecile: running it from a docker container
Cecile is relatively portable, requiring only about ~100MB of runtime dependencies. However, building it for each Linux distribution is rather cumbersome. As such, for general use,
Cecile can be packaged and run from a Docker container. This also makes it compatible with Windows.

**Note**: for building Cecile in a docker container, see `../building`.

Dockerized Cecile was used to distribute Cecile for the Sustrainable summer school assignment in 2023.

## A note on CLI tools in containers
Docker is not necessarily designed for running CLI tools. Inside the Docker container, the `cecile` binary cannot simply access the host file system. We work around this by mounting
the current working directory inside the container. As such, absolute paths and paths outside the working directory do not work.

## Creating a Cecile docker container
- Build Cecile for Ubuntu 22.04 (instructions in the root README.md, or follow the `../building` README.md)
- Navigate to the directory with the `cecile` binary
- `docker build -t cecile:run ../../path/to/this/directory/` (replace `cecile:run` with a name of your choice)

## Using the docker container
Cecile can be used by mounting the current working directory in the container:
- `docker run -it -v "$(pwd):/root/workdir" cecile:run cecile --coord forkjoin.tey --nfp forkjoin.nfp --config forkjoin.xml`

To conveniently use the container, create an alias:
- `alias cecile='docker run -it -v "$(pwd):/root/workdir" cecile:run cecile'`

This creates an alias for the `cecile` command, which automatically mounts the current working directory in the docker container and launches it. Note that mounting directories
with many files is very slow with Docker.

Then, we can use Cecile like normal:
- `cecile --coord forkjoin.tey --nfp forkjoin.nfp --config forkjoin.xml`
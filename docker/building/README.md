# Compiling Cecile in a Docker container

The following instructions create a docker container within which Cecile can be built.

**Note**: for running Cecile in a docker container, see `../running`.

## Create and run docker container

Replace `cecile:buildenv` with your preferred tag.
- `docker build -t cecile:buildenv path/to/this/directory`
- `docker run -it -v "/host/path/to/cecile/source:/root" cecile:buildenv bash’`

Then, follow normal build instructions, e.g. (make sure submodules are checked out and the patch is applied)

- `mkdir -p build`
- `cd build`
- `cmake .. -DCMAKE_BUILD_TYPE=Release -DCECILE_BUILD_TESTS=ON`
- `make -j$(nproc)`

And, to run the tests

- `mkdir test-results`
- `./dist/cecile_test -sr junit -o ../test-results/results.xml`

## Updating the Bitbucket pipeline

The buildenv Docker file is used for CI builds. After updating the Docker file, please push an updated container to Dockerhub. 

* Make a Docker hub account.
* Create a Docker image with all the required dependencies. Modify `Dockerfile` if necessary.
    * `cd DockerHub`
    * `docker build -t yourdockerusername/imagename .` (in case of missing dependencies, force rerunning `apt update` with `docker build <...> --no-cache`)
    * `docker push yourdockerusername/imagename`
* Modify the `bitbucket-pipelines.yml` file if necessary.

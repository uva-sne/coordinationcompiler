/* 
 * Copyright (C) 2017 Benjamin Rouxel <benjamin.rouxel@inria.fr> 
 *                    Irisa/INRIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <cstdlib>
#include <string>
#include <libgen.h>
#include <boost/program_options.hpp>
#include <boost/filesystem/path.hpp>
#include <boost/filesystem/operations.hpp>
#include <Utils/InputPath.hpp>
#include <StateMachine/Generator/StrategyGenerator.hpp>
#include <StateMachine/StrategyStateMachine.hpp>

#include "Utils/Log.hpp"
#include "config.hpp"
#include "Solvers/ILP/ILP.hpp"
#include "Schedule.hpp"
#include "CompilerPass.hpp"
#include "RegistryEntries.hpp"

using namespace std;
namespace po = boost::program_options;

static char version[] = "4.10.1";

void isxmlfile(const InputPath &f) {
    if(f->empty())
        throw CompilerException("CLI opts", "missing XML file");
    if(!boost::filesystem::exists(*f))
        throw CompilerException("CLI opts", "file " + f->string() + " doesn't exist");
    if(f->extension() != ".xml")
        throw CompilerException("CLI opts", "file " + f->string() + " is not an XML file");
}

void iscoordfile(const InputPath &f) {
    if(f->empty())
        return;
    if(!boost::filesystem::exists(*f))
        throw CompilerException("CLI opts", "file " + f->string() + " doesn't exist");
    if(f->extension() != ".coord" && f->extension() != ".tey")
        throw CompilerException("CLI opts", "file " + f->string() + " is not an Coordination (.coord or .tey) file");
}

void isnfpfile(const InputPath &f) {
    if(f->empty())
        return;
    if(!boost::filesystem::exists(*f))
        throw CompilerException("CLI opts", "file " + f->string() + " doesn't exist");
    if(f->extension() != ".nfp")
        throw CompilerException("CLI opts", "file " + f->string() + " is not an Non-Functional Properties (.nfp) file");
}

template<typename K>
void print_registry_items(const string &title, const string &tag, const string &attribute = "id") {
    cout << "Category " << title << ": " << endl;
        cout << "  XML tag: <" << tag << " " << attribute << "=\"\" ... />" << endl;
    cout << "  Available ids:" << endl;
    for (typename K::key_t k : K::keys()) {
        cout << "    - " << k << endl;
    }
    cout << endl;
}

template<typename Registry>
void print_strategy_registry_item_or_nothing(const string &prefix, const string &tag) {
    auto keys = Registry::keys();
    if (keys.size() > 0) {
        cout << "  XML tag: <" << tag << " id=\"\" ... />" << endl;
        cout << "  Available " << tag << " ids:" << endl;
        for (auto &k : keys)
            cout << "    - " << prefix << "::" << k << endl;
    }
}

template<typename StrategyManagerType>
void print_strategy_registry_items(const string& prefix, const string &title) {
    cout << "Category " << title << " strategy passes with prefix '" << prefix << "': " << endl;
    print_strategy_registry_item_or_nothing<typename StrategyGenerator<StrategyManagerType>::registry_t>(prefix, "strategy-generator");
    print_strategy_registry_item_or_nothing<typename StrategyTransform<StrategyManagerType>::registry_t>(prefix, "strategy-transform");
    print_strategy_registry_item_or_nothing<typename StrategyLinker<StrategyManagerType>::registry_t>(prefix, "strategy-linker");
    print_strategy_registry_item_or_nothing<typename StrategyExporter<StrategyManagerType>::registry_t>(prefix, "strategy-exporter");
}

void print_version() {
    cout << "Cecile: a Compiler for the tEamplay CoordInation LanguagE" << endl;
    cout << "Version: " << version << endl;
}

void help_passes() {
    cout << "Help to build the XML configuration file." << endl;
    cout << "For further help on a specific id use `--help-pass category:id`, e.g. --help-pass analyse:cycle. " << endl;
    cout << endl;
    print_registry_items<AnalyseRegistry>("Analyses", "analyse");
    print_registry_items<TransformRegistry>("Transformations", "transform");
    print_registry_items<PriorityAssignmentRegistry>("Priority assignment algorithms", "priority");
    print_registry_items<PartitionRegistry>("Paritioning algorithms", "partition");
    print_registry_items<GeneratorRegistry>("Generators", "generator");
    print_registry_items<SolverRegistry>("Schedulers", "scheduler", "solver");
    print_registry_items<SimulatorRegistry>("Simulators", "simulator");
    print_registry_items<ExporterRegistry>("Exporters", "exporter");
    print_registry_items<SimulatorEventHandlerRegistry>("Simulator Event Handlers", "event-handler");
    print_strategy_registry_items<FaultToleranceStrategyManager>("ft", "Fault tolerance");
    print_registry_items<FaultToleranceRegistry>("Fault Tolerance redundancy schemes", "strategy-generator", R"(id="ft::..." protected="" unprotected)");
    print_strategy_registry_items<StrategyManager>("any", "General purpose");
    cout << endl;
    cout << "For further help on a specific id use `--help-pass category:id`, e.g. --help-pass analyse:cycle. " << endl;
}

void help(const po::options_description &desc) {
    cout << desc << endl << endl;
}

template<typename K>
void print_help_pass(std::vector<std::string> pn, const std::string &prefix = "") {
    cout << "Help description of " << (prefix.empty() ? "" : prefix + "::" ) << pn[1] << " in category " << pn[0] << ": " << endl;
    cout << "    " ;
    if(!K::IsRegistered(pn[1]))
        throw CompilerException("help", "Unrecognized id "+pn[1]+" in category "+pn[0]);
    CompilerPass *pass = K::Create(pn[1]);
    string help_msg;
    if(pn.size() == 2)
        help_msg = pass->help();
    else if(pn.size() == 3)
        help_msg = pass->more_help(pn[2]);
    
    ba::replace_all(help_msg, "&nbsp;", " ");
    cout << help_msg << endl;
    
    delete pass;
}


template<template<typename> typename StrategyPassType>
void print_help_strategy_pass(const string &cat, const string &fullId) {
    auto [prefix, id] = StrategyStateMachine::split_id(fullId);
    bool found = false;

#define DECLARE_MANAGER(managerId, type)  \
    if (prefix == managerId) {            \
        print_help_pass<typename StrategyPassType<type>::registry_t>({cat, id}, managerId); \
        found = true;                     \
    }
#include "StateMachine/StrategyManagers.hpp"
#undef DECLARE_MANAGER

    if (!found)
        throw CompilerException("help", "Urecognized strategy pass prefix '" + prefix + "'");
}


void help_pass(const string &fullname) {
    auto splitpos = fullname.find_first_of(':');
    if(splitpos == string::npos)
        throw CompilerException("help", "Missing ':' in name "+fullname+", maybe you forgot the xmltag?");

    std::string category = fullname.substr(0, splitpos);
    std::string passname = fullname.substr(splitpos + 1);
    
    if(category == "analyse")
        print_help_pass<AnalyseRegistry>({ category, passname });
    else if(category == "transform")
        print_help_pass<TransformRegistry>({ category, passname });
    else if(category == "priority")
        print_help_pass<PriorityAssignmentRegistry>({ category, passname });
    else if(category == "partitioning")
        print_help_pass<PartitionRegistry>({ category, passname });
    else if(category == "generator")
        print_help_pass<GeneratorRegistry>({ category, passname });
    else if(category == "scheduler")
        print_help_pass<SolverRegistry>({ category, passname });
    else if(category == "simulator")
        print_help_pass<SimulatorRegistry>({ category, passname });
    else if(category == "event" || category == "event-handler")
        print_help_pass<SimulatorEventHandlerRegistry>({ category, passname });
    else if(category == "exporter")
        print_help_pass<ExporterRegistry>({ category, passname });
    else if(category == "group")
        print_help_pass<GroupRegistry>({ category, passname });
    else if(category == "strategy-generator")
        print_help_strategy_pass<StrategyGenerator>(category, passname);
    else if(category == "strategy-transform")
        print_help_strategy_pass<StrategyTransform>(category, passname);
    else if(category == "strategy-linker")
        print_help_strategy_pass<StrategyLinker>(category, passname);
    else if(category == "strategy-exporter")
        print_help_strategy_pass<StrategyExporter>(category, passname);
    else
        throw CompilerException("help", "Unrecognized compiler pass of type '" + category + "'");
}

/**
 * Add almost mandatory compiler passes that are at least not harmful
 * 
 * As we are adding the default pass at the beginning of the vector, mind the
 * push order.
 * 
 * As of now we want to execute the default pass in this order:
 * 1) propagate_properties
 * 2) simplify
 * 
 * @param conf
 */
void add_default_passes(config_t *conf) {
// due to the call to transitiv closure, simplifying the graph might take a while
//    if(!any_of(conf->passes.begin(), conf->passes.end(), [](CompilerPass *p) { return p->get_uniqid_rtti() == "transform-simplify";})) {
//        conf->passes.insert(conf->passes.begin(), TransformRegistry::Create("simplify"));
//    }
    if(!conf->passes.find_pass_before("transform-propagate_properties"))
        conf->passes.passes().insert(conf->passes.passes().begin(), std::unique_ptr<CompilerPass>(TransformRegistry::Create("propagate_properties")));
}

int main(int argc, char** argv) {
    config_t conf;
    init_config(&conf);

//    #ifndef NDEBUG
//    use param options
//    Utils::setLevel(Utils::Level::DEBUG);
//    #endif

    InputPath resfile, cplexsolfile, config_path, nfp_path, outdir;
    InputPath coord_path;
    bool verbose, quiet;
    vector<string> cli_properties;

    po::options_description desc("Allowed options");
    desc.add_options()
        ("help,h", "displays this help message.")
        ("help-pass,hp", po::value<string>(), "displays a help message for a particular pass, invoke with \"category:passid\"")
        ("show-pass,s", "shows the list of all available compiler passes.")
        ("config,cf", po::value<InputPath>(&config_path)->required()->notifier(&isxmlfile), "sets the configuration file.")
        ("rfile", po::value<InputPath>(&resfile), "From result file")
//        ("cplexsolfile", po::value<InputPath>(&cplexsolfile), "From Cplex solution file")
        ("coord,co", po::value<InputPath>(&coord_path)->required()->notifier(&iscoordfile), "sets the TeamPlay coordination (.coord or .tey) file.")
        ("nfp", po::value<InputPath>(&nfp_path)->notifier(&isnfpfile), "sets the Non-Functional Properties file (.nfp).")
        ("outdir,o", po::value<InputPath>(&outdir), "sets the global output directory for produced files.")
        ("property, p", po::value<vector<string>>(&cli_properties)->composing(), "key:value property to replace in each input files. Can be specified multiple times.")
        ("verbose,v", po::bool_switch(&verbose)->default_value(false), "displays debugging messages in addition to other severity levels")
        ("quiet,q", po::bool_switch(&quiet)->default_value(false), "displays debugging messages in addition to other severity levels")
        ("version", "displays the version")
    ;

    po::variables_map vm;
    try {
        po::store(po::command_line_parser(argc, argv).options(desc).run(), vm);
        if (vm.count("help")) {
            help(desc);
            return EXIT_SUCCESS;
        }
        if(vm.count("help-pass")) {
            help_pass(vm["help-pass"].as<string>());
            return EXIT_SUCCESS;
        }
        if(vm.count("show-pass")) {
            help_passes();
            return EXIT_SUCCESS;
        }
        if(vm.count("version")) {
            print_version();
            return EXIT_SUCCESS;
        }
        po::notify(vm);
    }
    catch(po::error &e) {
        Utils::ERROR(e.what());
        help(desc);
        return EXIT_FAILURE;
    }
    catch(CompilerException &e) {
        Utils::ERROR(e.type(), e.what());
        help(desc);
        return EXIT_FAILURE;
    }

    if (verbose) {
        Utils::setLevel(Utils::Level::DEBUG);
    }
    else if(quiet)
        Utils::setLevel(Utils::Level::ERROR);

    conf.files.config_path = *config_path;
    conf.files.coord_path = *coord_path;
    conf.files.nfp_path = *nfp_path;
    conf.files.coord_basename = conf.files.coord_path.leaf().replace_extension("");

    if (outdir->empty())
        conf.files.output_folder = boost::filesystem::current_path();
    else
        conf.files.output_folder = *outdir;
    
    CommandLineProperties properties;
    for(const string &property : cli_properties) {
        size_t position = property.find(':');
        if (position == string::npos) {
            Utils::ERROR("Property " + property + " is malformed. Use key:value.");
            return EXIT_FAILURE;
        }
        string key = property.substr(0, position);
        string value = property.substr(position + 1);
        if (properties.count(key)) {
            Utils::WARN("Property with key '" + key + "' is provided more than once.");
        }
        properties.insert({key, value});
    }

    SystemModel tg;
    StrategyStateMachine ssm(std::make_unique<Schedule>(&tg, &conf));
    try {

        // get task-graph and config from related Xml task-graph
        auto* parser = ParserRegistry::Create(".xml", &tg, &conf, &properties);
        parser->parse(conf.files.config_path);
        delete parser;

        add_default_passes(&conf);


        if (conf.files.coord_path.extension() == ".coord") {
            auto* parser = ParserRegistry::Create(".coord", &tg, &conf, &properties); 
            parser->parse(conf.files.coord_path);
            delete parser;
        } else if (conf.files.coord_path.extension() == ".tey") {
            auto* parser = ParserRegistry::Create(".tey", &tg, &conf, &properties);
            parser->parse(conf.files.coord_path);
            delete parser;
        } else {
            throw std::runtime_error("Unknown coordination file extension " + conf.files.coord_path.extension().string());

        }

        if(!conf.files.nfp_path.empty()) {
            auto* parser = ParserRegistry::Create(".nfp", &tg, &conf, &properties);
            parser->parse(conf.files.nfp_path);
            delete parser;
        }

        if((!resfile->empty() && boost::filesystem::exists(*resfile))) {
            InputParser *rp = ParserRegistry::Create("res-xml", &tg, &conf, &properties);
            dynamic_cast<ResXMLSaxParser*>(rp)->schedule(ssm);
            rp->parse(*resfile);
        }

        conf.passes.dispatch(&tg, &conf, &ssm);
//        }
    }
    catch(CompilerException &e) {
        Utils::ERROR(e.type(), e.what());
        Utils::INFO("Cecile has run into a catastrophic error and cannot continue");
        return EXIT_FAILURE;
    }
    catch(Unschedulable &e) { // TODO when is this exception thrown?
        Utils::WARN("Un-Schedulable", e.what());

        for(const auto &p : conf.passes.passes()) {
            if(!p->force_unschedulable()) continue;
            
            Utils::INFO(string("--> calling ")+Utils::demangle(typeid(*p).name()));
            p->run(&tg, &conf, &ssm);
        }
        
        return EXIT_SUCCESS;
    }
    catch(Todo &e) {
        Utils::WARN("Schedule configuration is not ready yet (@TODO)", e.what());
        return EXIT_FAILURE;
    }
    catch(Timeout &) {
        Utils::WARN("Timeout");

        for(const auto &p : conf.passes.passes()) {
            if(!p->force_unschedulable()) continue;
            
            Utils::INFO(string("--> calling ")+Utils::demangle(typeid(*p).name()));
            p->run(&tg, &conf, &ssm);
        }
        
        return EXIT_SUCCESS;
    }
    return EXIT_SUCCESS;
}

# 
# Copyright (C) 2020 Benjamin Rouxel <benjamin.rouxel@uva.nl> 
#                    University of Amsterdam
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Define a binary to be produced from the source files.

cmake_policy(SET CMP0003 NEW)

add_executable(cecile main.cpp)

target_link_libraries(cecile PUBLIC cecile_antlr)
target_link_libraries(cecile PRIVATE cecile_core)
target_link_libraries(cecile PUBLIC ANTLR4::cpp_runtime)

add_custom_target(
    methane
    ALL 
    DEPENDS cecile
    COMMAND cd ${CMAKE_RUNTIME_OUTPUT_DIRECTORY} && cp cecile methane
    COMMENT "Backward compatibility for the command that has been renamed from methane to cecile"
)

# Add an installation target.
install(TARGETS cecile DESTINATION bin)

# Add backward compatible install target
install(PROGRAMS ${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/methane 
    DESTINATION bin
)

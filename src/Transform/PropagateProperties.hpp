/*!
 * \file PropagateProperties.hpp
 * \copyright GNU Public License v3.
 * \date 2019 
 * \author Benjamin Rouxel <benjamin.rouxel@uva.nl>, University of Amsterdam
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PROPAGATEPROPERTIES_H
#define PROPAGATEPROPERTIES_H

#include "Transform.hpp"

/*!
 * \brief Propagate timing properties through each DAG
 * 
 * For each DAG, gather the deadline/period and set it to each task.
 * 
 * \see PropagateProperties::help
 * 
 * \copydetails PropagateProperties::help()
 */
class PropagateProperties : public Transform {
    //! list of DAGs with a single period for the whole DAG
    std::vector<std::vector<Task*>> simple_period_dags;
    //! list of DAGS where tasks have different periods
    std::vector<std::vector<Task*>> multi_period_dags;
    
public:
    //! \brief Constructor
    PropagateProperties();
    virtual std::string help() override;
    //! \copydoc CompilerPass::get_uniqid_rtti
    const std::string get_uniqid_rtti() const override;
    void forward_params(const std::map<std::string,std::string>&) override;
    
protected:
    //! \copydoc Transform::check_dependencies
    void check_dependencies() override;
    //! \copydoc Transform::apply_transform
    void apply_transform() override;
    
    /*!\brief Simply propagate period/deadline to each task of the DAG
     * 
     * Work on DAG with a single period
     * 
     * \param DAGS list of DAGS
     */
    void propagate_timing_simple(std::vector<std::vector<Task*>> &DAGS);
    
    /*!\brief Set the period/deadline to task that have none according to dependencies
     * 
     * Work on DAG with multiple periods
     * 
     * \param DAGS list of DAGS
     */
    void propagate_multi_period_per_dag(std::vector<std::vector<Task*>> &DAGS);
    
    /*! \brief Forward timing infos to successors 
     * \param t the current task holding the period
     * \return true if period was propagated, false otherwise
     */
    bool set_period_fwd(Task *t);
    /*! \brief Forward timing infos to predecessors 
     * \param t the current task holding the period
     * \return true if period was propagated, false otherwise
     */
    bool set_period_bwd(Task *t);
    /*! \brief Propagate the deadline 
     * \param DAGS list of DAGS
     */
    void propagate_deadline(std::vector<std::vector<Task*>> &DAGS);
};

/*!
 * \remark Register the PropagateProperties class as a transformation with config id
 * "propagate_properties"
 */ 
REGISTER_TRANSFORM(PropagateProperties, "propagate_properties")

#endif
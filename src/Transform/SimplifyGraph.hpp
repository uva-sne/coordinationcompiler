/*!
 * \file SimplifyGraph.hpp
 * \copyright GNU Public License v3.
 * \date 2019 
 * \author Benjamin Rouxel <benjamin.rouxel@uva.nl>, University of Amsterdam
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SIMPLIFYGRAPH_H
#define SIMPLIFYGRAPH_H

#include "Transform.hpp"

/**
 * @brief Transform that ensures a task graph has exactly one source and one
 * sink.
 * 
 * \copydetails SimplifyGraph::help()
 *
 * In some cases, it is desirable for a task graph to have exactly one source
 * and one sink. This transformation ensures that fact by, if necessary, adding
 * a dummy source and/or sink to the task graph. A dummy source is connected to
 * the (newly created) inputs of all existing sources with a connector of type
 * 0 void. Similarly, the dummy sink is connected to the (newly created) outputs
 * of the existing sinks with the same connector type. Note that if the graph
 * already has exactly one source or sink, then this transform will not create
 * a new one.
 *
 * @note Througout this text, a source is defined as a task without inputs, and
 * a sink is defined as a task without outputs.
 * 
 * \see SimplifyGraph::help
 */
class SimplifyGraph : public Transform {
public:
    //! \brief Constructor
    SimplifyGraph();
    virtual std::string help() override;
    void forward_params(const std::map<std::string,std::string>&) override;
    
    //! \copydoc CompilerPass::get_uniqid_rtti
    const std::string get_uniqid_rtti() const override;
    
protected:
    //! \copydoc Transform::apply_transform
    void apply_transform() override;
    
};

/*!
 * \remark Register the SimplifyGraph class as a transformation with config id
 * "normalize"
 */ 
REGISTER_TRANSFORM(SimplifyGraph, "simplify")

#endif
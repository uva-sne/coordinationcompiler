/*!
 * \file ApplySecurityLevel.cpp
 * \copyright GNU Public License v3.
 * \date 2019 
 * \author Benjamin Rouxel <benjamin.rouxel@uva.nl>, University of Amsterdam
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "ApplySecurityLevel.hpp"

using namespace std;

IMPLEMENT_HELP(ApplySecurityLevel, 
    Flatten the IR by leving only a single version for each task
    \n
    * level [int], default -: Minimum security level to select a version within a task. All versions having a security level lower will be withdrawn.\n
    * tie-break [max/min/scheduler], default -: Specify what to do if there are multiple versions with a security level higher. \n
    - max -> use the version with the highest security level\n
    - min -> use the version with the lowest security level above the threshold\n
    - scheduler-> leave the scheduler decide, there might still be several versions in the IR if this options is selected\n
)

void ApplySecurityLevel::forward_params(const map<string, string> &args) {
    if(args.count("level")) 
        lvl = stoi(args.at("level"));
    
    if(args.count("tie-break")) {
        string tbs = args.at("tie-break");
        if(tbs == "max") tb = MAXSECU;
        else if(tbs == "min") tb = MINSECU;
        else if(tbs == "scheduler") tb = SCHEDULER;
        else
            throw CompilerException("transform-security", "Bad parameter for tie breaking ("+tbs+"): max/min/scheduler allowed");
    }
}
const string ApplySecurityLevel::get_uniqid_rtti() const { return "transform-security"; }

ApplySecurityLevel::ApplySecurityLevel() : Transform() {}

void ApplySecurityLevel::apply_transform() {
    for (Task *t : tg->tasks_it()) {
        for(Version *v : t->versions()) {
            if(v->security_lvl() < (uint) lvl)
                t->rem_version(v);
        }
    }
    for (Task *t : tg->tasks_it()) {
        if(t->versions().size() == 0)
            throw Unschedulable(t, "No more version after security lowering");
        if(t->versions().size() == 1)
            continue;
        
        if(tb == SCHEDULER) {
            throw Todo("Missing conditional edge management");
        }
        else {
            vector<Version *> versions = t->versions();
            auto bounds = minmax_element(versions.begin(), versions.end(), [](Version *a, Version *b) { 
                return a->security_lvl() < b->security_lvl();
            });
            Version *bound = (tb == MINSECU) ? *bounds.first : *bounds.second; 
            for(Version *v : versions) {
                if(bound != v)
                    t->rem_version(v);
            }
        }
    }   
}
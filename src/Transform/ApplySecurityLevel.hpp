/*!
 * \file ApplySecurityLevel.hpp
 * \copyright GNU Public License v3.
 * \date 2019 
 * \author Benjamin Rouxel <benjamin.rouxel@uva.nl>, University of Amsterdam
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef APPLYSECURITYLEVEL_H
#define APPLYSECURITYLEVEL_H

#include "Transform.hpp"
#include "Schedule.hpp"

/*!
 * \brief Apply the security level
 * 
 * \see ApplySecurityLevel::help
 * 
 * Remove version of task from the graph that have a security level below the
 * configured one.
 */
class ApplySecurityLevel : public Transform {
public:
    //! Rule to break tie
    enum tie_breaking_e {
        MAXSECU, //!< always choose the version with the highest security level
        MINSECU, //!< always choose the version with the lowest security level
        SCHEDULER //!< leave the scheduler decides, add a conditional edge
    };
private:
    //! Configured security level
    int lvl = 0;
    //! Selected rule to break tie
    ApplySecurityLevel::tie_breaking_e tb = SCHEDULER;
public:
    //! \copydoc CompilerPass::forward_params
    void forward_params(const std::map<std::string, std::string> &args) override;
    virtual std::string help() override;
    //! \copydoc CompilerPass::get_uniqid_rtti
    const std::string get_uniqid_rtti() const override;
    
    //! \brief Constructor
    ApplySecurityLevel();
    
protected:
    //! \copydoc Transform::apply_transform
    void apply_transform() override;
};

/*!
 * \remark Register the ApplySecurityLevel class as a transformation with config id
 * "security"
 */
REGISTER_TRANSFORM(ApplySecurityLevel, "security");

#endif
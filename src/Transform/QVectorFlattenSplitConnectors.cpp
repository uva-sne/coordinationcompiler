/*!
 * \file QVectorFlattenSplitConnectors.cpp
 * \copyright GNU Public License v3.
 * \date 2019 
 * \author Benjamin Rouxel <benjamin.rouxel@uva.nl>, University of Amsterdam
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "QVectorFlattenSplitConnectors.hpp"
#include "Utils/Log.hpp"

#include <cmath>
#include <queue>
#include <boost/algorithm/string.hpp>

using namespace std;

QVectorFlattenSplitConnectors::QVectorFlattenSplitConnectors() : QVectorFlatten() {}

IMPLEMENT_HELP(QVectorFlattenSplitConnectors, 
    Expand each graph with the expansion ratio computed by the repetition_vector 
    compiler pass. It duplicates task in the graph, and connect it to
    the proper successors according to the initial graph. Each duplicate
    of a task is called a replica. In the code generation each replica has 
    its own coord function, but at the end call the same user function
    corresponding to the initial replicated task.\n
    The user function could need the replicate id number to, i.e. access
    an array of something. This can be parametrised with the `replica-var`.\n
    \n
    Compare to `homogenize`, this compiler pass splits
    connectors on an edge, which means consumption and production for 
    each edge is identical. And each edge has only a single source and
    a single sink.
    \n
    * replica-var, default `int repnum`: this is found in the signature of the function
    either from the NFP file or in the coord file. \n
    As an example: `int repnum` given in replica-var, and found in task
    function signature will replace this with the id of the replica.
)

const string QVectorFlattenSplitConnectors::get_uniqid_rtti() const { return "transform-homogenize_splitters"; }

void QVectorFlattenSplitConnectors::connect_join(Task *basesrc, Task *src, Task *basesink, Connection *basecon, int nbtokprod, int nbtokcons) {
    list<Task*> withdrawn_sinks;
    Task *sink = get_sink(basesink, basesrc, basecon, withdrawn_sinks);

    Connector *csrc = get_connector(src, basecon->from(), nbtokprod, "src");
    Connector *csink = get_connector(sink, basecon->to(), nbtokprod, "sink");

    tg->add_dependency(src, sink, csrc, csink);
    set_state_sink(basesink, sink);
    for(Task *t : withdrawn_sinks)
        available[basesink].push_front(t);
}

void QVectorFlattenSplitConnectors::connect_split(Task *basesrc, Task *src, Task *basesink, Connection *basecon, int nbtokprod, int nbtokcons) {
    list<Task*> withdrawn_sinks;
    //make sure that we don't reuse a sink that we already filled
    vector<Task*> sink_used; 

    // while all produced tokens haven't been distributed
    while(nbtokprod-nbtokcons >= 0) {
        Connector *csrc = get_connector(src, basecon->from(), nbtokcons, "src");
        Task *sink = get_sink(basesink, basesrc, basecon, withdrawn_sinks);
        Connector *csink = get_connector(sink, basecon->to(), nbtokcons, "sink");

        tg->add_dependency(src, sink, csrc, csink);
        sink_used.push_back(sink);
        nbtokprod -= nbtokcons;
    }
    if(nbtokprod > 0) {
        Connector *csrc = get_connector(src, basecon->from(), nbtokcons, "src");
        Task *sink = get_sink(basesink, basesrc, basecon, withdrawn_sinks);
        Connector *csink = get_connector(sink, basecon->to(), nbtokcons, "sink");
        tg->add_dependency(src, sink, csrc, csink);
        sink_used.push_back(sink);
    }
    for(Task *s : sink_used)
        set_state_sink(basesink, s);
    for(Task *t : withdrawn_sinks)
        available[basesink].push_front(t);
}

Connector* QVectorFlattenSplitConnectors::get_connector(Task *t, Connector *basecon, int nbtokens, const string & place) {
    Connector *c = new Connector(basecon->id()+"_r"+to_string(counter++), nbtokens, basecon->token_type(), basecon->tainted());
    vector<Connector *> *conlist = (place == "sink") ? &t->inputs() : &t->outputs();
    conlist->push_back(c);
    return c;
}

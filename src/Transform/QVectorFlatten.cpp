/*!
 * \file QVector.cpp
 * \copyright GNU Public License v3.
 * \date 2019 
 * \author Benjamin Rouxel <benjamin.rouxel@uva.nl>, University of Amsterdam
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "QVectorFlatten.hpp"
#include "Utils/Log.hpp"

#include <cmath>
#include <queue>
#include <boost/algorithm/string.hpp>

using namespace std;

IMPLEMENT_HELP(QVectorFlatten,
    Expand each graph with the expansion ratio computed by the repetition_vector 
    compiler pass. It duplicates task in the graph, and connect it to
    the proper successors according to the initial graph. Each duplicate
    of a task is called a replica. In the code generation each replica has 
    its own coord function, but at the end call the same user function
    corresponding to the initial replicated task.\n
    The user function could need the replicate id number to, i.e. access
    an array of something. This can be parametrised with the `replica-var`.\n
    \n
    Compare to `homogenize-splitters`, this compiler pass does not split
    connectors on an edge, this leads to the usage of arrays of tokens
    in the code generation.
    \n
    * replica-var, default `int repnum`: this is found in the signature of the function
    either from the NFP file or in the coord file. \n
    As an example: `int repnum` given in replica-var, and found in task
    function signature will replace this with the id of the replica.
)

void QVectorFlatten::forward_params(const map<string, string> &args) {
    if(args.count("replica-var"))
        replica_var = args.at("replica-var");
}

QVectorFlatten::QVectorFlatten() : Transform() {}

const string QVectorFlatten::get_uniqid_rtti() const { return "transform-homogenize"; }

/*!
 * \remark depends on #QVector
 * \remark depends on #CycleCheck
 * \remark mutually exclusive with #QVectorFlattenSplitConnectors
 */
void QVectorFlatten::check_dependencies() {
    if (!conf->passes.find_pass_before("analyse-repetition_vector", this)) {
        throw CompilerException("dependency check", "Flattening the graph to get the homogeneous version requires the computation of the repetition vector first to properly work");
    }
    if (!conf->passes.find_pass_before("transform-homogenize_splitters", this)) {
        throw CompilerException("dependency check", "Transformations homogenize and homogenize-splitters are mutually exclusive");
    }
    if (!conf->passes.find_pass_before("analyse-cycle")) {
        throw CompilerException("dependency check", "Homogenize requires to have no cycle, please check it first");
    }
}

void QVectorFlatten::apply_transform() {
    map<Task*, vector<Task*>> added; // adding task to tg->tasks() invalidate iterator
    map<Task*, Task*> inv_added;
    
    for (Task *t : tg->tasks_it()) {
        size_t nbrepeat = t->repeat();
        for(size_t i= 0; i < nbrepeat ; ++i) {
            Task *nt = new Task(t->id()+"_r"+to_string(i));
            nt->clone(t);
            nt->repeat(1);
            for(Version *v : nt->versions()) {
                string sig = v->csignature();
                boost::algorithm::replace_first(sig, replica_var, to_string(i));
            }
            
            added[t].push_back(nt);
            inv_added[nt] = t;
            if(t->predecessor_tasks().size() == 0)
                ready.push_back(nt);
            else
                available[t].push_back(nt);
        }
    }
    vector<Task*> tasks = Utils::mapkeys(inv_added);
    for(Task *t : tasks)
        tg->add_task(t);
    
    // add edges/connectors
    Task *last = nullptr;
    while(!ready.empty()) {
        Task *r = get_next_ready(last, inv_added);
        Task *t = inv_added[r];
        simulate(t, r);
        last  = t;
    }
    
//     delete all base tasks
    vector<Task*> oldtasks = Utils::mapkeys(added);
    for(Task *t : oldtasks) {
        tg->tasks().erase(t->id());
        delete t;
    }
    
    tg->compute_hyperperiod();
    tg->compute_sequential_schedule_length();
}

Task* QVectorFlatten::get_next_ready(Task *lastbase, map<Task*, Task*> added) {
    list<Task*> copy;
    while(!ready.empty() && added[ready.front()] != lastbase) {
        copy.push_back(ready.front());
        ready.pop_front();
    }
    
    Task *r;
    if(ready.empty()) {
        r = copy.front();
        copy.pop_front();
    }
    else {
        r = ready.front();
        ready.pop_front();
    }
    while(!copy.empty()) {
        ready.push_front(copy.back());
        copy.pop_back();
    }
    return r;
}

void QVectorFlatten::simulate(Task *base, Task *rep) {
    for(Connector *elcon : base->outputs()) {
        for(Task::dep_t elsuc : base->successor_tasks()) {
            for (Connection *conn : elsuc.second) {
                Connector *outport = conn->from();
                Connector *inport = conn->to();
                if (outport != elcon) continue;

                int nbtokprod = (signed) outport->tokens();
                int nbtokcons = (signed) inport->tokens();

                Utils::DEBUG("Simulate " + base->id() + " - " + rep->id() + " -- " + elcon->id() + " - " + elsuc.first->id() + " --- p: " + to_string(nbtokprod) + ", c: " +
                        to_string(nbtokcons));

                if (nbtokprod == nbtokcons) {
                    connect_regular(base, rep, elsuc.first, conn, nbtokprod);
                } else if (nbtokprod < nbtokcons) {
                    connect_join(base, rep, elsuc.first, conn, nbtokprod, nbtokcons);
                } else { //nbtokprod > nbtokcons
                    connect_split(base, rep, elsuc.first, conn, nbtokprod, nbtokcons);
                }
            }
        }
    }
}

void QVectorFlatten::connect_regular(Task *basesrc, Task *src, Task *basesink, Connection *outconn, int nbtok) {
    list<Task*> withdrawn_sinks;
    Task *sink = get_sink(basesink, basesrc, outconn, withdrawn_sinks);

    Connector *csrc = get_connector(src, outconn->from(), "src");
    Connector *csink = get_connector(sink, outconn->to(), "sink");

    tg->add_dependency(src, sink, csrc, csink);
    
    set_state_sink(basesink, sink);
    for(Task *t : withdrawn_sinks)
        available[basesink].push_front(t);
}

void QVectorFlatten::connect_join(Task *basesrc, Task *src, Task *basesink, Connection *outconn, int nbtokprod, int nbtokcons) {
    list<Task*> withdrawn_sinks;
    Task *sink = get_sink(basesink, basesrc, outconn, withdrawn_sinks);

    Connector *csrc = get_connector(src, outconn->from(), "src");
    Connector *csink = get_connector(sink, outconn->to(), "sink");

    csrc->type(Connector::type_e::JOINED);
    csink->type(Connector::type_e::JOINER);
    
    // Need to check if we have to send the source tokens to multiple sinks
    dataqty_t current_rec = 0;
    for(Task::dep_t el : sink->predecessor_tasks()) {
        for (Connection *conn: el.second) {
            if (conn->to() != csink) continue;
            size_t send_index = Utils::indexof(el.first->successor_tasks()[sink], conn).value();
            current_rec += el.first->eff_suc_send()[sink][send_index];
        }
    }

    Connection *newconn = tg->add_dependency(src, sink, csrc, csink);

    size_t send_index = Utils::indexof(src->successor_tasks()[sink], newconn).value();
    size_t receive_index = Utils::indexof(sink->predecessor_tasks()[src], newconn).value();
    src->eff_suc_send()[sink][send_index] =
            sink->eff_prev_rec()[src][receive_index] = Utils::min(nbtokprod, nbtokcons - current_rec);
    set_state_sink(basesink, sink);
    for(Task *t : withdrawn_sinks)
        available[basesink].push_front(t);
    
    Utils::DEBUG("-> Join "+src->id()+"->"+sink->id()+" -- "+to_string(current_rec)+" + "+to_string(nbtokprod)+" >? "+to_string(nbtokcons));
    if(current_rec + nbtokprod > nbtokcons)
        connect_join(basesrc, src, basesink, newconn, nbtokprod - src->eff_suc_send()[sink][send_index], nbtokcons);
}

void QVectorFlatten::connect_split(Task *basesrc, Task *src, Task *basesink, Connection *outconn, int nbtokprod, int nbtokcons) {
    Connector *basesrccon = outconn->from();
    Connector *csrc = get_connector(src, basesrccon, "src");
    csrc->type(Connector::type_e::SPLITTER);
    
    //make sure that we don't reuse a sink that we already filled
    vector<Task*> sink_used; 

    list<Task*> withdrawn_sinks;
    // while all produced tokens haven't been distributed
    while(nbtokprod-nbtokcons >= 0) {
        Task *sink = get_sink(basesink, basesrc, outconn, withdrawn_sinks);
        Connector *csink = get_connector(sink, outconn->to(), "sink");
        csink->type(Connector::type_e::SPLITTED);

        Connection *newconn = tg->add_dependency(src, sink, csrc, csink);

        size_t send_index = Utils::indexof(src->successor_tasks()[sink], newconn).value();
        size_t receive_index = Utils::indexof(sink->predecessor_tasks()[src], newconn).value();
        src->eff_suc_send()[sink][send_index] = sink->eff_prev_rec()[src][receive_index] = nbtokcons;
        sink_used.push_back(sink);
        Utils::DEBUG("-> Split "+src->id()+"->"+sink->id()+" -- p "+to_string(nbtokprod)+" /  c "+to_string(nbtokcons));
        nbtokprod -= nbtokcons;
    }
    if(nbtokprod > 0) {
        Task *sink = get_sink(basesink, basesrc, outconn, withdrawn_sinks);
        Connector *csink = get_connector(sink, outconn->to(), "sink");
        csink->type(Connector::type_e::SPLITTED);
        
        tg->add_dependency(src, sink, csrc, csink);
        sink_used.push_back(sink);
        Utils::DEBUG("-> Split "+src->id()+"->"+sink->id()+" -- p "+to_string(nbtokprod)+" / c "+to_string(nbtokcons));
    }
    for(Task *s : sink_used)
        set_state_sink(basesink, s);
    for(Task *t : withdrawn_sinks)
        available[basesink].push_front(t);
}

Task * QVectorFlatten::get_sink(Task *basesink, Task *basesrc, Connection *baseconn, list<Task*> &withdrawn_sinks) {
    Task *sink = nullptr;
    while(1) {
        if(available[basesink].empty())
            throw std::logic_error("Task has no sink pending and no sinks available.");
        
        sink = available[basesink].front();
        available[basesink].pop_front();
    
        Connector *consink = nullptr;
        for(Connector *a : sink->inputs()) {
            if(a->id() == baseconn->to()->id())
                consink = a;
        }
        if(consink == nullptr)
            break; //sink has no connector, no edge has already been added to represebt basesrc->basesink
        else {
            //check if the connector is full and we need to take another replica for the sink
            size_t supposed_to_rec = 0;
            for(Task::dep_t ep : basesink->predecessor_tasks()) {
                for (Connection *conn : ep.second)
                    if (conn->to()->id() == consink->id())
                        supposed_to_rec += conn->to()->tokens();
            }
            size_t currently_sent = 0;
            for(Task::dep_t ep : sink->predecessor_tasks()) {
                for (Connection *conn : ep.second)
                    if (conn->to()->id() == consink->id())
                        currently_sent += conn->from()->tokens();
            }
            if(supposed_to_rec != currently_sent)
                break; //we can reuse this sink as the connector do not receive everything yet
            else
                withdrawn_sinks.push_front(sink);
        }
    }
    
    return sink;
}

Connector* QVectorFlatten::get_connector(Task *t, Connector *basecon, const string & place) {
    Connector *c = nullptr;
    vector<Connector *> *conlist = (place == "sink") ? &t->inputs() : &t->outputs();
    
    for(Connector *tmp : *conlist) {
        if(tmp->id() == basecon->id()) {
            c = tmp;
            break;
        }
    }
    if(c == nullptr) {
        c = new Connector(*basecon);
        conlist->push_back(c);
    }
    return c;
}

void QVectorFlatten::set_state_sink(Task *base, Task *rep) {
    //if rep is connected to all its dependencies, then it is ready
    //if rep is ready to fire, then place it in ready, either put it back in available
    
    if(base->inputs().size() > rep->inputs().size()) {
        available[base].push_front(rep);
        return;
    }
    
    size_t supposed_to_rec = 0;
    size_t currently_sent = 0;
    for(Task::dep_t ep : base->predecessor_tasks()) {
        size_t rec = 0;
        for (Connection *conn : ep.second) {
            if (rec != 0 && rec != conn->to()->tokens())
                std::logic_error("Multiple edges must sent same data qt " + base->id() + " and " + rep->id()); // this could be relaxed
            rec = conn->to()->tokens();
        }
        supposed_to_rec += rec;
    }
    for(Task::dep_t ep : rep->predecessor_tasks()) {
        currently_sent += ep.first->successor_tasks()[rep][0]->from()->tokens();
    }
    if(supposed_to_rec > currently_sent) {
        available[base].push_front(rep);
    }
    else {
        ready.push_back(rep);
    }
}

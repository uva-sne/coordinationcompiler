/*!
 * \file NormalizeSrcSink.cpp
 * \copyright GNU Public License v3.
 * \date 2019 
 * \author Benjamin Rouxel <benjamin.rouxel@uva.nl>, University of Amsterdam
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "NormalizeSrcSink.hpp"
#include "Utils/Graph.hpp"

using namespace std;

NormalizeSrcSink::NormalizeSrcSink() : Transform() {}

IMPLEMENT_HELP(NormalizeSrcSink, 
    For each graph of the application, normalise it by adding a dummy 
    source or dummy sink to it if it has more than one source, resp sink.
)
void NormalizeSrcSink::forward_params(const map<string,string>&) {}

const string NormalizeSrcSink::get_uniqid_rtti() const { return "transform-normalize-src-sink"; }

void NormalizeSrcSink::apply_transform() {
    vector<vector<Task*>> DAGS;
    Utils::extractGraphs(tg, &DAGS);

    for(vector<Task*> g : DAGS) {
        vector<Task*> src;
        vector<Task*> sink;
        for(Task *t : g) {
            if(t->predecessor_tasks().size() == 0)
                src.push_back(t);
            if(t->successor_tasks().size() == 0)
                sink.push_back(t);
        }
        normalize(src, sink);
    }
}

void NormalizeSrcSink::normalize(vector<Task*> src, vector<Task*> sink) {
    if(src.size() > 1) {
        Utils::DEBUG("Add a unique source node");
        Task* t = new Task("dummy_src");
        tg->add_task(t);
        
        size_t cnt = 0;
        for(Task *s : src) {
            if(t->T() != 0 && s->T() != t->T()) 
                throw CompilerException(get_uniqid_rtti(), "Can't handle graph with different src period");
            if(t->D() != 0 && s->D() != t->D()) 
                throw CompilerException(get_uniqid_rtti(), "Can't handle graph with different src deadline");
            t->T(s->T());
            t->D(s->D());
            
            Connector *c_in = new Connector("dummyIn"+to_string(cnt), 0, "void", false);
            Connector *c_out = new Connector("dummyOut"+to_string(cnt), 0, "void", false);
            ++cnt;
            s->inputs().push_back(c_in);
            t->outputs().push_back(c_out);
            tg->add_dependency(t, s, c_out, c_in);
        }
    }
    
    if(sink.size() > 1) {
        Utils::DEBUG("Add a unique sink node");
        Task* t = new Task("dummy_sink");
        tg->add_task(t);
        
        size_t cnt = 0;
        for(Task *s : sink) {
            if(t->T() != 0 && s->T() != t->T()) 
                throw CompilerException(get_uniqid_rtti(), "Can't handle graph with different sink period");
            if(t->D() != 0 && s->D() != t->D()) 
                throw CompilerException(get_uniqid_rtti(), "Can't handle graph with different sink deadline");
            t->T(s->T());
            t->D(s->D());
            
            Connector *c_in = new Connector("dummyIn"+to_string(cnt), 0, "void", false);
            Connector *c_out = new Connector("dummyOut"+to_string(cnt), 0, "void", false);
            ++cnt;
            t->inputs().push_back(c_in);
            s->outputs().push_back(c_out);
            tg->add_dependency(s, t, c_out, c_in);
        }
    }
}
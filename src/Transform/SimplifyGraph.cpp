/*!
 * \file SimplifyGraph.cpp
 * \copyright GNU Public License v3.
 * \date 2019 
 * \author Benjamin Rouxel <benjamin.rouxel@uva.nl>, University of Amsterdam
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "SimplifyGraph.hpp"
#include "Utils/Graph.hpp"
#include "TransitivClosure.hpp"

using namespace std;

SimplifyGraph::SimplifyGraph() : Transform() {}

IMPLEMENT_HELP(SimplifyGraph, 
    For each graph of the application, normalise it by adding a dummy 
    source or dummy sink to it if it has more than one source, resp sink.
)
void SimplifyGraph::forward_params(const map<string,string>&) {}

const string SimplifyGraph::get_uniqid_rtti() const { return "transform-simplify"; }

void SimplifyGraph::apply_transform() {
    TransitiveClosure tc;
    tc.run(tg, const_cast<config_t*>(conf), nullptr);
    
    vector<vector<Task*>> DAGS;
    Utils::extractGraphs(tg, &DAGS);

    for(vector<Task*> dag : DAGS)  {
        bool is_stable;
        do {
            is_stable = true;
            Utils::rBFS(&dag, [&is_stable](Task *t) {
                vector<Task*> to_delete;

                Task::connector_map_t predecessors = t->predecessor_tasks();
                for(Task::dep_t a : predecessors) {
                    Task *a_pred = a.first;
                    vector<Task*> a_pred_trans = a_pred->previous_transitiv();
                    for(Task::connector_map_t::iterator it=predecessors.begin(), et=predecessors.end() ; it != et ; ++it) {
                        Task::dep_t b = *it;
                        if(a == b) continue;
                        Task *b_pred = b.first;
                        if(find(a_pred_trans.begin(), a_pred_trans.end(), b_pred) != a_pred_trans.end()) {
                            to_delete.push_back(b_pred);
                            Utils::DEBUG("Delete "+t->id()+" -> "+b_pred->id()+" through "+a_pred->id());
                            is_stable = false;
                        }
                    }
                }

                sort(to_delete.begin(), to_delete.end(), [](const Task* a, const Task* b) { return a->id() < b->id();});
                auto it2 = unique(to_delete.begin(), to_delete.end());
                to_delete.resize(distance(to_delete.begin(), it2));
                for(Task* it : to_delete) {
                    t->predecessor_tasks().erase(it);
                    it->successor_tasks().erase(t);
                }
            });
        }
        while(!is_stable);
    }
    
    for (Task *t : tg->tasks_it()) {
        vector<Connector*> to_delete;
        vector<Connector*> used_conn = Utils::flatmapvalues(t->predecessor_tasks(), Task::connector_to);
        for(Connector *c : t->inputs()) {
            if(find(used_conn.begin(), used_conn.end(), c) == used_conn.end()) {
                to_delete.push_back(c);
            }
        }
        for(Connector *c : to_delete) {
            for(vector<Connector*>::iterator it=t->inputs().begin(), et=t->inputs().end() ; it != et ; ++it) {
                if(*it == c) {
                    t->inputs().erase(it);
                    break;
                }
            }
        }
        
        to_delete.clear();
        used_conn = Utils::flatmapvalues(t->successor_tasks(), Task::connector_from);
        for(Connector *c : t->outputs()) {
            if(find(used_conn.begin(), used_conn.end(), c) == used_conn.end()) {
                to_delete.push_back(c);
            }
        }
        for(Connector *c : to_delete) {
            for(vector<Connector*>::iterator it=t->outputs().begin(), et=t->outputs().end() ; it != et ; ++it) {
                if(*it == c) {
                    t->outputs().erase(it);
                    break;
                }
            }
        }
    }
}

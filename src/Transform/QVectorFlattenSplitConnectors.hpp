/*!
 * \file QVectorFlattenSplitConnectors.hpp
 * \copyright GNU Public License v3.
 * \date 2019 
 * \author Benjamin Rouxel <benjamin.rouxel@uva.nl>, University of Amsterdam
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef QVECTORFLATTENSPLITTERS_H
#define QVECTORFLATTENSPLITTERS_H

#include "QVectorFlatten.hpp"

#include <cmath>
#include <queue>
#include <list>
#include <boost/algorithm/string.hpp>

/*!
 * \brief Expand an SDF graph
 * 
 * \see QVectorFlattenSplitConnectors::help
 * 
 * \remark Difference with QVectorFlatten is that connectors are split when sending/receiving
 * tokens from multiple replicate
 */
class QVectorFlattenSplitConnectors : public QVectorFlatten {
protected:
    //! used to have unique connector ID
    size_t counter = 0;

public:
    //! \brief Constructor
    QVectorFlattenSplitConnectors();
    
    //! \copydoc CompilerPass::get_uniqid_rtti
    const std::string get_uniqid_rtti() const override;
    virtual std::string help() override;
    
protected:
    //! \copydoc QVectorFlatten::connect_split
    void connect_split(Task *basesrc, Task *src, Task *basesink, Connection *basecon, int nbtokprod, int nbtokcons) override;
    
    //! \copydoc QVectorFlatten::connect_join
    void connect_join(Task *basesrc, Task *src, Task *basesink, Connection *basecon, int nbtokprod, int nbtokcons) override;
    
    /*! \brief Get an available connector or create one
     * 
     * If a connector with the same name already exists for the replica as in the base then reuse it
     * either create a new one
     * 
     * \param t Replica task to sink the connector
     * \param basecon Connector from the initial graph
     * \param nbtokens Nb tokens the connector manages
     * \param place {source;sink} to know if the connector is input or output
     * \return the connector to use
     */
    Connector* get_connector(Task *t, Connector *basecon, int nbtokens, const std::string& place);
};

/*!
 * \remark Register the QVectorFlattenSplitConnectors class as a transformation with config id
 * "homogeneise_split"
 */ 
REGISTER_TRANSFORM(QVectorFlattenSplitConnectors, "homogenize-split")

#endif //QVECTORFLATTENSPLITTERS_H
/*!
 * \file FlattenMultiPeriodDAG.cpp
 * \copyright GNU Public License v3.
 * \date 2019 
 * \author Benjamin Rouxel <benjamin.rouxel@uva.nl>, University of Amsterdam
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "FlattenMultiPeriodDAG.hpp"
#include "Utils/Log.hpp"
#include "Utils/Graph.hpp"

using namespace std;

LowerMultiPeriodDAG::LowerMultiPeriodDAG() : Transform() {}

IMPLEMENT_HELP(LowerMultiPeriodDAG, 
    When 2 dependent tasks have different periods, this compiler pass 
    delete the edge between them to allow the different periodicity
    to be handled, but keep the dependency as virtual to allow the
    code generation to put an edge implementation between the two.
)
void LowerMultiPeriodDAG::forward_params(const map<string,string>&) {}

const string LowerMultiPeriodDAG::get_uniqid_rtti() const { return "transform-lower_multiperiod_dag"; }

//! \remark depends on #PropagateProperties
void LowerMultiPeriodDAG::check_dependencies() {
    if(!conf->passes.find_pass_before("transform-propagate_properties", this)) {
        throw CompilerException("propagate-timing-infos", "Dependency unsatisfied, need to propagate timing infos with transform-propagate_properties");
    }
}

void LowerMultiPeriodDAG::apply_transform() {
    vector<vector<Task*>> dags;
    Utils::extractGraphs(tg, &dags);
    for(vector<Task*> dag : dags) {
        uint64_t period = 0;
        for(Task *t : dag) {
            if(period > 0 && period != t->T()) {
                lower_to_periodic(dag);
                break;
            }
            period = t->T();
        }
    }
}

void LowerMultiPeriodDAG::lower_to_periodic(vector<Task*> dag) {
    for(Task *t : dag) {
        Utils::DEBUG("Deleting dependencies for "+t->id());
        for(Task::dep_t el : t->predecessor_tasks())
            t->previous_virtual()[el.first] = el.second;
        for(Task::dep_t el : t->successor_tasks())
            t->successors_virtual()[el.first] = el.second;
        t->predecessor_tasks().clear();
        t->successor_tasks().clear();
        t->previous_transitiv().clear();
        t->successors_transitiv().clear();
    }
}
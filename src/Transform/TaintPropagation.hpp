/*
 * Copyright (C) 2020 Benjamin Rouxel <benjamin.rouxel@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TAINTPROPAGATION_HPP
#define TAINTPROPAGATION_HPP

#include <vector>

#include "Utils/Log.hpp"
#include "Transform.hpp"

/**
 * Propagate tainted tasks thoughout graphs
 * 
 * Tainting a task is done in the TeamPlay specification file. It marks a task
 * as a security risk which requires extra protection.
 * 
 * When a task is marked as tainted, its successors are marked as tainted as well
 * etc... If an edge is marked as tainted, its src/sink are marked as tainted.
 * 
 */
class TaintPropagation : public Transform {
public:
    TaintPropagation();
    void forward_params(const std::map<std::string,std::string>&) override;
    const std::string get_uniqid_rtti() const override;
    void check_dependencies() override;
    
    virtual std::string help();
protected:
    void apply_transform() override;
    
    /**
     * Propagate the taint
     * 
     * When a task is marked as tainted, its successors are marked as tainted as well
     * etc... If an edge is marked as tainted, its src/sink are marked as tainted.
     * @param t
     * @param visited
     */
    void propagate_taint(Task *t, std::vector<Task*> *visited);
};

REGISTER_TRANSFORM(TaintPropagation, "tainting")

#endif /* TAINTPROPAGATION_HPP */


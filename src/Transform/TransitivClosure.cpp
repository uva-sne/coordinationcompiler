/*!
 * \file TransitivClosure.cpp
 * \copyright GNU Public License v3.
 * \date 2019 
 * \author Benjamin Rouxel <benjamin.rouxel@uva.nl>, University of Amsterdam
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "TransitivClosure.hpp"
#include "Utils/Graph.hpp"
using namespace std;

TransitiveClosure::TransitiveClosure() : Transform() {}

IMPLEMENT_HELP(TransitiveClosure, 
    Compute the transitive closure of each graph.
)
void TransitiveClosure::forward_params(const map<string,string>&) {}

const string TransitiveClosure::get_uniqid_rtti() const { return "transform-transitive-closure"; }

void TransitiveClosure::apply_transform() {
    vector<vector<Task*>> dags;
    Utils::extractGraphs(tg, &dags);
    
    for(vector<Task*> dag : dags) {
        Utils::BFS(&dag, [](Task *t) {
            for(Task::dep_t elp : t->predecessor_tasks()) {
                Task *pred = elp.first;
                t->previous_transitiv().push_back(pred);
                for(Task *transpred : pred->previous_transitiv()) {
                    t->previous_transitiv().push_back(transpred);
                }
            }
        });
        Utils::rBFS(&dag, [](Task *t) {
            for(Task::dep_t els : t->successor_tasks()) {
                Task *succ = els.first;
                t->successors_transitiv().push_back(succ);
                for(Task *transsucc : succ->successors_transitiv()) {
                    t->successors_transitiv().push_back(transsucc);
                }
            }
        });
    }
}

/*!
 * \file FlattenMultiPeriodDAG.hpp
 * \copyright GNU Public License v3.
 * \date 2019 
 * \author Benjamin Rouxel <benjamin.rouxel@uva.nl>, University of Amsterdam
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef FLATTENMULTIPERIODDAG_H
#define FLATTENMULTIPERIODDAG_H

#include "Transform.hpp"

/*!
 * \brief Lower DAG with tasks with multiple different periods
 * 
 * \see #LowerMultiPeriodDAG::help
 * 
 * \copydetails LowerMultiPeriodDAG::help()
 */
class LowerMultiPeriodDAG : public Transform {
public:
    //! \brief Constructor
    LowerMultiPeriodDAG();
    virtual std::string help() override;
    //! \copydoc CompilerPass::get_uniqid_rtti
    const std::string get_uniqid_rtti() const override;
    void forward_params(const std::map<std::string,std::string>&) override;
    
protected:
    //! \copydoc Transform::check_dependencies
    void check_dependencies() override;
    //! \copydoc Transform::apply_transform
    void apply_transform() override;
    
    /*!\brief Lower a tag to periodic taskset
     * 
     * Remove all dependencies
     * 
     * \todo adapt period/deadline/offset
     * 
     * \param dag the DAG to lower
     */
    void lower_to_periodic(std::vector<Task*> dag);
};

/*!
 * \remark Register the LowerMultiPeriodDAG class as a transformation with config id
 * "lower_multiperiod_dag"
 */ 
REGISTER_TRANSFORM(LowerMultiPeriodDAG, "lower_multiperiod_dag")

#endif
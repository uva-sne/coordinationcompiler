/*
 * Copyright (C) 2023 Lukas Miedema <l.miedema@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <StateMachine/FaultTolerance/NMR.hpp>
#include "ApplyFaultTolerance.hpp"
#include "ScheduleFactory.hpp"

IMPLEMENT_HELP(ApplyFaultTolerance, Apply replication to the scheduling elements as per the profile specifications);

void ApplyFaultTolerance::apply_transform() {
    if (!_schedule->is_built())
        ScheduleFactory::build_elements(this->_schedule);

    // Each no of replications has an associated FT scheme, keep track of those and their state
    std::map<size_t, std::unique_ptr<FaultToleranceScheme>> replSchemes;
    std::map<size_t, std::unique_ptr<ft_state_t>> replState;

    replSchemes[1] = std::make_unique<NullFaultTolerance>();
    replState[1] = replSchemes[1]->new_state();

    std::map<std::string, SchedEltPtr> result;
    mapping_state_t mapping;
    for (SchedEltPtr &element : this->_schedule->elements()) {
        std::string replicaStr = element->task()->profile().get("nModular_replicas");
        uint32_t replicas = 1;
        if (!replicaStr.empty()) {
            replicas = std::stoi(replicaStr);
        }
        Utils::INFO("Creating " + std::to_string(replicas) + " replicas for " + element->id());

        if (!replSchemes.count(replicas)) {
            switch (replicas) {
                case 2: replSchemes[2] = std::make_unique<NMR_2>(); break;
                case 3: replSchemes[3] = std::make_unique<NMR_3>(); break;
                case 4: replSchemes[4] = std::make_unique<NMR_4>(); break;
                case 5: replSchemes[5] = std::make_unique<NMR_5>(); break;
                default: throw CompilerException("fault-tolerance", std::to_string(replicas) + " replicas not supported");
            }
            replState[replicas] = replSchemes[replicas]->new_state();
        }

        replSchemes[replicas]->apply(element.get(), &result, replState[replicas].get(), &mapping);
    }
    this->_schedule->elements_lut() = std::move(result);
}

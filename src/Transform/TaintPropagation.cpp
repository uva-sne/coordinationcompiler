/*
 * Copyright (C) 2020 Benjamin Rouxel <benjamin.rouxel@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "TaintPropagation.hpp"

using namespace std;

TaintPropagation::TaintPropagation() : Transform() {}

void TaintPropagation::forward_params(const map<string,string>&) {}
IMPLEMENT_HELP(TaintPropagation, 
    Propagate "taint" information to successing tasks/components
)

const string TaintPropagation::get_uniqid_rtti() const { return "transform-tainting"; }


void TaintPropagation::check_dependencies() {
    if(!conf->passes.find_pass_before("analyse-cycle", this)) {
        Utils::WARN(
            "Dependency unsatisfied, the \"analyse-cycle\" "
            "pass should be executed before the \""+get_uniqid_rtti()+"\" pass "
            "to guarantee that no cycle exists."
        );
    }
}

void TaintPropagation::apply_transform() {
    for (Task *t : tg->tasks_it()) {
        if(t->tainted() > 0) {
            vector<Task*> visited;
            propagate_taint(t, &visited);
        }
    }
    for (Task *t : tg->tasks_it()) {
        for(Connector *c : t->inputs()) {
            if(c->tainted()) {
                t->tainted(1);
                for(Task::dep_t elprev : t->predecessor_tasks()) {
                    for (Connection *conn : elprev.second)
                        if(conn->to() == c)
                            elprev.first->tainted(1);
                }
            }
        }
        
        for(Connector *c : t->outputs()) {
            if(c->tainted()) {
                t->tainted(1);
                for(Task::dep_t elsuc : t->successor_tasks()) {
                    for (Connection *conn : elsuc.second)
                        if(conn->from() == c)
                            elsuc.first->tainted(1);
                }
            }
        }
    }
}

void TaintPropagation::propagate_taint(Task *t, vector<Task*> *visited) {
    for(Task::dep_t elsuc : t->successor_tasks()) {
        if(elsuc.first->tainted() == 0 || elsuc.first->tainted() > t->tainted()+1) {
            elsuc.first->tainted(t->tainted()+1);
            propagate_taint(elsuc.first, visited);
        }
    }
}

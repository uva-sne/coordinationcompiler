/*
 * Copyright (C) 2023 Lukas Miedema <l.miedema@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include "Transform.hpp"

/**
 * Apply fault-tolerance per the profile (task->profile->...) specification to the scheduling elements.
 * This will build the SchedElts if they are not already build, and makes use of strategy/fault-tolerance machinery.
 */
class ApplyFaultTolerance : public Transform {
public:
    const std::string get_uniqid_rtti() const override {
        return "transform-fault-tolerance";
    }
    std::string help() override;

private:
    Schedule *_schedule = nullptr;

protected:
    void forward_params(const std::map<std::string, std::string> &_) override {};
    void run(SystemModel *m, config_t *c, Schedule *s) override {
        this->_schedule = s; // ensure we have access to this
        Transform::run( m, c, s);
    }
    void apply_transform() override;
};

REGISTER_TRANSFORM(ApplyFaultTolerance, "fault-tolerance");

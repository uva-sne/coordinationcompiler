/*!
 * \file QVector.hpp
 * \copyright GNU Public License v3.
 * \date 2019 
 * \author Benjamin Rouxel <benjamin.rouxel@uva.nl>, University of Amsterdam
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef QVECTORFLATTEN_H
#define QVECTORFLATTEN_H

#include "Transform.hpp"

#include <cmath>
#include <queue>
#include <boost/algorithm/string.hpp>

/*!
 * \brief Expand an SDF graph
 * 
 * \see QVectorFlatten::help
 * 
 * \remark Tasks present in the input graph are erased and replaced by as many 
 * copies as necessary.
 */
class QVectorFlatten : public Transform {
protected:
    //! list of available duplicate task that have not been added to the new graph
    std::map<Task*, std::list<Task*>> available;
    //! list of task ready for expansion according to their precedences
    std::list<Task*> ready;
    //! name of the argument used by the user code to identify the task duplicate
    std::string replica_var = "int repnum";
    
public:
    virtual void forward_params(const std::map<std::string, std::string> &args) override;
    virtual std::string help() override;
    QVectorFlatten();
    
    //! \copydoc CompilerPass::get_uniqid_rtti
    const std::string get_uniqid_rtti() const override;
    //! \copydoc CompilerPass::check_dependencies
    virtual void check_dependencies() override;
    
protected:
    //! \copydoc Transform::apply_transform
    virtual void apply_transform() override;
    
    /*!
     * \brief Get the next task ready for duplication according to precendencies
     * 
     * Walk through the ready list to find the next task
     * Try to pick a replicate of the last handled task.
     * If no more replicate for that task is available then take the front one
     * 
     * \param lastbase last replicated task
     * \param added dictionary of replicate and corresponding base task
     * \return the next task to expand
     */
    virtual Task* get_next_ready(Task *lastbase, std::map<Task*, Task*> added);

    /*! \brief Simulate a scheduling step to determine how to expand a task
     * 
     * \param base Task to expand
     * \param rep the current replicate to append in the graph
     */
    virtual void simulate(Task *base, Task *rep);
    
    /*! \brief Connect two tasks
     * 
     * Append a regular connection/edge (1:1) to the graph between two tasks
     * 
     * \param basesrc Source task in the initial graph
     * \param src Source task of the new edge
     * \param basesink Sink task in the initial graph
     * \param outconn Connection from the initial source task
     * \param nbtok number of tokens exchange on the new edge
     */
    virtual void connect_regular(Task *basesrc, Task *src, Task *basesink, Connection *outconn, int nbtok);
    /*! \brief Connect two tasks
     * 
     * Append a join connection/edge (1:N) to the graph between two tasks
     * 
     * \param basesrc Source task in the initial graph
     * \param src Source task of the new edge
     * \param basesink Sink task in the initial graph
     * \param outconn Connector from the initial source task
     * \param nbtokprod number of tokens produced on the edge
     * \param nbtokcons number of tokens consumed on the edge
     */
    virtual void connect_join(Task *basesrc, Task *src, Task *basesink, Connection *conn, int nbtokprod, int nbtokcons);
    /*! \brief Connect two tasks
     * 
     * Append a split connection/edge (N:1) to the graph between two tasks
     * 
     * \param basesrc Source task in the initial graph
     * \param src Source task of the new edge
     * \param basesink Sink task in the initial graph
     * \poram baseconn Connection in the initial graph
     * \param nbtokprod number of tokens produced on the edge
     * \param nbtokcons number of tokens consumed on the edge
     */
    virtual void connect_split(Task *basesrc, Task *src, Task *basesink, Connection *baseconn, int nbtokprod, int nbtokcons);
    
    /*! \brief Get a replica of a task to use as a sink
     * 
     * \param basesink Sink in the initial graph
     * \param basesrc Source in the initial graph
     * \poram baseconn Connection in the initial graph
     * \param withdrawn_sinks list of replica sinks that already fulfilled their consumption
     * \return the sink to use
     */
    virtual Task * get_sink(Task *basesink, Task *basesrc, Connection *baseconn, std::list<Task*> &withdrawn_sinks);
    
    /*! \brief Get an available connector or create one
     * 
     * If a connector with the same name already exists for the replica as in the base then reuse it
     * either create a new one
     * 
     * \param t Replica task to sink the connector
     * \param basecon Connector from the initial graph
     * \param place {source;sink} to know if the connector is input or output
     * \return the connector to use
     */
    virtual Connector* get_connector(Task *t, Connector *basecon, const std::string& place);
    
    /*!
     * \brief Update the state of a sink task
     * 
     * Set the state of the sink in the sens that is it fully connected or does it need
     * more tokens from another source
     * 
     * If it does need more tokens, then put it as available in the front of available to use it in first
     * If it's complete, then put it as ready
     * 
     * \param base Task from the initial graph
     * \param rep Replica of the task
     */
    virtual void set_state_sink(Task *base, Task *rep);
};

/*!
 * \remark Register the QVectorFlatten class as a transformation with config id
 * "homogeneise"
 */ 
REGISTER_TRANSFORM(QVectorFlatten, "homogenize")

#endif
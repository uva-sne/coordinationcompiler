/*!
 * \file PropagateProperties.cpp
 * \copyright GNU Public License v3.
 * \date 2019 
 * \author Benjamin Rouxel <benjamin.rouxel@uva.nl>, University of Amsterdam
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "PropagateProperties.hpp"
#include "Utils/Log.hpp"
#include "Utils/Graph.hpp"

using namespace std;


PropagateProperties::PropagateProperties() : Transform() {}

IMPLEMENT_HELP(PropagateProperties, 
    Propagate timing properties from top-level to lower-level, from the
    application, to the node. Handled properties are deadline and period.\n
    \n
    Also propagate from the source node to the sink node of a graph.
)
void PropagateProperties::forward_params(const map<string,string>&) {}

const string PropagateProperties::get_uniqid_rtti() const { return "transform-propagate_properties"; }

//! \remark depends on #CycleCheck if there are DAGs with multiple periods
void PropagateProperties::check_dependencies() {
    vector<vector<Task*>> DAGS;
    Utils::extractGraphs(tg, &DAGS);
    for(vector<Task*> g : DAGS) {
        uint64_t period = 0;
        bool has_multiple_period = false;
        for(Task *t : g) {
            if(t->T() > 0) {
                if(period > 0 && period != t->T()) {
                    has_multiple_period = true;
                    break;
                }
                period = t->T();
            }
        }
        if(has_multiple_period)
            multi_period_dags.push_back(g);
        else if(period > 0)
            simple_period_dags.push_back(g);
        else {
            g.at(0)->D(tg->global_deadline());
            g.at(0)->T(tg->global_period());
            simple_period_dags.push_back(g);
        }
    }
    if(multi_period_dags.size()) {
        if(!conf->passes.find_pass_before("analyse-cycle", this)) {
            throw CompilerException("propagate-timing-infos", "Dependency unsatisfied, need to check for cycle first analyse-cycle");
        }
    }
}
void PropagateProperties::apply_transform() {
    propagate_multi_period_per_dag(multi_period_dags);
    propagate_timing_simple(simple_period_dags);
    propagate_deadline(multi_period_dags);
    propagate_deadline(simple_period_dags);
    
    Utils::DEBUG("periodicity:");
    for (Task *t : tg->tasks_it())
        Utils::DEBUG("\t"+t->id()+", "+to_string(t->T())+"ns, "+to_string(1/(t->T()/1000000000.0f))+"Hz");
    Utils::DEBUG("deadline:");
    for (Task *t : tg->tasks_it())
        Utils::DEBUG("\t"+t->id()+", "+to_string(t->D())+"ns, "+to_string(1/(t->D()/1000000000.0f))+"Hz");
    
    tg->compute_hyperperiod();
}

void PropagateProperties::propagate_timing_simple(vector<vector<Task*>> &DAGS) {
    /* FIXME: what the hell is this? If even a single task has a deadline/period, it will overwrite all other tasks */
    for(vector<Task*> g : DAGS) {
        uint64_t period = 0;
        uint64_t deadline = 0;
        for(Task *t : g) {
            if(t->T() > 0)
                period = t->T();
            if(t->D() > 0)
                deadline = t->D();
            if(period && deadline)
                break;
        }
        for(Task *t : g) {
            t->T(period);
            t->D(deadline);
        }
    }
}

void PropagateProperties::propagate_multi_period_per_dag(vector<vector<Task*>> &DAGS) {
    for(vector<Task*> g : DAGS) {
        for(Task *t : g) {
            if(t->predecessor_tasks().size() == 0)
                set_period_fwd(t);
        }
        for(Task *t : g) {
            if(t->successor_tasks().size() == 0)
                set_period_bwd(t);
        }
        for(Task *t : g) {
            if(t->T() == 0)
                throw CompilerException("propagate-period", "Can't find period for "+t->id());
        }
    }
}
// Warning: t->T() is in nanosecond
bool PropagateProperties::set_period_fwd(Task *t) {
    if(t->T() > 0)
        return true;
    float sumsibfreq = 0;
    for(Task::dep_t elp : t->predecessor_tasks()) {
        if(elp.first->T() == 0)
            return false;
        sumsibfreq += 1/(elp.first->T()/1000000000.0f);
    }
    for(Task::dep_t els : t->successor_tasks()) {
        if(els.first->T() == 0) {
            if(!set_period_fwd(els.first))
                return false;
        }
        sumsibfreq += 1/(els.first->T()/1000000000.0f);
    }
    
    t->T(round((1/sumsibfreq)*1000000000)); // round(sum of frequency of siblings)
    return true;
}

bool PropagateProperties::set_period_bwd(Task *t) {
    if(t->T() > 0)
        return true;
    float sumsibfreq = 0;
    for(Task::dep_t elp : t->predecessor_tasks()) {
        if(elp.first->T() == 0) {
            if(!set_period_bwd(elp.first))
                return false;
        }
        sumsibfreq += 1/(elp.first->T()/1000000000.0f);
    }
    for(Task::dep_t els : t->successor_tasks()) {
        if(els.first->T() == 0)
            return false;
        sumsibfreq += 1/(els.first->T()/1000000000.0f);
    }
    
    t->T(round((1/sumsibfreq)*1000000000)); // round(sum of frequency of siblings)
    return true;
}

/**
 * If no deadline is set to the task, then either it is largest period found in the
 * graph
 * larger than the period.
 * @param DAGS
 */
void PropagateProperties::propagate_deadline(vector<vector<Task*>> &DAGS) {
    for(vector<Task*> DAG : DAGS) {
        timinginfos_t deadline = 0;
        // get the largest deadline from element the root elements
        for(Task *t: DAG) {
            if(t->predecessor_tasks().empty() && t->D() > deadline)
                deadline = t->D();
        }
        if(deadline == 0) {
            // there was no deadline set, assuming the largest period of the roots in the graph is the deadline
            for(Task *t: DAG) {
                if(t->predecessor_tasks().empty() && deadline < t->T())
                    deadline = t->T();
            }
        }
        
        for(Task *t: DAG) {
            if(t->D() == 0)
                t->D(deadline);
        }
    }
}
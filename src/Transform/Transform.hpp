/*!
 * \file Transform.hpp
 * \copyright GNU Public License v3.
 * \date 2019 
 * \author Benjamin Rouxel <benjamin.rouxel@inria.fr>, INRIA/Irisa
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TRANSFORM_H
#define TRANSFORM_H

#include <algorithm>
#include <string>

#include "config.hpp"
#include "registry.hpp"
#include "SystemModel.hpp"
#include "Utils.hpp"

#ifndef NDEBUG
#include "Schedule.hpp"
#include "Exporter/Exporter.hpp"
#endif

#include "CompilerPass.hpp"

/*!
 * \brief Base class for transformations
 * 
 * Perform an transformation of the IR
 */
class Transform : public CompilerPass {
protected:
    //! IR of the current application
    SystemModel *tg;
    //! the global configuration
    const config_t *conf;
    
public:
    //! Constructor
    explicit Transform() {};
    //! \copydoc CompilerPass::run
    void run(SystemModel *m, config_t *c, Schedule *s) override {
        tg = m;
        conf = c;
        
        check_dependencies();
//#ifndef NDEBUG
//        std::string fn = conf->files.config_path.string().substr(0, conf->files.config_path.size() - 4);
//        fn += "_INIT_"+std::string(typeid(*this).name())+"_DEBUG.dot";
//        Exporter *g = ExporterRegistry::Create("dot");
//        g->run(tg, c, nullptr);
//#endif
        
        apply_transform();
        
//#ifndef NDEBUG
//        //Todo: switch to Boost path caused an error below - casting to string fixes it. not sure if below line is still correct.
//        fn = conf->files.config_path.string().substr(0, conf->files.config_path.size() - 4);
//        fn += "_"+std::string(typeid(*this).name())+"_DEBUG.dot";
//        g->run(tg, c, nullptr);
//        delete g;
//#endif
    }
    
protected:
    //! Apply the transformation
    virtual void apply_transform() = 0;
};

/*!
 * \typedef TransformRegistry
 * \brief Registry containing all transformations
 */
using TransformRegistry = registry::Registry<Transform, std::string>;

/*!
 * \brief Helper macro to register a new derived transformation
 */
#define REGISTER_TRANSFORM(ClassName, Identifier) \
  REGISTER_SUBCLASS(Transform, ClassName, std::string, Identifier)

#endif /* TRANSFORM_H */


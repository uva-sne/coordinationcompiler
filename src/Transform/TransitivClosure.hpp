/*!
 * \file TransitivClosure.hpp
 * \copyright GNU Public License v3.
 * \date 2019 
 * \author Benjamin Rouxel <benjamin.rouxel@uva.nl>, University of Amsterdam
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TRANSITIVECLOSURE_H
#define TRANSITIVECLOSURE_H

#include "Transform.hpp"

/*!
 * \brief Compute the transitive closure of a graph
 * 
 * \see TransitiveClosure::help
 * 
 * Populate fields Task#previous_transitiv and Task#successors_transitiv
 */
class TransitiveClosure : public Transform {
public:
    TransitiveClosure();
    //! \copydoc CompilerPass::get_uniqid_rtti
    const std::string get_uniqid_rtti() const override;
    void forward_params(const std::map<std::string,std::string>&) override;
    
    virtual std::string help() override;
    
protected:
    //! \copydoc Transform::apply_transform
    void apply_transform() override;
};

/*!
 * \remark Register the TransitiveClosure class as a transformation with config id
 * "transitive_closure"
 */ 
REGISTER_TRANSFORM(TransitiveClosure, "transitive_closure")

#endif
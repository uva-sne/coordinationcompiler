/*
 * Copyright (C) 2019 Benjamin Rouxel <benjamin.rouxel@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PRIORITYASSIGNMENT_H
#define PRIORITYASSIGNMENT_H

#include "config.hpp"
#include "SystemModel.hpp"
#include "Schedule.hpp"
#include "Utils.hpp"
#include "CompilerPass.hpp"
#include "Properties.hpp"

/*!
 * \brief Base class for priority assignment algorithm
 * 
 * Template parameter defines if the priority assignment is to be used off-line or on-line
 */
class PriorityAssignment : public CompilerPass {
public:
    /**
     * Constructor
     * @param e
     */
    explicit PriorityAssignment(ScheduleProperties::PriorityScheme_e e) : CompilerPass() {this->prio_scheme = e;};
    
    void run(SystemModel *m, config_t *c, Schedule *s) override {
        tg = m;
        conf = c;
        schedule = s;
        schedule->priority_mode(prio_scheme);
        
        check_dependencies();
        
        do_prioassign();
        
        std::list<SchedElt*> lelts;
        for (SchedEltPtr &elt: schedule->elements()) {
            lelts.push_back(elt.get());
        }
        update(&lelts);
    }
    
    /**
     * Update the priority on a list of elements
     * @param 
     */
    virtual void update(std::list<SchedElt*> *) = 0;
    
    /**
     * Return the priority scheme of the underlying algo
     * @return
     */
    ScheduleProperties::PriorityScheme_e scheme() const { return prio_scheme; }
    
protected:
    //! IR of the current application
    const SystemModel *tg;
    //! the global configuration
    const config_t *conf;
    //! current state of the schedule
    Schedule *schedule;
    
    ScheduleProperties::PriorityScheme_e prio_scheme; //!< Selected priority scheme
    
    //! Perform the priority assignment
    virtual void do_prioassign() = 0;
};

using PriorityAssignmentRegistry = registry::Registry<PriorityAssignment, std::string>;

#define REGISTER_PRIOASSIGNMENT(ClassName, Identifier) \
  REGISTER_SUBCLASS(PriorityAssignment, ClassName, std::string, Identifier)

#endif /* PRIORITYASSIGNMENT_H */


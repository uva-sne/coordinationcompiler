/*
 * Copyright (C) 2019 Benjamin Rouxel <benjamin.rouxel@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SHORTEST_EDF_HPP
#define SHORTEST_EDF_HPP

#include "PriorityAssignment.hpp"

/**
 * Perform a EDF priority assignment
 * 
 * \copydoc help
 * 
 */
class Shortest_EDF : public PriorityAssignment {
public:
    //! \brief Constructor
    Shortest_EDF() : PriorityAssignment(ScheduleProperties::PriorityScheme_e::PRIO_DYNAMIC) {}
    
    void forward_params(const std::map<std::string,std::string>&args) override;
    std::string help() override;
    const std::string get_uniqid_rtti() const override;
    virtual void check_dependencies() override;
    
    virtual void update(std::list<SchedElt*> *) override;
    
protected:
    virtual void do_prioassign() override;
};

REGISTER_PRIOASSIGNMENT(Shortest_EDF, "shortest_EDF")


#endif /* SHORTEST_EDF_HPP */


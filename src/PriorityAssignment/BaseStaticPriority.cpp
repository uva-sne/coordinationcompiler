/*
 * Copyright (C) 2019 Benjamin Rouxel <benjamin.rouxel@unimore.it>
 *                    University of Modena
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "BaseStaticPriority.hpp"

using namespace std;

void BaseStaticPriority::check_dependencies() {
    CompilerPass *pass = conf->passes.find_scheduler_or_solver_before(this);
    if(pass != nullptr && pass->get_uniqid_rtti().find("simulator") != string::npos)
        return;
    //else we don't have a scheduler afterward, or we have an off-line scheduler or a schedulability analysis
    // then we want to add the jobs to the schedule
    need_to_build_jobs = true;
}

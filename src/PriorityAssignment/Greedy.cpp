/*
 * Copyright (C) 2019 Benjamin Rouxel <benjamin.rouxel@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "Greedy.hpp"
#include "Utils/Log.hpp"
#include "Utils/Graph.hpp"
#include "ScheduleFactory.hpp"

using namespace std;

GreedyPriorityAssignment::GreedyPriorityAssignment() : BaseStaticPriority() {
}

GreedyPriorityAssignment::~GreedyPriorityAssignment() {
}

void GreedyPriorityAssignment::forward_params(const map<string,string>&) {}
IMPLEMENT_HELP(GreedyPriorityAssignment,
    Assign priority to task by following task input order
)

const string GreedyPriorityAssignment::get_uniqid_rtti() const { return "priority-assignment-greedy"; }

void GreedyPriorityAssignment::do_prioassign() {
    uint32_t current_prio = 0;
    vector<vector<Task*>> dags;
    for(vector<Task*> dag : dags) {
        Utils::BFS(&dag, [this, &current_prio](Task *t) {
            static_priorities[t] = current_prio++;
        });
    }
    
    for(Task *t : tg->tasks_it())
        Utils::DEBUG("Priority "+t->id()+": "+to_string(static_priorities[t]));
    
    if(need_to_build_jobs)
        ScheduleFactory::build_elements(schedule);
}

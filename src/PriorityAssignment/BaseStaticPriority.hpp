/*
 * Copyright (C) 2019 Benjamin Rouxel <benjamin.rouxel@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef BASESTATICPRIORITY_H
#define BASESTATICPRIORITY_H

#include "PriorityAssignment.hpp"

/**
 * Base class for static priority assignment
 * 
 * \copydoc help
 * 
 */
class BaseStaticPriority : public PriorityAssignment {
public:
    //! \brief Constructor
    BaseStaticPriority() : PriorityAssignment(ScheduleProperties::PriorityScheme_e::PRIO_FIXED) {}
    
    virtual void check_dependencies() override;
    
    void update(std::list<SchedElt*> *elts) override{
        for(SchedElt *elt : *elts) {
            const Task *t = elt->task();
            elt->schedtask()->priority(static_priorities.at(t));
        }
    }
    
protected:
    bool need_to_build_jobs = false;
    std::map<const Task*, int> static_priorities;
};

#endif
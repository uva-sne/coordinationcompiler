/*
 * Copyright (C) 2019 Benjamin Rouxel <benjamin.rouxel@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "RateMonotonic.hpp"
#include "Utils/Log.hpp"
#include "Utils/Graph.hpp"
#include "ScheduleFactory.hpp"

using namespace std;

RateMonotonic::RateMonotonic() : BaseStaticPriority() {
}
RateMonotonic::~RateMonotonic() {
}

void RateMonotonic::forward_params(const map<string,string>&) {}
const string RateMonotonic::get_uniqid_rtti() const { return "priority-assignment-ratemonotonic"; }
IMPLEMENT_HELP(RateMonotonic, 
    Assign priority to task by decreasing period -- Rate Monotonic RM\n\n
    Assign priority to task following a Rate Monotnonic algorithm\n
    This priority scheme assigns priority to tasks statically following the period
    of the task. Task with shortest period gets highest priority.\n
    \n
    Priority are increasing, highest priority gets the smallest value of 1.
)

void RateMonotonic::check_dependencies() {
    BaseStaticPriority::check_dependencies();
    if(!conf->passes.find_pass_before("transform-propagate_properties", this)) {
        throw CompilerException("propagate-timing-infos", "Dependency unsatisfied, need to propagate timing infos with transform-propagate_properties");
    }
}

void RateMonotonic::do_prioassign() {
    
    vector<Task*> tasks = tg->tasks_as_vector();
    sort(tasks.begin(), tasks.end(), [](Task *a, Task *b) {
        if(a->T() < b->T())
            return true;
        if(a->T() > b->T())
            return false;
        return a->C() > b->C();
    });
    int current_prio = 0;
    for(Task *t : tasks) {
        static_priorities[t] = ++current_prio;
//        Utils::DEBUG("Priority "+t->id()+": "+to_string(prio[t]));
    }
    
    if(need_to_build_jobs)
        ScheduleFactory::build_minimal_elements(schedule);
}

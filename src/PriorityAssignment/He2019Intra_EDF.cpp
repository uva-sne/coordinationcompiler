/*
 * Copyright (C) 2019 Benjamin Rouxel <benjamin.rouxel@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "He2019Intra_EDF.hpp"
#include "Utils/Log.hpp"
#include "Utils/Graph.hpp"

using namespace std;

void He2019Intra_EDF::forward_params(const map<string,string>&) {}

const string He2019Intra_EDF::get_uniqid_rtti() const { return "priority-assignment-gpuedf"; }

IMPLEMENT_HELP(He2019Intra_EDF, 
    Assign dynamic priority to job by earliest deadline -- Earliest Deadline First EDF\n\n
    Assign priority to jobs following a EDF algorithm\n
    This priority scheme assigns priority to tasks dynamically (simulator) following the deadline
    of the task. Task with closest deadline gets highest priority.\n
    \n
    Priority are increasing, highest priority gets the smallest value of 1.
)

void He2019Intra_EDF::check_dependencies() {
}

void He2019Intra_EDF::do_prioassign() {
    vector<vector<Task*>> dags;
    Utils::extractGraphs(tg, &dags);
    
    map<Task*, globtiminginfos_t> l, lf, lb;
    
    for(vector<Task*> dag : dags) {
        Utils::BFS(&dag, [&lf](Task *t) {
            timinginfos_t max = 0;
            for(Task::dep_t els : t->predecessor_tasks()) {
                if(max < lf[els.first]) 
                    max = lf[els.first];
            }
            lf[t] = t->C() + max;
        });
        Utils::rBFS(&dag, [&lb](Task *t) {
            timinginfos_t max = 0;
            for(Task::dep_t els : t->successor_tasks()) {
                if(max < lb[els.first]) 
                    max = lb[els.first];
            }
            lb[t] = t->C() + max;
        });
    }
    
    for(Task *t : tg->tasks_it())
        l[t] = lf[t] + lb[t] - t->C();
    
    assign_priority(l, lb, lf);
}


/*! \brief Walk through a graph in a BFS manner
 * 
 * \param Qready list of next to visit tasks
 * \param visited list of visited tasks
 */
void He2019Intra_EDF::doDFS(vector<Task*> Qready, uint32_t &current_prio, vector<Task*> &visited, const map<Task*, globtiminginfos_t> &l, const map<Task*, globtiminginfos_t> &lf, const map<Task*, globtiminginfos_t> &lb) {
    sort(Qready.begin(), Qready.end(), [l, lf, lb](Task *a, Task *b) {
        if(l.at(a) == l.at(b)){
            if(lb.at(a) == lb.at(b)) 
                return lf.at(a) > lf.at(b);
            else
                return lb.at(a) > lb.at(b);
        }
        return l.at(a) > l.at(b);
    });
    
    for(Task *current : Qready) {
        if(find(visited.begin(), visited.end(), current) != visited.end())
            continue;
        visited.push_back(current);

        // if any of the predecessor hasn't been visited, then we don't explore it
        if(any_of(current->predecessor_tasks().begin(), current->predecessor_tasks().end(), [visited](Task::dep_t elp) { return find(visited.begin(), visited.end(), elp.first) == visited.end();}))
            continue;

        prio[current] = current_prio++;

        doDFS(Utils::mapkeys(current->successor_tasks()), current_prio, visited, l, lf, lb);
    }
}

    
void He2019Intra_EDF::assign_priority(const map<Task*, globtiminginfos_t> &l, const map<Task*, globtiminginfos_t> &lf, const map<Task*, globtiminginfos_t> &lb) {
    vector<Task*> visited;
    vector<Task*> roots;
    for(Task *t : tg->tasks_it()) {
        if(t->predecessor_tasks().size() == 0)
            roots.push_back(t);
    }
    
    uint32_t prio = 0;
    doDFS(roots, prio, visited, l, lf, lb);
}

void He2019Intra_EDF::update(std::list<SchedElt*> *elts) {
    elts->sort([this](SchedElt *a, SchedElt *b) {
        // in case of period overlapping, like D > T, task from period n get
        //   a higher priority than tasks from period n+1
        if(a->min_rt_deadline() < b->min_rt_deadline())
            return true;
        if(a->min_rt_deadline() > b->min_rt_deadline())
            return false;

        return prio[a->schedtask()->task()] < prio[b->schedtask()->task()];
    });
    
    int current_prio = 0;
    for(SchedElt *e : *elts)
        e->schedtask()->priority(++current_prio);
    
//    for(SchedElt *e : *elts) 
//        Utils::DEBUG("Priority "+e->schedtask()->task()->id()+", job "+to_string(e->min_rt_period())+": "+to_string(e->schedtask()->priority()));
}

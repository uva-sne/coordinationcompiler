/*
 * Copyright (C) 2019 Benjamin Rouxel <benjamin.rouxel@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "Shortest_EDF.hpp"
#include "Utils/Log.hpp"
#include "Utils/Graph.hpp"

using namespace std;

void Shortest_EDF::forward_params(const map<string,string>&) {}

const string Shortest_EDF::get_uniqid_rtti() const { return "priority-assignment-shortestedf"; }

IMPLEMENT_HELP(Shortest_EDF, 
    Assign dynamic priority to job by earliest deadline -- Earliest Deadline First EDF\n\n
    Assign priority to jobs following a EDF algorithm\n
    This priority scheme assigns priority to tasks dynamically (simulator) following the deadline
    of the task. Task with closest deadline gets highest priority.\n
    \n
    Priority are increasing, highest priority gets the smallest value of 1.
)

void Shortest_EDF::check_dependencies() {
}

void Shortest_EDF::do_prioassign() {
}

void Shortest_EDF::update(std::list<SchedElt*> *elts) {
    elts->sort([this](SchedElt *a, SchedElt *b) {
        // in case of period overlapping, like D > T, task from period n get
        //   a higher priority than tasks from period n+1
        if(a->min_rt_deadline() < b->min_rt_deadline())
            return true;
        if(a->min_rt_deadline() > b->min_rt_deadline())
            return false;

        return a->wct() < b->wct();
    });
    
    int current_prio = 0;
    for(SchedElt *e : *elts)
        e->schedtask()->priority(++current_prio);
    
//    for(SchedElt *e : *elts) 
//        Utils::DEBUG("Priority "+e->schedtask()->task()->id()+", job "+to_string(e->min_rt_period())+": "+to_string(e->schedtask()->priority()));
}

/*
 * Copyright (C) 2019 Benjamin Rouxel <benjamin.rouxel@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "CritPath_EDF.hpp"
#include "Utils/Log.hpp"
#include "Utils/Graph.hpp"

using namespace std;

void CritPath_EDF::forward_params(const map<string,string>&) {}

const string CritPath_EDF::get_uniqid_rtti() const { return "priority-assignment-gpuedf"; }

IMPLEMENT_HELP(CritPath_EDF, 
    Assign dynamic priority to job by earliest deadline -- Earliest Deadline First EDF\n\n
    Assign priority to jobs following a EDF algorithm\n
    This priority scheme assigns priority to tasks dynamically (simulator) following the deadline
    of the task. Task with closest deadline gets highest priority.\n
    \n
    Priority are increasing, highest priority gets the smallest value of 1.
)

void CritPath_EDF::check_dependencies() {
    CompilerPass *pass = conf->passes.find_pass_before("analyse-critical-path", this);
    if(pass == nullptr)
        throw CompilerException("CritPath + EDF", "To run this priority assignment, the analyse critical-path must run first");
    critical_path_analysis = (CriticalPath*)pass;
}

void CritPath_EDF::do_prioassign() {
    vector<vector<Task*>> dags;
    Utils::extractGraphs(tg, &dags);
    
    for(Task *t : tg->tasks_it()) {
        weights[t] = 1;
    }
    
    vector<Task*> pat = critical_path_analysis->get_critical_path();
    map<Task*, std::pair<timinginfos_t, timinginfos_t>> earliest = critical_path_analysis->get_earliest();
    timinginfos_t cnt =  accumulate(pat.begin(), pat.end(), (timinginfos_t)0, [](timinginfos_t acc, Task* a) { return acc+a->C();});;
    
    //1st add weight to task on the critical path, early task gets the most, then we decrease
    sort(pat.begin(), pat.end(), [earliest](Task* a, Task *b) {
        return earliest.at(a) < earliest.at(b);
    });
    for(Task *t : pat) {
        weights[t] = cnt;
        cnt -= t->C();
    }
    //2nd compute the weight per task by summing up the WCET of the successors
    for(vector<Task*> dag : dags) {
        Utils::rBFS(&dag, [this](Task *t) {
            weights[t] += t->C();
            for(Task::dep_t els : t->successor_tasks()) {
                weights[t] += weights[els.first];
            }
        });
    }
    // The tasks with the more weights are the one generating the more load,
    //   and should have a higher priority
}

void CritPath_EDF::update(std::list<SchedElt*> *elts) {
    elts->sort([this](SchedElt *a, SchedElt *b) {
        // in case of period overlapping, like D > T, task from period n get
        //   a higher priority than tasks from period n+1
        if(a->min_rt_deadline() < b->min_rt_deadline())
            return true;
        if(a->min_rt_deadline() > b->min_rt_deadline())
            return false;

        return weights[a->schedtask()->task()] > weights[b->schedtask()->task()];
    });
    
    int current_prio = 0;
    for(SchedElt *e : *elts)
        e->schedtask()->priority(++current_prio);
    
//    for(SchedElt *e : *elts) 
//        Utils::DEBUG("Priority "+e->schedtask()->task()->id()+", job "+to_string(e->min_rt_period())+": "+to_string(e->schedtask()->priority()));
}

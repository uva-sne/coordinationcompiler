/*
 * Copyright (C) 2019 Benjamin Rouxel <benjamin.rouxel@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HE2019INTRA_EDF_HPP
#define HE2019INTRA_EDF_HPP

#include "PriorityAssignment.hpp"

/**
 * Perform a EDF priority assignment
 * 
 * 
@article{he2019intra,
  title={Intra-task priority assignment in real-time scheduling of dag tasks on multi-cores},
  author={He, Qingqiang and Guan, Nan and Guo, Zhishan and others},
  journal={IEEE Transactions on Parallel and Distributed Systems},
  volume={30},
  number={10},
  pages={2283--2295},
  year={2019},
  publisher={IEEE}
}
 * 
 * \copydoc help
 * 
 */
class He2019Intra_EDF : public PriorityAssignment {
public:
    //! \brief Constructor
    He2019Intra_EDF() : PriorityAssignment(ScheduleProperties::PriorityScheme_e::PRIO_DYNAMIC) {}
    
    void forward_params(const std::map<std::string,std::string>&args) override;
    std::string help() override;
    const std::string get_uniqid_rtti() const override;
    virtual void check_dependencies() override;
    
    virtual void update(std::list<SchedElt*> *) override;
    
protected:
    std::map<const Task*, uint32_t> prio;
    virtual void do_prioassign() override;
    
    void assign_priority(const std::map<Task*, globtiminginfos_t> &l, const std::map<Task*, globtiminginfos_t> &lf, const std::map<Task*, globtiminginfos_t> &lb);
    void doDFS(std::vector<Task*> Qready, uint32_t &current_prio, std::vector<Task*> &visited, const std::map<Task*, globtiminginfos_t> &l, const std::map<Task*, globtiminginfos_t> &lf, const std::map<Task*, globtiminginfos_t> &lb);
};

REGISTER_PRIOASSIGNMENT(He2019Intra_EDF, "he2019intra_EDF")


#endif /* HE2019INTRA_EDF_HPP */


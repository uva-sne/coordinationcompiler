/*!
 * \file config.cpp
 * \copyright GNU Public License v3.
 * \date 2017 
 * \author Benjamin Rouxel <benjamin.rouxel@inria.fr>, Irisa/INRIA
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "config.hpp"
#include "Utils.hpp"
#include "Generator/Runtime/RuntimeStrategyInterface.hpp"

using namespace std;

void init_config(config_t *conf) {
    conf->files.config_path = "";
    
    conf->archi.spm.assign_region = false;
    conf->archi.spm.prefetch_code = false;
    conf->archi.spm.store_code = false;
    
    conf->archi.interconnect.type = "NONE";
    conf->archi.interconnect.arbiter = "NONE";
    conf->archi.interconnect.active_ress_time = 4;
    conf->archi.interconnect.bit_per_timeunit = 8;
    conf->archi.interconnect.mechanism = "sharedonly";
    conf->archi.interconnect.behavior = "blocking";
    conf->archi.interconnect.burst = "none";
    
    conf->default_type = "bit";
    ADD_CONFIG_DATATYPES(conf, "void", 8, "", "void");
    ADD_CONFIG_DATATYPES(conf, "bit", 8, "0", "bit");
    ADD_CONFIG_DATATYPES(conf, "bool", 8, "false", "bool");
    ADD_CONFIG_DATATYPES(conf, "char", 8, "0", "char");
    ADD_CONFIG_DATATYPES(conf, "short", 16, "0", "short");
    ADD_CONFIG_DATATYPES(conf, "int", 32, "0", "int");
    ADD_CONFIG_DATATYPES(conf, "long", 64, "0", "long");
    ADD_CONFIG_DATATYPES(conf, "float", 32, "0", "float");
    ADD_CONFIG_DATATYPES(conf, "double", 64, "0", "double");
    ADD_CONFIG_DATATYPES(conf, "long double", 128, "0", "long double");
    
    conf->runtime.rtt = runtime_task_e::SINGLEPHASE;
    conf->runtime.tm = task_model_e::GRAPH;
    conf->runtime.gt = graph_type_e::SDF;
}

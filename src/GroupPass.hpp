/*
 * Copyright (C) 2023 Lukas Miedema <l.miedema@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <Utils.hpp>
#include <registry.hpp>
#include "Dispatcher.hpp"

/**
 * Compiler passes that nests a set of compiler passes can inherit from this as well. Such passes may run other
 * passes in a loop, set some environment and hten clean it up, or whatever.
 *
 * Inheriting from this must be public, otherwise "GroupAwarePass" will not work.
 */
class GroupPass : public CompilerPass {
protected:
    Dispatcher _dispatcher;

public:
    void initialize();
    ACCESSORS_R(GroupPass, Dispatcher &, dispatcher);
};

using GroupRegistry = registry::Registry<GroupPass, std::string>;

// must be registered with its id for config.xml parsing
#define REGISTER_GROUP(ClassName, Identifier) \
  REGISTER_SUBCLASS(GroupPass, ClassName, std::string, Identifier)

/**
 * Enable a compiler pass to be aware that it is being nested, and receive a reference to its parent. Use type T
 * to indicate which group passes should register themselves.
 */
template<typename T>
class GroupAwarePass;

template<>
class GroupAwarePass<GroupPass> {
public:
    virtual void setParent(GroupPass *parent) = 0; // to be called when parsing
};

template<typename T>
class GroupAwarePass: public GroupAwarePass<GroupPass> {
public:
    void setParent(GroupPass *parent) override {
        T *parentTyped = dynamic_cast<T *>(parent);
        if (parentTyped != nullptr)
            setParent(parentTyped);
    }

    virtual void setParent(T *parent) = 0;
};


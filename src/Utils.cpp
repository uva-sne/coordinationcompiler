/*!
 * \file Utils.hpp
 * \copyright GNU Public License v3.
 * \date 2017 
 * \author Benjamin Rouxel <benjamin.rouxel@inria.fr>, Irisa/INRIA
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "Utils.hpp"
#include "Utils/Log.hpp"

#include "SystemModel.hpp"
using namespace std;

namespace ba = boost::algorithm;

std::string to_string(uint128_t x) {
    std::string result = "";

    do {
        int rem = x % 10;
        result = to_string(rem) + result;
        x /= 10;
    } while (x != 0);

    return result;
}
std::string to_string(uint256_t x) {
    return x.convert_to<string>();
}

ostream& operator<< (ostream& os, const uint128_t x) {
    return os << to_string(x);
}
ostream& operator<< (ostream& os, const uint256_t x) {
    return os << to_string(x);
}

namespace Utils {
    // Leave std:: in arguments for the documentation
    string demangle(const std::string & str) {
        int status;
        string demangled = str;
        char *realname = abi::__cxa_demangle(str.c_str(), 0, 0, &status);
        if(status == 0) {
          demangled = realname;
          demangled = demangled.substr(0, demangled.find_first_of('('));
        }
        if(realname != NULL)
          free(realname);

        return demangled;
    }
    
    bool flipcoin() {
        int r = rand();
        return r < (RAND_MAX/2);
    }
    
    bool sort_Q_byComm(Task *a, Task *b) {
        uint32_t a_max = a->data_read() < a->data_written() ? a->data_written() : a->data_read();
        uint32_t b_max = b->data_read() < b->data_written() ? b->data_written() : b->data_read();
        return a_max < b_max;
    }
    bool sort_Q_byName(Task *a, Task *b) {
        return a->id() > b->id();
    }
    bool sort_Q_byMemUsageDec(Version *a, Version *b) {
        return a->memory_footprint() < b->memory_footprint();
    }
    bool sort_Q_byWCET(Task *a, Task *b) {
        if(a->C() == b->C())
            return a->id() < b->id();
        return a->C() > b->C();
    }
    bool sort_Q_byWCEE(Task *a, Task *b) {
        return a->C_E() > b->C_E();
    }
    bool sort_Q_byStatic_and_Dynamic(Task *a, Task *b) {
        return a->static_dynamic_energy() > b->static_dynamic_energy();
    }

    
    uint64_t stringmem_to_bit(const string &str) {
        if(str.empty())
            return 0;
        uint64_t val = stoull(str);
        char unit, prefix;
        if (str.size() >= 2) {
            prefix = tolower(str.at(str.size() - 2));
            if (prefix == 'k' || prefix == 'K')
                val *= 1024;
            else if (prefix == 'm' || prefix == 'M')
                val *= 1024 * 1024;
            else if (prefix == 'g' || prefix == 'G')
                val *= 1024 * 1024 * 1024;
            else if (prefix != ' ' && !(prefix >= '0' && prefix <= '9'))
                throw CompilerException("input", "bad unit prefix '" + to_string(prefix) + "' unit for size in string \"" + str + "\"");
        }
        unit = str.at(str.size()-1);
        if(unit == 'o' || unit == 'B')
            val *= 8;
        else if(unit != 'b' && !(unit >= '0' && unit <= '9'))
            throw CompilerException("input", "unrecognized data unit '" + to_string(unit) + "' in string \"" + str + "\"");
        return val;
    }
    
    string bit_to_stringmem(uint64_t val) {
        
        int gb=0;
        while((val-gb*(1024*1024*1024)) / (1024*1024*1024) > 0) gb++;
        if(gb) {
            int flt = (val-gb*(1024*1024*1024)) % (1024*1024*1024);
            return to_string(gb)+(flt ? ","+to_string(flt) : "")+"Gb";
        }
        
        int mb=0;
        while((val-mb*(1024*1024)) / (1024*1024) > 0) mb++;
        if(mb) {
            int flt = (val-mb*(1024*1024)) % (1024*1024);
            return to_string(mb)+(flt ? ","+to_string(flt) : "")+"Mb";
        }
        
        int kb=0;
        while((val-kb*1024) / 1024 > 0) kb++;
        if(kb) {
            int flt = (val-kb*1024) % 1024;
            return to_string(kb)+(flt ? ","+to_string(flt) : "")+"kb";
        }
        
        return to_string(val)+" b";
    }
    
    void memory_usage(const std::string &msg) {
        if(!msg.empty())
            Utils::DEBUG(msg);
        
        static int64_t prev_used = 0;
        static int64_t prev_used_swap = 0;
        
        uint64_t used_ram = Utils::get_ram_usage();
        uint64_t used_swap = Utils::get_swap_usage();
        uint64_t tot_ram = Utils::get_total_ram();
        uint64_t tot_swap = Utils::get_total_swap();
        
        Utils::DEBUG("total ram: "+to_string(tot_ram)+" -- used memory by proc: "+to_string(used_ram)+" -- freed/prod since last check : "+to_string(used_ram - prev_used));
        Utils::DEBUG("total swap: "+to_string(tot_swap)+" -- used swap by proc: "+to_string(used_swap)+" -- freed/prod since last check : "+to_string(used_swap - prev_used_swap));
        
        prev_used = used_ram ;
        prev_used_swap = used_swap ;
    }
    
    uint64_t get_ram_usage() {
#ifdef __linux__
        ifstream file("/proc/self/status");
        string key, value;
        while(!file.eof()) {
            file >> key >> value ;
            if(key.find("VmRSS") != string::npos) {
                file.close();
                return stoull(value);
            }
        }
        file.close();
#endif   
#ifdef __APPLE__
        struct task_basic_info t_info;
        mach_msg_type_number_t t_info_count = TASK_BASIC_INFO_COUNT;
        // resident size is in t_info.resident_size;
        // virtual size is in t_info.virtual_size;
        if (KERN_SUCCESS == task_info(mach_task_self(),
                                      TASK_BASIC_INFO, (task_info_t)&t_info, 
                                      &t_info_count))
        {
            return t_info.resident_size;
        }
#endif
        return 0;
    }
    
    uint64_t get_swap_usage() {
#ifdef __linux__
        ifstream file("/proc/self/status");
        string key, value;
        while(!file.eof()) {
            file >> key >> value ;
            if(key.find("VmSwap") != string::npos) {
                file.close();
                return stoull(value);
            }
        }
#endif   
#ifdef __APPLE__
       // no preallocated swap, when prog start consuming too much memory, process is killed, not swapped
#endif
        return 0;
    }
    
    uint64_t get_total_ram() {
#ifdef __linux__
        struct sysinfo info;
        sysinfo(&info);
        return info.totalram;
#else
#ifdef __APPLE__
        int mib[2] = {CTL_HW, HW_MEMSIZE};
        uint64_t physical_memory;
        size_t length = sizeof(uint64_t);
        sysctl(mib, 2, &physical_memory, &length, NULL, 0);
        return physical_memory;
#else
        return 0;
#endif
#endif
        
    }
    uint64_t get_total_swap() {
#ifdef __linux__
        struct sysinfo info;
        sysinfo(&info);
        return info.totalswap;
#else
#ifdef __APPLE__
       // no preallocated swap, when prog start consuming too much memory, process is killed, not swapped
#endif
        return 0;
#endif
    }


//----------------------------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------

    int256_t nano_to_milli(int256_t nano) {
        return (nano/1000000);
    }

    int256_t milli_to_nano(int256_t milli) {
        return (milli * 1000000);
    }
    
    uint256_t nano_to_milli(uint256_t nano) {
        return (nano/1000000);
    }

    uint256_t milli_to_nano(uint256_t milli) {
        return (milli * 1000000);
    }

    uint128_t nano_to_milli(uint128_t  nano) {
        return (nano/1000000);
    }

    uint128_t  milli_to_nano(uint128_t  milli) {
        return (milli * 1000000);
    }

//----------------------------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------
    energycons_t get_energy_units_per_power_time_unit(const string time_unit, const string power_unit, const string energy_unit) {
        if (time_unit == "ms" && power_unit == "mW" && energy_unit == "mJ") {
            return 1000;
        } else if (time_unit == "s" && power_unit == "W" && energy_unit == "J") {
            return 1;
        } else if (time_unit == "us" && power_unit == "uW" && energy_unit == "uJ") {
            return 1000000;
        } else if (time_unit == "ds" && power_unit == "mW" && energy_unit == "mJ") {
            return 10;
        } else if (time_unit == "ns" && power_unit == "W" && energy_unit == "J") {
            //really not advised for accurate energy....
            return 1000000000;
        } else {
            throw CompilerException("utils", "Cannot compute energy properly as the time and power units dont match any of the predetermined cases.");
        }
    }

    timinginfos_t stringtime_to_time(const std::string &stime, const std::string &time_unit){
        if (time_unit == "ns")
            return stringtime_to_ns(stime);
        if (time_unit == "us")
            return stringtime_to_us(stime);
        if (time_unit == "ms")
            return stringtime_to_ms(stime);
        if (time_unit == "ds")
            return stringtime_to_ds(stime);
        if (time_unit == "s")
            return stringtime_to_s(stime);
        if (time_unit == "m")
            return stringtime_to_m(stime);
        if (time_unit == "h")
            return stringtime_to_h(stime);

        throw CompilerException("Utils::stringtime_to_time", "Cannot convert time to system model unit 'other' (input: "+stime+" "+ time_unit +")");
    }

    timinginfos_t stringtime_to_us(const std::string &stime){
        return ceil((double) stringtime_to_ns(stime)/1000);
    }

    timinginfos_t stringtime_to_ms(const std::string &stime){
        return ceil((double) stringtime_to_ns(stime)/1000000);
    }

    timinginfos_t stringtime_to_ds(const std::string &stime){
        return ceil((double) stringtime_to_ns(stime)/100000000.0);
    }

    timinginfos_t stringtime_to_s(const std::string &stime){
        return ceil((double)stringtime_to_ns(stime)/1000000000);
    }

    timinginfos_t stringtime_to_m(const std::string &stime){
        return ceil((double)stringtime_to_ns(stime)/1000000000/60);
    }

    timinginfos_t stringtime_to_h(const std::string &stime){
        return ceil((double) stringtime_to_ns(stime)/1000000000/3600);
    }

    timinginfos_t stringtime_to_ns(const std::string &stime) {
        //TODO: check if these ns calculations are still correct!
        if(ba::find_last(stime, "Hz"))
            return (1/stod(stime))*1000000000;
        if(ba::find_last(stime, "mHz"))
            return (1/stod(stime))*1000000000000;
        if(ba::find_last(stime, "cycles"))
            return stod(stime);
        if(ba::find_last(stime, "ns"))
            return stod(stime);
        if(ba::find_last(stime, "us"))
            return stod(stime)*1000;
        if(ba::find_last(stime, "ms"))
            return stod(stime)*1000000;
        if(ba::find_last(stime, "ds"))
            return stod(stime)*100000000;
        if(ba::find_last(stime, "s"))
            return stod(stime)*1000000000;
        if(ba::find_last(stime, "m"))
            return stod(stime)*60000000000;
        if(ba::find_last(stime, "h"))
            return stod(stime)*3600000000000;

        return stoul(stime);
    }

    std::string nstime_to_string(timinginfos_t time,  const string &unit) {
        ostringstream out;
        out.precision(3);
        if(unit == "Hz")
            out << std::fixed << (1000000000/(time*1.0));
        else if(unit == "mHz")
            out << std::fixed << (1000000000000/(time*1.0));
        else if(unit == "cycles")
            out << std::fixed << time;
        else if(unit == "ns")
            out << std::fixed << time;
        else if(unit == "us")
            out << std::fixed << (time/1000.0);
        else if(unit == "ms")
            out << std::fixed << (time/1000000.0);
        else if(unit == "s")
            out << std::fixed << (time/1000000000.0);
        else if(unit == "m")
            out << std::fixed << (time/60000000000.0);
        else if(unit == "h")
            out << std::fixed << (time/3600000000000.0);
        else 
            out << std::fixed << time;
        return out.str();
    }

    std::string time_to_stringtime(uint128_t time, const std::string &system_unit, const std::string &target_unit) {
        if(system_unit == target_unit)
            return to_string(time);
        else
            Utils::WARN("Utils::time_to_stringtime -- does not yet support converting time to strings if the target unit differs from the system unit");
            //TODO: see above
//        if(unit == "Hz")
//            return to_string(1000000000/time);
//        if(unit == "mHz")
//            return to_string(1000000000000/time);
//        if(unit == "cycles")
//            return to_string(time);
//        if(unit == "ns")
//            return to_string(time);
//        if(unit == "us")
//            return to_string(time/1000.0);
//        if(unit == "ms")
//            return to_string(time/1000000.0);
//        if(unit == "s")
//            return to_string(time/1000000000.0);
//        if(unit == "m")
//            return to_string(time/60000000000.0);
//        if(unit == "h")
//            return to_string(time/3600000000000.0);

        Utils::WARN("Utils::time_to_stringtime -- dunno how to convert time with unit 'other'");
        return "";
    }

//----------------------------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------
// Hz to kHz
    frequencyinfo_t Hz_to_kHz (frequencyinfo_t Hz){
        return (Hz/1000);
    }

    frequencyinfo_t stringfrequency_to_frequency(const std::string &sfrequency, const std::string &frequency_unit){
        if (frequency_unit == "GHz")
            return stringfrequency_to_gHz(sfrequency);
        if (frequency_unit == "mHz")
            return stringfrequency_to_mHz(sfrequency);
        if (frequency_unit == "kHz")
            return stringfrequency_to_kHz(sfrequency);
        if (frequency_unit == "Hz")
            return stringfrequency_to_Hz(sfrequency);

        Utils::WARN("Utils::stringfrequency_to_frequency -- dunno how to convert frequency to system model unit 'other' (input: "+sfrequency+" "+ frequency_unit +")");
        return 0;
    }

    frequencyinfo_t stringfrequency_to_kHz(const std::string &sfrequency) {
        return ceil((double) stringfrequency_to_Hz(sfrequency)/1000);
    }
    frequencyinfo_t stringfrequency_to_mHz(const std::string &sfrequency) {
        return ceil((double)stringfrequency_to_Hz(sfrequency)/1000000);
    }
    frequencyinfo_t stringfrequency_to_gHz(const std::string &sfrequency) {
        return ceil((double) stringfrequency_to_Hz(sfrequency)/1000000000);
    }

    frequencyinfo_t stringfrequency_to_Hz(const std::string &sfrequency) {
        if(ba::find_last(sfrequency, "kHz"))
            return (stold(sfrequency)*1000);
        if(ba::find_last(sfrequency, "mHz"))
            return (stold(sfrequency)*1000000);
        if(ba::find_last(sfrequency, "gHz"))
            return (stold(sfrequency)*1000000000);
        if(ba::find_last(sfrequency, "Hz"))
            return (stold(sfrequency));

        Utils::WARN("Utils::stringfrequency_to_Hz -- dunno how to convert frequency with unit 'other' (input: "+sfrequency+")");
        return 0;
    }

    std::string frequency_to_stringfrequency(uint128_t frequency, const std::string &unit){
        if(unit == "Hz")
            return to_string(frequency);
        if(unit == "mHz")
            return to_string(frequency/1000000);
        if(unit == "gHz")
            return to_string(frequency/1000000000);

        Utils::WARN("Utils::frequency_to_stringfrequency -- dunno how to convert frequency with unit 'other'");
        return "";
    }

//----------------------------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------
    energycons_t stringenergy_to_energy(const std::string &sener, const std::string &energy_unit){
        if (energy_unit == "nJ")
            return stringenergy_to_njoule(sener);
        if (energy_unit == "uJ")
            return stringenergy_to_ujoule(sener);
        if (energy_unit == "mJ")
            return stringenergy_to_mjoule(sener);
        if (energy_unit == "J")
            return stringenergy_to_joule(sener);

        Utils::WARN("Utils::stringenergy_to_energy -- dunno how to convert time to system model unit 'other' (input: "+sener+" "+ energy_unit +")");
        return 0;
    }

    energycons_t stringenergy_to_joule(const std::string &sener){
        return ceil((double) stringenergy_to_njoule(sener)/1000000000);
    }

    energycons_t stringenergy_to_mjoule(const std::string &sener){
        return ceil((double) stringenergy_to_njoule(sener)/1000000);
    }
    
    energycons_t stringenergy_to_ujoule(const std::string &sener){
        return ceil((double) stringenergy_to_njoule(sener)/1000);
    }
    //nano joule
    energycons_t stringenergy_to_njoule(const std::string &sener) {
        if(ba::find_last(sener, "kWh"))
            return (energycons_t) (stold(sener)*3600000*1000000000);
        if(ba::find_last(sener, "Wh"))
            return (energycons_t) (stold(sener)*3600*1000000000);
        if(ba::find_last(sener, "kJ"))
            return (energycons_t) (stold(sener)*1000000000000);
        if(ba::find_last(sener, "nJ"))
            return (energycons_t) (stold(sener));
        if(ba::find_last(sener, "mJ"))
            return (energycons_t) (stold(sener)*1000000);
        if(ba::find_last(sener, "J"))
            return (energycons_t) (stold(sener) * 1000000000);

        Utils::WARN("Utils::stringenergy_to_jns_stroule -- dunno how to convert energy with unit 'other' (input: "+sener+")");
        return -1;
    }

    string njoule_to_stringenergy(energycons_t e, const string &unit) {
        if(unit == "J")
            return to_string(e/1000000000);
        if(unit == "kJ")
            return to_string(e/1000000000000);
        if(unit == "Wh")
            return to_string((e/1000000000)/3600);
        if(unit == "kWh")
            return to_string((e/1000000000)/3600000);

        Utils::WARN("Utils::joule_to_stringenergy -- dunno how to convert energy");
        return "";
    }


//----------------------------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------
    powerinfo_t stringwatt_to_power(const std::string &swatt, const std::string &power_unit){
        if (power_unit == "nW")
            return stringwatt_to_nwatt(swatt);
        if (power_unit == "uW")
            return stringwatt_to_uwatt(swatt);
        if (power_unit == "mW")
            return stringwatt_to_mwatt(swatt);
        if (power_unit == "W")
            return stringwatt_to_watt(swatt);

        Utils::WARN("Utils::stringenergy_to_energy -- dunno how to convert time to system model unit 'other' (input: "+swatt+" "+ power_unit +")");
        return 0;
    }

    powerinfo_t stringwatt_to_watt(const std::string &swatt) {
        return ceil((double) stringwatt_to_nwatt(swatt)/1000000000.0f);
    }

    powerinfo_t stringwatt_to_mwatt(const std::string &swatt) {
        return ceil((double) stringwatt_to_nwatt(swatt) / 1000000.0f);
    }

    powerinfo_t stringwatt_to_uwatt(const std::string &swatt) {
        return ceil((double) stringwatt_to_nwatt(swatt)/1000.0f);
    }

    powerinfo_t stringwatt_to_nwatt(const std::string &swatt) {
        //internal representation is nano watt
        if(ba::find_last(swatt, "kW"))
            return (stold(swatt))*1000000000000;
        if(ba::find_last(swatt, "W"))
            return (stold(swatt))*1000000000;
        if(ba::find_last(swatt, "mW"))
            return (stold(swatt))*1000000;
        if(ba::find_last(swatt, "uW")) //micro Watt - ISO small u
            return (stold(swatt))*1000;
        if(ba::find_last(swatt, "µW")) //micro Watt - legacy symbol
            return (stold(swatt))*1000;
        if(ba::find_last(swatt, "μW")) //micro Watt - greek letter
            return (stold(swatt))*1000;
        if(ba::find_last(swatt, "mcW")) //micro Watt - mc prefix
            return (stold(swatt))*1000;

        Utils::WARN("Utils::stringwatt_to_watt -- dunno how to convert watt with unit 'other' (input: "+swatt+")");
        return 0;
    }

    std::string nwatt_to_stringwatt(powerinfo_t watt, const std::string &unit){
        if(unit == "kW")
            return to_string(watt/1000000000000);
        if(unit == "W")
            return to_string(watt/1000000000);
        if(unit == "mW")
            return to_string(watt/1000000);
        if(unit == "μW")
            return to_string(watt/1000);

        Utils::WARN("Utils::watt_to_stringwatt -- dunno how to convert watt with unit 'other'");
        return "";
    }
}


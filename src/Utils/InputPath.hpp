/*
 * Copyright (C) 2020 Lukas Miedema <l.miedema@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CECILE_INPUTPATH_HPP
#define CECILE_INPUTPATH_HPP

#include <boost/filesystem/path.hpp>
#include <utility>
#include <iosfwd>
#include <iostream>

/**
 * @brief Wrapper around a boost::filesystem::path.
 *
 * The path >> operator splits on spaces, which prevents reading paths
 * from the command line containing spaces. This wrapper is a workaround for that.
 */
class InputPath {
private:
    boost::filesystem::path wrapped_path; //!< Contain the actual path
public:

    /**
     * Replace the current path with the given path.
     * @param path
     */
    inline void replace(boost::filesystem::path path) {
        wrapped_path = std::move(path);
    }

    /**
     * Access a member of the wrapped path.
     * @return pointer to the wrapped path.
     */
    const boost::filesystem::path *operator->() const {
        return &wrapped_path;
    }

    /**
     * Access a member of the wrapped path.
     * @return pointer to the wrapped path.
     */
    inline boost::filesystem::path *operator->() {
        return &wrapped_path;
    }

    /**
     * Access the wrapped path.
     * @return the wrapped path.
     */
    inline boost::filesystem::path operator*() {
        return wrapped_path;
    }

    /**
     * Access the wrapped path.
     * @return the wrapped path.
     */
    inline boost::filesystem::path operator*() const {
        return wrapped_path;
    }
};

/**
 * Set the given string as wrapped path. This operation differs from the implementation of boost::filesystem::path
 * in that it does not stop on whitespace.
 * @param is
 * @param existing
 * @return the rest of the stream (should be EOF).
 */
std::basic_istream<char>& operator>>(std::basic_istream<char>& is, InputPath& existing);

#endif //CECILE_INPUTPATH_HPP

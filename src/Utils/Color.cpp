/*
 * Copyright (C) 2020 brouxel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* 
 * File:   Color.cpp
 * Author: brouxel
 * 
 * Created on 9 septembre 2020, 10:16
 */

#include "Color.hpp"
#include <iostream>
using namespace std;

namespace Utils {
    string color_to_html(const color_t &c) {
        ostringstream out;
        out << "#" << hex << setfill('0') << setw(2) << (uint32_t)c.r;
        out << hex << setfill('0') << setw(2) << (uint32_t)c.g;
        out << hex << setfill('0') << setw(2) << (uint32_t)c.b;
        out << hex << setfill('0') << setw(2) << (uint32_t)c.a;
        return out.str();
    }
}
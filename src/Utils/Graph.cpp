/*!
 * \file Graph.cpp
 * \copyright GNU Public License v3.
 * \date 2020 
 * \author Benjamin Rouxel <benjamin.rouxel@uva.nl>, University of Amsterdam
 * \author Stephen Nicholas Swatman <s.n.swatman@uva.nl>, University of Amsterdam
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <set>

#include "Graph.hpp"

using namespace std;

namespace Utils {
    /*!\brief Helper function for BFS
     * 
     * Auxiliary function for the BFS algorithm
     * 
     * \param visited list of visited tasks
     * \param current the current task to visit
     * \param Qready list of next to visit tasks
     */
    static void doBFS_aux(vector<Task*> &visited, Task *current, vector<Task*> *Qready, std::function<void(Task*)> func) {
        vector<Task*> Qtmp;
        
        if(find(visited.begin(), visited.end(), current) != visited.end())
            return;
        visited.push_back(current);
        
        for(Task::dep_t els : current->successor_tasks()) {
            Task *s = els.first;
            if(find(visited.begin(), visited.end(), s) != visited.end())
                continue;
            bool ok = true;
            for(Task::dep_t elp : s->predecessor_tasks()) {
                if(find(visited.begin(), visited.end(), elp.first) == visited.end()) {
                    ok = false;
                    break;
                }
            }
            if(ok)
                Qtmp.push_back(s);
        }
        sort(Qtmp.begin(), Qtmp.end(), Utils::sort_Q_byWCET);
        Qready->insert(Qready->end(), Qtmp.begin(), Qtmp.end());
        
        func(current);
    }

    /*! \brief Walk through a graph in a BFS manner
     * 
     * \param Qready list of next to visit tasks
     * \param visited list of visited tasks
     */
    static void doBFS(vector<Task*> *Qready, vector<Task*> &visited, std::function<void(Task*)> func) {
        vector<Task*> nextQready;
        for(Task *root : *Qready) {
            doBFS_aux(visited, root, &nextQready, func);
        }
        if(nextQready.empty()) {
            return;
        }

        doBFS(&nextQready, visited, func);
        Qready->insert(Qready->end(), nextQready.begin(), nextQready.end());
    }
    
    void BFS(std::vector<Task*> *graph, std::function<void(Task*)> func) {
        vector<Task*> roots;
        for(Task *t : *graph) {
            if(t->predecessor_tasks().size() == 0)
                roots.push_back(t);
        }
        vector<Task*> visited;
        doBFS(&roots, visited, func);
    }

    /*!\brief Helper function for BFS
     * 
     * Auxiliary function for the BFS algorithm
     * 
     * \param visited list of visited tasks
     * \param current the current task to visit
     * \param Qready list of next to visit tasks
     */
    static void dorBFS_aux(vector<Task*> &visited, Task *current, vector<Task*> *Qready, std::function<void(Task*)> func) {
        vector<Task*> Qtmp;
        
        if(find(visited.begin(), visited.end(), current) != visited.end())
            return;
        visited.push_back(current);
        
        
        for(Task::dep_t elp : current->predecessor_tasks()) {
            Task *p = elp.first;
            if(find(visited.begin(), visited.end(), p) != visited.end())
                continue;
            bool ok = true;
            for(Task::dep_t els : p->successor_tasks()) {
                if(find(visited.begin(), visited.end(), els.first) == visited.end()) {
                    ok = false;
                    break;
                }
            }
            if(ok)
                Qtmp.push_back(p);
        }
        sort(Qtmp.begin(), Qtmp.end(), Utils::sort_Q_byWCET);
        Qready->insert(Qready->end(), Qtmp.begin(), Qtmp.end());
        
        func(current);
    }

    /*! \brief Walk through a graph in a BFS manner
     * 
     * \param Qready list of next to visit tasks
     * \param visited list of visited tasks
     */
    static void dorBFS(vector<Task*> *Qready, vector<Task*> &visited, std::function<void(Task*)> func) {
        vector<Task*> nextQready;
        for(Task *root : *Qready) {
            dorBFS_aux(visited, root, &nextQready, func);
        }
        if(nextQready.empty()) {
            return;
        }

        dorBFS(&nextQready, visited, func);
        Qready->insert(Qready->end(), nextQready.begin(), nextQready.end());
    }
    
    void rBFS(std::vector<Task*> *graph, std::function<void(Task*)> func) {
        vector<Task*> sinks;
        for(Task *t : *graph) {
            if(t->successor_tasks().size() == 0)
                sinks.push_back(t);
        }
        vector<Task*> visited;
        dorBFS(&sinks, visited, func);
    }
    
    /*! \brief Walk through a graph in a BFS manner
     * 
     * \param Qready list of next to visit tasks
     * \param visited list of visited tasks
     */
    static void doDFS(Task* current, vector<Task*> &visited, std::function<void(Task*)> func) {
        if(find(visited.begin(), visited.end(), current) != visited.end())
            return;
        visited.push_back(current);

        // if any of the predecessor hasn't been visited, then we don't explore it
        if(any_of(current->predecessor_tasks().begin(), current->predecessor_tasks().end(), [visited](Task::dep_t elp) { return find(visited.begin(), visited.end(), elp.first) == visited.end();}))
            return;

        func(current);

        for(Task::dep_t els : current->successor_tasks())
            doDFS(els.first, visited, func);
    }
    
    void DFS(std::vector<Task*> *graph, std::function<void(Task*)> func) {
        vector<Task*> visited;
        for(Task *t : *graph) {
            if(t->predecessor_tasks().size() == 0)
                doDFS(t, visited, func);
        }
    }
    
    /*! \brief Walk through a graph in a BFS manner
     * 
     * \param Qready list of next to visit tasks
     * \param visited list of visited tasks
     */
    static void dorDFS(Task* current, vector<Task*> &visited, std::function<void(Task*)> func) {
        if(find(visited.begin(), visited.end(), current) != visited.end())
            return;
        visited.push_back(current);

        // if any of the predecessor hasn't been visited, then we don't explore it
        if(any_of(current->successor_tasks().begin(), current->successor_tasks().end(), [visited](Task::dep_t els) { return find(visited.begin(), visited.end(), els.first) == visited.end();}))
            return;

        func(current);

        for(Task::dep_t els : current->predecessor_tasks())
            dorDFS(els.first, visited, func);
    }
    
    void rDFS(std::vector<Task*> *graph, std::function<void(Task*)> func) {
        vector<Task*> visited;
        for(Task *t : *graph) {
            if(t->successor_tasks().size() == 0)
                dorDFS(t, visited, func);
        }
    }

    // leave std:: in function arguments for doxygen
    void extractGraphs(const SystemModel *tg, std::vector<std::vector<Task*>> *DAGS) {
        vector<Task*> roots;
        for (Task *t : tg->tasks_it()) {
            if(t->predecessor_tasks().empty())
                roots.push_back(t);
        }
        vector<Task*> visited;
        doBFS(&roots, visited, [&DAGS](Task *a){
            bool found = false;
            vector<vector<Task*>>::iterator prevfound;
            for(Task::dep_t elp : a->predecessor_tasks()) {
                auto it = find_if(DAGS->begin(), DAGS->end(), [elp](const vector<Task*> &dag){return find(dag.begin(), dag.end(), elp.first) != dag.end();});
                if(it != DAGS->end()) {
                    if(found) {
                        if(*prevfound != *it) {
                            //need to merge
                            for(Task *t : *prevfound)
                                it->push_back(t);
                            DAGS->erase(prevfound);
                        }
                    }
                    else {
                        found = true;
                        if(find(it->begin(), it->end(), a) == it->end()) {
                            it->push_back(a);
                        }
                    }
                    // need to launch find again as `it' might have been invalidated by the erase
                    prevfound = find_if(DAGS->begin(), DAGS->end(), [elp](const vector<Task*> &dag){return find(dag.begin(), dag.end(), elp.first) != dag.end();});
                }
            }
            if(!found) {
                vector<Task*> g = {a};
                DAGS->push_back(g);
            }
        });
    }

    // leave std:: in function arguments for doxygen
    bool isTopologicalOrder(const std::vector<Task *> & tasks) {
        set<Task *> seen;

        for (Task * t : tasks) {
            for (Task::dep_t p : t->predecessor_tasks()) {
                if (seen.find(p.first) == seen.end()) {
                    return false;
                }
            }

            seen.insert(t);
        }

        return true;
    }

    bool isReversedTopologicalOrder(const std::vector<Task *> & tasks) {
        std::set<Task *> seen;

        for (Task * t : tasks) {
            for (Task * p : Utils::mapkeys(t->successor_tasks())) {
                if (seen.find(p) == seen.end()) {
                    return false;
                }
            }

            seen.insert(t);
        }

        return true;
    }

    bool isReversedTopologicalOrder(const std::vector<SchedElt *> & tasks) {
        std::set<SchedElt *> seen;

        for (SchedElt * t : tasks) {
            for (SchedElt * p : t->successors()) {
                if (seen.find(p) == seen.end()) {
                    return false;
                }
            }

            seen.insert(t);
        }

        return true;
    }
}

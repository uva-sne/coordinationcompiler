/*!
 * \file Log.cpp
 * \copyright GNU Public License v3.
 * \date 2020 
 * \author Stephen Nicholas Swatman <s.n.swatman@uva.nl>, University of Amsterdam
 * \author Benjamin Rouxel <benjamin.rouxel@uva.nl>, University of Amsterdam
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <iostream>
#include <string>
#include <map>
#include <functional>

#include "Log.hpp"

using namespace std;

namespace Utils {
    //! Global set level
    static enum Level level = Level::INFO;

    /*! \brief Convert a #Utils::Level to a string
     * \param l
     * \return string
     */
    static const string levelToString(enum Level l) {
        if (l == Level::DEBUG) {
            return "DEBUG";
        } else if (l == Level::INFO) {
            return "INFO";
        } else if (l == Level::WARNING) {
            return "WARNING";
        } else if(l == Level::DEPRECATED) {
            return "DEPRECATED";
        }
        else {
            return "ERROR";
        }
    }

    void setLevel(enum Level newLevel) {
        level = newLevel;
    }
    enum Level getLevel() {
        return level;
    }

    // leave std:: in function arguments for doxygen
    void log(enum Level l, const std::string & s) {
        if (l >= level) {
            cerr << "\e[91m" << levelToString(l) << ":\e[96m " << s << "\e[0m" << endl;
        }
    }

    void DEBUG(const std::string &str) {
        log(Level::DEBUG, str);
    }

    void INFO(const std::string &str) {
        log(Level::INFO, str);
    }

    void WARN(const std::string &lbl, const std::string &str) {
        log(Level::WARNING, lbl + ": " + str);
    }

    void WARN(const std::string &str) {
        WARN("warning", str);
    }
    
    static map<size_t, bool> warn_history;
    void WARNONCE(const std::string &str) {
        size_t str_hash = hash<string>{}(str);
        if(!warn_history.count(str_hash)) {
            WARN("warning", str);
            warn_history[str_hash] = true;
        }
    }

    void ERROR(const std::string &label, const std::string &message) {
        log(Level::ERROR, label + ": " + message);
    }

    void ERROR(const std::string &message) {
        ERROR("error", message);
    }
    void DEPRECATED(const std::string &message) {
        log(Level::DEPRECATED, message);
    }
}
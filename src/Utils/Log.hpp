/*!
 * \file Log.hpp
 * \copyright GNU Public License v3.
 * \date 2020
 * \author Stephen Nicholas Swatman <s.n.swatman@uva.nl>, University of Amsterdam
 * \author Benjamin Rouxel <benjamin.rouxel@uva.nl>, University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef UTILS_LOG_H
#define UTILS_LOG_H

#include <string>

namespace Utils {
    //! Log legel
    enum class Level {
        DEBUG = 0, //!< Report all kind of debugging information
        INFO = 1, //!< Report some informations
        DEPRECATED = 2, //!< Report deprecated infos
        WARNING = 3, //!< Only shows warning and errors
        ERROR = 4 //!< Onlys shows error
    };

    /*! \brief Set the logging level
     * \param Level
     */
    void setLevel(enum Level);
    
    enum Level getLevel();
    
    /*! \brief Display a message for a certain level
     * \param Level
     * \param msg
     */
    void log(enum Level, const std::string &msg);

    /*!\brief Display a debug message
     * \param msg
     */
    void DEBUG(const std::string &msg);
    /*!\brief Display a info message
     * \param msg
     */
    void INFO(const std::string &msg);
    /*!\brief Display a warning message
     * \param msg
     */
    void WARN(const std::string &msg);
    /*!\brief Display a warning message
     * \param msg
     */
    void WARNONCE(const std::string &msg);
    /*!\brief Display a warning message marked with a tag
     * \param tag
     * \param msg
     */
    void WARN(const std::string &tag, const std::string &msg);
    /*!\brief Display an error message
     * \param msg
     */
    void ERROR(const std::string &msg);
    /*!\brief Display an error message marked with a tag
     * \param tag
     * \param msg
     */
    void ERROR(const std::string &tag, const std::string &msg);
    /*!\brief Display an deprecated message
     * \param msg
     */
    void DEPRECATED(const std::string &msg);
}

#endif
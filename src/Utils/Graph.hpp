/*!
 * \file Graph.hpp
 * \copyright GNU Public License v3.
 * \date 2020 
 * \author Benjamin Rouxel <benjamin.rouxel@uva.nl>, University of Amsterdam
 * \author Stephen Nicholas Swatman <s.n.swatman@uva.nl>, University of Amsterdam
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef UTILS_GRAPH_H
#define UTILS_GRAPH_H

#include <vector>
#include <vector>
#include <set>
#include <algorithm>
#include <queue>

#include "SystemModel.hpp"
#include "Utils.hpp"
#include "Schedule.hpp"

namespace Utils {
    /*!
     * @brief Extracts all weakly connected components in a graph.
     *
     * A weakly connected component is a set of nodes in a graph which, if the
     * graph were undirected (that is to say, edges could be traversed in
     * reverse), would allow a path between any two nodes in the WCC. For a
     * given CECILE SystemModel, we may have more than one WCC, but each
     * should contain at least a single node.
     *
     * In addition, all nodes returned are, per graph, topologically sorted. This 
     * property is required at many places in the compiler.
     *
     * @warning If one of the input graph has multiple entry points (multiple 
     * node without a predecessor). The transitive closure of the graph must be
     * computed first before any extraction, as only one entry point would be
     * detected. This can be done by calling the `transitive-closure` compiler 
     * pass before any pass requiring to extract graph.
     *
     * @warning All input graphs are assumed to be acyclic. This can be checked
     * using the `analyse-cycle` compiler pass.
     *
     * @param tg - SystemModel containing our graph of which we will find WCCs.
     * @param DAGS A vector of WCCs, each of which is a vector of node pointers.
     */
    void extractGraphs(const SystemModel *tg, std::vector<std::vector<Task*>> *DAGS);

    /**
     * @brief Determine whether a vector of tasks is in topological order.
     *
     * For an ordering of tasks to be in topological order, if a task u is a
     * predecessor of task v, then u must come before v in the order. For
     * example, given the following task graph:
     *
     *     +-> B --+
     *     |       |
     * A --+-> C --+-> D
     *
     * Here, [A, B, C, D] is a topological ordering, and so is [A, C, B, D],
     * but [B, C, A, D] is not since A must come before B. Similarly,
     * [A, B, D, C] is not a valid topological ordering, since C must come
     * before D.
     *
     * @param[in] tasks A vector of tasks.
     * @returns True if the vector is topologically ordered, otherwise false.
     */
    bool isTopologicalOrder(const std::vector<Task *> & tasks);
    
    
    /*! \brief Walk through a graph in a BFS manner
     * 
     * \param Qready list of next to visit tasks
     * \param visited list of visited tasks
     */
    void BFS(std::vector<Task*> *graph, std::function<void(Task*)> func);
    
    /*! \brief Walk through a graph in a reverse BFS manner
     * 
     * \param Qready list of next to visit tasks
     * \param visited list of visited tasks
     */
    void rBFS(std::vector<Task*> *graph, std::function<void(Task*)> func);
    
    void DFS(std::vector<Task*> *graph, std::function<void(Task*)> func);
    
    void rDFS(std::vector<Task*> *graph, std::function<void(Task*)> func);

    /**
     * @brief Determine whether a vector of tasks is in reversed topological order.
     *
     * For an ordering of tasks to be in reversed topological order, if a task u is a
     * predecessor of task v, then v must come before u in the order. For
     * example, given the following task graph:
     *
     *     +-> B --+
     *     |       |
     * A --+-> C --+-> D
     *
     * Here, [D, B, C, A] is a topological ordering, and so is [D, C, B, A],
     * but [B, C, D, A] is not since D must come before B. Similarly,
     * [B, D, C, A] is not a valid topological ordering, since D must come
     * before B.
     *
     * @param[in] tasks A vector of tasks.
     * @returns True if the vector is topologically ordered, otherwise false.
     */
    bool isReversedTopologicalOrder(const std::vector<Task *> & tasks);
    bool isReversedTopologicalOrder(const std::vector<SchedElt *> & tasks);
}

#endif
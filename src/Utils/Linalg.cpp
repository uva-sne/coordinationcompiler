/*!
 * \file Linalg.cpp
 * \copyright GNU Public License v3.
 * \date 2020 
 * \author Benjamin Rouxel <benjamin.rouxel@uva.nl>, University of Amsterdam
 * \author Stephen Nicholas Swatman <s.n.swatman@uva.nl>, University of Amsterdam
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "Linalg.hpp"
#include "SystemModel.hpp"

#include <boost/numeric/ublas/matrix.hpp>
#include <boost/rational.hpp>

namespace Utils {
    /*! \brief Swap row in the topology matrix
     * 
     * \param topo topology matrix
     * \param row1 
     * \param row2
     */
    static void swap(boost::numeric::ublas::matrix<boost::rational<int>> & topo, size_t row1, size_t row2) {
        for (size_t i = 0; i < topo.size2(); i++) {
            boost::rational<int> temp = topo(row1, i);

            topo(row1, i) = topo(row2, i);
            topo(row2, i) = temp;
        }
    }

    /*! \brief Reduce the matrix
     * 
     * \param q
     */
    static void row_echelon(boost::numeric::ublas::matrix<boost::rational<int>> & q) {
        size_t n = q.size1();
        size_t m = q.size2();

        size_t i = 0;
        size_t j = 0;

        while (i < n && j < m) {
            size_t best_row = i;
            boost::rational<int> best_quality(-1, 1);

            for (size_t z = i; z < n; z++) {
                boost::rational<int> tmp = boost::abs(q(z, j));

                if (tmp > best_quality) {
                    best_row = z;
                    best_quality = tmp;
                }
            }

            if (q(best_row, j) == 0) {
                j++;
            } else {
                swap(q, i, best_row);

                for (size_t z = j + 1; z < n; z++) {
                    boost::rational<int> f = q(z, j) / q(i, j);
                    q(z, j).assign(0, 1);

                    for (size_t x = j + 1; x < m; x++) {
                        q(z, x) -= f * q(i, x);
                    }
                }

                i++;
                j++;
            }
        }
    }
    
    size_t matrix_rank(const boost::numeric::ublas::matrix<int> & topo) {
        boost::numeric::ublas::matrix<boost::rational<int>> q(topo.size1(), topo.size2());

        for (size_t i = 0; i < topo.size1(); i++) {
            for (size_t j = 0; j < topo.size2(); j++) {
                q(i, j).assign(topo(i, j), 1);
            }
        }

        row_echelon(q);

        size_t rank = 0;
        for (size_t i = 0; i < q.size1(); i++) {
            for (size_t j = 0; j < q.size2(); j++) {
                if (q(i, j) != 0) {
                    rank++;
                    break;
                }
            }
        }

        return rank;
    }
}


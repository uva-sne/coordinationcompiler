/*
 * Copyright (C) 2022 Lukas Miedema <l.miedema@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <cstdint>
#include <vector>
#include <string>

namespace Uppaal {

struct location_t {
    int64_t x;
    int64_t y;
    uint64_t id;
    std::string label;
    bool committed = false;
    bool urgent = false;

    void toModel(std::string &out);
};

struct transition_t {
    location_t *from;
    location_t *to;
    std::string synchronization;
};

class LocationGroup {
    friend class ModelBuilder;
private:
    std::vector<location_t> _nodes;
    std::vector<LocationGroup> _groups;

    void locationsToModel(std::string &out, uint64_t depth, int64_t x, int64_t y);
};

/**
 * UPPAAL Model Builder -- builds UPPAAL models
 * Models are  structured in "groups" of related nodes. Within a group, all nodes are placed in a circle.
 * The model builder does no typechecking on node names, e.g. duplicate labels are permitted (but will be rejected by UPPAAL).
 */
class ModelBuilder {
private:
    uint64_t _init_id = -1;
    uint64_t _max_depth = 0;
    std::vector<LocationGroup *> _scope_stack;
    std::map<std::string, location_t *> _all_locations;
    LocationGroup _root;
    std::vector<transition_t> _transitions;

public:
    explicit ModelBuilder() {
        this->_scope_stack.push_back(&_root);
    }

    location_t *locationByLabel(const std::string id);

    location_t *createLocation(std::string label, bool initial = false);

    transition_t *createTransition(location_t *from, location_t *to);

    /**
     * Enter (start) a new group of nodes. These nodes will be logically laid out together, but this has no
     * semantic meaning to the model.
     */
    void enterGroup();

    /**
     * Exit the group again.
     */
    void exitGroup();

    /**
     * Create model
     */
     std::string toModel();

    void setInitial(location_t *location);
};
}
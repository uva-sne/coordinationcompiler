/*
 * Copyright (C) 2020 brouxel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* 
 * File:   Color.hpp
 * Author: brouxel
 *
 * Created on 9 septembre 2020, 10:16
 */

#ifndef COLOR_HPP
#define COLOR_HPP

#include <string>
#include <sstream>
#include <iomanip>

namespace Utils {

//! Type to represent a color
typedef struct {
    uint8_t r; //!< red
    uint8_t g; //!< green
    uint8_t b; //!< blue
    uint8_t a; //!< ?
} color_t;

/**
 * Transform a color to its HTML representation
 * @param c
 * @return 
 */
std::string color_to_html(const color_t &c);

//! List of suporterd color in TeX
constexpr char const* colors[] = {
"Apricot",
"Aquamarine",
"Bittersweet",
"Blue",
"BlueGreen",
"BlueViolet",
"BrickRed",
"Brown",
"BurntOrange",
"CadetBlue",
"CarnationPink",
"Cerulean",
"CornflowerBlue",
"Cyan",
"Dandelion",
"DarkOrchid",
"Emerald",
"ForestGreen",
"Fuchsia",
"Goldenrod",
"Gray",
"Green",
"GreenYellow",
"JungleGreen",
"Lavender",
"LimeGreen",
"Magenta",
"Mahogany",
"Maroon",
"Melon",
"MidnightBlue",
"Mulberry",
"NavyBlue",
"OliveGreen",
"Orange",
"OrangeRed",
"Orchid",
"Peach",
"Periwinkle",
"PineGreen",
"Plum",
"ProcessBlue",
"Purple",
"RawSienna",
"Red",
"RedOrange",
"RedViolet",
"Rhodamine",
"RoyalBlue",
"RoyalPurple",
"RubineRed",
"Salmon",
"SeaGreen",
"Sepia",
"SkyBlue",
"SpringGreen",
"Tan",
"TealBlue",
"Thistle",
"Turquoise",
"Violet",
"VioletRed",
"WildStrawberry",
"Yellow",
"YellowGreen",
"YellowOrange"
};
//! Size of the color array
constexpr size_t colors_arr_size = sizeof(colors)/sizeof(colors[0]);

}

#endif /* COLOR_HPP */


/*
 * Copyright (C) 2022 Lukas Miedema <l.miedema@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#define _USE_MATH_DEFINES_
#include <math.h>
#include <config.hpp>
#include "UppaalUtils.hpp"

Uppaal::location_t *Uppaal::ModelBuilder::createLocation(std::string label, bool initial) {
    LocationGroup *currentScope = this->_scope_stack[this->_scope_stack.size() - 1];
    location_t &node = currentScope->_nodes.emplace_back();
    this->_all_locations[label] = &node;
    node.id = this->_all_locations.size();
    node.label = std::move(label);
    if (initial) {
        assert (this->_init_id == ((uint64_t) -1));
        this->_init_id = node.id;
    }
    return &node;
}

Uppaal::transition_t *Uppaal::ModelBuilder::createTransition(Uppaal::location_t *from, Uppaal::location_t *to) {
    transition_t &transition = this->_transitions.emplace_back();
    transition.from = from;
    transition.to = to;
    return &transition;
}

void Uppaal::ModelBuilder::enterGroup() {
    LocationGroup *currentScope = this->_scope_stack[this->_scope_stack.size() - 1];
    LocationGroup *child = &currentScope->_groups.emplace_back();
    this->_scope_stack.push_back(child);
    this->_max_depth = std::max(this->_max_depth, this->_scope_stack.size());
}

void Uppaal::ModelBuilder::exitGroup() {
    this->_scope_stack.pop_back();
    assert(!this->_scope_stack.empty());
}

std::string Uppaal::ModelBuilder::toModel() {
    std::string out;

    // All locations depth-first
    this->_root.locationsToModel(out, this->_max_depth, 0, 0);

    // Initial location
    assert(this->_init_id != ((uint64_t) -1));
    out.append("<init ref=\"id")
        .append(std::to_string(this->_init_id))
        .append("\"/>\n");

    // Transitions
    for (auto &transition : this->_transitions) {
        std::vector<std::pair<int64_t, int64_t>> nails; // nails must be in the XML after the label

        out.append("<transition>\n");
        out
            .append("    <source ref=\"id")
            .append(std::to_string(transition.from->id))
            .append("\"/>\n");
        out
            .append("    <target ref=\"id")
            .append(std::to_string(transition.to->id))
            .append("\"/>\n");

        int64_t mx = (transition.from->x + transition.to->x) / 2;
        int64_t my = (transition.from->y + transition.to->y) / 2;
        if (transition.from == transition.to) {
            // Add some nails to make it visible
            nails.emplace_back(transition.from->x + 10, transition.from->y + 15);
            nails.emplace_back(transition.from->x - 10, transition.from->y + 15);

            // Shift the label as well
            my += 10;
        } else {
            // Add a nail to prevent covering
            const double offset = 0.1;
            const int64_t labelOffset = 25;
            double dx = transition.from->x - transition.to->x;
            double dy = transition.from->y - transition.to->y;
            double len = boost::math::hypot(dx, dy);
            dx /= len;
            dy /= len;
            nails.emplace_back(mx + dy * offset * len, my - dx * offset * len);

            // Shift the label as well
            mx += dy * labelOffset;
            my -= dx * labelOffset;
        }

        if (!transition.synchronization.empty()) {
            out
                .append(R"(    <label kind="synchronisation" x=")")
                .append(std::to_string(mx))
                .append("\" y=\"")
                .append(std::to_string(my))
                .append("\">")
                .append(transition.synchronization)
                .append("</label>\n");
            // TODO: other transition labels (update, guard, etc)?
        }

        for (auto &nail : nails) {
            out
                .append("    <nail x=\"")
                .append(std::to_string(nail.first))
                .append("\" y=\"")
                .append(std::to_string(nail.second))
                .append("\"/>\n");
        }

        out.append("</transition>\n");
    }
    return out;
}

Uppaal::location_t *Uppaal::ModelBuilder::locationByLabel(const std::string label) {
    return this->_all_locations[label];
}

void Uppaal::ModelBuilder::setInitial(Uppaal::location_t *location) {
    this->_init_id = location->id;
}

void Uppaal::LocationGroup::locationsToModel(std::string &out, uint64_t depth, int64_t baseX, int64_t baseY) {
    // Groups in a big circle
    if (!this->_groups.empty()) {
        // Create a circle of all this->_groups.size() elements
        // Depth determines the size per item
        uint64_t separation = (uint64_t) std::pow(2.5, depth) * 50;
        auto circumference = (double) (separation * this->_groups.size());
        double radius = circumference / M_PI / 2;
        double angleIncrement = 2 * M_PI / (double) this->_groups.size();

        for (size_t i = 0; i < this->_groups.size(); i++) {
            double angle = angleIncrement * (double) i;
            auto x = (int64_t) ((double) baseX + (cos(angle) * radius));
            auto y = (int64_t) ((double) baseY + (sin(angle) * radius));
            this->_groups[i].locationsToModel(out, depth - 1, x, y);
        }
    }

    // Own elements in a small circle
    if (!this->_nodes.empty()) {
        if (this->_nodes.size() == 1) {
            // just place it in the center
            location_t &loc = this->_nodes[0];
            loc.x = baseX;
            loc.y = baseY;
            loc.toModel(out);
        } else {
            const uint64_t separation = 15; // fixed
            auto circumference = (double) (separation * this->_groups.size());
            double radius = circumference / M_PI / 2;
            double angleIncrement = 2 * M_PI / (double) this->_nodes.size();

            for (size_t i = 0; i < this->_nodes.size(); i++) {
                double angle = angleIncrement * (double) i;
                location_t &loc = this->_nodes[i];
                loc.x = (int64_t) ((double) baseX + (cos(angle) * radius));
                loc.y = (int64_t) ((double) baseY + (sin(angle) * radius));
                loc.toModel(out);
            }
        }
    }
}

void Uppaal::location_t::toModel(std::string &out) {
    out.append("<location id=\"id")
            .append(std::to_string(this->id))
            .append("\" x=\"")
            .append(std::to_string(this->x))
            .append("\" y=\"")
            .append(std::to_string(this->y))
            .append("\">\n");
    out.append("  <name x=\"")
            .append(std::to_string(this->x - 30))
            .append("\" y=\"")
            .append(std::to_string(this->y))
            .append("\">")
            .append(this->label)
            .append("</name>\n");
    if (this->committed)
        out.append("  <committed/>\n");
    if (this->urgent)
        out.append("  <urgent/>\n");
    out.append("</location>\n");
}

/*!
 * \file SystemModel.hpp
 * \copyright GNU Public License v3.
 * \date 2017
 * \author Benjamin Rouxel <benjamin.rouxel@inria.fr> Irisa/INRIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TASKMODEL_H
#define TASKMODEL_H

#include <string>
#include <vector>
#include <map>
#include <tuple>
#include <utility>
#include <cstdio>
#include <exception>
#include <algorithm>
#include <set>
#include <cassert>

#include <boost/range/adaptor/map.hpp>
#include <boost/range/algorithm/copy.hpp>
#include <boost/assign.hpp>
#include <boost/filesystem.hpp>

#include "config.hpp"
#include "Interconnect/CoreMemInterConnect.hpp"
#include "Utils.hpp"
#include "Profile.hpp"
#include "Properties.hpp"

#include "Helper_macros.hpp"

//! Type of conditional edges
//! \todo implement conditional edges
enum conditional_edge_e {
    NONE, //!< edge is not conditional
    DATA, //!< condition based on data
    SCHEDULER, //!< condition based on scheduler
    ENVIRONMENT //!< condition based on environement
};

struct voltage_island;

//! Class representing a target processor
struct ComputeUnit {
    std::string id = "";//!< Id of the processor
    datamemsize_t spm_size = -1;//!< Size of available SPM, -1 if no SPM
    std::string type = "";//!< Type of the processor
    int sysID = -1; //!< Id of the core in the target system
    std::string voltage_island_id = "policy0"; //TODO Get rid of the string and just use the pointer
    voltage_island* target_voltage_island = nullptr; //!< Voltage island
    ArchitectureProperties::ComputeUnitType proc_class; //!< Class of the processor \see #ArchitectureProperties::ComputeUnitType
};

enum resource_access_type_e {
    FREE, MUTEX
};
//! Class representing an extra resource 
struct ResourceUnit {
    std::string id = "";//!< Id of the processor
    resource_access_type_e access = resource_access_type_e::FREE;
};

//TODO Voltage islands get types too??
struct voltage_island {
    std::string id;
    std::string type;
    int sysID = -1;
    std::vector<std::pair<frequencyinfo_t , powerinfo_t>> frequency_watt; //frequency and watt
    std::vector<ComputeUnit*> impacted_processors;
    frequencyinfo_t max_frequency ;
};

class Task;

/*! \brief IR element representing a connector
 *
 * Connector are also known as "port" they are the connection point of an edge in a graph
 */
class Connector {
    /*! \brief Friend class Task
     * \relates Task
     */
    friend class Task;
public:
    //! Type of connector
    enum type_e {
        REGULAR, //!< for a 1:1 relation
        DUPLICATE, //!< if data send by this connector have to be duplicated, output side
        SPLITTER, //!< for a 1:N relation, output side
        SPLITTED, //!< for a 1:N relation, input side
        JOINER, //!< for a N:1 relation, input side
        JOINED //!< for a N:1 relation, output side
    };

protected:
    std::string _id; //!< Id of the connector
    dataqty_t _tokens; //!< Nb of tokens sent/received
    std::string _token_type; //!< Type of the token
    bool _tainted; //!< Connector is tainted #TaintPropagation
    
    bool _part_of_cond_edge = false; //!< Is part of a conditional edge \todo implement conditional edges
    
    type_e _type = Connector::type_e::REGULAR; //!< Type of the connector \see #Connector::type_e

public:
    
    /*! \brief Construct a connector
     *
     * \param i \copybrief Connector::_id
     * \param o \copybrief Connector::_tokens
     * \param tt \copybrief Connector::_token_type
     * \param taint \copybrief Connector::_tainted
     */
    explicit Connector(std::string i, uint32_t o, std::string tt, bool taint) : _id(i), _tokens(o), _token_type(tt), _tainted(taint) {}
    /*! \copydoc Connector::Connector(std::string i, uint32_t o, std::string tt)
     * \param ct \copybrief Connector::_type
     */
    explicit Connector(std::string i, uint32_t o, std::string tt, bool taint, Connector::type_e ct) : _id(i), _tokens(o), _token_type(tt), _tainted(taint), _type(ct) {}
    /*! \brief Copy constructor
     * \param rhs RHS value
     * \see Connector::clone
     */
    Connector(const Connector& rhs) { clone(rhs); };
    /*! \copydoc Connector::Connector(const Connector& rhs)
     * \return the current connector
     */
    Connector& operator=(const Connector &rhs) { clone(rhs); return *this; };
    //! Destructor
    virtual ~Connector();
    /*! \brief Perform a deep copy of the connector
     * \param rhs RHS value
     */
    void clone(const Connector &rhs);
    
    ACCESSORS_R(Connector, std::string&, id)
    ACCESSORS_RW(Connector, dataqty_t, tokens)
    ACCESSORS_RW(Connector, std::string&, token_type)
    ACCESSORS_RW(Connector, Connector::type_e, type)
    ACCESSORS_RW(Connector, bool, part_of_cond_edge)
    ACCESSORS_RW(Connector, bool, tainted)
};

/*!
 * Pair of outport->inport that represents a link between two tasks.
 * When A.out -> B.in, one Connector object would be created linking Connection "A.out" and Connection "B.in". Both
 * tasks would have a pointer to this one Connector object.
 */
class Connection {
    friend class Connector;
    friend class Task;
    friend class SystemModel;
private:
    Connector *_from; //!< Always an outport
    Connector *_to; //!< Always in inport
protected:
    Connection(Connector *from, Connector *to) : _from(from), _to(to) {}
public:
    Connection(const Connection&) = delete;
    ACCESSORS_R(Connection, Connector *, from);
    ACCESSORS_R(Connection, Connector *, to);
};

//! Class to represent a conditional edge
//! \todo implement conditional edges
struct conditional_edge_t {
    conditional_edge_e cond; //!< type of condition
    std::vector<Task*> succs; //!< list of successors
};

class Phase {
    friend class Version; //!< Friend class Task
    Version *_version; //! reference to the version
    
    std::string _id; //!< unique identifier (unique within the task)
    timinginfos_t _C = 0; //!< WCET
    energycons_t _C_E = 0; //!< worst-case energy consumption
    std::string _ilp_label; //!< \internal short ILP label
    timinginfos_t _rt = 0; //!< release time
    frequencyinfo_t _frequency = 0; //!< CPU frequency
    frequencyinfo_t  _gpu_frequency = 0; //!< GPU frequency
    bool _gpu_required = false; //!< GPU required
    powerinfo_t _watt{};  //TODO: Watt or Joule ???

    datamemsize_t _local_memory_storage = 0; //!< value reflecting amount of data stored in local memory
    datamemsize_t _memory_footprint = 0; //!< memory footprint
    datamemsize_t _memory_code_size = 0; //!< size in bits of the code
    
    std::string _cname; //!< name of the C function to call
    std::string _csignature; //!< signature of the C function
    
    boost::filesystem::path _cfile = ""; //!< filename in which to find the C function
    boost::filesystem::path _cbinary = ""; //!< object file in which to find the C function
    
    std::set<ComputeUnit*> _force_mapping_proc; //!< list of processors on which the phase can be executed
    std::vector<ResourceUnit*> _resource_usage; //!< list of additional resources needed
    std::set<voltage_island*> _required_voltage_islands; //!< list of voltage islands that are required for a phase; i.e. a phase can have multiple voltage islands

    uint32_t _order = 0; //! to guarantee the ordering of task when getting the list of it from Version::phases

    TaskProperties::TaskType _type = TaskProperties::TaskType::TT_REGULAR;
    
    /**
     * Set the type of the task
     */
    void classify();
public:
    static size_t phase_counter; //!< count the number of phases to add an unique identifier at the end of the ilp_label
    
    /*! \brief Construct a phase
     * \param t the related version
     * \param i an id
     */
    explicit Phase(Version *t, std::string i);
    //! Destruct a phase
    virtual ~Phase();

    /*! \brief Copy constructor
     * \param rhs RHS value
     * \see Phase::clone
     */
    Phase(const Phase& rhs) { clone(rhs); };
    /*! \copydoc Phase::Phase(const Version& rhs)
     * \return the current phase
     */
    Phase& operator=(const Phase &rhs) { clone(rhs); return *this; };
    /*! \brief Perform a deep copy of the phase
     * \param rhs RHS value
     */
    void clone(const Phase &rhs);
    
    /*!\brief Assign the phase a new version
     * \param nt the new version
     */
    void version(Version *nt);
    
    /**
     * Return the phases that preceeds
     * 
     * If the phase is the first one of its version, then we look into the predecessor
     * of the task. If the task's predecessors have multiple version, all last phase
     * of each version are possible predecessor phases and so are added to the 
     * returned list
     * 
     * @return 
     */    
    std::vector<Phase*> predecessor_phases();
    
    /**
     * Return phases that succeeds
     * 
     * If the phase is the last one of its version, then we look into the successor
     * of the task. If the task's successors have multiple version, all 1st phase
     * of each version are possible successor phases and so are added to the 
     * returned list
     * 
     * @return 
     */    
    std::vector<Phase*> successor_phases();

    /**
     * !\brief Can this phase run on the given CU?
     * @param cu
     * @return true if so, false otherwise.
     */
    bool canRunOn(ComputeUnit *cu);
    
    ACCESSORS_R(Phase, Version*, version)
    ACCESSORS_R(Phase, std::string, id)
    ACCESSORS_RW(Phase, timinginfos_t, C)
    ACCESSORS_RW(Phase, energycons_t , C_E)
    ACCESSORS_RW(Phase, frequencyinfo_t , frequency)
    ACCESSORS_RW(Phase, frequencyinfo_t , gpu_frequency)
    ACCESSORS_RW(Phase, bool , gpu_required)
    ACCESSORS_RW(Phase, powerinfo_t , watt)
    ACCESSORS_R(Phase, std::string, ilp_label)
    ACCESSORS_RW(Phase, datamemsize_t, local_memory_storage)
    ACCESSORS_RW(Phase, datamemsize_t, memory_footprint)
    ACCESSORS_RW(Phase, datamemsize_t, memory_code_size)
    ACCESSORS_R(Phase, SINGLE_ARG(std::set<ComputeUnit*>&), force_mapping_proc)
    void force_mapping_proc(ComputeUnit *p);
    ACCESSORS_R(Phase, SINGLE_ARG(std::vector<ResourceUnit*>&), resource_usage)
    void resource_usage(ResourceUnit *p);
    ACCESSORS_RW(Phase, std::string, cname)
    ACCESSORS_RW(Phase, std::string, csignature)
    ACCESSORS_RW(Phase, boost::filesystem::path, cfile)
    ACCESSORS_RW(Phase, boost::filesystem::path, cbinary)
    ACCESSORS_R(Phase, uint32_t, order)
    ACCESSORS_RW(Phase, timinginfos_t, rt)
    ACCESSORS_RW(Phase, TaskProperties::TaskType, type)
};

/*! \brief IR element representing a version
 *
 * Different versions for a same task correspond to different implementation,
 * or code targeting different platforms, or code optimised with different flags
 * resulting with different energy/execution property.
 *
 * However different version of a same task must share the same implementation
 * signature, as well as the same timing properties which are deadline and period.
 */
class Version {
    friend class Task; //!< Friend class Task
    Task *_task; //!< reference to the task

    std::string _id; //!< unique identifier (unique within the task)
    std::string _ilp_label; //!< \internal short ILP label
    timinginfos_t _rt = 0; //!< release time

    uint32_t _security_lvl = 0; //!< security level
    
    //!\bug there might be some issue with it due to cloning
    std::vector<Version*> _force_previous; //!< \todo ???
    std::vector<Version*> _force_successors; //!< \todo ???
    
    std::map<std::string, Phase*> _phases;
    
    /**
     * Set the type of the task
     */
    void classify();
    
public:
    static size_t version_counter; //!< count the number of versions to add an unique identifier at the end of the ilp_label
    
    /*! \brief Construct a version
     * \param t the related task
     * \param i an id
     */
    explicit Version(Task *t, std::string i);
    //! Destruct a version
    virtual ~Version();

    /*! \brief Copy constructor
     * \param rhs RHS value
     * \see Version::clone
     */
    Version(const Version& rhs) { clone(rhs); };
    /*! \copydoc Version::Version(const Version& rhs)
     * \return the current version
     */
    Version& operator=(const Version &rhs) { clone(rhs); return *this; };
    /*! \brief Perform a deep copy of the version
     * \param rhs RHS value
     */
    void clone(const Version &rhs);
    
    /*!\brief Assign the version a new task
     * \param nt the new task
     */
    void task(Task *nt);
    
    void add_default_phase();
    void add_phase(Phase *p);
    void rem_phase(Phase *p);

    /**
     * !\brief Can this version (w/ all its phases) run on the given CU?
     * @param cu
     * @return true if so, false otherwise.
     */
    bool canRunOn(ComputeUnit *cu);
    
    ACCESSORS_R(Version, Task*, task)
    ACCESSORS_R(Version, std::string, id)
    ACCESSORS_R(Version, std::string, ilp_label)
    ACCESSORS_RW(Version, uint32_t, security_lvl)
    ACCESSORS_RW(Version, std::vector<Version*>, force_previous)
    ACCESSORS_RW(Version, std::vector<Version*>, force_successors)

    ACCESSORS_MAPGETVAL_R(Version, std::string, Phase*, phases)

    std::vector<Phase*> phases();
    std::vector<Phase*> phases() const;

    FORWARD_ACCESSORS_RW(Version, frequencyinfo_t, frequency, phases)
    FORWARD_ACCESSORS_RW(Version, frequencyinfo_t , gpu_frequency, phases)
    FORWARD_ACCESSORS_RW(Version, bool, gpu_required, phases)
    FORWARD_ACCESSORS_RW(Version, powerinfo_t , watt, phases)

    FORWARD_ACCESSORS_RW(Version, timinginfos_t, C, phases)
    FORWARD_ACCESSORS_RW(Version, energycons_t, C_E, phases)
    FORWARD_ACCESSORS_RW(Version, datamemsize_t, local_memory_storage, phases)
    FORWARD_ACCESSORS_RW(Version, datamemsize_t, memory_footprint, phases)
    FORWARD_ACCESSORS_RW(Version, datamemsize_t, memory_code_size, phases)
    FORWARD_ACCESSORS_R(Version, SINGLE_ARG(std::set<ComputeUnit*>&), force_mapping_proc, phases)
    void force_mapping_proc(ComputeUnit *p);
    FORWARD_ACCESSORS_R(Version, SINGLE_ARG(std::vector<ResourceUnit*>&), resource_usage, phases)
    void resource_usage(ResourceUnit *p);
    FORWARD_ACCESSORS_RW(Version, std::string, cname, phases)
    FORWARD_ACCESSORS_RW(Version, std::string, csignature, phases)
    FORWARD_ACCESSORS_RW(Version, boost::filesystem::path, cfile, phases)
    FORWARD_ACCESSORS_RW(Version, boost::filesystem::path, cbinary, phases)
    FORWARD_ACCESSORS_RW(Version, TaskProperties::TaskType, type, phases)


};

/*! \brief IR element representing a Task/component
 */
class Task {
    /*! \brief Friend class SystemModel
     * \relates SystemModel
     */
    friend class SystemModel;
public:
//    typedef std::pair<std::string, Version*> vers_t;//!< Convenient type to ease for(...) loop
//    typedef std::pair<std::string, Phase*> pha_t;//!< Convenient type to ease for(...) loop
    typedef std::map<Task*, std::vector<Connection*>, std::less<>> connector_map_t; //!<Convenient type with transparent comparator allowing const Task * lookups
    typedef std::map<Task*, std::vector<dataqty_t>>::value_type const& dtok_t;//!< Convenient type to ease for(...) loop
    typedef connector_map_t::value_type const& dep_t;//!< Convenient type to ease for(...) loop

    static const std::function<Connector* (Connection *)> connector_from; //!< Use in Utils::flatmap to get vector<Connector *>
    static const std::function<Connector* (Connection *)> connector_to; //!< Use in Utils::flatmap to get vector<Connector *>

private:
    /*! \brief no copy constructor
     * \param rhs
     */
    Task(const Task& rhs) { };
    /*! \brief no copie constructor
     * \param rhs
     * \return
     */
    Task& operator=(const Task &rhs) { return *this; }
    
    std::string _id; //!< unique identifier
    std::string _ilp_label; //!< short ILP label

    datamemsize_t _data_read = 0; //!< data read from all predecessors, in bits
    datamemsize_t _data_written = 0; //!< data written to all successors in bits
    
    uint32_t _repeat = 0; //!< number of times to repeat the task in steady state -- 0=infinite

    datamemsize_t _force_read_delay = 0; //!< override the communication delay for read data
    datamemsize_t _force_write_delay = 0; //!< override the communication delay for written data

    //-- Code generation information
    bool _stateful = false; //!< stateful or stateless actor, \todo unused
    std::map<Task*, std::vector<dataqty_t>> _delay_tokens;//!< delay tokens per edge, index i matches Connections[task][i], \todo implement delay tokens
    
    timinginfos_t _T = 0; //!<default period for the component
    bool _is_sporadic = false;//!<false if the period is strict
    timinginfos_t _D = 0; //!<default deadline for the component
    timinginfos_t _offset = 0; //!<default WCEC for the component
    
    energycons_t _C_E = 0; //!<default energy for the component
    frequencyinfo_t _frequency = 0;
    frequencyinfo_t  _gpu_frequency = 0;
    
    uint32_t _security_level = 0;//!<security level
    uint32_t _tainted = 0; //!<tainted indice, 0=no taint, 1=tainted by user, >1=taint propagation
    
    Profile _profile;//!< Options profile

    //----------
    connector_map_t _predecessor_tasks; //!< list of preceding tasks
    connector_map_t _successor_tasks; //!< list of succesing tasks

    std::map<Task*, std::vector<dataqty_t>> _eff_prev_rec;//!< amount of tokens effectively receive on the edge
    std::map<Task*, std::vector<dataqty_t>> _eff_suc_send;//!< amount of tokens effectively send on the edge

    std::vector<Task*> _previous_transitiv;  //!< list of all transitive predecessors
    std::vector<Task*> _successors_transitiv; //!< list of all transitive successors

    /*! \brief edge in the DAG
     *
     * Edge in a DAG but periodicity of src and sink are different. It preserves
     * the dependencies for code generation, while it is removed by the lower multi period DAG pass*/
    connector_map_t _previous_virtual;
    connector_map_t _successors_virtual;//!< edge in the DAG, \see #_previous_virtual

    std::map<std::string, Version*> _versions; //!< list of versions

    std::vector<Connector*> _inputs; //!< list of input ports/connectors
    std::vector<Connector*> _outputs; //!< list of output ports/connectors

    std::vector<conditional_edge_t> _conditional_out_groups; //!< list of conditional edge \todo  implement conditional edges
    bool _is_conditional_task = false; //!< true if all input edges are conditional, or if predecessors are conditional tasks \todo  implement conditional edges

    Task *_parent_task = nullptr;
    std::vector<Task*> _child_tasks;
    
    /**
     * Set the type of the task
     */
    void classify();
    
public:
    static size_t task_counter; //!<count the number of tasks to add an unique identifier at the end of the ilp_label

    /*! \brief Construct a task
     * \param id of the task
     */
    Task(const std::string &id);
    //! Destruct a task
    virtual ~Task();
    /*! \brief Perform a copy of the task
     * Omit dependencies, to also copy dependencies use #Task::deep_clone
     * \param rhs RHS value
     */
    virtual void clone(Task *rhs);
    /*! \brief Perform a deep copy of the task
     * The deep copy also includes predecessors/successors
     * \param rhs RHS value
     */
    virtual void deep_clone(Task *rhs);
    
    /*! \brief Return true if the two tasks can be executed concurrently
     * \param b another task
     * \return \copybrief Task::is_parallel
     */
    bool is_parallel(const Task *b) const;
    
    /*! \brief Return the data received from a task
     * \param from predecessor
     * \return amount of data
     */
    datamemsize_t data_read(const Task* from) const;
    /*! \brief Return the data sent to a task
     * \param to successor
     * \return amount of data
     */
    datamemsize_t data_written(const Task* to) const;
    
    /*! \brief Get a version with a specific name
     * \param n version name
     * \return a version or nullptr
     */
    inline Version* version(const std::string &n) const {return _versions.count(n) ? _versions.at(n) : nullptr;}
    //! \copydoc Task::version(const std::string &n) const
    inline Version* version(const std::string &n) { return (const_cast<const Task*>(this))->version(n); }

    void add_default_version();
    void add_version(Version *v);
    void rem_version(Version *v);

    //! Erase all versions
    void clear_versions();

    /**
     * \brief Returns a pointer to the input connector with the specified name
     * \param name connector
     * \return connector or nullptr
     */
    Connector * input(std::string & name) const;

    /**
     * \brief Returns a pointer to the output connector with the specified name
     * \param name connector
     * \return connector or nullptr
     */
    Connector * output(std::string & name) const;
    
    energycons_t static_dynamic_energy();

    ACCESSORS_R(Task, std::string &, id)
    ACCESSORS_R(Task, std::string &, ilp_label)
    ACCESSORS_RW(Task, uint32_t, repeat)
    ACCESSORS_RW(Task, datamemsize_t, data_read)
    ACCESSORS_RW(Task, datamemsize_t, data_written)
    ACCESSORS_MAPVALUES_R(Task, SINGLE_ARG(std::vector<Version*>), versions)
    ACCESSORS_MAPGETVAL_R(Task, std::string, Version*, versions)
    ACCESSORS_RW(Task, datamemsize_t, force_read_delay)
    ACCESSORS_RW(Task, datamemsize_t, force_write_delay)
    ACCESSORS_RW(Task, bool, stateful)
    ACCESSORS_R(Task, SINGLE_ARG(std::map<Task*, std::vector<dataqty_t>>&), delay_tokens)
    ACCESSORS_RW(Task, Profile&, profile)
    ACCESSORS_RW(Task, timinginfos_t, T)
    ACCESSORS_RW(Task, bool, is_sporadic)
    ACCESSORS_RW(Task, timinginfos_t, D)
    ACCESSORS_RW(Task, timinginfos_t, offset)
    ACCESSORS_RW(Task, uint32_t, tainted)
            
    FORWARD_ACCESSORS_RW(Task, timinginfos_t, C, versions)
    FORWARD_ACCESSORS_RW(Task, energycons_t, C_E, versions)
    FORWARD_ACCESSORS_RW(Task, datamemsize_t, local_memory_storage, versions)
    FORWARD_ACCESSORS_RW(Task, datamemsize_t, memory_footprint, versions)
    FORWARD_ACCESSORS_RW(Task, datamemsize_t, memory_code_size, versions)
    FORWARD_ACCESSORS_R(Task, SINGLE_ARG(std::set<ComputeUnit*>&), force_mapping_proc, versions)
    void force_mapping_proc(ComputeUnit *p);
    FORWARD_ACCESSORS_R(Task, SINGLE_ARG(std::vector<ResourceUnit*>&), resource_usage, versions)
    void resource_usage(ResourceUnit *p);
    FORWARD_ACCESSORS_RW(Task, std::string, cname, versions)
    FORWARD_ACCESSORS_RW(Task, std::string, csignature, versions)
    FORWARD_ACCESSORS_RW(Task, boost::filesystem::path, cfile, versions)
    FORWARD_ACCESSORS_RW(Task, boost::filesystem::path, cbinary, versions)
    FORWARD_ACCESSORS_RW(Task, uint32_t, security_lvl, versions)
            
    FORWARD_ACCESSORS_RW(Task, TaskProperties::TaskType, type, versions)
    
//    ACCESSORS_RW(Task, std::string, cname)
//    ACCESSORS_RW(Task, std::string, csignature)
    ACCESSORS_RW(Task, uint128_t, frequency)
    ACCESSORS_RW(Task, uint128_t, gpu_frequency)

    ACCESSORS_R(Task, connector_map_t&, predecessor_tasks)
    ACCESSORS_R(Task, connector_map_t&, successor_tasks)

    ACCESSORS_R(Task, SINGLE_ARG(std::map<Task*, std::vector<dataqty_t>>&), eff_prev_rec)
    ACCESSORS_R(Task, SINGLE_ARG(std::map<Task*, std::vector<dataqty_t>>&), eff_suc_send)

    ACCESSORS_R(Task, SINGLE_ARG(std::vector<Task*>&), previous_transitiv)
    ACCESSORS_R(Task, SINGLE_ARG(std::vector<Task*>&), successors_transitiv)

    ACCESSORS_R(Task, connector_map_t&, previous_virtual)
    ACCESSORS_R(Task, connector_map_t&, successors_virtual)

    ACCESSORS_R(Task, SINGLE_ARG(std::vector<Connector*>&), inputs)
    ACCESSORS_R(Task, SINGLE_ARG(std::vector<Connector*>&), outputs)

    ACCESSORS_R(Task, SINGLE_ARG(std::vector<conditional_edge_t>&), conditional_out_groups)
    ACCESSORS_RW(Task, bool, is_conditional_task)
            
    ACCESSORS_RW(Task, Task*, parent_task)
    ACCESSORS_RW(Task, SINGLE_ARG(std::vector<Task*>&), child_tasks)
};

/*! \brief Global IR container
 *
 * It represents the whole system given by the user, tasks, processors, ...
 */
class SystemModel {
private:
    // Architecture related
    CoreMemInterConnect *_interconnect = nullptr; //!< the representation of the physical core interconnection
    std::vector<ComputeUnit*> _processors; //!< list of processors, i.e. general purpose
    std::vector<ResourceUnit*> _resources; //!< list of resources other than compute unit
    std::vector<ComputeUnit*> _coprocessors; //!< list of coprocessors i.e. non-general purpose
    std::vector<voltage_island*> _voltage_islands;
    uint128_t _base_watt = 0;
    //TODO _max_frequency per voltage island??
//    int256_t _max_frequency = 0;
//    int256_t _max_GPU_frequency = 0;


    // Application related
    std::map<std::string, Task*> _tasks; //!< Map of tasks mapped from its component name
    globtiminginfos_t _global_deadline = 0; //!< the global deadline
    energycons_t _global_available_energy = -1; //!< the global available energy budget
    globtiminginfos_t _sequential_length = 0; //!< sequential length of the schedule
    globtiminginfos_t _sequential_length_energy_based = 0;
    globtiminginfos_t _global_period = 0;//!< global period to execute the app
    globtiminginfos_t _hyperperiod = 0; //!< hyperperiod

    
    TaskProperties::TaskSetType _taskset_type = TaskProperties::TaskSetType::TAS_INDEPENDENT;
    TaskProperties::DeadlineModel _taskset_deadline_type = TaskProperties::DeadlineModel::DEA_ARBITRARY;
    TaskProperties::RecurringModel _taskset_rec_model = TaskProperties::RecurringModel::REC_ARBITRARY;
    ArchitectureProperties::Cores _cu_type = ArchitectureProperties::Cores::COR_NONE;
    
    //units used for parsing input data standard set to
    //------------------------------------------------------------------------------------------------------------------
    std::string _time_unit = "ns"; //!< s, ns, ms etc
    std::string _energy_unit = "J"; //!< J, nJ etc .
    std::string _power_unit = "W"; //!< W atc.
    std::string _frequency_unit = "Hz"; //!< Hz, kHz etc.
    uint128_t _energy_model_time_step = 1; //!< time step for integration in e.g. ILP

    // (Fault-Tolerance) Profiles. Used to manage settings.
    // Extendable to use other settings not related to fault-tolerance.
    std::map<std::string, Profile> _global_profiles; //!< global profile
    
    /*! \brief Check if the task is conditionally executed
     *
     * \todo  implement conditional edges
     * \param Qdone
     * \param current
     * \param type
     */
    void is_conditional_task(std::vector<Task*> &Qdone, Task *current, conditional_edge_e type);
    /*! \brief Check if the edge is conditionally executed
     *
     * \todo  implement conditional edges
     * \param t
     * \param c
     * \param type
     * \return true if it is, false otherwise
     */
    bool is_conditional_edge(Task *t, Connector *c, conditional_edge_e type);

    /*! \brief Helper function to compute the critical path
     *
     * \param Qdone task already visited
     * \param current task to visit
     * \param heaviest size of the current critical path
     * \param heaviestpath list of tasks in the critical path
     * \return new size of the critical path
     */
    size_t get_critical_path_aux(std::vector<Task*> &Qdone, Task *current, size_t heaviest, std::vector<Task*> *heaviestpath) const;
public:
    typedef std::pair<std::string, Task*> TaskElt; //! Thanks to someone we don't have a vector anymore to iterate over the tasks(), and that someone uses "auto" which is disgusting
    
    //! Construct an application
    SystemModel();
    //! Destruct an application
    virtual ~SystemModel();
    
    /*! \brief Get a task through its ID
     * \param id \copybrief Task::_id
     * \return a task or nullptr
     */
    inline Task* task(const std::string &id) { return (const_cast<const SystemModel*>(this))->task(id); }
    //! \copydoc SystemModel::task(const std::string &id)
    Task* task(const std::string &id) const;
    
    /*! \brief Compute the number of edges
     * \return number of edges
     */
    size_t nbedges() const;
    //! \copydoc SystemModel::nbedges() const
    inline size_t nbedges() { return (const_cast<const SystemModel*>(this))->nbedges(); }
    
    /*! \brief Get a processor through its ID
     * \param p \copybrief ComputeUnit::id
     * \return a processor or nullptr
     */
    ComputeUnit* processor(const std::string &p) const;
    //! \copydoc SystemModel::processor(const std::string &p) const
    inline ComputeUnit* processor(const std::string &p) { return (const_cast<const SystemModel*>(this))->processor(p); }
    /*! \brief Get a coprocessor through its ID
     * \param p \copybrief ComputeUnit::id
     * \return a coprocessor or nullptr
     */
    ComputeUnit* coprocessor(const std::string &p) const;
    //! \copydoc SystemModel::processor(const std::string &p) const
    inline ComputeUnit* coprocessor(const std::string &p) { return (const_cast<const SystemModel*>(this))->coprocessor(p); }
    
    /*! \brief Get a processor through its ID
     * \param p \copybrief ComputeUnit::id
     * \return a processor or nullptr
     */
    ResourceUnit* resource(const std::string &p) const;
    //! \copydoc SystemModel::processor(const std::string &p) const
    inline ResourceUnit* resource(const std::string &p) { return (const_cast<const SystemModel*>(this))->resource(p); }
    
    voltage_island* voltage_islands(const std::string &v) const;
    inline voltage_island* voltage_islands(const std::string &v) { return (const_cast<const SystemModel*>(this))->voltage_islands(v); }
    
    void link_processors_and_voltage_islands();
    
    /*! \brief Add an edge in the graph
     *
     * \param src of the edge
     * \param sink of the edge
     * \param srcconname name of the connector from the src
     * \param sinkconname name of the connector from the src
     */
    Connection* add_dependency(Task* src, Task* sink, std::string srcconname, std::string sinkconname);
    /*!\copybrief SystemModel::add_dependency(Task* src, Task* sink, std::string srcconname, std::string sinkconname)
     * \param src of the edge
     * \param sink of the edge
     * \param srccon name of the connector from the src
     * \param sinkcon name of the connector from the src
     */
    Connection* add_dependency(Task* src, Task* sink, Connector *srccon, Connector *sinkcon);

    /*! \brief Add a task to the graph
     *
     * @param task task pointer to add
     */
    void add_task(Task* task);

    void rem_dependency(Task *src, Task *sink);
    
    void rem_task(Task *t);
    
    //! Erase all dependencies
    void clean_previous();
    
    //! Populate memory usage for each task
    void populate_memory_usage();
    
    //! Build the memory model
    void build_memory_model();
    
    //! Compute the sequential length of the graph
    void compute_sequential_schedule_length();
    void compute_best_sequential_length_based_on_energy();
    
    //! Populate conditional edges \todo implement conditional edges
    void populate_conditionality();
    
    //! Compute the hyperperiod
    void compute_hyperperiod();
    
    //! Return true if there's a non-0 and non-max hyperperiod
    bool has_hyperperiod() const;
//    void add_default_values(); //TODO: Can be removed?
    
    void convert_all_to_milli();

    //! Return true if there's a non-0 deadline.
    bool has_global_deadline() const;

    /*! \brief Get the critical path of the whole task set
     * \return list of tasks in the critical path
     */
    std::vector<Task*> get_critical_path() const;
    /*! \brief Get the critical path of the given task set
     * \param graph
     * \return list of tasks in the critical path
     */
    std::vector<Task*> get_critical_path(std::vector<Task*> graph) const ;

    /*! \brief Shallow-copy all tasks to a vector. The vector is newly allocated.
     * \return list of tasks
     */
    std::vector<Task*> tasks_as_vector() const;

    /*! \brief Obtain a one-shot mutable task iterator over the tasks.
     * @return task iterator.
     */
    boost::select_second_mutable_range<std::map<std::string, Task *>> tasks_it();

    /*! \brief Obtain a one-shot const task iterator over the tasks.
     * @return task iterator.
     */
    boost::select_second_const_range<std::map<std::string, Task *>> tasks_it() const;
    
    /*! \brief Get a profile by name
     * \param name
     * \return the profile
     */
    inline Profile& global_profile_by_name(std::string name) { return _global_profiles[name]; }
    /*! \brief Set a profile by name
     * \param name
     * \param value the profile
     */
    inline void global_profile_by_name(std::string name, Profile value) { _global_profiles[name] = value; }
    
    ACCESSORS_R(SystemModel, SINGLE_ARG(std::map<std::string, Task*>&), tasks)
    ACCESSORS_R(SystemModel, std::vector<ComputeUnit*>&, processors)
    ACCESSORS_R(SystemModel, std::vector<voltage_island*>&, voltage_islands)
    ACCESSORS_R(SystemModel, std::vector<ResourceUnit*>&, resources)
    ACCESSORS_R(SystemModel, SINGLE_ARG(std::vector<ComputeUnit*>&), coprocessors)
    ACCESSORS_RW(SystemModel, globtiminginfos_t, global_deadline)
    ACCESSORS_RW(SystemModel, energycons_t, global_available_energy)
    ACCESSORS_RW(SystemModel, globtiminginfos_t, global_period)
    ACCESSORS_R(SystemModel, globtiminginfos_t, sequential_length)
    ACCESSORS_R(SystemModel, globtiminginfos_t, sequential_length_energy_based)
    ACCESSORS_R(SystemModel, globtiminginfos_t, hyperperiod)
    ACCESSORS_RW(SystemModel, CoreMemInterConnect*, interconnect)
            
    ACCESSORS_R(SystemModel, TaskProperties::TaskSetType, taskset_type)
    ACCESSORS_R(SystemModel, TaskProperties::DeadlineModel, taskset_deadline_type)
    ACCESSORS_R(SystemModel, TaskProperties::RecurringModel, taskset_rec_model)
    ACCESSORS_R(SystemModel, ArchitectureProperties::Cores, cu_type)
            
    void classify();
    ACCESSORS_RW(SystemModel, uint128_t, base_watt)
    ACCESSORS_RW(SystemModel, std::string, time_unit)
    ACCESSORS_RW(SystemModel, std::string, energy_unit)
    ACCESSORS_RW(SystemModel, std::string, power_unit)
    ACCESSORS_RW(SystemModel, std::string, frequency_unit)
    ACCESSORS_RW(SystemModel, uint128_t, energy_model_time_step)
//    ACCESSORS_RW(SystemModel, int256_t, max_frequency)
//    ACCESSORS_RW(SystemModel, int256_t, max_GPU_frequency)
};

/******************************************************************************/

/**
 * Concatene a stringstream a Task ID
 * @param os
 * @param a
 * @return
 */
std::ostream& operator<< (std::ostream& os, const Task &a);
/**
 * Concatene a stringstream a Task ID
 * @param os
 * @param a
 * @return
 */
std::ostream& operator<< (std::ostream& os, const Task *a);

DECL_OPERATOR_OVERLOAD2(std::string, +, std::string, Task, Concatene a string a Task ID)
DECL_OPERATOR_OVERLOAD2(std::string, +, Task, std::string, Concatene a string a Task ID)

DECL_OPERATOR_OVERLOAD(bool, ==, Task, Return true if the Task are equal)
DECL_OPERATOR_OVERLOAD2(bool, ==, Task, std::string, Return true if the Task are equal)

DECL_OPERATOR_OVERLOAD(bool, !=, Task, Return true if the Task are different)

/**
 * Concatene a stringstream a version ID
 * @param os
 * @param a
 * @return
 */
std::ostream& operator<< (std::ostream& os, const Version &a);
/**
 * Concatene a stringstream a version ID
 * @param os
 * @param a
 * @return
 */
std::ostream& operator<< (std::ostream& os, const Version *a);

DECL_OPERATOR_OVERLOAD2(std::string, +, std::string, Version, Concatene a string a Version ID)
DECL_OPERATOR_OVERLOAD2(std::string, +, Version, std::string, Concatene a string a Version ID)

DECL_OPERATOR_OVERLOAD(bool, ==, Version, Return true if the Version are equal)
DECL_OPERATOR_OVERLOAD2(bool, ==, Version, std::string, Return true if the Version are equal)

DECL_OPERATOR_OVERLOAD(bool, !=, Version, Return true if the Version are different)

//------------------------------------------------------------------------------

DECL_OPERATOR_OVERLOAD2(bool, ==, Connector, std::string, Return true if the Connector are equal)
DECL_OPERATOR_OVERLOAD2(bool, !=, Connector, std::string, Return true if the Connector are different)
        
//------------------------------------------------------------------------------

/**
 * Concatene a stringstream a version ID
 * @param os
 * @param a
 * @return
 */
std::ostream& operator<< (std::ostream& os, const Phase &a);
/**
 * Concatene a stringstream a version ID
 * @param os
 * @param a
 * @return
 */
std::ostream& operator<< (std::ostream& os, const Phase *a);

DECL_OPERATOR_OVERLOAD2(std::string, +, std::string, Phase, Concatene a string a Phase ID)
DECL_OPERATOR_OVERLOAD2(std::string, +, Phase, std::string, Concatene a string a Phase ID)

DECL_OPERATOR_OVERLOAD(bool, ==, Phase, Return true if the Phase are equal)
DECL_OPERATOR_OVERLOAD2(bool, ==, Phase, std::string, Return true if the Phase are equal)

DECL_OPERATOR_OVERLOAD(bool, !=, Phase, Return true if the Phase are different)

//------------------------------------------------------------------------------
/**
 * Concatene a stringstream a processor ID
 * @param os
 * @param a
 * @return
 */
std::ostream& operator<< (std::ostream& os, const ComputeUnit& a);
/**
 * Concatene a stringstream a processor ID
 * @param os
 * @param a
 * @return
 */
std::ostream& operator<< (std::ostream& os, const ComputeUnit* a);

DECL_OPERATOR_OVERLOAD2(std::string, +, std::string, ComputeUnit, Concatene a string a processor ID)
DECL_OPERATOR_OVERLOAD2(std::string, +, ComputeUnit, std::string, Concatene a string a processor ID)

DECL_OPERATOR_OVERLOAD(bool, ==, ComputeUnit, Return true if the processor are equal)
DECL_OPERATOR_OVERLOAD2(bool, ==, ComputeUnit, std::string, Return true if the processor are equal)

DECL_OPERATOR_OVERLOAD(bool, !=, ComputeUnit, Return true if the processor are different)
        
//------------------------------------------------------------------------------
/**
 * Concatene a stringstream a resource ID
 * @param os
 * @param a
 * @return
 */
std::ostream& operator<< (std::ostream& os, const ResourceUnit& a);
/**
 * Concatene a stringstream a processor ID
 * @param os
 * @param a
 * @return
 */
std::ostream& operator<< (std::ostream& os, const ResourceUnit* a);

DECL_OPERATOR_OVERLOAD2(std::string, +, std::string, ResourceUnit, Concatene a string a resource ID)
DECL_OPERATOR_OVERLOAD2(std::string, +, ResourceUnit, std::string, Concatene a string a resource ID)

DECL_OPERATOR_OVERLOAD(bool, ==, ResourceUnit, Return true if the resource are equal)
DECL_OPERATOR_OVERLOAD2(bool, ==, ResourceUnit, std::string, Return true if the resource are equal)

DECL_OPERATOR_OVERLOAD(bool, !=, ResourceUnit, Return true if the resource are different)


/******************************************************************************/

#endif /* TASKMODEL_H */

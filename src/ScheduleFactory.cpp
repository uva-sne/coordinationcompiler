/*
 * Copyright (C) 2022 Benjamin Rouxel <benjamin.rouxel@unimore.it>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* 
 * File:   ScheduleFactory.cpp
 * Author: Benjamin Rouxel <benjamin.rouxel@unimore.it>
 * 
 * Created on 26 mai 2022, 15:14
 */

#include "ScheduleFactory.hpp"
#include "Utils/Graph.hpp"

using namespace std;

ScheduleFactory::ScheduleFactory() {
}

ScheduleFactory::ScheduleFactory(const ScheduleFactory& orig) {
}

ScheduleFactory::~ScheduleFactory() {
}

void ScheduleFactory::build_elements(Schedule *s) {
    if (s->is_built()) return;

    vector<vector<Task *>> DAGs;
    Utils::extractGraphs(s->tg(), &DAGs);
    uint128_t hyperperiod = s->tg()->hyperperiod();

    vector<size_t> repetition(DAGs.size());
    for(size_t i = 0 ; i < DAGs.size() ; ++i) {
        uint64_t period = 0;
        for(Task *t : DAGs.at(i)) {
            if(t->predecessor_tasks().size() == 0) {
                period = t->T();
                break;
            }
        }
        repetition[i] = (period > 0) ? hyperperiod / period : 1;
        Utils::DEBUG("Graph with root "+DAGs.at(i).at(0)->id()+" (period "+to_string(period)+") repeat "+to_string(repetition[i])+" on HP "+to_string(hyperperiod));
    }

    for(size_t i = 0 ; i < DAGs.size() ; ++i) {
        for(int j = repetition.at(i)-1 ; j >= 0 ; --j) {
            std::map<Task*, SchedJob*> pool;
            for(Task *t : DAGs.at(i)) {
                pool[t] = ScheduleFactory::build_job(s, t, j*t->T()+t->offset());
            }

            if(s->conf()->archi.interconnect.burst == "none") {
                // must be done in 2 steps as successors/predecessors list needs to have all packets a priori created
                for(Task *t : DAGs.at(i)) {
                    ScheduleFactory::build_packet_list_noburst(s, pool[t]);
                }
                for(Task *t : DAGs.at(i)) {
                    ScheduleFactory::build_successors_noburst(s, pool[t], &pool);
                }
            }
            else {
                // predecessors/successors lists done when building packets
                for(Task *t : DAGs.at(i)) {
                    ScheduleFactory::build_packet_list_burst(s, pool[t], &pool);
                }
            }
        }
    }
}

void ScheduleFactory::build_minimal_elements(Schedule *s) {
    if (s->is_built()) return;

    vector<vector<Task*>> DAGs;
    Utils::extractGraphs(s->tg(), &DAGs);

    for(size_t i = 0 ; i < DAGs.size() ; ++i) {
        std::map<Task*, SchedJob*> pool;
        for(Task *t : DAGs.at(i)) {
            pool[t] = ScheduleFactory::build_job(s, t, t->offset());
        }

        if(s->conf()->archi.interconnect.burst == "none") {
            // must be done in 2 steps as successors/predecessors list needs to have all packets a priori created
            for(Task *t : DAGs.at(i)) {
                ScheduleFactory::build_packet_list_noburst(s, (SchedJob *)pool[t]);
            }
            for(Task *t : DAGs.at(i)) {
                ScheduleFactory::build_successors_noburst(s, (SchedJob *)pool[t], &pool);
            }
        }
        else {
            // predecessors/successors lists done when building packets
            for(Task *t : DAGs.at(i)) {
                ScheduleFactory::build_packet_list_burst(s, (SchedJob *)pool[t], &pool);
            }
        }
    }
}

SchedJob* ScheduleFactory::build_job(Schedule *s, const Task *t, uint128_t release) {
    SchedJob *elt = nullptr;
    ScheduleFactory::try_build_job(s, t, release, &elt);
    return elt;
}

bool ScheduleFactory::try_build_job(Schedule *s, const Task *t, uint128_t release, SchedJob **job) {
    SchedJobPtr elt = std::unique_ptr<SchedJob>(new SchedJob(t, release));
    auto existingElt = s->elements_lut().find(elt->id());
    if(existingElt != s->elements_lut().end()) {
        *job = dynamic_cast<SchedJob*>(existingElt->second.get());
        elt = nullptr;
        return false;
    }

    elt->wct(t->C());
    elt->mem_data(t->memory_footprint() - ((s->conf()->archi.spm.store_code) ? 0 : t->memory_code_size()));
    *job = elt.get();
    s->elements_lut()[elt->id()] = std::move(elt);
    return true;
}

SchedPacket* ScheduleFactory::build_packet(Schedule *s, SchedJob *t, timinginfos_t release, const std::string &dirlbl, SchedJob *senderOrDest, uint32_t packetnum) {
    SchedPacket *elt = nullptr;
    ScheduleFactory::try_build_packet(s, t, release, dirlbl, senderOrDest, packetnum, &elt);
    return elt;
}

bool ScheduleFactory::try_build_packet(Schedule *s, SchedJob *t, timinginfos_t release, const std::string &dirlbl, SchedJob *senderOrDest, uint32_t packetnum, SchedPacket **packet) {
    SchedPacketPtr elt = std::unique_ptr<SchedPacket>(new SchedPacket(t, release, dirlbl, senderOrDest, packetnum));
    auto existingElt = s->elements_lut().find(elt->id());
    if(existingElt != s->elements_lut().end()) {
        *packet = dynamic_cast<SchedPacket*>(existingElt->second.get());
        elt = nullptr;
        return false;
    }
    *packet = elt.get();
    
    if(elt->dirlbl() == "r")
        t->add_read_packet(elt.get());
    else
        t->add_write_packet(elt.get());
    
    s->elements_lut()[elt->id()] = std::move(elt);
    return true;
}

void ScheduleFactory::build_packet_list_burst(Schedule *sched, SchedJob *schedtask, std::map<Task*, SchedJob*> *pool) {
    if(schedtask->are_successors_created())
        return;
    schedtask->are_successors_created(true);

    vector<SchedPacket*> siblings;
    for(Task::dep_t prev : schedtask->task()->predecessor_tasks()) {
        SchedJob *prevelt = (SchedJob*) pool->at(prev.first);

        uint32_t D_ij = 0;
        for (Connection *conn : prev.second)
            D_ij += conn->to()->tokens();

        if(D_ij == 0) {
            prevelt->add_successor(schedtask);
            continue;
        }

        for (Connection *conn : prev.second) {
            Connector *inport = conn->to();
            string tokentype = inport->token_type();
            vector<dataqty_t> slots = CoreMemInterConnect::burst(*sched->conf(), D_ij, sched->conf()->datatypes.at(tokentype).size_bits);
            uint32_t worst_conc = sched->tg()->interconnect()->worst_contention();
            timinginfos_t forced_write_delay = prev.first->force_write_delay();
            timinginfos_t forced_read_delay = schedtask->task()->force_read_delay();

            SchedPacket *current_w_packet = ScheduleFactory::build_packet(sched, prevelt, prevelt->_min_rt_period, "w", schedtask, 1);
            current_w_packet->set_transmission(slots.size(), slots[0], tokentype, sched->tg()->interconnect()->comm_delay(worst_conc, slots[0], tokentype, forced_write_delay));
            prevelt->add_successor(current_w_packet);

            SchedPacket *current_r_packet = ScheduleFactory::build_packet(sched, schedtask, schedtask->_min_rt_period, "r", prevelt, 1);
            current_r_packet->set_transmission(slots.size(), slots[0], tokentype, sched->tg()->interconnect()->comm_delay(worst_conc, slots[0], tokentype, forced_read_delay));
            siblings.push_back(current_r_packet);
            current_w_packet->add_successor(current_r_packet);

            for (uint s = 1; s < slots.size(); ++s) {
                SchedPacket *w_packet = ScheduleFactory::build_packet(sched, prevelt, prevelt->_min_rt_period, "w", schedtask, s + 1);
                w_packet->set_transmission(slots.size(), slots[s], tokentype, sched->tg()->interconnect()->comm_delay(worst_conc, slots[s], tokentype, forced_write_delay));
                current_w_packet->add_successor(w_packet);
                current_w_packet = std::move(w_packet);

                SchedPacket *r_packet = ScheduleFactory::build_packet(sched, schedtask, schedtask->_min_rt_period, "r", prevelt, s + 1);
                r_packet->set_transmission(slots.size(), slots[s], tokentype, sched->tg()->interconnect()->comm_delay(worst_conc, slots[s], tokentype, forced_read_delay));
                current_r_packet->add_successor(r_packet);
                current_r_packet = std::move(r_packet);

                current_w_packet->add_successor(current_r_packet);
            }
            current_r_packet->add_successor(schedtask);
        }

    }
    //can be optimized with iterator
    for(SchedPacket *p : siblings) {
        for(SchedPacket *s : siblings) {
            if(s == p) continue;
            p->_siblings.push_back(s);
        }
    }
}
void ScheduleFactory::build_packet_list_noburst(Schedule *s, SchedJob *schedtask) {
    if(s->conf()->runtime.rtt == runtime_task_e::SINGLEPHASE)
        return;

    uint32_t data = schedtask->task()->data_read();
    if(data > 0 && !schedtask->task()->predecessor_tasks().empty()) {
        SchedPacket *readpacket = ScheduleFactory::build_packet(s, schedtask, schedtask->_min_rt_period, "r", schedtask, 1);
        // Suspicious code: datatype is only that of the first inport, even when there are > 1 inports
        string datatype = (*(schedtask->task()->predecessor_tasks().begin())).second[0]->to()->token_type();
        readpacket->set_transmission(1, data, datatype,
            s->tg()->interconnect()->comm_delay(s->tg()->interconnect()->worst_contention(), data, datatype, schedtask->task()->force_read_delay())
        );
    }

    data = schedtask->task()->data_written();
    if(data > 0 && !schedtask->task()->successor_tasks().empty()) {
        SchedPacket *writepacket = ScheduleFactory::build_packet(s, schedtask, schedtask->_min_rt_period, "w", schedtask, 1);
        assert(writepacket != nullptr);
        // Suspicious code: datatype is only that of the first outport, even when there are > 1 outports
        string datatype = (*(schedtask->task()->successor_tasks().begin())).second[0]->from()->token_type();
        writepacket->set_transmission(1, data, datatype,
            s->tg()->interconnect()->comm_delay(s->tg()->interconnect()->worst_contention(), data, datatype, schedtask->task()->force_write_delay())
        );
    }
}


void ScheduleFactory::build_successors_noburst(Schedule *s, SchedJob *schedtask, std::map<Task*, SchedJob*> *pool) {
    if(schedtask->are_successors_created())
        return;

    schedtask->are_successors_created(true);
    if(s->conf()->runtime.rtt == runtime_task_e::SINGLEPHASE) {
        for(Task::dep_t prev: schedtask->task()->predecessor_tasks())
            pool->at(prev.first)->add_successor(schedtask);
    } else {
        if(schedtask->packet_read().size() > 0) {
            SchedPacket *readpacket = schedtask->packet_read().at(0); //there is a single packet when no burst
            readpacket->add_successor(schedtask);

            for(Task::dep_t prev: schedtask->task()->predecessor_tasks()) {
                SchedJob *prevelt = (SchedJob*) pool->at(prev.first);
                if(prevelt->packet_list(1) != nullptr)
                    prevelt->packet_list(1)->add_successor(readpacket);
                else
                    prevelt->add_successor(readpacket);
            }
        }
        else {
            for(Task::dep_t prev: schedtask->task()->predecessor_tasks()) {
                SchedJob *prevelt = (SchedJob*) pool->at(prev.first);
                if(prevelt->packet_list(1) != nullptr)
                    prevelt->packet_list(1)->add_successor(schedtask);
                else
                    prevelt->add_successor(schedtask);
            }
        }

        
        if(schedtask->packet_write().size() > 0)
            schedtask->add_successor( schedtask->packet_write().at(0));//there is a single packet when no burst
    }
}

SchedCore* ScheduleFactory::add_core(Schedule *s, ComputeUnit *proc) {
    SchedCore *c = s->schedcore(proc);
    if(c != nullptr) return c;

    c = new SchedCore(proc);
    s->schedcores().push_back(c);
    return c;
}

SchedResource* ScheduleFactory::add_resource(Schedule *s, ResourceUnit *res) {
    SchedResource *r = s->schedresource(res->id);
    if(r != nullptr) return r;

    r = new SchedResource(res, res->id);
    s->schedresources().push_back(r);
    return r;
}

SchedEltPtr ScheduleFactory::shallow_copy(SchedElt *src) {
    if(Utils::isa<SchedJob*>(src))
        return std::unique_ptr<SchedElt>(new SchedJob((SchedJob &)*src));
    else
        return std::unique_ptr<SchedElt>(new SchedPacket((SchedPacket &)*src));
}

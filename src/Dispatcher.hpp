/*
 * Copyright (C) 2022 Lukas Miedema <l.miedema@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CECILE_DISPATCHER_HPP
#define CECILE_DISPATCHER_HPP

#include <Utils/Log.hpp>
#include "CompilerPass.hpp"
#include "Utils.hpp"
#include "Helper_macros.hpp"

typedef std::unique_ptr<CompilerPass> CompilerPassPtr;

class Dispatcher {
private:
    std::vector<CompilerPassPtr> _passes;

public:
    Dispatcher() = default;
    Dispatcher(Dispatcher&) = delete;

    /**
     * Find pass or null matching the RTTI before the given pass.
     * @param rtti
     * @param before
     * @return pass pointer, or nullptr
     */
    CompilerPass * find_pass_before(const std::string &rtti, const CompilerPass *before = nullptr) const;

    /**
     * Find pass or null matching the RTTI before the given pass.
     * @param rtti
     * @param before
     * @return pass pointer, or nullptr
     */
    CompilerPass *find_pass_before(const std::function<bool(const CompilerPass *)> &matcher, const CompilerPass *before = nullptr) const;

    /**
     * Find a pass that is either a scheduler or solver before the given pass.
     * @param before
     * @return pass pointer, or nullptr
     */
    CompilerPass *find_scheduler_or_solver_before(const CompilerPass *before) const;

    /**
     * Add a pass to the end of the list of passes. Ownership is assumed by the Dispatcher.
     * @param pass
     */
    CompilerPass *add_pass(CompilerPass *pass);


    /**
     * Call passes in the provided order.
     * @param tg
     * @param conf
     * @param ssm (optional)
     */
    void dispatch(SystemModel *tg, config_t *conf, StrategyStateMachine *smg = nullptr);

    /**
     * First compiler pass, for iteration
     * @return
     */
    std::vector<CompilerPassPtr>::iterator begin();

    /**
     * Last compiler pass, for iteration
     * @return
     */
    std::vector<CompilerPassPtr>::iterator end();

    /**
     * Last compiler pass (const)
     * @return
     */
    const CompilerPass *back() const;

    /**
     * Last compiler pass
    * @return
    */
    CompilerPass *back() {
        return const_cast<CompilerPass *>(const_cast<const Dispatcher*>(this)->back());
    }

    /**
     * Get the vector of passes.
     * @return
     */
    std::vector<CompilerPassPtr> *operator->();

    ACCESSORS_R(Dispatcher, std::vector<CompilerPassPtr>&, passes);
};


#endif //CECILE_DISPATCHER_HPP

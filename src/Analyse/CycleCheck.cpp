/*!
 * \file CycleCheck.cpp
 * \copyright GNU Public License v3.
 * \date 2019 
 * \author Benjamin Rouxel <benjamin.rouxel@uva.nl>, University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "CycleCheck.hpp"
#include "Utils/Log.hpp"
#include "Utils/Graph.hpp"

using namespace std;

IMPLEMENT_HELP(CycleCheck,
    Check if there is a cycle in any of the input graphs. \n
    Note: this pass should be run any time you play with graph as most
    of the underlying analyses deal only with DAG. \n
    HTMLINDENT * throwerror [CONFIG_FALSE-CONFIG_TRUE], default CONFIG_FALSE: 
        If set to CONFIG_TRUE will throw an error rather than raising a warning.
)

void CycleCheck::forward_params(const map<string, string> &args) {
    if(args.count("throwerror"))
        forbid_cycle = args.at("throwerror") == CONFIG_TRUE;
}

const string CycleCheck::get_uniqid_rtti() const { return "analyse-cycle"; }

CycleCheck::CycleCheck() : Analyse() {}

//! \remark Use a faster check if the transitive closure has been computed with #TransitiveClosure
void CycleCheck::do_analyse() {
    bool has_cycle = false;
    
    if(conf->passes.find_pass_before("transform-transitive-closure"))
        has_cycle = cycle_check_fast();
    else
        has_cycle = cycle_check();
    
    if(has_cycle) {
        if(forbid_cycle)
            throw CompilerException("cycle-check", "Cycle detected");
        Utils::WARN("cycle-check: Cycle detected");
    }
}

bool CycleCheck::cycle_check_fast() {
    for(Task *t : tg->tasks_it()) {
        for(Task *s : t->successors_transitiv()) {
            if(find(s->successors_transitiv().begin(), s->successors_transitiv().end(), t) != s->successors_transitiv().end()) {
                Utils::DEBUG("Cycle between "+t->id()+" and "+s->id());
                return true;
            }
        }
    }
    return false;
}

/**
 * @brief Determines whether a set of nodes contains a cycle.
 *
 * In a directed graph, a cycle is a path that can be taken to get from one
 * node back to itself. Even a single cycle disqualifies a graph from being
 * a DAG (DAGs are acyclic after all), so it can be important for us to
 * detect cycles.
 * 
 * @return true if there is a cycle 
 */
bool CycleCheck::cycle_check() {
    vector<Task*> global_visited;
    for(Task *t : tg->tasks_it()) {
        if(t->predecessor_tasks().size() > 0) continue;
        vector<Task*> visited;
        if(cycle_check_aux(t, &visited, &global_visited))
            return true;
    }
    //if we didn't visit all nodes, then there are graphs that are circular
    sort(global_visited.begin(), global_visited.end());
    vector<Task*>::iterator it = unique(global_visited.begin(), global_visited.end());
    global_visited.resize(distance(global_visited.begin(), it));
    return (tg->tasks().size() != global_visited.size());
}

bool CycleCheck::cycle_check_aux(Task *current, vector<Task*> *visited, vector<Task*> *glob_visited) {
    if(find(visited->begin(), visited->end(), current) != visited->end() || visited->size() > tg->tasks().size()) {
        return true;
    }
    visited->push_back(current);
    glob_visited->push_back(current);
    
    for(Task::dep_t el : current->successor_tasks()) {
        vector<Task*> nextvisited(visited->begin(), visited->end());
        if(cycle_check_aux(el.first, &nextvisited, glob_visited)) 
            return true;
    }
    return false;
}
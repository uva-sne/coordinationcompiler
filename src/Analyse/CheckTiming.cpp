/*!
 * \file CheckTiming.cpp
 * \copyright GNU Public License v3.
 * \date 2019 
 * \author Benjamin Rouxel <benjamin.rouxel@uva.nl>, University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "CheckTiming.hpp"

using namespace std;

IMPLEMENT_HELP(CheckTiming,
    Roughly check the timing of the application. This should be more
    used to check input rather than to guarantee timing constraints.
    It checks if the sum of wcet on the critical path is lower than
    the deadline. \n
    * throwerror [CONFIG_FALSE-CONFIG_TRUE], default CONFIG_FALSE: 
        If set to CONFIG_TRUE will throw an error rather than raising a warning.
)

void CheckTiming::forward_params(const map<string, string> &args) {
    if(args.count("throwerror"))
        force = args.at("throwerror") != CONFIG_TRUE;
}

const string CheckTiming::get_uniqid_rtti() const { return "analyse-check_timing_infos"; }

CheckTiming::CheckTiming() : Analyse() {}

void CheckTiming::do_analyse() {
    uint64_t hyperperiod = (uint64_t)tg->hyperperiod();
    
    vector<vector<Task*>> dags;
    Utils::extractGraphs(tg, &dags);
    for(vector<Task*> dag : dags) {
        vector<Task*> chain;
        get_longest_path(dag, &chain);
        
        uint64_t period = 0;
        uint64_t deadline = 0;
        uint64_t wcet = 0;
        string id = "";
        for(Task *t : chain) {
            id = t->id();
            if(t->T() > 0)
                period = t->T();
            if(t->D() > 0)
                deadline = t->D();
            if(t->C() <= 0)
                throw CompilerException("check timing properties", "Missing WCET for task "+t->id());
            wcet += t->C();
            // if communication does not work in the best case, then it will never work
            wcet += tg->interconnect()->comm_delay(0, t->data_read(), "bit", t->force_read_delay());
            wcet += tg->interconnect()->comm_delay(0, t->data_written(), "bit", t->force_write_delay());
        }
        
        if(period == 0) {
            Utils::DEBUG("No period for DAG with "+to_string(dag.size())+" tasks including task "+id);
            continue;
        }
        
        if(wcet > deadline) {
            Utils::DEBUG("DAG including task "+id+" with "+to_string(dag.size())+" tasks -- WCET: "+to_string(wcet)+", deadline: "+to_string(deadline));
            handle_dissonance("Schedule will not be possible as the sum of WCET in the critical path of one input DAG (including task "+id+") is above its deadline");
        }
        
        if(wcet*(hyperperiod/period) > hyperperiod) {
            Utils::DEBUG("DAG including task "+id+" with "+to_string(dag.size())+" tasks -- WCET: "+to_string(wcet)+", rep: "+to_string(hyperperiod/period)+", HP: "+to_string(hyperperiod));
            handle_dissonance("Schedule will not be possible as the sum of WCET in the critical path of one input DAG (including task "+id+") by the number of repetition in the hyperperiod is above the hyperperiod");
        }
        
        Utils::DEBUG("OK DAG including task "+id+" with "+to_string(dag.size())+" tasks -- WCET: "+to_string(wcet)+", period: "+to_string(period)+", rep: "+to_string(hyperperiod/period)+", HP: "+to_string(hyperperiod));
    }
}

//! \remark depends on Transform/#PropagateProperties
void CheckTiming::check_dependencies() {
    if (conf->passes.find_pass_before("transform-propagate_properties", this) == nullptr) {
        throw CompilerException("dependency check", "Dependency unsatisfied, need transform-propagate_properties");
    }
}

void CheckTiming::handle_dissonance(const string &msg) {
    if(force)
        Utils::WARN(msg);
    else
        throw CompilerException("check_timing_infos", msg);
}

uint128_t CheckTiming::get_longest_path_nbtasks_aux(map<Task*, pair<uint128_t, vector<Task*>>> &Qdone, Task *current, uint128_t longest, vector<Task*> *longestchain) {
    if(Qdone.count(current)) {
        longestchain->insert(longestchain->end(), Qdone.at(current).second.begin(), Qdone.at(current).second.end());
        return longest+Qdone.at(current).first;
    }
    Qdone[current].first = 0;

    uint128_t max = 0;
    vector<Task*> chain;
    for(Task::dep_t s : current->successor_tasks()) {
        vector<Task*> currentchain;
        uint128_t pathlength = get_longest_path_nbtasks_aux(Qdone, s.first, longest, &currentchain);
        if(pathlength > max) {
            max = pathlength;
            chain.clear();
            chain.insert(chain.end(), currentchain.begin(), currentchain.end());
        }
    }
    longestchain->insert(longestchain->end(), chain.begin(), chain.end());
    longestchain->push_back(current);
    Qdone[current].first = max+current->C();
    Qdone[current].second = *longestchain;
    return max+current->C();
}

void CheckTiming::get_longest_path(vector<Task*> dag, vector<Task*> *longestchain) {
    uint128_t longest = 0;
    vector<Task*> chain;
    map<Task*, pair<uint128_t, vector<Task*>>> Qdone;
    for(Task *root : dag) {
        if(root->predecessor_tasks().size() > 0) continue;
        
        vector<Task*> currentchain;
        uint128_t pathlength = get_longest_path_nbtasks_aux(Qdone, root, 0, &currentchain);
        if(pathlength > longest) {
            longest = pathlength;
            chain.clear();
            chain.insert(chain.end(), currentchain.begin(), currentchain.end());
        }
    }
    longestchain->insert(longestchain->end(), chain.begin(), chain.end());
}

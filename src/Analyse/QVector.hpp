/*!
 * \file QVector.hpp
 * \copyright GNU Public License v3.
 * \date 2019 
 * \author Benjamin Rouxel <benjamin.rouxel@uva.nl>, University of Amsterdam
 * \author Stephen Nicholas Swatman <s.n.swatman@uva.nl> University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef QVECTOR_H
#define QVECTOR_H

#include "Analyse.hpp"
#include "Schedule.hpp" //Unschedulable exception

#include <boost/numeric/ublas/matrix.hpp>
#include <boost/rational.hpp>

namespace bu = boost::numeric::ublas;

/**
 * @brief Compute the number of times (repetition vector, or q-vector) tasks 
 * must be duplicated to further transform a SDF into a HSDF.
 *
 * Some type of graph, e.g. SDF graph, fork-join graph.. , requires a
 * transformation step prior to be scheduled: computing the HSDF.
 *
 * The homogenization in cecile consists of two steps. First, we must compute
 * the number of duplicates (repetition vector, or q-vector) that we need to make 
 * for each task. Subsequently, it is possible to expand the graph to its
 * homogenized form with the "homogeneize" pass.
 *
 * As en example of homogenization, consider the following trivial graph:
 *
 * A ----> B
 *
 * If we assume A produces 1 token and B consumes 1 token, then the graph is 
 * already homogeneous. However, if A instead produces 2 tokens
 * the graph becomes non-homogeneous and we would have to call the task B two
 * times to reach a consistent production/consumption:
 *
 *     +-> B1
 *     |
 * A --+-> B2
 *
 * In this case, the 2-token output of A would be split into two 1-token
 * outputs which would be routed to the two instances of B. The repetition vector
 * would be [1, 2], as we need 1 instance of A and 2 instances of B.
 *
 * If we connect a further task to B, we obtain the following graph:
 *
 * A 1---->2 B 1---->1 C
 *
 * In this case, if the connection between B and C is homogeneous but the
 * connection between A and B is not (like before), we will need to instantiate C
 * together with B to keep homogeneity.
 *
 *     +-> B1 ----> C1
 *     |
 * A --+-> B2 ----> C2
 *
 * @remark For large graphs with complicated token counts, this process can
 * cause a rapid explosion in the number of tasks.
 *
 * For more information about this concept, see the paper "Static Scheduling of
 * Synchronous Data Flow Programs for Digital Signal Processing" by Edward
 * Ashford Lee and David G. Messerschmitt.
 *
 * <https://doi.org/10.1109/tc.1987.5009446>
 * 
 * \see QVector::help
 */
class QVector : public Analyse {
public:
    //! \brief Constructor
    QVector();
    const std::string get_uniqid_rtti() const override;
    virtual std::string help() override;
    void forward_params(const std::map<std::string,std::string>&) override;
    
protected:
    void do_analyse() override;
    
    bool is_communcation_unbalanced();
    
    /*! \brief Construct the topology matrix
     * 
     * The topology matrix represent the connexion within a graph. It is a square
     * matrix with as much rows/columns as tasks, and a positiv value means
     * there is an edge between the row task to the column one, while a negativ value
     * means there is an edge between the column task to the row one. 
     * 
     * The positiv/negativ values represent the data exchange production/consumption
     * between the source and the sink of the edge.
     * 
     * \param topo the topology matrix to fill
     * \param indices matching between tasks and rows/columns id
     */
    void fill_topology_matrix(bu::matrix<int> *topo, std::map<Task*, size_t> *indices);
    
    /*!
     * \brief Compute the repetition vector using a DFS algorithm to walk 
     * through the graph
     * 
     * \param qvector the resulting repetition vector
     */
    void compute_initial_qvector_dfs(std::map<Task*, boost::rational<uint32_t>> &qvector);
private:
    
    /*!
     * \brief Helper function to compute the repetition vector
     * 
     * \param Qdone list of visited tasks
     * \param current the current task to visit
     * \param qvector the repetition vector
     */
    void compute_qvector_dfs_aux(std::vector<Task*> &Qdone, Task*current, std::map<Task*, boost::rational<uint32_t>> &qvector);
};

/*!
 * \remark Register the QVector class as an analysis with config id
 * "repetition_vector"
 */ 
REGISTER_ANALYSE(QVector, "qvector");

#endif

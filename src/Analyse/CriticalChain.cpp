/*!
 * \file Deadlock.cpp
 * \copyright GNU Public License v3.
 * \date 2019 
 * \author Benjamin Rouxel <benjamin.rouxel@uva.nl>, University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "CriticalChain.hpp"

using namespace std;

IMPLEMENT_HELP(CriticalChain,
        Compute the critical chain
)

CriticalChain::CriticalChain() : Analyse() {}

void CriticalChain::forward_params(const map<string,string>&) {}

const string CriticalChain::get_uniqid_rtti() const { return "analyse-critical_chain"; }

/*! \remark depends on #QVectorFlatten or QVectorFlattenSplitConnectors if the graph is not homogeneous
 */
void CriticalChain::check_dependencies() {
    if(sched->status() != ScheduleProperties::sched_statue_e::SCHED_COMPLETE)
        throw CompilerException("Critical Chain", "A schedule must have been completed");
}

void CriticalChain::do_analyse() {
    Schedule copy_sched = *sched;
    rem_unsched_task_dependencies(&copy_sched);
    add_sched_dependencies(&copy_sched);
    Utils::DEBUG("Extra dependencies added");
    
    vector<SchedElt*> dag = copy_sched.scheduled_elements();
    timinginfos_t latest_f = 0;
    BFS(&dag, [this, &latest_f](SchedElt *node) {
        timinginfos_t est = 0;
        for(SchedElt *elp : node->previous()) {
            if(est < earliest[elp].second)
                est = earliest[elp].second;
        }
        earliest[node] = make_pair(est, est+node->wct());
        if(latest_f < est+node->wct())
            latest_f = est+node->wct();
    });
    Utils::DEBUG("BFS done -- ERT computed");
    rBFS(&dag, [this, latest_f](SchedElt *node) {
        timinginfos_t lft = latest_f;
        for(SchedElt *elp : node->successors()) {
            if(lft > latest[elp].first) {
                lft = latest[elp].first;
            }
        }
        latest[node] = make_pair(lft-node->wct(), lft);
    });
    Utils::DEBUG("rBFS done -- LFT computed");
//    for(SchedElt *t : dag) {
//        Utils::DEBUG("-> "+to_string(earliest[t].first)+"/"+to_string(latest[t].first)+" -- "+t->schedtask()->task()->id()+"("+to_string(t->wct())+")"+" -- "+to_string(earliest[t].second)+"/"+to_string(latest[t].second));
//    }
    
    build_critical_path(dag);
    
    string msg = "";
    for(SchedElt *t : critical_chain) 
        msg += "-> "+t->schedtask()->task()->id()+" ("+to_string(t->rt())+"-"+to_string(t->rt()+t->wct())+") ";
    Utils::DEBUG("Critical chain: "+msg);
}

void CriticalChain::build_critical_path(const vector<SchedElt*> &graph) {
    for(SchedElt *elt : graph) {
        if((latest[elt].second - earliest[elt].second) == 0)
            critical_chain.push_back(elt);
    }
    
    sort(critical_chain.begin(), critical_chain.end(), [](SchedElt *a, SchedElt *b) {return a->rt() < b->rt(); });
}

static bool sort_Q_byWCET(SchedElt *a, SchedElt *b) {
    return a->wct() > b->wct();
}

/*!\brief Helper function for BFS
 * 
 * Auxiliary function for the BFS algorithm
 * 
 * \param visited list of visited tasks
 * \param current the current task to visit
 * \param Qready list of next to visit tasks
 */
void CriticalChain::doBFS_aux(vector<SchedElt*> &visited, SchedElt *current, vector<SchedElt*> *Qready, std::function<void(SchedElt*)> func) {
    vector<SchedElt*> Qtmp;
    
    if(find(visited.begin(), visited.end(), current) != visited.end())
        return;
    visited.push_back(current);

    for(SchedElt* s : current->successors()) {
        if(find(visited.begin(), visited.end(), s) != visited.end())
            continue;
        bool ok = true;
        for(SchedElt *elp : s->previous()) {
            if(find(visited.begin(), visited.end(), elp) == visited.end()) {
                ok = false;
                break;
            }
        }
        if(ok)
            Qtmp.push_back(s);
    }
    sort(Qtmp.begin(), Qtmp.end(), sort_Q_byWCET);
    Qready->insert(Qready->end(), Qtmp.begin(), Qtmp.end());

    func(current);
}

/*! \brief Walk through a graph in a BFS manner
 * 
 * \param Qready list of next to visit tasks
 * \param visited list of visited tasks
 */
void CriticalChain::doBFS(vector<SchedElt*> *Qready, vector<SchedElt*> &visited, std::function<void(SchedElt*)> func) {
    vector<SchedElt*> nextQready;
    for(SchedElt *root : *Qready) {
        doBFS_aux(visited, root, &nextQready, func);
    }
    if(nextQready.empty()) {
        return;
    }

    doBFS(&nextQready, visited, func);
    Qready->insert(Qready->end(), nextQready.begin(), nextQready.end());
}

void CriticalChain::BFS(std::vector<SchedElt*> *graph, std::function<void(SchedElt*)> func) {
    vector<SchedElt*> roots;
    for(SchedElt *t : *graph) {
        if(t->previous().size() == 0)
            roots.push_back(t);
    }
    vector<SchedElt*> visited;
    doBFS(&roots, visited, func);
}

/*!\brief Helper function for BFS
 * 
 * Auxiliary function for the BFS algorithm
 * 
 * \param visited list of visited tasks
 * \param current the current task to visit
 * \param Qready list of next to visit tasks
 */
void CriticalChain::dorBFS_aux(vector<SchedElt*> &visited, SchedElt *current, vector<SchedElt*> *Qready, std::function<void(SchedElt*)> func) {
    vector<SchedElt*> Qtmp;
    
    if(find(visited.begin(), visited.end(), current) != visited.end())
        return;
    visited.push_back(current);

    for(SchedElt *p : current->previous()) {
        if(find(visited.begin(), visited.end(), p) != visited.end())
            continue;
        bool ok = true;
        for(SchedElt *els : p->successors()) {
            if(find(visited.begin(), visited.end(), els) == visited.end()) {
                ok = false;
                break;
            }
        }
        if(ok)
            Qtmp.push_back(p);
    }
    sort(Qtmp.begin(), Qtmp.end(), sort_Q_byWCET);
    Qready->insert(Qready->end(), Qtmp.begin(), Qtmp.end());

    func(current);
}

/*! \brief Walk through a graph in a BFS manner
 * 
 * \param Qready list of next to visit tasks
 * \param visited list of visited tasks
 */
void CriticalChain::dorBFS(vector<SchedElt*> *Qready, vector<SchedElt*> &visited, std::function<void(SchedElt*)> func) {
    vector<SchedElt*> nextQready;
    for(SchedElt *root : *Qready) {
        dorBFS_aux(visited, root, &nextQready, func);
    }
    if(nextQready.empty()) {
        return;
    }

    dorBFS(&nextQready, visited, func);
    Qready->insert(Qready->end(), nextQready.begin(), nextQready.end());
}

void CriticalChain::rBFS(std::vector<SchedElt*> *graph, std::function<void(SchedElt*)> func) {
    vector<SchedElt*> sinks;
    for(SchedElt *t : *graph) {
        if(t->successors().size() == 0)
            sinks.push_back(t);
    }
    vector<SchedElt*> visited;
    dorBFS(&sinks, visited, func);
}

void CriticalChain::add_sched_dependencies(Schedule *s) {
    for(SchedCore *c : s->schedcores()) {
        auto elts = s->scheduled_elements(c);
        if(boost::size(elts) < 2) continue;
        
        for(auto it=elts.begin(), et=elts.end() ; std::next(it) != et ; ++it) {
            SchedElt *src = *it;
            SchedElt *sink = *(std::next(it));
            src->add_successor(sink);
        }
    }
}

void CriticalChain::rem_unsched_task_dependencies(Schedule *s) {
    vector<SchedElt*> schedelts = s->scheduled_elements();
    for(SchedElt *j : schedelts) {
        vector<SchedElt*> to_del;
        for(SchedElt *s : j->successors()) {
            if(find(schedelts.begin(), schedelts.end(), s) == schedelts.end()) {
                to_del.push_back(s);
            }
        }
        for(SchedElt* s : to_del) {
            j->rem_successor(s);
        }
    }
}
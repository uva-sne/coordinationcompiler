/*!
 * \file EdgeTypeCheck.hpp
 * \copyright GNU Public License v3.
 * \date 2019 
 * \author Benjamin Rouxel <benjamin.rouxel@uva.nl>, University of Amsterdam
 * \author Stephen Nicholas Swatman <s.n.swatman@uva.nl> University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef EDGETYPECHECK_H
#define EDGETYPECHECK_H

#include "Analyse.hpp"

/**
 * @brief Find discrepancies in the types of connectors in the task graph.
 *
 * Connectors in our task graph model consist of a token type and a token
 * count. To ensure that our task graph can fit together correctly, we must
 * ensure that any two connected connectors share the same token type. This
 * pass analysis a task graph and reports an error if there are any connectors
 * which do not satisfy this property. In essence, this process is similar to
 * type checking in any other compiler; an input connector of type A can only
 * be satisfied by an output connector of type A, not any other type.
 *
 * @remark It is explicitly not an error for a pair of connectors to have
 * differing token counts. This can be checked with a deadlock analysis.
 *
 * @remark Unlike most other analyses, the deadlock analysis does not have a
 * flag to configure its failure behaviour. It never fails silently and always
 * throws an exception. This prevents generating wrong code.
 * 
 * \see #EdgeTypeCheck::help
 */
class EdgeTypeCheck : public Analyse {
public:
    //! \brief Constructor
    EdgeTypeCheck();
    const std::string get_uniqid_rtti() const override;
    
    virtual std::string help() override;
    
    void forward_params(const std::map<std::string,std::string>&) override;
    
protected:
    void do_analyse() override;
};

/*!
 * \remark Register the EdgeTypeCheck class as an analysis with config id
 * "edge_type_check"
 */ 
REGISTER_ANALYSE(EdgeTypeCheck, "edge_type_check");

#endif

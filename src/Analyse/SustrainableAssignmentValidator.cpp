/*
 * Copyright (C) 2023 Lukas Miedema <l.miedema@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "SustrainableAssignmentValidator.hpp"


using E = LabeledCompilerException<C_STR("sustrainable")>;

IMPLEMENT_HELP(SustrainableAssignmentValidator, Validate that the input TeamPlay program matches one expected for the Sustrainable summer school assignment
    - assignment [4,6] the assignment to validate (either 4 or 6))

void SustrainableAssignmentValidator::do_analyse() {
    std::cout << "=== Assignment validation result for assignment " << this->_assignmentNumber << " ===" << std::endl;
    printCriteria(tg->global_deadline() == Utils::stringtime_to_time("1s", tg->time_unit()), 0, "Deadline is specified correctly");

    switch (this->_assignmentNumber) {
        case 4: validateAssignment4(); break;
        case 6: validateAssignment6(); break;
    }
}

void SustrainableAssignmentValidator::forward_params(const std::map<std::string, std::string> &map) {
    if (!map.count("assignment"))
        throw E("no 'assignment' attribute specified");
    this->_assignmentNumber = std::stoi(map.at("assignment"));
    if (_assignmentNumber != 4 && _assignmentNumber != 6)
        throw E("invalid assignment number, only 4 and 6 can be validated");
}

void SustrainableAssignmentValidator::printCriteria(bool criteria, int indent, const std::string &message) {
    std::string prefix;
    for (int i = 0; i < indent; i++)
        prefix += "  ";
    if (criteria)
        std::cout << prefix << "[PASS] ";
    else
        std::cout << prefix << "[FAIL] ";
    std::cout << message << std::endl;
}

bool assertTokenType(Task *from, Task *to, const std::string& expected, dataqty_t tokensIn = 1, dataqty_t tokensOut = 1) {
    if (from->successor_tasks().count(to) == 0)
        return false;
    auto connection = from->successor_tasks()[to][0];
    if (connection->from()->tokens() != tokensIn || connection->to()->tokens() != tokensOut)
        return false;
    return connection->from()->token_type() == expected && connection->to()->token_type() == expected;
}

void SustrainableAssignmentValidator::validateAssignment4() {
    printCriteria(tg->tasks().size() == 3, 0, "Correct number of components are specified");
    auto apc = tg->tasks().find("AccessPointsCollector");
    auto slc = tg->tasks().find("StudentsLocator");
    auto aa = tg->tasks().find("Alarm");
    auto end = tg->tasks().end();
    printCriteria(apc != end, 1, "AccessPointsCollector exists");
    printCriteria(slc != end, 1, "StudentsLocator exists");
    printCriteria(aa != end, 1, "Alarm exists");
    if (apc != end && slc != end && aa != end) {
        auto apcSucc = Utils::mapkeys(apc->second->successor_tasks());
        auto slcSucc = Utils::mapkeys(slc->second->successor_tasks());
        auto aaSucc = Utils::mapkeys(aa->second->successor_tasks());
        printCriteria(apcSucc.size() == 1 && apcSucc[0] == slc->second, 1, "AccessPointsCollector has only a link to StudentsLocator");
        printCriteria(slcSucc.size() == 1 && slcSucc[0] == aa->second, 1, "StudentsLocator has only a link to Alarm");
        printCriteria(assertTokenType(apc->second, slc->second, "signal_strength_table") && assertTokenType(slc->second, aa->second, "location_table"), 1, "All token types and quantities are correct");
    }
}

void SustrainableAssignmentValidator::validateAssignment6() {
    printCriteria(tg->tasks().size() == 7, 0, "Correct number of components are specified");
    auto apc_start = tg->tasks().find("AccessPointCollectorStart");
    auto apc_main = tg->tasks().find("AccessPointCollector");
    auto apc_end = tg->tasks().find("AccessPointCollectorEnd");
    auto slc_start = tg->tasks().find("StudentLocatorStart");
    auto slc_main = tg->tasks().find("StudentLocator");
    auto slc_end = tg->tasks().find("StudentLocatorEnd");
    auto aa = tg->tasks().find("Alarm");

    auto end = tg->tasks().end();

    printCriteria(apc_start != end, 1, "AccessPointCollectorStart exists");
    printCriteria(apc_main != end, 2, "AccessPointCollector exists");
    printCriteria(apc_end != end, 2, "AccessPointCollectorEnd exists");
    printCriteria(slc_start != end, 1, "StudentLocatorStart exists");
    printCriteria(slc_main != end, 2, "StudentLocator exists");
    printCriteria(slc_end != end, 2, "StudentLocatorEnd exists");
    printCriteria(aa != end, 1, "Alarm exists");

    if (apc_start != end && apc_main != end && apc_end != end && slc_start != end && slc_main != end && slc_end != end && aa != end) {
        auto slcEndSucc = Utils::mapkeys(slc_end->second->successor_tasks());

        printCriteria(assertTokenType(apc_start->second, apc_main->second, "void", 7, 1), 1, "AccessPointCollectorStart output -> 7:1");
        printCriteria(assertTokenType(apc_main->second, apc_end->second, "signal_strength_item", 1, 7), 1, "AccessPointCollector output -> 1:7");
        printCriteria(assertTokenType(apc_end->second, slc_start->second, "signal_strength_table", 1, 1), 1, "AccessPointCollectorEnd output -> 1:1");

        printCriteria(assertTokenType(slc_start->second, slc_main->second, "signal_strength_table", 30, 1), 1, "StudentLocatorStart output -> 30:1");
        printCriteria(assertTokenType(slc_main->second, slc_end->second, "location_item", 1, 30), 1, "StudentLocator output -> 1:30");
        printCriteria(assertTokenType(slc_end->second, aa->second, "location_table", 1, 1), 1, "StudentLocatorEnd output -> 1:1");
    }
}

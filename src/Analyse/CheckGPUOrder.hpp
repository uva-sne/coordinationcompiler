/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   CheckGPUOrder.hpp
 * Author: brouxel
 *
 * Created on 9 novembre 2021, 16:10
 */

#ifndef CHECKGPUORDER_HPP
#define CHECKGPUORDER_HPP

#include "Analyse.hpp"

class CheckGPUOrder : public Analyse {
public:
    CheckGPUOrder() : Analyse() {}
    
    virtual void forward_params(const std::map<std::string, std::string> &params) override;
    const std::string get_uniqid_rtti() const override;
    virtual std::string help() override;
protected:
    void check_dependencies() override;
    void do_analyse() override;
};

REGISTER_ANALYSE(CheckGPUOrder, "check-gpu-order")

#endif /* CHECKGPUORDER_HPP */


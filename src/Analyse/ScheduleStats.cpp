/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   NvidiaStats.cpp
 * Author: brouxel
 * 
 * Created on 12 octobre 2021, 14:30
 */

#include "ScheduleStats.hpp"
#include "CriticalPath.hpp"
#include "CriticalChain.hpp"

using namespace std;

IMPLEMENT_HELP(ScheduleStats,
    Extract some statistics from the schedule
)

ScheduleStats::ScheduleStats() : Analyse() {}

void ScheduleStats::forward_params(const map<string,string>&) {}

void ScheduleStats::setAuxilliaryParams(const std::string &tag, const std::map<std::string, std::string> &params) {
    if(tag == "task") {
        if(params.count("id"))
            add_task_stats.push_back(params.at("id"));
        else
            throw CompilerException("Schedule Stats", "Missing `id' on a sub `task' xmltag");
    }
    if(tag == "length") {
        if(!params.count("start"))
            throw CompilerException("Schedule Stats", "Missing `start' on a sub `length' xmltag");
        if(!params.count("end"))
            throw CompilerException("Schedule Stats", "Missing `end' on a sub `length' xmltag");
        add_length_stats.push_back(make_pair(params.at("start"), params.at("end")));
    }
}

const string ScheduleStats::get_uniqid_rtti() const { return "analyse-schedule-stats"; }

void ScheduleStats::check_dependencies() {
    if (conf->passes.find_pass_before([](const CompilerPass *p) -> bool {
        return p->get_uniqid_rtti().find("simulator") != string::npos || p->get_uniqid_rtti().find("solver") != string::npos; }, this)) {
        throw CompilerException("Schedule Stats", "You need to have a simulator/scheduler simulator call before getting the stats");
    }
}

void ScheduleStats::do_analyse() {
    sched->stats("#nodes scheduled", to_string(sched->scheduled_elements().size()));
    
    globtiminginfos_t tot_idle_time = 0;
    float tot_util = 0.0f;
    for(SchedCore *c : sched->schedcores()) {
        auto elts = sched->scheduled_elements(c);
        globtiminginfos_t exect = accumulate(elts.begin(), elts.end(), (globtiminginfos_t)0, [](globtiminginfos_t acc, SchedElt *a) {return acc+a->wct();});
        auto it = max_element(elts.begin(), elts.end(), [](SchedElt *a, SchedElt *b) {return a->rt()+a->wct() < b->rt()+b->wct();});
        globtiminginfos_t endtime = (it != elts.end()) ? (*it)->rt()+(*it)->wct() : 0;
        
        float util = (endtime > 0) ? exect/(float)endtime : 0.0f;
        tot_util += util;
        tot_idle_time += endtime-exect;
        
        sched->stats("#nodes on "+c->core()->id, to_string(boost::size(elts)));
        sched->stats("utilisation "+c->core()->id, to_string(util));
        sched->stats("idle time "+c->core()->id, Utils::nstime_to_string(endtime-exect, "ms")+" ("+to_string(endtime-exect)+" ns");
        sched->stats("last completion on "+c->core()->id, Utils::nstime_to_string(endtime, "ms")+" ("+to_string(endtime)+"ns)");
    }
    sched->stats("total utilisation", to_string(tot_util));
    sched->stats("total idle time", Utils::nstime_to_string(tot_idle_time, "ms")+" ("+to_string(tot_idle_time)+"ns)");
    
    map<string, size_t> cnt;
    
    for(string lbl : add_task_stats)
        cnt[lbl] = 0;
    
    map<size_t, vector<timinginfos_t>> starts, ends;
    for(SchedElt *e : sched->scheduled_elements()) {
        for(string lbl : add_task_stats) {
            if(e->schedtask()->task()->id().find(lbl) != string::npos) {
                ++cnt[lbl];
            }
        }
        size_t i = 0;
        for(pair<string, string> el : add_length_stats) {
            if(e->schedtask()->task()->id().find(el.first) != string::npos) {
                starts[i].push_back(e->rt());
            }
            if(e->schedtask()->task()->id().find(el.second) != string::npos) {
                ends[i].push_back(e->rt()+e->wct());
            }
            ++i;
        }
    }
    
    for(string lbl : add_task_stats)
        sched->stats("#"+lbl, to_string(cnt[lbl]));
    
//    sched->stats("", "");
//    for (Task *t : tg->tasks_it()) {
//        sched->stats(t->id(), to_string(sched->get_scheduled_elts(t).size()));
//    }
    
    size_t cnt_overrun = 0, cnt_underrun = 0, cnt_exactrun = 0;
    for(SchedElt *st : sched->scheduled_elements()) {
        if(st->wct() > st->schedtask()->task()->C())
            ++cnt_overrun;
        else if(st->wct() < st->schedtask()->task()->C())
            ++cnt_underrun;
        else
            ++cnt_exactrun;
    }
    sched->stats("#nodes in overrun", to_string(cnt_overrun)+" ("+to_string(cnt_overrun/(float)sched->scheduled_elements().size()*100)+"%)");
    sched->stats("#nodes in underrun", to_string(cnt_underrun)+" ("+to_string(cnt_underrun/(float)sched->scheduled_elements().size()*100)+"%)");
    sched->stats("#nodes in wcetrun", to_string(cnt_exactrun)+" ("+to_string(cnt_exactrun/(float)sched->scheduled_elements().size()*100)+"%)");
    
    CriticalPath *pass_critic_path = nullptr;
    CriticalChain *pass_critic_chain = nullptr;
    for (auto &p : conf->passes.passes()) {
        if(p->get_uniqid_rtti() == "analyse-critical_path") {
            pass_critic_path = dynamic_cast<CriticalPath *>(p.get());
        }
        else if(p->get_uniqid_rtti() == "analyse-critical_chain") {
            pass_critic_chain = dynamic_cast<CriticalChain *>(p.get());
        }
    }
    
    if(pass_critic_path != nullptr) {
        vector<Task*> critical_path = pass_critic_path->get_critical_path();
        sched->stats("#nodes Critical path", to_string(critical_path.size()));
        timinginfos_t cp_length = accumulate(critical_path.begin(), critical_path.end(), (timinginfos_t)0, [](timinginfos_t acc, Task* a) { return acc+a->C();});
        sched->stats("Critical path length", Utils::nstime_to_string(cp_length, "ms")+" ("+to_string(cp_length)+"ns)");
//        string msg = "";
//        for(Task *t : critical_path) 
//            msg += "-> "+t->id()+" ";
//        sched->stats("Critical path", msg);
    }
    if(pass_critic_chain != nullptr) {
        vector<SchedElt*> critical_chain = pass_critic_chain->get_critical_chain();
        sched->stats("#nodes Critical chain", to_string(critical_chain.size()));
        timinginfos_t cc_length = accumulate(critical_chain.begin(), critical_chain.end(), (timinginfos_t)0, [](timinginfos_t acc, SchedElt* a) { return acc+a->wct();});
        sched->stats("Critical chain length", Utils::nstime_to_string(cc_length, "ms")+" ("+to_string(cc_length)+"ns)");
        size_t gpu_task = 0;
        timinginfos_t gpu_length = 0;
        for(SchedElt *elt : critical_chain) {
            if(elt->schedtask()->task()->id().find("submittee") != string::npos) {
                ++gpu_task;
                gpu_length += elt->wct();
            }
        }
        sched->stats("#GPU nodes in the Critical chain", to_string(gpu_task));
        sched->stats("Critical chain length on GPU", Utils::nstime_to_string(gpu_length, "ms")+" ("+to_string(gpu_length)+"ns)");
//        string msg = "";
//        for(SchedElt *t : critical_chain) 
//            msg += "-> "+t->schedtask()->task()->id()+" ("+to_string(t->rt())+"-"+to_string(t->rt()+t->wct())+") ";
//        sched->stats("Critical chain", msg);
    }
    
    size_t i = 0;
    for(pair<string, string> el : add_length_stats) {
        if(starts[i].size() != ends[i].size())
            Utils::WARN("Different amount of data for the exec length between "+el.first+" and "+el.second);
        
        sort(starts[i].begin(), starts[i].end());
        sort(ends[i].begin(), ends[i].end());
        
        for(size_t j= 0 ; j < starts[i].size() && j < ends[i].size() ; ++j) {
            timinginfos_t exec = ends[i][j] - starts[i][j];
            sched->stats(el.second+" - "+el.first+" #"+to_string(j), Utils::nstime_to_string(exec, "ms")+" ("+to_string(exec)+"ns)");
        }
        
        ++i;
    }
}

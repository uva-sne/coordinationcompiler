/*
 * Copyright (C) 2023 Lukas Miedema <l.miedema@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include "Analyse.hpp"

/**
 * The validator is designed for the Sustrainable Summer School taught in summer 2023
 * Students model an application in TeamPlay, and this validator asserts their modeling is correct.
 * There is some liberties students are allowed to take, such as choosing the names of the inports/outports, but
 * other than that the structure that it validates is quite rigid.
 *
 * The associated assigments are in the examples/sustrainable directory.
 */
class SustrainableAssignmentValidator : public Analyse {
private:
    int _assignmentNumber;
    void printCriteria(bool, int indent, const std::string&);

protected:
    void do_analyse() override;

    void forward_params(const std::map<std::string, std::string> &map) override;

public:
    const std::string get_uniqid_rtti() const override {
        return "analyse-sustrainable";
    }

    std::string help() override;

    void validateAssignment4();

    void validateAssignment6();
};

REGISTER_ANALYSE(SustrainableAssignmentValidator, "sustrainable");




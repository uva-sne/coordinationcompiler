/*
 * Copyright (C) 2020 Lukas Miedema <l.miedema@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SCHEDULEVALIDATIONANALYZER_H
#define SCHEDULEVALIDATIONANALYZER_H


#include "Analyse.hpp"
#include "Schedule.hpp"

/**
 * Schedule validator analysis pass, which checks if the schedule is well-formed to help detect logic errors
 * in the various solvers. More specifically, it checks:
 * - that a schedule is computed
 * - that every task is scheduled
 * - that the last task finishes at or before the makespan
 * - that no two tasks are scheduled on the same core at the same time
 * - that all tasks are scheduled after their dependencies are completed
 *
 * \see ScheduleValidationAnalyzer::help
 * \see SummaryViewer
 */
class ScheduleValidationAnalyzer : public Analyse {
public:
    /**
     * Report function callback.
     * \see ScheduleValidator::do_analysis
     */
   typedef std::function<void(bool&, bool, const std::string&)> ReportFunction;
   
   /**
    * Scheduling error indicating two tasks scheduled on the same core at the same time.
    * One entry will be generated for each such occurrence in the schedule.
    * \see ScheduleValidator::show_results
    */
   struct OverlappingTask {
       SchedCore *core; //!< Core on which the two taks overlap
       SchedElt *element0; //!< Overlapping element
       SchedElt *element1; //!< Overlapping element

       /**
        * Construct a new OverlappingTask scheduling error.
        * @param core
        * @param element0
        * @param element1
        */
       explicit OverlappingTask(SchedCore *core, SchedElt *element0, SchedElt *element1) :
           core(core),
           element0(element0),
           element1(element1) {}
   };

   /**
    * Scheduling error indicating a task was scheduled before its dependencies finished.
    * One entry will be generated for each such occurrence in the schedule.
    * \see ScheduleValidator::show_results
    */
   struct UnsatisfiedDependency {
       const Task *element; //!< Element that is missing a dependency (predecessor) when it is started
       const Task *missing_predecessor; //!< The missing predecessor that may not yet have finished
       const std::string message; //!< Message

       /**
        * Construct a new UnsatisfiedDependency scheduling error.
        * @param element
        * @param missing_predecessor
        * @param message
        */
       explicit UnsatisfiedDependency(const Task *element, const Task *missing_predecessor, const std::string message) :
           element(element), missing_predecessor(missing_predecessor), message(message) {}
   };

   /**
    * Scheduling error indicating the WCET set on the SchedElt does not match that of the selected version.
    */
   struct VersionWCETMismatch {
       const Task *element;
       timinginfos_t actualWCET; // <! reported on SchedElt
       timinginfos_t versionWCET;

       explicit VersionWCETMismatch(const Task *element, timinginfos_t actualWcet, timinginfos_t versionWcet) : element(
               element), actualWCET(actualWcet), versionWCET(versionWcet) {}
   };

    /**
     * Scheduling error indicating the task is assigned to a forbidden processor.
     */
    struct IllegalArchitectureAssignment {
        const Task *element;
        Version *version;
        std::vector<std::string> supported_processors; // <! supported architectures as per the version
        std::string actual_processor;

        explicit IllegalArchitectureAssignment(const Task *element, Version *version,
                const std::vector<std::string> &supportedProcessors, const std::string &actualArchitecture)
                : element(element), version(version), supported_processors(supportedProcessors),
                actual_processor(actualArchitecture) {}
    };



   /**
    * Validation result summarizing the state of the schedule, including structured information about each error.
    * \see ScheduleValidator::show_results
    */
   struct ValidationResult {
       bool has_schedule = false; //!< true iff there is any schedule
// see comment in do_analyse
//       bool all_tasks_scheduled = false; //!< true iff the schedule contains all tasks
       std::vector<OverlappingTask> overallocated_tasks; //!< vec of overallocated tasks, empty iff no overallocated tasks
       std::vector<UnsatisfiedDependency> tasks_with_unsatisfied_dependencies; //!< vec of tasks with unsatisfied dependencies
       std::vector<SchedElt *> tasks_without_assigned_cores; //!< vec of tasks without core assignments
       std::vector<SchedElt *> tasks_without_versions; //!< vec of tasks without selected (and existing) versions
       std::vector<VersionWCETMismatch> wcet_mismatches; //!< vec of tasks with incorrect WCETs
       std::vector<IllegalArchitectureAssignment> tasks_with_illegal_architectures; //!< vec of tasks+versions assigned to processors that they do not support

       uint64_t reported_makespan = 0; //!< makespan from the schedule
       uint64_t actual_makespan = 0; //!< max(rt[i] + wct[i]) for all i
   };
   
private:
    bool throw_error = false; //!< set to true to throw an error when the schedule is found to be invalid.
    ReportFunction _report_function = nullptr; //!< Configurable function to display results, useful to switch between console and file

protected:
    void do_analyse() override;
    const std::string get_uniqid_rtti() const override;
    std::string help() override;
    
    /**
     * Display the results
     * @param result
     */
    void show_results(ValidationResult *result);
    
public:
    ScheduleValidationAnalyzer() : Analyse() {
        _report_function = ScheduleValidationAnalyzer::show_results_property;
    }
    void check_dependencies() override;
    void forward_params(const std::map<std::string, std::string> &map) override;
    
    ACCESSORS_RW(ScheduleValidationAnalyzer, ReportFunction, report_function)
            
    /**
     * Generic function to print the result of a property to the console
     * 
     * @param all_ok True if all given properties have been ok so far
     * @param property_ok True if the property to check is ok
     * @param label Label to print for the checked property
     */
    static void show_results_property(bool &all_ok, bool property_ok, const std::string &label);
};

REGISTER_ANALYSE(ScheduleValidationAnalyzer, "sched-validator");


#endif //SCHEDULEVALIDATIONANALYZER_H

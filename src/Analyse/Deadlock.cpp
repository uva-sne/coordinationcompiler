/*!
 * \file Deadlock.cpp
 * \copyright GNU Public License v3.
 * \date 2019 
 * \author Benjamin Rouxel <benjamin.rouxel@uva.nl>, University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "Deadlock.hpp"

using namespace std;

IMPLEMENT_HELP(DeadlockCheck,
    Check if there is a possibility of deadlock in the 
    production/consumption of data between tasks within a graph.
)

DeadlockCheck::DeadlockCheck() : Analyse() {}

void DeadlockCheck::forward_params(const map<string,string>&) {}

const string DeadlockCheck::get_uniqid_rtti() const { return "analyse-deadlock"; }

/*! \remark depends on #QVectorFlatten or QVectorFlattenSplitConnectors if the graph is not homogeneous
 */
void DeadlockCheck::check_dependencies() {
    if (!conf->passes.find_pass_before([](const CompilerPass *p) {
        return p->get_uniqid_rtti() == "transform-homogeneise" || p->get_uniqid_rtti() == "transform-homogeneise_splitters";
    }, this)) {
        Utils::WARN("Deadlock checking requires the computation of the repetition vector first to properly work");
        Utils::WARN("This can be ignored if the graph is already homogeneous");
    }
}

#define GET_NB_REPEAT(T) (T->repeat() > 0 ? T->repeat() : 1)

void DeadlockCheck::do_analyse() {
    for(Task *tsrc : tg->tasks_it()) {
        vector<Connector*> allinconn = Utils::flatmapvalues(tsrc->predecessor_tasks(), Task::connector_to);
        for(Connector *el : tsrc->inputs()) {
            if(!is_connector_used(allinconn, el))
                throw CompilerException("deadlock-check", "Unused input connector "+el->id()+" for task "+tsrc->id());
        }
        vector<Connector*> alloutconn = Utils::flatmapvalues(tsrc->successor_tasks(), Task::connector_from);
        for(Connector *el : tsrc->outputs()) {
            if(!is_connector_used(alloutconn, el))
                throw CompilerException("deadlock-check", "Unused output connector "+el->id()+" for task "+tsrc->id());
        }
        
        for(Connector *csink : tsrc->inputs()) {
            uint32_t nbtoken_sent = 0;
            for(Task::dep_t el : tsrc->predecessor_tasks()) {
                for(size_t i = 0; i < el.second.size(); i++) {
                    Connection *conn = el.second[i];
                    if (conn->to() != csink) continue;
                    nbtoken_sent += el.first->eff_suc_send()[tsrc][i] * GET_NB_REPEAT(el.first);
                }
            }
            if(csink->tokens()*GET_NB_REPEAT(tsrc) > nbtoken_sent)
                throw CompilerException("deadlock-check", "Deadlock detected to spawn "+tsrc->id()+", receives "+to_string(nbtoken_sent)+" but expect "+to_string(csink->tokens()*GET_NB_REPEAT(tsrc)));
        }
        
        for(Connector *csrc : tsrc->outputs()) {
            uint32_t nbtoken_rec = 0;
            for(Task::dep_t el : tsrc->successor_tasks()) {
                for(size_t i = 0; i < el.second.size(); i++) {
                    Connection *conn = el.second[i];
                    if (conn->from() != csrc) continue;
                    nbtoken_rec += el.first->eff_prev_rec()[tsrc][i] * GET_NB_REPEAT(el.first);
                }
            }
            if(csrc->tokens()*GET_NB_REPEAT(tsrc) > nbtoken_rec)
                throw CompilerException("deadlock-check", "Deadlock detected to spawn successors of "+tsrc->id()+", receives "+to_string(nbtoken_rec)+" but expect "+to_string(csrc->tokens()*GET_NB_REPEAT(tsrc)));
        }
    }
}

bool DeadlockCheck::is_connector_used(const vector<Connector*> &allconn, Connector *test) {
    if(find(allconn.begin(), allconn.end(), test) != allconn.end())
        return true;
    
    return false;
}

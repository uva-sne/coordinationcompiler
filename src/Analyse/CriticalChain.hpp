/*!
 * \file CriticalChain.hpp
 * \copyright GNU Public License v3.
 * \date 2019 
 * \author Benjamin Rouxel <benjamin@lexuor.net>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CRITICALCHAIN_HPP
#define CRITICALCHAIN_HPP

#include "Analyse.hpp"

/**
 * @brief Determines 
 *
 * \see #CriticalChain::help
 */
class CriticalChain : public Analyse {
public:
    void forward_params(const std::map<std::string, std::string> &args) override;
    const std::string get_uniqid_rtti() const override;
    CriticalChain();
    
    std::vector<SchedElt*> get_critical_chain() { return critical_chain; }

protected:
    void check_dependencies() override;
    void do_analyse() override;
    
private: 
    std::vector<SchedElt*> critical_chain;
    std::map<SchedElt*, std::pair<timinginfos_t, timinginfos_t>> earliest;
    std::map<SchedElt*, std::pair<timinginfos_t, timinginfos_t>> latest;
    
    virtual std::string help() override;
    
    void build_critical_path(const std::vector<SchedElt*> &graph);
    void doBFS_aux(std::vector<SchedElt*> &visited, SchedElt *current, std::vector<SchedElt*> *Qready, std::function<void(SchedElt*)> func);
    void doBFS(std::vector<SchedElt*> *Qready, std::vector<SchedElt*> &visited, std::function<void(SchedElt*)> func);
    void BFS(std::vector<SchedElt*> *graph, std::function<void(SchedElt*)> func);
    void dorBFS_aux(std::vector<SchedElt*> &visited, SchedElt *current, std::vector<SchedElt*> *Qready, std::function<void(SchedElt*)> func);
    void dorBFS(std::vector<SchedElt*> *Qready, std::vector<SchedElt*> &visited, std::function<void(SchedElt*)> func);
    void rBFS(std::vector<SchedElt*> *graph, std::function<void(SchedElt*)> func);
    void add_sched_dependencies(Schedule *s);
    void rem_unsched_task_dependencies(Schedule *s);

}; 

/*!
 * \remark Register the CriticalChain class as an analysis with config id
 * "cycle"
 */ 
REGISTER_ANALYSE(CriticalChain, "critical-chain")

#endif /* CRITICALCHAIN_HPP */


/*!
 * \file ComputeUtilization.hpp
 * \copyright GNU Public License v3.
 * \date 2020 
 * \author Benjamin Rouxel <benjamin.rouxel@uva.nl>, University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef COMPUTEUTILIZATION_H
#define COMPUTEUTILIZATION_H

#include <string>
#include <map>
#include <vector>

#include "Analyse.hpp"
#include "Schedule.hpp"

/**
 * @brief Roughly checks whether the core utilisation of a scheduling is feasible.
 *
 * When a scheduling is performed for a task graph, we would like to know
 * whether it is feasible in terms of core utilisation, as well as total
 * system utilisation. This pass analysis the utilisation of a scheduled task
 * graph, and reports errors if the utilisation ever exceeds the maximum.
 *
 * We define the utilisation of a task as the ratio between its worst-case
 * execution time and its period. Then the sum of these ratios must not exceed
 * some limit. For the system as a whole, this limit is equal to the number of
 * cores in the target architecture. For each core, this limit is always equal
 * to 1.
 *
 * @remark The task graph must be mapped to cores for this analysis pass to have
 * any meaningful effect.
 * 
 * \see #ComputeUtilization::help
 */
class ComputeUtilization : public Analyse {
    //! true to throw an exception, false to issue a warning
    bool force = false; 
    
public:
    //! \brief Constructor
    ComputeUtilization();
    void forward_params(const std::map<std::string,std::string>&) override;
    virtual std::string help() override;
    const std::string get_uniqid_rtti() const override;
    
protected:    
    void check_dependencies() override;
    void do_analyse() override;
};

/*!
 * \remark Register the ComputeUtilization class as an analysis with config id
 * "utilisation"
 */ 
REGISTER_ANALYSE(ComputeUtilization, "utilization");

#endif /* COMPUTEUTILIZATION_H */


/*!
 * \file CycleCheck.hpp
 * \copyright GNU Public License v3.
 * \date 2019 
 * \author Benjamin Rouxel <benjamin.rouxel@uva.nl>, University of Amsterdam
 * \author Stephen Nicholas Swatman <s.n.swatman@uva.nl> University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CYCLECHECK_H
#define CYCLECHECK_H

#include "Analyse.hpp"

/**
 * @brief Determines whether a set of nodes contains a cycle.
 *
 * In a directed graph, a cycle is a path that can be taken to get from one
 * node back to itself. Even a single cycle disqualifies a graph from being
 * a DAG (DAGs are acyclic after all), so it can be important for us to
 * detect cycles.
 * 
 * @remark If the transitive closure of the graph has been a priori computed,
 * this analysis is faster.
 * 
 * \see #CycleCheck::help
 */
class CycleCheck : public Analyse {
    //! true to throw an exception, false to issue a warning
    bool forbid_cycle = false; 

public:
    void forward_params(const std::map<std::string, std::string> &args) override;
    const std::string get_uniqid_rtti() const override;
    CycleCheck();

protected:
    void do_analyse() override;
    
private: 
    /*! \brief Fast cycle check
     * 
     * If the transitive closure of the graph has been computed, then it is 
     * faster to check for cycle.
     * 
     * \return true if there is a cycle, false otherwise
     */
    bool cycle_check_fast();
    /*! \brief Cycle check
     * 
     * Walk through graphs to find a cycle
     * 
     * \return true if there is a cycle, false otherwise
     */
    bool cycle_check();
    
    /*! \brief Auxiliary cycle check
     * 
     * Auxiliary recursive function to detect cycles through a graph
     * 
     * \param current the current task to visit
     * \param visited contains the list of visited task within a graph
     * \param glob_visited contains the global list of visited task
     * \return true if there is a cycle, false otherwise
     */
    bool cycle_check_aux(Task *current, std::vector<Task*> *visited, std::vector<Task*> *glob_visited);
    
    virtual std::string help() override;
}; 

/*!
 * \remark Register the CycleCheck class as an analysis with config id
 * "cycle"
 */ 
REGISTER_ANALYSE(CycleCheck, "cycle")

#endif

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   CriticalPath.hpp
 * Author: brouxel
 *
 * Created on 19 octobre 2021, 14:52
 */

#ifndef CRITICALPATH_HPP
#define CRITICALPATH_HPP

#include "Analyse.hpp"

/**
 * @brief Determines 
 *
 * \see #CriticalPath::help
 */
class CriticalPath : public Analyse {
public:
    void forward_params(const std::map<std::string, std::string> &args) override;
    const std::string get_uniqid_rtti() const override;
    CriticalPath();

    std::vector<Task*> get_critical_path() { return critical_path;}
    std::vector<std::vector<Task*>> get_critical_paths() { return critical_paths;}
    std::map<Task*, std::pair<timinginfos_t, timinginfos_t>> get_earliest() { return earliest; }
    std::map<Task*, std::pair<timinginfos_t, timinginfos_t>> get_latest() { return latest; }
protected:
    void do_analyse() override;
    
private: 
    std::vector<std::vector<Task*>> critical_paths;
    std::vector<Task*> critical_path;
    
    std::map<Task*, std::pair<timinginfos_t, timinginfos_t>> earliest;
    std::map<Task*, std::pair<timinginfos_t, timinginfos_t>> latest;

    virtual std::string help() override;
}; 

/*!
 * \remark Register the CriticalPath class as an analysis with config id
 * "cycle"
 */ 
REGISTER_ANALYSE(CriticalPath, "critical-path")

#endif /* CRITICALPATH_HPP */


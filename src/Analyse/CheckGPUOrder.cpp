/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   CheckGPUOrder.cpp
 * Author: brouxel
 * 
 * Created on 9 novembre 2021, 16:10
 */

#include "CheckGPUOrder.hpp"

using namespace std;

const string CheckGPUOrder::get_uniqid_rtti() const {
    return "analyse-check_gpu_order";
}
IMPLEMENT_HELP(CheckGPUOrder,
    Check if the order of the callee on the GPU are in the same order as their
    respective submitters.
)

void CheckGPUOrder::forward_params(const std::map<std::string, std::string> &params) {
}
void CheckGPUOrder::check_dependencies() {
    if (!conf->passes.find_pass_before(
            [](const CompilerPass *p) { return p->get_uniqid_rtti().find("simulator") != string::npos || p->get_uniqid_rtti().find("solver") != string::npos; }, this)) {
        throw CompilerException("Check GPU task order", "You need to have a simulator/scheduler call before getting the stats");
    }
}
    
void CheckGPUOrder::do_analyse() {
    vector<SchedElt*> submitters;
    vector<SchedElt*> submittees;
    for(SchedElt *submittee : sched->scheduled_elements()) {
        string tname = submittee->schedtask()->task()->id();
        if(submittee->schedtask()->task()->type() == TaskProperties::TaskType::TT_SUBMITTEE) {
            SchedElt *submitter = nullptr;
            for(SchedElt *elp : submittee->previous()) {
                if(elp->schedtask()->task()->type() == TaskProperties::TaskType::TT_SUBMITTER && elp->min_rt_period() == submittee->min_rt_period()) {
                    submitter = elp;
                    break;
                }
            }
            if(submitter == nullptr)
                throw CompilerException("Check GPU task order", "Missing submitter for "+tname);
            submitters.push_back(submitter);
            submittees.push_back(submittee);
//            cout << "---> " << submitter->schedtask()->task()->id() << " -- " << submitter->min_rt_period() << " / " << submittee->schedtask()->task()->id() << " -- " << submittee->min_rt_period() << endl;
        }
    }
    
    if(submitters.size() != submittees.size())
        throw CompilerException("Check GPU task order", to_string(submitters.size())+" != "+to_string(submittees.size()));
    
    sort(submitters.begin(), submitters.end(), [](SchedElt *a, SchedElt *b) { return a->rt() < b->rt(); } );
    sort(submittees.begin(), submittees.end(), [](SchedElt *a, SchedElt *b) { return a->rt() < b->rt(); } );
    
    
    
    for(size_t i = 0 ; i < submitters.size() ; ++i) {
        SchedElt* submittern = submitters[i];
        SchedElt* submitteen = submittees[i];
//        cout << "---> " << submittern->schedtask()->task()->id() << "(" << submittern->min_rt_period() << ")" << " -- " << submitters[i]->rt()+submitters[i]->wct() << " -> " << submitteen->schedtask()->task()->id() << "(" << submitteen->min_rt_period() << ")" << " -- " << submittees[i]->rt() << endl;
        
        if(find(submitteen->previous().begin(), submitteen->previous().end(), submittern) == submitteen->previous().end()) {
//            for(SchedElt* s : submitters[i]->successors())
//                cout << "==> " << s->schedtask()->task()->id() << "(" << s->min_rt_period() << ")" << " -- " << s->rt() << endl;
//            cout << " === " << endl;
//            for(SchedElt *p : submittees[i]->previous())
//                cout << "==> " << p->schedtask()->task()->id() << "(" << p->min_rt_period() << ")" << " -- " << (p->rt()+p->wct()) << endl;
//            throw CompilerException("Check GPU task order", "Submittees don't follow the submitters order");
            Utils::ERROR("Submittees don't follow the submitters order");
        }
    }
    Utils::INFO("Submitters and submittess are in sync");
}
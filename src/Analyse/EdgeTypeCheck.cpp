/*!
 * \file EdgeTypeCheck.cpp
 * \copyright GNU Public License v3.
 * \date 2019 
 * \author Benjamin Rouxel <benjamin.rouxel@uva.nl>, University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "EdgeTypeCheck.hpp"

using namespace std;

EdgeTypeCheck::EdgeTypeCheck() : Analyse() {}

IMPLEMENT_HELP(EdgeTypeCheck,
    Check for each edge if source and sink ports types match.
)
void EdgeTypeCheck::forward_params(const map<string,string>&) {}


const string EdgeTypeCheck::get_uniqid_rtti() const { return "analyse-edge_type_check"; }

void EdgeTypeCheck::do_analyse() {
    for(Task *tsrc : tg->tasks_it()) {
        for(Task::dep_t el : tsrc->successor_tasks()) {
            for (Connection *conn : el.second) {
                Connector *csrc = conn->from();
                Task *tsink = el.first;
                Connector *csink = conn->to();
                if (csrc->token_type() != csink->token_type())
                    throw CompilerException("type-check", "Mismatch type for " + *tsrc + "." + csrc->id() + " and " + *tsink + "." + csink->id());
            }
        }
    }
}

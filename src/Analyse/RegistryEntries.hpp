/*!
 * \file RegistryEntries.hpp
 * \copyright GNU Public License v3.
 * \date 2020 
 * \author Stephen Nicholas Swatman <s.n.swatman@uva.nl> University of Amsterdam
 * \author Benjamin Rouxel <benjamin.rouxel@uva.nl>, University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "CheckTiming.hpp"
#include "ComputeUtilization.hpp"
#include "CycleCheck.hpp"
#include "Deadlock.hpp"
#include "EdgeTypeCheck.hpp"
#include "ScheduleValidationAnalyzer.hpp"
#include "ScheduleStats.hpp"
#include "CriticalPath.hpp"
#include "CriticalChain.hpp"
#include "CheckGPUOrder.hpp"
#include "SustrainableAssignmentValidator.hpp"
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   NvidiaStats.hpp
 * Author: brouxel
 *
 * Created on 12 octobre 2021, 14:30
 */

#ifndef NVIDIASTATS_HPP
#define NVIDIASTATS_HPP

#include "Analyse.hpp"
#include "Schedule.hpp"

#include <numeric>

class ScheduleStats : public Analyse {
    std::vector<std::string> add_task_stats;
    std::vector<std::pair<std::string, std::string>> add_length_stats;
public:
    ScheduleStats();
    void forward_params(const std::map<std::string,std::string>&) override;
    const std::string get_uniqid_rtti() const override;
    
    virtual void setAuxilliaryParams(const std::string &tag, const std::map<std::string, std::string> &params);
    
protected:
    void check_dependencies() override;
    void do_analyse() override;
    /*!
     * \brief Check if a connetor is used
     * 
     * \param allconn vector of all connectors
     * \param test connector to check
     * \return true if the connecter is used, false otherwise
     */
    bool is_connector_used(const std::vector<Connector*> &allconn, Connector *test);
    
    virtual std::string help() override;
};

/*!
 * \remark Register the DeadlockCheck class as an analysis with config id
 * "deadlock"
 */ 
REGISTER_ANALYSE(ScheduleStats, "schedule-stats");

#endif /* NVIDIASTATS_HPP */


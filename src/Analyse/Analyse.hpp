/*!
 * \file Analyse.hpp
 * \copyright GNU Public License v3.
 * \date 2019 
 * \author Benjamin Rouxel <benjamin.rouxel@uva.nl>, University of Amsterdam
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ANALYSE_H
#define ANALYSE_H

#include <string>
#include <vector>

#include "config.hpp"
#include "registry.hpp"
#include "SystemModel.hpp"
#include "Utils.hpp"
#include "CompilerPass.hpp"
#include "Utils/Log.hpp"
#include "Utils/Graph.hpp"
#include "Schedule.hpp"

/*!
 * \brief Base class for analyses
 * 
 * Perform an analysis on the IR, or the schedule
 */
class Analyse : public CompilerPass {
protected:
    //! IR of the current application
    SystemModel *tg;
    //! the global configuration
    const config_t *conf;
    //! current state of the schedule
    Schedule *sched;
    
public:
    //! Constructor
    explicit Analyse() {};
    void run(SystemModel *m, config_t *c, Schedule *s) override {
        tg = m;
        conf = c;
        sched = s;

        check_dependencies();
        do_analyse();
    }
    
protected:
    //! Perform the analysis
    virtual void do_analyse() = 0;
};

/*!
 * \typedef AnalyseRegistry
 * \brief Registry containing all analyses
 */
using AnalyseRegistry = registry::Registry<Analyse, std::string>;

/*!
 * \brief Helper macro to register a new derived analysis
 */
#define REGISTER_ANALYSE(ClassName, Identifier) \
  REGISTER_SUBCLASS(Analyse, ClassName, std::string, Identifier)

#endif /* ANALYSE_H */


/*!
 * \file Deadlock.cpp
 * \copyright GNU Public License v3.
 * \date 2019 
 * \author Benjamin Rouxel <benjamin.rouxel@uva.nl>, University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "CriticalPath.hpp"

using namespace std;

IMPLEMENT_HELP(CriticalPath,
    Compute the critical path
)

CriticalPath::CriticalPath() : Analyse() {}

void CriticalPath::forward_params(const map<string,string>&) {}

const string CriticalPath::get_uniqid_rtti() const { return "analyse-critical_path"; }

void CriticalPath::do_analyse() {
    vector<vector<Task*>> dags;
    Utils::extractGraphs(tg, &dags);
    
    for(vector<Task*> dag : dags) {
        timinginfos_t latest_f = 0;
        Utils::BFS(&dag, [this, &latest_f](Task *node) {
            timinginfos_t est = 0;
            for(Task::dep_t elp : node->predecessor_tasks()) {
                if(est < earliest[elp.first].second)
                    est = earliest[elp.first].second;
            }
            earliest[node] = make_pair(est, est+node->C());
            if(latest_f < est+node->C())
                latest_f = est+node->C();
        });
        Utils::rBFS(&dag, [this, latest_f](Task *node) {
            timinginfos_t lft = latest_f;
            for(Task::dep_t elp : node->successor_tasks()) {
                if(lft > latest[elp.first].first) {
                    lft = latest[elp.first].first;
                }
            }
            latest[node] = make_pair(lft-node->C(), lft);
        });
        
        // Build critical path, all tasks with a no floating time are on it
        vector<Task*> current_cp;
        for(Task *t : dag) {
            if((latest[t].second - earliest[t].second) == 0)
                current_cp.push_back(t);
        }
        sort(current_cp.begin(), current_cp.end(), [this](Task *a, Task *b) {return earliest[a].first < earliest[b].first; });
        
        critical_paths.push_back(current_cp);
    }
    
    sort(critical_paths.begin(), critical_paths.end(), [this](const vector<Task*> &a, const vector<Task*> &b) { return earliest[a.back()].second < earliest[b.back()].second; });
    critical_path = critical_paths.back();
    
//    for (Task *t : tg->tasks_it()) {
//        Utils::DEBUG("-> "+to_string(earliest[t].first)+"/"+to_string(latest[t].first)+" -- "+t->id()+"("+to_string(t->C())+")"+" -- "+to_string(earliest[t].second)+"/"+to_string(latest[t].second));
//    }
//    
//    string msg = "";
//    for(Task *t : critical_path) 
//        msg += "-> "+t->id()+" ("+to_string(earliest[t].first)+"-"+to_string(earliest[t].first+t->C())+") ";
//    Utils::DEBUG("Critical path: "+msg);
}

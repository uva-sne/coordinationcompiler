/*!
 * \file Deadlock.hpp
 * \copyright GNU Public License v3.
 * \date 2019 
 * \author Benjamin Rouxel <benjamin.rouxel@uva.nl>, University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef DEADLOCK_H
#define DEADLOCK_H

#include "Analyse.hpp"

/**
 * @brief Determine whether a graph contains possible sources of deadlock.
 *
 * Deadlock can occur in several ways in a task graph, and it is important that
 * we are able to detect it. This analysis pass checks for three possible
 * sources of deadlock in a task graph:
 *
 * 1. Output connectors which do not connect to any input connectors.
 * 2. Input connectors which do not connect to any output connectors.
 * 3. Connected connectors which have differing token counts.
 *
 * An output connector must have its output read or it will fill up its output
 * buffer and be unable to produce further output. If no tasks are connected to
 * that output, the buffer will never be emptied and the system will deadlock.
 * That input connectors need to be connected is perhaps more obvious, and
 * tasks cannot run if their input is not provided.
 *
 * The third checked scenario is guarantee that enough tokens are produced 
 * according to the corresponding consumption.
 *
 * @remark If the graph is a SDF graph, then the repetition vector must be 
 * computed a priori.
 *
 * @remark Unlike most other analyses, the deadlock analysis does not have a
 * flag to configure its failure behaviour. It never fails silently and always
 * throws an exception. This prevents generating wrong code.
 * 
 * \see #DeadlockCheck::help
 */
class DeadlockCheck : public Analyse {
public:
    //! \brief Constructor
    DeadlockCheck();
    void forward_params(const std::map<std::string,std::string>&) override;
    const std::string get_uniqid_rtti() const override;
    
    virtual std::string help() override;
protected:    
    void check_dependencies() override;
    void do_analyse() override;
    /*!
     * \brief Check if a connetor is used
     * 
     * \param allconn vector of all connectors
     * \param test connector to check
     * \return true if the connecter is used, false otherwise
     */
    bool is_connector_used(const std::vector<Connector*> &allconn, Connector *test);
    
};

/*!
 * \remark Register the DeadlockCheck class as an analysis with config id
 * "deadlock"
 */ 
REGISTER_ANALYSE(DeadlockCheck, "deadlock");

#endif

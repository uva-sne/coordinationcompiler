/*!
 * \file CheckTiming.hpp
 * \copyright GNU Public License v3.
 * \date 2019 
 * \author Benjamin Rouxel <benjamin.rouxel@uva.nl>, University of Amsterdam
 * \author Stephen Nicholas Swatman <s.n.swatman@uva.nl> University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CHECKTIMING_H
#define CHECKTIMING_H

#include "Analyse.hpp"
#include "Schedule.hpp"
#include "Utils/Log.hpp"
#include "Utils/Graph.hpp"

/**
 * @brief Analysis pass to check Amdahl's law, if the sequential execution of a 
 * task graph meets the deadline.
 *
 * This compiler pass provides functionality to ensure that the critical path
 * of tasks in a task graph fits within the timing parameters of the problem.
 * Within a series of tasks, we consider the critical path to be a
 * list of tasks that maximizes the sum of the worst case execution times of
 * its tasks.
 *
 * For example, consider the following graph, with nodes annotated with their
 * worst case execution time:
 *
 *          2s       1s           5s
 *      +-> B  ----> D        +-> F
 *      |            |        |
 * 1s   |     1s     |   2s   |   4s
 * A  --+---> C  ----+-> E  --+-> G
 *
 * In this case, the critical path is [A, B, D, E, F] with a total worst case
 * execution time of 1+2+1+2+5=11 seconds. The pass will compare this runtime
 * with the deadline and the period of the subgraph. For example, if the
 * deadline of this subgraph is less than 11 seconds, the pass will reject the
 * task graph. Similarly, if this subgraph runs n times in the hyperperiod, if
 * 11n is longer than the hyperperiod of the task graph, the pass will also
 * reject it.
 *
 * @warning The timing check pass only works correctly if the property
 * propagation transformation has been run, as it assumes that the deadlines
 * and periods are consistent within subgraphs.
 *
 * @remark This pass will only fail loudly with an exception if the throwerror
 * parameter is set to true.
 *
 * @remark The fact that the timing is feasible does not have significant
 * bearing on whether a schedule can be found for the task graph; a task graph
 * may be feasible from a timing point of view but might still not be possible
 * to schedule.
 * 
 * \see #CheckTiming::help
 */
class CheckTiming : public Analyse{
    //! true to throw an exception, false to issue a warning
    bool force = false; 

public:
    void forward_params(const std::map<std::string, std::string> &args) override;
    const std::string get_uniqid_rtti() const override;
    CheckTiming();
    
    virtual std::string help() override;

protected:
    void check_dependencies() override;
    void do_analyse() override;
    
private: 
    /*!
     * \brief Handle issue 
     * 
     * Depending on the configuration it will display a warning or throw an
     * exception
     * 
     * \throws MyException
     * \param msg message to display or throw
     */
    void handle_dissonance(const std::string &msg);

    /*!
     * \brief Find the critical path through a task graph.
     * 
     * \param dag the vector of task of a single DAG
     * \param longestchain return the vector of the longest chain in the DAG
     */
    void get_longest_path(std::vector<Task*> dag, std::vector<Task*> *longestchain);
    /**
     * @brief Helper function to find the critical path through a task graph.
     *
     * @remark This is an auxiliary recursive function for get_longest_path().
     *
     * \param Qdone list of tasks already explored with their weight and the 
     * corresponding sub-chain
     * \param current task to explore
     * \param longest current longest chain size
     * \param longestchain current vector of the longest chain
     * \return last longest chain size
     */
    uint128_t get_longest_path_nbtasks_aux(
        std::map<Task*, std::pair<uint128_t, std::vector<Task*>>> &Qdone, Task *current, 
        uint128_t longest, std::vector<Task*> *longestchain);    
};

/*!
 * \remark Register the CheckTiming class as an analysis with config id
 * "check_timing_infos"
 */ 
REGISTER_ANALYSE(CheckTiming, "check_timing_infos")

#endif

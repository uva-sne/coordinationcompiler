/*
 * Copyright (C) 2020 Lukas Miedema <l.miedema@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "ScheduleValidationAnalyzer.hpp"

IMPLEMENT_HELP(ScheduleValidationAnalyzer, Validate the created schedule to help detect logic errors in the
        various solvers\n
        HTMLINDENT- throwerror[CONFIG_FALSE-CONFIG_TRUE], default CONFIG_FALSE:
    If set to " CONFIG_TRUE " will throw an error rather than raising a warning)

void ScheduleValidationAnalyzer::show_results_property(bool &all_ok, bool property_ok, const std::string &label) {
    if (property_ok)
        Utils::INFO(" [OKAY] " + label);
    else
        Utils::WARN(" [FAIL] " + label);
    all_ok &= property_ok;
}

void ScheduleValidationAnalyzer::forward_params(const std::map<std::string, std::string> &args) {
    if (args.count("throwerror"))
        this->throw_error = args.at("throwerror") == CONFIG_TRUE;
}

void ScheduleValidationAnalyzer::check_dependencies() {
    if (this->sched == nullptr)
        throw CompilerException("dependency check",
                "Dependency unsatisfied, no schedule present. Did you add a solver pass before the ScheduleValidator?");
}

const std::string ScheduleValidationAnalyzer::get_uniqid_rtti() const {
    return "analyse-validator";
}

void ScheduleValidationAnalyzer::show_results(ValidationResult *result) {
    bool is_valid = true;
    _report_function(is_valid, result->has_schedule, "Schedule has been computed");
// see in do analyse
//    _report_function(is_valid, result->all_tasks_scheduled, "All tasks are in the schedule");
    if (result->has_schedule) {
        _report_function(is_valid, result->overallocated_tasks.empty(), "No overallocated tasks");
        for (const OverlappingTask &task: result->overallocated_tasks) {
            Utils::WARN(" - Overallocation on core " + task.core->core()->id + " between " +
                    task.element0->schedtask()->toString() + " [ends at t=" +
                    to_string(task.element0->rt() + task.element0->wct()) + "] and " +
                    task.element1->schedtask()->toString() + " [starts at t=" + to_string(task.element1->rt()) + "]");
        }
        _report_function(is_valid, result->tasks_without_assigned_cores.empty(),
                "All tasks are assigned to a CPU core");
        for (SchedElt *element: result->tasks_without_assigned_cores) {
            Utils::WARN(" - No CPU core assigned to " + element->toString());
        }
        _report_function(is_valid, result->tasks_with_unsatisfied_dependencies.empty(), "All task dependencies met");
        for (const UnsatisfiedDependency &dependency: result->tasks_with_unsatisfied_dependencies) {
            Utils::WARN(" - Task " + dependency.element->id() + " scheduled before " +
                    dependency.missing_predecessor->id() + ": " + dependency.message);
        }
        _report_function(is_valid, result->tasks_without_versions.empty(), "All tasks have an active version");
        for (SchedElt *element: result->tasks_without_assigned_cores) {
            Utils::WARN(" - No version assigned to " + element->toString());
        }

        _report_function(is_valid, result->wcet_mismatches.empty(), "All tasks in the schedule have the correct WCET");
        for (const VersionWCETMismatch &wcetMismatch: result->wcet_mismatches) {
            Utils::WARN(" - Task " + wcetMismatch.element->id() +
                    " has a WCET that does not match that of its assigned version (version WCET=" +
                    to_string(wcetMismatch.versionWCET) + "ns, schedule WCET=" +
                    to_string(wcetMismatch.actualWCET) + "ns)");
        }

        _report_function(is_valid, result->tasks_with_illegal_architectures.empty(), "All task versions are scheduled on processors that support that version");
        for (const IllegalArchitectureAssignment &illegalArch: result->tasks_with_illegal_architectures) {
            Utils::WARN(" - Task " + illegalArch.element->id() + " with version " + illegalArch.version->id() +
                    " supports processors [" + boost::algorithm::join(illegalArch.supported_processors, ", ") +
                    "] but is mapped to processor " + illegalArch.actual_processor);
        }

        _report_function(is_valid, result->actual_makespan <= result->reported_makespan,
                "Schedule fits within reported makespan (" + to_string(result->reported_makespan) + " <= " +
                        to_string(result->actual_makespan) + ")");
        if (this->tg->has_global_deadline())
            _report_function(is_valid, result->reported_makespan <= this->tg->global_deadline(),
                    "Schedule fits within deadline (" + to_string(result->reported_makespan) + " <= " +
                            to_string(tg->global_deadline()) + ")");
    }
    _report_function(is_valid, is_valid, "∴ Schedule is valid");

    if (!is_valid && this->throw_error)
        throw CompilerException(get_uniqid_rtti(), "Invalid schedule. See console output for details");

}

void ScheduleValidationAnalyzer::do_analyse() {
    ValidationResult result;
    if (sched->status() == ScheduleProperties::sched_statue_e::SCHED_COMPLETE) {
        result.has_schedule = true;
        // Ben: This is completely wrong, there can be more schedelt in the schedule than in there are tasks in the taskset
        //      - Schedule on the hyperperiod of periodic tasks
        //      - Schedule of both communication and tasks
//        result.all_tasks_scheduled = tg->tasks().size() == sched->get_scheduled_elts().size();
        result.reported_makespan = sched->makespan();
    } else {
        result.has_schedule = false;
        show_results(&result);
        return;
    }

    std::map<SchedCore *, std::vector<SchedElt *>> tasks_by_core;
    for (SchedEltPtr &element: sched->elements()) {

        if (element->type() == SchedElt::TASK) {
            // check for processor assignment legality
            std::string selected_version = element->schedtask()->selected_version();
            Version *version = element->task()->versions(selected_version);
            if (version == nullptr)
                result.tasks_without_versions.push_back(element.get());
            else {
                // check for the WCET
                if (element->wct() != version->C()) {
                    result.wcet_mismatches.emplace_back(element->task(), element->wct(), version->C());
                }

                // check processor assignment legality
                SchedCore *core = sched->get_mapping(element.get());
                if (core == nullptr) {
                    result.tasks_without_assigned_cores.push_back(element.get());
                } else {
                    tasks_by_core[core].push_back(element.get());

                    ComputeUnit *actual_cu = core->core();
                    if (!version->canRunOn(actual_cu)) {
                        std::vector<std::string> supported_processors;
                        for (ComputeUnit *cu: version->force_mapping_proc())
                            supported_processors.push_back(cu->id + "/" + cu->type);
                        std::string actual_processor = actual_cu->id + "/" + actual_cu->type;
                        result.tasks_with_illegal_architectures.emplace_back(element->task(), version,
                                supported_processors, actual_processor);
                    }
                }
            }
        }

    }

    // sorts schedule elements by their end time (rt + wct)
    auto end_time_comparator = [](SchedElt *a, SchedElt *b) {
        if (a->rt() + a->wct() == b->rt() + b->wct())
            return a->rt() < b->rt();
        return a->rt() + a->wct() < b->rt() + b->wct();
    };

    // check for overallocation
    for (std::pair<SchedCore *, std::vector<SchedElt *>> entry: tasks_by_core) {
        std::vector<SchedElt *> elements = entry.second;
        std::sort(elements.begin(), elements.end(), end_time_comparator); // sort by end
        uint64_t busy_until = 0;

        for (std::vector<SchedElt *>::iterator i = elements.begin(); i != elements.end(); i++) {
            SchedElt *element = *i;
            if (busy_until > element->rt()) {
                // must exist as busy_until is unsigned so the comparison is always false for the first element
                SchedElt *prev_element = *(i -1);
                result.overallocated_tasks.emplace_back(entry.first, prev_element, element);
            }
            busy_until = std::max(busy_until, element->rt() + element->wct());
        }

        result.actual_makespan = std::max(result.reported_makespan, busy_until);
    }

    // check if all dependencies are OK
    // TODO: also validate packets?
    for (SchedJob *schedtask: sched->schedjobs()) {
        // for each task, the start time must be after the end time of all its dependencies
        uint64_t job_start = schedtask->rt();
        for (SchedElt *pred: schedtask->previous()) {
            uint64_t pred_job_end = pred->rt() + pred->wct();
            if (pred_job_end > job_start) {
                result.tasks_with_unsatisfied_dependencies.emplace_back(schedtask->task(), pred->schedtask()->task(),
                        "Task scheduled too soon. Predecessor not yet finished (predecessor end=" +
                                to_string(pred_job_end) + " after start=" + to_string(job_start) + ")");
            }
        }
    }


    show_results(&result);
}

/*
 * Copyright (C) 2019 Benjamin Rouxel <benjamin.rouxel@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "QVector.hpp"
#include "Utils/Linalg.hpp"
#include "Utils/Log.hpp"

using namespace std;

using E = LabeledCompilerException<C_STR("qvector")>;

IMPLEMENT_HELP(QVector, 
    Compute the repetition vector of each graph, a.k.a q vector. This
    corresponds to the expansion ratio of each actor within a SDF graph.
)

QVector::QVector() : Analyse() {}
void QVector::forward_params(const map<string,string>&) {}
const string QVector::get_uniqid_rtti() const { return "analyse-repetition_vector"; }
    
void QVector::do_analyse() {
    for(Task *t : tg->tasks_it())
        t->repeat(1);
    
    size_t nbedges = tg->nbedges();
    if(nbedges == 0) {
        Utils::WARN("No edge, so no repetition vector to compute");
        return;
    }
    
    if(!is_communcation_unbalanced()) {
        Utils::INFO("The graph is already a HSDF");
        return;
    }
    
    map<Task*, size_t> indices;
    bu::matrix<int> topo(nbedges, tg->tasks().size(), 0);
    fill_topology_matrix(&topo, &indices);
    Utils::DEBUG("Topology matrice: ");
    for(size_t k = 0 ; k < topo.size1() ; ++k) {
        string tmp = "";
        for(size_t l = 0 ; l < topo.size2() ; ++l) {
            tmp += to_string(topo.at_element(k, l))+" ";
        }
        Utils::DEBUG(tmp);
    }
    
    size_t rank = Utils::matrix_rank(topo);
    if(rank != tg->tasks().size()-1)
        throw E("Rank of the topology matrix is "+to_string(rank)+" but should be "+to_string(tg->tasks().size()-1));
    Utils::DEBUG(("Rank: "+to_string(rank)+" == "+to_string(tg->tasks().size()-1)));
    
    map<Task*, boost::rational<uint32_t>> qv;
    compute_initial_qvector_dfs(qv);
    Utils::DEBUG("Temporary QVector: ");
    for(pair<Task*, boost::rational<uint32_t>> el : qv)
        Utils::DEBUG(to_string(el.second.numerator())+"/"+to_string(el.second.denominator())+"="+to_string(el.second.numerator()/(float)el.second.denominator()));
    
    vector<uint32_t> d;
    for(pair<Task*, boost::rational<uint32_t>> el : qv) {
        if(el.second.numerator() % el.second.denominator() != 0)
            d.push_back(el.second.denominator());
    }
    if(!d.empty()) {
        uint32_t lcm = Utils::lcm(d);
        Utils::DEBUG("LCM: "+to_string(lcm));
        for(map<Task*, boost::rational<uint32_t>>::iterator it=qv.begin(), et=qv.end() ; it != et ; ++it) {
            (*it).second *= lcm;
        }
    }
    
    for(size_t k = 0 ; k < topo.size1() ; ++k) {
        int accu = 0;
        for(size_t l = 0 ; l < topo.size2() ; ++l) {
            auto it = find_if(indices.begin(), indices.end(), [l](pair<Task*, size_t> el) { return el.second == l;});
            boost::rational<uint32_t> r = qv.at(it->first);
            accu += topo.at_element(k, l) * (r.numerator()/r.denominator());
        }
        if(accu != 0)
            throw E("Topology matrix * qvector != {0}");
    }
    
    Utils::DEBUG("QVector:");
    for(pair<Task*, boost::rational<uint32_t>> el : qv) {
        el.first->repeat(el.second.numerator() / el.second.denominator());

        if (el.second.numerator() % el.second.denominator() != 0) {
            Utils::WARN(
                "Q vector denominator " + to_string(el.second.denominator()) +
                " does not divide numerator " + to_string(el.second.numerator())
            );
        }

        Utils::DEBUG(
            el.first->id() + ": " + to_string(el.second.numerator()) + "/" +
            to_string(el.second.denominator()) + " = " + to_string(el.first->repeat())
        );
    }
}

bool QVector::is_communcation_unbalanced() {
    for(Task *a : tg->tasks_it()) {
        for (Task::dep_t dep: a->successor_tasks()) {
            for (Connection *conn : dep.second) {
                uint32_t toka = conn->from()->tokens();
                uint32_t tokb = conn->to()->tokens();
                if(toka != tokb)
                    return true;
            }
        }
    }
    return false;
}

void QVector::fill_topology_matrix(bu::matrix<int> *topo, map<Task*, size_t> *indices) {
    size_t i = 0;
    for(Task *t : tg->tasks_it())
        (*indices)[t] = i++;
    size_t current_line = 0;
    for(Task *a : tg->tasks_it()) {
        for (Task::dep_t dep: a->successor_tasks()) {
            Task *b = dep.first;
            for (Connection *conn : dep.second) {
                uint32_t toka = conn->from()->tokens();
                uint32_t tokb = conn->to()->tokens();
                topo->insert_element(current_line, indices->at(a), toka);
                topo->insert_element(current_line, indices->at(b), -tokb);
                ++current_line;
            }
        }
    }
}

void QVector::compute_initial_qvector_dfs(map<Task*, boost::rational<uint32_t>> &qvector) {
    vector<Task*> Qdone;
    for(Task *t : tg->tasks_it())
        qvector[t].assign(1, 1);
    for(Task *t : tg->tasks_it()) {
        if(t->successor_tasks().size() != 0) continue;
        compute_qvector_dfs_aux(Qdone, t, qvector);
    }
}

void QVector::compute_qvector_dfs_aux(vector<Task*> &Qdone, Task*current, map<Task*, boost::rational<uint32_t>> &qvector) {
    if(find(Qdone.begin(), Qdone.end(), current) != Qdone.end())
        return;
    Qdone.push_back(current);
    
    for(Task::dep_t el : current->predecessor_tasks()) {
        for (Connection *conn : el.second) {
            uint32_t n = conn->to()->tokens();
            uint32_t d = conn->from()->tokens();
            boost::rational<uint32_t> r(n, d);

            qvector[el.first] = qvector[current] * r;

            compute_qvector_dfs_aux(Qdone, el.first, qvector);
        }
    }
}

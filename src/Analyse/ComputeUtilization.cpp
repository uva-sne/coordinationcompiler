/*!
 * \file ComputeUtilization.cpp
 * \copyright GNU Public License v3.
 * \date 2020 
 * \author Benjamin Rouxel <benjamin.rouxel@uva.nl>, University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "ComputeUtilization.hpp"

using namespace std;

IMPLEMENT_HELP(ComputeUtilization,
    Compute the total utilization and the utilization per core (if mapping done).
    Utilization is then check to be inferior to the number of core, or inferior
    to 1 per core.\n
    HTMLINDENT * throwerror [CONFIG_FALSE-CONFIG_TRUE], default CONFIG_FALSE: 
        If set to CONFIG_TRUE will throw an error rather than raising a warning.
)

ComputeUtilization::ComputeUtilization() : Analyse() {}

void ComputeUtilization::forward_params(const map<string,string>&args) {
    if(args.count("throwerror"))
        force = args.at("throwerror") != CONFIG_TRUE;
}

const string ComputeUtilization::get_uniqid_rtti() const { return "analyse-utilization"; }

/*!
 * \remark depends on Transform/#PropagateProperties
 * \remark a schedule pass must have been done, or at least a partitioning algorithm
 */
void ComputeUtilization::check_dependencies() {
    if(sched == nullptr || sched->mapping_mode() < ScheduleProperties::Mapping::MAP_PARTITIONED)
        throw CompilerException("dependency check", "A mapping algorithm must have run");
    
    if(!conf->passes.find_pass_before("transform-propagate_properties", this)) {
        throw CompilerException(
            "analyse-utilization",
            "Dependency unsatisfied, the \"transform-propagate_properties\" "
            "pass must be executed before the \"analyse-utilization\" pass."
        );
    }
}

void ComputeUtilization::do_analyse() {
    Utils::DEBUG("Utilization:");
    
    float tot_util = 0;
    for(Task *t : tg->tasks_it()) {
        float util = t->C()/ (float)t->T();
        Utils::DEBUG("\t"+t->id()+": "+to_string(util));
        tot_util += util;
    }
    
    Utils::DEBUG("Tot util: "+to_string(tot_util));
    if(tot_util > tg->processors().size()) {
        if(!force)
            throw Unschedulable("Total utilization ("+to_string(tot_util)+") > nb cores ("+to_string(tg->processors().size())+")");
        Utils::WARN("\tUtilization above "+to_string(tg->processors().size())+"("+to_string(tot_util)+")");
    }
    
    map<SchedCore*, float> utils;
    for(SchedCore *c : sched->schedcores())
        utils[c] = 0.0f;
    
    for(SchedElt *e : sched->schedjobs()){
        float util = e->schedtask()->task()->C()/ (float)e->schedtask()->task()->T();
        SchedCore *c = sched->get_mapping(e);
        if(c != nullptr)
            utils[c] += util;
    }
    
    for(SchedCore *c : sched->schedcores()) {
        Utils::DEBUG("U "+c->core()->id+": "+to_string(utils[c]));
        if(utils[c] > 1.0f) {
            if(!force)
                throw Unschedulable("Utilization on core "+c->core()->id+" above 1 ("+to_string(utils[c])+")");
            Utils::WARN("\tUtilization on core "+c->core()->id+" above 1 ");
        }
    }
}

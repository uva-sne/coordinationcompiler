/*
 * Copyright (C) 2020 Wouter Loeve <wouterloeve0@gmail.com>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef FTVISUALISER_H
#define FTVISUALISER_H

#include "Exporter.hpp"
#include "boost/filesystem.hpp"

/**
 * Export Fault-Tolerance settings
 * 
 * This generator exports the graph of the program readable for the FT-visualiser program.
 */
class FTVisualiserExport : public Exporter {
public:
    //! Constructor
    explicit FTVisualiserExport();
    
    std::string filename = "";

    void forward_params(const std::map<std::string, std::string> &args) override;
    std::string help() override;
    
    const std::string get_uniqid_rtti() const override;
    
protected:
    void do_export(const boost::filesystem::path &f) override;
    const std::string file_extension() override {
        return ".FTGraph";
    }
};

REGISTER_EXPORTER(FTVisualiserExport, "FTVisualiser");

#endif
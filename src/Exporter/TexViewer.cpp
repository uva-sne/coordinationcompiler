/* 
 * Copyright (C) 2020 Benjamin Rouxel <benjamin.rouxel@uva.nl> 
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "TexViewer.hpp"

using namespace std;

IMPLEMENT_HELP(TexViewer,
    Export the generated schedule into a Tex file.\n
    HTMLINDENT- step [int], default 1: time step, how many ns a square of the export is equal to\n
    HTMLINDENT- scale [float], default 1.0: scale of the tikzpicture\n
    HTMLINDENT- filename, coordFileDir/appname.svg: name with or without absolute path for the generated file.
)
void TexViewer::forward_params(const map<string, string> &args) {
    if(args.count("step") && !args.at("step").empty())
        step = stoul(args.at("step"));
    if(args.count("scale") && !args.at("scale").empty())
        scale = stof(args.at("scale"));
    if(args.count("filename") && !args.at("filename").empty())
        filename(args.at("filename"));
}

TexViewer::TexViewer() : Exporter() {}

const string TexViewer::get_uniqid_rtti() const { return "exporter-tex"; }

void TexViewer::check_dependencies() {
    if (!conf->passes.find_scheduler_or_solver_before(this))
        throw CompilerException("export schedule", "No schedule have been generated.");
}

void TexViewer::do_export(const boost::filesystem::path& fn) {
    string sched_gen_label = "";
    const auto &passes = conf->passes.passes();
    auto et = find_if(passes.rbegin(), passes.rend(), [this](const auto &p) {return p->get_uniqid_rtti() == this->get_uniqid_rtti();});
    auto it = find_if(et, passes.rend(), [](const auto &p) { return p->get_uniqid_rtti().find("solver") != string::npos; });
    if(it != passes.rend()) {
        Solver *s = (Solver*) it->get();
        sched_gen_label = s->type()+"/"+Utils::demangle(typeid(*s).name());
    }
    else {
        it = find_if(et, passes.rend(), [](const auto &p) { return p->get_uniqid_rtti().find("simulator-sched") != string::npos; });
        if(it != passes.rend()) {
            Simulator *s = (Simulator*) it->get();
            sched_gen_label = Utils::demangle(typeid(*s).name());
        }
    }


    ofstream out(fn.string());
    out << header() << endl;
    out << commands() << endl;
    out << start_document() << endl;

    if(sched->status() < ScheduleProperties::sched_statue_e::SCHED_UNSCHEDULABLE) {
        out << "\\newcommand{\\valHyperperiod}{" << tg->hyperperiod() << "}" << endl;
        int heightmax=0, heightmin=1;
        vector<Task*> tasks = tg->tasks_as_vector();
        //! @remark Tasks in the tex appear sorted by the smallest period at the top (request from Yoann)
        sort(tasks.begin(), tasks.end(),[](Task *a, Task *b) {
            return a->T() > b->T();
        });
        int height = heightmin;
        for(Task *t : tasks) {
            out << task_infos(t, height) << endl;
            ++height;
        }
        heightmax = height-1;

        out << "\\newcommand{\\heightMax}{" << heightmax << "}" << endl;
        out << "\\newcommand{\\heightMin}{" << heightmin << "}" << endl;

        for(SchedCore *core : sched->schedcores()) {
            out << start_tikz() << endl;
            out << "\\node [font=\\bfseries] at (-1*\\widthUnit,1.4+\\heightMax*\\heightUnit) {\\LARGE "<< core->core()->id << "};" << endl;
            for(Task *t : tg->tasks_it()) {
                out << "\\tikzset{" << endl;
                out << "    style"<< t->id() << "/.style={line width=6pt,font=\\bfseries,color=\\color"<< t->id() << "}" << endl;
                out << "}" << endl;
                out << "\\drawTaskNode{" << t->id() << "}" << endl;
                out << "\\drawTaskPeriodsDeadlines{" << t->id() << "}" << endl;
            }
            
            for(SchedElt *elt : sched->scheduled_elements()) {
                if(sched->get_mapping(elt) != core) continue;
                for(pair<uint64_t, uint64_t> ec : elt->schedtask()->exec_chunks()) {
                    uint64_t rt = ec.first;
                    uint64_t wct = ec.second;
                    out << "\\drawTaskInterval{" << elt->schedtask()->task()->id() << "}{" << int(floor(rt/step))+1 << "}{" << int(floor(rt+wct))/step << "}" << endl;
                }

            }
            out << end_tikz() << endl;
        }
    }
    out << end_document() << endl;

    out.close();
    Utils::DEBUG("Tex file saved in "+fn.string());
}
string TexViewer::header() {
    return R"CPPRAWMARKER(
\PassOptionsToPackage{dvipsnames}{xcolor}
\documentclass[tikz]{standalone}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{multido}
\usepackage{ifthen}
            )CPPRAWMARKER";
}
string TexViewer::commands() {
    return R"CPPRAWMARKER(
\def\basiceval#1{\the\numexpr#1\relax}

% colors independent from tasks
\newcommand{\colorNeutral}{Black!100}

% lengths independent from tasks
\newcommand{\widthUnit}{1.0}
\newcommand{\heightUnit}{1.0}
\newcommand{\heightPeriod}{0.5}

% task functions
\newcommand{\funTask}[2]{\csname#1#2\endcsname}
\newcommand{\heightTask}[1]{\funTask{height}{#1}}
\newcommand{\periodTask}[1]{\funTask{period}{#1}}
\newcommand{\offsetTask}[1]{\funTask{offset}{#1}}
\newcommand{\deadlineTask}[1]{\funTask{deadline}{#1}}
\newcommand{\diffTask}[1]{\basiceval{\periodTask{#1} - \deadlineTask{#1}}}
\newcommand{\occTask}[1]{\basiceval{\valHyperperiod/\periodTask{#1}}}

% draw hyperperiod
\newcommand{\drawHyperperiod}[1]{
    \draw[styleHyperperiod](#1*\widthUnit,\heightMin*\heightUnit)--(#1*\widthUnit,1+\heightMax*\heightUnit);
}

% draw task nodes
\newcommand{\drawTaskNode}[1]{\node[style#1] at (-1*\widthUnit,0.3+\heightTask{#1}*\heightUnit) {\large #1};}

% draw task periods and deadlines
\newcommand{\drawTaskPeriod}[2]{%
    \draw[style#1, ultra thick,->,>=stealth]
        (#2*\widthUnit,{(\heightTask{#1}-0*\heightPeriod/2)*\heightUnit})
        --
        (#2*\widthUnit,{0.3+(\heightTask{#1}+\heightPeriod)*\heightUnit});
}
\newcommand{\drawTaskDeadline}[2]{%
    \draw[style#1, ultra thick,->,>=stealth]
        (#2*\widthUnit,{0.3+(\heightTask{#1}+\heightPeriod)*\heightUnit})
        --
        (#2*\widthUnit,{(\heightTask{#1}-0*\heightPeriod/2)*\heightUnit});
}
\newcommand{\startDeadline}{0}
\newcommand{\drawTaskPeriodsDeadlines}[1]{%
    % draw periods
    \multido{\i=\offsetTask{#1}+\periodTask{#1}}{\basiceval{\occTask{#1} + 1}}{%
    	\drawTaskPeriod{#1}{\i}
    }%
    \ifthenelse{\offsetTask{#1} = 0}{%
        \drawTaskPeriod{#1}{\valHyperperiod}
    }{%
    }%
    % draw deadlines
    \ifthenelse{\offsetTask{#1} < \diffTask{#1}}{%
        \renewcommand{\startDeadline}{\basiceval{\offsetTask{#1} + \deadlineTask{#1}}}
    }{%
        \renewcommand{\startDeadline}{\basiceval{\offsetTask{#1} - \diffTask{#1}}}
    }%
    \multido{\i=\startDeadline+\periodTask{#1}}{\occTask{#1}}{%
        \drawTaskDeadline{#1}{\i}
    }%
    \ifthenelse{\basiceval{\offsetTask{#1} + \deadlineTask{#1}} = \periodTask{#1}}{%
        \drawTaskDeadline{#1}{\valHyperperiod}
    }{%
    }%
}

% draw task intervals and slots
\newcommand{\drawTaskInterval}[3]{%
    \draw[style#1]
        ({(#2 - 1)*\widthUnit},0.1+\heightTask{#1}*\heightUnit)
        --
        (#3*\widthUnit,0.1+\heightTask{#1}*\heightUnit);
}
\newcommand{\drawTaskSlot}[2]{\drawTaskInterval{#1}{#2}{#2}}
            
)CPPRAWMARKER";
}

string TexViewer::task_infos(Task *t, int h) {
    ostringstream task;
    task << "\\newcommand{\\color" << t->id() << "}{" << Utils::colors[color_index] << "!100}" << endl;
    color_index = (color_index + 1) % Utils::colors_arr_size;
    task << "\\newcommand{\\height" << t->id() << "}{"<< h << "}" << endl;
    task << "\\newcommand{\\period" << t->id() << "}{"<< t->T() << "}" << endl;
    task << "\\newcommand{\\offset" << t->id() << "}{"<< t->offset() << "}" << endl;
    task << "\\newcommand{\\deadline" << t->id() << "}{"<< t->D() << "}" << endl;
    return task.str();
}

string TexViewer::start_document() {
    return R"CPPRAWMARKER(
\begin{document}
)CPPRAWMARKER";
}

string TexViewer::end_document() {
    return R"CPPRAWMARKER(
\end{document}
)CPPRAWMARKER";
}

string TexViewer::start_tikz() {
    string rotate = (step > 1) ? ",rotate=90,anchor=north" : "";
    return R"CPPRAWMARKER(
\begin{tikzpicture}[scale=)CPPRAWMARKER"+to_string(scale)+R"CPPRAWMARKER(]
    % styles
    \tikzset{
        styleHyperperiod/.style={ultra thick,font=\bfseries,color=\colorNeutral}
    }
    % grid and hyperperiod
        \draw [ultra thin, gray] (0,\heightMin*\heightUnit) grid (\valHyperperiod,1+\heightMax*\heightUnit);
        \drawHyperperiod{0}
        \drawHyperperiod{\valHyperperiod}
    % numbering
        \multido{\i=0+1}{\basiceval{\valHyperperiod+1}}{%
            \node[styleHyperperiod)CPPRAWMARKER"+rotate+R"CPPRAWMARKER(] at
            ({(\i*1)*\widthUnit},{(\heightMin-0.5)*\heightUnit}) 
            {\basiceval{\i*)CPPRAWMARKER"+to_string(step)+R"CPPRAWMARKER(}};
        }%
)CPPRAWMARKER";
}

string TexViewer::end_tikz() {
    return R"CPPRAWMARKER(
\end{tikzpicture}
)CPPRAWMARKER";
}


/*
 
\PassOptionsToPackage{dvipsnames}{xcolor}
\documentclass[tikz]{standalone}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{multido}
\usepackage{ifthen}
            
% arithmetic
\def\basiceval#1{\the\numexpr#1\relax}

% colors independent from tasks
\newcommand{\colorNeutral}{Black!100}

% lengths independent from tasks
\newcommand{\widthUnit}{1.0}
\newcommand{\heightUnit}{1.0}
\newcommand{\heightPeriod}{0.5}

% task functions
\newcommand{\funTask}[2]{\csname#1#2\endcsname}
\newcommand{\heightTask}[1]{\funTask{height}{#1}}
\newcommand{\periodTask}[1]{\funTask{period}{#1}}
\newcommand{\offsetTask}[1]{\funTask{offset}{#1}}
\newcommand{\deadlineTask}[1]{\funTask{deadline}{#1}}
\newcommand{\diffTask}[1]{\basiceval{\periodTask{#1} - \deadlineTask{#1}}}
\newcommand{\occTask}[1]{\basiceval{\valHyperperiod/\periodTask{#1}}}

% draw hyperperiod
\newcommand{\drawHyperperiod}[1]{\draw[styleHyperperiod](#1*\widthUnit,\heightMin*\heightUnit)--(#1*\widthUnit,\heightMax*\heightUnit);}

% draw task nodes
\newcommand{\drawTaskNode}[1]{\node[style#1] at (-1*\widthUnit,\heightTask{#1}*\heightUnit) {#1};}

% draw task periods and deadlines
\newcommand{\drawTaskPeriod}[2]{%
    \draw[style#1,->,>=stealth]
        (#2*\widthUnit,{(\heightTask{#1}-0*\heightPeriod/2)*\heightUnit})
        --
        (#2*\widthUnit,{(\heightTask{#1}+\heightPeriod)*\heightUnit});
}
\newcommand{\drawTaskDeadline}[2]{%
    \draw[style#1,->,>=stealth]
        (#2*\widthUnit,{(\heightTask{#1}+\heightPeriod)*\heightUnit})
        --
        (#2*\widthUnit,{(\heightTask{#1}-0*\heightPeriod/2)*\heightUnit});
}
\newcommand{\startDeadline}{0}
\newcommand{\drawTaskPeriodsDeadlines}[1]{%
    % draw periods
    \multido{\i=\offsetTask{#1}+\periodTask{#1}}{\occTask{#1}}{%
        \drawTaskPeriod{#1}{\i}
    }%
    \ifthenelse{\offsetTask{#1} = 0}{%
        \drawTaskPeriod{#1}{\valHyperperiod}
    }{%
    }%
    % draw deadlines
    \ifthenelse{\offsetTask{#1} < \diffTask{#1}}{%
        \renewcommand{\startDeadline}{\basiceval{\offsetTask{#1} + \deadlineTask{#1}}}
    }{%
        \renewcommand{\startDeadline}{\basiceval{\offsetTask{#1} - \diffTask{#1}}}
    }%
    \multido{\i=\startDeadline+\periodTask{#1}}{\occTask{#1}}{%
        \drawTaskDeadline{#1}{\i}
    }%
    \ifthenelse{\basiceval{\offsetTask{#1} + \deadlineTask{#1}} = \periodTask{#1}}{%
        \drawTaskDeadline{#1}{\valHyperperiod}
    }{%
    }%
}

% draw task intervals and slots
\newcommand{\drawTaskInterval}[3]{%
    \draw[style#1]
        ({(#2 - 1)*\widthUnit},\heightTask{#1}*\heightUnit)
        --
        (#3*\widthUnit,\heightTask{#1}*\heightUnit);
}
\newcommand{\drawTaskSlot}[2]{\drawTaskInterval{#1}{#2}{#2}}
            


\begin{document}

\newcommand{\valHyperperiod}{36}
\newcommand{\colorhigh}{Apricot!100}
\newcommand{\heighthigh}{1}
\newcommand{\periodhigh}{4}
\newcommand{\offsethigh}{0}
\newcommand{\deadlinehigh}{4}

\newcommand{\colorvictim}{Aquamarine!100}
\newcommand{\heightvictim}{2}
\newcommand{\periodvictim}{6}
\newcommand{\offsetvictim}{1}
\newcommand{\deadlinevictim}{6}

\newcommand{\colorattacker}{Bittersweet!100}
\newcommand{\heightattacker}{3}
\newcommand{\periodattacker}{9}
\newcommand{\offsetattacker}{0}
\newcommand{\deadlineattacker}{9}

\newcommand{\heightMax}{3}
\newcommand{\heightMin}{1}

\begin{tikzpicture}[scale=1.000000]
    % styles
    \tikzset{
        styleHyperperiod/.style={ultra thick,font=\bfseries,color=\colorNeutral}
    }
    % grid and hyperperiod
        \draw [ultra thin, gray] (0,\heightMin*\heightUnit) grid (\valHyperperiod,1+\heightMax*\heightUnit);
        \drawVerticalBar{0}
        \drawVerticalBar{\valHyperperiod}
    % numbering
        \multido{\i=0+1}{\basiceval{\valHyperperiod+1}}{%
            \node[styleHyperperiod] at 
            ({(\i*1)*\widthUnit},{(\heightMin-0.5)*\heightUnit}) 
            {\basiceval{\i*1}};
        }%

\node [font=\bfseries] at (-1*\widthUnit,1.4+\heightMax*\heightUnit) {\LARGE P1};
\tikzset{
    stylehigh/.style={line width=6pt,font=\bfseries,color=\colorhigh}
}
\drawTaskNode{high}
\drawTaskPeriodsDeadlines{high}
\tikzset{
    stylevictim/.style={line width=6pt,font=\bfseries,color=\colorvictim}
}
\drawTaskNode{victim}
\drawTaskPeriodsDeadlines{victim}
\tikzset{
    styleattacker/.style={line width=6pt,font=\bfseries,color=\colorattacker}
}
\drawTaskNode{attacker}
\drawTaskPeriodsDeadlines{attacker}
\drawTaskInterval{high}{1}{1}
\drawTaskInterval{victim}{2}{2}
\drawTaskInterval{attacker}{3}{4}
\drawTaskInterval{high}{5}{5}
\drawTaskInterval{victim}{8}{8}
\drawTaskInterval{high}{9}{9}
\drawTaskInterval{attacker}{10}{11}
\drawTaskInterval{high}{13}{13}
\drawTaskInterval{victim}{14}{14}
\drawTaskInterval{high}{17}{17}
\drawTaskInterval{victim}{20}{20}
\drawTaskInterval{high}{21}{21}
\drawTaskInterval{attacker}{19}{19}
\drawTaskInterval{attacker}{22}{22}
\drawTaskInterval{high}{25}{25}
\drawTaskInterval{victim}{26}{26}
\drawTaskInterval{high}{29}{29}
\drawTaskInterval{attacker}{28}{28}
\drawTaskInterval{attacker}{30}{30}
\drawTaskInterval{high}{33}{33}

\end{tikzpicture}


\end{document}


 
 
 */
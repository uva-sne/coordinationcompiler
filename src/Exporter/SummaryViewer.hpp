/*
 * Copyright (C) 2020 Lukas Miedema <l.miedema@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SUMMARYVIEWER_HPP
#define SUMMARYVIEWER_HPP

#include "Exporter.hpp"

#include "Analyse/ScheduleValidationAnalyzer.hpp"
#include <Solvers/Solver.hpp>

/**
 * Generate a textual summary of the schedule. This summary is designed to be stable -- it should be identical
 * across different ideal solvers except for the "solver" field, even though an ideal schedule for a given task graph
 * need not be unique.
 * As such, the information in the summary is restricted to basic information about the scheduling problem (number of
 * cores, number of tasks), makespan, and validation information about the schedule computed by the ScheduleValidator.
 *
 * \see ScheduleValidator
 * \see SummaryViewer::help
 */
class SummaryViewer : public Exporter {
public:
    void forward_params(const std::map<std::string, std::string> &args) override;
    std::string help() override;

    explicit SummaryViewer();
    const std::string get_uniqid_rtti() const override;

protected:
    void check_dependencies() override;
    void do_export() override;
    void do_export(const boost::filesystem::path& f) override;
    const std::string file_extension() override {
        return ".txt";
    }

    /**
     * Export the summary to an output stream (which can be std::cout).
     * @param out
     */
    void do_export(std::ostream& out);
};

REGISTER_EXPORTER(SummaryViewer, "summary")

#endif //SUMMARYVIEWER_HPP

/*
 * Copyright (C) 2021 Benjamin Rouxel <benjamin.rouxel@uva.nl> 
 *                    Lukas Miedina <l.miedina@uva.nl>
 *                    University of Amsterdam
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef UPPAAL_FT_HPP
#define UPPAAL_FT_HPP

#include "Exporter.hpp"
#include "boost/algorithm/string.hpp"
#include "UppaalTrait/UppaalTrait.hpp"

class FaultToleranceUppaalExporter : public Exporter {
    timinginfos_t interarrival_time = 3600000;
    std::string queries_dir = "";
public:
    FaultToleranceUppaalExporter();
    void forward_params(const std::map<std::string, std::string> &args) override;
    std::string help() override;
    const std::string get_uniqid_rtti() const override;
    std::vector<UppaalTrait *> traits;


    static std::string get_system(UppaalExportConfiguration &configuration);
    static std::string get_timestamp();
    static std::string get_features(UppaalExportConfiguration &conf);
protected:
    void do_export(const boost::filesystem::path& f) override;
    const std::string file_extension() override;

    std::string get_template();
    std::string get_embedded_queries(UppaalExportConfiguration configuration);
    std::string get_verifyta_query(UppaalExportConfiguration configuration, uint64_t iterations);

    const std::string make_trait_string();

};

REGISTER_EXPORTER(FaultToleranceUppaalExporter, "uppaal-ft")

#endif


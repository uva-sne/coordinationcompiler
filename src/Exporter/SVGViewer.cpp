/* 
 * Copyright (C) 2017 Benjamin Rouxel <benjamin.rouxel@inria.fr> 
 *                    Irisa/INRIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "SVGViewer.hpp"
#include "CriticalChain.hpp"

using namespace std;

IMPLEMENT_HELP(SVGViewer,
    Export the generated schedule into a SVG file.\n
    - filename, coordFileDir/appname.svg: name with or without absolute path for the generated file.\n
)

void SVGViewer::forward_params(const map<string, string> &args) {
    if(args.count("filename") && !args.at("filename").empty())
        filename(args.at("filename"));

    if(args.count("force_length"))
        force_length = Utils::stringtime_to_ns(args.at("force_length"));

    //assign_color_to_iter = true;
}

void SVGViewer::setAuxilliaryParams(std::string key, const std::map<std::string, std::string> &args) {
    if(key == "marker") {
        if(args.count("time") == 0 || args.count("color") == 0)
            throw CompilerException("svg", "Missing time or color for the SVG marker");

        markers.push_back(make_pair(Utils::stringtime_to_ns(args.at("time")), args.at("color")));
    }
}

SVGViewer::SVGViewer() : Exporter() {}

const string SVGViewer::get_uniqid_rtti() const { return "generator-svg"; }

void SVGViewer::check_dependencies() {
    if(!force_unschedulable() && sched->status() != ScheduleProperties::sched_statue_e::SCHED_COMPLETE && sched->status() != ScheduleProperties::sched_statue_e::SCHED_PARTIAL)
        throw CompilerException("export schedule", "No schedule have been generated.");
}

void SVGViewer::do_export(const boost::filesystem::path& fn) {
    string sched_gen_label = "";
    auto &passes = conf->passes.passes();
    auto et = find_if(passes.rbegin(), passes.rend(), [this](const auto &p) {return p.get() == this;});
    auto it = find_if(et, passes.rend(), [](const auto &p) { return p->get_uniqid_rtti().find("solver") != string::npos; });
    if(it != passes.rend()) {
        Solver *s = (Solver*) it->get();
        sched_gen_label = s->type()+"/"+Utils::demangle(typeid(*s).name());
    }
    else {
        it = find_if(et, passes.rend(), [](const auto &p) { return p->get_uniqid_rtti().find("simulator-sched") != string::npos; });
        if(it != passes.rend()) {
            Simulator *s = (Simulator*) it->get();
            sched_gen_label = Utils::demangle(typeid(*s).name());
        }
    }

    it = find_if(et, passes.rend(), [](const auto &p) { return p->get_uniqid_rtti() == "analyse-critical_chain"; });
    if(it != passes.rend()) {
        CriticalChain *pass_critic_chain = (CriticalChain*) it->get();
        critical_chain = pass_critic_chain->get_critical_chain();
    }

    // ns per 1 of the base unit, we have enough precision here that we can convert everything to ns (max is 5 * 10^6 hours)
    this->time_scale = Utils::stringtime_to_ns("1" + tg->time_unit());
    string timeunit = "ns";
    if(sched->makespan() > 100000 / time_scale) /*100us*/
        timeunit = "us";
    if(sched->makespan() > 100000000 / time_scale) /*100ms*/
        timeunit = "ms";
    if(sched->makespan() > 100000000000 / time_scale) /*100s*/
        timeunit = "s";


    ofstream out(fn.string());

    uint32_t nbP = sched->schedcores().size();

    uint32_t startx = 50, starty = 80,
            fontsize=12
            ;

    double makespan = sched->makespan();
    if(force_length > 0)
        makespan = force_length;

    uint32_t width = 1500;
    uint32_t axey = 350;
    uint32_t proc_line_separator = axey / nbP;
    uint32_t proc_lineheight = 30;



    timinginfos_t time_step = 10;
    float pixel_step = time_step*width / makespan;
    if(pixel_step < 10) {
        do {
            pixel_step = time_step*width / makespan;
            time_step *= 10;
        }
        while(pixel_step < 10);
    }


    Utils::color_t base_color = {
        .r = 53,
        .g = 122,
        .b = 255,
        .a = 255
    };
    for (Task *t : tg->tasks_it()) {
        Utils::color_t c = {
            .r = base_color.r,
            .g = base_color.g,
            .b = base_color.b,
            .a = base_color.a,
        };
        colors[t] = c;
        base_color.r = (base_color.r + 31) % 256;
        base_color.g = (base_color.g + 13) % 256;
        base_color.b = (base_color.b + 7) % 256;
    }

    base_color = {
        .r = 53,
        .g = 122,
        .b = 255,
        .a = 255
    };
    for(SchedElt *st : sched->scheduled_elements()) {
        if(!colors_per_iter.count(st->min_rt_period())) {
            Utils::color_t c = {
                .r = base_color.r,
                .g = base_color.g,
                .b = base_color.b,
                .a = base_color.a,
            };
            colors_per_iter[st->min_rt_period()] = c;
            base_color.r = (base_color.r + 40) % 256;
            base_color.g = (base_color.g - 101) % 256;
            base_color.b = (base_color.b + 201) % 256;
        }
    }

    uint32_t linenum = 1;
    uint32_t lineheight = fontsize+5;

    out << "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>" << endl;
    out << "<svg id=\"svgImage\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:svg=\"http://www.w3.org/2000/svg\" ";
    out << "height=\"" << starty+axey*1.2+20 + ((fontsize+20)*nbP)+ (lineheight+10)*(sched->stats().size()+4)+100 << "px\" ";
    out << "width=\"" << width+100 << "px\" ";
    out << "viewBox=\"0 0 100% 100%\" ";
    out << "version=\"1.1\"> " << endl;

    out << add_css(fontsize) << endl;
    out << add_js() << endl;

    size_t i=0;
    for(pair<timinginfos_t, Utils::color_t> el : colors_per_iter) {
        out << "<rect x=\"" << (500+ 20*i+10) << "\" y=\"" << linenum*lineheight << "\" width=\"20\" height=\"20\" style=\"fill:" << Utils::color_to_html(el.second) << "\" />" << endl;
        ++i;
    }

    out << "<text x=\"10\" y=\"" << linenum*lineheight << "\">Status: </text>";
    out << "<text class=\"highlight\" x=\"70\" y=\"" << linenum*lineheight << "\">";
    out << ScheduleProperties::to_string(sched->status());
    out << "</text>" << endl;
    ++linenum;

    /* Check if the schedule has a specified energy usage. */
    if (sched->energy()) {
        out << "<text x=\"10\" y=\"" << linenum*lineheight << "\">Energy Usage: </text>";
        out << "<text class=\"highlight\" x=\"110\" y=\"" << linenum*lineheight << "\">";
        out << to_string(sched->energy()) + " " + tg->energy_unit();
        out << "</text>" << endl;
        ++linenum;
    }

    out << "<text x=\"10\" y=\"" << linenum*lineheight << "\">Solve Time: </text>";
    out << "<text class=\"highlight\" x=\"90\" y=\"" << linenum*lineheight << "\">";
    out << Utils::nstime_to_string(sched->solvetime(), "s") << " secondes";
    out << "</text>" << endl;
    ++linenum;

    if(!sched_gen_label.empty()) {
        out << "<text x=\"10\" y=\"" << linenum*lineheight << "\">Method: </text>";
        out << "<text class=\"highlight\" x=\"70\" y=\"" << linenum*lineheight << "\">";
        out << sched_gen_label;
        out << "</text>" << endl;
        ++linenum;
    }

    if(conf->archi.interconnect.type != "NONE") {
        out << "<text x=\"10\" y=\"" << linenum*lineheight << "\">Communication mode: </text>";
        out << "<text class=\"highlight\" x=\"160\" y=\"" << linenum*lineheight << "\">";
        out << conf->archi.interconnect.type << "/" << conf->archi.interconnect.arbiter << " -- " << conf->archi.interconnect.mechanism
            << "/" << conf->archi.interconnect.behavior << " -- active window time: " << conf->archi.interconnect.active_ress_time;
        out << "</text>";
        ++linenum;
    }

    out << "<text x=\"10\" y=\"" << linenum*lineheight << "\">Length: </text>";
    out << "<text class=\"highlight\" x=\"70\" y=\"" << linenum*lineheight << "\">";
    out << Utils::nstime_to_string(sched->makespan() * time_scale, timeunit) << " " << timeunit << " (" << sched->makespan() << " " << tg->time_unit() << ")";
    out << "</text>" << endl;
    ++linenum;

    out << "<text x=\"10\" y=\"" << linenum*lineheight << "\">Hyperperiod: </text>";
    out << "<text class=\"highlight\" ";
    out << "x=\"100\" y=\"" << linenum*lineheight << "\">";
    out << Utils::nstime_to_string(tg->hyperperiod() * time_scale, timeunit) << " " << timeunit << " (" << tg->hyperperiod() << " " << tg->time_unit() << ")";
    out << "</text>" << endl;
    ++linenum;

    starty = linenum*lineheight;
    string taskdesc = "";
    linenum = 0;
    vector<SchedCore*> cores = sched->schedcores();
    sort(cores.begin(), cores.end(), [](SchedCore *a, SchedCore *b) { return a->core()->id < b->core()->id;});
        for(SchedCore *core : cores) {
            string procid = core->core()->id;
            uint32_t y = starty + proc_lineheight + linenum * (proc_line_separator);
            out << "<text x=\"1\" y=\""<< y << "\" style=\"font-weight:bold;\" >";
            out << procid;
            out << "</text>" << endl;

            for(SchedElt *elt : sched->scheduled_elements(core)) {
                if(elt->rt() > makespan)
                    continue;

                taskdesc += add_task_desc(elt, timeunit);

                string tid = elt->schedtask()->task()->id();

                if(Utils::isa<SchedPacket*>(elt) && ((SchedPacket*)elt)->dirlbl() == "r") {
                    double boxX = startx+(elt->rt()*width)/makespan;
                    double boxY = y-proc_lineheight/2;
                    out << "<rect id=\"p" << tid << "_c\" style=\"fill:" << get_color(elt);
                    out << ";stroke:#000000;stroke-opacity:1;stroke-width:0.4;\" "; // rgb(183, 255, 80) stroke:grey;stroke-width:1px;stroke-opacity:0.8
                    out << "width=\"" << (elt->wct() * width)/makespan << "\" height=\"" << proc_lineheight/4 << "\" ";
                    out << "x=\"" << boxX << "\" y=\"" << boxY << "\" />";

                            out << "<text id=\"tcr" << tid << "_c\" ";
                            out << "style=\"font-size:" << fontsize/2 << "px;fill:rgb(17, 86, 17);\" ";
                    out << "x=\"" << boxX << "\" y=\"" << boxY << "\">";
                    out << ((SchedPacket*)elt)->data() << "</text>" << endl;
                } 
                else if(Utils::isa<SchedPacket*>(elt) && ((SchedPacket*)elt)->dirlbl() == "w") {
                    double boxX = startx+(elt->rt()*width)/makespan;
                    double boxY = y-proc_lineheight/2+proc_lineheight;
                    out << "<rect id=\"p" << tid << "_c\" style=\"fill:" << get_color(elt);
                    out << ";stroke:#000000;stroke-opacity:1;stroke-width:0.4;\" "; // rgb(255, 249, 185) stroke:grey;stroke-width:1px;stroke-opacity:0.8
                    out << "width=\"" << (elt->wct() * width)/makespan << "\" height=\"" << proc_lineheight/4 << "\" ";
                    out << "x=\"" << boxX << "\" y=\"" << boxY << "\" />";

                    out << "<text id=\"tcs" << tid << "_c\" ";
                    out << "style=\"font-size:" << fontsize/2 << "px;fill:rgb(17, 86, 17);\" ";
                    out << "x=\"" << boxX << "\" y=\"" << boxY << "\">";
                    out << ((SchedPacket*)elt)->data() << "</text>" << endl;
                }
                else if(Utils::isa<SchedJob*>(elt)) {

                    for(pair<timinginfos_t, timinginfos_t> ec : elt->schedtask()->exec_chunks()) {
                        timinginfos_t rt = ec.first;
                        timinginfos_t wct = ec.second;
                        double boxX = startx+(rt*width)/makespan;
                        double boxY = y-proc_lineheight/2;
                        out << "<rect id=\"r" << tid << "_" << elt->schedtask()->min_rt_period() << "\" class=\"rect_task\" style=\"fill:" << get_color(elt) << "\" ";
                        out << "width=\"" << (wct*width)/makespan << "\" height=\"" << proc_lineheight << "\" ";
                        out << "x=\"" << boxX << "\" y=\"" << boxY << "\"/>" << endl;

                        out << "<text x=\"" << boxX << "\" y=\"" << (boxY+20) << "\">"
                                << elt->schedtask()->task()->id() << "</text>"
                                << endl; // TODO: this will break when task names contain XML characters

//                        if(elt->schedtask()->min_rt_period() > 0) {
//                            out << "<path d=\"M"<< startx+(elt->schedtask()->min_rt_period()*zoomx)/step << " " 
//                                << y-proc_line_height << "l-6 7-0.5-1 7-9 7 9-0.5.1-6-7v30h-1v-1000z\"/>";
//                            out << "<text id=\"tt" << tid << "\" ";
//                            out << "style=\"font-size:" << fontsize*0.8 << "px;fill:#f10000;\" ";
//                            out << "x=\"" << startx+(elt->schedtask()->min_rt_period()*zoomx)/step-10 << "\" y=\"" << y+fontsize*0.8  << "\" ";
//                            out << "text-anchor=\"bottom\" transform=\"rotate(90," << startx+(elt->schedtask()->min_rt_period()*zoomx)/step-10+5 << ", " << y+fontsize*0.8+5 << ")\">";
//                            out << tid << "</text>" << endl;
//                        }
//                        
//                        if(elt->schedtask()->min_rt_deadline() > 0) {
//                            if(elt->schedtask()->min_rt_period() != elt->schedtask()->min_rt_deadline()) {
//                                out << "<text id=\"tt" << tid << "\" ";
//                                out << "style=\"font-size:" << fontsize*0.8 << "px;fill:#f10000;\" ";
//                                out << "x=\"" << startx+(elt->schedtask()->min_rt_deadline()*zoomx)/step-10 << "\" y=\"" << y-proc_line_height-5 << "\" ";
//                                out << "text-anchor=\"bottom\" transform=\"rotate(90," << startx+(elt->schedtask()->min_rt_deadline()*zoomx)/step-10 << ", " << y-proc_line_height-5 << ")\">";
//                                out << tid << "</text>" << endl;
//                            }
//                            out << "<path d=\"M"<< startx+(elt->schedtask()->min_rt_deadline()*zoomx)/step << " " 
//                                << y << "l-6.235-7-0.75.6 7.5 9 7.5-9-.8-.6-6.2 7v-30h-1v1000z\"/>";
//                        }
                    }
                }
                else
                    throw CompilerException("SVG generator", "Can't find type of elt: "+elt->toString());
            }

        linenum++;
    }
    out << "<path class=\"axis\" d=\"m " << startx << ".0, " << starty << ".0 0," << axey << ".0\" />" << endl;
    out << "<path class=\"axis\" d=\"m " << startx << ".0, "<< (axey+starty) << " " << width << ".0,0\" />" << endl;

    for(uint32_t i=0 ; i < (width/pixel_step)+1 ; ++i) {
        out << "<path class=\"axis grid\" d=\"m " << startx+i*pixel_step << ", " << starty << ".0 0," << axey << ".0\" />" << endl;

        out << "<text ";
        out << "text-anchor=\"start\" transform=\"translate(" << startx+i*pixel_step << ", " << axey+starty+10 << ") rotate(45)\" ";
        out << "style=\"font-size:" << fontsize/2 << "px;fill:#000;\" ";
        out << ">";
        out << i*time_step << "</text>" << endl;
    }

    for(pair<timinginfos_t, string> el : markers) {
        for(size_t i=1 ; i*el.first < makespan ; ++i) {
            out << "<path class=\"axis marker\" style=\"stroke:" << el.second << "\" d=\"m " << startx+(((i*el.first) / (time_step/10)) * pixel_step) << ", " << starty << ".0 0," << axey << ".0\" />" << endl;
        }
    }

    out << stats(axey+starty+10+40, fontsize, floor((width-100)/2.0)) << endl;
    
    out << spmalloc(ceil((width-100)/2.0), axey+starty+10+40, floor((width-100)/2.0)) << endl;

    out << taskdesc;
    out << "</svg>" << endl;

    out.close();
    Utils::INFO("SVG file saved in "+fn.string());
}

string SVGViewer::add_js() {
    return R"CPPRAWMARKER(<script type="text/javascript">
// <![CDATA[
window.addEventListener('load', function () {
    var elts = document.getElementsByClassName("rect_task");
    for (var i = 0; i < elts.length; i++) {
        elts[i].addEventListener('mouseover', function (event) {
            var taskid = "tt" + event.target.id.substr(1, event.target.id.length);
            var task = document.getElementById(taskid);
            console.log(event);
            if (!task)
                console.log("Task " + taskid + " not found");
            else {
                task.style.display = "block";
                var txts = task.getElementsByTagName("text");
                var width_max = 0;
                for (var j = 0; j < txts.length; ++j) {
                    if (width_max < txts[j].getComputedTextLength())
                        width_max = txts[j].getComputedTextLength();
                }
                width_max += 20;
                task.setAttribute("width", width_max);

                var newX;
                if (event.pageX + width_max > window.innerWidth)
                    newX = Math.round(event.pageX - width_max - 10);
                else
                    newX = event.pageX;

                task.setAttribute("x", newX);
                task.setAttribute("y", event.target.y.baseVal.value - 5);


                /*task.style.display = "block";
                var newX;
                if(event.pageX + task.getComputedTextLength() > window.innerWidth)
                    newX = event.pageX - (event.pageX + task.getComputedTextLength() - window.innerWidth) - 10;
                else
                   newX = event.pageX;
                task.transform.baseVal.getItem(0).setTranslate(newX,event.target.y.baseVal.value - 5);
                */
            }
        });
        elts[i].addEventListener('mouseout', function (event) {
            var taskid = "tt" + event.target.id.substr(1, event.target.id.length);
            var task = document.getElementById(taskid);
            if (!task)
                console.log("Task " + taskid + " not found");
            else {
                var oldX = parseInt(task.getAttribute("x"));
                var oldY = parseInt(task.getAttribute("y"));
                var width = parseInt(task.getAttribute("width"));
                var height = parseInt(task.getAttribute("height"));

                if (event.pageX < oldX - 5 || event.pageX > oldX + width + 5 || event.pageY < oldY - 5 || event.pageY > oldY + height + 5) {
                    task.style.display = "none";
                }
            }
        });

    }
    elts = document.getElementsByClassName("task_desc");
    for (var i = 0; i < elts.length; ++i) {
        elts[i].addEventListener('mouseleave', function (event) {
            event.target.style.display = "none";
            console.log("trigger");
        });
    }

});
// ]]>
</script>
)CPPRAWMARKER";
}

string SVGViewer::add_css(uint32_t fontsize) {
    return R"CPPRAWMARKER(<style>
.task_desc {
    display:none;
}
* {
    font-weight:normal;
    font-size: )CPPRAWMARKER"+to_string(fontsize)+R"CPPRAWMARKER(px;
    white-space: preserve;
    fill:#000;
    fill-opacity:1;
}
.highlight {
    font-weight:bold;
    fill:#F00;
}
.rect_task {
    fill-opacity:0.7;
}
.tdesc_rec {
    fill:#e2e2e3;fill-opacity:1;stroke:#18425b;stroke-width:1;
}
.axis {
    fill:none;
    stroke:#000000;
    stroke-width:1px;
    stroke-opacity:1;
}
.marker {
    stroke-width:2px;
    stroke-opacity:1;
}
.grid {
    stroke-miterlimit:4;
    stroke-dasharray:1,8;
    stroke-dashoffset:0;
}
            #stats table {
            border: 1px solid black;
            border-collapse: collapse;
            }
#stats td {
            font-weight: bold;
            padding: 2px 10px;
}
#stats tr:nth-child(even) {background: #CCC}
#stats tr:nth-child(odd) {background: #FFF}
</style>
)CPPRAWMARKER";
}

string SVGViewer::add_task_desc(SchedElt *elt, const string &timeunit) {
    string str =
"<rect class=\"tdesc_rec\" x=\"0\" y=\"0\" width=\"100%\" height=\"100%\"/>"
"<text x=\"10px\" y=\"15px\">"+elt->schedtask()->task()->id();
    if(!elt->schedtask()->selected_version().empty())
        str += "("+elt->schedtask()->selected_version()+")";
    str += "</text>";

    size_t x = 32;
    for(pair<uint64_t, uint64_t> ec : elt->schedtask()->exec_chunks()) {
        str +=
"<text y=\""+to_string(x)+"px\" style=\"\" x=\"20px\">start: "+Utils::nstime_to_string(ec.first * time_scale, timeunit)+" "+timeunit+" ("+to_string(ec.first)+" "+tg->time_unit()+")</text>"
"<text y=\""+to_string(x+12+5)+"px\" x=\"20px\">end: "+Utils::nstime_to_string((ec.first+ec.second) * time_scale, timeunit)+" "+timeunit+" ("+to_string(ec.first+ec.second)+" "+tg->time_unit()+")</text>"
"<text y=\""+to_string(x+2*(12+5))+"px\" x=\"20px\">WCET: "+Utils::nstime_to_string(ec.second * time_scale, timeunit)+" "+timeunit+" ("+to_string(ec.second)+" "+tg->time_unit()+")</text>"
;
        x += 3*(12+5);
    }
    str += "<text y=\""+to_string(x)+"px\" x=\"20px\">priority: "+to_string(elt->schedtask()->priority())+"</text>";
    x += 12+5;

    for(Version *elv : elt->schedtask()->task()->versions()) {
        if(!elv->id().empty()) {
            str +=
"<text y=\""+to_string(x)+"px\" x=\"20px\">- "+elv->id()+"</text>";
            x += 12+5;
        }
    }

    return "<svg xmlns=\"http://www.w3.org/2000/svg\" height=\""+to_string(x+12)+"\" class=\"task_desc\""
            " id=\"tt"+elt->schedtask()->task()->id()+"_"+to_string(elt->schedtask()->min_rt_period())+"\" x=\"0\" y=\"0\" width=\"0\">"
            + str + "</svg>";
}

string SVGViewer::stats(size_t y, uint32_t fontsize, uint32_t width) {
    if(sched->stats().size() == 0)
        return "";

    string stats = "<foreignObject x=\"10\" y=\""+to_string(y)+"\" height=\""+to_string(2*fontsize*sched->stats().size())+"\" width=\""+to_string(width)+"px\">"
           "<body xmlns=\"http://www.w3.org/1999/xhtml\" id=\"stats\">"
           "<h1>Statistiques</h1>"
           "<table>\n";
    for(pair<string, string> el : sched->stats()) {
        stats +="<tr><td>"+el.first+":</td><td>"+el.second+"</td></tr>\n";
    }
    stats += "</table>"
           "</body>"
           "</foreignObject>\n";
    return stats;
}

string SVGViewer::get_color(SchedElt *elt) {
    if(find_if(critical_chain.begin(), critical_chain.end(), [elt](SchedElt *a) {return elt->schedtask()->task() == a->schedtask()->task() && elt->rt() == a->rt();}) != critical_chain.end())
        return "#FF0000";
        
    if(assign_color_to_iter)
        return Utils::color_to_html(colors_per_iter[elt->min_rt_period()]);
    
    return Utils::color_to_html(colors[elt->schedtask()->task()]);
}

string SVGViewer::spmalloc(int startx, int starty, uint32_t width) {
    if(!conf->archi.spm.assign_region)
        return "";
    
    ostringstream str;
    int y = starty;//axex+fontsize/2+50;
    int height = 20;
    int actualwidth = ceil(width / 0.8);
    for(SchedCore *p : sched->schedcores()) {
        str << "<text xml:space=\"preserve\" ";
        str << "style=\"fill:#000;fill-opacity:1\" ";
        str << "x=\"" << startx << "\" y=\"" << y << "\">";
        str << " SPM occupancy of " << p->core()->id << "</text>" << endl;

        str << "<rect id=\"spm" << p->core()->id << "\" style=\"fill-opacity:0;stroke-width:1; stroke-opacity:1;stroke:#000;\" ";
        str << "width=\"" << actualwidth << "\" height=\"" << height << "px\" ";
        str << "x=\"" << startx << "\" y=\"" << (y+5) << "\" />" << endl;

        Utils::color_t base_color = {
            .r = 53,
            .g = 122,
            .b = 255,
            .a = 255
        };

        float x = startx+1;
        for(SchedSpmRegion *r : p->memory_regions()) {
            if(r->size() == 0) continue;
            float width = r->size() * actualwidth / (float) p->total_spm_size();

            str << "<rect id=\"sr" << r->label() << "\" style=\"fill:" << Utils::color_to_html(base_color) << ";fill-opacity:1;stroke-width:0;\" ";
            str << "width=\"" << width << "\" height=\"" << height-2 << "\" ";
            str << "x=\"" << x << "\" y=\"" << (y+5+1) << "\" />" << endl;

            x += width+0.1;

            base_color.g = (base_color.g + 40) % 256;
        }
        y += height+25;
    }
    
    return str.str();
}
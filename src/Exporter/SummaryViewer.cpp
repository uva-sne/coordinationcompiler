/*
 * Copyright (C) 2020 Lukas Miedema <l.miedema@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "SummaryViewer.hpp"
#include "StateMachine/FaultTolerance/FaultToleranceStrategy.hpp"

IMPLEMENT_HELP(SummaryViewer,
    Writes a textual summary of the schedule to a file.\n
    The summary details properties like makespan as well as validation information.\n
    The output of this file is stable, and can be used by diff tools to detect changes and bugs,
    including variances between different ideal solvers.\n
    filename [string]: optional name for the generated file. Omit to write to stdout.
)

void SummaryViewer::forward_params(const std::map<std::string, std::string> &args) {
    if(args.count("filename") && !args.at("filename").empty())
        filename(args.at("filename"));
}

SummaryViewer::SummaryViewer() : Exporter() {}

const std::string SummaryViewer::get_uniqid_rtti() const { return "exporter-summary"; }

void SummaryViewer::do_export(const boost::filesystem::path& fn) {
    std::ofstream fout(fn.string());
    do_export(fout);
    fout.close();
    Utils::INFO("Textual summary written to '" + fn.string() + "'");
}

void SummaryViewer::do_export(std::ostream& out) {
    std::string scheduler_name = "missing";
    auto &passes = conf->passes.passes();
    auto et = find_if(passes.rbegin(), passes.rend(), [this](auto &p) {return p->get_uniqid_rtti() == this->get_uniqid_rtti();});
    auto it = find_if(et, passes.rend(), [](auto &p) { return p->get_uniqid_rtti().find("solver") != std::string::npos; });
    if(it != passes.rend()) {
        auto *s = dynamic_cast<Solver *>(it->get());
        scheduler_name = s->type() + "/" + Utils::demangle(typeid(*s).name());
    }

    const Schedule *schedule = this->sched;

    out << "--- Schedule details ---" << std::endl;
    out << "Status:    " << (this->sched == nullptr ? "missing" : ScheduleProperties::to_string(schedule->status())) << std::endl;
    out << "Scheduler: " << scheduler_name << std::endl;
    out << "Makespan:  " << (this->sched == nullptr ? "missing" : to_string(schedule->makespan())) << std::endl;
    out << "Cores:     " << (this->sched == nullptr ? "missing" : to_string(schedule->schedcores().size())) << std::endl;
    out << "Tasks:     " << (this->sched == nullptr ? "missing" : to_string(boost::size(schedule->schedjobs()))) << std::endl;
    out << std::endl;

    if (this->sched != nullptr) {
        out << "--- Schedule ---" << std::endl;
        for (SchedCore *core: schedule->schedcores()) {
            out << "Core [id = " << core->core()->id << ", type=" << core->core()->type << "]" << std::endl << "  ";
            for (SchedElt *element: schedule->scheduled_elements(core)) {
                if (element->type() == SchedElt::TASK)
                    out << element->task()->id() << " [start=" << element->rt() << ", end=" << (element->rt() + element->wct()) << "] ";
                else
                    out << "Packet [start=" << element->rt() << ", end=" << (element->rt() + element->wct()) << "] ";
            }
            out << std::endl;
        }
    }
    out << std::endl;

    out << "--- Schedule validation ---" << std::endl;
    ScheduleValidationAnalyzer sva;
    sva.report_function([&out](bool &all_ok, bool property_ok, const std::string &label) {
        out << (property_ok ? "[OKAY] " : "[FAIL] ") << label << std::endl;
        all_ok &= property_ok;
    });
    sva.run(const_cast<SystemModel*>(tg), const_cast<config_t*>(conf), const_cast<Schedule*>(schedule));
    out.flush();
}

void SummaryViewer::check_dependencies() {
    // No dependencies: if there is no solver pass, it will just report that
}

void SummaryViewer::do_export() {
    if(!_filename.empty())
        if (_filename.at(0) == '/') // is absolute path?
            do_export(_filename);
        else
            do_export(conf->files.output_folder / _filename);
    else
        do_export(std::cout);
}
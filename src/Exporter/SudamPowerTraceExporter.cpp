/*
 * Copyright (C) 2023 Lukas Miedema <l.miedema@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "SudamPowerTraceExporter.hpp"

IMPLEMENT_HELP(SudamPowerTraceExporter,
Export a power trace for each task + core combination that is compatible with Sudams reliability simulator.\n
The power traces used there are time series of power values, with one data point every 100ns.
Cecile does not have such data, so instead we compute average power over the WCET of each task and write a constant
power trace.)

void SudamPowerTraceExporter::do_export(const boost::filesystem::path &output_dir) {
    if (tg->energy_unit() != "nJ" || tg->time_unit() != "ns")
        throw CompilerException("power-trace-exporter", "Only supports ns as time unit and nJ as energy unit");

    if (boost::filesystem::exists(output_dir)) {
        if (!boost::filesystem::is_directory(output_dir)) {
            Utils::ERROR(output_dir.string() +
                    " already exists and is not a directory. Cannot generate power traces.");
            return;
        }
    }

    for (Task *t : this->tg->tasks_it()) {
        std::set<std::string> seenArches;
        for (ComputeUnit *cu : tg->processors()) {
            if (seenArches.find(cu->type) != seenArches.end())
                continue;
            seenArches.insert(cu->type);

            // Find a version that is compatible with either this CU, or a CU of the same type
            Version *needle = nullptr;

            auto cuDir = output_dir / cu->type;
            boost::filesystem::create_directories(cuDir);

            for (Version *v : t->versions()) {
                if (std::any_of(v->force_mapping_proc().begin(),  v->force_mapping_proc().end(),
                        [&cu](ComputeUnit *cu_other) { return cu->type == cu_other->type;})) {
                    if (needle != nullptr)
                        throw CompilerException("aroma-generator", "AXF does not support multiple versions for the same processor arch, task "
                                + t->id() + ", versions [" + needle->id() + ", " + v->id() + "] for arch = " + cu->type);
                    needle = v;
                }
            }
            if (needle == nullptr)
                Utils::WARN("Missing power data: skipping power trace generation for task " + t->id() + " and core type " + cu->type);
            else {
                boost::filesystem::path traceFile = cuDir / (t->id() + ".ptrace");

                timinginfos_t time = needle->C(); // in ns
                double energy = needle->C_E(); // in J
                double avgpower = ((double) energy) / (((double) time));
                writeTraceFile(traceFile, time, avgpower);
            }
        }
    }
}

void SudamPowerTraceExporter::writeTraceFile(boost::filesystem::path &file, timinginfos_t time, double avgpower) {
    Utils::INFO("Writing " + file.string());
    std::ofstream ofs(file.string());
    // time is in ns, need 100ns steps + 1
    uint64_t steps = time / 100 + 1;
    for (uint64_t step = 0; step < steps; step++) {
        ofs << avgpower << "\n";
    }
    ofs.close();
}

void SudamPowerTraceExporter::forward_params(const std::map<std::string, std::string> &args) {
    if(args.count("dir") && !args.at("dir").empty())
        this->filename(args.at("dir"));
}
/* 
 * Copyright (C) 2020 Benjamin Rouxel <benjamin.rouxel@uva.nl> 
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TEXVIEWER_H
#define TEXVIEWER_H

#include "Exporter.hpp"
#include "Solvers/Solver.hpp"
#include "Simulator/Simulator.hpp"
#include "boost/filesystem.hpp"
#include "Utils/Color.hpp"

/**
 * Export a standalone TeX with a tikzpicture of the schedule
 * 
 * @todo multicore
 */
class TexViewer : public Exporter {
public:
    float scale = 1.0f; //!< Scale of the generated picture
    uint32_t step = 1; //!< How many time units a step is
    size_t color_index = 0; //!< Current color to use \see Utils/Color.hpp
    
    void forward_params(const std::map<std::string, std::string> &args) override;
    std::string help() override;
    
    explicit TexViewer();
    const std::string get_uniqid_rtti() const override;
    void check_dependencies() override;

protected:
    virtual void do_export(const boost::filesystem::path& fn) override;
    virtual const std::string file_extension() override { return "tex"; }
    
private:
    /**
     * Return the header of the TeX file
     * @return 
     */
    std::string header();
    /**
     * Some commands to make the drawing rendered by TeX
     * @return 
     */
    std::string commands();
    /**
     * Start of the document
     * @return 
     */
    std::string start_document();
    /**
     * End of the document
     * @return 
     */
    std::string end_document();
    /**
     * Start the picture
     * @return 
     */
    std::string start_tikz();
    /**
     * Close the picture
     * @return 
     */
    std::string end_tikz();
    /**
     * Return the information required to have a task rendered in the picture
     * @param t
     * @param height
     * @return 
     */
    std::string task_infos(Task *t, int height);
};

REGISTER_EXPORTER(TexViewer, "tex")

#endif
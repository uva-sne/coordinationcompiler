/* 
 * Copyright (C) 2017 Benjamin Rouxel <benjamin.rouxel@inria.fr> 
 *                    Irisa/INRIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "ResXMLViewer.hpp"

using namespace std;

ResXMLViewer::ResXMLViewer() : Exporter() {}

void ResXMLViewer::check_dependencies() {
    if(!force_unschedulable() && sched->status() != ScheduleProperties::sched_statue_e::SCHED_COMPLETE && sched->status() != ScheduleProperties::sched_statue_e::SCHED_PARTIAL)
        throw CompilerException("export schedule", "No schedule have been generated.");
}

void ResXMLViewer::forward_params(const map<string, string> &args) {
    if(args.count("filename") && !args.at("filename").empty())
        filename(args.at("filename"));
}
const string ResXMLViewer::get_uniqid_rtti() const { return "generator-xml"; }
IMPLEMENT_HELP(ResXMLViewer ,
    Export the generated schedule into a XML file.
)

void ResXMLViewer::do_export(const boost::filesystem::path& fn) {
    ofstream out(fn.string());
    out << "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>" << endl;
    out << "<schedule processors=\"" << tg->processors().size() << 
            "\" time=\"" << sched->solvetime() << 
            "\" status=\"" << ScheduleProperties::to_string(sched->status()) << 
            "\" length=\"" << sched->makespan() << "\" ";
    out << ">" << endl;
    if(sched->status() < ScheduleProperties::sched_statue_e::SCHED_UNSCHEDULABLE) {
        for(SchedElt *st : sched->scheduled_elements()) {
            if(!Utils::isa<SchedJob*>(st))
                continue;
            
            const Task *t = st->schedtask()->task();
            SchedCore *core = sched->get_mapping(st);
            if(core == nullptr)
                throw CompilerException("XML generator", "Can't find mapping for task "+*t);

            out << "    <job id=\"" << st->id() << "\" task=\"" << t->id() << "\"" ;
            out << " version=\"" << st->schedtask()->selected_version() << "\"";
            out << " release=\"" << st->min_rt_period() << "\"" ;
            out << " start=\"" << st->rt() << "\"";
            out << " exectime=\"" << st->wct() << "\"";
            out << " endtime=\"" << (st->rt() + st->wct()) << "\"";
            out << " core=\"" << core->core()->id << "\"";
            out << ">";
            if(st->schedtask()->packet_list().size() > 0)
                out << endl;
            
            for(SchedPacket *pac : st->schedtask()->packet_list()) {
                if(pac == nullptr) continue;
                out << "        <transmission type=\"" << pac->dirlbl() << "\" ";
                out << "release=\"" << pac->rt() << "\" ";
                out << "delay=\"" << pac->wct() <<"\" ";
                out << "job=\"" << pac->tofromschedtask()->id() <<"\" ";
                out << "data=\""<< pac->data() <<"\" ";
                out << "datatype=\"" << pac->datatype() <<"\" ";
                out << "conc=\"" << pac->concurrency() <<"\" ";
                out << "packetnum=\"" << pac->packetnum() << "\" ";
                out << "/>" << endl ;
            }
            
            out << "    </job>" << endl;
        }

        for(SchedCore *p : sched->schedcores()) {
            out << "    <core id=\"" << p->core()->id << "\">" << endl;
            
            for(SchedSpmRegion *sp : p->memory_regions()) {
                if(sp->booking().size() == 0) continue;
                out << "        <memory_region id=\"" << sp->label() << "\" ";
                out << " size=\"" << sp->size() << "\">" << endl;
                for(SchedResource::booking_elt_t b : sp->booking()) {
                    out << "            <booked by=\"" << b.elt << "\" ";
                    out << " start=\"" << b.start << "\" ";
                    out << " end=\"" << b.end << "\" ";
                    out << "/>" << endl;
                }
                out << "        </memory_region>" << endl;
            }
            
            out << "    </core>" << endl;
        }
        
        for(SchedResource *r : sched->schedresources()) {
            if(r->booking().size() == 0) continue;
            
            out << "\t<shared_resource id=\"" << r->label() << "\">" << endl;
            for(SchedResource::booking_elt_t b : r->booking()) {
                out << "\t\t<booked by=\"" << b.elt << "\" ";
                    out << " start=\"" << b.start << "\" ";
                    out << " end=\"" << b.end << "\" ";
                out << "/>" << endl;
            }
            out << "\t</shared_resource>" << endl;
        }
        
        for(pair<string, string> st : sched->stats()) {
            out << "\t<stat label=\"" << st.first << "\" value=\"" << st.second << "\" />" << endl;
        }
    }

    out << "</schedule>" << endl;
    out.close();
    Utils::INFO("XML File saved in "+fn.string());
}

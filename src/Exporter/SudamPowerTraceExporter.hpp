/*
 * Copyright (C) 2023 Lukas Miedema <l.miedema@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once


#include "Exporter.hpp"

/**
 * Generate a power trace that is compatible with Sudams reliability simulator. The power traces used there
 * are time series of power values, with one data point every 100ns. Cecile does not have such data, so instead
 * we compute average power over the WCET of each task and write a constant power trace.
 */
class SudamPowerTraceExporter : public Exporter {
public:
    const std::string get_uniqid_rtti() const override {
        return "power-trace-exporter";
    }

    std::string help() override;

protected:
    void forward_params(const std::map<std::string, std::string> &map) override;
    void do_export(const boost::filesystem::path &f) override;

    const std::string file_extension() override {
        return ""; // it's a dir
    }

    void writeTraceFile(boost::filesystem::path &file, timinginfos_t time, double avgpower);
};

REGISTER_EXPORTER(SudamPowerTraceExporter, "power-trace-generator");

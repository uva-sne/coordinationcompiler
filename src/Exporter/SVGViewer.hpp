/*!
 * \file SVGViewer.hpp
 * \copyright GNU Public License v3.
 * \date 2017 
 * 
 * Copyright (C) 2017 Benjamin Rouxel <benjamin.rouxel@inria.fr> 
 *                    Irisa/INRIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SVGVIEWER_H
#define SVGVIEWER_H

#include "Exporter.hpp"
#include "Solvers/Solver.hpp"
#include "Simulator/Simulator.hpp"
#include "boost/filesystem.hpp"
#include "Utils/Color.hpp"

/**
 * Generate a SVG representation of the schedule
 * 
 * <https://www.w3schools.com/graphics/svg_intro.asp>
 * 
 * \see SVGViewer::help
 */
class SVGViewer : public Exporter {
    bool assign_color_to_iter = false;
    std::map<const Task*, Utils::color_t> colors;
    std::map<timinginfos_t, Utils::color_t> colors_per_iter;
    globtiminginfos_t force_length = 0;
    globtiminginfos_t time_scale = 0; // scaling factor to get ns, can overflow when any value >= 5 * 10^6 hours
    
    std::vector<SchedElt*> critical_chain;
    
    std::vector<std::pair<timinginfos_t, std::string>> markers;
public:
    void forward_params(const std::map<std::string, std::string> &args) override;
    std::string help() override;
    
    /**
     * Constructor
     */
    explicit SVGViewer();
    
    const std::string get_uniqid_rtti() const override;
    
    virtual void setAuxilliaryParams(std::string key, const std::map<std::string, std::string> &args);
protected:
    void check_dependencies() override;
    void do_export(const boost::filesystem::path& f) override;
    const std::string file_extension() override {
        return ".svg";
    }
    std::string add_js() ;
    std::string add_css(uint32_t fontsize) ;
    std::string add_task_desc(SchedElt *elt, const std::string &timeunit);
    
    std::string stats(size_t y, uint32_t fontsize, uint32_t width);
    
    std::string get_color(SchedElt *elt);
    
    std::string spmalloc(int startx, int starty, uint32_t width);
};

REGISTER_EXPORTER(SVGViewer, "svg")

#endif
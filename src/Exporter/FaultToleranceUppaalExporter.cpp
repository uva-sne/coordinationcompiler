/* 
 * Copyright (C) 2021 Benjamin Rouxel <benjamin.rouxel@uva.nl> 
 *                    Lukas Miedina <l.miedina@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <iostream>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <Exporter/UppaalTrait/UppaalExportConfiguration.hpp>

#include "FaultToleranceUppaalExporter.hpp"

using namespace std;
namespace ba = boost::algorithm;

FaultToleranceUppaalExporter::FaultToleranceUppaalExporter() : Exporter() {}

void FaultToleranceUppaalExporter::forward_params(const map<string, string> &args) {
    if(args.count("interarrival_time"))
        interarrival_time = stoul(args.at("interarrival_time"));
    if(args.count("filename") && !args.at("filename").empty())
        filename(args.at("filename"));
    if(args.count("queries_dir") && !args.at("queries_dir").empty())
        queries_dir = args.at("queries_dir");
}

const string FaultToleranceUppaalExporter::get_uniqid_rtti() const { return "generator-uppaal-ft"; }

IMPLEMENT_HELP(FaultToleranceUppaalExporter ,
    Export the generated schedule into a XML file used as input for UPPAAL.HTMLLF
HTMLINDENT- interarrival_time [uint128], default 3600000 (10^-3): inter-arrival time between failures HTMLLF
HTMLINDENT- filename, outDir/appname-uppaal.xml: name with or without path for the generated file. HTMLLF
HTMLINDENT- queries_dir: Set to export separate verifta query files. With or without path, otherwise relative to outDir. HTMLLF
)

const std::string FaultToleranceUppaalExporter::file_extension() {
    return "--" + make_trait_string() + "uppaal.xml";
}

const std::string FaultToleranceUppaalExporter::make_trait_string() {
    std::string trait_str;
    for (UppaalTrait *trait : this->traits) {
        trait_str += trait->name();
        trait_str += "--";
    }
    return trait_str;
}

void FaultToleranceUppaalExporter::do_export(const boost::filesystem::path& fn) {
    // Process all traits
    UppaalExportConfiguration configuration(this->tg, this->conf);
    configuration.interarrival_time = this->interarrival_time;
    configuration.task_count = this->tg->tasks().size();

    for (UppaalTrait *trait : this->traits) {
        trait->apply(configuration);
    }
    configuration.test_complete();

    // Write files
    string uppaal_model = get_template();
    ba::replace_all(uppaal_model, "%SCHEDULER%", configuration.scheduling_algo);
    ba::replace_all(uppaal_model, "%TASK_TEMPLATE%", configuration.task_template);
    ba::replace_all(uppaal_model, "%EDGE_TEMPLATE%", configuration.edge_template);
    ba::replace_all(uppaal_model, "%PROCESSOR_TEMPLATE%", configuration.processor_template);
    ba::replace_all(uppaal_model, "%SCHEDULER_TEMPLATE%", configuration.scheduler_template);
    ba::replace_all(uppaal_model, "%SYSTEM_DECL%", get_system(configuration));
    ba::replace_all(uppaal_model, "%WEIGHT_DETECTED_FAULT%", configuration.detected_fault_weight);
    ba::replace_all(uppaal_model, "%WEIGHT_UNDETECTED_FAULT%", configuration.undetected_fault_weight);

    ba::replace_all(uppaal_model, "%DAG_NAME%", conf->files.coord_basename.string());
    ba::replace_all(uppaal_model, "%FEATURES%", get_features(configuration));
    ba::replace_all(uppaal_model, "%TIMESTAMP%", get_timestamp());
    ba::replace_all(uppaal_model, "%FAULT_RATE%", "1:" + to_string(configuration.interarrival_time));
    ba::replace_all(uppaal_model, "%TASK_COUNT%", to_string(configuration.task_count));
    ba::replace_all(uppaal_model, "%PROC_COUNT%", to_string(configuration.processor_instantiations.size()));
    ba::replace_all(uppaal_model, "%QUERIES%", get_embedded_queries(configuration));
    ofstream out(fn.string());
    out << uppaal_model;
    out.close();

    if(!queries_dir.empty()) {
        boost::filesystem::path queries_path;
        if (queries_dir.at(0) == '/') // is absolute path?
            queries_path = queries_dir;
        else
            queries_path = conf->files.output_folder / queries_dir;
        if (boost::filesystem::exists(queries_path)) {
            if (!boost::filesystem::is_directory(queries_path)) {
                Utils::WARN(queries_path.string() +
                            " already exists and is not a directory. Skipping verifyta query generation");
                return;
            }
        }
        boost::filesystem::create_directories(queries_path);
        std::string basename = fn.leaf().string();
        if (boost::algorithm::ends_with(basename, "--uppaal.xml"))
            basename = basename.substr(0, basename.length() - std::string("--uppaal.xml").length());

        for (int64_t iterations = 1000; iterations < (100 * 1000 * 1000); iterations*=10) {
            boost::filesystem::path query_fn = queries_path / (basename + "--" + to_string(iterations) + ".q");
            ofstream query(query_fn.string());
            query << get_verifyta_query(configuration, iterations);
            query.close();
        }
    }
}

std::string FaultToleranceUppaalExporter::get_system(UppaalExportConfiguration &configuration) {
    std::string out;
    out.append("// Processors\n");
    for (UppaalTemplateInstantiation &proc : configuration.processor_instantiations) {
        out.append(proc.name() + " = " + proc.constructor() + ";\n");
    }

    out.append("\n// Tasks and edges\n");
    for (UppaalTemplateInstantiation &task : configuration.task_instantiations) {
        out.append(task.name() + " = " + task.constructor() + ";\n");
    }

    out.append("\n// System\n");
    out.append("system Scheduler,\n");
    out.append("    ");
    for (UppaalTemplateInstantiation &proc : configuration.processor_instantiations) {
        out.append(proc.name() + ", ");
    }
    out.append("\n    ");
    for (UppaalTemplateInstantiation &task : configuration.task_instantiations) {
        out.append(task.name() + ", ");
    }
    out[out.size() - 2] = ';';
    out[out.size() - 1] = '\n';
    return out;
}

string FaultToleranceUppaalExporter::get_template() {
    return 
#include "UppaalTrait/Templates/RootTemplate.tpl"
    ;
}

string FaultToleranceUppaalExporter::get_embedded_queries(UppaalExportConfiguration configuration) {
    string queries;
    timinginfos_t global_deadline = tg->global_deadline() == 0 ? 1000UL * 1000UL * NS_TO_S_FACTOR : tg->global_deadline();
    std::string D_seconds = to_string(global_deadline / NS_TO_S_FACTOR);

    for (int64_t iterations = 1000; iterations < (1000 * 1000); iterations*=10) {
        // Faults query
        queries.append("<query><formula>");
        queries.append("E [&lt;=" + D_seconds + ";" + to_string(iterations)+ + "] (max:faults)");
        queries.append("</formula><comment>");
        queries.append("Number of faults (" + to_string(iterations) + " iterations)");
        queries.append("</comment></query>\n");

        // Makespan query
        queries.append("<query><formula>");
        queries.append("Pr [&lt;=" + D_seconds + ";" + to_string(iterations)+ + "] (&lt;&gt; ");

        // Each task must be in its finished state (T${tid}.Finished)
        for(auto &&ti : configuration.final_tasks) {
            queries.append(ti + ".Finished");
            queries.append(" &amp;&amp; ");
        }
        queries.resize(queries.size() - 12);

        queries.append(")");
        queries.append("</formula><comment>");
        queries.append("Makespan (" + to_string(iterations) + " iterations)");
        queries.append("</comment></query>\n");
    }
    return queries;
}

string FaultToleranceUppaalExporter::get_verifyta_query(UppaalExportConfiguration configuration, uint64_t iterations) {
    // This is different from the embedded query as here we use "simulate" instead of "Pr"
    // Only the GUI tool can export all data for Pr, whereas verifyta simulate always dumps all results
    // Both the finishing of all tasks (as a single query) is recorded together with the faults
    timinginfos_t global_deadline = tg->global_deadline() == 0 ? 1000UL * 1000UL * NS_TO_S_FACTOR : tg->global_deadline();
    std::string D_seconds = to_string(global_deadline / (1000 * 1000 * 1000));
    string query("simulate [<=" + D_seconds + ";" + to_string(iterations) + + "] {");

    // Each task must be in its finished state (T${tid}.Finished)
    for(auto &&ti : configuration.final_tasks) {
        query.append(ti + ".Finished");
        query.append(" && ");
    }
    query.resize(query.size() - 4);
    query.append(", faults, undetected_faults}\n");
    return query;
}


std::string FaultToleranceUppaalExporter::get_timestamp() {
    using namespace boost::posix_time;
    return to_iso_extended_string(microsec_clock::universal_time());
}

std::string FaultToleranceUppaalExporter::get_features(UppaalExportConfiguration &conf) {
    std::string features;
    for (std::string &feature : conf.features) {
        features.append(" * ").append(feature).append("\n");
    }
    return features;
}
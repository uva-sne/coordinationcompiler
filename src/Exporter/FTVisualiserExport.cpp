/*
 * Copyright (C) 2020 Wouter Loeve <wouterloeve0@gmail.com>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <boost/algorithm/string/replace.hpp>

#include "FTVisualiserExport.hpp"

using namespace std;

IMPLEMENT_HELP(FTVisualiserExport,
    return "Exports the graph of the program readable for the FT-visualiser program.\n"
            "filename, coordFileDir/appname.FTGraph: name with or without path for the generated file.";
)

/* Sets filename
 */
void FTVisualiserExport::forward_params(const map<string, string> &args) {
    if(args.count("filename") && !args.at("filename").empty())
        filename = args.at("filename");
}

FTVisualiserExport::FTVisualiserExport() : Exporter() {}

const string FTVisualiserExport::get_uniqid_rtti() const { return "generator-FTVisualiser"; }

void FTVisualiserExport::do_export(const boost::filesystem::path &fn) {
    ofstream out(fn.string());
    for(Task * i : tg->tasks_it()) {
        // render edges between the inputs and outputs of the nodes.
        // Networkx creates nodes automatically when you supply edges, so that is enough.
        for(Task::dep_t ii : i->successor_tasks()) {
            for (Connection *tail : ii.second) {
                // Edges and ports are seperated by ':'
                out << i->id() << ":" << tail->from()->id();
                // Edges and edges are seperated by ->
                out << " -> ";
                out << ii.first->id() << ":" << tail->to()->id() << endl;
            }
        }
    }

    out.close();
}

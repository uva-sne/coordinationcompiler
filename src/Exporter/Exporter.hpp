/* 
 * Copyright (C) 2020 Benjamin Rouxel <benjamin.rouxel@uva.nl> 
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef EXPORTER_H
#define EXPORTER_H

#include <string>
#include <iostream>
#include <fstream>
#include <map>
#include <cassert>

#include "config.hpp"
#include "SystemModel.hpp"
#include "Schedule.hpp"
#include "StateMachine/StrategyStateMachine.hpp"
#include "registry.hpp"
#include "CompilerPass.hpp"
#include <boost/filesystem.hpp>

/*!
 * \brief Base class for exporters
 * 
 * Exporters output can be based on any of the two IR: #SystemModel or #Schedule
 */
class Exporter : public CompilerPass {
protected:
    //! IR of the current application
    const SystemModel *tg;
    //! the global configuration
    const config_t *conf;
    //! current state of the schedule
    const Schedule *sched;
    
    std::string _filename = ""; //!< relative or absolute path/filename given in the configuration

public:
    //! Constructor
    explicit Exporter() {};
    void run(SystemModel *m, config_t *c, Schedule *s) override {
        tg = m;
        conf = c;
        sched = s;

        check_dependencies();
        do_export();
    }

    virtual ~Exporter() = default;

    /*!
     * \brief Set params extracted from the configuration file
     *
     * This parameters are found in subtag of the exporter tag.
     *
     * \param key   subtag name
     * \param args   map of parameters
     */
    virtual void setAuxilliaryParams(std::string key, const std::map<std::string, std::string> &args) {}

    ACCESSORS_RW(Exporter, std::string, filename);
protected:
    /**
     * Return the file extension for this kind of exporter. This is used to generate the default file name,
     * which is the same as the
     * @return
     */
   virtual const std::string file_extension() = 0;

    /**
     * Do the export with either given path, or generate a new file based on the basename of the coordination file.
     */
    virtual void do_export() {
        if(!_filename.empty())
            if (_filename.at(0) == '/') // is absolute path?
                do_export(_filename);
            else
                do_export(conf->files.output_folder / _filename);
        else
            do_export(conf->files.output_folder / boost::filesystem::path(conf->files.coord_basename.string() + this->file_extension()));
    }
    /**
     * Perform the export, output should in f
     * 
     * @param f
     */
    virtual void do_export(const boost::filesystem::path &f) = 0;
};

using ExporterRegistry = registry::Registry<Exporter, std::string>;

#define REGISTER_EXPORTER(ClassName, Identifier) \
  REGISTER_SUBCLASS(Exporter, ClassName, std::string, Identifier)

#endif /* EXPORTER_H */
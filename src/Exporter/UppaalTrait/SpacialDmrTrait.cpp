/*
 * Copyright (C) 2021 Lukas Miedema <l.miedema@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "SpacialDmrTrait.hpp"

void SpacialDmrTrait::apply(UppaalExportConfiguration &conf) {
    conf.features.emplace_back(
            "- Spacial DMR implemented in the processor model where each processor is two processors in hardware, "
            "with implicit voting at the end (fault are only detected when the task finishes)");
    conf.features.emplace_back(
            "- Faults are detected when a DMR task terminates by implicit voting");
    conf.processor_template =
#include "Templates/ProcessorTemplate.tpl"
    ;
    // Set task & edge template. TODO: separate these from the DMR mechanism (unsure how hard)
    conf.task_template =
#include "Templates/TaskTemplate.tpl"
            ;
    conf.edge_template =
#include "Templates/EdgeTemplate.tpl"
            ;

    // Half the time between faults -> each proc represents two now, doubling the fault rate 
    conf.interarrival_time /= 2;
    unsigned long proc_count = conf.tg()->processors().size();
    if (proc_count & 0b1)
        throw CompilerException("export uppaal", "Cannot use Spacial DMR with an uneven number of processors");

    // Create processor instantiations
    for (unsigned long i = 0; i < (proc_count >> 1); i++) {
        conf.processor_instantiations.emplace_back("P" + to_string(i), "Processor(" + to_string(i) + ")");
    }
}

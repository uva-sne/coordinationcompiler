/*
 * Copyright (C) 2021 Lukas Miedema <l.miedema@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CECILE_SWIFTRTRAIT_HPP
#define CECILE_SWIFTRTRAIT_HPP

#include "UppaalTrait.hpp"

/**
 * Trait implementing the effects of blue/green SEU detection+mitigation on each task
 * This
 *   - doubles the WCET (assumption), effectively deploying temporal DMR
 *   - makes fault detection instantaneous
 */
class SwiftRTrait : public UppaalTrait {
public:
    void apply(UppaalExportConfiguration &conf) override;
    std::string name() override {
        return "swift-r";
    }
};

REGISTER_UPPAAL_TRAIT(SwiftRTrait, "swift-r");

#endif //CECILE_SWIFTRTRAIT_HPP

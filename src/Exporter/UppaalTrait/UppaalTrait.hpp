/*
 * Copyright (C) 2021 Lukas Miedema <l.miedema@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef UPPAALTRAIT_HPP
#define UPPAALTRAIT_HPP

#define NS_TO_S_FACTOR (1000UL * 1000UL * 1000UL)
#include "UppaalExportConfiguration.hpp"

/*!
 * \brief Base class for UPPAAL export model traits. Multiple traits can be active given that they're in
 * separate concerns.
 *
 * The traits modify the UPPAAL Export Configuration, which contains
 *  - the scheduling algorithm
 *  - the templates to be used for the processor, task, edge, scheduler
 *  - for each task, the task declaration (includes WCET + scheduler-dependent properties)
 *  - for each dependency, the edge declaration
 */
class UppaalTrait {
public:
    /**
     * Apply this trait.
     * @param conf
     */
    virtual void apply(UppaalExportConfiguration &conf) = 0;

    /**
     * Get the name of this trait.
     * @return
     */
    virtual std::string name() = 0;
};

using UppaalTraitRegistry = registry::Registry<UppaalTrait, std::string>;

#define REGISTER_UPPAAL_TRAIT(ClassName, Identifier) \
  REGISTER_SUBCLASS(UppaalTrait, ClassName, std::string, Identifier)

#endif //UPPAALTRAIT_HPP

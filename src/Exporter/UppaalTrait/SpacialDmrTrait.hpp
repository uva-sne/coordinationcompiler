/*
 * Copyright (C) 2021 Lukas Miedema <l.miedema@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CECILE_SPACIALDMRTRAIT_HPP
#define CECILE_SPACIALDMRTRAIT_HPP


#include "UppaalTrait.hpp"

/**
 * Straightfoward trait which doubles the global fault rate and allocates only half the processors.
 * This simulates the exact effect of running each task twice concurrently, while keeping the number of UPPAAL
 * templates (somewhat) at bay.
 * Fault detection is not instant, but only when the task finishes.
 */
class SpacialDmrTrait : public UppaalTrait {
public:
    void apply(UppaalExportConfiguration &conf) override;
    std::string name() override {
        return "spacial-dmr";
    }
};

REGISTER_UPPAAL_TRAIT(SpacialDmrTrait, "spacial-dmr");

#endif //CECILE_SPACIALDMRTRAIT_HPP

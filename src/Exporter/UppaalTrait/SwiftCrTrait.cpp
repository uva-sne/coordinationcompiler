/*
 * Copyright (C) 2021 Lukas Miedema <l.miedema@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "SwiftCrTrait.hpp"

void SwiftCrTrait::apply(UppaalExportConfiguration &conf) {
    conf.features.emplace_back(
            "- SWIFT instruction-level redundancy implemented in the task binary. All control-flow logic is duplicated,"
            "leading to instantaneous detection of faults at the cost of 1.5x the WCET estimate.");
    conf.features.emplace_back(
            "- When a fault is detect, the task is immediately made available to be rescheduled.");

    // Modify all WCET estimates for SWIFT (x1.5)
    // Checkpointing overhead is assumed at 0
    for (auto &&pair : conf.task_wcet) {
        pair.second = pair.second * 3 / 2;
    }

    // Set undetected fault rate
    // SWIFT (p10, Table 2) never produced incorrect output in the tested examples according to the authors
    // SWIFT (same table) only reacts to 81.96% of all faults, as most faults do not affect the program
    // As such, increase the inter-arrival time to meet this lowered fault rate
    conf.undetected_fault_weight = "0";
    conf.detected_fault_weight = "1";
    conf.interarrival_time = (timinginfos_t) (((double) conf.interarrival_time) / 0.8196);

    // Create processor instantiations
    for (unsigned long i = 0; i < conf.tg()->processors().size(); i++) {
        conf.processor_instantiations.emplace_back("P" + to_string(i), "Processor(" + to_string(i) + ")");
    }

    conf.processor_template =
#include "Templates/ProcessorTemplate.tpl"
            ;
    // Swift does instant detection, but will require restarting the task (CR)
    conf.task_template =
#include "Templates/InstantFaultDetectionTaskTemplate.tpl"
            ;
    conf.edge_template =
#include "Templates/EdgeTemplate.tpl"
            ;
}

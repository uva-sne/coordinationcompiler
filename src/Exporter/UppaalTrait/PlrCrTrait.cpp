/*
 * Copyright (C) 2021 Lukas Miedema <l.miedema@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <boost/algorithm/string/replace.hpp>
#include "PlrCrTrait.hpp"

namespace ba = boost::algorithm;

void PlrCrTrait::apply(UppaalExportConfiguration &conf) {
    conf.features.emplace_back(
            "- PLR-2 process-level redundancy implemented in the task binary. All tasks run twice concurrently, and "
            "checkpointing is performed in syscalls. Unmatched syscalls cause the task to restart (PLR + CR).");
    conf.features.emplace_back(
            "- When a fault is detect, the task has to visit the next syscall. It is then immediately made available to be rescheduled.");

    // Modify all WCET estimates for PLR2 (16.9% overhead assuming -O2)
    // Checkpointing overhead is assumed at 0
    for (auto &&pair : conf.task_wcet) {
        pair.second = (unsigned long) (((double) pair.second) * 1.169);
    }

    // Set undetected fault rate
    // PLR2 has very good fault response -- it almost only reacts to faults that actually change the output
    // So we take the NOFT value from the SWIFT paper (63.10% affect output), and then make it 5 percentage points worse (68.10%)
    // Furthermore, each processor represents two logical processors
    // To correct for this, double the fault rate
    conf.interarrival_time = (timinginfos_t) (((double) conf.interarrival_time) / 2 / 0.6810);

    // Create 1/2 of the processor instantiations as each processor is an abstract processor representing two
    for (unsigned long i = 0; i < conf.tg()->processors().size() / 2; i++) {
        conf.processor_instantiations.emplace_back("P" + to_string(i), "Processor(" + to_string(i) + ")");
    }

    conf.processor_template =
#include "Templates/ProcessorTemplate.tpl"
            ;
    // Swift does instant detection, but will require restarting the task (CR)
    conf.task_template =
#include "Templates/PlrCrTaskTemplate.tpl"
            ;
    conf.edge_template =
#include "Templates/EdgeTemplate.tpl"
            ;

    // Syscall rate is 10 syscalls / second (average)
    ba::replace_all(conf.task_template, "%SYSCALLS_PER_SECOND%", "10");
}

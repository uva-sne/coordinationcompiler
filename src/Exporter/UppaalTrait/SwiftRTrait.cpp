/*
 * Copyright (C) 2021 Lukas Miedema <l.miedema@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "SwiftRTrait.hpp"

void SwiftRTrait::apply(UppaalExportConfiguration &conf) {
    conf.features.emplace_back(
            "- SWIFT+R instruction-level redundancy + recovery implemented in the task binary. All control-flow logic is duplicated threefold,"
            "leading to instantaneous detection and mitigation of faults at 2x the WCET estimate.");
    conf.features.emplace_back(
            "- When a fault is detect, the task is immediately made available to be rescheduled.");

    // Modify all WCET estimates for SWIFT-R (x2)
    // Checkpointing overhead is assumed at 0
    for (auto &&pair : conf.task_wcet) {
        pair.second = pair.second * 2;
    }

    // Set unmitigated fault rate
    // SWIFT-R (p6) faults result in SEGV 1.93% and Silent Data Corruption 0.81% of the time = 2.74%
    // As we have no secondary fault recovery mechanism (checkpoint-restart), as such all remaining faults are undetected faults
    // The actual fault rate is considerably reduced however (the interarrival time is increased)
    conf.interarrival_time = (timinginfos_t) (((double) conf.interarrival_time) / 0.0274);
    conf.undetected_fault_weight = "1";
    conf.detected_fault_weight = "0";

    // Create processor instantiations
    for (unsigned long i = 0; i < conf.tg()->processors().size(); i++) {
        conf.processor_instantiations.emplace_back("P" + to_string(i), "Processor(" + to_string(i) + ")");
    }

    conf.processor_template =
#include "Templates/ProcessorTemplate.tpl"
            ;
    // Swift does instant detection, but will require restarting the task (CR)
    conf.task_template =
#include "Templates/InstantFaultDetectionTaskTemplate.tpl"
            ;
    conf.edge_template =
#include "Templates/EdgeTemplate.tpl"
            ;
}

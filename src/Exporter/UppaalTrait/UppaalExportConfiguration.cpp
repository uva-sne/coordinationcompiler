/*
 * Copyright (C) 2021 Lukas Miedema <l.miedema@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "UppaalExportConfiguration.hpp"

#define REQUIRE_NOT_EMPTY(value)\
    if ((value).empty()) {      \
        throw CompilerException("export uppaal", "Property " STRINGIFY(value) " was not set by a trait. Are your traits complete?");\
    }

void UppaalExportConfiguration::test_complete() {
    REQUIRE_NOT_EMPTY(task_template)
    REQUIRE_NOT_EMPTY(processor_template)
    REQUIRE_NOT_EMPTY(scheduler_template)
    REQUIRE_NOT_EMPTY(edge_template)
    REQUIRE_NOT_EMPTY(scheduling_algo)
    REQUIRE_NOT_EMPTY(task_instantiations)
    REQUIRE_NOT_EMPTY(processor_instantiations)
}

UppaalExportConfiguration::UppaalExportConfiguration(const SystemModel *tg, const config_t *conf) : _tg(tg), _conf(conf) {
    for (Task *t : tg->tasks_it()) {
        if (t->repeat() == 0)
            throw CompilerException("export uppaal", "Repeat property not set. Did you run the QVector analysis pass?");
        task_wcet[t] = t->C() * t->repeat();
    }
}

/*
 * Copyright (C) 2021 Lukas Miedema <l.miedema@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "TtmrTrait.hpp"
#include "GedfSchedulerTrait.hpp"

void TtmrTrait::apply(UppaalExportConfiguration &conf) {
    conf.features.emplace_back(
            "- Time Triple Modular Redundancy (TTMR) task-level redundancy. All tasks run two times, and if a fault is detected, a third tie-breaker task also runs");
    conf.features.emplace_back(
            "- Faults can only be detected (i.e. acted upon) after the first two copies of each task finish.");

    // TTMR only responds to faults affecting the output, not to any fault
    // Take the NOFT-Correct result from SWIFT (table 2)
    conf.interarrival_time = (timinginfos_t) (((double) conf.interarrival_time) / 0.6310);

    // Create processor instantiations
    for (unsigned long i = 0; i < conf.tg()->processors().size(); i++) {
        conf.processor_instantiations.emplace_back("P" + to_string(i), "Processor(" + to_string(i) + ")");
    }

    // Build custom task instantiation
    // Compute task instantiation: this overwrites the instantiation from G-EDF
    conf.task_instantiations = get_task_instantiations(conf);
    conf.task_count = conf.tg()->tasks().size() * 3;

    conf.processor_template =
#include "Templates/ProcessorTemplate.tpl"
            ;
    conf.task_template =
#include "Templates/TtmrTaskTemplate.tpl"
            ;
    conf.edge_template =
#include "Templates/EdgeTemplate.tpl"
            ;
}

std::vector<UppaalTemplateInstantiation> TtmrTrait::get_task_instantiations(UppaalExportConfiguration &conf) {
    std::vector<UppaalTemplateInstantiation> instantiations;
    uint32_t taskid = 0, edgeid=0;
    std::map<Task *, uint64_t> task_to_id;
    std::map<Task *, timinginfos_t> shifted_deadlines;
    timinginfos_t global_deadline = conf.tg()->global_deadline() == 0 ? 1000UL * 1000UL * NS_TO_S_FACTOR : conf.tg()->global_deadline();
    GedfSchedulerTrait::compute_gedf_critical_path_deadlines(conf,shifted_deadlines, global_deadline);

    for (const auto &pair : conf.task_wcet) {
        uint64_t C_seconds = pair.second / NS_TO_S_FACTOR;
        uint64_t D_seconds = shifted_deadlines[pair.first] / NS_TO_S_FACTOR;
        uint64_t replica1 = taskid++;
        uint64_t replica2 = taskid++;
        uint64_t tiebreaker = taskid++;

        instantiations.emplace_back("T"+to_string(replica1), "Task("+to_string(replica1)
                                                           +", "+to_string(C_seconds)
                                                           +", "+to_string(D_seconds) + ")");
        instantiations.emplace_back("T"+to_string(replica2), "Task("+to_string(replica2)
                                                           +", "+to_string(C_seconds)
                                                           +", "+to_string(D_seconds) + ")");
        instantiations.emplace_back("T"+to_string(tiebreaker), "TieBreakerTask("+to_string(tiebreaker)
                                                           +", "+to_string(replica1)
                                                           +", "+to_string(replica2)
                                                           +", "+to_string(C_seconds)
                                                           +", "+to_string(D_seconds) + ")");
        // Force replica1 to finish before replica2
        instantiations.emplace_back("E"+to_string(replica1) + "_" + to_string(replica2), "Edge("+to_string(replica1)+", "+to_string(replica2)+")");
        task_to_id[pair.first] = replica1; // use the ID of replica1 - that's what the incoming edges must go to

        if (pair.first->successor_tasks().empty())
            conf.final_tasks.push_back("T"+to_string(tiebreaker));
    }

    for (const auto &pair : conf.task_wcet) {
        for(Task::dep_t els : pair.first->successor_tasks()) {
            uint64_t from = task_to_id[pair.first] + 2; // plus 2 gets us the tie breaker task
            uint64_t to = task_to_id[els.first];
            instantiations.emplace_back("E"+to_string(edgeid), "Edge("+ to_string(from)+", "+ to_string(to)+")");
            ++edgeid;
        }
    }
    return instantiations;
}

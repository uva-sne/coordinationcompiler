/*
 * Copyright (C) 2021 Lukas Miedema <l.miedema@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CECILE_TTMRTRAIT_HPP
#define CECILE_TTMRTRAIT_HPP

#include "UppaalExportConfiguration.hpp"
#include "UppaalTrait.hpp"

/**
 * Triple Time Modular Redundancy
 *  - has a special task template that instantiates three tasks for every task
 *  - task 1 and 2 are regular task, and an edge is created such that 1 runs after 2
 *  - task 3 is special: it is the voter/tiebreaker task, if 1 or 2 fails only then will it run
 *  - task 3 always completes, i.e. successor tasks can rely on task 3 to "finish", but it will finish
 *    instantly if 1 and 2 two did not fail
 */
class TtmrTrait : public UppaalTrait {
public:
    void apply(UppaalExportConfiguration &conf) override;
    std::string name() override {
        return "ttmr";
    }

    std::vector<UppaalTemplateInstantiation> get_task_instantiations(UppaalExportConfiguration &conf);
};

REGISTER_UPPAAL_TRAIT(TtmrTrait, "ttmr");


#endif //CECILE_TTMRTRAIT_HPP

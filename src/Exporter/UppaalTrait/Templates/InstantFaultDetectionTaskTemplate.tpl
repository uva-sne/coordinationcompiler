R"CPPRAWMARKER(
	<template>
		<name>Task</name>
		<parameter>int32_t task_id, int32_t t_min, int32_t deadline</parameter>
		<declaration>int32_t assigned_proc_id = -1; // for when running
bool undetected_fault = false;</declaration>
		<location id="id9" x="-969" y="-1011">
			<name x="-952" y="-1020">Unscheduled</name>
		</location>
		<location id="id10" x="-969" y="-637">
			<name x="-952" y="-646">Running</name>
			<label kind="invariant" x="-875" y="-646">processor_timers[assigned_proc_id] &gt;= 0 &amp;&amp; processor_timers[assigned_proc_id]'==-1</label>
		</location>
		<location id="id11" x="-952" y="-467">
			<name x="-1028" y="-475">Finished</name>
		</location>
		<location id="id12" x="-969" y="-799">
			<name x="-952" y="-807">Ready</name>
		</location>
		<location id="id13" x="-969" y="-1113">
			<name x="-952" y="-1121">UnmetDependencies</name>
		</location>
		<location id="id14" x="-1521" y="-637">
			<name x="-1666" y="-646">RunningWithFault</name>
			<committed/>
		</location>
		<location id="id15" x="-952" y="-552">
			<committed/>
		</location>
		<location id="id16" x="-1598" y="-748">
			<committed/>
		</location>
		<location id="id17" x="-1232" y="-637">
			<name x="-1266" y="-620">Faulting</name>
			<committed/>
		</location>
		<branchpoint id="id18" x="-1351" y="-637">
		</branchpoint>
		<init ref="id13"/>
		<transition>
			<source ref="id10"/>
			<target ref="id17"/>
			<label kind="synchronisation" x="-1198" y="-629">task_transient_fault[task_id]?</label>
		</transition>
		<transition>
			<source ref="id18"/>
			<target ref="id10"/>
			<label kind="assignment" x="-1334" y="-748">undetected_fault = true,
undetected_faults++</label>
			<label kind="probability" x="-1334" y="-688">%WEIGHT_UNDETECTED_FAULT%</label>
			<nail x="-1351" y="-697"/>
			<nail x="-1028" y="-697"/>
		</transition>
		<transition>
			<source ref="id18"/>
			<target ref="id14"/>
			<label kind="assignment" x="-1674" y="-544">undetected_fault = false</label>
			<label kind="probability" x="-1445" y="-629">%WEIGHT_DETECTED_FAULT%</label>
		</transition>
		<transition>
			<source ref="id14"/>
			<target ref="id16"/>
			<label kind="synchronisation" x="-1513" y="-714">sync_finish!</label>
			<nail x="-1521" y="-731"/>
			<nail x="-1555" y="-748"/>
		</transition>
		<transition>
			<source ref="id14"/>
			<target ref="id16"/>
			<label kind="synchronisation" x="-1708" y="-714">sync_finish?</label>
			<nail x="-1598" y="-671"/>
		</transition>
		<transition>
			<source ref="id15"/>
			<target ref="id11"/>
			<label kind="synchronisation" x="-1096" y="-527">task_finished[task_id]!</label>
		</transition>
		<transition>
			<source ref="id10"/>
			<target ref="id15"/>
			<label kind="guard" x="-1258" y="-586">processor_timers[assigned_proc_id] == 0</label>
			<label kind="synchronisation" x="-1071" y="-603">sync_finish?</label>
			<nail x="-986" y="-612"/>
			<nail x="-986" y="-552"/>
		</transition>
		<transition>
			<source ref="id16"/>
			<target ref="id9"/>
			<label kind="synchronisation" x="-1504" y="-1053">task_finished_err[task_id]!</label>
			<label kind="assignment" x="-1504" y="-1036">queue_push(task_id, t_min)</label>
			<nail x="-1598" y="-1011"/>
		</transition>
		<transition>
			<source ref="id17"/>
			<target ref="id18"/>
			<nail x="-1343" y="-637"/>
		</transition>
		<transition>
			<source ref="id13"/>
			<target ref="id9"/>
			<label kind="guard" x="-960" y="-1087">open_dependencies[task_id] == 0</label>
			<label kind="synchronisation" x="-960" y="-1070">any_dependency_satisfied?</label>
			<label kind="assignment" x="-960" y="-1053">queue_push(task_id, t_min)</label>
		</transition>
		<transition>
			<source ref="id12"/>
			<target ref="id10"/>
			<label kind="synchronisation" x="-960" y="-765">start?</label>
			<label kind="assignment" x="-960" y="-748">assigned_proc_id = scheduler_proc_id,
scheduler_proc_id = -1,
processor_timers[assigned_proc_id] = t_min</label>
		</transition>
		<transition>
			<source ref="id10"/>
			<target ref="id15"/>
			<label kind="guard" x="-943" y="-586">processor_timers[assigned_proc_id] == 0</label>
			<label kind="synchronisation" x="-943" y="-603">sync_finish!</label>
			<nail x="-952" y="-612"/>
		</transition>
		<transition>
			<source ref="id9"/>
			<target ref="id12"/>
			<label kind="guard" x="-960" y="-875">waiting_for_task == true &amp;&amp; queue_peek() == task_id</label>
			<label kind="synchronisation" x="-960" y="-858">task_ready!</label>
			<label kind="assignment" x="-960" y="-841">scheduler_task_id = task_id, queue_pop()</label>
		</transition>
	</template>
)CPPRAWMARKER"
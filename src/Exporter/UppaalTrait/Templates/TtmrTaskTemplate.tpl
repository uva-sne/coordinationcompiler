R"CPPRAWMARKER(
	<template>
		<name>Task</name>
		<parameter>int32_t task_id, int32_t t_min, int32_t deadline</parameter>
		<declaration>int32_t assigned_proc_id = -1; // for when running</declaration>
		<location id="id9" x="-127" y="-1147">
			<name x="-110" y="-1156">Unscheduled</name>
		</location>
		<location id="id10" x="-127" y="-909">
			<name x="-110" y="-918">Running</name>
			<label kind="invariant" x="-110" y="-935">processor_timers[assigned_proc_id] &gt;= 0 &amp;&amp; processor_timers[assigned_proc_id]'==-1</label>
		</location>
		<location id="id11" x="-110" y="-739">
			<name x="-85" y="-739">Finished</name>
		</location>
		<location id="id12" x="-127" y="-1045">
			<name x="-110" y="-1053">Ready</name>
		</location>
		<location id="id13" x="-127" y="-1249">
			<name x="-110" y="-1257">UnmetDependencies</name>
		</location>
		<location id="id14" x="-475" y="-909">
			<name x="-620" y="-918">RunningWithFault</name>
			<label kind="invariant" x="-1045" y="-935">processor_timers[assigned_proc_id] &gt;= 0 &amp;&amp; processor_timers[assigned_proc_id]'==-1</label>
		</location>
		<location id="id15" x="-110" y="-824">
			<committed/>
		</location>
		<location id="id16" x="-433" y="-824">
			<committed/>
		</location>
		<init ref="id13"/>
		<transition>
			<source ref="id10"/>
			<target ref="id14"/>
			<label kind="synchronisation" x="-399" y="-935">task_transient_fault[task_id]?</label>
		</transition>
		<transition>
			<source ref="id14"/>
			<target ref="id16"/>
			<label kind="synchronisation" x="-424" y="-884">sync_finish!</label>
			<nail x="-458" y="-892"/>
			<nail x="-433" y="-867"/>
		</transition>
		<transition>
			<source ref="id14"/>
			<target ref="id16"/>
			<label kind="synchronisation" x="-577" y="-867">sync_finish?</label>
			<nail x="-475" y="-824"/>
		</transition>
		<transition>
			<source ref="id15"/>
			<target ref="id11"/>
			<label kind="synchronisation" x="-272" y="-790">task_finished[task_id]!</label>
		</transition>
		<transition>
			<source ref="id10"/>
			<target ref="id15"/>
			<label kind="guard" x="-408" y="-858">processor_timers[assigned_proc_id] == 0</label>
			<label kind="synchronisation" x="-229" y="-875">sync_finish?</label>
			<nail x="-144" y="-884"/>
			<nail x="-144" y="-824"/>
		</transition>
		<transition>
			<source ref="id16"/>
			<target ref="id15"/>
			<label kind="synchronisation" x="-374" y="-824">task_finished_err[task_id]!</label>
		</transition>
		<transition>
			<source ref="id13"/>
			<target ref="id9"/>
			<label kind="guard" x="-118" y="-1223">open_dependencies[task_id] == 0</label>
			<label kind="synchronisation" x="-118" y="-1206">any_dependency_satisfied?</label>
			<label kind="assignment" x="-118" y="-1189">queue_push(task_id, t_min)</label>
		</transition>
		<transition>
			<source ref="id12"/>
			<target ref="id10"/>
			<label kind="synchronisation" x="-118" y="-1011">start?</label>
			<label kind="assignment" x="-118" y="-994">assigned_proc_id = scheduler_proc_id,
            scheduler_proc_id = -1,
            processor_timers[assigned_proc_id] = t_min</label>
		</transition>
		<transition>
			<source ref="id10"/>
			<target ref="id15"/>
			<label kind="guard" x="-101" y="-858">processor_timers[assigned_proc_id] == 0</label>
			<label kind="synchronisation" x="-101" y="-875">sync_finish!</label>
			<nail x="-110" y="-884"/>
		</transition>
		<transition>
			<source ref="id9"/>
			<target ref="id12"/>
			<label kind="guard" x="-118" y="-1121">waiting_for_task == true &amp;&amp; queue_peek() == task_id</label>
			<label kind="synchronisation" x="-118" y="-1104">task_ready!</label>
			<label kind="assignment" x="-118" y="-1087">scheduler_task_id = task_id, queue_pop()</label>
		</transition>
	</template>
    <template>
    <name>TieBreakerTask</name>
    <parameter>int32_t task_id, int32_t replica1, int32_t replica2, int32_t t_min, int32_t deadline</parameter>
    <declaration>int32_t assigned_proc_id = -1; // for when running
    bool vote_fault = false;</declaration>
    <location id="id17" x="-127" y="-1181">
                                   <name x="-102" y="-1190">Unscheduled</name>
                                                    </location>
    <location id="id18" x="-127" y="-909">
                                   <name x="-110" y="-918">Running</name>
                                                    <label kind="invariant" x="-33" y="-918">processor_timers[assigned_proc_id] &gt;= 0 &amp;&amp; processor_timers[assigned_proc_id]'==-1</label>
    </location>
    <location id="id19" x="-110" y="-739">
                                   <name x="-85" y="-748">Finished</name>
                                                   </location>
    <location id="id20" x="-127" y="-1071">
                                   <name x="-102" y="-1079">Ready</name>
                                                    </location>
    <location id="id21" x="-272" y="-1436">
                                   <name x="-255" y="-1444">WaitingForReplicas</name>
                                                    </location>
    <location id="id22" x="-110" y="-824">
                                   <committed/>
                                   </location>
    <location id="id23" x="-425" y="-1351">
                                   <name x="-552" y="-1360">Replica1Success</name>
                                                    </location>
    <location id="id24" x="-126" y="-1351">
                                   <name x="-102" y="-1360">Replica1Fault</name>
                                                    </location>
    <location id="id25" x="-424" y="-1266">
                                   <name x="-552" y="-1275">Replica2Success</name>
                                                    <committed/>
                                                    </location>
    <location id="id26" x="-127" y="-1266">
                                   <name x="-102" y="-1275">Replica2Fault</name>
                                                    <committed/>
                                                    </location>
    <init ref="id21"/>
              <transition>
              <source ref="id24"/>
                          <target ref="id26"/>
                                      <label kind="synchronisation" x="-110" y="-1334">task_finished_err[replica2]?</label>
    <nail x="-118" y="-1300"/>
                     </transition>
    <transition>
    <source ref="id25"/>
                <target ref="id19"/>
                            <label kind="synchronisation" x="-280" y="-765">task_finished[task_id]!</label>
    <nail x="-424" y="-739"/>
                     </transition>
    <transition>
    <source ref="id23"/>
                <target ref="id25"/>
                            <label kind="synchronisation" x="-577" y="-1317">task_finished[replica2]?</label>
    </transition>
    <transition>
    <source ref="id23"/>
                <target ref="id26"/>
                            <label kind="synchronisation" x="-348" y="-1275">task_finished_err[replica2]?</label>
    </transition>
    <transition>
    <source ref="id24"/>
                <target ref="id26"/>
                            <label kind="synchronisation" x="-110" y="-1317">task_finished[replica2]?</label>
    <nail x="-135" y="-1326"/>
                     </transition>
    <transition>
    <source ref="id21"/>
                <target ref="id24"/>
                            <label kind="synchronisation" x="-152" y="-1411">task_finished_err[replica1]?</label>
    </transition>
    <transition>
    <source ref="id21"/>
                <target ref="id23"/>
                            <label kind="synchronisation" x="-517" y="-1419">task_finished[replica1]?</label>
    </transition>
    <transition>
    <source ref="id18"/>
                <target ref="id18"/>
                            <label kind="guard" x="-365" y="-1011">vote_fault==false</label>
    <label kind="synchronisation" x="-399" y="-943">task_transient_fault[task_id]?</label>
    <label kind="assignment" x="-365" y="-994">undetected_faults++, vote_fault=true</label>
    <nail x="-153" y="-969"/>
                     <nail x="-212" y="-969"/>
                                      <nail x="-212" y="-909"/>
                                                       </transition>
    <transition>
    <source ref="id22"/>
                <target ref="id19"/>
                            <label kind="synchronisation" x="-102" y="-799">task_finished[task_id]!</label>
    </transition>
    <transition>
    <source ref="id18"/>
                <target ref="id22"/>
                            <label kind="guard" x="-408" y="-858">processor_timers[assigned_proc_id] == 0</label>
    <label kind="synchronisation" x="-229" y="-875">sync_finish?</label>
    <nail x="-144" y="-884"/>
                     <nail x="-144" y="-824"/>
                                      </transition>
    <transition>
    <source ref="id26"/>
                <target ref="id17"/>
                            <label kind="assignment" x="-102" y="-1232">queue_push(task_id, t_min)</label>
    </transition>
    <transition>
    <source ref="id20"/>
                <target ref="id18"/>
                            <label kind="synchronisation" x="-118" y="-1037">start?</label>
    <label kind="assignment" x="-118" y="-1020">assigned_proc_id = scheduler_proc_id,
            scheduler_proc_id = -1,
            processor_timers[assigned_proc_id] = t_min</label>
    </transition>
    <transition>
    <source ref="id18"/>
                <target ref="id22"/>
                            <label kind="guard" x="-101" y="-858">processor_timers[assigned_proc_id] == 0</label>
    <label kind="synchronisation" x="-101" y="-875">sync_finish!</label>
    <nail x="-110" y="-884"/>
                     </transition>
    <transition>
    <source ref="id17"/>
                <target ref="id20"/>
                            <label kind="guard" x="-118" y="-1147">waiting_for_task == true &amp;&amp; queue_peek() == task_id</label>
    <label kind="synchronisation" x="-118" y="-1130">task_ready!</label>
    <label kind="assignment" x="-118" y="-1113">scheduler_task_id = task_id, queue_pop()</label>
    </transition>
    </template>
)CPPRAWMARKER"
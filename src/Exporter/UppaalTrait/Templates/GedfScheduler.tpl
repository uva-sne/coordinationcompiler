R"CPPRAWMARKER(
// Global EDF DAG scheduling, sorting the tasks according to the scheduling policy
// Note that since queue contention is not modelled, there is no penalty to using inefficient data structures.
// Using a binary heap-based priority queue would only improve wall time of the simulator

typedef struct {
    int32_t task_id;
    int32_t deadline_minus_wcet; // = deadline - wcet
} queue_t;
queue_t queue[TASK_COUNT];

void queue_init() {
    int32_t i = 0;
    queue_t nil = { -1, 0 };
    while (i &lt; TASK_COUNT) {
        queue[i] = nil;
        i++;
    }
}

// Sort by deadline-wcet (lowest first), then by task id (lowest first). A task_id = -1 is considered NULL
int32_t queue_compare(queue_t left, queue_t right) { // left &lt; right --&gt; compare(left, right) &lt; 0
    if (left.task_id == -1) {
        if (right.task_id == -1)
            return 0;
        return -1;
    }
    if (right.task_id == -1)
        return 1;
    if (left.deadline_minus_wcet &lt; right.deadline_minus_wcet)
        return 1;
    if (left.deadline_minus_wcet &gt; right.deadline_minus_wcet)
        return -1;
    if (left.task_id &gt; right.task_id)
        return -1;
    if (left.task_id &lt; right.task_id)
        return 1;
    return 0;
}


void queue_push(int32_t task_id, int32_t WCET) {
    queue_t element = { task_id, WCET };
    queue_t left, right;
    int32_t i;

    queue[TASK_COUNT-1] = element;
    i = TASK_COUNT-1;
    while (i &gt; 0) { // bubblesort swapping if queue[i-1] &lt; queue[i]
        left = queue[i - 1];
        right = queue[i];
        if (queue_compare(left, right) &lt; 0) {
            queue[i - 1] = right;
            queue[i] = left;
        }
        i--;
    }
}

bool queue_has_item() {
    return queue[0].task_id != -1;
}

int32_t queue_peek() {
    return queue[0].task_id;
}

void queue_pop() {
    queue_t nil = { -1, 0 };
    int32_t i;

    queue[0] = nil;
    i = 1;
    // Shift the whole queue
    while (i &lt; TASK_COUNT) {
        queue[i-1] = queue[i];
        i++;
    }
}
)CPPRAWMARKER"
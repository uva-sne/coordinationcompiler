R"CPPRAWMARKER(
<template>
    <name x="5" y="5">Processor</name>
    <parameter>int32_t proc_id</parameter>
    <declaration>
int32_t assigned_task_id = -1; // for when running a task</declaration>
    <location id="id17" x="-969" y="-1045">
        <name x="-952" y="-1053">Idle</name>
    </location>
    <location id="id18" x="-1147" y="-867">
        <name x="-1172" y="-901">Running</name>
        <label kind="exponentialrate" x="-1215" y="-918">%FAULT_RATE%</label>
    </location>
    <location id="id19" x="-1266" y="-1046">
        <name x="-1385" y="-1054">WaitingForTask</name>
    </location>
    <init ref="id17"/>
    <transition>
        <source ref="id18"/>
        <target ref="id17"/>
        <label kind="synchronisation" x="-952" y="-1020">task_finished_err[assigned_task_id]?</label>
        <label kind="assignment" x="-952" y="-1003">processor_has_task[proc_id]=false</label>
        <nail x="-1130" y="-858"/>
        <nail x="-960" y="-858"/>
        <nail x="-960" y="-1020"/>
    </transition>
    <transition>
        <source ref="id18"/>
        <target ref="id18"/>
        <label kind="synchronisation" x="-1249" y="-705">task_transient_fault[assigned_task_id]!</label>
        <label kind="assignment" x="-1181" y="-688">faults++</label>
        <nail x="-1088" y="-714"/>
        <nail x="-1207" y="-714"/>
    </transition>
    <transition>
        <source ref="id19"/>
        <target ref="id18"/>
        <label kind="synchronisation" x="-1258" y="-986">start?</label>
        <label kind="assignment" x="-1258" y="-969">assigned_task_id = scheduler_task_id,
scheduler_task_id = -1,
processor_has_task[proc_id] = true</label>
        <nail x="-1266" y="-867"/>
    </transition>
    <transition>
        <source ref="id18"/>
        <target ref="id17"/>
        <label kind="synchronisation" x="-1190" y="-1020">task_finished[assigned_task_id]?</label>
        <label kind="assignment" x="-1190" y="-1003">processor_has_task[proc_id]=false</label>
        <nail x="-969" y="-866"/>
    </transition>
    <transition>
        <source ref="id17"/>
        <target ref="id19"/>
        <label kind="guard" x="-1190" y="-1105">waiting_for_core == true</label>
        <label kind="synchronisation" x="-1190" y="-1088">core_ready!</label>
        <label kind="assignment" x="-1190" y="-1071">scheduler_proc_id = proc_id</label>
    </transition>
</template>
)CPPRAWMARKER"
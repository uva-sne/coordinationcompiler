R"CPPRAWMARKER(
<template>
    <name>Scheduler</name>
    <location id="id4" x="-399" y="-42">
        <name x="-382" y="-51">Idle</name>
    </location>
    <location id="id5" x="-399" y="-323">
        <name x="-476" y="-357">HasProcessor</name>
    </location>
    <location id="id6" x="-93" y="-204">
        <name x="-68" y="-213">HasTaskAndProcessor</name>
    </location>
    <location id="id7" x="-629" y="-42">
        <name x="-646" y="-76">Start</name>
        <urgent/>
    </location>
    <location id="id8" x="-858" y="-42">
        <name x="-868" y="-76">Setup</name>
        <urgent/>
    </location>
    <init ref="id8"/>
    <transition>
        <source ref="id8"/>
        <target ref="id7"/>
        <label kind="synchronisation" x="-782" y="-68">setup!</label>
        <label kind="assignment" x="-799" y="-34">queue_init()</label>
    </transition>
    <transition>
        <source ref="id7"/>
        <target ref="id4"/>
        <label kind="synchronisation" x="-603" y="-68">any_dependency_satisfied!</label>
        <label kind="assignment" x="-603" y="-34">waiting_for_core=true</label>
    </transition>
    <transition>
        <source ref="id6"/>
        <target ref="id4"/>
        <label kind="synchronisation" x="-229" y="-119">start!</label>
        <label kind="assignment" x="-272" y="-102">waiting_for_core=true</label>
    </transition>
    <transition>
        <source ref="id5"/>
        <target ref="id6"/>
        <label kind="synchronisation" x="-229" y="-297">task_ready?</label>
        <label kind="assignment" x="-229" y="-280">waiting_for_task=false</label>
    </transition>
    <transition>
        <source ref="id4"/>
        <target ref="id5"/>
        <label kind="synchronisation" x="-391" y="-195">core_ready?</label>
        <label kind="assignment" x="-391" y="-178">waiting_for_core=false,
waiting_for_task=true</label>
    </transition>
</template>
)CPPRAWMARKER"
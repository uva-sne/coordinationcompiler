R"CPPRAWMARKER(
<template>
    <name>Task</name>
    <parameter>int32_t task_id, int32_t t_min, int32_t deadline</parameter>
    <declaration>int32_t assigned_proc_id = -1; // for when running
bool undetected_fault = false;</declaration>
    <location id="id9" x="-68" y="-833">
        <name x="-51" y="-842">Unscheduled</name>
    </location>
    <location id="id10" x="-68" y="-459">
        <name x="-51" y="-468">Running</name>
        <label kind="invariant" x="26" y="-468">processor_timers[assigned_proc_id] &gt;= 0 &amp;&amp; processor_timers[assigned_proc_id]'==-1</label>
    </location>
    <location id="id11" x="-51" y="-289">
        <name x="-127" y="-297">Finished</name>
    </location>
    <location id="id12" x="-68" y="-621">
        <name x="-51" y="-629">Ready</name>
    </location>
    <location id="id13" x="-68" y="-935">
        <name x="-51" y="-943">UnmetDependencies</name>
    </location>
    <location id="id14" x="-51" y="-374">
        <committed/>
    </location>
    <location id="id15" x="-416" y="-459">
        <name x="-459" y="-442">RunningWithFault</name>
        <label kind="invariant" x="-1020" y="-467">processor_timers[assigned_proc_id] &gt;= 0 &amp;&amp; processor_timers[assigned_proc_id]'==-1</label>
        <label kind="exponentialrate" x="-408" y="-484">%SYSCALLS_PER_SECOND%</label>
    </location>
    <location id="id16" x="-416" y="-620">
        <name x="-484" y="-629">Syscall</name>
        <committed/>
    </location>
    <init ref="id13"/>
    <transition>
        <source ref="id16"/>
        <target ref="id9"/>
        <label kind="synchronisation" x="-357" y="-858">task_finished_err[task_id]!</label>
        <label kind="assignment" x="-357" y="-824">queue_push(task_id, t_min)</label>
        <nail x="-416" y="-833"/>
    </transition>
    <transition>
        <source ref="id15"/>
        <target ref="id16"/>
    </transition>
    <transition>
        <source ref="id10"/>
        <target ref="id15"/>
        <label kind="synchronisation" x="-297" y="-451">task_transient_fault[task_id]?</label>
    </transition>
    <transition>
        <source ref="id14"/>
        <target ref="id11"/>
        <label kind="synchronisation" x="-195" y="-349">task_finished[task_id]!</label>
    </transition>
    <transition>
        <source ref="id10"/>
        <target ref="id14"/>
        <label kind="guard" x="-357" y="-408">processor_timers[assigned_proc_id] == 0</label>
        <label kind="synchronisation" x="-170" y="-425">sync_finish?</label>
        <nail x="-85" y="-434"/>
        <nail x="-85" y="-374"/>
    </transition>
    <transition>
        <source ref="id13"/>
        <target ref="id9"/>
        <label kind="guard" x="-59" y="-909">open_dependencies[task_id] == 0</label>
        <label kind="synchronisation" x="-59" y="-892">any_dependency_satisfied?</label>
        <label kind="assignment" x="-59" y="-875">queue_push(task_id, t_min)</label>
    </transition>
    <transition>
        <source ref="id12"/>
        <target ref="id10"/>
        <label kind="synchronisation" x="-59" y="-587">start?</label>
        <label kind="assignment" x="-59" y="-570">assigned_proc_id = scheduler_proc_id,
scheduler_proc_id = -1,
processor_timers[assigned_proc_id] = t_min</label>
    </transition>
    <transition>
        <source ref="id10"/>
        <target ref="id14"/>
        <label kind="guard" x="-42" y="-408">processor_timers[assigned_proc_id] == 0</label>
        <label kind="synchronisation" x="-42" y="-425">sync_finish!</label>
        <nail x="-51" y="-434"/>
    </transition>
    <transition>
        <source ref="id9"/>
        <target ref="id12"/>
        <label kind="guard" x="-59" y="-697">waiting_for_task == true &amp;&amp; queue_peek() == task_id</label>
        <label kind="synchronisation" x="-59" y="-680">task_ready!</label>
        <label kind="assignment" x="-59" y="-663">scheduler_task_id = task_id, queue_pop()</label>
    </transition>
</template>
)CPPRAWMARKER"
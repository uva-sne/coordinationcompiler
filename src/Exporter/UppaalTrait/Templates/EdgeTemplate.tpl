R"CPPRAWMARKER(
<template>
    <name>Edge</name>
    <parameter>int32_t task_from, int32_t task_to</parameter>
    <location id="id0" x="-1462" y="-161">
        <name x="-1496" y="-204">Unsatisfied</name>
    </location>
    <location id="id1" x="-1156" y="-161">
        <name x="-1173" y="-204">Notify</name>
        <committed/>
    </location>
    <location id="id2" x="-918" y="-161">
        <name x="-935" y="-204">Satisfied</name>
    </location>
    <location id="id3" x="-1717" y="-161">
        <name x="-1727" y="-195">Setup</name>
    </location>
    <init ref="id3"/>
    <transition>
        <source ref="id3"/>
        <target ref="id0"/>
        <label kind="synchronisation" x="-1683" y="-204">setup?</label>
        <label kind="assignment" x="-1683" y="-187">open_dependencies[task_to]++</label>
    </transition>
    <transition>
        <source ref="id1"/>
        <target ref="id2"/>
        <label kind="synchronisation" x="-1122" y="-187">any_dependency_satisfied!</label>
    </transition>
    <transition>
        <source ref="id0"/>
        <target ref="id1"/>
        <label kind="synchronisation" x="-1394" y="-204">task_finished[task_from]?</label>
        <label kind="assignment" x="-1394" y="-187">open_dependencies[task_to]--</label>
    </transition>
</template>
)CPPRAWMARKER"
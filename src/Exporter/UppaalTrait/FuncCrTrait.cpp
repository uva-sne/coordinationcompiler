/*
 * Copyright (C) 2021 Lukas Miedema <l.miedema@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <boost/algorithm/string/replace.hpp>
#include "FuncCrTrait.hpp"

namespace ba = boost::algorithm;

void FuncCrTrait::apply(UppaalExportConfiguration &conf) {
    conf.features.emplace_back(
            "- FUNC-2 function-level redundancy implemented in the task binary. All tasks run twice concurrently, and "
            "checkpointing is performed in functions. Unmatched functions cause the task to restart similar to PLR (FUNC + CR).");
    conf.features.emplace_back(
            "- When a fault is detect, the task has to visit the next function. It is then immediately made available to be rescheduled.");
    conf.features.emplace_back(
            "- Key difference between FUNC-2 and PLR-2 is that the modeled rate of functions is much higher (1000/s vs 10/s), as such there is less time between a hidden fault and a restart. However, the overhead is also higher (27% vs 16%)");

    // Implements FUNC policy for PLR (it's basically PLR but then with a higher detection rate)
    // The overhead is also increased -> regular PLR2 has 16.9% overhead (assuming -O2)
    // FUNC assumes ~1000 syncpoints / second (14% overhead as per Figure 7)
    // Furthermore, the fixed overhead for PLR is 13% (contention, 5.4.1)
    // Modify all WCET estimates for FUNC-CR then increases WCET by 27%
    for (auto &&pair : conf.task_wcet) {
        pair.second = (unsigned long) (((double) pair.second) * 1.27);
    }

    // Set undetected fault rate
    // PLR2/FUNC2 has very good fault response -- it almost only reacts to faults that actually change the output
    // So we take the NOFT value from the SWIFT paper (63.10% affect output), and then make it 5 percentage points worse (68.10%)
    // Furthermore, each processor represents two logical processors
    // To correct for this, double the fault rate
    conf.interarrival_time = (timinginfos_t) (((double) conf.interarrival_time) / 2 / 0.6810);

    // Create 1/2 of the processor instantiations as each processor is an abstract processor representing two
    for (unsigned long i = 0; i < conf.tg()->processors().size() / 2; i++) {
        conf.processor_instantiations.emplace_back("P" + to_string(i), "Processor(" + to_string(i) + ")");
    }

    conf.processor_template =
#include "Templates/ProcessorTemplate.tpl"
            ;

    conf.task_template =
#include "Templates/PlrCrTaskTemplate.tpl"
            ;
    conf.edge_template =
#include "Templates/EdgeTemplate.tpl"
            ;

    // Syscall rate is 1000 syscalls / second for FUNC
    ba::replace_all(conf.task_template, "%SYSCALLS_PER_SECOND%", "1000");
}

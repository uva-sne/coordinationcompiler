/*
 * Copyright (C) 2021 Lukas Miedema <l.miedema@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <boost/algorithm/string/replace.hpp>
#include "Plr3Trait.hpp"

namespace ba = boost::algorithm;

void Plr3Trait::apply(UppaalExportConfiguration &conf) {
    conf.features.emplace_back(
            "- PLR-3 process-level redundancy implemented in the task binary. All tasks run three times concurrently, and "
            "voting is performed in syscalls. Unmatched syscalls cause seamless mitigation by killing the failed task and forking another copy.");
    conf.features.emplace_back("- Each modeled processor symbolizes three logical processors, running three redundant copies of each task in parallel. The fault rate is adjusted for this");
    conf.features.emplace_back(
            "- When a fault is detect, the task has to visit the next syscall. If a second fault occurs before a syscall, it counts as an undetected fault (i.e. an unrecoverable fault for PLR3)");

    // Modify all WCET estimates for PLR3 (41.1% overhead assuming -O2)
    for (auto &&pair : conf.task_wcet) {
        pair.second = (unsigned long) (((double) pair.second) * 1.411);
    }

    // Set undetected fault rate
    // PLR(2,3) has very good fault response -- it almost only reacts to faults that actually change the output
    // So we take the NOFT value from the SWIFT paper (63.10% affect output), and then make it 5 percentage points worse (68.10%)
    // Furthermore, each processor represents three logical processors
    // To correct for this, double the fault rate
    conf.interarrival_time = (timinginfos_t) (((double) conf.interarrival_time) / 3 / 0.6810);

    // Create 1/3 of the processor instantiations as each processor is an abstract processor representing two
    for (unsigned long i = 0; i < conf.tg()->processors().size() / 3; i++) {
        conf.processor_instantiations.emplace_back("P" + to_string(i), "Processor(" + to_string(i) + ")");
    }

    conf.processor_template =
#include "Templates/ProcessorTemplate.tpl"
            ;
    // Swift does instant detection, but will require restarting the task (CR)
    conf.task_template =
#include "Templates/Plr3TaskTemplate.tpl"
            ;
    conf.edge_template =
#include "Templates/EdgeTemplate.tpl"
            ;

    // Syscall rate is 10 syscalls / second (average)
    ba::replace_all(conf.task_template, "%SYSCALLS_PER_SECOND%", "10");
}

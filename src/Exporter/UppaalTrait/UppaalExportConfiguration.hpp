/*
 * Copyright (C) 2021 Lukas Miedema <l.miedema@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CECILE_UPPAALEXPORTCONFIGURATION_HPP
#define CECILE_UPPAALEXPORTCONFIGURATION_HPP


#include <string>
#include <vector>
#include <SystemModel.hpp>
#include "Helper_macros.hpp"

class UppaalTemplateInstantiation {
    std::string _name;
    std::string _constructor;

public:
    UppaalTemplateInstantiation(const std::string &name, const std::string &constructor) :
        _name(name), _constructor(constructor) {};

    ACCESSORS_R(UppaalTemplateInstantation, std::string, name);
    ACCESSORS_R(UppaalTemplateInstantation, std::string, constructor);
};

class UppaalExportConfiguration {
private:
    //! IR
    const SystemModel *_tg;

    //! global configuration
    const config_t *_conf;

protected:

public:
    UppaalExportConfiguration(const SystemModel *tg, const config_t *conf);

    // TODO: document attributes
    // UPPAAL templates
    std::string task_template;
    std::string processor_template;
    std::string scheduler_template;
    std::string edge_template;

    // Scheduling algorithm
    std::string scheduling_algo;

    // Probability of undetected faults
    std::string undetected_fault_weight = "0"; // P(undetected fault | any fault) = undetected_fault_weight / (undetected_fault_weight + detected_fault_weight)
    std::string detected_fault_weight = "1"; // P(detected fault | any fault) = detected_fault_weight / (undetected_fault_weight + detected_fault_weight)

    // Template instantiation
    std::vector<UppaalTemplateInstantiation> task_instantiations; // task and edge
    std::vector<std::string> final_tasks; // list of tasks without successors on which a "all tasks completed" query is based
    std::vector<UppaalTemplateInstantiation> processor_instantiations;

    // Special attributes enabling traits to modify them
    std::map<Task *, timinginfos_t> task_wcet;
    timinginfos_t interarrival_time; // global interarrival time between faults (rate = 1/interarrival_time)
    uint128_t task_count;

    // List of features
    std::vector<std::string> features;

    ACCESSORS_R_CONSTONLY(UppaalExportConfiguration, SystemModel *, tg);
    ACCESSORS_R_CONSTONLY(UppaalExportConfiguration, config_t *, conf);

    //! Test if all properties are set throwing an exception otherwise
    void test_complete();

};


#endif //CECILE_UPPAALEXPORTCONFIGURATION_HPP

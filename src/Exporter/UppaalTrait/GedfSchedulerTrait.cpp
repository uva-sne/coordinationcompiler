/*
 * Copyright (C) 2021 Lukas Miedema <l.miedema@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "GedfSchedulerTrait.hpp"

#define NS_TO_S_FACTOR (1000UL * 1000UL * 1000UL)

void GedfSchedulerTrait::apply(UppaalExportConfiguration &conf) {
    conf.features.emplace_back(
            "- sync_finish anti-timing anomaly mechanism, forcing all tasks that can finish to finish before the scheduler acts");
    conf.features.emplace_back(
            "- GEDF deadline shifting. A deadline is assigned to each task which is\n"
                "       deadline (d_i) = DAG_deadline - sum(WCET of all tasks on its critical path)");
    conf.features.emplace_back(
            "- GEDF online scheduler, which sorts tasks by their deadline - wcet and executes earliest-deadline-first\n"
                "       Earliest-deadline (minus wcet) first, with a globally shared queue (queue contention not modelled)\n"
                "       Tasks are only in the queue when ready, so it may execute lower-priority tasks first\n"
                "       Non-preemptive, will not interrupt a lower-priority task for a higher-priority one when it becomes available\n"
                "       Note:  the deadlines are shifted by the TeamPlay tool that is used to generate these UPPAAL files");

    // Set scheduler
    conf.scheduler_template =
#include "Templates/GedfSchedulerTemplate.tpl"
        ;

    conf.scheduling_algo =
#include "Templates/GedfScheduler.tpl"
        ;

    // Compute task instantiation
    if (conf.task_instantiations.empty())
        conf.task_instantiations = get_task_instantiations(conf);
}

std::vector<UppaalTemplateInstantiation> GedfSchedulerTrait::get_task_instantiations(UppaalExportConfiguration &conf) {
    std::vector<UppaalTemplateInstantiation> instantiations;
    uint32_t taskid = 0, edgeid=0;
    std::map<Task *, std::string> task_to_id;
    std::map<Task *, timinginfos_t> shifted_deadlines;
    timinginfos_t global_deadline = conf.tg()->global_deadline() == 0 ? 1000UL * 1000UL * NS_TO_S_FACTOR : conf.tg()->global_deadline();
    compute_gedf_critical_path_deadlines(conf,shifted_deadlines, global_deadline);

    std::vector<Task *> tasks = conf.tg()->tasks_as_vector();
    std::sort(tasks.begin(), tasks.end(), [](Task *a, Task *b) -> bool {
        return a->id() < b->id();
    });

    for (auto *task : tasks) {
        uint64_t C_seconds = conf.task_wcet[task] / NS_TO_S_FACTOR;
        uint64_t D_seconds = shifted_deadlines[task] / NS_TO_S_FACTOR;
        std::string name = "T"+to_string(taskid);
        instantiations.emplace_back(name, "Task("+to_string(taskid)
                  +", "+to_string(C_seconds)
                  +", "+to_string(D_seconds) + ")");
        task_to_id[task] = to_string(taskid++);

        if (task->successor_tasks().empty())
            conf.final_tasks.push_back(name);
    }

    for (auto *task : tasks) {
        for(Task::dep_t els : task->successor_tasks()) {
            instantiations.emplace_back("E"+to_string(edgeid), "Edge("+task_to_id[task]+", "+task_to_id[els.first]+")");
            ++edgeid;
        }
    }
    return instantiations;
}

void GedfSchedulerTrait::compute_gedf_critical_path_deadlines(UppaalExportConfiguration &conf, std::map<Task *, timinginfos_t> &mapped_deadlines, const timinginfos_t deadline) {
    // The critical path is the longest path from any task to a terminal task (tasks without successors)
    std::list<Task *> tentative;

    for (Task *task : conf.tg()->tasks_it()) {
        if (task->successor_tasks().empty()) {
            tentative.push_back(task);
        }
    }
    if (tentative.empty())
        throw CompilerException("export uppaal", "No tasks without successors found: either there are no tasks, or the task graph is circular");

    while (!tentative.empty()) {
        std::list<Task *>::iterator i = tentative.begin();
        while (i != tentative.end()) {
            if (mapped_deadlines.count(*i)) {
                // Already processed, the same task can be in the queue multiple times
                i = tentative.erase(i);
                continue;
            }
            bool all_successors_visited = true;
            timinginfos_t lowest_deadline = deadline;
            Task::connector_map_t &successors = (*i)->successor_tasks();
            for (Task::dep_t entry : successors) {
                if (mapped_deadlines.count(entry.first)) {
                    timinginfos_t successor_deadline = mapped_deadlines[entry.first];
                    if (successor_deadline < conf.task_wcet[entry.first])
                        throw CompilerException("export uppaal", "Underflow: cannot offset deadline to negative number. DAG is unschedulable (critical path > deadline).");
                    lowest_deadline = std::min(lowest_deadline, successor_deadline - conf.task_wcet[entry.first]);
                } else {
                    all_successors_visited = false;
                    break;
                }
            }
            if (all_successors_visited) {
                mapped_deadlines[*i] = lowest_deadline;
                Task::connector_map_t &predecessors = (*i)->predecessor_tasks();
                for (Task::dep_t entry : predecessors) {
                    tentative.push_back(entry.first);
                }
                i = tentative.erase(i);
            }
            i++;
        }
    }
}
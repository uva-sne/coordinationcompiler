/*
 * Copyright (C) 2017 Benjamin Rouxel <benjamin.rouxel@inria.fr>
 *                    Irisa/INRIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "DotViewer.hpp"
#include "CriticalPath.hpp"

using namespace std;

 IMPLEMENT_HELP(DotViewer,
    Generate a dot file representing the application.\n
    HTMLINDENT- rankdir, default LR: martin-loetzsch.de/S-DOT/rankdir.html\n
    HTMLINDENT- filename, coordFileDir/appname.dot: name with or without path for the generated file.\n
    HTMLINDENT- transitive, [CONFIG_FALSE-CONFIG_TRUE], default CONFIG_FALSE: Add transitive edges if computed\n
    HTMLINDENT- verbose, [CONFIG_FALSE-CONFIG_TRUE], default CONFIG_TRUE: Show versions\n
)
            
void DotViewer::forward_params(const map<string, string> &args) {
    if(args.count("rankdir") && !args.at("rankdir").empty())
        rankdir = args.at("rankdir");
    if(args.count("filename") && !args.at("filename").empty())
        filename(args.at("filename"));
    if(args.count("transitive"))
        transitive = args.at("transitive") == CONFIG_TRUE;
    if(args.count("parent_only"))
        parent_only = args.at("parent_only") == CONFIG_TRUE;
    if(args.count("verbose"))
        verbose = args.at("verbose") == CONFIG_TRUE;
}

DotViewer::DotViewer() : Exporter() {}

const string DotViewer::get_uniqid_rtti() const { return "generator-dot"; }

void DotViewer::do_export(const boost::filesystem::path &fn) {
    vector<Task*> critical_path;
    for(const auto &p : conf->passes.passes()) {
        if(p->get_uniqid_rtti() == "analyse-critical_path") {
            auto *pass_critic_path = dynamic_cast<CriticalPath*>(p.get());
            critical_path = pass_critic_path->get_critical_path();
        }
    }
    
    map<ArchitectureProperties::ComputeUnitType, string> core_colors;
    core_colors[ArchitectureProperties::ComputeUnitType::CUT_CPU] = "#000000";
    core_colors[ArchitectureProperties::ComputeUnitType::CUT_GPU] = "#31993A";
    
    ofstream out(fn.string());

    // Start out by configuring the whole Dot graph with some global settings.
    out << "Digraph G {" << endl;
    out << "rankdir=" << rankdir << "; " << endl
        << "nodesep=0.5;" << endl
        << "ranksep=1.2;" << endl
        << "node [shape=record];" << endl;
    
    Utils::color_t base_color = {
        .r = 53,
        .g = 122,
        .b = 255,
        .a = 255
    };

    // Then, sequentially render each task.
    for(Task * i : tg->tasks_it()) {
        Utils::color_t taint_color = base_color;
        if(i->tainted() > 1) {
            taint_color.r = base_color.r + 32*i->tainted();
            taint_color.g = base_color.g + 20*i->tainted();
        }
        if(parent_only && i->parent_task() != nullptr) continue;
        
        // Give the task a name, and start working on its label.
        out << "task_" << i << " [label=\"" << i->id() << " | {";

        // Render the task's inputs.
        {
            out << "{";

            ostringstream output;
            bool first = true;
            for (Connector * c : i->inputs()) {
                if (!first) {
                    output << " | ";
                }

                std::string safe_tokens = c->token_type();
                boost::algorithm::replace_all(safe_tokens, "<", "\\<");
                boost::algorithm::replace_all(safe_tokens, ">", "\\>");
                
                output << "<" << c->id() << "> " << c->id() << " (" << c->tokens() << " " << safe_tokens << ") -- " << contype_to_string(c->type());
                first = false;
            }
            if(output.str().size() > 16384)
                out << "Too many input ports" ;
            else
                out << output.str();

            out << "}";
        }

        // Render the task's versions.
        if (this->verbose) {
            out << " | {";

            bool first = true;
            for(Version *v : i->versions()) {
                if (!first) {
                    out << " | ";
                }
                out << v->id();
                first = false;
                
                out << "| {{WCET} | {" << to_string(v->C()) << "}}";
                out << "| {{WCEC} | {" << to_string(v->C_E()) << "}}";
            }

            out << "}";
        }

        // Render the task's outputs.
        {
            out << " | {";

            ostringstream output;
            bool first = true;
            for (Connector * c : i->outputs()) {
                if (!first) {
                    output << " | ";
                }

                std::string safe_tokens = c->token_type();
                boost::algorithm::replace_all(safe_tokens, "<", "\\<");
                boost::algorithm::replace_all(safe_tokens, ">", "\\>");

                output << "<" << c->id() << "> " << c->id() << " (" << c->tokens() << " " << safe_tokens << ") " << contype_to_string(c->type());
                first = false;
            }
            if(output.str().size() > 16384)
                out << "Too many output ports" ;
            else
                out << output.str();

            out << "}";
        }
        out << "}";

        out << "| {{WCET} | {" << to_string(i->C()) << "}}";
        out << "| {{T} | {" << to_string(i->T()) << "}}";
        out << "| {{D} | {" << to_string(i->D()) << "}}";
        out << "| {{WCEC} | {" << to_string(i->C_E()) << "}}";
        
        // Show Profile options
        if (this->verbose)
            out << i->profile().to_dot();

        // Close the task's label.
        out << "\", style=filled";
        if(i->tainted() >= 1)
            out << ", fillcolor=\"" << Utils::color_to_html(taint_color) << "\"";
        else if(find(critical_path.begin(), critical_path.end(), i) != critical_path.end())
            out << ", fillcolor=\"#FF0000\"";
        else
            out << ", fillcolor=\"#FFFFFF\"";
        
        if(!i->force_mapping_proc().empty())
            out << ", penwidth=5.0, color=\""+core_colors.at((*i->force_mapping_proc().begin())->proc_class)+"\"";
        
        out << "];" << endl;
        
        if(parent_only) {
            // Finally, render edges between the inputs and outputs of the nodes.
            for(Task::dep_t ii : i->predecessor_tasks()) {
                if(ii.first->parent_task() != nullptr)
                    continue;

                std::vector<Connection *> &ii_successor_conns = ii.first->successor_tasks().at(i);
                std::vector<Connection *> &i_predecessor_conns = i->predecessor_tasks().at(ii.first);

                for (size_t ii_send_index = 0; ii_send_index < ii_successor_conns.size(); ii_send_index++) {
                    Connection *conn = ii_successor_conns[ii_send_index];
                    // conn also exists in i_predecessor_conns, find its index
                    size_t i_recv_index = find(i_predecessor_conns.begin(),  i_predecessor_conns.end(), conn) - i_predecessor_conns.begin();
                    dataqty_t ii_send = ii.first->eff_suc_send().at(i)[ii_send_index];
                    dataqty_t i_recv = i->eff_prev_rec().at(ii.first)[i_recv_index];

                    out << "task_" << ii.first << ":" << conn->from()->id();
                    out << " -> ";
                    out << "task_" << i << ":" << conn->to()->id();
                    out << " [label=\"" << ii_send << "/" << i_recv << "\"";
                    //        if(i->tainted() >= 1 && ii.first->tainted() >= 1)
                    //            out << ", fillcolor=\"" << Utils::color_to_html(taint_color) << "\"";
                    out << "] ;\n";
                }
            }
        }
    }
    
    if(!parent_only) {
        vector<Task*> visited;
        for(Task *i : tg->tasks_it()) {
            render_dependencies(out, i, &visited);
        }
    }

    out << "}" << endl;

    out.close();
}

string DotViewer::contype_to_string(Connector::type_e t) {
    switch(t) {
        case Connector::type_e::DUPLICATE: return "D";
        case Connector::type_e::JOINED: return "JD";
        case Connector::type_e::JOINER: return "JR";
        case Connector::type_e::REGULAR: return "R";
        case Connector::type_e::SPLITTED: return "SD";
        case Connector::type_e::SPLITTER: return "SR";
        default: return "U";
    }
    return "?";
} 

void DotViewer::render_dependencies(ostream & out, Task *i, vector<Task*> *visited) {
    
    if(find(visited->begin(), visited->end(), i) != visited->end())
        return;
    visited->push_back(i);
    
    if(i->child_tasks().size() > 0) {
        out << "subgraph cluster_" << i << "{" << endl;
        for(Task *t : i->child_tasks()) {
            render_dependencies(out, t, visited);
            out << "task_" << t << ";" << endl;
        }
        out << "task_" << i << ";" << endl;
        out << "}" << endl;
        
        for(Task *ch : i->child_tasks()) {
            for(Task::dep_t ii : ch->predecessor_tasks()) {
                if(ii.first->child_tasks().size() > 0 || find(i->child_tasks().begin(), i->child_tasks().end(), ii.first) != i->child_tasks().end())
                    continue;

                std::vector<Connection *> &ii_successor_conns = ii.first->successor_tasks().at(ch);
                std::vector<Connection *> &i_predecessor_conns = i->predecessor_tasks().at(ii.first);

                for (size_t ii_send_index = 0; ii_send_index < ii_successor_conns.size(); ii_send_index++) {
                    Connection *conn = ii_successor_conns[ii_send_index];
                    // conn also exists in i_predecessor_conns, find its index
                    size_t i_recv_index = find(i_predecessor_conns.begin(),  i_predecessor_conns.end(), conn) - i_predecessor_conns.begin();
                    dataqty_t ii_send = ii.first->eff_suc_send().at(ch)[ii_send_index];
                    dataqty_t i_recv = i->eff_prev_rec().at(ch)[i_recv_index];

                    out << "task_" << ii.first << ":" << conn->from()->id();
                    out << " -> ";
                    out << "task_" << ch << ":" << conn->to()->id();
                    out << " [label=\"" << ii_send;
                    out << "/" << i_recv << "\"";
                    //        if(i->tainted() >= 1 && ii.first->tainted() >= 1)
                    //            out << ", fillcolor=\"" << Utils::color_to_html(taint_color) << "\"";
                    out << "] ";
                    out << ";\n";
                }
            }
        }
        return;
    }
    
    vector<Task*> pch;
    if(i->parent_task() != nullptr)
        pch = i->parent_task()->child_tasks();
    
    // Finally, render edges between the inputs and outputs of the nodes.
    for(Task::dep_t ii : i->predecessor_tasks()) {
        if(i->parent_task() != nullptr && find(pch.begin(), pch.end(), ii.first) == pch.end())
            continue;

        std::vector<Connection *> &ii_successor_conns = ii.first->successor_tasks().at(i);
        std::vector<Connection *> &i_predecessor_conns = i->predecessor_tasks().at(ii.first);

        for (size_t ii_send_index = 0; ii_send_index < ii_successor_conns.size(); ii_send_index++) {
            Connection *conn = ii_successor_conns[ii_send_index];
            // conn also exists in i_predecessor_conns, find its index
            size_t i_recv_index = find(i_predecessor_conns.begin(),  i_predecessor_conns.end(), conn) - i_predecessor_conns.begin();
            dataqty_t ii_send = ii.first->eff_suc_send().at(i)[ii_send_index];
            dataqty_t i_recv = i->eff_prev_rec().at(ii.first)[i_recv_index];
            out << "task_" << ii.first << ":" << conn->from()->id();
            out << " -> ";
            out << "task_" << i << ":" << conn->to()->id();
            out << " [label=\"" << ii_send << "/" << i_recv << "\"";
//        if(i->tainted() >= 1 && ii.first->tainted() >= 1)
//            out << ", fillcolor=\"" << Utils::color_to_html(taint_color) << "\"";
            out << "] ;\n";
        }
    }

    if(transitive) {
        vector<Task*> pred = Utils::mapkeys(i->predecessor_tasks());
        vector<Task*> pred_trans = i->previous_transitiv();
        vector<Task*> diff(pred.size()+pred_trans.size());
        sort(pred.begin(), pred.end());
        sort(pred_trans.begin(), pred_trans.end());
        vector<Task*>::iterator it = set_difference(pred.begin(), pred.end(), pred_trans.begin(), pred_trans.end(), diff.begin());
        diff.resize(distance(diff.begin(), it));

        for(Task *ii : diff) {

            out << "task_" << ii;
            out << " -> ";
            out << "task_" << i;
            out << " [color=\"grey\"] ;\n";
        }
    }
}

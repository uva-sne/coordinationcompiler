/*!
 * \file ResXMLViewer.hpp
 * \copyright GNU Public License v3.
 * \date 2017 
 * 
 * Copyright (C) 2017 Benjamin Rouxel <benjamin.rouxel@inria.fr> 
 *                    Irisa/INRIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef RESXMLVIEWER_H
#define RESXMLVIEWER_H

#include "Exporter.hpp"
#include "boost/filesystem.hpp"

/**
 * Generate a XML representation of the schedule
 * 
 * \see ResXMLViewer::help
 */
class ResXMLViewer : public Exporter {
public:
    /**
     * Constructor
     */
    explicit ResXMLViewer();
    void forward_params(const std::map<std::string, std::string> &args) override;
    std::string help() override;
    const std::string get_uniqid_rtti() const override;
    
protected:
    void check_dependencies() override;
    void do_export(const boost::filesystem::path& f) override;
    const std::string file_extension() override {
        return ".xml";
    }
};

REGISTER_EXPORTER(ResXMLViewer, "xml")

#endif
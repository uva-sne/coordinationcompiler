/*
 * Copyright (C) 2017 Benjamin Rouxel <benjamin.rouxel@inria.fr>
 *                    Irisa/INRIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef DOTVIEWER_H
#define DOTVIEWER_H

#include "Exporter.hpp"
#include "boost/filesystem.hpp"
#include <boost/algorithm/string/replace.hpp>
#include "Utils/Color.hpp"

/**
 * Export the dot representation of the graphs
 * 
 * \see help
 * 
 * \see https://graphviz.org/doc/info/lang.html
 */
class DotViewer : public Exporter {
    std::string rankdir = "LR"; //!< reading direction left-right, portrait picture
    bool transitive = false; //!< add transitive edges relationship
    bool parent_only = false;
    bool verbose = true; //!< include all versions and profiles
public:
    //! Constructor 
    explicit DotViewer();
    
    void forward_params(const std::map<std::string, std::string> &args) override;
    std::string help() override;
    const std::string get_uniqid_rtti() const override;

protected:
    void do_export(const boost::filesystem::path &f) override;
    const std::string file_extension() override {
        return ".dot";
    }

    /**
     * Return the string representation of a connector type
     * @param t
     * @return 
     */
    std::string contype_to_string(Connector::type_e t);
    
    void render_dependencies(std::ostream & out, Task *i, std::vector<Task*> *visited);
};

REGISTER_EXPORTER(DotViewer, "dot");

#endif

/*!
 * \file Utils.hpp
 * \copyright GNU Public License v3.
 * \date 2017 
 * \author Benjamin Rouxel <benjamin.rouxel@inria.fr>, Irisa/INRIA
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef UTILS_HPP
#define UTILS_HPP

#include <string>
#include <stdio.h>
#include <iostream>
#include <cxxabi.h>
#include <boost/lexical_cast.hpp>
#include <boost/integer/common_factor.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/compute/functional/identity.hpp>

#define EOF (-1) // boost bug? EOF undefined
#include <boost/multiprecision/cpp_int.hpp>
#include <sys/stat.h>
#include <numeric>
#include <vector>
#include <map>
#include <type_traits>

#ifdef __APPLE__
#include <mach/vm_statistics.h>
#include <mach/mach_types.h>
#include <mach/mach_init.h>
#include <mach/mach_host.h>
#include<mach/mach.h>
#include <sys/types.h>
#include <sys/sysctl.h>
#endif

#ifdef __linux
#include <sys/types.h>
#include <sys/sysinfo.h>
#include <fstream>
#endif

#include <boost/filesystem.hpp>

using boost::multiprecision::int256_t;
using boost::multiprecision::uint256_t;
#include "Properties.hpp"

//! Real type to use for uint128_t numbers
//typedef boost::multiprecision::uint128_t uint128_t;
typedef boost::uint128_type uint128_t;
//! Type for timing informations (WCET, period, deadline, ...) usually in nanoseconds
typedef uint64_t timinginfos_t;
//! Type for data size in memory, usually in bits
typedef uint64_t datamemsize_t;
//! Type for data quantity
typedef uint32_t dataqty_t;
//! Type for timing informations (hyperperiod, ...) usually in nanoseconds
typedef uint64_t globtiminginfos_t;
//! Type for energy consumption, usually in Joules
typedef uint64_t energycons_t;
//! Type for frequency, usually in Hz etc
typedef uint64_t frequencyinfo_t;
//! Type for power, usually in Watt
typedef uint64_t powerinfo_t;

class Task;
class Version;
class SystemModel;

/*! \brief Return a string representation of a uint128_t number
 * \param x the number
 * \return a string
 */
std::string to_string(uint128_t x);
std::string to_string(uint256_t x);
/*! \brief Overload << operator to write a uint128_t number
 * \param os output stream
 * \param x the number
 * \return the output stream
 */
std::ostream& operator<< (std::ostream& os, const uint128_t x);
std::ostream& operator<< (std::ostream& os, const uint256_t x);

//! \brief Namespace containing global utilitary functions
namespace Utils {
    /*! \brief Return a demangle representation of a type name
     * 
     * \param str the type name
     * \return its intelligible representation
     */
    std::string demangle(const std::string & str);
    /*! \brief Random function to flip a coin
     * \return true or false, only god knows
     */
    bool flipcoin();
    
    /*! \brief Sort task by communication
     * \param a
     * \param b
     * \return bool if communication of a < b
     */
    bool sort_Q_byComm(Task *a, Task *b);
    /*! \brief Sort task by name
     * \param a
     * \param b
     * \return bool if name of a < b
     */
    bool sort_Q_byName(Task *a, Task *b);
    /*! \brief Sort task by memory usage
     * \param a
     * \param b
     * \return bool if memory usage of a < b
     */
    bool sort_Q_byMemUsageDec(Version *a, Version *b);
    /*! \brief Sort task by WCET
     * \param a
     * \param b
     * \return bool if WCET of a < b
     */
    bool sort_Q_byWCET(Task *a, Task *b);
    /*! \brief Sort task by WCEE
     * \param a
     * \param b
     * \return bool if WCEE of a < b
     */
    bool sort_Q_byWCEE(Task *a, Task *b);
    
    /*! \brief Convert a size information given in string into a uin64_t
     * 
     * Possible units: {;K;M;G}{;o;B;b}
     * 
     * \param s
     * \return number in b
     */
    uint64_t stringmem_to_bit(const std::string &s);
    /*! \brief Inverse of #Utils::size_to_uint
     * \param x
     * \return string
     */
    std::string bit_to_stringmem(uint64_t x);
    
    // nano to milli and vice versa for ILP solvers.
    int256_t nano_to_milli (int256_t nano);
    int256_t milli_to_nano (int256_t milli);
    uint256_t nano_to_milli (uint256_t nano);
    uint256_t milli_to_nano (uint256_t milli);
    uint128_t nano_to_milli (uint128_t  nano);
    uint128_t  milli_to_nano (uint128_t  milli);

    // Hz to kHz
    int256_t Hz_to_kHz (int256_t Hz);
    frequencyinfo_t  Hz_to_kHz (frequencyinfo_t Hz);
    
    timinginfos_t stringtime_to_time(const std::string &stime, const std::string &time_unit);
    /*! \brief Convert a time information given in string into a uint128_t
     * 
     * Possible units: Hz, mHz, cycles, ns, us, s, m, h
     * 
     * \param s
     * \return the value in ns
     */
    timinginfos_t stringtime_to_ns(const std::string &s);
    /*! \brief Inverse of #Utils::stringtime_to_ns
     * \param unit
     * \return string
     */
    std::string nstime_to_string(timinginfos_t, const  std::string &unit="S");
    timinginfos_t stringtime_to_ms(const std::string &);
    timinginfos_t stringtime_to_ds(const std::string &);
    timinginfos_t stringtime_to_s(const std::string &);
    
     timinginfos_t stringtime_to_us(const std::string &stime);
     timinginfos_t stringtime_to_m(const std::string &stime);
     timinginfos_t stringtime_to_h(const std::string &stime);

    /*! \brief Convert an energy given in string into a float
     * 
     * Possible units: kWh, Wh, kJ, J
     * energy - nano-joule is too small for CPLEX (exceeds integer limit)
     * 
     * \param s
     * \return number in J
     */
    energycons_t stringenergy_to_energy(const std::string &sener, const std::string &energy_unit);
    energycons_t stringenergy_to_joule(const std::string &s);
    energycons_t stringenergy_to_ujoule(const std::string &sener);
    energycons_t stringenergy_to_njoule(const std::string &sener);
    std::string  njoule_to_stringenergy(energycons_t, const std::string &unit="J");
    energycons_t stringenergy_to_mjoule(const std::string &sener);

    //frequency in Hz
    frequencyinfo_t stringfrequency_to_frequency(const std::string &sfrequency, const std::string &frequency_unit);
    frequencyinfo_t stringfrequency_to_Hz(const std::string &);
    std::string frequency_to_stringfrequency(uint128_t, const std::string &unit="mHz");
    frequencyinfo_t stringfrequency_to_kHz(const std::string &stime);
    frequencyinfo_t stringfrequency_to_mHz(const std::string &);
    frequencyinfo_t stringfrequency_to_gHz(const std::string &);

    // nanowatt
    powerinfo_t stringwatt_to_power(const std::string &swatt, const std::string &power_unit);
    powerinfo_t stringwatt_to_nwatt(const std::string &);
    std::string nwatt_to_stringwatt(uint128_t, const std::string &unit="W");
    powerinfo_t stringwatt_to_watt(const std::string &);
    powerinfo_t stringwatt_to_mwatt(const std::string &);
    powerinfo_t stringwatt_to_uwatt(const std::string &swatt);

    // Conversion
    energycons_t get_energy_units_per_power_time_unit(const std::string time_unit, const std::string power_unit, const std::string energy_unit);
    
    /*! \brief Return the min of 2 values
     * \param a
     * \param b
     * \return (a < b) ? a : b
     */
    template<typename T1, typename T2>
    typename std::enable_if<std::is_convertible<T2, T1>::value, T1>::type
        min(T1 a, T2 b) {
        if(a < b) return a;
        return (T1)b;
    }
    
    /*! \brief Return the max of 2 values
     * \param a
     * \param b
     * \return (a > b) ? a : b
     */
    template<typename T1, typename T2>
    typename std::enable_if<std::is_convertible<T2, T1>::value, T1>::type
        max(T1 a, T2 b) {
        if(a > b) return a;
        return (T1)b;
    }
    
    // Helper to determine whether there's a const_iterator for T.
    template<typename T>
    struct has_const_iterator {
        private:
            template<typename C> static char test(typename C::const_iterator*);
            template<typename C> static int  test(...);
        public:
            enum { value = sizeof(test<T>(0)) == sizeof(char) };
    };
    
    /*! \brief Compute the Least Common Multiple of a vector
     * \param values a vector of value
     * \return LCM
     */
    template<typename _Container, typename _AccumulatorType = typename _Container::value_type, typename = std::enable_if<has_const_iterator<_Container>::value>>
    _AccumulatorType lcm(const _Container &values) {
        return std::accumulate(values.begin(), values.end(), _AccumulatorType(1),
                [](const typename _Container::value_type &a, const typename _Container::value_type &b) {
#if _LIBCPP_STD_VER > 14
                    return _AccumulatorType(a) * _AccumulatorType(b) / __gcd(a, b);
#else
                    return _AccumulatorType(a) * _AccumulatorType(b) / boost::integer::gcd(a, b);
#endif
                });
    }
    
    /*! \brief Display the current memory usage
     * \param msg a prefix message
     */
    template<typename _Container, typename = std::enable_if<has_const_iterator<_Container>::value>>
    typename _Container::value_type
        gcd(const _Container &values) {
        return std::accumulate(values.begin(), values.end(), (typename _Container::value_type)0, [](const typename _Container::value_type & a, const typename _Container::value_type & b) {
            #if _LIBCPP_STD_VER > 14
            return __gcd(a, b);
            #else
            return boost::integer::gcd(a,b);
            #endif
        });
    }
    
    void memory_usage(const std::string &msg = "");
    /*!\brief Return the current RAM usage
     * \return RAM usage
     */
    uint64_t get_ram_usage();
    /*!\brief Return the current SWAP usage
     * \return SWAP usage
     */
    uint64_t get_swap_usage();
    /*!\brief Return the total RAM usage
     * \return total RAM usage
     */
    uint64_t get_total_ram();
    /*!\brief Return the total SWAP usage
     * \return SWAP usage
     */
    uint64_t get_total_swap();

    /**
     * Find the index of the search key in the vector
     * @tparam _ValueType
     * @param values
     * @param search
     * @return
     */
    template<typename _ValueType>
    std::optional<size_t> indexof(std::vector<_ValueType> &values, _ValueType &search) {
        auto it = find(values.begin(), values.end(), search);
        if (it == values.end())
            return std::nullopt;
        return it - values.begin();
    }
    
    /*! \brief Return the list of keys in a std::map
     * \param m input map
     * \return the list of keys
     */
    template<typename _KeyType, typename _ValueType, typename... _Extra>
    std::vector<_KeyType> mapkeys(const std::map<_KeyType, _ValueType, _Extra...> &m) {
        // TODO: this function is overused and has O(n) time complexity with O(n) space complexity
        // Why not just return an iterator?
        std::vector<_KeyType> results;
        for(typename std::map<_KeyType, _ValueType, _Extra...>::const_iterator it=m.begin(), et=m.end() ; it != et ; ++it)
            results.push_back(it->first);
        return results;
    }

    /*! \brief Return the list of values in a std::map
     * \param m input map
     * \return the list of values
     */
    template<typename _KeyType, typename _ValueType, typename... _Extra>
    std::vector<_ValueType> mapvalues(const std::map<_KeyType, _ValueType, _Extra...> &m) {
        // TODO: this function is overused and has O(n) time complexity with O(n) space complexity
        // Why not just return an iterator?
        std::vector<_ValueType> results;
        for(typename std::map<_KeyType, _ValueType, _Extra...>::const_iterator it=m.begin(), et=m.end() ; it != et ; ++it)
            results.push_back(it->second);
        return results;
    }

    /*! \brief Return the list of values from collections in a std::map
     * \param m input map of type <_Key, _Vector_Of_Values>
     * \return the vector of all values
     */
    template<typename _KeyType, typename _ValueType, typename... _Extra>
    std::vector<_ValueType> flatmapvalues(const std::map<_KeyType, std::vector<_ValueType>, _Extra...> &m) {
        return flatmapvalues(m, boost::compute::identity<_ValueType>());
    }

    template<typename _KeyType, typename _ValueType, typename _ReturnValueType, typename... _Extra>
    std::vector<_ReturnValueType> flatmapvalues(const std::map<_KeyType, std::vector<_ValueType>, _Extra...> &m, const std::function<_ReturnValueType(_ValueType)> &mapper) {
        std::vector<_ReturnValueType> results;
        for(typename std::map<_KeyType, std::vector<_ValueType>, _Extra...>::const_iterator it=m.begin(), et=m.end() ; it != et ; ++it)
            for (auto &v : it->second)
                results.push_back(mapper(v));
        return results;
    }

    /*! \brief Return a vector of values in a std::map. All values have been std::move'd, making the values in the map
     * null or default constructed values.
     * \param m input map
     * \return the vector of values
     */
    template<typename _KeyType, typename _ValueType>
    std::vector<_ValueType> movevalues(std::map<_KeyType, _ValueType> &m) {
        std::vector<_ValueType> results;
        for(typename std::map<_KeyType, _ValueType>::iterator it=m.begin(), et=m.end() ; it != et ; ++it)
            results.push_back(std::move(it->second));
        return results;
    }

    /*! \brief Delete all element from a container (vector, map, ...) if a
     * predicate is true
     * \param items container
     * \param __pred predicate
     */
    template< typename _ContainerT, typename _PredicateT >
        void erase_if(_ContainerT& items, const _PredicateT& __pred ) {
          for( auto it = items.begin(); it != items.end(); ) {
            if( __pred(*it) ) it = items.erase(it);
            else ++it;
          }
        }
    
    /*!\brief Check if a object is of a certain type
     * \param in the object
     * \return true if the object belongs to the template arguments type
     */
    template<class _OutputClass, class _InputClass>
    bool isa(_InputClass in) {
        _OutputClass tmp = dynamic_cast<_OutputClass>(in);
        return (tmp != nullptr);
    }
    
    /**
     * Return the textual representation of a number
     * 
     * 1 -> One
     * 2 -> Two
     * ...
     * 12 -> OneTwo
     * 
     * @param n
     * @return 
     */
    template<typename _Int>
    std::string number_to_string(_Int n) {
        std::string str = std::to_string(n);
        std::string res = "";
        for(size_t i=0 ; i < str.length() ; ++i) {
            if(str.at(i) == '-')
                res += "Minus";
            else if(str.at(i) == '1')
                res += "One";
            else if(str.at(i) == '2')
                res += "Two";
            else if(str.at(i) == '3')
                res += "Three";
            else if(str.at(i) == '4')
                res += "Four";
            else if(str.at(i) == '5')
                res += "Five";
            else if(str.at(i) == '6')
                res += "Six";
            else if(str.at(i) == '7')
                res += "Seven";
            else if(str.at(i) == '8')
                res += "Eight";
            else if(str.at(i) == '9')
                res += "Nine";
        }
        return res;
    }
}

#define NS_TO_S(input) ((input) / 1000UL / 1000UL / 1000UL)
#define NS_TO_S__FP(input) ((input) / 1000.0 / 1000.0 / 1000.0)
#define NS_TO_DS(input) ((input) / 10UL)
#define NS_TO_MS(input) ((input) / 1000UL)
#define S_TO_NS(input) ((input) * 1000UL * 1000UL * 1000UL)

#endif /* UTILS_HPP */


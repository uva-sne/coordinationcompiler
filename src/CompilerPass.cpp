/*
 * Copyright (C) 2022 Lukas Miedema <l.miedema@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "CompilerPass.hpp"
#include "StateMachine/StrategyStateMachine.hpp"

void CompilerPass::run(SystemModel *m, config_t *c, StrategyStateMachine *ssm) {
    run(m, c, ssm == nullptr ? nullptr : ssm->operator Schedule *());
}

void CompilerPass::run(SystemModel *m, config_t *c, Schedule *s) {
    throw CompilerException("compiler-pass", "Not implemented");
}

void CompilerPass::set_params(const std::map<std::string, std::string> &args) {
    if(args.count("force_unsched"))
        force_unsched = args.at("force_unsched") == CONFIG_TRUE;
    forward_params(args);
}


/*!
 * \file config.hpp
 * \copyright GNU Public License v3.
 * \date 2017 
 * \author Benjamin Rouxel <benjamin.rouxel@inria.fr>, Irisa/INRIA
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CONFIG_H
#define CONFIG_H

#include <cstdio>
#include <cstdlib>
#include <string>
#include <execinfo.h>
#include <map>
#include <iostream>
#include <set>
#include <vector>
#include <boost/filesystem.hpp>
#include <boost/mpl/string.hpp>
#include <boost/metaparse/string.hpp>
#include "Dispatcher.hpp"

//! Bool value for TRUE in config file
#define CONFIG_TRUE "true"
//! Bool value for FALSE in config file
#define CONFIG_FALSE "false"

//! Trace buffer size to display the backtrace in exception
#define TRACE_BUFFER_SIZE 100

//! Default version name
#define CONFIG_DEF_VERSION_NAME string("")

#ifdef __GNU_LIBRARY__ //Linux
#   define NOEX _GLIBCXX_USE_NOEXCEPT
#endif
//! Compatibility macro
#ifdef _NOEXCEPT    //Darwin
#   define NOEX _NOEXCEPT
#endif
#ifndef NOEX
#   define NOEX
#endif


/*! \brief Exception throw in case of an error in the compilation process
 */
class CompilerException : public std::exception {
    //! Message explaining the error
    std::string msg;
    //! Type or category or placeholder where the exception arised
    std::string _type;
    //! Should the displayed message include a backtrace
    bool _backtrace;
    
public:
    /*! \brief Construct a new exception
     * 
     * \param t \copybrief CompilerException::_type
     * \param m \copybrief CompilerException::msg
     * \param bt \copybrief CompilerException::_backtrace
     */
    explicit CompilerException(const std::string &t, const std::string &m, bool bt=false) : std::exception(), _type(t), _backtrace(bt) {
        msg = m + "\e[0m\n";
        
        if(_backtrace) {
            void *buffer[TRACE_BUFFER_SIZE];
            int nbtracerecords = backtrace(buffer, TRACE_BUFFER_SIZE);
            char **tracerecords = backtrace_symbols(buffer, nbtracerecords);
            if (tracerecords == NULL) {
                perror("backtrace_symbols");
                exit(EXIT_FAILURE);
            }
        
            for(int i=0 ; i < nbtracerecords ; ++i) {
                msg += "\t";
                msg += tracerecords[i];
                msg += "\n";
            }
            if(nbtracerecords > 0)
                free(tracerecords);
        }
    }
    //! destructor
    virtual ~CompilerException() NOEX = default;
    
    /*! \brief Display the exception message
     * \return the message
     */ 
    virtual const char* what() const NOEX override {
        return msg.c_str();
    }
    
    /*! \copybrief CompilerException::_type
     * \return #CompilerException::_type
     */
    virtual const std::string & type() const {return _type;}
    //! \copydoc CompilerException::type() const
    virtual const std::string & type() {return _type;}
};

#define C_STR(str_) boost::mpl::c_str< BOOST_METAPARSE_STRING(str_) >::value
/*!
 * \brief Specialized compiler pass exception for one particular location.
 * Usage: using E = LabeledCompilerException<C_STR("my-compiler-pass")>;
 *        throw E("my message");
 * @tparam label
 */
template<const char* label>
class LabeledCompilerException : public CompilerException {
public:
    explicit LabeledCompilerException(const std::string &m, bool bt=false) : CompilerException(label, m, bt) {}
    virtual ~LabeledCompilerException() NOEX = default;
};

/*! \brief Exception raised when dev when to stress a todo point*/
class Todo : public std::exception {
    //! Message explaining the error
    std::string msg;
    
public:
    /*! \brief Construct a new exception
     * 
     * \param m \copybrief CompilerException::msg
     */
    explicit Todo(const std::string &m) : std::exception(), msg(m) {}
    /*! \brief Display the exception message
     * \return the message
     */ 
    virtual const char* what() const NOEX override {
        return msg.c_str();
    }
};

class Timeout : public std::exception {
    
public:
    explicit Timeout() : std::exception() {}
};

class CompilerPass;
//! Runtime Type of task
enum runtime_task_e {
    AER, //!< Use the Acquisition-Execution-Restitution (AER) method
    PREM, //!< Use the PRedictable Executable Model (PREM)
    SINGLEPHASE //!< Monolithic runtime
};
//! Type of task set
//! \todo implement an infering mechanism
enum task_model_e {
    PERIODIC, //!< tasks are all periodic
    SPORADIC, //!< tasks all sporadic
    GRAPH, //!< tasks is in a single graph
    MULTIGRAPH, //!< task set is composed of multiple graphs
    MIXED_PERIODIC_SPORADIC, //!< task set is composed of a mix of periodic and sporadic
    MIXED_PERIODIC_GRAPH,//!< task set is composed of graph and single node task, all periodic
    MIXED_SPORADIC_GRAPH,//!< task set is composed of graph and single node task, all sporadic
    MIXED,//!< task set is a mix
    OTHERTM //!< task set is something
};
//! Type of graph
//! \todo implement an infering mechanism
enum graph_type_e {
    SDF, //!< graph is a SDF
    HSDF, //!< graph is a HSDF
    CSDF, //!< graph is a CSDF
    FORKJOIN, //!< graph is a Fork-Join
    OTHERGRAPH //!< graph is something
};

namespace config {
    //! Structure to describe a data type
    struct datatype {
        uint64_t size_bits;//!< size in bits
        std::string initval; //!< Initalisation value
        std::string ctype; //!< Correspinding string to add in generated code
    };
}

//! Contains all the configuration
struct config_t {

    config_t() = default;
    config_t(config_t&) = delete;

    //! Contains the input/output files configuration
    //! The base name of the coordination file (--coord argument) is used as a default filename for newly generated
    //! files.
    struct {
        boost::filesystem::path config_path; //!< full path to configuration file (e.g. "config.xml")
        boost::filesystem::path coord_path; //!< full path to coordination file (e.g. "some/path/forkjoin.tey")
        boost::filesystem::path output_folder; //!< output folder, either provided with --output or the current working dir
        boost::filesystem::path coord_basename; //!< filename of coordination file, without extension (e.g. "forkjoin")
        boost::filesystem::path nfp_path; //!< full path to the non-functional properties (NFP) file (e.g. "some/path/properties.nfp")
    } files = {};
    
    std::set<boost::filesystem::path> outputs; //!< what are the wished outputs, see into Generator folder

    struct {
        struct {
            bool assign_region; //should a phase of SPM allocation be done?
            bool store_code; //should the SPM store the code?
            bool prefetch_code; //should the read phase of a task prefetch the code?
        } spm = {};
        
        //! How physically task communicate
        struct {
            std::string type; //!< BUS / NOC
            std::string arbiter; //!< arbiter is FAIR / TDM
            uint32_t active_ress_time; //!< active window time
            uint32_t bit_per_timeunit;//!< number of token per unit of time
            std::string mechanism; //!<direct: SPM -> SPM ; shared : SPM -> MEM -> SPM ; sharedonly : idem shared and forced for dependent tasks on the same core
            std::string behavior; //!< blocking/non-blocking
            std::string burst; //!< none / edge / packet (Dslot) / fine (bit/timeunit)
        } interconnect = {};
    } archi = {};
    
    //! Config for the runtime environment
    //! \todo implement an infering mechanism
    struct {
        runtime_task_e rtt; //!< runtime
        task_model_e tm; //!< task model type
        graph_type_e gt; //!< graph type
    } runtime = {};
    
    std::string default_type; //!< default datatype
    std::map<std::string, struct config::datatype> datatypes;//!< map datatype names and their attributes
    
    //! Compiler pass to call in order
    Dispatcher passes;
};

/*! \brief Initialise the configuration
 * 
 * \param conf the initialised configuration
 */
void init_config(config_t *conf);
#endif /* CONFIG_H */


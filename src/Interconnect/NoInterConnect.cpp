/* 
 * Copyright (C) 2017 Benjamin Rouxel <benjamin.rouxel@inria.fr> 
 *                    Irisa/INRIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "NoInterConnect.hpp"

using namespace std;

NoComm::NoComm(const SystemModel& t, const config_t& c) : CoreMemInterConnect(t, c) {}

timinginfos_t NoComm::comm_delay(uint32_t concurrency, dataqty_t data, string datatype, timinginfos_t forced_delay) const {
    return 0; 
}

timinginfos_t NoComm::wait_time(uint32_t concurrency, dataqty_t data) const {
    return 0;
}

uint32_t NoComm::worst_contention() const {
    return 0;
}

timinginfos_t NoComm::getActiveWindowTime() const {
    return 0;
}
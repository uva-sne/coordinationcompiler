/* 
 * Copyright (C) 2017 Benjamin Rouxel <benjamin.rouxel@inria.fr> 
 *                    Irisa/INRIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef COMMUNICATIONMODEL_H
#define COMMUNICATIONMODEL_H

#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <map>
#include <vector>
#include <cmath>
#include <string>

#include "config.hpp"
#include "registry.hpp"

class SystemModel;
class Task;
struct ComputeUnit;

/**
 * Allows to compute the communication between a core and a memory
 * 
 * When data are being transfered from the core to a memory, e.g. external memory,
 * SPM, etc... there is a cost in time to pay, a delay. The computation of this 
 * cost varies with the type of the medium, the type of arbitration policy, and 
 * the involved concurrency.
 */
class CoreMemInterConnect {
protected:
    //! IR of the current application
    const SystemModel &tg;
    //! the global configuration
    const config_t &conf;
public:
    /**
     * Constructor
     * @param t
     * @param c
     */
    explicit CoreMemInterConnect(const SystemModel & t, const config_t &c) : tg(t), conf(c) {}
    //! Destructor
    virtual ~CoreMemInterConnect() {}
    
    /**
     * Compute the communication delay to send some data of a certain datatype with a given concurrency
     * 
     * @param concurrency
     * @param data amount of element of datatype sent (i.e. tokens)
     * @param datatype 
     * @param forced_delay don't compute the cost, return this forced value. Used with previous work on Kalray
     * @return 
     */
    virtual timinginfos_t comm_delay(uint32_t concurrency, dataqty_t data, std::string datatype) const;
    virtual timinginfos_t comm_delay(uint32_t concurrency, dataqty_t data, std::string datatype, timinginfos_t forced_delay) const = 0;
    
    /**
     * Return the worst contention
     * @return 
     */
    virtual uint32_t worst_contention() const = 0;
    
    /**
     * Return the maximum time the arbiter will grant access, if there is one
     * 
     * @return 
     */
    virtual timinginfos_t getActiveWindowTime() const = 0;
    
    /**
     * Split the data to set in chunks to create a burst communication
     * 
     * @param conf
     * @param data
     * @param datatypesize
     * @return 
     */
    static std::vector<dataqty_t> burst(const config_t &conf, dataqty_t data, datamemsize_t datatypesize);
    
    /**
     * Return how much tokens can be sent within a chunk in burst communication
     * @param conf
     * @param datatypesize
     * @return 
     */
    static dataqty_t token_per_burst_chunk(const config_t &conf, datamemsize_t datatypesize);
    
protected:
    /**
     * Compute the time to wait for the resource to be available
     * 
     * This waiting time is dependent on the concurrency. Then some arbiters grant
     * access for a specific duration before putting the transmission on hold, which is
     * then resumed after some time.
     * 
     * @param concurrency
     * @param data amount of data in bits
     * @return 
     */
    virtual timinginfos_t wait_time(uint32_t concurrency, dataqty_t data) const = 0;
};

using InterConnectRegistry = registry::Registry<CoreMemInterConnect, std::string, const SystemModel &, const config_t &>;

#define REGISTER_INTERCONNECT(ClassName, Identifier) \
  REGISTER_SUBCLASS(CoreMemInterConnect, ClassName, std::string, Identifier, const SystemModel &, const config_t &)

#endif /* COMMUNICATIONMODEL_H */


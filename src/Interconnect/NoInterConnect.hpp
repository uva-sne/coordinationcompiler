/* 
 * Copyright (C) 2017 Benjamin Rouxel <benjamin.rouxel@inria.fr> 
 *                    Irisa/INRIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef NOINTERCONNECT_H
#define NOINTERCONNECT_H

#include "CoreMemInterConnect.hpp"

/**
 * To disable core to memory communication
 * 
 * All communication delays are 0
 */
class NoComm : public CoreMemInterConnect {
public:
    /**
     * Constructor
     * @param t
     * @param c
     */
    NoComm(const SystemModel& t, const config_t& c);

    virtual timinginfos_t comm_delay(uint32_t concurrency, dataqty_t data, std::string datatype, timinginfos_t forced_delay) const override;
    virtual timinginfos_t wait_time(uint32_t concurrency, dataqty_t data) const override;
    virtual uint32_t worst_contention() const override;
    virtual timinginfos_t getActiveWindowTime() const override;
};

REGISTER_INTERCONNECT(NoComm, "NONE");

#endif
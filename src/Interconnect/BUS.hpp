/* 
 * Copyright (C) 2018 Benjamin Rouxel <benjamin.rouxel@inria.fr> 
 *                    Irisa/INRIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef BUS_H
#define BUS_H

#include "CoreMemInterConnect.hpp"
#include "SystemModel.hpp"

/**
 * Allows to compute the communication between a core and a memory using a BUS
 * 
 * When data are being transfered from the core to a memory, e.g. external memory,
 * SPM, etc... there is a cost in time to pay, a delay. The computation of this 
 * cost varies with the type of the medium, the type of arbitration policy, and 
 * the involved concurrency.
 * 
 * Only FAIR Round-Robin policy is supported for the arbitration policy. This means
 * that the arbiter grants access to the bus to each core in a round-robin fashion
 * for a configured maximal time window. Then the core transmission is put on hold
 * until the round-robin comes back.
 * 
 */
class BUS : public CoreMemInterConnect {
public:
    /**
     * Constructor
     * @param t
     * @param c
     */
    explicit BUS(const SystemModel& t, const config_t& c);

    virtual timinginfos_t comm_delay(uint32_t concurrency, dataqty_t data, std::string datatype, timinginfos_t forced_delay) const override;

   /**
    * \copydoc CoreMemInterConnect::wait_time
    * \note Return the wait time that is avoidable depending if we are in conflict or not
    */
    virtual timinginfos_t wait_time(uint32_t concurrency, dataqty_t data) const override;
    
    virtual uint32_t worst_contention() const override;
    
    virtual timinginfos_t getActiveWindowTime() const override;
    
private:
    std::string arbiter; //!< Type of arbiter, for now only FAIR is supported
    
    /**
     * Compute the communication delay for a BUS using a FAIR arbitration policy
     * 
     * @param concurrency
     * @param data_sent
     * @param datatype
     * @return 
     */
    timinginfos_t comm_delay_FAIR(uint32_t concurrency, dataqty_t data_sent, std::string datatype) const;
};

REGISTER_INTERCONNECT(BUS, "BUS");

#endif
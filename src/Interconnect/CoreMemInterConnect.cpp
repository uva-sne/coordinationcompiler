/* 
 * Copyright (C) 2017 Benjamin Rouxel <benjamin.rouxel@inria.fr> 
 *                    Irisa/INRIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "CoreMemInterConnect.hpp"

#include "SystemModel.hpp"

using namespace std;

dataqty_t CoreMemInterConnect::token_per_burst_chunk(const config_t &conf, datamemsize_t datatypesize) {
    if(conf.archi.interconnect.burst == "flit") {
        return conf.archi.interconnect.bit_per_timeunit / datatypesize;
    }
    else if(conf.archi.interconnect.burst == "packet") {
        return (conf.archi.interconnect.bit_per_timeunit * conf.archi.interconnect.active_ress_time ) / datatypesize;
    }
    return (dataqty_t)-1; //infinity
}

//return a number of tokens, can not send half a token
vector<dataqty_t> CoreMemInterConnect::burst(const config_t &conf, dataqty_t data, datamemsize_t datatypesize) {
    vector<dataqty_t> res;
    datamemsize_t burst_chunk_size = token_per_burst_chunk(conf, datatypesize);
    
    while(data >= burst_chunk_size) {
        res.push_back(burst_chunk_size);
        data -= burst_chunk_size;
    }
    
    // burst == "edge" will just add the data here
    if(data > 0)
        res.push_back(data);
 
    return res;
}

datamemsize_t CoreMemInterConnect::comm_delay(uint32_t concurrency, dataqty_t data_sent, string datatype) const {
    return comm_delay(concurrency, data_sent, datatype, 0);
}





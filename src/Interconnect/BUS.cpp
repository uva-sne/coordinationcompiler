/* 
 * Copyright (C) 2018 Benjamin Rouxel <benjamin.rouxel@inria.fr> 
 *                    Irisa/INRIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "BUS.hpp"

using namespace std;

BUS::BUS(const SystemModel& t, const config_t& c) : CoreMemInterConnect(t,c) {
    arbiter = c.archi.interconnect.arbiter;
}

timinginfos_t BUS::comm_delay(uint32_t concurrency, dataqty_t data_sent, string datatype, timinginfos_t forced_delay) const {
    if(data_sent == 0)
        return 0;
    if(forced_delay > 0)
        return forced_delay;

    if(arbiter == "FAIR")
        return comm_delay_FAIR(concurrency, data_sent, datatype);

    return data_sent;
}
/**
* Return the wait time that is avoidable depending if we are in conflict or not
* 
* @return 
*/
timinginfos_t BUS::wait_time(uint32_t concurrency, dataqty_t data) const {
    if(arbiter == "FAIR") {// if there is no conflict, we can ensure the request will be served immediatly
        if(data == 0 || concurrency == 0) return 0;

        dataqty_t tokens = data / conf.archi.interconnect.bit_per_timeunit;
        uint32_t waiting_slot = ceil(tokens / (float)conf.archi.interconnect.active_ress_time);
        return concurrency * waiting_slot * conf.archi.interconnect.active_ress_time;
    }

    return 0;
}

uint32_t BUS::worst_contention() const {
    return tg.processors().size()-1;
}

timinginfos_t BUS::getActiveWindowTime() const { 
    return conf.archi.interconnect.active_ress_time; 
}

timinginfos_t BUS::comm_delay_FAIR(uint32_t concurrency, dataqty_t data_sent, string datatype) const {
    data_sent = data_sent * conf.datatypes.at(datatype).size_bits;
    dataqty_t token_sent = ceil(data_sent / (float)conf.archi.interconnect.bit_per_timeunit);
    uint32_t nb_slot = floor(token_sent / (float)conf.archi.interconnect.active_ress_time);
    dataqty_t remaining_data = token_sent % conf.archi.interconnect.active_ress_time;

    timinginfos_t wait = wait_time(concurrency, data_sent);

    return wait + conf.archi.interconnect.active_ress_time * nb_slot + remaining_data;
}
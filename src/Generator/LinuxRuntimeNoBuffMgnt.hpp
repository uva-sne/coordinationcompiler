/*
 * Copyright (C) 2019 Benjamin Rouxel <benjamin.rouxel@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CODEGENERATORNOBUFF_H
#define CODEGENERATORNOBUFF_H

#include "LinuxRuntime.hpp"
#include "CodeGen/CHelpers/NoBufferMgnt.hpp"

/**
 * Generate the glue code for the provided application targeting Linux-based OS
 * 
 * At the different of LinuxRuntime, this generator do not generate the code for
 * managing the FIFO buffers
 * 
 * \see LinuxRuntime
 * \see LinuxRuntimeNoBuffMgnt::help
 */
class LinuxRuntimeNoBuffMgnt : public LinuxRuntime {
public:
    //! Constructor
    explicit LinuxRuntimeNoBuffMgnt();
    const std::string get_uniqid_rtti() const override;
    
    void generate(const boost::filesystem::path& fn) override;
    virtual void forward_params(const std::map<std::string, std::string> &args) override;
    virtual std::string help() override;
};

REGISTER_GENERATOR(LinuxRuntimeNoBuffMgnt, "linux_nobuf")
#endif /* CODEGENERATORNOBUFF_H */


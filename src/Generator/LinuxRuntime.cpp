/* 
 * Copyright (C) 2017 Benjamin Rouxel <benjamin.rouxel@inria.fr> 
 *                    Irisa/INRIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "LinuxRuntime.hpp"

namespace ba = boost::algorithm;
using namespace std;

LinuxRuntime::LinuxRuntime() : Generator() {
    buffmgnt = new BufferMgnt;
    syncmgnt = new DataExchangeSynchro;
    mk = new Makefile;
    ccode = new CCode;
}

IMPLEMENT_HELP_WITH_CODE(LinuxRuntime, {msg += ccode->help()
            +strategy->help()
            +syncmgnt->help()
            +buffmgnt->help()
            +mk->help();
}, Generate code to run the app on GNU/Linux.\n
    env [user-space, init, kernel-space]: Select in what environment 
    the application should be running.\n
    HTMLINDENT- user-space: executed in user space.\n
    HTMLINDENT- init: replace the init process with this app, executed at boot time.\n
    HTMLINDENT- kernel-space (soon): the app is built as a kernel module.\n
)

void LinuxRuntime::forward_params(const map<string, string> &args) {

    string env = "user-space";
    if(args.count("env"))
        env = args.at("env");

    strategy = TargetRuntimeRegistry::Create(env);
    strategy->setParams(args);
    buffmgnt->setParams(args);
    syncmgnt->setParams(args);
    mk->setParams(args);
    ccode->setParams(args);
}

void LinuxRuntime::generate(const boost::filesystem::path& fn) {
    if(strategy == nullptr)
        throw CompilerException("codegen", "Unspecified target os");
    
    buffmgnt->init(tg, conf, sched, strategy);
    syncmgnt->init(tg, conf, sched, strategy);
    mk->init(tg, conf, sched, strategy);
    ccode->init(tg, conf, sched, strategy);

    strategy->generatePath(fn, ccode->codegendir());

    generateBufferMgnt();
    generateMainCode();
    
    mk->generate(ccode->includes());
}

void LinuxRuntime::generateBufferMgnt() {
    string deffile = buffmgnt->generate_definitions();
    ba::replace_first(deffile, txtinc, ccode->includes_to_string());
    ofstream ofs((strategy->path() / buffmgnt->getFileName(mk->target_code_language())).string());
    ofs << deffile;
    ofs.close();
    ccode->addInclude(buffmgnt->getFileName(mk->target_code_language()));
}

string LinuxRuntime::generateTasksCode() {
    string tasks = "";
    for(Task *t : tg->tasks_it()) {
        for(Version *v : t->versions()) {
            tasks += generateTask(t, v);
        }
    }
    return tasks;
}

string LinuxRuntime::generateTask(Task *t, Version *v) {
    string code = strategy->tpl_getTask();

    string waitpred = "";

    string inputs, outputs, args, decl;
    buffmgnt->generate_task_connection(t, &args, &decl, &inputs, &outputs);

    waitpred = syncmgnt->generate_wait_predecessors(buffmgnt, t);

    string fncallable;
    if(v->cname().empty())
        throw CompilerException("c-code", "Don't know how to call task "+t->id());
    if(!v->csignature().empty())
        fncallable = v->cname()+"("+v->csignature()+", "+args.substr(0, args.size()-2)+");"; //we will see what I have for the signature inside the nfp, but for FMRadio I need it due to the replication of components
    else
        fncallable = v->cname()+"("+args.substr(0, args.size()-2)+");";

    ba::replace_all(code, txttaskname, t->id());
    ba::replace_all(code, txtversionname, v->id());
    ba::replace_all(code, txtperiod, to_string(t->T()));
    ba::replace_first(code, txtcallin, decl+inputs);
    ba::replace_first(code, txtcallable, fncallable);
    ba::replace_first(code, txtcallout, outputs);
    ba::replace_first(code, txtwaitpred, waitpred);
    ba::replace_first(code, txtreleasesuc, syncmgnt->generate_release_successors(buffmgnt, t));
    ba::replace_all(code, txtnbsuc, to_string(t->successor_tasks().size()));

    return code;
}

void LinuxRuntime::generateMainCode() {
    string main = strategy->tpl_getMain();


    size_t nbthreads = 0, nbsrcs = 0;
    string codeinvoke = "", codedelete = "", tasks = "", runnabletasks = "", codedecl = "";

    nbthreads = generateTasksMgnt(codeinvoke, codedelete, codedecl, runnabletasks);

    for(Task *t : tg->tasks_it()) {
        if(t->predecessor_tasks().size() == 0)
            ++nbsrcs;
    }

    tasks = generateTasksCode();

    ba::replace_all(main, txtinc, ccode->includes_to_string());

    ba::replace_first(main, txtbuffers, buffmgnt->generate_declarations());
    ba::replace_first(main, txtbufinit, buffmgnt->generate_initialisations());
    ba::replace_first(main, txtlocks, syncmgnt->generate_declaration());
    ba::replace_first(main, txtlockinit, syncmgnt->generate_initialisation());

    ba::replace_first(main, txtbufclean, buffmgnt->generate_terminaisons());
    ba::replace_first(main, txtlockclean, syncmgnt->generate_cleanup());

    if(!ccode->has_multiple_period()) {
        ba::replace_first(main, txtperiodlock, strategy->tpl_getPeriodLock());
        ba::replace_first(main, txtperiodinit, strategy->tpl_getPeriodLockInit());
        ba::replace_first(main, txtperiodunlock, strategy->tpl_getPeriodUnLock());
        ba::replace_first(main, txtperiodclean, strategy->tpl_getPeriodLockClean());
        ba::replace_all(main, txtperiod, to_string((long)tg->global_period()));
        ba::replace_all(main, txttaskname, "");
    }
    else {
        ba::replace_first(main, txtperiodlock, "");
        ba::replace_first(main, txtperiodinit, "");
        ba::replace_first(main, txtperiodunlock, "");
        ba::replace_first(main, txtperiodclean, "");
    }

    ba::replace_first(main, txtdecl, codedecl);
    ba::replace_first(main, txtinvoke, codeinvoke);
    ba::replace_first(main, txtdelete, codedelete);
    ba::replace_first(main, txtimplem, tasks+runnabletasks);
    ba::replace_all(main, txtnbthreads, to_string(nbthreads));
    ba::replace_all(main, txtnbiteration, ccode->maxiter());
    ba::replace_all(main, txtnbsrcs, to_string(nbsrcs));
    ba::replace_first(main, txtinit, ccode->initializer());
    ba::replace_first(main, txtcleaner, ccode->cleaner());

    std::string iteration;

    if (ccode->maxiter().empty()) {
        iteration = "size_t __i__ = 0 ; ; ++__i__";
    } else {
        iteration = "size_t __i__ = 0 ; __i__ < " + ccode->maxiter() + " ; ++__i__";
    }

    ba::replace_all(main, txtiteration, iteration);

    ba::replace_all(main, txtsched, strategy->os_scheduler());

    ba::replace_all(main, txtshared, to_string(strategy->is_SharedProcesses()));

    ba::replace_all(main, txtcache, "");

    ba::replace_first(main, txtmainargs, generateMainArgs());

    string chg_main_mapping = "";
    if(!ccode->main_mapping().empty()) {
        chg_main_mapping = strategy->tpl_getMainMapping();
        ComputeUnit *proc = tg->processor(ccode->main_mapping());
        if(proc == nullptr)
            throw CompilerException("c-generator", "Can't find processor with id "+ccode->main_mapping());
        ba::replace_first(chg_main_mapping, txtmapping, to_string(proc->sysID));
    }
    ba::replace_first(main, txtmapping, chg_main_mapping);

    ofstream ofs((strategy->path() / strategy->getMainFileName()).string()+"."+mk->target_code_language());
    ofs << main;
    ofs.close();
}

std::string LinuxRuntime::generateConditionalEdgeCode(Task *t) {
    string code = "";
    return code;
}

size_t LinuxRuntime::generateTasksMgnt(string &codeinvoke, string &codedelete, string &codedecl, string &codetasks) {
    size_t nb_runnable = 0;
    if(strategy->is_GroupedRunnable()) {
        if(strategy->runtime() == CodeGeneration::runtime_container_e::FORKANDPTHREAD) {
            size_t grp_id = 0;
            for(vector<Task*> dag : ccode->DAGS()) {
                ++grp_id;
                nb_runnable += generateGroup("grp_"+to_string(grp_id), dag, tg->processor(ccode->main_mapping()), codeinvoke, codedelete, codedecl, codetasks);
            }
        }
        else {
            map<ComputeUnit*, vector<Task*>> mapping_grp;
            for(Task *t : tg->tasks_it()) {
                SchedCore *core = sched->get_mapping(*(sched->schedjobs(t).begin()));
                if(core != nullptr)
                    mapping_grp[core->core()].push_back(t);
            }

            if(!mapping_grp.size()) {
                Utils::WARN("No schedule done, but grouped processses/threads requested, might not be what you expected");
                vector<Task*> tasks;
                for(vector<Task*> dag : ccode->DAGS())
                    tasks.insert(tasks.end(), dag.begin(), dag.end()); // no tg->tasks() as we want the order from the DAGs
                mapping_grp[tg->processor(ccode->main_mapping())] = tasks;
            }

            for(pair<ComputeUnit*, vector<Task*>> el : mapping_grp)
                nb_runnable += generateGroup("grp_"+el.first->id, el.second, el.first, codeinvoke, codedelete, codedecl, codetasks);
        }
    }
    else {
        vector<Task*> tasks;
        for(vector<Task*> dag : ccode->DAGS())
            tasks.insert(tasks.end(), dag.begin(), dag.end()); // no tg->tasks() as we want the order from the DAGs
        nb_runnable = generateTasksInvocation(tasks, codeinvoke, codedelete, codedecl, codetasks);
    }
    return nb_runnable;
}

size_t LinuxRuntime::generateGroup(const string &id, const vector<Task*> tasks, ComputeUnit *grp_mapping, string &codeinvoke, string &codedelete, string &codedecl, string &codetasks) {
    if(!tasks.size())
        return 0;

    std::vector<Task *> stasks = tasks;
    std::sort(
        stasks.begin(),
        stasks.end(),
        [this](Task * a, Task * b) {
            return (*(sched->schedjobs(a).begin()))->rt() < (*(sched->schedjobs(b).begin()))->rt();
        }
    );

    size_t nb_runnable = 0;

    std::string invoke_tasks = "";
    std::string del_tasks = "";
    std::string tmp_codedecl = "";
    std::string tmp_codetasks = "";

    nb_runnable = generateTasksInvocation(stasks, invoke_tasks, del_tasks, tmp_codedecl, tmp_codetasks);

    {
        std::string invoke_grp = strategy->tpl_getGroupInstantiate();
        ba::replace_all(invoke_grp, txttaskname, id);
        ba::replace_all(invoke_grp, txtversionname, "");
        ba::replace_all(invoke_grp, txtshortname, id.substr(0, 14));
        if(grp_mapping != nullptr)
            ba::replace_first(invoke_grp, txtmapping, to_string(grp_mapping->sysID));
        else
            ba::replace_first(invoke_grp, txtmapping, "-1");

        if(!strategy->default_priority().empty())
            ba::replace_all(invoke_grp, txtpriority, strategy->default_priority());
        else
            ba::replace_all(invoke_grp, txtpriority, "-1");
        codeinvoke += invoke_grp;
    }

    {
        std::string del_grp = strategy->tpl_getGroupCleanup();
        ba::replace_all(del_grp, txttaskname, id);
        ba::replace_all(del_grp, txtversionname, "");
        codedelete += del_grp;
    }

    {
        std::string decl_grp = strategy->tpl_getGroupDecl();
        ba::replace_all(decl_grp, txttaskname, id);
        ba::replace_all(decl_grp, txtversionname, "");
        codedecl += decl_grp;
        codedecl += tmp_codedecl;

        codedecl += "long __period_" + id + " = " + to_string(sched->makespan()) + ";\n";

        for (Task * t : stasks) {
            SchedJob * e = *(sched->schedjobs(t).begin());

            if (sched->get_mapping(e)->core() != grp_mapping) {
                continue;
            }

            codedecl += "long __task_offset_" + id + "_" + t->id() + " = " + to_string(e->rt()) + ";\n";
        }
    }

    {
        std::string code_grp = strategy->tpl_getRunnableGroup();
        ba::replace_all(code_grp, txttaskname, id);

        // ba::replace_first(code_grp, txtperiodwait, strategy->tpl_getPeriodWait());
        ba::replace_first(code_grp, txtperiodwait, "");

        ba::replace_all(code_grp, txtversionname, "");
        ba::replace_first(code_grp, txtseqcall, invoke_tasks);
        ba::replace_first(code_grp, txtdelete, del_tasks);
        ba::replace_all(code_grp, txttaskname, id);

        if (ccode->has_multiple_period()) {
            ba::replace_first(code_grp, txtperiodlock, strategy->tpl_getPeriodLock());
            ba::replace_first(code_grp, txtperiodunlock, strategy->tpl_getPeriodUnLock());
            ba::replace_first(code_grp, txtperiodinit, strategy->tpl_getPeriodLockInit());
            ba::replace_first(code_grp, txtperiodclean, strategy->tpl_getPeriodLockClean());
        } else {
            ba::replace_first(code_grp, txtperiodlock, "");
            ba::replace_first(code_grp, txtperiodunlock, "");
            ba::replace_first(code_grp, txtperiodinit, "");
            ba::replace_first(code_grp, txtperiodclean, "");
        }

        ba::replace_all(code_grp, txtgroupname, id);

        codetasks += tmp_codetasks;
        codetasks += code_grp;

    }

    return nb_runnable;
}

size_t LinuxRuntime::generateTasksInvocation(const vector<Task*> tasks, string &codeinvoke, string &codedelete, string &codedecl, string &codetasks) {
    size_t nb_runnable = 0;
    static size_t cnt = 0;
    const string invoke = strategy->tpl_getTaskInstantiate();
    const string del = strategy->tpl_getTaskCleanup();
    const string decl = strategy->tpl_getTaskDecl();
    const string sleep = strategy->tpl_getGroupTaskWait();
    for(Task *t : tasks) {
        SchedJob *st = *(sched->schedjobs(t).begin());
        SchedCore *c = sched->get_mapping(st);
        ComputeUnit *mapping = nullptr;
        if(c != nullptr)
            mapping = c->core();

        for(Version *v : t->versions()) {
            codeinvoke += ba::replace_all_copy(invoke, txttaskwait, sleep);
            ba::replace_all(codeinvoke, txttaskname, t->id());

            ba::replace_all(codeinvoke, txtversionname, v->id());
            string id = to_string(cnt++);
            ba::replace_all(codeinvoke, txtshortname, t->id().substr(0, 14-id.size())+id);
            
            if(st != nullptr && st->priority() != 99)
                    ba::replace_all(codeinvoke, txtpriority, to_string(st->priority()));
            else if(!strategy->default_priority().empty())
                ba::replace_all(codeinvoke, txtpriority, strategy->default_priority());
            else
                ba::replace_all(codeinvoke, txtpriority, "-1");

            if(mapping != nullptr)
                ba::replace_first(codeinvoke, txtmapping, to_string(mapping->sysID));
            else
                ba::replace_first(codeinvoke, txtmapping, "-1");

            codedelete += ba::replace_all_copy(del, txttaskname, t->id());
            ba::replace_all(codedelete, txtversionname, v->id());

            codedecl += ba::replace_all_copy(decl, txttaskname, t->id());
            ba::replace_all(codedecl, txtversionname, v->id());

            string tmp = generateRunnable(t, v);
            if(!tmp.empty())
                nb_runnable++;
            codetasks += tmp;
        }
    }
    return nb_runnable;
}

string LinuxRuntime::generateRunnable(Task *t, Version *v) {
    string tpl = strategy->tpl_getRunnableTask();
    ba::replace_first(tpl, txtseqcall, strategy->tpl_getTaskCall());
    if(ccode->has_multiple_period() && t->predecessor_tasks().size() == 0) {
        ba::replace_first(tpl, txtperiodlock, strategy->tpl_getPeriodLock());
        ba::replace_first(tpl, txtperiodinit, strategy->tpl_getPeriodLockInit());
        ba::replace_first(tpl, txtperiodunlock, strategy->tpl_getPeriodUnLock());
        ba::replace_first(tpl, txtperiodclean, strategy->tpl_getPeriodLockClean());
        ba::replace_all(tpl, txtperiod, to_string(t->T()));
        ba::replace_all(tpl, txtnbsrcs, "1");

        ba::replace_first(tpl, txtperiodwait, strategy->tpl_getPeriodWait());
    }
    else {
        ba::replace_first(tpl, txtperiodlock, "");
        ba::replace_first(tpl, txtperiodinit, "");
        ba::replace_first(tpl, txtperiodunlock, "");
        ba::replace_first(tpl, txtperiodclean, "");

        if(!ccode->has_multiple_period() && t->predecessor_tasks().size() == 0) {
            string itertpl = strategy->tpl_getPeriodWait();
            ba::replace_all(itertpl, txttaskname, "");
            ba::replace_first(tpl, txtperiodwait, itertpl);
        }
        else
            ba::replace_first(tpl, txtperiodwait, "");
    }
    ba::replace_all(tpl, txttaskname, t->id());
    ba::replace_all(tpl, txtversionname, v->id());

    return tpl;
}

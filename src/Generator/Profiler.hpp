/*
 * Copyright (C) 2019 Benjamin Rouxel <benjamin.rouxel@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PROFILER_H
#define PROFILER_H

#include <string>
#include <map>
#include <vector>
#include <iostream>

#include "Generator.hpp"
#include "Runtime/RuntimeStrategyInterface.hpp"
#include "LinuxRuntime.hpp"
#include "LinuxRuntimeNoBuffMgnt.hpp"
#include "boost/filesystem.hpp"

namespace ba = boost::algorithm;

/**
 * @brief Generate the C-code for the profiler
 * 
 * The template parameter configure which code generator is the parent. For now,
 * the supported parent is the LinuxRuntime or the LinuxRuntimeNoBugMgnt
 * 
 * \see Profiler::help
 */
template<class Parent>
class ProfilerGenerator : public Parent {
    std::string startprof_function = ""; //!< Function to use to start the profiling
    std::string stopprof_function = ""; //!< Function to use to stop the profiling
    std::string randsampprof_function = ""; //!< Function to use to perform random sampling
    std::string set_args_function = ""; //!< Function to parse arguments given to generated main
    
    /**
     * Structure to store information given in the configuration file regarding
     * arguments given to the generated main
     */
    struct main_arg {
        std::string id; //!< id of the argument generated (variable name)
        std::string type; //!< type of the argument
        std::string index; //!< index in the array given to main
        std::string conversion; //!< as they come as char*, name of a function to convert it to type
    };
    std::vector<main_arg> main_args; //!< List of all arguments given to the generated main function
    
    /*!
     * Internal representation of a component to profile, and given by the configuration file
     */
    struct component {
        std::string id; //!< Id of the component
        Version *version; //!< Version to use for the profiling
        std::string mapped; //!< On which core it should be mapped
        bool profile; //!< True if this component should be profiled
    };
    std::vector<component> comp_prof; //!< List of component to profile according to the documentation
    /*! List of version that can be freed by the destructor
     * 
     * These specific versions are created to replace the ones present in the coord file. 
     * For example, it allows to replace an component/version that requires external device access, e.g. eth, serial ...
     */
    std::vector<Version*> pool_to_free;

public:
    /**
     * Constructor
     */
    explicit ProfilerGenerator();
    //! \copydoc CompilerPass::get_uniqid_rtti
    const std::string get_uniqid_rtti() const override;
    ~ProfilerGenerator(); //!< Destructor, free version in pool_to_free

    virtual void forward_params(const std::map<std::string, std::string> &args) override;
    virtual std::string help() override;
    //! \copydoc Generator::setAuxilliaryParams
    void setAuxilliaryParams(std::string key, const std::map<std::string, std::string> &args) override;
    
protected:
    //! \copydoc Generator::generate(const boost::filesystem::path &f)
    void generate(const boost::filesystem::path &f) override;
    
    /**
     * Generate the part of the code that catches arguments given to the main
     * 
     * \see help
     * @return 
     */
    std::string generateMainArgs() override;
    
    /**
     * Generate the code of tasks
     * @return 
     */
    std::string generateTasksCode() override;
    
    /**
     * Generate code for the task management
     * 
     * In the case of the profiler, it's about a sequential call of all tasks 
     * surrounded by profiling start/stop and maybe pre-defined mapping
     * 
     * @param codeinvoke to store the code to invoke tasks
     * @param codedelete unused here
     * @param codedecl unused here
     * @param runnabletasks unused here
     * @return unused
     */
    size_t generateTasksMgnt(std::string &codeinvoke, std::string &codedelete, std::string &codedecl, std::string &runnabletasks) override;
    
    /**
     * Generate the code to call a task
     * 
     * Adds the profiling start/stop and maybe pre-defined mapping
     * 
     * @param t 
     * @param v
     * @param c
     * @param seqcall to store the code
     */
    void generateProfilingInvocation(Task *t, Version *v, const component &c, std::string &seqcall);
};

using ProfilerGen = ProfilerGenerator<LinuxRuntime>;
using ProfilerGenNoBuf = ProfilerGenerator<LinuxRuntimeNoBuffMgnt>;

REGISTER_GENERATOR(ProfilerGen, "c_profiler")
REGISTER_GENERATOR(ProfilerGenNoBuf, "c_profiler_nobuf")

#endif /* PROFILER_H */


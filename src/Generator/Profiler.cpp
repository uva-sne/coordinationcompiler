/*
 * Copyright (C) 2019 Benjamin Rouxel <benjamin.rouxel@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "Profiler.hpp"

using namespace std;
namespace ba = boost::algorithm;

template<typename Parent>
ProfilerGenerator<Parent>::ProfilerGenerator() : Parent() {}

template<typename Parent>
const string ProfilerGenerator<Parent>::get_uniqid_rtti() const { return "generator-profiler"; }

template<typename Parent>
ProfilerGenerator<Parent>::~ProfilerGenerator() {
    for(Version *v : pool_to_free)
        delete v;
    pool_to_free.clear();
}

template<typename Parent>
IMPLEMENT_HELP(ProfilerGenerator<Parent>,
    Generate code to run the app sequentially with some introspection code, 
    with or without buffer management. This will allow for example to 
    gather execution time, energy consumption of each individual task/component.\n
    HTMLINDENT- start-function: function to call before the beginning of a task/component\n
    HTMLINDENT- stop-function: function to call after the execution of a task/component\n
    HTMLINDENT- random-sampling-function: function to randomly call between task/component execution\n
    HTMLINDENT- arg-function: function to call to process arguments given to the main\n
    @see CCode::help \n
    @see RuntimeStrategyInterface::help \n
    @see BufferMgnt::help \n
    @see BuildFile::help \n
)

template<typename Parent>
void ProfilerGenerator<Parent>::forward_params(const map<string, string> &args) {
    Parent::forward_params(args);
    this->strategy->runtime(CodeGeneration::runtime_container_e::SEQUENTIAL);
    
    this->ccode->addInclude("<ctype.h>");
    this->ccode->addInclude("<unistd.h>");
    this->ccode->addInclude("<assert.h>");
    
    if(args.count("start-function"))
        startprof_function = args.at("start-function");
    if(args.count("stop-function"))
        stopprof_function = args.at("stop-function");
    if(args.count("random-sampling-function"))
        randsampprof_function = args.at("random-sampling-function");
    if(args.count("arg-function"))
        set_args_function = args.at("arg-function");
}

template<typename Parent>
void ProfilerGenerator<Parent>::setAuxilliaryParams(string key, const map<string, string> &args) {
    if(key == "mainargs") {
        if(!args.count("id") || !args.count("index") || !args.count("type") || !args.count("conversion"))
            throw CompilerException("c-code-profiler", "Missing a required parameter to set a main arg: id/index/type/conversion");
        if(stoul(args.at("index")) <= 0)
            throw CompilerException("c-code-profiler", "Index of main arg starts at 1");
        main_arg a = {
            .id = args.at("id"),
            .type = args.at("type"),
            .index = args.at("index"),
            .conversion = args.at("conversion")
        };
        main_args.push_back(a);
    }
    else if(key == "component") {
        if(!args.count("id") || !args.count("profile"))
            throw CompilerException("c-code-profiler", "Missing a required parameter to set a component: id/profile");
        component c = {
            .id = args.at("id"),
            .version = nullptr,
            .mapped = (args.count("mapped")) ? args.at("mapped") : "",
            .profile = !(args.at("profile") == "off" || args.at("profile") == "no")
        };
        comp_prof.push_back(c);
    }
    else if(key == "version") {
        if(comp_prof.empty())
            throw CompilerException("c-code-profiler", "No component added for the profiler, tag version might be missplaced");
        if(!args.count("id") || !args.count("callable"))
            throw CompilerException("c-code-profiler", "Missing a required parameter to set a component version: id/callable");
        component &last = comp_prof.back();
        Version *v = new Version(nullptr, args.at("id"));
        pool_to_free.push_back(v);
        v->cname(args.at("callable"));
        last.version = v;
    }
}

template<typename Parent>
void ProfilerGenerator<Parent>::generate(const boost::filesystem::path& fn) {
    if(!any_of(main_args.begin(), main_args.end(), [](const main_arg &a) { return a.id == "maxiter"; }))
        throw CompilerException("c-code-profiler", "You need to at least specify the main arg \"maxiter\"");
    
    for(component &c : comp_prof) {
        Task *t = this->tg->task(c.id);
        if(t == nullptr)
            throw CompilerException("c-code-profiler", "Unknow task "+c.id);
        if(c.version != nullptr)
            c.version->task(t);
    }
    
    // Add all task in comp_prof, if it was empty before then profile every task, if not add them and set them to not profiling them
    bool was_empty = comp_prof.empty();
    for(Task *t : this->tg->tasks_it()) {
        for(Version *v : t->versions()) {
            if(!was_empty) {
                auto it = find_if(comp_prof.begin(), comp_prof.end(), [t, v](const component &c) { return c.id == t->id() && (c.version == nullptr || v->id() == c.version->id()); });
                if(it != comp_prof.end()) continue;
            }
            component c = {
                .id = t->id(),
                .version = v,
                .mapped = "",
                .profile = was_empty
            };
            comp_prof.push_back(c);
        }
    }
    
    this->ccode->maxiter("maxiter");
    
    Parent::generate(fn);
}

template<typename Parent>
std::string ProfilerGenerator<Parent>::generateMainArgs() {
    string code = "assert(argc > "+to_string(main_args.size())+");\n";
    string call_set_arg = set_args_function+"(";
    for(main_arg &a : main_args) {
        code += a.type+" "+a.id+" = "+a.conversion+"(argv["+a.index+"]);\n";
        call_set_arg += a.id+",";
    }
    code += "cpu_set_t cpuset_prof;\n";
    code += "pthread_t self_prof = pthread_self();\n";
    code += "CPU_ZERO(&cpuset_prof);\n";
    code += "pthread_getaffinity_np(self_prof, sizeof(cpu_set_t), &cpuset_prof);\n";
    code += "int main_mapping = -1;\n";
    code += "for(size_t __j__= 0; __j__ < CPU_SETSIZE; __j__++) \n";
    code += "    if(CPU_ISSET(__j__, &cpuset_prof)) main_mapping = __j__;\n";
    return code;
}

template<typename Parent>
string ProfilerGenerator<Parent>::generateTasksCode() {
    string tasks = "";
    for(Task *t : this->tg->tasks_it()) {
        auto it = find_if(comp_prof.begin(), comp_prof.end(), [t](const component &c) { return c.id == t->id(); });
        if(it->version != nullptr) 
            tasks += this->generateTask(t, it->version);
        else {
            for(Version *v : t->versions()) 
                tasks += this->generateTask(t, v);
        }
    }
    return tasks;
}

template<typename Parent>
size_t ProfilerGenerator<Parent>::generateTasksMgnt(string &codeinvoke, string &codedelete, string &codedecl, string &runnabletasks) {
    codeinvoke += "powprof_newiter(__i__, maxiter);\n\n";
    for(vector<Task*> dag : this->ccode->DAGS()) {
        for(Task *t : dag) {
            auto it = find_if(comp_prof.begin(), comp_prof.end(), [t](const component &c) { return c.id == t->id(); });
            if(it->version != nullptr)
                generateProfilingInvocation(t, it->version, *it, codeinvoke);
            else {
                assert(t->versions().size() == 1 && "yet multi-version are not supported, or you have to specify one");
                for(Version *v : t->versions())
                    generateProfilingInvocation(t, v, *it, codeinvoke);
            }
        }
    }
    return 0;
}

template<typename Parent>
void ProfilerGenerator<Parent>::generateProfilingInvocation(Task *t, Version *v, const component &c, string &seqcall) {
    string procid;
    if(c.profile) {
        seqcall += "\n";
        if(!c.mapped.empty()) {
            ComputeUnit *proc = this->tg->processor(c.mapped);
            if(proc == nullptr) 
                throw CompilerException("c-code-profiler", "Unknown core id: "+c.mapped);
            seqcall += "CPU_ZERO(&cpuset_prof);\nCPU_SET("+to_string(proc->sysID)+", &cpuset_prof);\n";
            seqcall += "status = pthread_setaffinity_np(self_prof, sizeof(cpu_set_t), &cpuset_prof);\n";
            seqcall += "tryExit(status, \"main - pthread_setaffinity_np(&self, "+to_string(proc->sysID)+")\");\n";
            procid = to_string(proc->sysID);
        }
        else
            procid = "main_mapping";

        seqcall += startprof_function+"(\""+t->id()+"\", \""+v->id()+"\", "+procid+");\n";
    }

    seqcall += "__coord_"+t->id()+"_"+v->id()+"();\n";

    if(c.profile) {
        seqcall += stopprof_function+"(\""+t->id()+"\", \""+v->id()+"\", "+procid+");\n";
        if(!c.mapped.empty()) {
            seqcall += "CPU_ZERO(&cpuset_prof);\nCPU_SET(main_mapping, &cpuset_prof);\n";
            seqcall += "status = pthread_setaffinity_np(self_prof, sizeof(cpu_set_t), &cpuset_prof);\n";
            seqcall += "tryExit(status, \"main - pthread_setaffinity_np(&self, main_mapping)\");\n";
        }
        seqcall += "\n";
    }

    if(!randsampprof_function.empty() && Utils::flipcoin()) {
        seqcall += startprof_function+"(\"random\", \"sampling\", main_mapping);\n";
        seqcall += randsampprof_function+"();\n";
        seqcall += stopprof_function+"(\"random\", \"sampling\", main_mapping);\n\n";
    }
}

template class ProfilerGenerator<LinuxRuntime>;
template class ProfilerGenerator<LinuxRuntimeNoBuffMgnt>;
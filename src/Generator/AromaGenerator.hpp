/*
 * Copyright (C) 2021 Lukas Miedema <l.miedema@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CECILE_AROMAGENERATOR_HPP
#define CECILE_AROMAGENERATOR_HPP

#include "Generator/CodeGen/CHelpers/AromaDotWriter.hpp"
#include "Generator.hpp"

/**
 * Format the input in the ADMORPH/Aroma Exchange Format (AXF)
 * This exports the following items to the output folder
 *  - The task DAG (with certain restrictions, see below) as a DOT file. The file is called %APP_NAME%.axf.dot
 *  - For each task, generate RAPID-compatible glue code (C/C++)
 *
 *  Limitations:
 *  - The Aroma RTE at this time has certain limitations on the shape of the DAG
 *    - Each task must provide at least one output token (synthetic void-token is generated, which is not ignored)
 *    - Each task may provide at most one output token
 *    These two effectively reduce the format to input* -> output
 */
class AromaGenerator : public Generator {
public:
    //! Constructor
    explicit AromaGenerator() = default;
    //! Destructor
    virtual ~AromaGenerator() = default;

    const std::string get_uniqid_rtti() const override { return "generator-axf"; }
    std::string help() override;
    void forward_params(const std::map<std::string, std::string> &args) override;
    void generate(const boost::filesystem::path& fn) override;
protected:
    //! Includes for generated wrapper code (space separated strings)
    std::vector<std::string> c_includes;

    //! Includes for generated wrapper code (space separated strings)
    std::vector<std::string> cpp_includes;

    //! Initialization function
    std::string initialization_function;

    //! Destroy function
    std::string destroy_function;

    //! Communication factor
    std::string communication_factor;
};

REGISTER_GENERATOR(AromaGenerator, "axf_generator")

#endif //CECILE_AROMAGENERATOR_HPP

/*
 * Copyright (C) 2020 Benjamin Rouxel <benjamin.rouxel@uva.nl>
 *                     University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "RTEMS.hpp"

using namespace std;
namespace ba = boost::algorithm;

RTEMSgenerator::RTEMSgenerator() : Generator() {
    buffmgnt = new BufferMgnt;
    make = new Makefile;
    ccode = new CCode;
    strategy = new RTEMSRuntime;
}

IMPLEMENT_HELP_WITH_CODE(RTEMSgenerator, 
    {msg += ccode->help()
           +strategy->help()
           +buffmgnt->help()
           +make->help();
    },
    Generate code to run with the RTEMS www.rtems.org .\n
    \n
    HTMLINDENT genpath, default COORDFILEPATH/codegen/APPNAME_rtems/: path to place the generated files.\n
    HTMLINDENT rtems_makefile_path: path to the Makefile.inc that is included in any
    HTMLINDENT linker-add-library: space separated list of additional library to set in LINK_LIBS in the makefile\n
    RTEMS project. Usually something like: rtems_prefix/arch-rtemsversion/bsp.\n
)

void RTEMSgenerator::forward_params(const map<string, string> &args) {
    if(args.count("rtems_makefile_path"))
        rtmkp = args.at("rtems_makefile_path");
    else {
        Utils::WARN("You didn't specify the `rtems_makefile_path` parameter. "
        "In order for the project to compile, it then needs to be set as an "
        "environment variable: export RTEMS_MAKEFILE_PATH=...");
    }
    if(args.count("genpath")) 
        genpath = args.at("genpath");
    if(args.count("linker-add-library"))
        additional_lib = args.at("linker-add-library");
    
    buffmgnt->setParams(args);
    make->setParams(args);
    ccode->setParams(args);
}

void RTEMSgenerator::generate(const boost::filesystem::path& fn) {
    if(sched->status() == ScheduleProperties::sched_statue_e::SCHED_UNSCHEDULABLE)
        throw CompilerException("code generation", "Can't generate code as the application is unschedulable");
    
    buffmgnt->init(tg, conf, sched, strategy);
    make->init(tg, conf, sched, strategy);
    ccode->init(tg, conf, sched, strategy);

    strategy->generatePath(fn, ccode->codegendir());

    generateBufferMgnt();
    generateInit();
    generateConfig();
    
    map<string, string> extra;
    extra["%RTEMS_MK_PATH%"] = rtmkp;
    extra["%LINK_LIB%"] = additional_lib;
    make->generate(ccode->includes(), extra);
}

void RTEMSgenerator::generateBufferMgnt() {
    string deffile = buffmgnt->generate_definitions();
    ba::replace_first(deffile, txtinc, ccode->includes_to_string());
    ofstream ofs((strategy->path() / buffmgnt->getFileName(make->target_code_language())).string());
    ofs << deffile;
    ofs.close();
    ccode->addInclude(buffmgnt->getFileName(make->target_code_language()));
}

void RTEMSgenerator::generateInit() {
    
    string main = strategy->tpl_getMain();
    
    ba::replace_first(main, txtappname, strategy->app_name());
    
    ba::replace_all(main, "%NBTASKS%", to_string(tg->tasks().size()));
    ba::replace_first(main, "%TASKS%", generateTasks());
    ba::replace_first(main, "%SETUPTASKS%", generateSetupTasks());
    
    ba::replace_first(main, txtinc, ccode->includes_to_string());
    
    ba::replace_first(main, txtinit, ccode->initializer());
    ba::replace_first(main, txtcleaner, ccode->cleaner());
    
    ba::replace_first(main, txtbuffers, buffmgnt->generate_declarations());
    ba::replace_first(main, txtbufinit, buffmgnt->generate_initialisations());
    ba::replace_first(main, txtbufclean, buffmgnt->generate_terminaisons());
    
    ofstream ofs((strategy->path() / strategy->getMainFileName()).string()+"."+make->target_code_language());
    ofs << main;
    ofs.close();
}

void RTEMSgenerator::generateConfig() {
    string config = strategy->tpl_getConfig();
    ba::replace_all(config, "%NBTASKS%", to_string(tg->tasks().size()));
    ba::replace_all(config, "%TASKS%", generateDeclTasks());
    
     
    string sconfigs = "";
    ba::replace_first(config, "%CONFIGS%", sconfigs);
    
    ofstream ofs((strategy->path() / "config.h").string());
    ofs << config;
    ofs.close();
}

string RTEMSgenerator::generateTasks() {
    string code_tasks = "";
    string task_tpl = strategy->tpl_getTask();
    
    for(SchedJob *st : sched->schedjobs()) {
        Task *t = const_cast<Task *>(st->task());
        
        Version *v = (!st->selected_version().empty()) ? t->version(st->selected_version()) : t->versions()[0];
        
        string inputs, outputs, args, decl;
        buffmgnt->generate_task_connection(t, &args, &decl, &inputs, &outputs);
        
        string fncallable;
        if(v->cname().empty())
            throw CompilerException("c-code", "Don't know how to call task "+t->id());
        if(!v->csignature().empty())
            fncallable = v->cname()+"("+v->csignature()+", "+args.substr(0, args.size()-2)+");"; //we will see what I have for the signature inside the nfp, but for FMRadio I need it due to the replication of components
        else
            fncallable = v->cname()+"("+args.substr(0, args.size()-2)+");";
        
        code_tasks += ba::replace_all_copy(task_tpl, txttaskname, t->id());
        ba::replace_all(code_tasks, txtversionname, v->id());
        ba::replace_first(code_tasks, txtcallin, decl+inputs);
        ba::replace_first(code_tasks, txtcallable, fncallable);
        ba::replace_first(code_tasks, txtcallout, outputs);
    }
    return code_tasks;
}

string RTEMSgenerator::generateSetupTasks() {
    string code_tasks = "";
    string task_tpl = strategy->tpl_getTaskCall();
    
//    cout << "--------------------> " << sched->schedtasks().size() << endl;
    
    size_t i = 0;
    for(SchedJob *st : sched->schedjobs()) {
        const Task *t = st->task();
        string name = t->id();
        if(name.length() < 4)
            name.append(4-name.length(), ' ');
        string rtems_tname = string("'")+name.at(0)+"', "+"'"+name.at(1)+"', "+"'"+name.at(2)+"', "+"'"+name.at(3)+"'";
        
        Version *v = (!st->selected_version().empty()) ? t->version(st->selected_version()) : t->versions()[0];
        
        code_tasks += ba::replace_all_copy(task_tpl, txttaskname, rtems_tname);
        ba::replace_all(code_tasks, txtversionname, v->id());
        ba::replace_all(code_tasks, "%TASKINDEX%", to_string(i++));
        ba::replace_all(code_tasks, txttaskcall, "__coord_"+t->id()+"_"+v->id());
    }
    return code_tasks;
}

string RTEMSgenerator::generateDeclTasks() {
    string code_tasks = "";
    string task_tpl = strategy->tpl_getTaskDecl();
    
    for(SchedJob *st : sched->schedjobs()) {
        const Task *t = st->task();
        
        Version *v = (!st->selected_version().empty()) ? t->version(st->selected_version()) : t->versions()[0];
        
        code_tasks += ba::replace_all_copy(task_tpl, txttaskname, t->id());
        ba::replace_all(code_tasks, txtversionname, v->id());
    }
    return code_tasks;
}
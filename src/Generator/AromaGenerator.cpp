/*
 * Copyright (C) 2021 Lukas Miedema <l.miedema@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "Generator/CodeGen/CHelpers/AromaWrapperWriter.hpp"
#include "AromaGenerator.hpp"

IMPLEMENT_HELP(AromaGenerator,
    Generate an Aroma RTE compatible representation of the task DAG and environment configuration, including C++ glue code.
)

void AromaGenerator::generate(const boost::filesystem::path &fn) {
    AromaGeneratorHelper helper = AromaGeneratorHelper(tg, conf);
    AromaDotWriter dot_gen = AromaDotWriter(&helper);
    AromaWrapperWriter wrapper_gen = AromaWrapperWriter(&helper);

    dot_gen.initialization_function(this->initialization_function);
    dot_gen.destroy_function(this->destroy_function);
    dot_gen.communication_factor(this->communication_factor);
    dot_gen.generate(fn);

    wrapper_gen.set_include_paths(this->c_includes, this->cpp_includes);
    wrapper_gen.generate(fn);
}

void AromaGenerator::forward_params(const std::map<std::string, std::string> &args) {
    if (args.count("c_includes") && !args.at("c_includes").empty()) {
        std::vector<std::string> incs;
        boost::split(incs, args.at("c_includes"), boost::is_any_of(" "));
        for (const std::string& inc : incs) {
            if (inc.empty()) continue;
            this->c_includes.push_back(inc);
        }
    }
    if (args.count("cpp_includes") && !args.at("cpp_includes").empty()) {
        std::vector<std::string> incs;
        boost::split(incs, args.at("cpp_includes"), boost::is_any_of(" "));
        for (const std::string& inc : incs) {
            if (inc.empty()) continue;
            this->cpp_includes.push_back(inc);
        }
    }
    if (args.count("initialization_function"))
        this->initialization_function = args.at("initialization_function");
    if (args.count("destroy_function"))
        this->destroy_function = args.at("destroy_function");
    if (args.count("communication_factor"))
        this->communication_factor = args.at("communication_factor");
}
/*
 * Copyright (C) 2020 brouxel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef YASMIN_H
#define YASMIN_H

#include "Generator.hpp"
#include "Runtime/RuntimeStrategyInterface.hpp"
#include "Utils/Log.hpp"
#include "Utils/Graph.hpp"
#include <boost/filesystem.hpp>
#include "Generator/CodeGen/CodeGenerationPatterns.hpp"
#include "Generator/CodeGen/CodeGenerationEnums.hpp"
#include "Generator/CodeGen/CHelpers/BufferMgnt.hpp"
#include "Utils/Log.hpp"
#include "Generator/Runtime/YasminRuntime.hpp"
#include "Generator/CodeGen/CHelpers/CMake.hpp"
#include "Generator/CodeGen/CCode.hpp"
#include "Generator/CodeGen/CHelpers/DataExchangeSynchro.hpp"
#include "Generator/CodeGen/CHelpers/YasminChannels.hpp"
#include "Helper_macros.hpp"
#include "Properties.hpp"

/**
 * Generate the glue code for the provided application to use the Coordination Library
 * 
 * This generator generates the C-Code allowing the application to run under
 * the Coordination Library. The glue code includes the creation of the main, the buffer
 * management to fulfil tasks dependencies, the synchronisation code,  the
 * task management releasing/executing/stopping ...
 * 
 * It also generates a CMakeLists.txt to build the application.
 * 
 * The generation of code is based on the concept of template. Templates express
 * what the final file should look like and include some strings that must be 
 * replaced with actual value/code. The template can differ depending on the 
 * execution mode, but the string to replace will be the same.\n
 * Using templates prevent from having huge string of code embedded in the methods.
 * 
 * \see CoordRuntimeLib::help
 */
namespace yasmin {

class Yasmin : public Generator {
    // @todo find a better solution, and beter values
    static constexpr long thread_task_wrapper_overhead = 1000000; //!< Overhead to add to the WCET of tasks due to the thread wrapping function
    static constexpr long start_iter_overhead = 100000+thread_task_wrapper_overhead; //!< Overhead to add to start an iteration of the graph

protected:
    YasminRuntime *strategy = nullptr; //!< Strategy that returns template string
    YasminChannels *buffmgnt; //!< Helper to generate the buffer management code
    CMake *cmake; //!< Helper to generate the CMakeLists.txt
    CCode *ccode; //!< Helper to generate the whole c-code

    std::string execuntilreturn = ""; //!< Terminaison function, the application will run until this function returns
    std::string yasmin_path = ""; //!< Path on the system to find the coordination library
    bool trace = false; //!< True if the coord runtime should generate a trace
    bool preemption = false; //!< True if preemption are enabled
    std::string pre_hook= ""; //!< Pre-hook function that is called before each task
    std::string post_hook= ""; //!< Post-hook function that is called after each task

    bool mapping_partitioned = false; //!< True if a mapping has been completed
    bool schedule_offline = false; //!< True if the application has been statically scheduled

    std::map<ComputeUnit*, std::string> proc_internal_id; //!< Map of processors and internal virtual CPU id (used by the coordination library)

    std::map<std::string,std::string> coordlib_configs; //!< Map to generate the configuration file of the library

    std::string prehook = "";
    std::string posthook = "";

public:
    //! Constructor
    explicit Yasmin();
    //! Destructor
    virtual ~Yasmin() {}
    const std::string get_uniqid_rtti() const override { return "generator-coord_runtime"; }

    virtual void generate(const boost::filesystem::path& fn) override;
    virtual void forward_params(const std::map<std::string, std::string> &args) override;
    virtual std::string help() override;
protected:
    /**
     * Generate the code of the main function
     */
    virtual void generateMainCode();
    /**
     * Generate the mandatory config.h that configures the Runtime Library
     */
    virtual void generateConfig();

    /**
     * Generate the separate file to include for the buffer management
     */
    virtual void generateBufferMgnt();

    /**
     * Generate the code to manage an off-line schedule
     * @param setup
     * @param decl_tasks
     * @param code_tasks
     */
    virtual void generateOffLineSchedule(std::string &setup, std::string &decl_tasks, std::string &code_tasks);
    /**
     * Generate the code to manage a partitioned on-line schedule
     * @param setup
     * @param decl_tasks
     * @param code_tasks
     */
    virtual void generatePartitionedSchedule(std::string &setup, std::string &decl_tasks, std::string &code_tasks);
    /**
     * Generate the code to manage a global on-line schedule
     * @param setup
     * @param decl_tasks
     * @param code_tasks
     */
    virtual void generateGlobalOnLineSchedule(std::string &setup, std::string &decl_tasks, std::string &code_tasks);
    /**
     * Create the mapping of CPU id and internal virtual CPU
     */
    virtual void fill_internal_procid();

    /**
     * Generate the calling of the task
     * @param t
     * @param v
     * @param args
     * @param structparams
     * @param initparams
     * @param sendinitparams
     * @return
     */
    std::string gen_call_signature(Task *t, Version *v, const std::string &args, std::string *structparams, std::string *initparams, std::string *sendinitparams);
};

REGISTER_GENERATOR(Yasmin, "yasmin")
}
#endif /* YASMIN_H */


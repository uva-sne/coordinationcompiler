/*
 * Copyright (C) 2020 Wouter Loeve <wouterloeve0@gmail.com>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef FTSIMULATORCODEGEN_H
#define FTSIMULATORCODEGEN_H

#include "Generator.hpp"
#include "Utils/Log.hpp"
#include "Utils.hpp"
#include "SystemModel.hpp"

#include <memory>
#include <string>
#include <stdexcept>

/**
 * Generate code for the Fault-Tolerance simulator
 */
class FTSimulatorGen : public Generator {
public:
	std::string help() override;
        
        virtual void forward_params(const std::map<std::string, std::string> &args) override {}

	const std::string get_uniqid_rtti() const override;
	void generate() override;
	void generate(const boost::filesystem::path &f) override;
	
private:
	/*!
	 * Generates the structs used for in and output.
         * @param out
	 */
	void generateStructs(std::ofstream *out);

	/*!
	 * Generates the function that stores output in the appropriate buffers for each component.
         *  @param out
	 */
	void generateStoreOutput(std::ofstream *out);

        /**
         * ??
         * @param out
         */
	void generateTakeOutput(std::ofstream *out);

        /**
         * ??
         * @param out
         */
	void generateCopyCompData(std::ofstream *out);

        /**
         * ??
         * @param out
         * @param i
         */
	void generateTaskGraphEdge(std::ofstream *out, Task *i);

        /**
         * ??
         * @param out
         */
	void generateSetError(std::ofstream *out);

        /**
         * ??
         * @param out
         */
	void generateProfiles(std::ofstream *out);

        /**
         * ??
         * @param out
         */
	void generateAllocOutput(std::ofstream *out);
	/*!
	 * Generates name of the associated struct.
	 * Type is a string, either 'in' or 'out'.
         * @param i
         * @param type
         * @return
	 */
	std::string getStructName(Task *i, std::string type);

        /**
         * ??
         * @param i
         * @return 
         */
	std::string getGraphStructName(Task *i);

        /**
         * ??
         * @param e
         * @return 
         */
	std::string connectorTypeToString(Connector *e);

        /**
         * ??
         * @param tokenType
         * @return 
         */
	std::string tokenTypeToId(std::string tokenType);

        /**
         * Generates names of the connector structure.
         * 
         * @param c
         * @return 
         */
	std::string generateConnectorStruct(Connector *c);

        /**
         * ??
         * @param out
         */
	void generateTaskGraph(std::ofstream *out);

        /**
         * ??
         * @param out
         */
	void generateCompFunc(std::ofstream *out);

        /**
         * ??
         * @param i
         * @return 
         */
	std::string getImplementationName(Task *i);

	/*!
	 * Adds double quotes to a string.
         * @param s
         * @return
	 */
	std::string addQuotes(std::string s);

        /**
         * ??
         * @param c
         * @return 
         */
	bool isPtrType(Connector *c);

        /**
         * ??
         * @param outports
         * @return 
         */
	std::vector<Connector*> removeDuplicateConns(std::vector<Connector*> outports);

        /**
         * ??
         * @param token_type
         * @return 
         */
	std::string getCopyName(std::string token_type);

        /**
         * ??
         * @param m
         * @return 
         */
	std::map<std::string, int> getNumOutports(std::vector<Connector *> m);
};

REGISTER_GENERATOR(FTSimulatorGen, "FTCodegen")
        
#endif /* FTSIMULATORCODEGEN_H */


/* 
 * Copyright (C) 2017 Benjamin Rouxel <benjamin.rouxel@inria.fr> 
 *                    Irisa/INRIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LINUXRUNTIME_H
#define LINUXRUNTIME_H

#include "Generator.hpp"
#include "Runtime/RuntimeStrategyInterface.hpp"
#include "Utils/Log.hpp"
#include "Utils/Graph.hpp"
#include <boost/filesystem.hpp>
#include "CodeGen/CodeGenerationPatterns.hpp"
#include "CodeGen/CodeGenerationEnums.hpp"
#include "CodeGen/CHelpers/DataExchangeSynchro.hpp"
#include "CodeGen/CHelpers/BufferMgnt.hpp"
#include "CodeGen/CHelpers/Makefile.hpp"
#include "CodeGen/CCode.hpp"

/**
 * Generate the glue code for the provided application targeting Linux-based OS
 * 
 * This generator generates the C-Code allowing the application to run under
 * a Linux-based OS. The glue code includes the creation of the main, the buffer
 * management to fulfil tasks dependencies, the synchronisation code,  the
 * task management releasing/executing/stopping ...
 * 
 * It also generates a Makefile to build the application.
 * 
 * The generation of code is based on the concept of template. Templates express
 * what the final file should look like and include some strings that must be 
 * replaced with actual value/code. The template can differ depending on the 
 * execution mode, but the string to replace will be the same.\n
 * Using templates prevent from having huge string of code embedded in the methods.
 * 
 * \see LinuxRuntime::help
 */
class LinuxRuntime : public Generator {
protected:
    /*!
     * In Linux, there is possibility to run the code in user-space, kernel-space,
     * as the init process. This strategy will select the mode. As of writing,
     * only the user-space mode is fully supported
     * This strategy will return the template to use for the generation
     */
    RuntimeStrategyInterface *strategy = nullptr;
    BufferMgnt *buffmgnt; //!< Helper to generate the buffer management code
    Makefile *mk; //!< Helper to generate the makefile
    DataExchangeSynchro *syncmgnt; //!< Helper to generate the synchronisation between tasks
    CCode *ccode; //!< Helper to generate the whole c-code
    
public:
    //! Constructor
    explicit LinuxRuntime();
    const std::string get_uniqid_rtti() const override { return "generator-linux"; }
    
    virtual void forward_params(const std::map<std::string, std::string> &args) override;
    virtual std::string help() override;
    
    virtual void generate(const boost::filesystem::path& fn) override;

protected:    
    /**
     * Generate the code of the main function
     */
    virtual void generateMainCode();
    
    /**
     * Generate and return the wrapping code for all tasks
     * 
     * @return 
     */
    virtual std::string generateTasksCode();
    
    /**
     * Generate the wrapping code for a task 
     * 
     * The wrapping code will fetch the tokens, execute the user function, then
     * push tokens into the FIFO before releasing successors
     * 
     * @param t
     * @param v
     * @return 
     */
    virtual std::string generateTask(Task *t, Version *v);
    
    /**
     * Generate the code for a runnable, an execution container ?
     * 
     * @param t
     * @param v
     * @return 
     */
    virtual std::string generateRunnable(Task *t, Version *v);
    
    /**
     * Generate the separate file to include for the buffer management
     */
    void generateBufferMgnt();
    
    /**
     * Unused here, check Profiler
     * @return 
     */
    virtual std::string generateMainArgs() { return ""; }

    /**
     * Initial work for conditional edges
     * @todo
     * @param t
     * @return 
     */
    std::string generateConditionalEdgeCode(Task *t);
    
    /**
     * Generate the code to manage a task
     * 
     * It includes the declaration, invocation, cleaning, and body of wrapping task
     * 
     * @param codeinvoke invocation code
     * @param codedelete cleaning up code
     * @param codedecl declaration code
     * @param codetasks wrapping code
     * @return 
     */
    virtual size_t generateTasksMgnt(std::string &codeinvoke, std::string &codedelete, std::string &codedecl, std::string &codetasks);
    
    /**
     * If the selected runtime container includes a group, then generate code for a group
     * 
     * \see runtime_container_e
     * 
     * @param id
     * @param tasks
     * @param grp_mapping
     * @param codeinvoke
     * @param codedelete
     * @param codedecl
     * @param paralleltasks
     * @return 
     */
    virtual size_t generateGroup(const std::string &id, const std::vector<Task*> tasks, ComputeUnit *grp_mapping, std::string &codeinvoke, std::string &codedelete, std::string &codedecl, std::string &paralleltasks);
    
    /**
     * Generate the invocation of the end-user function for a task
     * @param tasks
     * @param codeinvoke
     * @param codedelete
     * @param codedecl
     * @param codetasks
     * @return 
     */
    virtual size_t generateTasksInvocation(const std::vector<Task*> tasks, std::string &codeinvoke, std::string &codedelete, std::string &codedecl, std::string &codetasks);
};

REGISTER_GENERATOR(LinuxRuntime, "linux")

#endif /* LINUXRUNTIME_H */


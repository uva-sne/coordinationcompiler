/* 
 * Copyright (C) 2017 Benjamin Rouxel <benjamin.rouxel@inria.fr> 
 *                    Irisa/INRIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GENERATOR_H
#define GENERATOR_H

#include <string>
#include <iostream>
#include <fstream>
#include <map>
#include <cassert>

#include "config.hpp"
#include "SystemModel.hpp"
#include "Schedule.hpp"
#include "registry.hpp"
#include "CompilerPass.hpp"
#include <boost/filesystem.hpp>

/*!
 * \brief Base class for generators
 * 
 * Generators output can be based on any of the two IR: #SystemModel or #Schedule
 */
class Generator : public CompilerPass {
protected:
    //! IR of the current application
    const SystemModel *tg;
    //! the global configuration
    const config_t *conf;
    //! current state of the schedule
    const Schedule *sched;
    
public:
    //! Constructor
    explicit Generator() {};
    void run(SystemModel *m, config_t *c, Schedule *s) {
        tg = m;
        conf = c;
        sched = s;
        
        check_dependencies();
        generate();
    }

    virtual ~Generator() = default;
    
    /*!
     * \brief Set params extracted from the configuration file
     * 
     * This parameters are found in subtag of the generator tag.
     * 
     * \param key   subtag name
     * \param args   map of parameters
     */
    virtual void setAuxilliaryParams(std::string key, const std::map<std::string, std::string> &args) {}

protected:
    //! Perform the generation
    virtual void generate() {
        generate(conf->files.output_folder / conf->files.coord_basename);
    }
    /**
     * Perform the generation output should in f
     * @param f
     */
    virtual void generate(const boost::filesystem::path &f) = 0;
};

using GeneratorRegistry = registry::Registry<Generator, std::string>;

#define REGISTER_GENERATOR(ClassName, Identifier) \
  REGISTER_SUBCLASS(Generator, ClassName, std::string, Identifier)

#endif /* GENERATOR_H */


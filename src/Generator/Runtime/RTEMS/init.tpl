/*
 * Copyright (C) 2020 Benjamin Rouxel <benjamin.rouxel@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
R"CPPRAWMARKER(
// START RTEMS/init.tpl
#include "config.h"

#define CONFIGURE_INIT

const char rtems_app_name[] = "%APPNAME%";

static rtems_id main_task = 0;
rtems_id Task_id[CONFIGURE_MAXIMUM_TASKS];  /* array of task ids */
rtems_name Task_name[CONFIGURE_MAXIMUM_TASKS]; /* array of task names */

%INCLUDES%

%BUFFERS%

%TASKS%

void main_exit(int);
void main_init() {
    %BUFFERSINIT%
    %INITIALIZER%
}

void main_clean() {
    int status = 0;
    %CLEANER%
    %BUFFERSCLEAN%
}

void main_exit(int unused) {
    main_clean();
    exit(0);
}

rtems_task Init(rtems_task_argument arg) {
    rtems_status_code status;
    rtems_time_of_day time;

    rtems_task_ident(RTEMS_SELF,RTEMS_SEARCH_ALL_NODES,&main_task);

    time.year   = 1900;
    time.month  = 1;
    time.day    = 1;
    time.hour   = 0;
    time.minute = 0;
    time.second = 0;
    time.ticks  = 0;
    status = rtems_clock_set( &time );

    main_init();

    %SETUPTASKS%

    rtems_task_delete( RTEMS_SELF );
    exit(0);
}
// END RTEMS/init.tpl
)CPPRAWMARKER"



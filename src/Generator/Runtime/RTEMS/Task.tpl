/*
 * Copyright (C) 2020 Benjamin Rouxel <benjamin.rouxel@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
R"CPPRAWMARKER(
// START RTEMS/Task.tpl
rtems_task __coord_%TASKNAME%_%VERSI%(rtems_task_argument unused) {
//  rtems_id          tid;
//  uint32_t          task_index;
//  rtems_status_code status;

//  status = rtems_task_ident( RTEMS_SELF, RTEMS_SEARCH_ALL_NODES, &tid );
//  task_index = task_number( tid );

  rtems_task_mode(RTEMS_TIMESLICE,RTEMS_TIMESLICE_MASK,&__coord_%TASKNAME%_%VERSI%_old_mode);

  %TASK_INPUTS%
  %TASK_CALLABLE%
  %TASK_OUTPUTS%
  
  rtems_task_delete(rtems_task_self());
}
// END RTEMS/Task.tpl
)CPPRAWMARKER"

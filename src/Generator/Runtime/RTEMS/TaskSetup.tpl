/*
 * Copyright (C) 2020 Benjamin Rouxel <benjamin.rouxel@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
R"CPPRAWMARKER(
// START RTEMS/SetupTask.tpl
  Task_name[ %TASKINDEX% ] = rtems_build_name(%TASKNAME%);
  status = rtems_task_create(
    Task_name[ %TASKINDEX% ], 1, RTEMS_MINIMUM_STACK_SIZE, RTEMS_DEFAULT_MODES,
    RTEMS_DEFAULT_ATTRIBUTES, &Task_id[ %TASKINDEX% ]
  );
  status = rtems_task_start( Task_id[ %TASKINDEX% ], %TASKS_CALL%, 0 );
  if ( status != RTEMS_SUCCESSFUL)
    printf( "unable to start %TASKNAME% - %TASKINDEX%\n" );
// END RTEMS/SetupTask.tpl
)CPPRAWMARKER"


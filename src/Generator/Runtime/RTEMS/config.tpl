/*
 * Copyright (C) 2020 Benjamin Rouxel <benjamin.rouxel@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
R"CPPRAWMARKER(
// START RTEMS/config.tpl
#ifndef CONFIG_H
#define CONFIG_H

#define CONFIGURE_RTEMS_INIT_TASKS_TABLE
#define CONFIGURE_APPLICATION_NEEDS_CLOCK_DRIVER
#define CONFIGURE_APPLICATION_NEEDS_CONSOLE_DRIVER
#define CONFIGURE_APPLICATION_NEEDS_LIBBLOCK

#define CONFIGURE_INIT
#define CONFIGURE_INIT_TASK_ATTRIBUTES (RTEMS_FLOATING_POINT)
#define CONFIGURE_STACK_CHECKER_ENABLED

#define CONFIGURE_MAXIMUM_TASKS %NBTASKS%

#define CONFIGURE_MAXIMUM_PROCESSORS %NBCORES%

//#define CONFIGURE_MINIMUM_TASK_STACK_SIZE (4*1024)
//#define CONFIGURE_UNIFIED_WORK_AREAS
//
//#define CONFIGURE_APPLICATION
//#define CONFIGURE_MEMORY_OVERHEAD (2560)
//#define CONFIGURE_MICROSECONDS_PER_TICK 1000
//#define CONFIGURE_TICKS_PER_TIMESLICE 5
//
//#define CONFIGURE_MAXIMUM_FILE_DESCRIPTORS 50
//#define CONFIGURE_USE_IMFS_AS_BASE_FILESYSTEM
//
//#define CONFIGURE_SWAPOUT_TASK_PRIORITY 220
//#define CONFIGURE_MAXIMUM_SEMAPHORES 2
//
//#define CONFIGURE_INIT_TASK_INITIAL_MODES (RTEMS_NO_PREEMPT | \
//  RTEMS_NO_TIMESLICE | \
//  RTEMS_ASR | \
//  RTEMS_INTERRUPT_LEVEL(0))
//
//#define CONFIGURE_INIT_TASK_STACK_SIZE (RTEMS_MINIMUM_STACK_SIZE * 20)
//#define CONFIGURE_INIT_TASK_PRIORITY 50
//#define CONFIGURE_RTEMS_INIT_TASKS_TABLE
//
//#define CONFIGURE_EXTRA_TASK_STACKS (RTEMS_MINIMUM_STACK_SIZE * 3)
//
//#define CONFIGURE_MAXIMUM_USER_EXTENSIONS 10
//
//#define CONFIGURE_SHELL_COMMANDS_INIT
//
//#include <rtems/shell.h>
//
//#define CONFIGURE_SHELL_COMMANDS_ALL
//
//#include <rtems/shellconfig.h>

%CONFIGS%

#include <rtems/confdefs.h>

#include <rtems.h>
#include <inttypes.h>
#include <stdio.h>

rtems_task Init(
  rtems_task_argument argument
);

%TASKS%

extern rtems_id   Task_id[ CONFIGURE_MAXIMUM_TASKS ];         /* array of task ids */
extern rtems_name Task_name[ CONFIGURE_MAXIMUM_TASKS ];       /* array of task names */
extern volatile bool TaskRan[ CONFIGURE_MAXIMUM_PROCESSORS ]; /* array if task mapping */

#endif /* CONFIG_H */

// END RTEMS/config.tpl
)CPPRAWMARKER"

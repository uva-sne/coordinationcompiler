/*
 * Copyright (C) 2020 Benjamin Rouxel <benjamin.rouxel@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
R"CPPRAWMARKER(
# START RTEMS/build.tpl
PGM=%APPNAME%.exe

RTEMS_MAKEFILE_PATH ?= %RTEMS_MK_PATH%

# optional managers required
MANAGERS=all

# C source names
SRCS = %SRC%

EXTRAOBJS = %EXTRAOBJECT%

APPOBJS = $(SRCS:%.%CODELANG%=%.o)

include $(RTEMS_MAKEFILE_PATH)/Makefile.inc
include $(RTEMS_CUSTOM)
include $(PROJECT_ROOT)/make/leaf.cfg

%UPCODELANG%FLAGS += %COMPOPT%
LDFLAGS += %LINKOPT%

LINK_LIBS += -lm %LINK_LIB%
OBJS= $(APPOBJS) $(COBJS) $(CXXOBJS) $(ASOBJS) $(EXTRAOBJS)

all:    $(PGM)

$(PGM): $(OBJS)
	$(make-exe)

# END RTEMS/build.tpl
)CPPRAWMARKER"




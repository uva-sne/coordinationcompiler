/*
 * Copyright (C) 2019 Benjamin Rouxel <benjamin.rouxel@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LINUXGENUSERLAND_H
#define LINUXGENUSERLAND_H

#include "Generator/Runtime/RuntimeStrategyInterface.hpp"
#include "Generator/LinuxRuntime.hpp"
#include "Generator/CodeGen/CodeGenerationEnums.hpp"

#include <signal.h>
#include <string>

/*! \brief Interface for Linux-based Runtime Generator
 * 
 * Interface to select the correct template file for runtime generator
 * 
 */
class LinuxGenUserLand : public RuntimeStrategyInterface {
public:
    virtual std::string getMainFileName() override;
    
    virtual std::string tpl_getBuildRule() override;
    virtual std::string tpl_getMain() override;
    virtual std::string tpl_getMainMapping() override;
    virtual std::string tpl_getTask() override;
    virtual std::string tpl_getGroupInstantiate() override;
    virtual std::string tpl_getGroupDecl() override;
    virtual std::string tpl_getGroupCleanup() override;
    virtual std::string tpl_getRunnableGroup() override;
    virtual std::string tpl_getRunnableTask() override;
    virtual std::string tpl_getTaskCall() override;
    virtual std::string tpl_getTaskInstantiate() override;
    virtual std::string tpl_getTaskDecl() override;
    virtual std::string tpl_getTaskCleanup() override;
    virtual std::string tpl_getTaskWaitForInput() override;
    virtual std::string tpl_getTaskReleaseSuccessor() override;
    virtual std::string tpl_getLockInit() override;
    virtual std::string tpl_getLockClean() override;
    virtual std::string tpl_getLockDecl() override;
    virtual std::string tpl_getSharedDefs() override;
    virtual std::string tpl_getSharedDefsBufferMgnt() override;
    virtual std::string tpl_getTaskInput(uint32_t nbtokens) override;
    virtual std::string tpl_getTaskOutput(uint32_t nbtokens) override;
    virtual std::string tpl_getPeriodLockClean() override;
    virtual std::string tpl_getPeriodUnLock() override;
    virtual std::string tpl_getPeriodLockInit() override;
    virtual std::string tpl_getPeriodLock() override;
    virtual std::string tpl_getPeriodWait() override;
    virtual std::string tpl_getGroupTaskWait() override;
};


#endif /* LINUXGENUSERLAND_H */


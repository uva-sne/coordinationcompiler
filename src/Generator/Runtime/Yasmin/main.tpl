/*
 * Copyright (C) 2020 Benjamin Rouxel <benjamin.rouxel@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
R"CPPRAWMARKER(
// START CoordRuntime/main.tpl
#include <stdlib.h>

#include "yasmin_config.h"
#include "yasmin_api.h"
#include "yasmin_api_channels.h"
%INCLUDES%

%BUFFERS%

%COORDTASK%

void main_exit(int);
void main_init() {
//    signal(SIGINT, main_exit);
//    signal(SIGILL, main_exit);
//    signal(SIGABRT, main_exit);
//    signal(SIGFPE, main_exit);
//    signal(SIGSEGV, main_exit);
//    signal(SIGTERM, main_exit);
//    signal(SIGKILL, main_exit);
//    signal(SIGSTOP, main_exit);
//    signal(SIGTSTP, main_exit);

    yas_init();

    %INITIALIZER%
}

void main_clean() {
    int status = 0;

    %CLEANER%
    yas_cleanup();
}

void main_exit(int unused) {
    main_clean();
    exit(0);
}

int main(int argc, char **argv) {
    main_init();

    %SETUPTASKS%

    %BUFFERSINIT%

    yas_start();

    %ENDING%
    
    yas_stop();
    
    main_clean();
    return 0;
}
// END CoordRuntime/main.tpl
)CPPRAWMARKER"

/*
 * Copyright (C) 2020 Benjamin Rouxel <benjamin.rouxel@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
R"CPPRAWMARKER(
# START CoordRuntime/cmake.tpl
%CC%
cmake_minimum_required(VERSION 2.8)
project(%APPNAME% %UPCODELANG%)

%ARCH%

set(CMAKE_VERBOSE_MAKEFILE "off")
set(CMAKE_%UPCODELANG%_FLAGS "${CMAKE_%UPCODELANG%_FLAGS} %COMPOPT%")
set(CMAKE_%UPCODELANG%_FLAGS_RELEASE "${CMAKE_%UPCODELANG%_FLAGS_RELEASE} %COMPOPT%")
set(CMAKE_%UPCODELANG%_FLAGS_DEBUG "${CMAKE_%UPCODELANG%_FLAGS_DEBUG} %COMPOPT%")
set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} %LINKOPT%")
set(CMAKE_%UPCODELANG%_FLAGS_DEBUG "-g -Og -D_DEBUG")
set(CMAKE_%UPCODELANG%_FLAGS_RELEASE "-O0")
set(CMAKE_ASM_FLAGS_RELEASE "-O0")

get_filename_component(YASRUNTIMECORE_DIR "%YASRUNTIME%/tbrem.useless" DIRECTORY)
if(NOT EXISTS "${YASRUNTIMECORE_DIR}/cmake/")
    message(FATAL_ERROR "File `${YASRUNTIMECORE_DIR}`/cmake/ doesn't exists.\nPlease check the parameter yasminpath=\"\" in the XML compiler config file.")
endif()
# The cmake/ directory includes a few macros for finding architecture specificities
set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${YASRUNTIMECORE_DIR}/cmake/")
message("-- Found ${YASRUNTIMECORE_DIR}/cmake/")

%EXTRAPKG%

set(YAS_RUNTIME_APP_PATH ${CMAKE_SOURCE_DIR})
find_package(yasmin_runtime_core REQUIRED)
message("-- Library yasmin_runtime_core included")

file(GLOB YAS_TARGET_SOURCES ${CMAKE_SOURCE_DIR}/*.%CODELANG%)
message("-- Checking sources in ${CMAKE_SOURCE_DIR}")

set(YAS_TARGET_ADD_SOURCES %SRC%)
set(YAS_TARGET_ADD_OBJS %EXTRAOBJECT%)
set(CMAKE_TARGET_NAME %APPNAME%)

add_executable(${CMAKE_TARGET_NAME} ${YAS_TARGET_SOURCES} ${YAS_TARGET_ADD_SOURCES} ${YAS_TARGET_ADD_OBJS})

target_link_libraries(${CMAKE_TARGET_NAME} PUBLIC ${YAS_CUSTOM_LIB_NAME})
%EXTRALIB%

target_include_directories(${CMAKE_TARGET_NAME} PUBLIC %INCLUDES%)
# END CoordRuntime/cmake.tpl
)CPPRAWMARKER"



/*
 * Copyright (C) 2021 Lukas Miedema <l.miedema@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
R"CPPRAWMARKER(
// Generated dot file based on %APPNAME%
digraph hardware {
%HARDWARE_NODES%
%HARDWARE_ATTRIBUTES%}

digraph application {
    // --- Attributes ---
    rankdir=LR;
    nodesep=0.5;
    ranksep=0.2;
%ATTRIBUTES%
    // --- Section 0 ---
    subgraph 0 {
        // --- Data nodes ---
%DATA_NODES%
        // --- Actor nodes ---
%ACTOR_NODES%
        // --- Edges ---
%EDGES%
    }
}
)CPPRAWMARKER"

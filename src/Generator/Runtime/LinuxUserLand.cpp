/*
 * Copyright (C) 2019 Benjamin Rouxel <benjamin.rouxel@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "Generator/Runtime/LinuxUserLand.hpp"
#include <boost/filesystem.hpp>

namespace ba = boost::algorithm;
using namespace std;

string LinuxGenUserLand::getMainFileName() {
    return app_name()+"_app"; 
}

std::string LinuxGenUserLand::tpl_getBuildRule() {
    return 
#include "Generator/Runtime/Linux/user-space/MakefileRule.tpl"
            ;
}

std::string LinuxGenUserLand::tpl_getMain() {
    if(runtime() == CodeGeneration::runtime_container_e::SEQUENTIAL)
        return 
#include "Generator/Runtime/Linux/user-space/AppSequential.tpl"
            ;
    if(runtime() == CodeGeneration::runtime_container_e::FORK)
        return 
#include "Generator/Runtime/Linux/user-space/AppFork.tpl"
            ;
    if(runtime() == CodeGeneration::runtime_container_e::PTHREAD)
        return 
#include "Generator/Runtime/Linux/user-space/AppPthread.tpl"
            ;
    if(runtime() == CodeGeneration::runtime_container_e::GROUPEDFORK)
            return 
#include "Generator/Runtime/Linux/user-space/AppFork.tpl"
            ;
    if(runtime() == CodeGeneration::runtime_container_e::GROUPEDPTHREAD) 
            return 
#include "Generator/Runtime/Linux/user-space/AppPthread.tpl"
            ;
    if(runtime() == CodeGeneration::runtime_container_e::FORKANDPTHREAD)
            return 
#include "Generator/Runtime/Linux/user-space/AppForkAndThread.tpl"
            ;
    throw std::logic_error("End of templating function reached.");
}

std::string LinuxGenUserLand::tpl_getMainMapping() {
    if(runtime() == CodeGeneration::runtime_container_e::SEQUENTIAL)
        return 
#include "Generator/Runtime/Linux/user-space/MainMapping.tpl"
            ;
    return "";
}

std::string LinuxGenUserLand::tpl_getTask() {
    if(synchro() == CodeGeneration::synchro_mechanism_e::NONE)
        return 
#include "Generator/Runtime/Linux/TaskMgnt/NoSyncTask.tpl"
;
    if(synchro() == CodeGeneration::synchro_mechanism_e::IFTHENELSE)
        return 
#include "Generator/Runtime/Linux/TaskMgnt/IfThenTask.tpl"
;
    if(synchro() == CodeGeneration::synchro_mechanism_e::SEMAPHORES)
        return 
#include "Generator/Runtime/Linux/TaskMgnt/SemTask.tpl"
            ;
    if(synchro() == CodeGeneration::synchro_mechanism_e::SEMAPHORESPERCHAN)
        return 
#include "Generator/Runtime/Linux/TaskMgnt/SemChanTask.tpl"
            ;
    throw std::logic_error("End of templating function reached.");
}

std::string LinuxGenUserLand::tpl_getGroupInstantiate() {
    if(runtime() == CodeGeneration::runtime_container_e::SEQUENTIAL)
        return "";
    if(runtime() == CodeGeneration::runtime_container_e::FORK)
        return "";
    if(runtime() == CodeGeneration::runtime_container_e::PTHREAD)
        return "";
    if(runtime() == CodeGeneration::runtime_container_e::GROUPEDFORK)
            return 
#include "Generator/Runtime/Linux/user-space/ForkCreate.tpl"
            ;
    if(runtime() == CodeGeneration::runtime_container_e::GROUPEDPTHREAD) 
            return 
#include "Generator/Runtime/Linux/user-space/PthreadCreate.tpl"
            ;
    if(runtime() == CodeGeneration::runtime_container_e::FORKANDPTHREAD)
            return 
#include "Generator/Runtime/Linux/user-space/ForkCreate.tpl"
            ;
    throw std::logic_error("End of templating function reached.");
}

std::string LinuxGenUserLand::tpl_getGroupDecl() {
    if(runtime() == CodeGeneration::runtime_container_e::SEQUENTIAL)
        return "";
    if(runtime() == CodeGeneration::runtime_container_e::FORK)
        return "";
    if(runtime() == CodeGeneration::runtime_container_e::PTHREAD)
        return "";
    if(runtime() == CodeGeneration::runtime_container_e::GROUPEDFORK)
            return 
#include "Generator/Runtime/Linux/user-space/ForkDecl.tpl"
            ;
    if(runtime() == CodeGeneration::runtime_container_e::GROUPEDPTHREAD) 
            return 
#include "Generator/Runtime/Linux/user-space/PthreadDecl.tpl"
            ;
    if(runtime() == CodeGeneration::runtime_container_e::FORKANDPTHREAD)
            return 
#include "Generator/Runtime/Linux/user-space/ForkDecl.tpl"
            ;
    throw std::logic_error("End of templating function reached.");
}

std::string LinuxGenUserLand::tpl_getGroupCleanup() {
    if(runtime() == CodeGeneration::runtime_container_e::SEQUENTIAL)
        return "";
    if(runtime() == CodeGeneration::runtime_container_e::FORK)
        return "";
    if(runtime() == CodeGeneration::runtime_container_e::PTHREAD)
        return "";
    if(runtime() == CodeGeneration::runtime_container_e::GROUPEDFORK)
            return 
#include "Generator/Runtime/Linux/user-space/ForkJoin.tpl"
            ;
    if(runtime() == CodeGeneration::runtime_container_e::GROUPEDPTHREAD) 
            return 
#include "Generator/Runtime/Linux/user-space/PthreadJoin.tpl"
            ;
    if(runtime() == CodeGeneration::runtime_container_e::FORKANDPTHREAD)
            return 
#include "Generator/Runtime/Linux/user-space/ForkJoin.tpl"
            ;
    throw std::logic_error("End of templating function reached.");
}

std::string LinuxGenUserLand::tpl_getRunnableGroup() {
    if(runtime() == CodeGeneration::runtime_container_e::SEQUENTIAL)
        return "";
    if(runtime() == CodeGeneration::runtime_container_e::FORK)
        return "";
    if(runtime() == CodeGeneration::runtime_container_e::PTHREAD)
        return "";
    if(runtime() == CodeGeneration::runtime_container_e::GROUPEDFORK)
            return 
#include "Generator/Runtime/Linux/user-space/Fork.tpl"
            ;
    if(runtime() == CodeGeneration::runtime_container_e::GROUPEDPTHREAD) 
            return 
#include "Generator/Runtime/Linux/user-space/GroupedPthread.tpl"
            ;
    if(runtime() == CodeGeneration::runtime_container_e::FORKANDPTHREAD)
            return 
#include "Generator/Runtime/Linux/user-space/ForkPthread.tpl"
            ;
    throw std::logic_error("End of templating function reached.");
}

std::string LinuxGenUserLand::tpl_getRunnableTask() {
    if(runtime() == CodeGeneration::runtime_container_e::SEQUENTIAL)
        return "";
    if(runtime() == CodeGeneration::runtime_container_e::FORK)
        return 
#include "Generator/Runtime/Linux/user-space/Fork.tpl"
            ;
    if(runtime() == CodeGeneration::runtime_container_e::PTHREAD)
        return 
#include "Generator/Runtime/Linux/user-space/Pthread.tpl"
            ;
    if(runtime() == CodeGeneration::runtime_container_e::GROUPEDFORK)
        return "";
    if(runtime() == CodeGeneration::runtime_container_e::GROUPEDPTHREAD) 
        return "";
    if(runtime() == CodeGeneration::runtime_container_e::FORKANDPTHREAD)
        return 
#include "Generator/Runtime/Linux/user-space/Pthread.tpl"
            ;
    throw std::logic_error("End of templating function reached.");
}

std::string LinuxGenUserLand::tpl_getTaskCall() {
    return 
#include "Generator/Runtime/Linux/user-space/SeqCall.tpl"
            ;
}

std::string LinuxGenUserLand::tpl_getTaskInstantiate() {
    if(runtime() == CodeGeneration::runtime_container_e::SEQUENTIAL)
        return 
#include "Generator/Runtime/Linux/user-space/SeqCall.tpl"
            ;
    if(runtime() == CodeGeneration::runtime_container_e::PTHREAD)
        return 
#include "Generator/Runtime/Linux/user-space/PthreadCreate.tpl"
            ;
    if(runtime() == CodeGeneration::runtime_container_e::FORK)
        return 
#include "Generator/Runtime/Linux/user-space/ForkCreate.tpl"
            ;
    if(runtime() == CodeGeneration::runtime_container_e::GROUPEDPTHREAD)
        return 
#include "Generator/Runtime/Linux/user-space/SeqCallGrouped.tpl"
            ;
    if(runtime() == CodeGeneration::runtime_container_e::GROUPEDFORK)
        return 
#include "Generator/Runtime/Linux/user-space/SeqCall.tpl"
            ;
    if(runtime() == CodeGeneration::runtime_container_e::FORKANDPTHREAD)
        return 
#include "Generator/Runtime/Linux/user-space/PthreadCreate.tpl"
            ;
    throw std::logic_error("End of templating function reached.");
}

std::string LinuxGenUserLand::tpl_getTaskDecl() {
    if(runtime() == CodeGeneration::runtime_container_e::SEQUENTIAL)
        return "";
    if(runtime() == CodeGeneration::runtime_container_e::PTHREAD)
        return 
#include "Generator/Runtime/Linux/user-space/PthreadDecl.tpl"
            ;
    if(runtime() == CodeGeneration::runtime_container_e::FORK)
        return 
#include "Generator/Runtime/Linux/user-space/ForkDecl.tpl"
            ;
    if(runtime() == CodeGeneration::runtime_container_e::GROUPEDPTHREAD)
        return "";
    if(runtime() == CodeGeneration::runtime_container_e::GROUPEDFORK)
        return "";
    if(runtime() == CodeGeneration::runtime_container_e::FORKANDPTHREAD)
        return 
#include "Generator/Runtime/Linux/user-space/PthreadDecl.tpl"
            ;
    throw std::logic_error("End of templating function reached.");
}


std::string LinuxGenUserLand::tpl_getTaskCleanup() {
    if(runtime() == CodeGeneration::runtime_container_e::SEQUENTIAL)
        return "";
    if(runtime() == CodeGeneration::runtime_container_e::PTHREAD)
        return 
#include "Generator/Runtime/Linux/user-space/PthreadJoin.tpl"
            ;
    if(runtime() == CodeGeneration::runtime_container_e::FORK)
        return 
#include "Generator/Runtime/Linux/user-space/ForkJoin.tpl"
            ;
    if(runtime() == CodeGeneration::runtime_container_e::GROUPEDPTHREAD)
        return "";
    if(runtime() == CodeGeneration::runtime_container_e::GROUPEDFORK)
        return "";
    if(runtime() == CodeGeneration::runtime_container_e::FORKANDPTHREAD)
        return 
#include "Generator/Runtime/Linux/user-space/PthreadJoin.tpl"
            ;
    throw std::logic_error("End of templating function reached.");
}
std::string LinuxGenUserLand::tpl_getTaskWaitForInput() {
    if(synchro() == CodeGeneration::synchro_mechanism_e::NONE)
        return "";
    if(synchro() == CodeGeneration::synchro_mechanism_e::IFTHENELSE)
        return 
#include "Generator/Runtime/Linux/TaskMgnt/IfThenTaskPred.tpl"
;
    if(synchro() == CodeGeneration::synchro_mechanism_e::SEMAPHORES)
        return 
#include "Generator/Runtime/Linux/TaskMgnt/SemTaskPred.tpl"
            ;
    if(synchro() == CodeGeneration::synchro_mechanism_e::SEMAPHORESPERCHAN)
        return 
#include "Generator/Runtime/Linux/TaskMgnt/SemChanTaskPred.tpl"
            ;
    throw std::logic_error("End of templating function reached.");
}
std::string LinuxGenUserLand::tpl_getTaskReleaseSuccessor() {
    if(synchro() == CodeGeneration::synchro_mechanism_e::NONE)
        return "";
    if(synchro() == CodeGeneration::synchro_mechanism_e::IFTHENELSE)
        return "";
    if(synchro() == CodeGeneration::synchro_mechanism_e::SEMAPHORES)
        return "";
    if(synchro() == CodeGeneration::synchro_mechanism_e::SEMAPHORESPERCHAN)
        return 
#include "Generator/Runtime/Linux/TaskMgnt/SemChanTaskSuc.tpl"
            ;
    throw std::logic_error("End of templating function reached.");
}
std::string LinuxGenUserLand::tpl_getLockInit() {
    if(synchro() == CodeGeneration::synchro_mechanism_e::NONE)
        return "";
    if(synchro() == CodeGeneration::synchro_mechanism_e::IFTHENELSE)
        return "";
    if(synchro() == CodeGeneration::synchro_mechanism_e::SEMAPHORES)
    return 
#include "Generator/Runtime/Linux/TaskMgnt/SemInit.tpl"
            ;
    if(synchro() == CodeGeneration::synchro_mechanism_e::SEMAPHORESPERCHAN)
    return 
#include "Generator/Runtime/Linux/TaskMgnt/SemChanInit.tpl"
            ;
    throw std::logic_error("End of templating function reached.");
}
std::string LinuxGenUserLand::tpl_getLockClean() {
    if(synchro() == CodeGeneration::synchro_mechanism_e::NONE)
        return "";
    if(synchro() == CodeGeneration::synchro_mechanism_e::IFTHENELSE)
        return "";
    if(synchro() == CodeGeneration::synchro_mechanism_e::SEMAPHORES)
    return 
#include "Generator/Runtime/Linux/TaskMgnt/SemClean.tpl"
            ;
    if(synchro() == CodeGeneration::synchro_mechanism_e::SEMAPHORESPERCHAN)
    return 
#include "Generator/Runtime/Linux/TaskMgnt/SemChanClean.tpl"
            ;
    throw std::logic_error("End of templating function reached.");
}
std::string LinuxGenUserLand::tpl_getLockDecl() {
    if(synchro() == CodeGeneration::synchro_mechanism_e::NONE)
        return "";
    if(synchro() == CodeGeneration::synchro_mechanism_e::IFTHENELSE)
        return "";
    if(synchro() == CodeGeneration::synchro_mechanism_e::SEMAPHORES)
    return 
#include "Generator/Runtime/Linux/TaskMgnt/SemDecl.tpl"
            ;
    if(synchro() == CodeGeneration::synchro_mechanism_e::SEMAPHORESPERCHAN)
    return 
#include "Generator/Runtime/Linux/TaskMgnt/SemChanDecl.tpl"
            ;
    throw std::logic_error("End of templating function reached.");
}

std::string LinuxGenUserLand::tpl_getSharedDefs() {
    if(channel_type() == CodeGeneration::channel_mechanism_e::SHAREDMEMORY)
        return 
#include "Generator/Runtime/Linux/BufMgnt/GenSharedDefs.tpl"
            ;
    if(channel_type() == CodeGeneration::channel_mechanism_e::PIPE)
        return 
#include "Generator/Runtime/Linux/BufMgnt/GenPipeDefs.tpl"
            ;
    
    throw std::logic_error("End of templating function reached.");
}
std::string LinuxGenUserLand::tpl_getSharedDefsBufferMgnt() {
    if(channel_type() == CodeGeneration::channel_mechanism_e::SHAREDMEMORY)
        return 
#include "Generator/Runtime/Linux/BufMgnt/GenSharedDefsBufferMgnt.tpl"
            ;
    if(channel_type() == CodeGeneration::channel_mechanism_e::PIPE)
        return 
#include "Generator/Runtime/Linux/BufMgnt/GenPipeDefsBufferMgnt.tpl"
            ;
    
    throw std::logic_error("End of templating function reached.");
}
std::string LinuxGenUserLand::tpl_getTaskInput(uint32_t nbtokens) {
    if(nbtokens < 2)
        return 
#include "Generator/Runtime/Linux/TaskMgnt/TaskInputTokens.tpl"
            ;
    return 
#include "Generator/Runtime/Linux/TaskMgnt/TaskInputArrayTokens.tpl"
            ;
}
std::string LinuxGenUserLand::tpl_getTaskOutput(uint32_t nbtokens) {
    if(nbtokens < 2)
        return 
#include "Generator/Runtime/Linux/TaskMgnt/TaskOutputTokens.tpl"
            ;
    return 
#include "Generator/Runtime/Linux/TaskMgnt/TaskOutputArrayTokens.tpl"
            ;
}

std::string LinuxGenUserLand::tpl_getPeriodLockClean() {
    return
#include "Generator/Runtime/Linux/CycleMgnt/PeriodSemClean.tpl"
        ;
}
std::string LinuxGenUserLand::tpl_getPeriodUnLock() {
    return
#include "Generator/Runtime/Linux/CycleMgnt/PeriodSem.tpl"
        ;
}
std::string LinuxGenUserLand::tpl_getPeriodLockInit() {
    return
#include "Generator/Runtime/Linux/CycleMgnt/PeriodSemInit.tpl"
        ;
}
std::string LinuxGenUserLand::tpl_getPeriodLock() {
    return
#include "Generator/Runtime/Linux/CycleMgnt/PeriodSemDecl.tpl"
        ;
}
std::string LinuxGenUserLand::tpl_getPeriodWait() {
    return 
#include "Generator/Runtime/Linux/CycleMgnt/PeriodSemTask.tpl"
            ;
}

std::string LinuxGenUserLand::tpl_getGroupTaskWait() {
    switch (sleep_mechanism()) {
        case CodeGeneration::sleep_mechanism_e::SLEEP:
            return
                #include "Generator/Runtime/Linux/user-space/sleep/Sleep.tpl"
                ;
        case CodeGeneration::sleep_mechanism_e::BUSYLOOP:
            return
                #include "Generator/Runtime/Linux/user-space/sleep/BusyLoop.tpl"
                ;
        case CodeGeneration::sleep_mechanism_e::ALARM:
        default:
            throw std::logic_error("Not currently implemented.");
    }
}
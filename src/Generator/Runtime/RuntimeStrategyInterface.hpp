/*
 * Copyright (C) 2019 Benjamin Rouxel <benjamin.rouxel@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LINUXGENERATOR_H
#define LINUXGENERATOR_H

#include <string>

#include <regex>
#include <boost/filesystem.hpp>
#include <boost/algorithm/string.hpp>
#include <libgen.h>

#include "config.hpp"
#include "SystemModel.hpp"
#include "Schedule.hpp"
#include "Generator/CodeGen/CodeGenerationEnums.hpp"

#include "Helper_macros.hpp"

/*! \brief Interface for Runtime Strategy Generator
 * 
 * Interface to select the correct template file for runtime generator
 * 
 */
class RuntimeStrategyInterface {
protected:
    boost::filesystem::path _path; //!< The final path to store the generated files
    std::string _app_name; //!< Application name used in output directory name, and/or file name
    std::string _default_priority = "1"; //!< The default priority to use OS scheduler for the generated code
    std::string _os_scheduler = "SCHED_FIFO"; //!< The OS scheduler for the generated code
    
    std::string _conf_path = ""; //!< The path given in the configuration file to store the generated files
    
    CodeGeneration::synchro_mechanism_e _synchro = CodeGeneration::synchro_mechanism_e::NONE; //!< The type of synchronisation
    CodeGeneration::runtime_container_e _runtime = CodeGeneration::runtime_container_e::PTHREAD; //!< The runtime container
    CodeGeneration::channel_mechanism_e _channel_type = CodeGeneration::channel_mechanism_e::SHAREDMEMORY; //!< The mechanism to use to implement channels
    CodeGeneration::sleep_mechanism_e _sleep_mechanism = CodeGeneration::sleep_mechanism_e::SLEEP; //!< The mechanism to use to sleep/wait
    
    bool _check_before_release = false;
    
public:
    /*!
     * Return the template content to build a file
     * @return
     */
    virtual std::string tpl_getBuildFile() = 0; 
    /*!     
     * Return the template content to create a build rule
     * @return
     */
    virtual std::string tpl_getBuildRule() = 0; 
    
    /*!     
     * Return the template content to generate the main
     * @return
     */
    virtual std::string tpl_getMain() = 0; 
    
    /*!     
     * Return the template content to set the mapping of the main thread
     * @return
     */
    virtual std::string tpl_getMainMapping() = 0; 
    
    /*!     
     * Return the template content to generate a task
     * @return
     */
    virtual std::string tpl_getTask() = 0; 
    /*!     
     * Return the template content to read task input from FIFO
     * @param nbtok number of expected tokens
     * @return
     */
    virtual std::string tpl_getTaskInput(uint32_t nbtok) = 0;
    /*!     
     * Return the template content to write task input into FIFO
     * @param nbtok number of expected tokens
     * @return
     */
    virtual std::string tpl_getTaskOutput(uint32_t nbtok) = 0; 
    /*!     
     * Return the template content to instantiate a task
     * @return
     */
    virtual std::string tpl_getTaskInstantiate() = 0; 
    /*!     
     * Return the template content to declare a task
     * @return
     */
    virtual std::string tpl_getTaskDecl() = 0; 
    /*!     
     * Return the template content to de-instantiate/cleanup a task
     * @return
     */
    virtual std::string tpl_getTaskCleanup() = 0; 
    /*!     
     * Return the template content to make a task wait for its input to be ready
     * @return
     */
    virtual std::string tpl_getTaskWaitForInput() = 0; 
    /*!     
     * Return the template content to release successor tasks
     * @return
     */
    virtual std::string tpl_getTaskReleaseSuccessor() = 0; 
    /*!     
     * Return the template content to init a lock
     * @return
     */
    virtual std::string tpl_getLockInit() = 0; 
    /*!     
     * Return the template content to cleanup a lock
     * @return
     */
    virtual std::string tpl_getLockClean() = 0; 
    /*!     
     * Return the template content to declare a lock
     * @return
     */
    virtual std::string tpl_getLockDecl() = 0; 
    
    /*!     
     * Return the template content to instantiate a group of tasks
     * @return
     */
    virtual std::string tpl_getGroupInstantiate() = 0; 
    /*!     
     * Return the template content to cleanup a group of tasks
     * @return
     */
    virtual std::string tpl_getGroupCleanup() = 0; 
    /*!     
     * Return the template content to declare a container of group of tasks
     * @return
     */
    virtual std::string tpl_getGroupDecl() = 0; 
    /*!     
     * Return the template content to ??
     * @return
     */
    virtual std::string tpl_getRunnableGroup() = 0; 
    /*!     
     * Return the template content to ??
     * @return
     */
    virtual std::string tpl_getRunnableTask() = 0;
    /*!     
     * Return the template content to call a task
     * @return
     */
    virtual std::string tpl_getTaskCall() = 0; 
    
    /*!     
     * Return the template content to generate the shared buffers top-level
     * @return
     */
    virtual std::string tpl_getSharedDefs() = 0; 
    /*!     
     * Return the template content to generate one shared buffer type
     * @return
     */
    virtual std::string tpl_getSharedDefsBufferMgnt() = 0; 
    
    /*!     
     * Return the template content to cleanup the periodic lock
     * @return
     */
    virtual std::string tpl_getPeriodLockClean() = 0; 
    /*!     
    * Return the template content to unlock the periodic lock
     * @return
     */
    virtual std::string tpl_getPeriodUnLock() = 0;
    /*!     
     * Return the template content to initialise the periodic lock
     * @return
     */
    virtual std::string tpl_getPeriodLockInit() = 0; 
    /*!     
     * Return the template content to lock the periodic lock
     * @return
     */
    virtual std::string tpl_getPeriodLock() = 0; 
    /*!     
     * Return the template content to put in a wait status for the periodic lock to be released
     * @return
     */
    virtual std::string tpl_getPeriodWait() = 0; 

    /*!     
     * Return the template content to put in a wait status for the periodic lock to be released
     * @return
     */
    virtual std::string tpl_getGroupTaskWait() = 0; 

    /*!     
     * Return the main file name, usually main.c/main.cpp
     * @return
     */
    virtual std::string getMainFileName() = 0; 
    
    /*!
     * Return a string specific to the current generator to append to the path
     * @return
     */
    virtual std::string appendToPath() = 0;

    /*!
     * Return true if the generator uses shared processes for the generated code
     * @return bool
     */
    virtual bool is_SharedProcesses();
    /*!
     * Return true if tasks are grouped
     * @return bool
     */
    virtual bool is_GroupedRunnable();

    ACCESSORS_R(RuntimeStrategyInterface, std::string, app_name)
    ACCESSORS_R(RuntimeStrategyInterface, boost::filesystem::path, path)
    ACCESSORS_R(RuntimeStrategyInterface, std::string, os_scheduler)
    ACCESSORS_R(RuntimeStrategyInterface, std::string, default_priority)
    ACCESSORS_R(RuntimeStrategyInterface, std::string, conf_path)
    ACCESSORS_RW(RuntimeStrategyInterface, CodeGeneration::synchro_mechanism_e, synchro)
    ACCESSORS_RW(RuntimeStrategyInterface, CodeGeneration::runtime_container_e, runtime)
    ACCESSORS_R(RuntimeStrategyInterface, CodeGeneration::channel_mechanism_e, channel_type)
    ACCESSORS_R(RuntimeStrategyInterface, CodeGeneration::sleep_mechanism_e, sleep_mechanism)
    ACCESSORS_R(RuntimeStrategyInterface, bool, check_before_release)

    /*!
     * Generate the final path to store the generated files and create the directories if necessary
     * 
     * \remark The initial path, and codegen dir are ignored if a path has been set in the configuration file
     * 
     * @param p initial path
     * @param codegendir name of the codegen dir to append
     */
    virtual void generatePath(const boost::filesystem::path &p, const boost::filesystem::path &codegendir);
    
    /*! \copydoc CompilerPass::setParams
     */
    void setParams(const std::map<std::string,std::string>& args);
    
    std::string help();
};

using TargetRuntimeRegistry = registry::Registry<RuntimeStrategyInterface, std::string>;

#define REGISTER_TARGETRUNTIME(ClassName, Identifier) \
  REGISTER_SUBCLASS(RuntimeStrategyInterface, ClassName, std::string, Identifier)

#endif /* LINUXGENERATOR_H */


/*
 * Copyright (C) 2020 Benjamin Rouxel, University of Amsterdam, benjamin.rouxel@uva.nl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "RTEMSRuntime.hpp"

using namespace std;

string RTEMSRuntime::tpl_getTask() {
    return 
#include "Generator/Runtime/RTEMS/Task.tpl"
    ;
}
string RTEMSRuntime::tpl_getTaskCall() {
    return
#include "Generator/Runtime/RTEMS/TaskSetup.tpl"
    ;
}
string RTEMSRuntime::tpl_getTaskDecl() {
    return
#include "Generator/Runtime/RTEMS/TaskDecl.tpl"
    ;
}

string RTEMSRuntime::tpl_getMain() {
    return 
#include "Generator/Runtime/RTEMS/init.tpl"
    ;
}
string RTEMSRuntime::tpl_getConfig() {
    return 
#include "Generator/Runtime/RTEMS/config.tpl"
    ;
}
string RTEMSRuntime::tpl_getBuildFile() {
    return
#include "Generator/Runtime/RTEMS/build.tpl"
    ;
}
string RTEMSRuntime::getMainFileName() {
    return "init"; 
}

string RTEMSRuntime::appendToPath() {
    return "-rtems";
}

string RTEMSRuntime::tpl_getSharedDefs() {
    if(_channel_type == CodeGeneration::channel_mechanism_e::SHAREDMEMORY)
        return 
#include "Generator/Runtime/Linux/BufMgnt/GenSharedDefs.tpl"
            ;
    if(_channel_type == CodeGeneration::channel_mechanism_e::PIPE)
        return 
#include "Generator/Runtime/Linux/BufMgnt/GenPipeDefs.tpl"
            ;
    
    throw logic_error("End of templating function reached.");
}
string RTEMSRuntime::tpl_getSharedDefsBufferMgnt() {
    if(_channel_type == CodeGeneration::channel_mechanism_e::SHAREDMEMORY)
        return 
#include "Generator/Runtime/Linux/BufMgnt/GenSharedDefsBufferMgnt.tpl"
            ;
    if(_channel_type == CodeGeneration::channel_mechanism_e::PIPE)
        return 
#include "Generator/Runtime/Linux/BufMgnt/GenPipeDefsBufferMgnt.tpl"
            ;
    
    throw logic_error("End of templating function reached.");
}
string RTEMSRuntime::tpl_getTaskInput(uint32_t nbtokens) {
    if(nbtokens < 2)
        return 
#include "Generator/Runtime/Linux/TaskMgnt/TaskInputTokens.tpl"
            ;
    return 
#include "Generator/Runtime/Linux/TaskMgnt/TaskInputArrayTokens.tpl"
            ;
}
string RTEMSRuntime::tpl_getTaskOutput(uint32_t nbtokens) {
    if(nbtokens < 2)
        return 
#include "Generator/Runtime/Linux/TaskMgnt/TaskOutputTokens.tpl"
            ;
    return 
#include "Generator/Runtime/Linux/TaskMgnt/TaskOutputArrayTokens.tpl"
            ;
}

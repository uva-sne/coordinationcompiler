/*
 * Copyright (C) 2020 Benjamin Rouxel, University of Amsterdam, benjamin.rouxel@uva.nl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef RTEMSRUNTIME_H
#define RTEMSRUNTIME_H

#include "Generator/Runtime/RuntimeStrategyInterface.hpp"

class RTEMSRuntime : public RuntimeStrategyInterface {
public:
    explicit RTEMSRuntime() : RuntimeStrategyInterface() {}
    virtual ~RTEMSRuntime() {}

    std::string tpl_getConfig();
    virtual std::string getMainFileName() override;
    
    virtual std::string tpl_getBuildFile() override;
    virtual std::string tpl_getBuildRule() override {return "";}
    
    virtual std::string tpl_getMain() override;
    virtual std::string tpl_getMainMapping() override {return "";}
    
    virtual std::string tpl_getTask() override;
    virtual std::string tpl_getTaskInstantiate() override {return "";}
    virtual std::string tpl_getTaskCall() override;
    virtual std::string tpl_getTaskDecl() override;
    virtual std::string tpl_getTaskCleanup() override {return "";}
    virtual std::string tpl_getTaskWaitForInput() override {return "";}
    virtual std::string tpl_getTaskReleaseSuccessor() override {return "";}
    virtual std::string tpl_getRunnableTask() override {return "";}
    
    virtual std::string tpl_getGroupInstantiate() override {return "";}
    virtual std::string tpl_getGroupDecl() override {return "";}
    virtual std::string tpl_getRunnableGroup() override {return "";}
    virtual std::string tpl_getGroupCleanup() override {return "";}
    
    virtual std::string tpl_getLockInit() override {return "";}
    virtual std::string tpl_getLockClean() override {return "";}
    virtual std::string tpl_getLockDecl() override {return "";}
    
    virtual std::string tpl_getSharedDefs() override;
    virtual std::string tpl_getSharedDefsBufferMgnt() override;
    
    virtual std::string tpl_getTaskInput(uint32_t nbtokens) override;
    virtual std::string tpl_getTaskOutput(uint32_t nbtokens) override;
    
    virtual std::string tpl_getPeriodLockClean() override {return "";}
    virtual std::string tpl_getPeriodUnLock() override {return "";}
    virtual std::string tpl_getPeriodLockInit() override {return "";}
    virtual std::string tpl_getPeriodLock() override {return "";}
    virtual std::string tpl_getPeriodWait() override {return "";}
    virtual std::string tpl_getGroupTaskWait() override {return "";}
    
    virtual std::string appendToPath() override;
};

#endif /* RTEMSRUNTIME_H */


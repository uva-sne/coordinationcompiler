/*
 * Copyright (C) 2019 Benjamin Rouxel <benjamin.rouxel@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef VANILLALINUXUSERLAND_H
#define VANILLALINUXUSERLAND_H

#include "Generator/Runtime/LinuxUserLand.hpp"

/*! \brief Interface for Linux-based Runtime Generator
 * 
 * Interface to select the correct template file for runtime generator. 
 * The generated code will be executed in User-Land
 * 
 */
class VanillaLinuxUserLand : public LinuxGenUserLand {
public:
    VanillaLinuxUserLand();
    std::string appendToPath() override;
    
protected:
    virtual std::string tpl_getBuildFile() override;
};

REGISTER_TARGETRUNTIME(VanillaLinuxUserLand, "user-space")

#endif
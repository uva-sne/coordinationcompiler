/*
 * Copyright (C) 2020 brouxel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* 
 * File:   CoordRuntime.cpp
 * Author: brouxel
 * 
 * Created on 4 juin 2020, 09:18
 */

#include "YasminRuntime.hpp"

using namespace std;

string YasminRuntime::tpl_getTask() {
    return 
#include "Generator/Runtime/Yasmin/CoordTask.tpl"
    ;
}
string YasminRuntime::tpl_getMain() {
    return 
#include "Generator/Runtime/Yasmin/main.tpl"
    ;
}
string YasminRuntime::tpl_getConfig() {
    return 
#include "Generator/Runtime/Yasmin/config.tpl"
    ;
}
string YasminRuntime::tpl_getBuildFile() {
    return
#include "Generator/Runtime/Yasmin/cmake.tpl"
    ;
}
string YasminRuntime::getMainFileName() {
    return "main"; 
}

string YasminRuntime::appendToPath() {
    return "-yasmin";
}

string YasminRuntime::tpl_getTaskInput(uint32_t nbtokens) {
    if(nbtokens < 2)
        return 
#include "Generator/Runtime/Yasmin/TaskInputTokens.tpl"
            ;
    return 
#include "Generator/Runtime/Yasmin/TaskInputArrayTokens.tpl"
            ;
}
string YasminRuntime::tpl_getTaskOutput(uint32_t nbtokens) {
    if(nbtokens < 2)
        return 
#include "Generator/Runtime/Yasmin/TaskOutputTokens.tpl"
            ;
    return 
#include "Generator/Runtime/Yasmin/TaskOutputArrayTokens.tpl"
            ;
}


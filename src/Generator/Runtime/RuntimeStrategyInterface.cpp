/*
 * Copyright (C) 2019 Benjamin Rouxel <benjamin.rouxel@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "RuntimeStrategyInterface.hpp"

using namespace std;

bool RuntimeStrategyInterface::is_SharedProcesses() {
    return (_runtime == CodeGeneration::runtime_container_e::FORK || _runtime == CodeGeneration::runtime_container_e::GROUPEDFORK || _runtime == CodeGeneration::runtime_container_e::FORKANDPTHREAD);
}
bool RuntimeStrategyInterface::is_GroupedRunnable() {
    return (_runtime == CodeGeneration::runtime_container_e::GROUPEDFORK || _runtime == CodeGeneration::runtime_container_e::GROUPEDPTHREAD || _runtime == CodeGeneration::runtime_container_e::FORKANDPTHREAD);
}

void RuntimeStrategyInterface::generatePath(const boost::filesystem::path &p, const boost::filesystem::path &codegendir) {
    _app_name = p.leaf().c_str();
    std::regex r("[^a-zA-Z0-9]");
    _app_name = regex_replace(_app_name, r, "");
    if(_conf_path.empty())
        _path = p.parent_path() / codegendir / boost::filesystem::path(_app_name + appendToPath());
    else
        _path = _conf_path;
    boost::filesystem::create_directories(_path);
}

IMPLEMENT_HELP(RuntimeStrategyInterface, 
        Possible configuration for Runtime Generators.\n
    - path, default coordFileDir/codegen/appname-runtime-suffix/: where to put
      the generated file.\n
    - os-scheduler, default SCHED_FIFO: OS scheduler to use, on linux different 
    schedulers are available, e.g. SCHED_FIFO, SCHED_RR, SCHED_OTHER, ...\n
    - default-priority, default 1: default priority to set on the runtime container.\n
    - synchro [none, semaphores, semaphore-per-chan, if-then-else], default none:
    Select the synchronisation mechanism for dependent tasks, or how 
    precedencies are enforced.\n
        HTMLINDENT+ none: no synchronisation added\n
        HTMLINDENT+ semaphores: predecessors unlock their successors, 1 semaphore per predecessor\n
        HTMLINDENT+ semaphore-per-chan: edge sources unlock their sink, 1 semaphore per edge in the graph\n
        HTMLINDENT+ if-then-else: successors check if a token is available and return if not.\n
    - runtime [sequential, fork, thread, grouped-thread, grouped-fork, fork-and-thread], default thread:
    Select the runtime container to execute tasks/components.\n
        HTMLINDENT+ sequential: all tasks are sequentially executed respecting dependencies\n
        HTMLINDENT+ fork: use a heavy process per task\n
        HTMLINDENT+ thread: use a light process per task\n
        HTMLINDENT+ grouped-thread: requires a mapping step, each core runs a thread
    and all tasks mapped to the core are sequentially executed within this thread\n
        HTMLINDENT+ grouped-fork: idem grouped-thread but using fork\n
        HTMLINDENT+ fork-and-thread: each graph of the app is a fork, then each task of the
    graph is a thread. Threads can then be mapped on different cores.\n
    - channel-type [shared-memory, socket, pipe], default shared-memory: 
    Select how data are transmitted between dependent tasks\n
        HTMLINDENT+ shared-memory: if using threads, statically allocate memory shared among them.
     If using forks, a shared segment of memory is dynamically allocated.\n
        HTMLINDENT+ socket (experimental): uses data socket stream to connect two tasks\n
        HTMLINDENT+ pipe: uses unix pipe to transmit data.\n
    - group-wait [sleep, busyloop, alarm], default sleep:
    When generating an off-line schedule, and using a grouped-* runtime container
     tasks mapped on the same core will execute sequentially. But, a waiting
     delay might be required between two tasks, depending on the schedule,
     this option selects how this delay is enforced.\n
        HTMLINDENT+ sleep: a call to sleep is performed\n
        HTMLINDENT+ busyloop: spin to wait to proper amount of time\n
        HTMLINDENT+ alarm: places an alarm and go to sleep.\n
)

void RuntimeStrategyInterface::setParams(const std::map<std::string,std::string>& args) {
    if(args.count("path"))
        _conf_path = args.at("path");
    if(args.count("synchro")) {
        if(args.at("synchro") == "none")
            _synchro = CodeGeneration::synchro_mechanism_e::NONE;
        else if(args.at("synchro") == "semaphores")
            _synchro = CodeGeneration::synchro_mechanism_e::SEMAPHORES;
        else if(args.at("synchro") == "semaphore-per-chan")
            _synchro = CodeGeneration::synchro_mechanism_e::SEMAPHORESPERCHAN;
        else if(args.at("synchro") == "if-then-else")
            _synchro = CodeGeneration::synchro_mechanism_e::IFTHENELSE;
        else
            throw CompilerException("code-gen", "Unknow synchronisation mechanism "+args.at("synchro")+" available: none, semaphores, semaphore-per-chan, if-then-else");
    }
    if(args.count("runtime")) {
        if(args.at("runtime") == "sequential")
            _runtime = CodeGeneration::runtime_container_e::SEQUENTIAL;
        else if(args.at("runtime") == "fork")
            _runtime = CodeGeneration::runtime_container_e::FORK;
        else if(args.at("runtime") == "thread")
            _runtime = CodeGeneration::runtime_container_e::PTHREAD;
        else if(args.at("runtime") == "grouped-thread")
            _runtime = CodeGeneration::runtime_container_e::GROUPEDPTHREAD;
        else if(args.at("runtime") == "grouped-fork")
            _runtime = CodeGeneration::runtime_container_e::GROUPEDFORK;
        else if(args.at("runtime") == "fork-and-thread")
            _runtime = CodeGeneration::runtime_container_e::FORKANDPTHREAD;
        else
            throw CompilerException("code-gen", "Unkwnown runtime: "+args.at("runtime")+" available: sequential, fork, thread, fork&thread");
    }
    if(args.count("channel-type")) {
        if(args.at("channel-type") == "shared-memory")
            _channel_type = CodeGeneration::channel_mechanism_e::SHAREDMEMORY;
        else if(args.at("channel-type") == "socket")
            _channel_type = CodeGeneration::channel_mechanism_e::SOCKET;
        else if(args.at("channel-type") == "pipe")
            _channel_type = CodeGeneration::channel_mechanism_e::PIPE;
        else
            throw CompilerException("code-gen", "Unkwnown channel-type: "+args.at("channel-type")+" available: shared-memory, socket(experimental), pipe");
    }

    if (args.count("group-wait")) {
        if (args.at("group-wait") == "sleep") {
            _sleep_mechanism = CodeGeneration::sleep_mechanism_e::SLEEP;
        } else if (args.at("group-wait") == "busyloop") {
            _sleep_mechanism = CodeGeneration::sleep_mechanism_e::BUSYLOOP;
        } else if (args.at("group-wait") == "alarm") {
            _sleep_mechanism = CodeGeneration::sleep_mechanism_e::ALARM;
        } else {
            throw std::runtime_error("Invalid waiting mechanism \"" + args.at("group-wait") + "\".");
        }
    }
    
    if(args.count("check_before_release")) {
        _check_before_release = (args.at("check_before_release") == CONFIG_TRUE);
    }

    if(args.count("os-scheduler"))
        _os_scheduler = args.at("os-scheduler");
    if(args.count("default-priority"))
        _default_priority = args.at("default-priority");
}
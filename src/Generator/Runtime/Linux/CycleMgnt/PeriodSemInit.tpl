/*
 * Copyright (C) 2019 Benjamin Rouxel <benjamin.rouxel@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
R"CPPMARKER(
// START PeriodSemInit.tpl
#if %SHARED% == 1
    __start_lock_%TASKNAME% = (sem_t*)mmap(NULL, sizeof(sem_t), PROT_READ | PROT_WRITE, MAP_SHARED | MAP_ANONYMOUS, -1, 0);
    status = (__start_lock_%TASKNAME% == NULL);
    tryExit(status, "mmap __start_lock_%TASKNAME%");
    status = mlock(__start_lock_%TASKNAME%, sizeof(sem_t));
    tryExit(status, "mlock __start_lock_%TASKNAME%");
#else
    __start_lock_%TASKNAME% = (sem_t*)malloc(sizeof(sem_t));
#endif
    status = sem_init(__start_lock_%TASKNAME%, %SHARED%, 0);
    tryExit(status, "sem_init(__start_lock_%TASKNAME%, %SHARED%, 0)");

    struct sigaction sa;
    struct sigevent te;
    struct itimerspec its;

    sa.sa_flags = SA_SIGINFO;
    sa.sa_sigaction = handler_timer_%TASKNAME%;
    sigemptyset(&sa.sa_mask);
    status = sigaction(SIGRTMIN+0, &sa, NULL);
    tryExit(status, "sigaction");

    //set timer, as now all threads are waiting for their predecessors to complete
    te.sigev_notify = SIGEV_SIGNAL;
    te.sigev_signo = SIGRTMIN+0;
    te.sigev_value.sival_ptr = &timer_%TASKNAME%;
    status = timer_create(CLOCK_REALTIME, &te, &timer_%TASKNAME%);
    tryExit(status, "timer_create(CLOCK_REALTIME, &te, &timer_%TASKNAME%)");

    its.it_interval.tv_sec = %PERIOD% / 1000000000;
    its.it_value.tv_sec = %PERIOD% / 1000000000 ;//* %NBITERATIONS%;
    its.it_interval.tv_nsec = %PERIOD% % 1000000000;
    its.it_value.tv_nsec = %PERIOD% % 1000000000;
    status = timer_settime(timer_%TASKNAME%, 0, &its, NULL);
    tryExit(status, "timer_settime(timer_%TASKNAME%, 0, &its, NULL) ");
// END PeriodSemInit.tpl
)CPPMARKER"
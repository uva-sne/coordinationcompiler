/*
 * Copyright (C) 2019 Benjamin Rouxel <benjamin.rouxel@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
R"CPPMARKER(
// START PeriodSemClean.tpl
timer_delete(timer_%TASKNAME%);
if(__start_lock_%TASKNAME% != NULL) {
    sem_destroy(__start_lock_%TASKNAME%);
#if %SHARED% == 1
    munlock(__start_lock_%TASKNAME%, sizeof(sem_t));
    munmap(__start_lock_%TASKNAME%, sizeof(sem_t));
#else
    free(__start_lock_%TASKNAME%);
#endif
    __start_lock_%TASKNAME% = NULL;
}
// END PeriodSemClean.tpl
)CPPMARKER"
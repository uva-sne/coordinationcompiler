/*
 * Copyright (C) 2019 Benjamin Rouxel <benjamin.rouxel@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
R"CPPMARKER(
// START TaskMgnt/SemInit.tpl
#if %SHARED% == 1
%TASKNAME%_lock = (sem_t*)mmap(NULL, sizeof(sem_t), PROT_READ | PROT_WRITE, MAP_SHARED | MAP_ANONYMOUS, -1, 0);
status = (%TASKNAME%_lock == NULL);
tryExit(status, "mmap %TASKNAME%_lock");
status = mlock(%TASKNAME%_lock, sizeof(sem_t));
tryExit(status, "mlock %TASKNAME%_lock");
#else
%TASKNAME%_lock = (sem_t*)malloc(sizeof(sem_t));
#endif
status = sem_init(%TASKNAME%_lock, %SHARED%, 0);
if(status != 0) {
    perror("sem_init(%TASKNAME%_lock, %SHARED%, 0)");
    return status;
}
// END TaskMgnt/SemInit.tpl
)CPPMARKER"
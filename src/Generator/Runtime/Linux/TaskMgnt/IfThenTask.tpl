/*
 * Copyright (C) 2019 Benjamin Rouxel <benjamin.rouxel@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
R"CPPMARKER(
// START TaskMgnt/IfThenTask.tpl
void __coord_%TASKNAME%_%VERSI%() {
    int status;

    %WAIT_PRED%

    %TASK_INPUTS%
    %TASK_CALLABLE%
    %TASK_OUTPUTS%

    %RELEASESUC%
}
// END TaskMgnt/IfThenTask.tpl
)CPPMARKER"

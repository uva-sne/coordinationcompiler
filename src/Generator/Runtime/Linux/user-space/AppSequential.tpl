/*
 * Copyright (C) 2019 Benjamin Rouxel <benjamin.rouxel@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
R"CPPMARKER(
// START user-space/AppSequential.tpl
#include <sys/timerfd.h>
#include <time.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <semaphore.h> 
#include <signal.h>
#include <limits.h>
#include <errno.h>
#include <sched.h>
#include <sys/wait.h>

%INCLUDES%

static inline int sem_wait_nointr(sem_t *sem) {
    while (sem_wait(sem)) {
        if (errno == EINTR) errno = 0;
        else return -1;
    }
    return 0;
}

static void sleep_until(long time) {
    struct timespec sleep, current;

    do {
        clock_gettime(CLOCK_MONOTONIC, &current);

        long current_ns = 1000000000L * current.tv_sec + current.tv_nsec;
        long diff_ns = time - current_ns;

        if (diff_ns < 0) break;

        sleep.tv_sec = diff_ns / 1000000000L;
        sleep.tv_nsec = diff_ns % 1000000000L;
    } while (nanosleep(&sleep, NULL) != 0);
}

void main_clean();

#define errExit(msg) { perror(msg); main_clean(); exit(EXIT_FAILURE); }
#define tryExit(var, msg) if(var != 0) {printf("-> %d <-\n",var); errno = var ; errExit(msg) }
#define STRINGIFY(X) #X
#define CONCAT(W,X,Y,Z) W ## X ## Y ## Z

%BUFFERS%

%LOCKDECL%

%THREAD_IMPLEM%

void main_exit(int);
int main_init() {
    int status = 0;

    signal(SIGINT, main_exit);
    signal(SIGILL, main_exit);
    signal(SIGABRT, main_exit);
    signal(SIGFPE, main_exit);
    signal(SIGSEGV, main_exit);
    signal(SIGTERM, main_exit);
    signal(SIGKILL, main_exit);
    signal(SIGSTOP, main_exit);
    signal(SIGTSTP, main_exit);

    %BUFFERSINIT%

    %LOCKINIT%

    %INITIALIZER%

    return status;
}

void main_clean() {
    int status = 0;

    %BUFFERSCLEAN%

    %LOCKCLEAN%

    %CLEANER%
}

void main_exit(int unused) {
    main_clean();
    exit(0);
}

int main(int argc, char **argv) 
{
    int status = 0;

    main_init();

    %MAIN_ARGS%

    %MAPPING%

    for(%ITERATION_LOOP%) {
        %THREAD_CREATE%
    }

    main_clean();
    printf("Done, ready to exit\n");
    return status;
}
// END user-space/AppSequential.tpl
)CPPMARKER"
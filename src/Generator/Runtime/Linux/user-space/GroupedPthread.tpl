/*
 * Copyright (C) 2019 Benjamin Rouxel <benjamin.rouxel@uva.nl>
 *                    Univeristy of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
R"CPPMARKER(
// START user-space/GroupedPthread.tpl
%DECL_PERIOD_LOCK%
%PERIOD_HANDLER%
void * __%TASKNAME%_%VERSI%(void* unused)
{
    int status;


    %INIT_PERIOD_LOCK%

    struct timespec start, current, sleep;
    clock_gettime(CLOCK_MONOTONIC, &start);
    long start_ns = 1000000000L * start.tv_sec + start.tv_nsec;

    for(%ITERATION_LOOP%) {
        %WAIT_NEXT_ITER%

        %SEQ_CALL%

        %CACHEFLUSH%
    }

    %CLEAN_PERIOD_LOCK%

    return NULL;
}
// END user-space/GroupedPthread.tpl
)CPPMARKER"

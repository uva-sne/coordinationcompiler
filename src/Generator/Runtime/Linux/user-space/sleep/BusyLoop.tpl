/*
 * Copyright (C) 2020 Stephen Nicholas Swatman <s.n.swatman@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
R"TEMPLATE(
// START user-space/sleep/BusyLoop.tpl
{
    long diff_ns;
    do {
        clock_gettime(CLOCK_MONOTONIC, &current);
        long current_ns = 1000000000L * current.tv_sec + current.tv_nsec;
        diff_ns = (__i__ * __period_%GROUPNAME% + __task_offset_%GROUPNAME%_%TASKNAME% + start_ns) - current_ns;
    } while (diff_ns > 0);
}
// END user-space/sleep/BusyLoop.tpl
)TEMPLATE"
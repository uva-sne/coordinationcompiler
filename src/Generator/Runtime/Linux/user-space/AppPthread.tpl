/*
 * Copyright (C) 2019 Benjamin Rouxel <benjamin.rouxel@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
R"CPPMARKER(
// START user-space/AppPthread.tpl
#ifndef _GNU_SOURCE
#   define _GNU_SOURCE
#endif
#include <sys/timerfd.h>
#include <time.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <pthread.h>
#include <semaphore.h> 
#include <signal.h>
#include <limits.h>
#include <errno.h>
#include <sched.h>

%INCLUDES%

static inline int sem_wait_nointr(sem_t *sem) {
    while (sem_wait(sem)) {
        if (errno == EINTR) errno = 0;
        else return -1;
    }
    return 0;
}

static void sleep_until(long time) {
    struct timespec sleep, current;

    do {
        clock_gettime(CLOCK_MONOTONIC, &current);

        long current_ns = 1000000000L * current.tv_sec + current.tv_nsec;
        long diff_ns = time - current_ns;

        if (diff_ns < 0) break;

        sleep.tv_sec = diff_ns / 1000000000L;
        sleep.tv_nsec = diff_ns % 1000000000L;
    } while (nanosleep(&sleep, NULL) != 0);
}

void main_clean();

#define errExit(msg) { perror(msg); main_clean(); exit(EXIT_FAILURE); }
#define tryExit(var, msg) if(var != 0) {printf("-> %d <-\n",var); errno = var ; errExit(msg) }
#define STRINGIFY(X) #X
#define CONCAT(W,X,Y,Z) W ## X ## Y ## Z

#define THREAD_INSTANTIATE(TASK, VERSI, SHORTNAME, PRIORITY, MAPPING) status = pthread_attr_init(&attr); \
    tryExit(status, STRINGIFY(TASK) " - pthread_attr_init(&attr)"); \
    status = pthread_attr_setinheritsched(&attr, PTHREAD_EXPLICIT_SCHED);\
    tryExit(status, STRINGIFY(TASK) " pthread_attr_setinheritsched ");\
    status = pthread_attr_setschedpolicy (&attr, %CONFIG_THREAD_SCHED%);\
    tryExit(status, STRINGIFY(TASK) " pthread_attr_setschedpolicy ");\
    if(PRIORITY > 0) {\
        struct sched_param param;\
        param.sched_priority = PRIORITY;\
        status = pthread_attr_setschedparam(&attr, &param); \
        tryExit(status, STRINGIFY(TASK) " - pthread_attr_setschedprio(&attr,&param)"); \
    }\
    if(MAPPING >= 0) {\
        cpu_set_t cpuset;\
        CPU_ZERO(&cpuset);\
        CPU_SET(MAPPING, &cpuset);\
        status = pthread_attr_setaffinity_np(&attr, sizeof(cpu_set_t), &cpuset); \
        tryExit(status, STRINGIFY(TASK) " - pthread_attr_setaffinity_np(&attr," STRINGIFY(MAPPING) ")");\
    }\
    status = pthread_create(CONCAT(&th_,TASK,_,VERSI), &attr, CONCAT(&__,TASK,_,VERSI), NULL); \
    tryExit(status, "pthread_create(&th_" STRINGIFY(TASK) "_" STRINGIFY(VERSI) ", &attr, &" STRINGIFY(TASK) "_" STRINGIFY(VERSI) ", NULL)\n"); \
    status = pthread_setname_np(CONCAT(th_,TASK,_,VERSI), STRINGIFY(SHORTNAME));\
    tryExit(status, STRINGIFY(TASK) " - pthread_setname_np(" STRINGIFY(TASK) ", " STRINGIFY(SHORTNAME) ")");\
    status = pthread_attr_destroy(&attr); \
    tryExit(status, STRINGIFY(TASK) " - pthread_attr_destroy(&attr)");

%DECL_PERIOD_LOCK%

%BUFFERS%

%LOCKDECL%

%THREAD_DECL%

%THREAD_IMPLEM%

%PERIOD_HANDLER%

int main_init() {
    int status = 0;

    %BUFFERSINIT%

    %LOCKINIT%

    %INITIALIZER%

    %INIT_PERIOD_LOCK%

    return status;
}

void main_clean() {
    %CLEAN_PERIOD_LOCK%

    %CLEANER%

    %LOCKCLEAN%
    
    %BUFFERSCLEAN%
}

void main_exit(int unused) {
    main_clean();
    exit(0);
}

int main(int argc, char **argv) 
{
    int status = 0;
    pthread_attr_t attr;
    struct sched_param schedattr;

    status = main_init();
    tryExit(status, "main_init()");

    // WARNING: If you experience an error at this point in the
    // program, specifically the program exiting with error code
    // 22, it is likely that your process does not have the
    // capabilities to set the requested scheduler. Try using
    // a non-real-time scheduler instead.

    %THREAD_CREATE%

    signal(SIGINT, main_exit);
    signal(SIGILL, main_exit);
    signal(SIGABRT, main_exit);
    signal(SIGFPE, main_exit);
    signal(SIGSEGV, main_exit);
    signal(SIGTERM, main_exit);
    signal(SIGKILL, main_exit);
    signal(SIGSTOP, main_exit);
    signal(SIGTSTP, main_exit);

    %THREAD_DELETE%

    main_clean();

    printf("Done, ready to exit\n");
    return status;
}
// END user-space/AppPthread.tpl
)CPPMARKER"
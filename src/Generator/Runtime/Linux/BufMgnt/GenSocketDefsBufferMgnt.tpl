/*
 * Copyright (C) 2019 Benjamin Rouxel <benjamin.rouxel@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
R"CPPRAWMARKER(
// START GenSocketDefsBufferMgnt.tpl
typedef struct {
    int sock = -1;
    char pathname[19] = SOCKET_NAME_TEMPLATE;
    char sockname[26];
    pollfd pollList[1];
} buffer_%BUFFERTYPENAME%_t;

static inline void init_buffer_%BUFFERTYPENAME%(buffer_%BUFFERTYPENAME%_t *buffer) {
    if(mkdtemp(buffer->pathname) == NULL) {
        printf("mkdtemp %BUFFERTYPENAME%: ");
        perror(strerror(errno));
        main_exit(1);
    }
    strncpy(buffer->sockname, buffer->pathname, 18);
    strncat(buffer->sockname, "/socket", 8);
    unlink(buffer->sockname);
    
    buffer->sock = socket(AF_UNIX, SOCK_STREAM, 0);
    if(buffer->sock == -1) {
        printf("socket %BUFFERTYPENAME%: ");
        perror(strerror(errno));
        main_exit(1);
    }
    
    struct sockaddr_un addr;
    memset(&addr, 0, sizeof(addr));
    addr.sun_family = AF_UNIX;
    strncpy(addr.sun_path, buffer->sockname, sizeof(addr.sun_path)-1);
    if(bind(buffer->sock, (struct sockaddr*)&addr, sizeof(addr)) == -1) {
        printf("bind %BUFFERTYPENAME%: ");
        perror(strerror(errno));
        main_exit(1);
    }

    buffer->pollList[0].fd = buffer->sock;
    buffer->pollList[0].events = POLLIN;
}
static inline void clean_buffer_%BUFFERTYPENAME%(buffer_%BUFFERTYPENAME%_t *buffer) {
    if(buffer->sock != -1)
        close(buffer->sock);
    unlink(buffer->sockname);
    rmdir(buffer->pathname);
}
static inline BOOL token_available_%BUFFERTYPENAME%(buffer_%BUFFERTYPENAME%_t *buffer) {
    buffer->pollList[0].revents = 0;
    int nEvents = poll(buffer->pollList, 1, 0);
    return (nEvents > 0 && buffer->pollList[0].revents & POLLIN);
}
static inline BOOL peek_%BUFFERTYPENAME%(buffer_%BUFFERTYPENAME%_t *buffer, %TYPE%* res, int offset=0) {
    assert(false && "not available for socket");
}
static inline BOOL pop_%BUFFERTYPENAME%(buffer_%BUFFERTYPENAME%_t *buffer, %TYPE%* res) {
    int c = read(buffer->sock, res, sizeof(%TYPE%));
    if(c < 0) {
        perror("pop_%BUFFERTYPENAME%");
        return FALSE;
    }
    return TRUE;
}
static inline void push_%BUFFERTYPENAME%(buffer_%BUFFERTYPENAME%_t *buffer, %TYPE% val) {
    int c = write(buffer->sock, &val, sizeof(%TYPE%));
    if(c < 0)
        perror("push_%BUFFERTYPENAME%");
}
static inline void insert_%BUFFERTYPENAME%(buffer_%BUFFERTYPENAME%_t *buffer, %TYPE% val, int offset=0) {
    assert(false && "not available for socket");
}
// END GenSocketDefsBufferMgnt.tpl
)CPPRAWMARKER"
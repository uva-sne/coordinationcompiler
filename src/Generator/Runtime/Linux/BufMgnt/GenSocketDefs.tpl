/*
 * Copyright (C) 2019 Benjamin Rouxel <benjamin.rouxel@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
R"CPPRAWMARKER(
// START GenSocketDefs.tpl
#pragma once
#ifndef SHAREDDEFS_H
#define SHAREDDEFS_H

#include <assert.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <errno.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/un.h>

%INCLUDES%

#ifdef __cplusplus
typedef bool BOOL;
#define TRUE true
#define FALSE false
#else
typedef short BOOL;
#define TRUE 1
#define FALSE 0
#endif

#ifndef SOCKET_NAME_TEMPLATE
#define SOCKET_NAME_TEMPLATE "/tmp/socketsXXXXXX"
#endif

extern void main_exit(int);

%TYPESTRUCT%

#endif //SHAREDDEFS_H
// END GenSocketDefs.tpl
)CPPRAWMARKER"
/*
 * Copyright (C) 2019 Benjamin Rouxel <benjamin.rouxel@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
R"CPPRAWMARKER(
// START GenPipeDefsBufferMgnt.tpl
typedef struct {
    int pipe[2]; // 0: read, 1: write
    pollfd pollList[1];
} buffer_%BUFFERTYPENAME%_t;

static inline void init_buffer_%BUFFERTYPENAME%(buffer_%BUFFERTYPENAME%_t *buffer) {
    if(pipe(buffer->pipe) == -1) {
        printf("pipe %BUFFERTYPENAME%: ");
        perror(strerror(errno));
        main_exit(1);
    }
    if(fcntl(buffer->pipe[0], F_SETFL, O_NONBLOCK)) {
        printf("pipe %BUFFERTYPENAME% set non-block: ");
        perror(strerror(errno));
        main_exit(1);
    }
    if(fcntl(buffer->pipe[1], F_SETFL, O_NONBLOCK)) {
        printf("pipe %BUFFERTYPENAME% set non-block: ");
        perror(strerror(errno));
        main_exit(1);
    }

    buffer->pollList[0].fd = buffer->pipe[0];
    buffer->pollList[0].events = POLLIN;
}
static inline void clean_buffer_%BUFFERTYPENAME%(buffer_%BUFFERTYPENAME%_t *buffer) {
    close(buffer->pipe[0]);
    close(buffer->pipe[1]);
}
static inline BOOL token_available_%BUFFERTYPENAME%(buffer_%BUFFERTYPENAME%_t *buffer) {
    buffer->pollList[0].revents = 0;
    int nEvents = poll(buffer->pollList, 1, 0);
    return (nEvents > 0 && buffer->pollList[0].revents & POLLIN);
}
static inline BOOL peek_%BUFFERTYPENAME%(buffer_%BUFFERTYPENAME%_t *buffer, %TYPE%* res, int offset=0) {
    assert(false && "not available for socket");
}
static inline BOOL pop_%BUFFERTYPENAME%(buffer_%BUFFERTYPENAME%_t *buffer, %TYPE%* res) {
    int c = read(buffer->pipe[0], res, sizeof(%TYPE%));
    if(c < 0) {
        perror("pop_%BUFFERTYPENAME%");
        return FALSE;
    }
    return TRUE;
}
static inline void push_%BUFFERTYPENAME%(buffer_%BUFFERTYPENAME%_t *buffer, %TYPE% val) {
    int c = write(buffer->pipe[1], &val, sizeof(%TYPE%));
    if(c < 0)
        perror("push_%BUFFERTYPENAME%");
}
static inline void insert_%BUFFERTYPENAME%(buffer_%BUFFERTYPENAME%_t *buffer, %TYPE% val, int offset=0) {
    assert(false && "not available for socket");
}
// END GenPipeDefsBufferMgnt.tpl
)CPPRAWMARKER"
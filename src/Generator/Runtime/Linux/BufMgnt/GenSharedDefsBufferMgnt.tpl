/* 
 * Copyright (C) 2017 Benjamin Rouxel <benjamin.rouxel@inria.fr> 
 *                    Irisa/INRIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
R"CPPRAWMARKER(
// START GenSharedDefsBufferMgnt.tpl
typedef struct {
   size_t head;
   size_t tail;
   %TYPE% buffer[%SIZE%+1];//add +1 to the size as a circular buffer of size 1 doesn't work
} buffer_%BUFFERTYPENAME%_t;
static inline void init_buffer_%BUFFERTYPENAME%(buffer_%BUFFERTYPENAME%_t *buffer) {
    buffer->head = buffer->tail = 0;
}
static inline void clean_buffer_%BUFFERTYPENAME%(buffer_%BUFFERTYPENAME%_t *buffer) {
}
static inline BOOL token_available_%BUFFERTYPENAME%(buffer_%BUFFERTYPENAME%_t *buffer) {
    return (buffer->tail != buffer->head);
}
static inline BOOL peek_%BUFFERTYPENAME%(buffer_%BUFFERTYPENAME%_t *buffer, %TYPE%* res, int offset) {
    if(buffer->tail == buffer->head)
        return FALSE;
    *res = (buffer->buffer)[(buffer->tail+offset)%(%SIZE%+1)];
    return TRUE;
}
static inline BOOL pop_%BUFFERTYPENAME%(buffer_%BUFFERTYPENAME%_t *buffer, %TYPE%* res) {
    if(buffer->tail == buffer->head)
        return FALSE;
    if(res != NULL)
        *res = (buffer->buffer)[buffer->tail];
    buffer->tail = (buffer->tail+1)%(%SIZE%+1);
    return TRUE;
}
static inline void push_%BUFFERTYPENAME%(buffer_%BUFFERTYPENAME%_t *buffer, %TYPE% val) {
    (buffer->buffer)[buffer->head] = val;
    buffer->head = (buffer->head+1)%(%SIZE%+1);
}
static inline void insert_%BUFFERTYPENAME%(buffer_%BUFFERTYPENAME%_t *buffer, %TYPE% val, int offset) {
    (buffer->buffer)[buffer->tail+offset] = val;
}
// END GenSharedDefsBufferMgnt.tpl
)CPPRAWMARKER"
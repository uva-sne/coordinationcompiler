/*
 * Copyright (C) 2019 Benjamin Rouxel <benjamin.rouxel@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
R"CPPMARKER(
// START init/App.tpl
#include <sys/timerfd.h>
#include <time.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <pthread.h>
#include <semaphore.h> 
#include <signal.h>
#include <limits.h>
#include <errno.h>

%INCLUDES%

static inline int sem_wait_nointr(sem_t *sem) {
    while (sem_wait(sem)) {
        if (errno == EINTR) errno = 0;
        else return -1;
    }
    return 0;
}

#define errExit(msg) { perror(msg); exit(EXIT_FAILURE); }
#define tryExit(var, msg) if(var != 0) {printf("-> %d <-\n",var); errExit(msg) }

#define THREAD_INSTANTIATE(THREAD_VAR, PRIORITY, THREAD_FUN) status = pthread_attr_init(&attr); \
    tryExit(status, "THREAD_VAR - pthread_attr_init(&attr)"); \
    status = pthread_attr_getschedparam(&attr, &schedattr); \
    tryExit(status, "THREAD_VAR - pthread_attr_getschedparam(&attr, &schedattr)"); \
    status = pthread_create(&THREAD_VAR, &attr, &THREAD_FUN, NULL); \
    tryExit(status, "pthread_create(&THREAD_VAR, &attr, &THREAD_FUN, NULL)\n"); \
    /* ack just created thread */ \
    status = sem_wait_nointr(&main_ack); \
    tryExit(status, "THREAD_VAR - sem_wait_nointr(&main_ack)"); \
    status = pthread_attr_destroy(&attr); \
    tryExit(status, "THREAD_VAR - pthread_attr_destroy(&attr)");

pthread_barrier_t main_barrier;
sem_t main_ack;
sem_t __start__lock;

%BUFFERS%

%THREAD_DECL%

%THREAD_IMPLEM%

static void handler_timer_start_iteration(int sig, siginfo_t *si, void *uc) {
    for(size_t i=0 ; i < %NBSOURCES% ; ++i)
        sem_post(&__start__lock);
}

void main_init() {
%BUFFERSINIT%

    %INITIALIZER%
}

int main(int argc, char **argv) 
{
    //delay start as init starts a bit too early
    sleep(5);
        
    int status;
    pthread_attr_t attr;
    struct sched_param schedattr;
    struct sigaction sa;
    struct sigevent te;
    struct itimerspec its;
    timer_t timer;

    status = pthread_barrier_init(&main_barrier, NULL, %NBTHREADS%+1); //+1 -> main
    tryExit(status, "pthread_barrier_init(&main_barrier, NULL, %NBTHREADS%+1)");

    status = sem_init(&main_ack, 0, 0);
    tryExit(status, "sem_init(&main_ack, 0, 0)");
    status = sem_init(&__start__lock, 0, 0);
    tryExit(status, "sem_init(&__start__lock, 0, 0)");

    %THREAD_CREATE%

    main_init();

    // release all tasks and sources will be waiting for the next period
    status = pthread_barrier_wait(&main_barrier);
    if(status != 0 && status != PTHREAD_BARRIER_SERIAL_THREAD)
        errExit("pthread_barrier_wait(&main_barrier)");

    sa.sa_flags = SA_SIGINFO;
    sa.sa_sigaction = handler_timer_start_iteration;
    sigemptyset(&sa.sa_mask);
    status = sigaction(SIGRTMIN, &sa, NULL);
    tryExit(status, "sigaction");

    //set timer, as now all threads are waiting for their predecessors to complete
    te.sigev_notify = SIGEV_SIGNAL;
    te.sigev_signo = SIGRTMIN;
    te.sigev_value.sival_ptr = &timer;
    status = timer_create(CLOCK_REALTIME, &te, &timer);
    tryExit(status, "timer_create(CLOCK_REALTIME, &te, &timer)");

#if %PERIOD% < INT_MAX
    its.it_interval.tv_sec = 0;
    its.it_value.tv_sec = 0;
    its.it_interval.tv_nsec = %PERIOD%;
    its.it_value.tv_nsec = %PERIOD% * %NBITERATIONS%;
#else
    its.it_interval.tv_sec = %PERIOD% / 1000000000;
    its.it_value.tv_sec = (%PERIOD% / 1000000000) * %NBITERATIONS%;
    its.it_interval.tv_nsec = 0;
    its.it_value.tv_nsec = 0;
#endif
    status = timer_settime(timer, 0, &its, NULL);
    tryExit(status, "timer_settime(timer, 0, &its, NULL) ");

    %THREAD_DELETE%

    status = timer_delete(timer);
    tryExit(status, "timer_delete(timer)");

    status = pthread_barrier_destroy(&main_barrier);
    tryExit(status, "pthread_barrier_destroy(&main_barrier)");

    printf("Done, ready to exit\n");
    //init should never stop
    while(2+2 == 4) {}
    return status;
}
// END init/App.tpl
)CPPMARKER"
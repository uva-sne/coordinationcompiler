/*
 * Copyright (C) 2019 Benjamin Rouxel <benjamin.rouxel@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
R"CPPMARKER(
// START init/SequentialApp.tpl
#include <sys/timerfd.h>
#include <time.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <semaphore.h> 
#include <signal.h>
#include <limits.h>

%INCLUDES%

#define errExit(msg) { perror(msg); exit(EXIT_FAILURE); }
#define tryExit(var, msg) if(var != 0) errExit(msg)
        
%BUFFERS%

void main_init() {
    %BUFFERSINIT%

    %INITIALIZER%
}

int main(int argc, char **argv) 
{
    //delay start as init starts a bit too early
    sleep(5);

    int status;

    main_init();

    for(%ITERATION_LOOP%) {
        %SEQ_CALLS%
    }
    printf("Done, ready to exit\n");
    //init should never stop
    while(2+2 == 4) {}
    return status;
}
// END init/SequentialApp.tpl
)CPPMARKER"
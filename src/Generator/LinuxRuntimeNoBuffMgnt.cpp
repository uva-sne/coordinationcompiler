/*
 * Copyright (C) 2019 Benjamin Rouxel <benjamin.rouxel@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "LinuxRuntimeNoBuffMgnt.hpp"

using namespace std;

LinuxRuntimeNoBuffMgnt::LinuxRuntimeNoBuffMgnt() : LinuxRuntime() {
    delete buffmgnt;
    buffmgnt = new NoBufferMgnt;
}
const std::string LinuxRuntimeNoBuffMgnt::get_uniqid_rtti() const {
    return "generator-linux-nobuff-mgnt"; 
}

IMPLEMENT_HELP(LinuxRuntimeNoBuffMgnt, {msg += ccode->help()
            +strategy->help()
            +syncmgnt->help()
            +mk->help();
},
    Generate code to run the app on GNU/Linux, but leave the buffer management
    to the user code.\n
    HTMLINDENT- env [user-space, init, kernel-space]: Select in what environment 
    the application should be running.\n
    HTMLINDENT- user-space: executed in user space.\n
    HTMLINDENT- init: replace the init process with this app, executed at boot time.\n
    HTMLINDENT- kernel-space (soon): the app is built as a kernel module.\n
    @see CCode::help\n
    @see RuntimeInterfaceStrategy::help\n
    @see DataExchangeSynchro::help\n
    @see BuildFile::help
)

void LinuxRuntimeNoBuffMgnt::generate(const boost::filesystem::path& fn) {
    if(strategy == nullptr)
        throw CompilerException("codegen", "Unspecified target os");
    
    buffmgnt->init(tg, conf, sched, strategy);
    syncmgnt->init(tg, conf, sched, strategy);
    mk->init(tg, conf, sched, strategy);
    ccode->init(tg, conf, sched, strategy);
    
    strategy->generatePath(fn, ccode->codegendir());

    generateMainCode();
    mk->generate(ccode->includes());
}
void LinuxRuntimeNoBuffMgnt::forward_params(const std::map<std::string, std::string> &args) {
    LinuxRuntime::forward_params(args);
    if(strategy != nullptr)
        strategy->synchro(CodeGeneration::synchro_mechanism_e::NONE);
}
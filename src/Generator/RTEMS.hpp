/*
 * Copyright (C) 2020 Benjamin Rouxel <benjamin.rouxel@uva.nl>
 *                     University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef RTEMS_H
#define RTEMS_H

#include "Generator.hpp"
#include "Runtime/RuntimeStrategyInterface.hpp"
#include "Utils/Log.hpp"
#include "Utils/Graph.hpp"
#include <boost/filesystem.hpp>
#include "CodeGen/CodeGenerationPatterns.hpp"
#include "CodeGen/CodeGenerationEnums.hpp"
#include "Generator/CodeGen/CHelpers/BufferMgnt.hpp"
#include "Utils/Log.hpp"
#include "Generator/Runtime/RTEMSRuntime.hpp"
#include "Generator/CodeGen/CHelpers/Makefile.hpp"
#include "CodeGen/CCode.hpp"




class RTEMSgenerator : public Generator {
protected:
    RTEMSRuntime *strategy = nullptr;
    BufferMgnt *buffmgnt;
    Makefile *make;
    CCode *ccode;
    
    std::string rtmkp = "";
    std::string genpath = "";
    std::string additional_lib = "";
public:
    RTEMSgenerator();
    virtual ~RTEMSgenerator() {}
    virtual void forward_params(const std::map<std::string, std::string> &args) override;
    virtual std::string help() override;
    const std::string get_uniqid_rtti() const override { return "generator-rtems"; }

    void generate(const boost::filesystem::path& basename) override;
    
protected:
    virtual void generateBufferMgnt();
    virtual void generateInit();
    virtual void generateConfig();
    virtual std::string generateTasks();
    virtual std::string generateSetupTasks();
    virtual std::string generateDeclTasks();
};

REGISTER_GENERATOR(RTEMSgenerator, "rtems")

#endif /* RTEMS_H */


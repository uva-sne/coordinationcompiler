/*
 * Copyright (C) 2020 Wouter Loeve <wouterloeve0@gmail.com>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "FTSimulatorCodeGen.hpp"
#include <boost/format.hpp>
#include <boost/algorithm/string.hpp> 
#include <algorithm>

using namespace boost::algorithm;
using namespace std;

IMPLEMENT_HELP(FTSimulatorGen, 
    Code generation for the fault-tolerance simulation runtime
)

const string FTSimulatorGen::get_uniqid_rtti() const {
    return "generator-FTCodegen";
}

void FTSimulatorGen::generate(const boost::filesystem::path &fn) {
    ofstream out(fn.string());
    out << "#include \"generated.h\"" << endl;
    out << "#include \"computation.h\"" << endl << endl;

    generateStructs(&out);
    generateTaskGraph(&out);
    generateCompFunc(&out);
    generateStoreOutput(&out);
    generateTakeOutput(&out);
    generateSetError(&out);
    generateCopyCompData(&out);
    generateAllocOutput(&out);
}

void FTSimulatorGen::generate() {
    generate(conf->files.output_folder / boost::filesystem::path(conf->files.coord_basename.string() + ".c"));
}

void FTSimulatorGen::generateStructs(ofstream *out) {
    for (Task *i : tg->tasks_it()) {
        if(i->inputs().size() > 0) {
            *out << "typedef struct {" << endl;
            for (Connector *c : i->inputs()) {
                *out << generateConnectorStruct(c) << ";" << endl;
            }
            *out << "bool _error;" << endl;
            *out << "} " + getStructName(i, "in") + ";" << endl;
        }
        *out << endl;
        if(i->outputs().size() > 0) {
            *out << "typedef struct {" << endl;
            for (Connector *c : i->outputs()) {
                *out << generateConnectorStruct(c) << ";" << endl;
            }
            *out << "bool _error;" << endl;
            *out << "} " + getStructName(i, "out") + ";" << endl;
        }
        *out << endl;
    }
}

void FTSimulatorGen::generateTaskGraphEdge(ofstream *out, Task *i) {
    if(i->predecessor_tasks().size() == 0) {
        *out << boost::format{"task_graph_add_root(g, %s);"} % getGraphStructName(i) << endl;
    }
    else {
        int index = 0;
        for (Task::dep_t pair : i->predecessor_tasks()) {
            std::map<string, int> outports = getNumOutports(Utils::flatmapvalues(pair.first->successor_tasks(), Task::connector_from));
            for (Connection *conn : pair.second) {
                string prev = getGraphStructName(pair.first);
                string cur = getGraphStructName(i);
                string inport = conn->to()->id();
                string type = connectorTypeToString(conn->to());
                int multiplicity = conn->to()->tokens();

                string outport = conn->from()->id();
                int numOutports = outports[outport];
                string func_name = "task_graph_add_comp";

                if (index > 0) {
                    func_name = "task_graph_add_edge";
                }
                *out << boost::format{"%s(g, %s, %s, \"%s\", %i, \"%s\", %i);"} % func_name % prev % cur % inport % multiplicity % outport % numOutports << endl;
                index++;
            }
        }
    }
}

void FTSimulatorGen::generateCopyCompData(ofstream *out) {
    *out << "void *copy_comp_data(void *data, int compId) {" << endl;
    *out << "\tswitch(compId) {" << endl;
    int index = 0;
    bool hasInputs = false;
    string inputStructName = "";
    for (Task *i : tg->tasks_it()) {
        inputStructName = getStructName(i, "in");
        hasInputs = (i->inputs().size() > 0);
        *out << boost::format{"\tcase %i: {"} % index << endl;
        if(hasInputs) {
            *out << boost::format{"\t\t%s *in = (%s *) data;"} % inputStructName % inputStructName << endl;
            *out << boost::format{"\t\t%s *in_copy = malloc(sizeof(%s));"} % inputStructName % inputStructName << endl;
            *out << boost::format{"\t\tassert(%s);"} % "in" << endl;

            // I would have rather named it something with the datatype specified in the coordination language, but that is not available in the systemmodel.
            for (Connector *in : i->inputs()) {
                if(isPtrType(in)) {
                    *out << boost::format{"\t\tin_copy->%s = %s(in->%s);"} % in->id() % getCopyName(in->token_type()) % in->id() << endl;
                }
                else {
                    *out << boost::format{"\t\tin_copy->%s = *((%s*) %s(&in->%s));"} % in->id() % in->token_type() % getCopyName(in->token_type()) % in->id() << endl;
                }

            }

            *out << "\t\treturn in_copy;" << endl;
        }
        else {

            *out << "\t\treturn NULL;" << endl;
        }
        *out << boost::format{"\t}"} << endl;
        index++;
    }
    *out << "\t}" << endl;
    *out << "return NULL;" << endl;
    *out << "}" << endl;
}

void FTSimulatorGen::generateAllocOutput(ofstream *out) {
    *out << "void *alloc_output_struct(int compId) {" << endl;
    *out << "\tswitch(compId) {" << endl;
    int index = 0;
    bool hasOutputs = false;
    string outputStructName = "";
    for (Task *i : tg->tasks_it()) {
        outputStructName = getStructName(i, "out");
        hasOutputs = (i->outputs().size() > 0);
        *out << boost::format{"\tcase %i: {"} % index << endl;
        if(hasOutputs) {
            *out << boost::format{"\t\t%s *out = malloc(sizeof(%s));"} % outputStructName % outputStructName << endl;
            *out << boost::format{"\t\tassert(%s);"} % "out" << endl;
            *out << "out->_error = false;" << endl;
            *out << "\t\treturn out;" << endl;
        }
        else {
            *out << "\t\treturn NULL;" << endl;
        }
        *out << boost::format{"\t}"} << endl;
        index++;
    }
    *out << "\t}" << endl;
    *out << "return NULL;" << endl;
    *out << "}" << endl;
}

void FTSimulatorGen::generateSetError(ofstream *out) {
    *out << "void set_error(TaskStruct *currentTask) {" << endl;
    *out << "\tint compId = currentTask->componentId;" << endl;
    *out << "\tswitch(compId) {" << endl;
    int index = 0;
    bool hasOutputs = false;
    string inputStructName = "";
    string outputStructName = "";
    for (Task *i : tg->tasks_it()) {
        outputStructName = getStructName(i, "out");
        hasOutputs = (i->outputs().size() > 0);
        *out << boost::format{"\tcase %i: {"} % index << endl;
        if(hasOutputs) {
            *out << boost::format{"\t\t%s *out = (%s *) currentTask->outputData;"} % outputStructName % outputStructName << endl;
            *out << "\t\tout->_error = true;" << endl;
        }
        *out << "\t\tbreak;" << endl;
        *out << boost::format{"\t}"} << endl;
        index++;
    }
    *out << "\t}" << endl;
    *out << "}" << endl;
}

void FTSimulatorGen::generateProfiles(ofstream *out) {
    string delimiter = "_";
    size_t pos = 0;
    string s;
    string token;
    for (Task *i : tg->tasks_it()) {
        string gs_name = getGraphStructName(i);
        for (std::pair<std::string, ProfileOption> p : i->profile().getSettings()) {
            s = p.first;
            if(p.first == "nModular" || p.first == "checkpoint" ||
                    p.first == "nVersion" || p.first == "standby") {

                *out << boost::format{"profile_%s_init(%s->profile);"} % p.first % gs_name << endl;
            }
            else {
                while ((pos = s.find(delimiter)) != std::string::npos) {
                    token = s.substr(0, pos);
                    s.erase(0, pos + delimiter.length());
                    break;
                }

                if(token.empty()) {
                    *out << boost::format{"profile_add(%s->profile, \"%s\", \"%s\");"} % gs_name % p.first % p.second.value << endl;
                }
                else {
                    string value = p.second.value;
                    if(value.back() == '%') {
                        value.pop_back();
                    }
                    *out << boost::format{"profile_%s_add(%s->profile, \"%s\", \"%s\");"} % token % gs_name % s % value << endl;
                }
            }
            token = "";
        }
    }
}

void FTSimulatorGen::generateTaskGraph(ofstream *out) {
    *out << "task_graph *generate_task_graph() {" << endl;

    *out << boost::format{"task_graph *g = task_graph_init(%i);"} % tg->tasks().size() << endl;

    int id = 0;
    for (Task *i : tg->tasks_it()) {
        string inportsId = "NULL";
        // Generate Ports
        if(i->inputs().size() > 0) {
            inportsId = boost::str(boost::format{"ports%i"}
            % id);
            *out << boost::format{"char *%s[] = {"} % inportsId;
            for (Connector *c : i->inputs()) {
                *out << boost::format{"\"%s\""} % c->id();
                if(c != i->inputs()[i->inputs().size() - 1]) {
                    *out << +", ";
                }
            }
            *out << "};" << endl;
        }

        *out << boost::format{"GraphStruct *%s = task_graph_create_gs(\"%s\", %i, %s, %i);"}
        % getGraphStructName(i) % i->id() % id % inportsId % i->inputs().size() << endl;


        id++;
        *out << endl;
    }

    generateProfiles(out);

    for (Task *i : tg->tasks_it()) {
        generateTaskGraphEdge(out, i);
    }

    *out << "return g;" << endl;
    *out << "}" << endl;
}

void FTSimulatorGen::generateStoreOutput(ofstream *out) {
    *out << "void store_output(MainStruct *minfo, TaskStruct *currentTask, threadTimeStruct *time) {" << endl;
    *out << "\tint compId = currentTask->componentId;" << endl;
    *out << "\tassert(compId <= minfo->taskGraph->compsIndex);" << endl;
    *out << "\tGraphStruct *finished = minfo->taskGraph->taskList[compId];" << endl << endl;
    *out << "\tswitch(compId) {" << endl;
    int index = 0;
    bool hasOutputs = false;
    string inputStructName = "";
    string outputStructName = "";
    for (Task *i : tg->tasks_it()) {
        inputStructName = getStructName(i, "in");
        outputStructName = getStructName(i, "out");
        hasOutputs = (i->outputs().size() > 0);
        *out << boost::format{"\tcase %i: {"} % index << endl;
        if(hasOutputs) {
            *out << boost::format{"\t\t%s *out = (%s *) currentTask->outputData;"} % outputStructName % outputStructName << endl << endl;
            vector<Connector *> outports = removeDuplicateConns(i->outputs());
            *out << "\t\tif(out->_error) {" << endl;
            for (Connector *suc : outports) {

                *out << boost::format{"\t\t\ttask_graph_add_to_buffer(finished, minfo->errorToken, \"%s\", %s, time);"} % suc->id() % getCopyName(suc->token_type()) << endl;
            }
            *out << "\t\t} else {" << endl;
            for (Connector *suc : outports) {
                string var = boost::str(boost::format{"&(out->%s)"}
                % suc->id());
                if(isPtrType(suc)) {
                    var = boost::str(boost::format{"out->%s"}
                    % suc->id());
                }
                *out << boost::format{"\t\t\ttask_graph_add_to_buffer(finished, %s, \"%s\", %s, time);"} % var % suc->id() % getCopyName(suc->token_type()) << endl;
            }
            *out << "\t\t}" << endl;
        }
        *out << "\t\tbreak;" << endl;
        *out << boost::format{"\t}"} << endl;
        index++;
    }
    *out << "\t}" << endl;
    *out << "}" << endl;
}

void FTSimulatorGen::generateTakeOutput(ofstream *out) {
    *out << "TaskStruct *take_output(GraphStruct *node, list_linked *edges, MainStruct *mainInfo, void *errorToken, bool checkpoint) {" << endl;
    *out << "\tint compId = node->componentId;" << endl;
    *out << "\tTaskStruct *task = NULL;" << endl << endl;
    *out << "\tbool error = false;" << endl;
    *out << "\tswitch(compId) {" << endl;
    int index = 0;
    bool hasInputs = false;
    bool hasOutputs = false;
    string inputStructName = "";
    string outputStructName = "";
    string invalidateOutput = "";
    for (Task *i : tg->tasks_it()) {
        inputStructName = getStructName(i, "in");
        outputStructName = getStructName(i, "out");
        hasInputs = (i->inputs().size() > 0);
        hasOutputs = (i->outputs().size() > 0);
        if(hasOutputs) {
            invalidateOutput = "out->_error = true;";
        }
        else {
            invalidateOutput = "";
        }

        *out << boost::format{"\tcase %i: {"} % index << endl;
        string inputData = "NULL";
        string outputData = "NULL";
        if(hasOutputs) {
            outputData = "out";
            *out << boost::format{"\t\t%s *out = malloc(sizeof(%s));"}
            % outputStructName % outputStructName << endl;
            *out << boost::format{"\t\tassert(%s);"} % "out" << endl;
            *out << "\t\tout->_error = false;" << endl;
        }
        if(hasInputs) {
            inputData = "in";
            *out << boost::format{"\t\t%s *in = malloc(sizeof(%s));"}
            % inputStructName % inputStructName << endl;
            *out << boost::format{"\t\tassert(%s);"} % inputData << endl;

            for (Connector *c : i->inputs()) {
                *out << boost::format{"\t\tvoid **%s_var = task_graph_get_data_by_port(\"%s\", edges, checkpoint);"} % c->id() % c->id() << endl;
                *out << boost::format{"\t\tassert(%s_var);"} % c->id() << endl;
                for (uint32_t i = 0; i < c->tokens(); i++) {
                    *out << boost::format{"\t\tif (%s_var == errorToken) {error = true; %s}"} % c->id() % invalidateOutput << endl;
                    *out << boost::format{"\t\tif (%s_var[%i] == errorToken) {error = true; %s}"} % c->id() % i % invalidateOutput << endl;
                    if(isPtrType(c)) {
                        *out << boost::format{"\t\tin->%s = *((%s *) %s_var); "} % c->id() % c->token_type() % c->id() << endl;
                    }
                    else {
                        *out << boost::format{"\t\tin->%s = *((%s *) %s_var[%i]); "} % c->id() % c->token_type() % c->id() % i << endl;
                    }
                }
            }
        }

        *out << boost::format{"\t\ttask = task_graph_create_task(%i, \"%s\", %s, %s);"}
        % index % getImplementationName(i) % inputData % outputData << endl;
        *out << "\t\tbreak;" << endl;
        *out << boost::format{"\t}"} << endl;
        index++;
    }
    *out << "\t}" << endl;
    *out << "\tif (error) { store_output(mainInfo, task, mainInfo->diagnostics->time); return NULL; }" << endl;
    *out << "\treturn task;" << endl;
    *out << "}" << endl;
}

bool FTSimulatorGen::isPtrType(Connector *c) {
    if(c->token_type().find("*") == std::string::npos) {
        return false;
    }
    return true;
}

void FTSimulatorGen::generateCompFunc(ofstream *out) {
    *out << "void compFunc(TaskStruct *currentTask) {" << endl;
    *out << "\tswitch(currentTask->componentId) {" << endl;
    int index = 0;
    bool hasInputs = false;
    bool hasOutputs = false;
    string inputStructName = "";
    string outputStructName = "";
    for (Task *i : tg->tasks_it()) {
        inputStructName = getStructName(i, "in");
        outputStructName = getStructName(i, "out");
        hasInputs = (i->inputs().size() > 0);
        hasOutputs = (i->outputs().size() > 0);
        *out << boost::format{"\tcase %i: {"} % index << endl;

        if(hasInputs) {
            *out << boost::format{"\t\t%s *in = (%s *) currentTask->inputData;"} % inputStructName % inputStructName << endl;
        }
        if(hasOutputs) {
            *out << boost::format{"\t\t%s *out = (%s *) currentTask->outputData;"} % outputStructName % outputStructName << endl;
        }
        *out << boost::format{"\t\t%s("} % getImplementationName(i);

        std::map<Task*, Connector*>::size_type connIndex = 0;
        for (Task::dep_t pair : i->predecessor_tasks()) {
            for (Connection *conn : pair.second) {
                *out << boost::format{"in->%s"} % conn->to()->id();
                if (connIndex < i->predecessor_tasks().size() - 1 || hasOutputs) {
                    *out << ", ";
                }
                connIndex++;
            }
        }
        connIndex = 0;
        std::vector<Connector*> outports = removeDuplicateConns(i->outputs());

        for (Connector * elc : outports) {
            *out << boost::format{"&out->%s"} % elc->id();
            if(connIndex < i->outputs().size() - 1) {
                *out << ", ";
            }
            connIndex++;
        }


        *out << ");" << endl;

        *out << "\t\tbreak;" << endl;
        *out << boost::format{"\t}"} << endl;
        index++;
    }
    *out << "\t}" << endl;
    *out << "}" << endl;
}

std::map<string, int> FTSimulatorGen::getNumOutports(vector<Connector *> m) {
    std::map<string, int> ids;
    std::map<string, int>::iterator f;
    for (vector<Connector *>::iterator it = m.begin(); it != m.end(); it++) {
        f = ids.find((*it)->id());
        if (f != ids.end()) {
            ids[(*it)->id()]++;
        } else {
            ids.insert({(*it)->id(), 1});
        }
    }

    return ids;
}

string FTSimulatorGen::tokenTypeToId(string tokenType) {
    return replace_all_copy(replace_all_copy(tokenType, "*", "ptr"), " ", "_");
}

vector<Connector*> FTSimulatorGen::removeDuplicateConns(vector<Connector*> outports) {
    auto it = std::unique(outports.begin(), outports.end(), [](Connector *v1, Connector * v2) {
        // comparison function that returns true or false
        return v1->id() == v2->id();
    });
    outports.erase(it, outports.end());
    return outports;
}

string FTSimulatorGen::getGraphStructName(Task *i) {
    return boost::str(boost::format{"%s_gs"} % i->id());
}

string FTSimulatorGen::connectorTypeToString(Connector *e) {
    Connector::type_e type = e->type();
    if(type == Connector::type_e::REGULAR) {
        return "Simple";
    }
    else if(type == Connector::type_e::DUPLICATE) {
        // does this work?
        return "Duplicate";
    }
    return "Simple";
}

string FTSimulatorGen::generateConnectorStruct(Connector *c) {
    return boost::str(boost::format{"%s %s"} % c->token_type() % c->id());
}

string FTSimulatorGen::getStructName(Task *i, string type) {
    return boost::str(boost::format{"%s_%s_struct"} % i->id() % type);
}

string FTSimulatorGen::getCopyName(string token_type) {
    return boost::str(boost::format{"copy_%s"} % tokenTypeToId(token_type));
}

string FTSimulatorGen::getImplementationName(Task *i) {
    return i->id();
}

string FTSimulatorGen::addQuotes(string s) {
    return boost::str(boost::format{"\"%s\""} % s);
}
/* 
 * Copyright (C) 2019 Benjamin Rouxel <benjamin.rouxel@uva.nl> 
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PATTERNS_H
#define PATTERNS_H

constexpr static char txtinc[] = "%INCLUDES%";
constexpr static char txtcinc[] = "%C_INCLUDES%";
constexpr static char txtcppinc[] = "%CPP_INCLUDES%";
constexpr static char txtdecl[] = "%THREAD_DECL%";
constexpr static char txtinvoke[] = "%THREAD_CREATE%";
constexpr static char txtdelete[] = "%THREAD_DELETE%";
constexpr static char txtimplem[] = "%THREAD_IMPLEM%";
constexpr static char txtperiod[] = "%PERIOD%";
constexpr static char txtiteration[] = "%ITERATION_LOOP%";
constexpr static char txtnbiteration[] = "%NBITERATIONS%";
constexpr static char txtcallable[] = "%TASK_CALLABLE%";
constexpr static char txtcallin[] = "%TASK_INPUTS%";
constexpr static char txtcallout[] = "%TASK_OUTPUTS%";
constexpr static char txtwaitpred[] = "%WAIT_PRED%";
constexpr static char txttaskname[] = "%TASKNAME%";
constexpr static char txtgroupname[] = "%GROUPNAME%";
constexpr static char txtconname[] = "%CONN%";
constexpr static char txtvarconname[] = "%VARCONN%";
constexpr static char txtversionname[] = "%VERSI%";
constexpr static char txtshortname[] = "%SHORTNAME%";
constexpr static char txtnbthreads[] = "%NBTHREADS%";
constexpr static char txtnbsuc[] = "%NBSUC%";
constexpr static char txtnbtokens[] = "%NBTOKENS%";
constexpr static char txtreleasesuc[] = "%RELEASESUC%";
constexpr static char txtreleasecond[] = "%RELEASECOND%";
constexpr static char txtinit[] = "%INITIALIZER%";
constexpr static char txtcleaner[] = "%CLEANER%";
constexpr static char txtnbsrcs[] = "%NBSOURCES%";
constexpr static char txtobject[] = "%OBJECT%";
constexpr static char txtextraobject[] = "%EXTRAOBJECT%";
constexpr static char txtsrc[] = "%SRC%";
constexpr static char txtcompopt[] = "%COMPOPT%";
constexpr static char txtcc[] = "%CC%";
constexpr static char txtarch[] = "%ARCH%";
constexpr static char txtappname[] = "%APPNAME%";
constexpr static char txtlinkopt[] = "%LINKOPT%";
constexpr static char txtrules[] = "%RULES%";
constexpr static char txtcodelang[] = "%CODELANG%";
constexpr static char txtupcodelang[] = "%UPCODELANG%";
constexpr static char txtkerneldir[] = "%KERNELDIR%";
constexpr static char txtpriority[] = "%PRIORITY%";

constexpr static char txtcache[] = "%CACHEFLUSH%";
constexpr static char txtseqcall[] = "%SEQ_CALL%";

constexpr static char txtcommunications[] = "%TYPESTRUCT%";
constexpr static char txtbufftypename[] = "%BUFFERTYPENAME%";
constexpr static char txtcommtype[] = "%TYPE%";
constexpr static char txtcommsize[] = "%SIZE%";
constexpr static char txtbuffers[] = "%BUFFERS%";
constexpr static char txtbufinit[] = "%BUFFERSINIT%";
constexpr static char txtbufclean[] = "%BUFFERSCLEAN%";
constexpr static char txtlockinit[] = "%LOCKINIT%";
constexpr static char txtlockclean[] = "%LOCKCLEAN%";
constexpr static char txtlocks[] = "%LOCKDECL%";

constexpr static char txtperiodinit[] = "%INIT_PERIOD_LOCK%";
constexpr static char txtperiodunlock[] = "%PERIOD_HANDLER%";
constexpr static char txtperiodlock[] = "%DECL_PERIOD_LOCK%";
constexpr static char txtperiodclean[] = "%CLEAN_PERIOD_LOCK%";
constexpr static char txtperiodwait[] = "%WAIT_NEXT_ITER%";
constexpr static char txttaskwait[] = "%WAIT_NEXT_TASK%";
constexpr static char txtmapping[] = "%MAPPING%";
constexpr static char txtsched[] = "%CONFIG_THREAD_SCHED%";
constexpr static char txttaskcall[] = "%TASKS_CALL%";

constexpr static char txtshared[] = "%SHARED%"; //shared among processes, thread either
constexpr static char txtmainargs[] = "%MAIN_ARGS%";

constexpr static char txtdatanodes[] = "%DATA_NODES%";
constexpr static char txtactornodes[] = "%ACTOR_NODES%";
constexpr static char txtedges[] = "%EDGES%";
constexpr static char txtattributes[] = "%ATTRIBUTES%";
constexpr static char txthardwareattributes[] = "%HARDWARE_ATTRIBUTES%";
constexpr static char txtwrappers[] = "%WRAPPERS%";
constexpr static char txthardwarenodes[] = "%HARDWARE_NODES%";
constexpr static char txtcompositetypes[] = "%COMPOSITE_TYPES%";


#endif /* PATTERNS_H */


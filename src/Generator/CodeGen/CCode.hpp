/* 
 * Copyright (C) 2017 Benjamin Rouxel <benjamin.rouxel@inria.fr> 
 *                    Irisa/INRIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CODEGENERATOR_H
#define CODEGENERATOR_H
#include <string>
#include <vector>
#include <map>

#include "Generator/Runtime/RuntimeStrategyInterface.hpp"
#include "Utils/Log.hpp"
#include "Utils/Graph.hpp"
#include <boost/filesystem.hpp>
#include "CodeGenerationPatterns.hpp"
#include "CodeGenerationEnums.hpp"
#include "CodeGenHelper.hpp"

/**
 * General helper to generate C code 
 * 
 * \see CCode::help
 * \see sleep_mechanism_e
 * \see runtime_container_e
 */
class CCode : CodeGenHelper {
protected:    
    std::vector<std::vector<Task*>> _DAGS; //!< Hold the list of DAGs
    bool _has_multiple_period = false; //!< True if the application has multiple DAGs
    
    std::vector<std::string> _includes; //!< Extra include declaration to put in header file, system includes must be prefixed by "sys:"
    std::string _initializer = ""; //!< End-user initialisation function
    std::string _cleaner = ""; //!< End-user termination function
    std::string _main_mapping = ""; //!< Where to map the main thread
    std::string _maxiter = ""; //!< bound on the execution of the end-user application, number of graphs iteration
    
public:
    //! Constructor
    explicit CCode() : CodeGenHelper() {}
    
    virtual void init(const SystemModel *s, const config_t *c, const Schedule *se, RuntimeStrategyInterface *st) override;
    
    ACCESSORS_R(CCode, std::vector<std::string>, includes)
    ACCESSORS_R(CCode, std::string, initializer)
    ACCESSORS_R(CCode, std::string, cleaner)
    ACCESSORS_R(CCode, std::string, main_mapping)
    ACCESSORS_RW(CCode, std::string, maxiter)
    ACCESSORS_R(CCode, bool, has_multiple_period)
    ACCESSORS_R(CCode, std::vector<std::vector<Task*>>, DAGS)
    
    /**
     * Return a string representation of all includes
     * @return 
     */
    virtual std::string includes_to_string();
    
    /**
     * Return the name of the directory in which generated files are stored
     * @return 
     */
    virtual const std::string codegendir() {return "codegen/"; }
    
    /**
     * Add an include
     * 
     * If the include is related to a system inclusion, then the suffixe "sys:" should be used
     * 
     * @param i
     */
    virtual void addInclude(const std::string &i);
    
    
    virtual void setParams(const std::map<std::string, std::string> &args) override;
    virtual std::string help() override;
};

#endif /* CODEGENERATOR_H */


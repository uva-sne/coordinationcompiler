/* 
 * Copyright (C) 2020 Benjamin Rouxel <benjamin.rouxel@uva.nl> 
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef DATAEXCHANGESYNCHRO_H
#define DATAEXCHANGESYNCHRO_H

#include <string>
#include "BufferMgnt.hpp"

/**
 * Helper to generate code for the synchronisation
 * 
 * There is a need for synchronisation on FIFO buffer to guarantee that tokens
 * have been received by the consumer before the consumer actually tries to pop
 * the tokens.
 * 
 * This synchronisation can take different forme, such as semaphores, busy-waiting,
 * etc... But in all cases, when two tasks exchange data, the consumer (predecessor)
 * must wait for the producer (successor). While the producer must release the 
 * consumer.
 * 
 * \see synchro_mechanism_e
 */
class DataExchangeSynchro : public CodeGenHelper {
public:
    //! Constructor
    explicit DataExchangeSynchro() : CodeGenHelper() {}
    
    void setParams(const std::map<std::string, std::string> &args) override {}
    virtual std::string help() override;
    
    /**
     * Generate the code to release the successors of a task
     * 
     * @param buffmgnt
     * @param t
     * @return 
     */
    virtual std::string generate_release_successors(BufferMgnt *buffmgnt, Task *t);
    /**
     * Generate the code to wait that predecessors finish their task
     * 
     * @param buffmgnt
     * @param t
     * @return 
     */
    virtual std::string generate_wait_predecessors(BufferMgnt *buffmgnt, Task *t);
    /**
     * Generate initialisation code each required locks
     * @return 
     */
    virtual std::string generate_initialisation();
    /**
     * Generate termination code for each created locks
     * @return 
     */
    virtual std::string generate_cleanup();
    
    /**
     * Generate locks declaration code
     * @return 
     */
    virtual std::string generate_declaration();
    
private:

};

#endif /* DATAEXCHANGESYNCHRO_H */


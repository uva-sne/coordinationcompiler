/*
 * Copyright (C) 2021 Lukas Miedema <l.miedema@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CECILE_AROMAWRAPPERWRITER_HPP
#define CECILE_AROMAWRAPPERWRITER_HPP

#include "AromaGeneratorHelper.hpp"

/**
 * Generates interoperability wrappers (C++ code) to bridge between the user-supplied task implementations and the
 * Aroma RTE developed by the University of Augsburg.
 */
class AromaWrapperWriter { ;
public:
    /**
     * Generate the file and save it as the specified file. A ".axf.hpp" extension will be added to the file.
     * @param fn the file to write to.
     */
     void generate(const boost::filesystem::path& fn);

     /**
      * Add the provided paths as an include statement to the generated code.
      * @param c_includes
      * @param cpp_includes
      * */
     void set_include_paths(const std::vector<std::string> c_includes, const std::vector<std::string> cpp_includes);

    //! Constructor
    explicit AromaWrapperWriter(AromaGeneratorHelper *helper) : helper(helper) {}
protected:
    //! Helper
    const AromaGeneratorHelper *helper;

    //! Additional include paths wrapped in "extern C"
    std::vector<std::string> include_paths_c;

    //! Additional include paths
    std::vector<std::string> include_paths_cpp;

    //! Generate wrapper functions
    std::string generateRapidWrappers();

    //! Generate forward declarations for the RAPID functions
    std::string generateRapidForwardDeclarations();

    //! Generate include statements for C include paths
    std::string generateIncludePathsC();

    //! Generate include statements for CPP include paths
    std::string generateIncludePathsCpp();

    //! Generate composite types for task pairs with multiple connections
    std::string generateCompositeTypes();
};


#endif //CECILE_AROMAWRAPPERWRITER_HPP

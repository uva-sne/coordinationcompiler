/*
 * Copyright (C) 2021 Lukas Miedema <l.miedema@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "AromaDotWriter.hpp"
#include "Generator/CodeGen/CodeGenerationPatterns.hpp"

#define INDENT          "    " // also update AromaDot.tpl if you change this
#define INDENT_W        INDENT INDENT // wide
#define NAME_LENGTH     32
#define FILE_EXTENSION  ".axf" // axf = Admorph Exchange Format

namespace ba = boost::algorithm;

void AromaDotWriter::generate(const boost::filesystem::path &fn) {
    std::string dot_template =
#include "Generator/Runtime/Aroma/AromaDot.tpl"
    ;

    std::map<std::string, size_t> coreTypes = this->helper->getCoreTypes();

    ba::replace_all(dot_template, txtappname, helper->conf()->files.coord_basename.string());
    ba::replace_all(dot_template, txtattributes, this->generateGlobalAttributes());
    ba::replace_all(dot_template, txthardwareattributes, this->generateHardwareAttributes());
    ba::replace_all(dot_template, txtactornodes, this->generateActorNodeStrings(coreTypes));
    ba::replace_all(dot_template, txtdatanodes, this->generateDataNodeStrings());
    ba::replace_all(dot_template, txtedges, this->generateEdgeStrings());
    ba::replace_all(dot_template, txthardwarenodes, this->generateHardwareNodes(coreTypes));

    std::ofstream ofs(fn.string() + FILE_EXTENSION);
    ofs << dot_template;
    ofs.close();
}

void pad_to_length(std::string& input) {
    while (input.length() < NAME_LENGTH)
        input.append(" ");
}

std::string AromaDotWriter::generateEdgeStrings() {
    std::string edges;
    for (Task *task : helper->tg()->tasks_it()) {
        std::string actor_name = this->helper->getActorNodeName(task) + " ->";
        pad_to_length(actor_name);

        // Write outgoing edges (actor -> data)
        if (task->successor_tasks().empty()) {
            // Generate edge to synthetic outport
            std::string end_data_node = this->helper->getSyntheticEndDataNodeForTask(task);
            pad_to_length(end_data_node);
            edges.append(INDENT_W).append(actor_name).append(end_data_node).append("[resultindex=0, synthetic=\"true\"];\n");
        }
        size_t connectionIndex = 0;
        for (Task::dep_t successor: task->successor_tasks()) {
            Task *sink = successor.first;
            std::string datanode_name = this->helper->getDataNodeName(task, sink);
            pad_to_length(datanode_name);
            edges.append(INDENT_W).append(actor_name).append(datanode_name).append("[resultindex=").append(to_string(connectionIndex++)).append("]").append(";\n");
        }

        // Write incoming edges (data -> actor)
        if (task->predecessor_tasks().empty()) {
            // Generate edge to synthetic inport
            std::string datanode_from = this->helper->getSyntheticInputNode() + " ->";
            std::string actor_to = this->helper->getActorNodeName(task);
            pad_to_length(datanode_from);
            pad_to_length(actor_to);
            edges.append(INDENT_W).append(datanode_from).append(actor_to).append(R"([paramindex=0, synthetic="true"];)").append("\n");
        }
        connectionIndex = 0;
        for (Task::dep_t predecessor: task->predecessor_tasks()) {
            std::string datanode_from = this->helper->getDataNodeName(predecessor.first, task) + " ->";
            std::string actor_to = this->helper->getActorNodeName(task);
            pad_to_length(datanode_from);
            pad_to_length(actor_to);
            edges.append(INDENT_W).append(datanode_from).append(actor_to)
                .append(R"([paramindex=)")
                .append(to_string(connectionIndex++))
                .append(R"(, synthetic="true"];)").append("\n");
        }
    }
    return edges;
}

std::string AromaDotWriter::generateGlobalAttributes() {
    std::string attributes;
    attributes
        .append(INDENT)
        .append("deadline=").append(to_string(NS_TO_MS(helper->tg()->global_deadline()))).append(";\n");
    attributes
        .append(INDENT)
        .append("period=").append(to_string(NS_TO_MS(helper->tg()->global_period()))).append(";\n");


    if (!this->_initialization_function.empty())
        attributes.append(INDENT).append("program_initialization=\"").append(this->_initialization_function).append("\";\n");

    if (!this->_destroy_function.empty())
        attributes.append(INDENT).append("program_destruction=\"").append(this->_destroy_function).append("\";\n");

    return attributes;
}

std::string AromaDotWriter::generateActorNodeStrings(std::map<std::string, size_t> &coresOfType) {
    std::string actor_nodes;
    std::vector<Task *> tasks = this->helper->getTasksSorted();
    for (Task *task : tasks) {
        std::string fn_name = this->helper->getWrapperFunctionName(task);
        std::string type = this->helper->getActorNodeType(task);
        std::string name = this->helper->getActorNodeName(task);
        std::map<std::string, frequencyinfo_t> Cs = this->helper->getWcetPerCoreType(task, coresOfType);

        pad_to_length(name);
        actor_nodes
            .append(INDENT_W)
            .append(name)
            .append(R"([shape=box, atype=")").append(type).append(R"(", afunc=")")
            .append(fn_name)
            .append("\"");

        for (auto &C : Cs) {
            actor_nodes
                .append(", ")
                .append("awcet_core_").append(C.first).append("=")
                .append(to_string(NS_TO_MS(C.second)));
        }
        actor_nodes.append("];\n");
    }
    return actor_nodes;
}

std::string AromaDotWriter::generateDataNodeStrings() {
    // Data nodes are a mapping of task pairs, for each task pair with an edge a singular data node exists
    std::string data_nodes;
    for (Task *task : this->helper->getTasksSorted()) {
        for (Task::dep_t successor : task->successor_tasks()) {

            // Sum all sizes
            uint64_t size = 0;
            for (Connection *conn : successor.second) {
                if (!helper->conf()->datatypes.count(conn->from()->token_type()))
                    throw CompilerException("aroma-generator", "No size specified in the configuration for datatype " + conn->from()->token_type());
                size += helper->conf()->datatypes.at(conn->from()->token_type()).size_bits / 8;
            }
            std::string name = this->helper->getDataNodeName(task, successor.first);
            pad_to_length(name);
            data_nodes += INDENT_W + name + "[esize=" + to_string(size) + ", ptype=\"inner\"];\n";
        }
    }

    // Add synthetic inport for tasks without an inport (requirement of Aroma)
    std::string synthetic_input = this->helper->getSyntheticInputNode();
    pad_to_length(synthetic_input);
    data_nodes
        .append(INDENT_W)
        .append(synthetic_input)
        .append("[esize=")
        .append(to_string(sizeof(bool)))
        .append(R"(, ptype="input", synthetic="true"];)")
        .append("\n");

    // Add synthetic outports for tasks without an outport (requirement of Aroma)
    std::vector<std::string> synthetic_data_nodes = this->helper->getSyntheticOutputNodes();
    for (std::string &name : synthetic_data_nodes) {
        pad_to_length(name);
        data_nodes
            .append(INDENT_W)
            .append(name)
            .append("[esize=")
            .append(to_string(sizeof(bool))) // this will never go wrong surely
            .append(R"(, ptype="output", synthetic="true"];)")
            .append("\n");
    }
    return data_nodes;
}

std::string AromaDotWriter::generateHardwareNodes(std::map<std::string, size_t> &coresOfType) {
    std::string hw;
    for (auto &ct : coresOfType) {
        std::string typeName = ct.first;
        pad_to_length(typeName);
        hw
            .append(INDENT)
            .append(typeName)
            .append("[num=")
            .append(to_string(ct.second))
            .append(", max=")
            .append(to_string(ct.second))
            .append("];\n");
    }
    return hw;
}

std::string AromaDotWriter::generateHardwareAttributes() {
    std::string attributes;
    if (!this->_communication_factor.empty())
        attributes.append(INDENT).append("communication_factor=").append(this->_communication_factor).append(";\n");
    return attributes;
}

/* 
 * Copyright (C) 2020 Benjamin Rouxel <benjamin.rouxel@uva.nl> 
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "CMake.hpp"

using namespace std;
namespace ba = boost::algorithm;

void  CMake::generate(const vector<string> & includes, const map<string, string> &extra) {
    string cmake = strategy->tpl_getBuildFile();
    
    string incs = "", compopts = "";
    set<string> extra_pkg;
    vector<string> copts;
    boost::split(copts, compiler_opts, boost::is_any_of(" "));
    for(size_t i = 0 ; i < copts.size() ; ++i) {
        if(copts[i].size() >= 2 && copts[i].at(1) == 'I') {
            if(copts[i].size() > 2)
                incs += "\""+copts[i].substr(2, copts[i].size())+"\"\n\t\t";
            else
                incs += "\""+copts[++i]+"\"\n\t\t";
        }
        else if(copts[i].size() >= 2 && copts[i].at(0) == '$' && copts[i].at(1) == '$') {
            while(copts[i].at(copts[i].size()-1) != ')') ++i;
            bool skip = false;
            if(copts[i].size() == 1) {
                --i;
                skip = true;
            }
            else
                copts[i] = copts[i].substr(0, copts[i].size()-1);
            extra_pkg.insert(copts[i]);
            if(skip)
                ++i;
        }
        else
            compopts += copts[i]+" ";
    }
    
    copts.clear();
    string linkopts = "";
    string extralib = "";
    string sextra_pkg = "";
    bool include_pkg_config = false;
    boost::split(copts, linker_opts, boost::is_any_of(" "));
    for(size_t i = 0 ; i < copts.size() ; ++i) {
        if(copts[i].size() >= 2 && copts[i].at(0) == '$' && copts[i].at(1) == '$') {
            while(copts[i].at(copts[i].size()-1) != ')') ++i;
            bool skip = false;
            if(copts[i].size() == 1) {
                --i;
                skip = true;
            }
            else
                copts[i] = copts[i].substr(0, copts[i].size()-1);
            
            include_pkg_config = true;
            sextra_pkg  += "pkg_check_modules(extra_"+copts[i]+" REQUIRED "+copts[i]+")\n";
            extralib += "target_link_libraries(${CMAKE_TARGET_NAME} PUBLIC ${extra_"+copts[i]+"_LIBRARIES})\n";
            incs += " ${extra_"+copts[i]+"_INCLUDE_DIRS}";
            
            if(skip)
                ++i;
        }
        else if(copts[i].size() >= 2 && copts[i].at(0) == '-' && copts[i].at(1) == 'l') {
            if(copts[i].size() == 2)
                ++i;
            else
                copts[i] = copts[i].substr(2, copts[i].size());
            extralib += "target_link_libraries(${CMAKE_TARGET_NAME} PUBLIC "+copts[i]+")\n";
        }
        else
            linkopts += copts[i]+" ";
    }
    
    if(include_pkg_config)
        sextra_pkg = "find_package(PkgConfig REQUIRED)\n"+sextra_pkg;
    
    ba::replace_all(cmake, "%EXTRAPKG%", sextra_pkg);
    ba::replace_all(cmake, "%EXTRALIB%", extralib);
    
    if(incs.empty())
        incs += "\"\"";
    ba::replace_first(cmake, txtinc, incs); 
    boost::trim(compopts);
    ba::replace_all(cmake, txtcompopt, compopts);
    
    ba::replace_first(cmake, txtlinkopt, linkopts);
    
    string cc = "";
    if(!compiler.empty()) {
        cc = "set(CMAKE_"+string(txtupcodelang)+"_COMPILER "+compiler+")";
    }
    ba::replace_first(cmake, txtcc, cc);
    
    string sarch = "";
    if(!arch.empty()) {
        sarch = "set(CMAKE_SYSTEM_PROCESSOR "+arch+")";
    }
    ba::replace_first(cmake, txtarch, sarch);
    
    ba::replace_all(cmake, txtcodelang, _target_code_language);
    string upcodelang = (_target_code_language == "cpp") ? "CXX" : boost::to_upper_copy(_target_code_language);
    ba::replace_all(cmake, txtupcodelang, upcodelang);
    
    string srcs = "";
    
    for(Task *t : tg->tasks_it()) {
        for(Version *v : t->versions()) {
            
            if(!v->cfile().empty()) {
                bool exists = true;
                boost::filesystem::path f = v->cfile();
                if(!boost::filesystem::exists(f)) {
                    f = conf->files.output_folder / v->cfile();
                    if(!boost::filesystem::exists(f))
                        exists = false;
                }
                if(exists && find(additional_srcs.begin(), additional_srcs.end(), f.string()) == additional_srcs.end()) {
                    srcs += f.string()+" \\\n";
                    additional_srcs.push_back(f.string());
                }
            }
            
            if(!v->cbinary().empty()) {
                bool exists = true;
                boost::filesystem::path f = v->cbinary();
                if(!boost::filesystem::exists(f)) {
                    f = conf->files.output_folder / v->cbinary();
                    if(!boost::filesystem::exists(f))
                        exists = false;
                }
                if(exists && find(additional_objs.begin(), additional_objs.end(), f.string()) == additional_objs.end()) {
                    additional_objs.push_back(f.string());
                }
            }
        }
    }
    
    for(string s : additional_srcs)
        srcs += "\""+s+"\"\n\t\t";
    if(srcs.empty())
        srcs = "\"\"";
    ba::replace_first(cmake, txtsrc, srcs);
    
    string objs = "";
    for(string s : additional_objs)
        objs += "\""+s+"\"\n\t\t";
    if(objs.empty())
        objs = "\"\"";
    ba::replace_first(cmake, txtextraobject, objs);
    
    ba::replace_all(cmake, txtappname, strategy->app_name());
    
    for(pair<string, string> el : extra)
        ba::replace_all(cmake, el.first, el.second);
    
    ofstream ofs((strategy->path() / "CMakeLists.txt").string());
    ofs << cmake;
    ofs.close();
}

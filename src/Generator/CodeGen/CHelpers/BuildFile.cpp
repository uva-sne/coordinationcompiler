/* 
 * Copyright (C) 2020 Benjamin Rouxel <benjamin.rouxel@uva.nl> 
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "BuildFile.hpp"

using namespace std;
namespace ba = boost::algorithm;

IMPLEMENT_HELP(BuildFile, 
    HTMLINDENT- compiler, default gcc: compiler to use in generated build file. Can be any string,
    e.g. gcc, g++, arm-gcc, ...\n
    HTMLINDENT- language [c, cpp/c++/cxx], deault c: target language for the generated code.
    If set to cpp, compiler is automatically switched to g++.\n
    HTMLINDENT- compiler-opts: general options to pass to the compiler, e.g. -g, 
    -I...\n
    HTMLINDENT- linker-opts: general options to pass to the linker, e.g. -L...\n
    HTMLINDENT- additional-srcs: list of space separated files to add to the compilation
    list when generating the build file. For example, if you placed your 
    components/tasks in an exteranl .c/.cpp file, you can give the full path
    here. The generated build file will then add it to the compilation process.\n
    HTMLINDENT- additional-objs: if some components/tasks require to be compiled with another
    compiler, e.g. nvcc for cuda file, you can give the full path to the generated
    object file here and this will be added to the linker process.\n
)

void BuildFile::setParams(const std::map<std::string,std::string>& args) {
    compiler = "gcc";
    compiler_opts = "";
    linker_opts = "";

    if(args.count("compiler"))
        compiler = args.at("compiler");
    if(args.count("language")) {
        _target_code_language = args.at("language");
        if(_target_code_language == "c++" || _target_code_language == "cxx")
            _target_code_language = "cpp";
        if(_target_code_language == "cpp" && compiler == "gcc")
            compiler = "g++";
    }
    if(args.count("compiler-opts"))
        compiler_opts += ba::replace_all_copy(args.at("compiler-opts"), "$", "$$");
    if(args.count("linker-opts"))
        linker_opts += ba::replace_all_copy(args.at("linker-opts"), "$", "$$");
    if(args.count("additional-srcs") && !args.at("additional-srcs").empty()) {
        vector<string> incs;
        boost::split(incs, args.at("additional-srcs"), boost::is_any_of(" "));
        for(string inc : incs) {
            if(!inc.empty())
                additional_srcs.push_back(inc);
        }
    }
    if(args.count("additional-objs") && !args.at("additional-objs").empty()) {
        vector<string> incs;
        boost::split(incs, args.at("additional-objs"), boost::is_any_of(" "));
        for(string inc : incs) {
            if(!inc.empty())
                additional_objs.push_back(inc);
        }
    }
    
    if(args.count("arch"))
        arch = args.at("arch");
}

void BuildFile::generate(const std::vector<std::string> &inc) {
    std::map<std::string, std::string> extra;
    generate(inc, extra);
}

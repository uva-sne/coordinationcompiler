/* 
 * Copyright (C) 2020 Benjamin Rouxel <benjamin.rouxel@uva.nl> 
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef BUFFERMGNT_H
#define BUFFERMGNT_H

#include "SystemModel.hpp"
#include "config.hpp"
#include "Schedule.hpp"
#include "Utils/Log.hpp"
#include "Utils/Graph.hpp"
#include <boost/filesystem.hpp>
#include "Generator/CodeGen/CodeGenerationPatterns.hpp"
#include "Generator/CodeGen/CodeGenerationEnums.hpp"
#include "Generator/Runtime/RuntimeStrategyInterface.hpp"
#include "Generator/CodeGen/CodeGenHelper.hpp"

#include <regex>
#include <string>
#include <map>
#include <vector>

/**
 * Helper to generate the code to manage buffers
 * 
 * Tasks within a graph are connected using FIFO buffers. This class generates the
 * code to manage this FIFO buffers in a C/C++ code. Because polymorphic doesn't 
 * exist in C code, a buffer per type needs to be created. To deal with real-time
 * systems, these buffers need to be statically allocated and loop to scan them
 * must be bounded. Therefore, a buffer type is created per combinaison of (type,size).\n
 * 
 * FIFO buffers can be materialised in different, as of writing, they can be a bounded
 * array stored in memory (shareable by threads), or using unix pipes shareable by
 * processes.
 * 
 * A buffer type definition when using array looks like:
 * typedef struct {
 *   size_t head;
 *   size_t tail;
 *   %TYPE% buffer[%SIZE%+1]; //add +1 to the size as a circular buffer of size 1 doesn't work
 * } buffer_%BUFFERTYPENAME%_t;
 * 
 * The name of the buffer is the concatenation of the type and the size. A buffer 
 * which can hold up to 42 floats will have the name buffer_float42_t.
 * 
 * Then to access the buffer a collection of function is generated as well:
 *   - an open function (init_buffer_%BUFFERTYPENAME%)\n
 *   - a close function (clean_buffer_%BUFFERTYPENAME%)\n
 *   - a peek function to cherry-pick a value in the buffer (peek_%BUFFERTYPENAME%), \n
 * note that it doesn't work for unix pipes.\n
 *   - a pop function to fetch the oldest inserted element (pop_%BUFFERTYPENAME%)\n
 *   - a push fucntion to insert a element (push_%BUFFERTYPENAME%)
 * 
 * \see channel_mechanism_e
 */
class BufferMgnt : public CodeGenHelper {
protected:
    //! Internal type to represent a buffer
    typedef struct {
        std::string buffTypeName; //!< Name of the buffer
        std::string init_type; //!< Type of element stored in the buffer
        size_t nbtokens; //!< Size of the buffer
    } buffer_types;
    
    std::map<std::string, buffer_types> buffer_types_list; //!< List of all buffers by task connector
public:
    //! Constructor
    BufferMgnt() : CodeGenHelper() {}
    //! Destructor
    virtual ~BufferMgnt() = default;
    
    virtual void setParams(const std::map<std::string, std::string> &args) override {}
    virtual std::string help() override {return "";}
    
    /**
     * \copydoc CodeGenHelper::init
     * 
     * Compute the list of used types
     */
    virtual void init(const SystemModel *s, const config_t *c, const Schedule *se, RuntimeStrategyInterface *st) override;
    /**
     * Generate the buffer declaration variables
     * @return 
     */
    virtual std::string generate_declarations();
    /**
     * Generate calls to initialise all buffers
     * @return 
     */
    virtual std::string generate_initialisations();
    /**
     * Generate calls to free all buffers
     * @return 
     */
    virtual std::string generate_terminaisons();
    /**
     * Generate the definition of buffers
     * @return 
     */
    virtual std::string generate_definitions();
    /**
     * Generate the code to connect a task
     * 
     * It generates the code to fetch the tokens, and to push the one produced
     * 
     * @param t
     * @param args to pass when calling the task
     * @param decl declare the local variable to store the fetched/produced tokens
     * @param inputs code to fetch tokens
     * @param outputs code to push tokens
     */
    virtual void generate_task_connection(Task *t, std::string *args, std::string *decl, std::string *inputs, std::string *outputs);
    
    /**
     * Get the buffer variable name for a given task connector
     * @param name should be the concatenation of task->id+_+con->id
     * @return 
     */
    virtual std::string get_buffer_name(std::string name);
    /**
     * Return the name of the file holding the generated code
     * @param codelang targeted language
     * @return 
     */
    virtual std::string getFileName(std::string codelang);
protected:
    /**
     * Declare a local variable to store a token before fetching/pushing it into the FIFO
     * 
     * @param nbtokens nb tokens are fetched/pushed
     * @param type type of the token
     * @param varname name of the variable
     * @param forcedinitval string to init the local variable
     * @return 
     */
    virtual std::string get_var_declaration(size_t nbtokens, std::string type, std::string varname, std::string forcedinitval="");
};

#endif /* BUFFERMGNT_H */


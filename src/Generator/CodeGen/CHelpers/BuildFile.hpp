/* 
 * Copyright (C) 2020 Benjamin Rouxel <benjamin.rouxel@uva.nl> 
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef BUILDFILE_H
#define BUILDFILE_H

#include <string>
#include <vector>
#include <map>

#include "Generator/Runtime/RuntimeStrategyInterface.hpp"
#include "Generator/CodeGen/CodeGenHelper.hpp"

/**
 * Helper to build the compilation directives file
 * 
 * \see BuildFile::help 
 */
class BuildFile : public CodeGenHelper {
protected:
    
    std::string compiler = "gcc"; //!< Compiler to use
    std::string arch = ""; //!< Target architecture, use for cross-compilation in CMake
    std::string compiler_opts = ""; //!< Extra compiler options
    std::string linker_opts = ""; //!< Extra linker options
    std::string _target_code_language = "c"; //!< Target language of the code
    std::vector<std::string> additional_srcs; //!< Additional external sources that need compilation directives
    std::vector<std::string> additional_objs; //!< Additional external objects to link the final executable
    
public:
    //! Construtor
    BuildFile() : CodeGenHelper() {}
    
    /**
     * 
     * @param inc
     */
    virtual void generate(const std::vector<std::string> &inc);
    /**
     * Generate the Makefile
     * @param inc list of extra files on which the generated code depends
     * @param extra parameters to replace in the Makefile
     */
    virtual void generate(const std::vector<std::string> &inc, const std::map<std::string, std::string> &extra) = 0;
    virtual void setParams(const std::map<std::string,std::string>& args) override;
    virtual std::string help() override;
    
    ACCESSORS_R(BuildFile, std::string, target_code_language)
protected:

};

#endif /* BUILDFILE_H */


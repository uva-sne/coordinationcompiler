/*
 * Copyright (C) 2021 Lukas Miedema <l.miedema@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CECILE_AROMADOTWRITER_HPP
#define CECILE_AROMADOTWRITER_HPP

#include <string>
#include "SystemModel.hpp"
#include "AromaGeneratorHelper.hpp"

/**
 * AromaDotWriter is a code (rather file) generator exporting the task graph in the Aroma DOT format (with modifications
 * for ADMORPH).
 */
class AromaDotWriter {
public:
    //! Constructor
    explicit AromaDotWriter(AromaGeneratorHelper *helper) : helper(helper) {};

    /**
     * Generate the file and save it as the specified file.
     * @param fn the file to write to
     */
    void generate(const boost::filesystem::path& fn);
    
    ACCESSORS_RW(AromaDotWriter, std::string, initialization_function)
    ACCESSORS_RW(AromaDotWriter, std::string, destroy_function)
    ACCESSORS_RW(AromaDotWriter, std::string, communication_factor);

protected:
    //! Helper
    const AromaGeneratorHelper *helper;

    //! Name of the (void, zero- args) initialization function that needs to be called first
    std::string _initialization_function;

    //! Name of the (void, zero-arg) teardown function that needs to be called last
    std::string _destroy_function;

    //! Communication factor, must be a number but is not parsed here
    std::string _communication_factor;

    /**
     * Generate and return the tasks represented as actor nodes in the Aroma DOT format.
     * @return
     */
    std::string generateActorNodeStrings(std::map<std::string, size_t> &coresOfType);

    /**
     * Generate and return the tasks represented as data nodes in the Aroma DOT format.
     * @return
     */
    std::string generateDataNodeStrings();

    /**
     * Generate and return the edge representations as strings in the DOT format.
     * @return
     */
    std::string generateEdgeStrings();

    /**
     * Generate and return global attributes in the DOT format.
     * @return
     */
    std::string generateGlobalAttributes();

    /**
     * Generate and return node representation of the hardware (processors)
     * @return
     */
    std::string generateHardwareNodes(std::map<std::string, size_t> &coresOfType);

    /**
     * Generate and return global hardware attributes in the DOT format.
     * @return
     */
    std::string generateHardwareAttributes();
};


#endif //CECILE_AROMADOTWRITER_HPP

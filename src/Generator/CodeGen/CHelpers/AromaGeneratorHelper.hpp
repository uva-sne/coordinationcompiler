/*
 * Copyright (C) 2021 Lukas Miedema <l.miedema@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CECILE_AROMAGENERATORHELPER_HPP
#define CECILE_AROMAGENERATORHELPER_HPP

#include "Helper_macros.hpp"
#include "SystemModel.hpp"

/**
 * Host for all common Aroma / ADMORPH Exchange Format (AXF) utility functions.
 */
class AromaGeneratorHelper {
public:
    //! Constructor
    explicit AromaGeneratorHelper(const SystemModel *tg, const config_t *conf) : _tg(tg), _conf(conf) {};

    //! Each task is mapped to an actor node. Return the name of this node.
    std::string getActorNodeName(const Task *task) const;

    //! Return the actor type for the given task ("map" or "zipmap")
    std::string getActorNodeType(const Task *task) const;

    //! Each outport is mapped to an data node. Return the name of this data node.
    std::string getDataNodeName(const Task *src, const Task *dest) const;

    //! Return the name of the synthetic wrapper function bridging the gap between the actual task implementation and
    //! the Aroma RTE
    std::string getWrapperFunctionName(const Task *task) const;

    //! The Aroma RTE requires at least 1 output data node for all actors. If the task has no output, get a synthetic one.
    std::string getSyntheticEndDataNodeForTask(const Task *task) const;

    //! Get a list of all synthetic data nodes that must be present in the DOT format.
    std::vector<std::string> getSyntheticOutputNodes() const;

    //! Get the name of the synthetic input data node that must be present in the DOT format.
    std::string getSyntheticInputNode() const;

    //! Get the wcet per core type for a given task
    std::map<std::string, timinginfos_t> getWcetPerCoreType(Task *task, std::map<std::string, size_t> &coreTypes) const;

    //! Get the energy consumption of a task per core
    std::map<std::string, energycons_t> getEnergyPerCoreType(Task *task, std::map<std::string, size_t> &coreTypes) const;

    //! Get a map of core types and the number of cores of that type
    std::map<std::string, size_t> getCoreTypes() const;

    //! Get the data type, or an exception
    const std::string& getCType(const std::string &name) const;

    //! Get type name of the composite type for the connection pair
    std::string getCompositeTypeName(Task *from, Task *to) const;

    //! Get a sorted list
    std::vector<Task *> getTasksSorted() const;

    ACCESSORS_R_CONSTONLY(AromaGeneratorHelper, SystemModel *, tg);
    ACCESSORS_R_CONSTONLY(AromaGeneratorHelper, config_t *, conf);



protected:
    //! IR of the current application
    const SystemModel *_tg = nullptr;
    //! the global configuration
    const config_t *_conf = nullptr;

};


#endif //CECILE_AROMAGENERATORHELPER_HPP

/* 
 * Copyright (C) 2020 Benjamin Rouxel <benjamin.rouxel@uva.nl> 
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "NoBufferMgnt.hpp"

void NoBufferMgnt::generate_task_connection(Task *t, std::string *args, std::string *decl, std::string *inputs, std::string *outputs) {
    *args = "";
    *decl = "";
    *inputs = "";
    *outputs = "";
    
    for(Connector *elc : t->inputs())
        *args += "*"+t->id()+"_"+elc->id()+", ";
    for(Connector *elc : t->outputs())
        *args += "&"+t->id()+"_"+elc->id()+", ";
}


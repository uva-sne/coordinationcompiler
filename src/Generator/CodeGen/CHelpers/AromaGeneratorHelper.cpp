/*
 * Copyright (C) 2021 Lukas Miedema <l.miedema@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "AromaGeneratorHelper.hpp"

std::string AromaGeneratorHelper::getActorNodeName(const Task *task) const {
    return "a_" + task->id();
}

std::string AromaGeneratorHelper::getDataNodeName(const Task *src, const Task *dest) const {
    return "p_" + src->id() + "__to__" + dest->id();
}

std::string AromaGeneratorHelper::getWrapperFunctionName(const Task *task) const {
    return task->id() + "__wrapper";
}

std::string AromaGeneratorHelper::getSyntheticEndDataNodeForTask(const Task *task) const {
    assert(task->outputs().empty()); // not empty -> no synthetic end data node
    return "p_" + task->id() + "_end";
}

std::string AromaGeneratorHelper::getSyntheticInputNode() const {
    return "p_input";
}

std::vector<std::string> AromaGeneratorHelper::getSyntheticOutputNodes() const {
    std::vector<std::string> out;
    for (Task *task : this->tg()->tasks_it()) {
        if (task->successor_tasks().empty())
            out.push_back(getSyntheticEndDataNodeForTask(task));
    }
    return out;
}

std::string AromaGeneratorHelper::getActorNodeType(const Task *task) const {
    return "generic";
}

std::map<std::string, timinginfos_t> AromaGeneratorHelper::getWcetPerCoreType(Task *task, std::map<std::string, size_t>& coreTypes) const {
    std::map<std::string, timinginfos_t> out;
    for (auto &ct : coreTypes) {
        Version *needle = nullptr;
        for (Version *v : task->versions()) {
            if (std::any_of(v->force_mapping_proc().begin(),  v->force_mapping_proc().end(),
                    [&ct](ComputeUnit *cu) { return cu->type == ct.first;})) {
                if (needle != nullptr)
                    throw CompilerException("aroma-generator", "AXF does not support multiple versions for the same processor arch, task "
                    + task->id() + ", versions [" + needle->id() + ", " + v->id() + "] for arch = " + ct.first);
                needle = v;
            }
        }
        out[ct.first] = needle == nullptr ? std::numeric_limits<timinginfos_t>::max() : needle->C();
    }
    return out;
}

std::map<std::string, energycons_t> AromaGeneratorHelper::getEnergyPerCoreType(Task *task, std::map<std::string, size_t> &coreTypes) const {
    std::map<std::string, timinginfos_t> out;
    for (auto &ct : coreTypes) {
        Version *needle = nullptr;
        for (Version *v : task->versions()) {
            if (std::any_of(v->force_mapping_proc().begin(),  v->force_mapping_proc().end(),
                    [&ct](ComputeUnit *cu) { return cu->type == ct.first;})) {
                if (needle != nullptr)
                    throw CompilerException("aroma-generator", "AXF does not support multiple versions for the same processor arch, task "
                    + task->id() + ", versions [" + needle->id() + ", " + v->id() + "] for arch = " + ct.first);
                needle = v;
            }
        }
        out[ct.first] = needle == nullptr ? std::numeric_limits<timinginfos_t>::max() : needle->C_E();
    }
    return out;
}

std::map<std::string, size_t> AromaGeneratorHelper::getCoreTypes() const {
    std::map<std::string, size_t> out;
    for (ComputeUnit *cu: tg()->processors())
        out[cu->type]++;
    return out;
}


const std::string& AromaGeneratorHelper::getCType(const std::string &name) const {
    auto res = this->_conf->datatypes.find(name);
    if (res == this->_conf->datatypes.end())
        throw CompilerException("aroma-generator", "Unknown datatype " + name);
    if (res->second.ctype.empty())
        return name;
    return res->second.ctype;
}

std::string AromaGeneratorHelper::getCompositeTypeName(Task *from, Task *to) const {
    return "struct __composite_" + from->id() + "_" + to->id() + "__";
}

std::vector<Task *> AromaGeneratorHelper::getTasksSorted() const {
    auto tasks = _tg->tasks_as_vector();
    auto visited = std::set<Task*>();
    auto Qready = std::queue<Task *>();
    auto Qdone = std::vector<Task*>(); // in reverse

    // Locate the end(s)
    for (Task *element : this->_tg->tasks_it()) {
        if (element->successor_tasks().empty()) {
            Qready.emplace(element);
        }
    }

    if (Qready.empty())
        throw CompilerException("aroma-generator", "Task graph contains cycles");

    while (!Qready.empty()) {
        Task *top = Qready.front();
        Qready.pop();

        Qdone.push_back(top);
        visited.insert(top);

        for (Task* predecessor : top->predecessor_tasks() | boost::adaptors::map_keys) {
            // Only add it if all its successors are already visited (=we are visiting the last path now)
            bool allVisited = true;
            for (Task *successor: predecessor->successor_tasks() | boost::adaptors::map_keys) {
                if (visited.find(successor) == visited.end()) {
                    allVisited = false;
                    break;
                }
            }

            // Add predecessor to the queue
            // We are the last path to this predecessor
            if (allVisited) {
                Qready.emplace(predecessor);
            }
        }
    }

    // Qdone was constructed in reverse
    std::reverse(Qdone.begin(),  Qdone.end());
    return Qdone;
}

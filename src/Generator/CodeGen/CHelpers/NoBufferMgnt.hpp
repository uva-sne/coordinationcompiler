/* 
 * Copyright (C) 2020 Benjamin Rouxel <benjamin.rouxel@uva.nl> 
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef NOBUFFERMGNT_H
#define NOBUFFERMGNT_H

#include "BufferMgnt.hpp"

/**
 * Prevent from adding code for the buffer management
 * 
 * \see BufferMgnt
 * 
 * Selecting this buffer management policy implies that the user-end code include
 * the necessary code for the buffer management, or that there is no buffer to manage.
 */
class NoBufferMgnt : public BufferMgnt {
public:
    //! Constructor
    NoBufferMgnt() : BufferMgnt() {}
    //! Destructor
    virtual ~NoBufferMgnt() {}
    
    //! \copydoc CodeGenHelper::setParams
    virtual void setParams(const std::map<std::string, std::string> &args) override {}
    //! \copydoc CodeGenHelper::help
    virtual std::string help() override {return "";}
    
    /**
     * \copydoc CodeGenHelper::init
     */
    virtual void init(const SystemModel *s, const config_t *c, const Schedule *se, RuntimeStrategyInterface *st) override {
        CodeGenHelper::init(s,c,se,st);
    }
    /**
     * Return an empty string
     * @return 
     */
    virtual std::string generate_declarations() override {return "";}
    /**
     * Return an empty string
     * @return 
     */
    virtual std::string generate_initialisations() override {return "";}
    /**
     * Return an empty string
     * @return 
     */
    virtual std::string generate_terminaisons() override {return "";}
    /**
     * Return an empty string
     * @return 
     */
    virtual std::string generate_definitions() override {return "";}
    /**
     * Return an empty string in the different parameters
     * @param t
     * @param args
     * @param decl
     * @param inputs
     * @param outputs
     */
    virtual void generate_task_connection(Task *t, std::string *args, std::string *decl, std::string *inputs, std::string *outputs) override;
    /**
     * Return an empty string
     * @param nbtokens
     * @param type
     * @param varname
     * @param forcedinitval
     * @return 
     */
    virtual std::string get_var_declaration(size_t nbtokens, std::string type, std::string varname, std::string forcedinitval="") override {return "";}
    
    /**
     * Return an empty string
     * @param name
     * @return 
     */
    virtual std::string get_buffer_name(std::string name) override {return "";}
};

#endif /* NOBUFFERMGNT_H */


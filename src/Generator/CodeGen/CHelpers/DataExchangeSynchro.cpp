/* 
 * Copyright (C) 2020 Benjamin Rouxel <benjamin.rouxel@uva.nl> 
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "DataExchangeSynchro.hpp"

using namespace std;
namespace ba = boost::algorithm;

IMPLEMENT_HELP(DataExchangeSynchro,
)

string DataExchangeSynchro::generate_declaration() {
    const string tpl = strategy->tpl_getLockDecl();

    string code = "";
    for(Task *t : tg->tasks_it()) {
        for(Connector *c : t->outputs()) {
            if(c->type() == Connector::type_e::SPLITTER && strategy->synchro() == CodeGeneration::synchro_mechanism_e::SEMAPHORESPERCHAN) {
                for(Task::dep_t es : t->successor_tasks()) {
                    for (Connection *conn : es.second) {
                        if (conn->from() != c) continue;

                        string tmp = ba::replace_all_copy(tpl, txttaskname, t->id());
                        ba::replace_all(tmp, txtconname, es.first->id());
                        if (code.find(tmp) == string::npos) //avoid multiple declaration
                            code += tmp;
                    }
                }
            }
            else {
                string tmp = ba::replace_all_copy(tpl, txttaskname, t->id());
                ba::replace_all(tmp, txtconname, c->id());
                if(code.find(tmp) == string::npos) //avoid multiple declaration
                    code += tmp;
            }
        }
    }
    return code;
}

string DataExchangeSynchro::generate_initialisation() {
    const string tpl = strategy->tpl_getLockInit();

    string code = "";
    for(Task *t : tg->tasks_it()) {
        string tmp_code = "";
        for(Connector *c : t->outputs()) {
            if(c->type() == Connector::type_e::SPLITTER && strategy->synchro() == CodeGeneration::synchro_mechanism_e::SEMAPHORESPERCHAN) {
                for(Task::dep_t es : t->successor_tasks()) {
                    for (Connection *conn : es.second) {
                        if (conn->from() != c) continue;
                        tmp_code += ba::replace_all_copy(tpl, txttaskname, t->id());
                        ba::replace_all(tmp_code, txtconname, es.first->id());
                    }
                }
            }
            else {
                tmp_code += ba::replace_all_copy(tpl, txttaskname, t->id());
                ba::replace_all(tmp_code, txtconname, c->id());
            }
        }
        if(t->successor_tasks().size() > 0) // don't look into successors_virtual, as those one might be shared accross processes
            ba::replace_all(tmp_code, txtshared, "0"); // not shared among processses
        code += tmp_code;
    }
    return code;
}

string DataExchangeSynchro::generate_cleanup() {
    const string tpl = strategy->tpl_getLockClean();

    string code = "";
    for(Task *t : tg->tasks_it()) {
        string tmp_code = "";
        for(Connector *c : t->outputs()) {
            if(c->type() == Connector::type_e::SPLITTER && strategy->synchro() == CodeGeneration::synchro_mechanism_e::SEMAPHORESPERCHAN) {
                for(Task::dep_t es : t->successor_tasks()) {
                    for (Connection *conn : es.second) {
                        if (conn->from() != c) continue;
                        tmp_code += ba::replace_all_copy(tpl, txttaskname, t->id());
                        ba::replace_all(tmp_code, txtconname, es.first->id());
                    }
                }
            }
            else {
                tmp_code += ba::replace_all_copy(tpl, txttaskname, t->id());
                ba::replace_all(tmp_code, txtconname, c->id());
            }
        }
        if(t->successor_tasks().size() > 0) // don't look into successors_virtual, as those one might be shared accross processes
            ba::replace_all(tmp_code, txtshared, "0"); // not shared among processses
        code += tmp_code;
    }
    return code;
}

string DataExchangeSynchro::generate_wait_predecessors(BufferMgnt *buffmgnt, Task *t) {
    const string tpl = strategy->tpl_getTaskWaitForInput();
    string code = "";
    for(Connector *elc : t->inputs()) {
        if(elc->type() == Connector::type_e::SPLITTED && strategy->synchro() == CodeGeneration::synchro_mechanism_e::SEMAPHORESPERCHAN) {
            for(Task::dep_t ep : t->predecessor_tasks()) {
                for (Connection *conn : ep.second) {
                    if (conn->from() == elc) {
                        code += ba::replace_all_copy(tpl, "%PREDCONN%", t->id());
                        ba::replace_all(code, "%PREDNAME%", ep.first->id());
                    }
                }
            }
        }
        else if(elc->type() == Connector::type_e::JOINER) {
            code += ba::replace_all_copy(tpl, txttaskname, "&"+t->id());
        }
        else
            code += ba::replace_all_copy(tpl, txttaskname, t->id());

        ba::replace_all(code, txtconname, elc->id());
        ba::replace_all(code, txtbufftypename, buffmgnt->get_buffer_name(t->id()+elc->id()));
        auto it = find_if(t->predecessor_tasks().begin(), t->predecessor_tasks().end(), [&elc](const Task::dep_t &pred) {
            return std::any_of(pred.second.begin(),  pred.second.end(), [&elc](Connection *conn) { return elc == conn->to();});
        });
        if(it == t->predecessor_tasks().end()) {
            it = find_if(t->previous_virtual().begin(), t->previous_virtual().end(), [&elc](const Task::dep_t &pred) {
                return std::any_of(pred.second.begin(),  pred.second.end(), [&elc](Connection *conn) { return elc == conn->to();});
            });
            if(it == t->previous_virtual().end())
            continue;
        }
        Task *pred = it->first;
        Connection *conn = *find_if(it->second.begin(),  it->second.end(), [&elc](Connection *c) { return elc == c->to(); });
        Connector *predcon = conn->from();
        ba::replace_all(code, "%PREDNAME%", pred->id());
        ba::replace_all(code, "%PREDCONN%", predcon->id());
        ba::replace_all(code, "%NBTOKS%", to_string(elc->tokens()));
    }
    return code;
}

string DataExchangeSynchro::generate_release_successors(BufferMgnt *buffmgnt, Task *t) {
    const string tpl = strategy->tpl_getTaskReleaseSuccessor();
    string output = "";
    for(Connector *elc : t->outputs()) {
        //string releasecond = elc->is_part_of_cond_edge() ? elc->id()+" != __bkinit__"+elc->id() : "1";
        string releasecond = "1";
        if(strategy->check_before_release())  {
            if(elc->tokens() == 1) {
                releasecond = elc->id()+" != __bkinit__"+elc->id();
            }
            else {
                releasecond = elc->id()+"[0] != __bkinit__"+elc->id();
            }
        }
        //if elc is a conditional data edge then check if data is here before releasing

        if(elc->type() == Connector::type_e::SPLITTER) {
            string tmp = "";
            if(strategy->synchro() == CodeGeneration::synchro_mechanism_e::SEMAPHORES) {
                size_t cnt = 0;
                for(Task::dep_t es: t->successor_tasks()) {
                    for (Connection *conn: es.second)
                        if (conn->from() == elc)
                            ++cnt;
                }

                tmp = "//if("+releasecond+") {\n";
                tmp += "\tfor(size_t __i__=0 ; __i__ < "+to_string(cnt)+" ; ++__i__) {\n";
                tmp += "\t\tsem_post("+t->id()+"_"+elc->id()+"_lock);\n";
                tmp += "}//}";
            }
            else if(strategy->synchro() == CodeGeneration::synchro_mechanism_e::SEMAPHORESPERCHAN) {
                for(Task::dep_t es : t->successor_tasks()) {
                    for (Connection *conn: es.second)
                        if (conn->from() == elc)
                            tmp += "sem_post(" + t->id() + "_" + es.first->id() + "_lock);\n";
                }
            }

            output += tmp;
        }
        else {
            output += ba::replace_all_copy(tpl, txttaskname, t->id());
            ba::replace_all(output, txtconname, elc->id());
            ba::replace_all(output, txtreleasecond, "if("+releasecond+")");
        }
    }
    return output;
}

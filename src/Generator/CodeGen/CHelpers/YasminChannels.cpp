/*
 * Copyright (C) 2021 brouxel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* 
 * File:   YasminChannels.cpp
 * Author: brouxel
 * 
 * Created on 11 février 2021, 10:41
 */

#include "YasminChannels.hpp"

using namespace std;
namespace ba = boost::algorithm;

std::string YasminChannels::generate_declarations() {
    string buffers = "";

    for(Task *t : tg->tasks_it()) {
        for(Task::dep_t els : t->successor_tasks()) {
            for (Connection *conn: els.second) {
                Task *s = els.first;
                Connector *c = conn->from();
                buffers += "yas_channel_decl(" + t->id() + "_" + s->id() + ", "
                        + conf->datatypes.at(buffer_types_list[t->id() + c->id()].init_type).ctype + ", "
                        + to_string(buffer_types_list[t->id() + c->id()].nbtokens) + ");\n";
            }
        }
    }

    return buffers;
}

string YasminChannels::generate_initialisations() {
    string init = "";
    
    for(Task *t : tg->tasks_it()) {
        SchedJob *st = *(sched->schedjobs(t).begin());
        Version *v = (st != nullptr && !st->selected_version().empty()) ? t->version(st->selected_version()) : t->versions()[0];
        string minper = (st != nullptr) ? to_string(st->min_rt_period()) : "";
        for(Task::dep_t els : t->successor_tasks()) {
            Task *s = els.first;
            SchedJob *sst = *(sched->schedjobs(s).begin());
            Version *sv = (sst != nullptr && !sst->selected_version().empty()) ? s->version(sst->selected_version()) : t->versions()[0];
            string sminper = (sst != nullptr) ? to_string(sst->min_rt_period()) : "";
            init += "yas_channel_connect(yt"+t->id()+v->id()+minper+"id, ";
            init += "yt"+s->id()+sv->id()+sminper+"id, ";
            init += t->id()+"_"+s->id()+");\n";
        }
    }

    return init;
}

void YasminChannels::generate_task_connection(Task *t, string *args, string *decl, string *inputs, string *outputs) {
    for(Connector *elc : t->inputs()) {
        *decl += get_var_declaration(elc->tokens(), elc->token_type(), elc->id());

        if (elc->type() == Connector::type_e::JOINER) {
            string tmp = "\tsize_t __cnt__ = 0;\n";
            for (Task::dep_t elp: t->predecessor_tasks()) {
                for (Connection *conn: elp.second) {
                    if (conn->to() != elc) continue;
                    Connector *pc = conn->from();
                    tmp += "\tfor(size_t __i__ = 0 ; __i__ < " + to_string(pc->tokens()) + " ; ++__i__) {\n";
                    tmp += "\t\tyas_channel_pop(" + elp.first->id() + "_" + t->id() + ", " + elc->id() + "[__cnt__++]);\n";
                    tmp += "\t}\n";
                }
            }
            *inputs += tmp;
        } else {
            for (Task::dep_t elp: t->predecessor_tasks()) {
                for (Connection *conn : elp.second) {
                    if (conn->to() != elc) continue;
                    *inputs += strategy->tpl_getTaskInput(elc->tokens());
                    ba::replace_all(*inputs, txttaskname, t->id());
                    ba::replace_all(*inputs, txtconname, elc->id());
                    ba::replace_all(*inputs, txtnbtokens, to_string(elc->tokens()));
                    ba::replace_all(*inputs, "%CHAN%", elp.first->id() + "_" + t->id());
                }
            }
        }
    }

    for(Connector *elc : t->outputs()) {
        *decl += get_var_declaration(elc->tokens(), elc->token_type(), elc->id());
        string releasecond = "1";
        //if elc is a conditional data edge then check if data is here before pushing
        //if(elc->is_part_of_cond_edge()) {
        if(strategy->check_before_release()) {
            if(elc->tokens() == 1) {
                *decl += get_var_declaration(elc->tokens(), elc->token_type(), " __bkinit__"+elc->id(), elc->id());
                releasecond = elc->id()+" != __bkinit__"+elc->id();
            }
            else {
                *decl += get_var_declaration(1, elc->token_type(), " __bkinit__"+elc->id(), elc->id()+"[0]");
                releasecond = elc->id()+"[0] != __bkinit__"+elc->id();
            }
        }
            
        if(elc->type() == Connector::type_e::SPLITTER) {
            string tmp = "if("+releasecond+") {\n";
            tmp += "\tsize_t __cnt__ = 0;\n";
            for(Task::dep_t els : t->successor_tasks()) {
                for (Connection *conn : els.second) {
                    if (conn->from() != elc) continue;
                    Connector *sc = conn->to();
                    tmp += "\tfor(size_t __i__ = 0 ; __i__ < " + to_string(sc->tokens()) + " ; ++__i__) {\n";
                    tmp += "\t\tyas_channel_push(" + t->id() + "_" + els.first->id() + ", " + elc->id() + "[__cnt__++]);\n";
                    tmp += "\t}\n";
                }
            }
            tmp += "}\n";
            *outputs += tmp;
        }
        else {
            for(Task::dep_t els : t->successor_tasks()) {
                for (Connection *conn : els.second) {
                    if (conn->from() != elc) continue;
                    *outputs += strategy->tpl_getTaskOutput(elc->tokens());
                    ba::replace_all(*outputs, txttaskname, t->id());
                    ba::replace_all(*outputs, txtconname, elc->id());
                    ba::replace_all(*outputs, txtvarconname, elc->id());
                    ba::replace_all(*outputs, txtnbtokens, to_string(elc->tokens()));
                    ba::replace_all(*outputs, "%CHAN%", t->id() + "_" + els.first->id());
                    ba::replace_all(*outputs, txtreleasecond, "if(" + releasecond + ")");
                }
            }
        }
    }
    
    for(Connector *elc : t->inputs())
        *args += elc->id()+", ";
    for(Connector *elc : t->outputs())
        *args += "&"+elc->id()+", ";
}
/* 
 * Copyright (C) 2020 Benjamin Rouxel <benjamin.rouxel@uva.nl> 
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CMAKE_H
#define CMAKE_H

#include "BuildFile.hpp"
#include "Generator/CodeGen/CodeGenerationPatterns.hpp"

/**
 * Helper to generate a CMakeLists.txt for the given application
 * 
 * This helper relies on a template (.tpl) file in which pattern are replaced.
 * 
 * \see BuildFile::help 
 */
class CMake : public BuildFile {
public:
    //! Constructor
    CMake() : BuildFile() {}
    
    //! \copydoc BuildFile::generate(const std::vector<std::string> &inc, const std::map<std::string, std::string> &extra)
    virtual void generate(const std::vector<std::string> &inc, const std::map<std::string, std::string> &extra) override;
private:

};

#endif /* CMAKE_H */


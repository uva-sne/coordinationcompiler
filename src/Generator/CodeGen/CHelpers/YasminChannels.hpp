/*
 * Copyright (C) 2021 brouxel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* 
 * File:   YasminChannels.hpp
 * Author: brouxel
 *
 * Created on 11 février 2021, 10:41
 */

#ifndef YASMINCHANNELS_HPP
#define YASMINCHANNELS_HPP

#include "BufferMgnt.hpp"
#include "Schedule.hpp"


class YasminChannels : public BufferMgnt {
public:
    YasminChannels(): BufferMgnt() {}
public:
    virtual std::string generate_initialisations() override;
    virtual std::string generate_terminaisons() override {return "";}
    
    virtual std::string generate_declarations() override;
    virtual void generate_task_connection(Task *t, std::string *args, std::string *decl, std::string *inputs, std::string *outputs) override;
};

#endif /* YASMINCHANNELS_HPP */


/* 
 * Copyright (C) 2020 Benjamin Rouxel <benjamin.rouxel@uva.nl> 
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "Makefile.hpp"

using namespace std;
namespace ba = boost::algorithm;

void  Makefile::generate(const vector<string> & includes, const map<string, string> &extra) {
    string makefile = strategy->tpl_getBuildFile();
    string rule = strategy->tpl_getBuildRule();
    
    string ssrcfiles = strategy->getMainFileName()+"."+_target_code_language+" \\\n";
    string rules = rule;
    ba::replace_all(rules, txtobject, strategy->getMainFileName()+".o");
    ba::replace_all(rules, txtsrc, strategy->getMainFileName()+"."+_target_code_language);
    vector<boost::filesystem::path> objects;

    vector<boost::filesystem::path> srcfiles;
    srcfiles.insert(srcfiles.end(), additional_srcs.begin(), additional_srcs.end());
    for(boost::filesystem::path srcfile : srcfiles) {
        string tmprule = rule;
        boost::filesystem::path obj_file(srcfile.string() + ".o");
        ba::replace_all(tmprule, txtobject, obj_file.string());
        ba::replace_all(tmprule, txtsrc, srcfile.string());
        rules += tmprule;
        objects.push_back(obj_file);
        ssrcfiles += srcfile.string()+" \\\n";
    }
    for(Task *t : tg->tasks_it()) {
        for(Version *v : t->versions()) {
            if(!v->cfile().empty()) {
                bool exists = true;
                boost::filesystem::path f = v->cfile();
                if(!boost::filesystem::exists(f)) {
                    f = conf->files.config_path / v->cfile();
                    if(!boost::filesystem::exists(f))
                        exists = false;
                }
                if(exists && find(srcfiles.begin(), srcfiles.end(), f) == srcfiles.end()) {
                    srcfiles.push_back(f);
                    ssrcfiles += f.string()+" \\\n";
                }
            }
            
            if(!v->cbinary().empty()) {
                bool exists = true;
                boost::filesystem::path f = v->cbinary();
                if(!boost::filesystem::exists(f)) {
                    f = conf->files.config_path / v->cbinary();
                    if(!boost::filesystem::exists(f))
                        exists = false;
                }
                if(exists && find(objects.begin(), objects.end(), f) == objects.end()) {
                    objects.push_back(f);
                }
            }
        }
    }

    //keep it at the end
    objects.push_back(strategy->getMainFileName()+".o");

    string incs = "";
    for(string inc : includes)
        incs += inc+" ";
    string sobjects = "";
    for(boost::filesystem::path obj : objects)
        sobjects += obj.string()+" \\\n";
    
    string sextraobject = "";
    for(string o : additional_objs)
        sextraobject += o+" \\\n";

    ba::replace_all(rules, txtcompopt, compiler_opts);
    ba::replace_all(rules, txtinc, "");//incs);

    ba::replace_all(makefile, txtcc, compiler);
    ba::replace_all(makefile, txtappname, strategy->app_name());
    ba::replace_all(makefile, txtobject, sobjects);
    ba::replace_all(makefile, txtextraobject, sextraobject);
    ba::replace_all(makefile, txtsrc, ssrcfiles);
    ba::replace_all(makefile, txtlinkopt, linker_opts);
    ba::replace_all(makefile, txtcompopt, compiler_opts);
    ba::replace_all(makefile, txtrules, rules);
    ba::replace_all(makefile, txtcodelang, _target_code_language);
    string upcodelang = (_target_code_language == "cpp") ? "CXX" : boost::to_upper_copy(_target_code_language);
    ba::replace_all(makefile, txtupcodelang, upcodelang);
    
    for(pair<string, string> el : extra)
        ba::replace_all(makefile, el.first, el.second);

    ofstream ofs((strategy->path() / "Makefile").string());
    ofs << makefile;
    ofs.close();
}
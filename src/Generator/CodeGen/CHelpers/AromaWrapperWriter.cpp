/*
 * Copyright (C) 2021 Lukas Miedema <l.miedema@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "AromaWrapperWriter.hpp"
#include "Generator/CodeGen/CodeGenerationPatterns.hpp"

#define INDENT                      "    " // also update AromaWrapper.tpl if you change this
#define HEADER_FILE_EXTENSION       ".axf.hpp" // axf = Admorph Exchange Format
#define SRC_HEADER_FILE_EXTENSION   ".axf.cpp"

namespace ba = boost::algorithm;

void AromaWrapperWriter::generate(const boost::filesystem::path &fn) {
    std::string src_template =
#include "Generator/Runtime/Aroma/AromaWrapper.cpp.tpl"
        ;
    std::string header_template =
#include "Generator/Runtime/Aroma/AromaWrapper.hpp.tpl"
        ;

    ba::replace_all(src_template, txtappname, helper->conf()->files.coord_basename.string());
    ba::replace_all(src_template, txtwrappers, generateRapidWrappers());

    std::ofstream src_file(fn.string() + SRC_HEADER_FILE_EXTENSION);
    src_file << src_template;
    src_file.close();

    ba::replace_all(header_template, txtappname, helper->conf()->files.coord_basename.string());
    ba::replace_all(header_template, txtcinc, generateIncludePathsC());
    ba::replace_all(header_template, txtcppinc, generateIncludePathsCpp());
    ba::replace_all(header_template, txtwrappers, generateRapidForwardDeclarations());
    ba::replace_all(header_template, txtcompositetypes, generateCompositeTypes());

    std::ofstream header_file(fn.string() + HEADER_FILE_EXTENSION);
    header_file << header_template;
    header_file.close();
}

std::string AromaWrapperWriter::generateRapidWrappers() {
    std::string rapids;

    for (Task *task : this->helper->tg()->tasks_it()) {
        // Each task has an associated actor node
        // Generate a wrapper function for this actor node
        std::string wrapper_name = this->helper->getWrapperFunctionName(task);
        std::string node_type = this->helper->getActorNodeType(task) + "_t";
        std::string fn_name, fn_signature;

        std::vector<Version *> versions = task->versions();
        if (versions.empty())
            throw CompilerException("aroma-generator", "No versions for task " + task->id()+ ". A version is required to call the task.");
        fn_name = versions[0]->cname();
        fn_signature = versions[0]->csignature();
        if (fn_name.empty())
            throw CompilerException("aroma-generator", "First version of " + task->id() + " does not have a c_callable.");

        // Build the signature with output parameters first
        rapids
            .append("\nrapid_function(")
            .append(wrapper_name)
            .append(", ")
            .append(node_type)
            .append(", [](");

        // Outgoing connections
        // All broadcasting is handled in the wrapper, as such we create one "out" type per outgoing connection
        if (task->successor_tasks().empty()) {
            // AROMA always requires at least one output, so if there are none create a synthetic one of type bool
            rapids.append("bool &_synthetic_out_");
        } else {
            size_t outIndex = 0;
            for (Task::dep_t outgoing : task->successor_tasks()) {
                if (outgoing.second.size() == 1) // Direct type reference
                    rapids.append(helper->getCType(outgoing.second[0]->from()->token_type()));
                else // Composite type
                    rapids.append(helper->getCompositeTypeName(task, outgoing.first));
                rapids
                        .append(" &")
                        .append("out")
                        .append(to_string(outIndex));
                if (++outIndex < task->successor_tasks().size())
                    rapids.append(", ");
            }
        }

        rapids.append(", "); // comma between output and input

        // Incoming connections
        if (task->predecessor_tasks().empty()) {
            // AROMA always requires at least one input, create a synthetic one
            rapids.append("const bool &_synthetic_in_");
        } else {
            size_t inIndex = 0;
            for (Task::dep_t incoming : task->predecessor_tasks()) {
                rapids.append("const ");
                if (incoming.second.size() == 1) // Direct type reference
                    rapids.append(helper->getCType(incoming.second[0]->to()->token_type()));
                else // Composite type
                    rapids.append(helper->getCompositeTypeName(incoming.first, task));
                rapids
                    .append(" &")
                    .append("in")
                    .append(to_string(inIndex));
                if (++inIndex + 1 < task->predecessor_tasks().size())
                    rapids.append(", ");
            }
        }

        rapids.append(") {\n");


        // Build the prologue
        rapids
            .append(INDENT)
            .append("// Prologue - Inports\n");

        for (Connector *inport : task->inputs()) {
            // Create local
            rapids
                .append(INDENT)
                .append(helper->getCType(inport->token_type()))
                .append(" *inport_")
                .append(inport->id())
                .append(" = ");

            // Find associated connection
            Connection *inconn = nullptr;
            std::unique_ptr<Task::connector_map_t::value_type> compositeOf = nullptr;
            size_t predId = 0;
            size_t connId = 0;

            for (Task::dep_t predecessor : task->predecessor_tasks()) {
                connId = 0;
                for (Connection *conn : predecessor.second) {
                    connId++;
                    if (conn->to() != inport)
                        continue;
                    inconn = conn;
                    compositeOf = std::make_unique<Task::connector_map_t::value_type>(predecessor); // work around no-assignment-op
                    break;
                }
                if (inconn != nullptr)
                    break;
                predId++;
            }
            connId--;
            if (inconn == nullptr)
                throw CompilerException("aroma-generator", "Incoming connection is required, inport " + task->id() + "." + inport->id());

            if (compositeOf->second.size() > 1) {
                // If it is a composite, get it from the composite type
                std::string typeName = helper->getCompositeTypeName(compositeOf->first, task);
                rapids
                    .append("&const_cast<")
                    .append(typeName)
                    .append(" *>(&in")
                    .append(to_string(predId))
                    .append(")->c")
                    .append(to_string(connId))
                    .append(";\n");
            } else {
                // Get it directly
                rapids
                    .append("const_cast<")
                    .append(helper->getCType(inport->token_type()))
                    .append(" *>(&in")
                    .append(to_string(predId))
                    .append(");\n");
            }
        }

        rapids
                .append(INDENT)
                .append("// Prologue - Outports\n");

        for (Connector *outport : task->outputs()) {
            rapids
                .append(INDENT)
                .append(helper->getCType(outport->token_type()))
                .append(" outport_")
                .append(outport->id())
                .append(";\n");
        }

        // Build the call to the actual function
        rapids
            .append("\n")
            .append(INDENT)
            .append(fn_name)
            .append("(");

        // Signature can provide default args that precede the inputs/outputs
        if (!fn_signature.empty())
            rapids.append(fn_signature).append(", ");

        for (Connector *inport : task->inputs()) {
            rapids
                .append("inport_")
                .append(inport->id());
            if (inport != task->inputs().back())
                rapids.append(", ");
        }

        if (!task->inputs().empty() && !task->outputs().empty())
            rapids.append(", "); // Comma between outports and inports

        for (Connector *outport : task->outputs()) {
            rapids
                    .append("&")
                    .append("outport_")
                    .append(outport->id());
            if (outport != task->outputs().back())
                rapids.append(", ");
        }
        rapids.append(");\n");

        // Create the epilogue to write back the outport values
        rapids
            .append("\n")
            .append(INDENT)
            .append("// Epilogue\n");

        size_t outportId = 0;
        for (Task::dep_t successor : task->successor_tasks()) {
            std::string outportName = "out" + to_string(outportId++);
            if (successor.second.size() > 1) {
                size_t connId = 0;
                for (Connection *conn: successor.second) {
                    rapids
                        .append(INDENT)
                        .append(outportName)
                        .append(".c")
                        .append(to_string(connId++))
                        .append(" = outport_")
                        .append(conn->from()->id())
                        .append(";\n");
                }
            } else {
                rapids
                    .append(INDENT)
                    .append(outportName)
                    .append(" = outport_")
                    .append(successor.second[0]->from()->id())
                    .append(";\n");
            }
        }


        // Finish the lambda expression
        rapids.append("});\n");
    }

    return rapids;
}

std::string AromaWrapperWriter::generateRapidForwardDeclarations() {
    std::string decls;
    for (Task *task : this->helper->tg()->tasks_it()) {
        // Build the signature with output parameters first
        // extern generic_function<int_t, int_t, const char> &fork_function;
        decls.append("extern generic_function<");

        // Outgoing connections
        if (task->successor_tasks().empty()) {
            decls.append("bool");
        } else {
            size_t i = 1;
            for (Task::dep_t outgoing : task->successor_tasks()) {
                if (outgoing.second.size() == 1) // Direct type reference
                    decls.append(helper->getCType(outgoing.second[0]->from()->token_type()));
                else // Composite type
                    decls.append(helper->getCompositeTypeName(task, outgoing.first));
                if (++i <= task->successor_tasks().size())
                    decls.append(", ");
            }
        }

        decls.append(", "); // comma between output and input

        // Incoming connections
        if (task->predecessor_tasks().empty()) {
            // AROMA always requires at least one input, create a synthetic one
            decls.append("const bool");
        } else {
            size_t i = 1;
            for (Task::dep_t incoming : task->predecessor_tasks()) {
                decls.append("const ");
                if (incoming.second.size() == 1) // Direct type reference
                    decls.append(helper->getCType(incoming.second[0]->to()->token_type()));
                else // Composite type
                    decls.append(helper->getCompositeTypeName(incoming.first, task));
                if (++i <= task->predecessor_tasks().size())
                    decls.append(", ");
            }
        }

        std::string wrapper_name = this->helper->getWrapperFunctionName(task);
        decls.append("> &").append(wrapper_name).append(";\n");
    }
    return decls;
}

std::string AromaWrapperWriter::generateIncludePathsC() {
    std::string include_paths;
    for (std::string& path : this->include_paths_c) {
        include_paths
            .append("#include \"")
            .append(path)
            .append("\"\n");
    }
    return include_paths;
}

std::string AromaWrapperWriter::generateIncludePathsCpp() {
    std::string include_paths;
    for (std::string& path : this->include_paths_cpp) {
        include_paths
            .append("#include \"")
            .append(path)
            .append("\"\n");
    }
    return include_paths;
}

void AromaWrapperWriter::set_include_paths(const std::vector<std::string> c_includes,
                                           const std::vector<std::string> cpp_includes) {
    this->include_paths_c.insert(this->include_paths_c.end(), c_includes.begin(), c_includes.end());
    this->include_paths_cpp.insert(this->include_paths_cpp.end(), cpp_includes.begin(), cpp_includes.end());
}

std::string AromaWrapperWriter::generateCompositeTypes() {
    std::string out;
    for (Task *src : this->helper->tg()->tasks_it()) {
        for (Task::dep_t sink : src->successor_tasks()) {
            const std::vector<Connection *> &connections = sink.second;
            if (connections.size() <= 1)
                continue;
            out.append(this->helper->getCompositeTypeName(src, sink.first).append(" {\n"));
            for (size_t i = 0; i < connections.size(); i++) {
                out.append(INDENT).append(this->helper->getCType(connections[i]->from()->token_type())).append(" c").append(to_string(i)).append(";\n");
            }
            out.append("};\n");
        }
    }
    return out;
}
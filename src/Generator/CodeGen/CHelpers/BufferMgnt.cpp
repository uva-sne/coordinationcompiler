/* 
 * Copyright (C) 2020 Benjamin Rouxel <benjamin.rouxel@uva.nl> 
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "BufferMgnt.hpp"

using namespace std;
namespace ba = boost::algorithm;

void BufferMgnt::init(const SystemModel *s, const config_t *c, const Schedule *se, RuntimeStrategyInterface *st) {
    CodeGenHelper::init(s,c,se,st);
    
    regex r("[^a-zA-Z0-9_]");
    for(Task *t : tg->tasks_it()) {
        for(Connector *elc : t->inputs()) {
            if(elc->tokens() > 0) {
                string stype = regex_replace(elc->token_type(), r, "");
                ba::replace_all(stype, "struct", "");
                string bdt = stype+to_string(elc->tokens());
                buffer_types write_buf = {.buffTypeName = bdt, .init_type = elc->token_type(), .nbtokens = elc->tokens() };
                buffer_types_list[t->id()+elc->id()] = write_buf;
            }
        }
        for(Connector *elc : t->outputs()) {
            if(elc->tokens() == 0) continue; 
            string stype = regex_replace(elc->token_type(), r, "");
            ba::replace_all(stype, "struct", "");
            string bdt = stype+to_string(elc->tokens());
            buffer_types read_buf = {.buffTypeName = bdt, .init_type = elc->token_type(), .nbtokens = elc->tokens() };
            buffer_types_list[t->id()+elc->id()] = read_buf;
        }
    }
}

string BufferMgnt::generate_initialisations() {
    string init = "";

    for(Task *t : tg->tasks_it()) {
        for(Connector *elc : t->inputs()) {
            if(elc->type() == Connector::type_e::JOINER)
                init += "\tinit_buffer_"+buffer_types_list[t->id()+elc->id()].buffTypeName+"(&"+t->id()+"_"+elc->id()+");\n";
        }
        for(Connector * elc : t->outputs()) {
            if(elc->type() == Connector::type_e::SPLITTER || elc->type() == Connector::type_e::DUPLICATE) {
                for(Task::dep_t els : t->successor_tasks()) {
                    for (Connection *conn : els.second) {
                        if (conn->from() != elc) continue;
                        Connector *to = conn->to();
                        init += "\tinit_buffer_" + buffer_types_list[els.first->id() + to->id()].buffTypeName + "(&" + t->id() + "_" + elc->id() + "___" + els.first->id() + ");\n";
                    }
                }
            }
            else {
                init += "\tinit_buffer_"+buffer_types_list[t->id()+elc->id()].buffTypeName+"(&"+t->id()+"_"+elc->id()+");\n";
            }
        }
    }
    for(Task *t : tg->tasks_it()) {
        for(Task::dep_t elp : t->predecessor_tasks()) {
            for (Connection *conn : elp.second) {
                Connector *to = conn->to(); // inport of t
                Connector *from = conn->from(); // outport of elp.first

                if (to->type() == Connector::type_e::JOINED) continue;

                if (to->type() == Connector::type_e::SPLITTER || to->type() == Connector::type_e::DUPLICATE)
                    init += "\t" + t->id() + "_" + to->id() + " = &" + elp.first->id() + "_" + from->id() + "___" + t->id() + ";\n";
                else
                    init += "\t" + t->id() + "_" + to->id() + " = &" + elp.first->id() + "_" + from->id() + ";\n";
            }
        }
        for(Task::dep_t elp : t->previous_virtual())
            for (Connection *conn : elp.second)
                init += "\t"+t->id()+"_"+conn->to()->id()+" = &"+elp.first->id()+"_"+conn->from()->id()+";\n";
    }

    return init;
}

string BufferMgnt::generate_terminaisons() {
    string init = "";

    for(Task *t : tg->tasks_it()) {
        for(Connector *elc : t->inputs()) {
            if(elc->type() == Connector::type_e::JOINER)
                init += "\tclean_buffer_"+buffer_types_list[t->id()+elc->id()].buffTypeName+"(&"+t->id()+"_"+elc->id()+");\n";
        }
        for(Connector * elc : t->outputs()) {
            if(elc->type() == Connector::type_e::SPLITTER || elc->type() == Connector::type_e::DUPLICATE) {
                for(Task::dep_t els : t->successor_tasks()) {
                    for (Connection *conn: els.second) {
                        if (conn->from() != elc) continue;
                        Connector *sc = conn->to();
                        init += "\tclean_buffer_" + buffer_types_list[els.first->id() + sc->id()].buffTypeName + "(&" + t->id() + "_" + elc->id() + "___" + els.first->id() +
                                ");\n";
                    }
                }
            }
            else
                init += "\tclean_buffer_"+buffer_types_list[t->id()+elc->id()].buffTypeName+"(&"+t->id()+"_"+elc->id()+");\n";
        }
    }
    return init;
}

string BufferMgnt::generate_declarations() {
    string buffers = "";

    for(Task *t : tg->tasks_it()) {
        string prefix = (strategy->synchro() == CodeGeneration::synchro_mechanism_e::NONE)
                            ? t->id()+"_" : "";
        
        for(Connector *elc : t->inputs()) {
            if(elc->type() == Connector::type_e::JOINED) continue;
            if(elc->type() == Connector::type_e::JOINER)
                buffers += "buffer_"+buffer_types_list[t->id()+elc->id()].buffTypeName+"_t "+t->id()+"_"+elc->id()+";\n";
            else
                buffers += "buffer_"+buffer_types_list[t->id()+elc->id()].buffTypeName+"_t *"+t->id()+"_"+elc->id()+";\n";
        }
        for(Connector *elc : t->outputs()) {
            if(elc->type() == Connector::type_e::SPLITTER || elc->type() == Connector::type_e::DUPLICATE) {
                for(Task::dep_t els : t->successor_tasks()) {
                    for (Connection *conn: els.second) {
                        if (conn->from() != elc) continue;
                        Connector *sc = conn->to();
                        buffers += "buffer_" + buffer_types_list[els.first->id() + sc->id()].buffTypeName + "_t " + t->id() + "_" + elc->id() + "___" + els.first->id() + ";\n";
                    }
                }
            }
            else
                buffers += "buffer_"+buffer_types_list[t->id()+elc->id()].buffTypeName+"_t "+t->id()+"_"+elc->id()+";\n";
        }
    }

    return buffers;
}

void BufferMgnt::generate_task_connection(Task *t, string *args, string *decl, string *inputs, string *outputs) {
    for(Connector *elc : t->inputs()) {
        *decl += get_var_declaration(elc->tokens(), elc->token_type(), elc->id());
        *inputs += strategy->tpl_getTaskInput(elc->tokens());
        
        if(elc->type() == Connector::type_e::JOINER)
            ba::replace_all(*inputs, txttaskname, "&"+t->id());
        else
            ba::replace_all(*inputs, txttaskname, t->id());
        
        ba::replace_all(*inputs, txtconname, elc->id());
        ba::replace_all(*inputs, txtnbtokens, to_string(elc->tokens()));
        ba::replace_all(*inputs, txtbufftypename, buffer_types_list[t->id()+elc->id()].buffTypeName);
    }

    for(Connector *elc : t->outputs()) {
        *decl += get_var_declaration(elc->tokens(), elc->token_type(), elc->id());
        string releasecond = "1";
        //if elc is a conditional data edge then check if data is here before pushing
        //if(elc->is_part_of_cond_edge()) {
        if(strategy->check_before_release()) {
            if(elc->tokens() == 1) {
                *decl += get_var_declaration(elc->tokens(), elc->token_type(), " __bkinit__"+elc->id(), elc->id());
                releasecond = elc->id()+" != __bkinit__"+elc->id();
            }
            else {
                *decl += get_var_declaration(1, elc->token_type(), " __bkinit__"+elc->id(), elc->id()+"[0]");
                releasecond = elc->id()+"[0] != __bkinit__"+elc->id();
            }
        }
            
        if(elc->type() == Connector::type_e::SPLITTER) {
            string tmp = "if("+releasecond+") {\n";
            tmp += "\tsize_t __cnt__ = 0;\n";
            for(Task::dep_t els : t->successor_tasks()) {
                for (Connection *conn: els.second) {
                    if (conn->from() != elc) continue;
                    Connector *sc = conn->to();
                    tmp += "\tfor(size_t __i__ = 0 ; __i__ < " + to_string(sc->tokens()) + " ; ++__i__) {\n";
                    tmp += "\t\tpush_" + buffer_types_list[els.first->id() + sc->id()].buffTypeName + "(&" + t->id() + "_" + elc->id() + "___" + els.first->id() + ", " +
                            elc->id() + "[__cnt__++]);\n";
                    tmp += "\t}\n";
                }
            }
            tmp += "}\n";
            *outputs += tmp;
        }
        else if(elc->type() == Connector::type_e::JOINED) {
            Connector *c = nullptr;
            Task *s = nullptr;
            for(Task::dep_t es : t->successor_tasks()) {
                for (Connection *conn: es.second) {
                    if (conn->from() == elc) {
                        s = es.first;
                        c = conn->to();
                    }
                }
            }
            *outputs += strategy->tpl_getTaskOutput(elc->tokens());
            ba::replace_all(*outputs, txttaskname, s->id());
            ba::replace_all(*outputs, txtconname, c->id());
            ba::replace_all(*outputs, txtvarconname, elc->id());
            ba::replace_all(*outputs, txtnbtokens, to_string(c->tokens()));
            ba::replace_all(*outputs, txtbufftypename, buffer_types_list[s->id()+c->id()].buffTypeName);
            ba::replace_all(*outputs, txtreleasecond, "if("+releasecond+")\n");
        }
        else if(elc->type() == Connector::type_e::DUPLICATE) {
            for (Task::dep_t els: t->successor_tasks()) {
                for (Connection *conn: els.second) {
                    if (conn->from() != elc) continue;

                    *outputs += strategy->tpl_getTaskOutput(elc->tokens());
                    ba::replace_all(*outputs, txttaskname, t->id());
                    ba::replace_all(*outputs, txtconname, elc->id() + "___" + els.first->id());
                    ba::replace_all(*outputs, txtvarconname, elc->id());
                    ba::replace_all(*outputs, txtnbtokens, to_string(elc->tokens()));
                    ba::replace_all(*outputs, txtbufftypename, buffer_types_list[t->id() + elc->id()].buffTypeName);
                    ba::replace_all(*outputs, txtreleasecond, "if(" + releasecond + ")\n");
                }
            }
        }
        else {
            *outputs += strategy->tpl_getTaskOutput(elc->tokens());
            ba::replace_all(*outputs, txttaskname, t->id());
            ba::replace_all(*outputs, txtconname, elc->id());
            ba::replace_all(*outputs, txtvarconname, elc->id());
            ba::replace_all(*outputs, txtnbtokens, to_string(elc->tokens()));
            ba::replace_all(*outputs, txtbufftypename, buffer_types_list[t->id()+elc->id()].buffTypeName);
            ba::replace_all(*outputs, txtreleasecond, "if("+releasecond+")");
        }
    }
    
    for(Connector *elc : t->inputs())
        *args += elc->id()+", ";
    for(Connector *elc : t->outputs())
        *args += "&"+elc->id()+", ";
}

string BufferMgnt::generate_definitions() {
    string deftpl = strategy->tpl_getSharedDefs();
    string type_defs_tpl = strategy->tpl_getSharedDefsBufferMgnt();
    string buffer_mgnt = "";
    vector<string> done;
    for(pair<string, buffer_types> bt : buffer_types_list) {
        if(bt.second.nbtokens == 0) continue; //bug don't know why
        if(find(done.begin(), done.end(), bt.second.buffTypeName) != done.end()) continue;
        
        string tmp = ba::replace_all_copy(type_defs_tpl, txtcommtype, bt.second.init_type);
        ba::replace_all(tmp, txtcommsize, to_string(bt.second.nbtokens));
        ba::replace_all(tmp, txtbufftypename, bt.second.buffTypeName);
        buffer_mgnt += tmp;
        done.push_back(bt.second.buffTypeName);
    }

    ba::replace_first(deftpl, txtcommunications, buffer_mgnt);

    return deftpl;
}

string BufferMgnt::get_var_declaration(size_t nbtokens, string type, string varname, string forcedinitval) {
    string decl = "";
    if(nbtokens < 2) {
        decl += conf->datatypes.at(type).ctype+" "+varname;
        if(!forcedinitval.empty())
            decl += " = "+forcedinitval;
        else if(conf->datatypes.count(type) && !conf->datatypes.at(type).initval.empty()) {
            decl += " = "+conf->datatypes.at(type).initval;
        }
        decl += ";\n";
    }
    else {
        decl += conf->datatypes.at(type).ctype + " " + varname + "[" + to_string(nbtokens) + "]";
        if(!forcedinitval.empty())
            decl += " = "+forcedinitval;
        else if(conf->datatypes.count(type) && !conf->datatypes.at(type).initval.empty()) {
            decl += " = { ";
            for(size_t i = 0 ; i < nbtokens ; ++i)
                decl += conf->datatypes.at(type).initval+", ";
            decl = decl.substr(0, decl.size()-2)+" }";
        }
        decl += ";\n";
    }
    return decl;
}

string BufferMgnt::get_buffer_name(string name) {
    return buffer_types_list[name].buffTypeName;
}

string BufferMgnt::getFileName(string codelang) { 
    return codelang == "cpp" ? "shared_defs.hpp" : "shared_defs.h"; 
}
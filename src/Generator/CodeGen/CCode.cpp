/* 
 * Copyright (C) 2017 Benjamin Rouxel <benjamin.rouxel@inria.fr> 
 *                    Irisa/INRIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "CCode.hpp"

namespace ba = boost::algorithm;
using namespace std;

IMPLEMENT_HELP(CCode, 
    HTMLINDENT- includes: list of space separated files to include into the 
    generated main file. \n
    HTMLINDENTTo include system file, use: sys:vector -- this will be transformed
    into &lt;vector&gt;.
    HTMLINDENT- main-mapping: specify on what core the main function thread should 
    be mapped. Needs to be a core id present in the processor list.\n
    HTMLINDENT- maxiter: Specify the number of time the programme should be executed\n
    WARNING, will probably produce incorrect program if used with a non-sequential
    generation, or non-semaphore-based synchronisation.
    HTMLINDENT- initializer: name of a user defined function that should be called while
    initialising the app, e.g. \n
    HTMLINDENT HTMLINDENT void my_init() {\n
    HTMLINDENT HTMLINDENT   do some stuff\n
    HTMLINDENT HTMLINDENT }\n
    HTMLINDENT- cleaner: similar to initializer but called before exiting the program.\n
)

void CCode::setParams(const map<string, string> &args) {
    if(args.count("includes") && !args.at("includes").empty()) {
        vector<string> incs;
        boost::split(incs, args.at("includes"), boost::is_any_of(" "));
        for(string inc : incs) {
            if(inc.empty()) continue;
            addInclude(inc);
        }
    }
    if(args.count("initializer"))
        _initializer = args.at("initializer")+"();";
    if(args.count("cleaner"))
        _cleaner = args.at("cleaner")+"();";

    if (args.count("maxiter")) {
        _maxiter = args.at("maxiter");
//        if(strategy->runtime != CodeGeneration::runtime_container_e::SEQUENTIAL) {
//            if(strategy->synchro != CodeGeneration::synchro_mechanism_e::SEMAPHORES || strategy->synchro != CodeGeneration::synchro_mechanism_e::SEMAPHORESPERCHAN) {
                Utils::WARN(
                    "Specifying a maximum iteration when not generating a sequential version "
                    "of the app, or not using a blocking synchronisation (semaphore-based), "
                    " is likely to produce incorrect output programs."
                );
//            }
//        }
    }

    if(args.count("main-mapping"))
        _main_mapping = args.at("main-mapping");
}
    
void CCode::init(const SystemModel *s, const config_t *c, const Schedule *se, RuntimeStrategyInterface *st) {
    CodeGenHelper::init(s, c, se, st);
    Utils::extractGraphs(tg, &_DAGS);
    
    sort(_DAGS.begin(), _DAGS.end(), [](const vector<Task*> &a, const vector<Task*> &b) {
        uint64_t seqLengthA = 0, seqLengthB = 0;
        for(Task *ela : a)
            seqLengthA += ela->C();
        for(Task *elb : b)
            seqLengthB += elb->C();
        return seqLengthA > seqLengthB;
    });
    
    // fill has_multi_period
    uint64_t period = 0;
    for(vector<Task*> g : _DAGS) {
        for(Task *t : g) {
            if(t->T() > 0) {
                if(period > 0 && period != t->T()) {
                    _has_multiple_period = true;
                    break;
                }
                period = t->T();
            }
        }
        if(_has_multiple_period)
            break;
    }
}

string CCode::includes_to_string() {
    if(CCode::_includes.empty()) return "";
    string incs = "";
    for(string inc : CCode::_includes) {
        if(inc.at(0) != '"' && inc.at(0) != '<')
            inc = '"'+inc+'"';
        incs += "#include "+inc+"\n";
    }
    return incs;
}

void CCode::addInclude(const string &inc) {
    if(inc.substr(0, 4) == "sys:")
        _includes.push_back("<"+inc.substr(4, inc.size())+">");
    else
        _includes.push_back("\""+inc+"\"");
}
/* 
 * Copyright (C) 2020 Benjamin Rouxel <benjamin.rouxel@uva.nl> 
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CODEGENERATIONENUMS_H
#define CODEGENERATIONENUMS_H

namespace CodeGeneration {
    //! Supported synchronisation mechanism
    enum synchro_mechanism_e {
        NONE, //!< No synchronisation
        IFTHENELSE, //!< Check if tokens are present, and returns if not
        SEMAPHORES, //!< 1 semaphore per producer, all consumers hold on it
        SEMAPHORESPERCHAN, //!< 1 semaphore per FIFO buffer
        //SPINLOCK, //!< Busy waiting, similar to IFTHENELSE, but spins on a while-loop
    };
    
    //! Runtime container for tasks/graphs
    enum runtime_container_e {
        SEQUENTIAL, //!< All tasks are sequentially executed following dependencies
        PTHREAD, //!< 1 task = 1 thread
        FORK, //!< 1 task = 1 process
        GROUPEDPTHREAD, //!< If there is a mapping done, all tasks mapped on the same core execute in the same thread, if no mapping 1 thread per graph if multiple graphs
        GROUPEDFORK, //!< Similar to GROUPEDTHREAD but with processes
        FORKANDPTHREAD //!< 1 fork per graph, then 1 thread per task in the graph
    };
    
    //! FIFO buffer implementation
    enum channel_mechanism_e {
        SHAREDMEMORY, //!< using shared memory (thread)
        PIPE, //!< using system pipes
        SOCKET //!< data stream socket @experimental
    };

    //! Sleeping mechanism when a task must wait, like in a static schedule
    enum sleep_mechanism_e {
        SLEEP, //!< use system sleep function
        BUSYLOOP, //!< while-loop
        ALARM //!< set an alarm
    };
}

#endif /* CODEGENERATIONENUMS_H */


/* 
 * Copyright (C) 2020 Benjamin Rouxel <benjamin.rouxel@uva.nl> 
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CODEGENHELPER_H
#define CODEGENHELPER_H

#include <string>
#include <map>

#include "SystemModel.hpp"
#include "Generator/Runtime/RuntimeStrategyInterface.hpp"

/**
 * CodeGenHelper are a collection of classes to help the code generation process
 */
class CodeGenHelper {
protected:
    //! IR of the current application
    const SystemModel *tg;
    //! the global configuration
    const config_t *conf;
    //! current state of the schedule
    const Schedule *sched;
    //! selected runtime environment
    RuntimeStrategyInterface *strategy;

public:
    //! Constructor
    explicit CodeGenHelper() {}
    
    /**
     * Init the helper
     * @param s
     * @param c
     * @param se
     * @param st
     */
    virtual void init(const SystemModel *s, const config_t *c, const Schedule *se, RuntimeStrategyInterface *st) {
        tg = s;
        conf = c;
        sched = se;
        strategy = st;
    }
    /*!
     * \brief Set params extracted from the configuration file
     * \param args   map of parameters
     */
    virtual void setParams(const std::map<std::string, std::string> &args) = 0;
    /*!
     * \brief Show help message 
     * \return help msg
     */
    virtual std::string help() = 0;
};

#endif /* CODEGENHELPER_H */


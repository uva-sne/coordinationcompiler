/*
 * Copyright (C) 2020 Benjamin Rouxel <benjamin.rouxel@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "Yasmin.hpp"

using namespace std;
using namespace yasmin;
namespace ba = boost::algorithm;

Yasmin::Yasmin() : Generator() {
    buffmgnt = new YasminChannels;
    cmake = new CMake;
    ccode = new CCode;
    strategy = new YasminRuntime;
}

IMPLEMENT_HELP_WITH_CODE(Yasmin, { 
            msg += strategy->help()
            + ccode->help()
            +buffmgnt->help()
            +cmake->help();
},
    Generate code to run with the coordination runtime library.
    \n
    execuntilreturn: name of a user defined function that will end the
    HTMLINDENT- execution upon returning, e.g: waitKeyStop which could be implemented
    HTMLINDENT HTMLINDENTvoid waitKeyStop() { \n
    HTMLINDENT HTMLINDENT  do { c = getchar(); } while(c != 'q');\n
    HTMLINDENT HTMLINDENT}\n
    HTMLINDENT- force_posix_synchro_primitives, [CONFIG_FALSE-CONFIG_TRUE], default 
        CONFIG_FALSE: There is the possibility to use
    HTMLINDENT- POSIX primitives for synchronisation within the library 
        (semaphores, barriers ...). But another implementation is also
        available which uses lock-free algorithms. When set to CONFIG_TRUE,
         POSIX primitives are used.\n
    HTMLINDENT- yasminpath: path to root of the Yasmin Library.\n
    HTMLINDENT- prehook: function to run before each task\n
    HTMLINDENT- posthook: function to run after each task\n
)

void Yasmin::forward_params(const map<string, string> &args) {
    if(args.count("execuntilreturn")) {
        execuntilreturn = args.at("execuntilreturn");
        if(execuntilreturn.at(execuntilreturn.length()-1) != ';') {
            if(execuntilreturn.at(execuntilreturn.length()-1) != ')')
                execuntilreturn += "()";
            execuntilreturn += ";";
        }
    }

    if(args.count("force_posix_synchro_primitives") && args.at("force_posix_synchro_primitives") == CONFIG_TRUE)
        coordlib_configs["YAS_SYNCHRO_FORCE_POSIX"] = "";
    
    if(args.count("yasminpath"))
        yasmin_path = args.at("yasminpath");
    if(yasmin_path.empty())
        throw CompilerException("yasmin runtime generator", "Missing parameter: yasminpath");
    
    if(args.count("trace"))
        trace = (args.at("trace") == CONFIG_TRUE);
    
    if(args.count("preemptive") && args.at("preemptive") == CONFIG_TRUE)
        preemption = true;
    
    if(args.count("prehook"))
        prehook = args.at("prehook");
    if(args.count("posthook"))
        posthook = args.at("posthook");

    buffmgnt->setParams(args);
    cmake->setParams(args);
    ccode->setParams(args);
    strategy->setParams(args);
    strategy->synchro(CodeGeneration::synchro_mechanism_e::IFTHENELSE);
}

void Yasmin::generate(const boost::filesystem::path& fn) {
    if(sched->status() == ScheduleProperties::sched_statue_e::SCHED_UNSCHEDULABLE)
        throw CompilerException("code generation", "Can't generate code as the application is unschedulable");

    mapping_partitioned = (sched->mapping_mode() == ScheduleProperties::Mapping::MAP_PARTITIONED);
    schedule_offline = (sched->status() == ScheduleProperties::sched_statue_e::SCHED_COMPLETE);
    
    buffmgnt->init(tg, conf, sched, strategy);
    cmake->init(tg, conf, sched, strategy);
    ccode->init(tg, conf, sched, strategy);
    
    fill_internal_procid();
    strategy->generatePath(fn, ccode->codegendir());

    generateBufferMgnt();
    generateMainCode();
    generateConfig();
    
    map<string, string> extra;
    extra["%YASRUNTIME%"] = yasmin_path;

    cmake->generate(ccode->includes(), extra);
}

void Yasmin::generateMainCode() {
    string setup = "", decl_tasks = "", codetasks = "";
    if(mapping_partitioned) {
        if(schedule_offline) {
            generateOffLineSchedule(setup, decl_tasks, codetasks);
        }
        else { //schedule online
            generatePartitionedSchedule(setup, decl_tasks, codetasks);
        }
    }
    else { //mapping global
        generateGlobalOnLineSchedule(setup, decl_tasks, codetasks);
    }
    
    string main = strategy->tpl_getMain();
    ba::replace_first(main, txtinc, ccode->includes_to_string());
    ba::replace_first(main, "%COORDTASK%", decl_tasks+codetasks);
    ba::replace_first(main, "%SETUPTASKS%", setup);
    ba::replace_first(main, txtinit, ccode->initializer());
    ba::replace_first(main, txtcleaner, ccode->cleaner());
    ba::replace_first(main, "%ENDING%", execuntilreturn);
    
    ba::replace_first(main, txtbuffers, buffmgnt->generate_declarations());
    ba::replace_first(main, txtbufinit, buffmgnt->generate_initialisations());
    ba::replace_first(main, txtbufclean, buffmgnt->generate_terminaisons());
    
    ofstream ofs((strategy->path() / strategy->getMainFileName()).string()+"."+cmake->target_code_language());
    ofs << main;
    ofs.close();
}

void Yasmin::generateConfig() {
    size_t nbcpu = 0;
    coordlib_configs["YAS_THREAD_AFFINITIES"] = "{";
    coordlib_configs["YAS_THREAD_PRIORITIES"] = "{";
    for(ComputeUnit *c : tg->processors()) {
        if(c->proc_class != ArchitectureProperties::ComputeUnitType::CUT_CPU) continue;
        ++nbcpu;
        
        coordlib_configs["YAS_THREAD_AFFINITIES"] += to_string(c->sysID)+",";
        coordlib_configs["YAS_THREAD_PRIORITIES"] += strategy->default_priority()+",";
    }
    coordlib_configs["YAS_THREAD_PRIORITIES"] = coordlib_configs["YAS_THREAD_PRIORITIES"].erase(coordlib_configs["YAS_THREAD_PRIORITIES"].size()-1, 1);
    coordlib_configs["YAS_THREAD_AFFINITIES"] = coordlib_configs["YAS_THREAD_AFFINITIES"].erase(coordlib_configs["YAS_THREAD_AFFINITIES"].size()-1, 1);
    coordlib_configs["YAS_THREAD_PRIORITIES"] += "}";
    coordlib_configs["YAS_THREAD_AFFINITIES"] += "}";
    coordlib_configs["YAS_THREAD_SIZE"] = to_string(nbcpu);
    
    coordlib_configs["YAS_THREAD_NAME"] = "\""+strategy->app_name()+"\"";
    
    if(!ccode->main_mapping().empty()) {
        ComputeUnit *proc = tg->processor(ccode->main_mapping());
        if(proc != nullptr)
            coordlib_configs["YAS_MAIN_THREAD_AFFINITY"] = to_string(proc->sysID);
        else
            coordlib_configs["YAS_MAIN_THREAD_AFFINITY"] = ccode->main_mapping();
        coordlib_configs["YAS_SCHEDULER_THREAD_AFFINITY"] = coordlib_configs["YAS_MAIN_THREAD_AFFINITY"];
    }
    coordlib_configs["YAS_MAIN_THREAD_PRIORITY"] = "10";
    coordlib_configs["YAS_SCHEDULER_THREAD_PRIORITY"] = "10";
    
    coordlib_configs["YAS_CLOCK"] = "CLOCK_MONOTONIC";
    
    coordlib_configs["YAS_THREAD_SCHED_POLICY"] = strategy->os_scheduler();
    
    coordlib_configs["YAS_ALARM_SIGNAL"] = "SIGRTMIN + 0";
    
    if(trace)
        coordlib_configs["YAS_TRACE"] = "";
    
    coordlib_configs["YAS_PREEMPTION_MODEL"] = (preemption) ? "YAS_SCHED_PREEMPTIVE" : "YAS_SCHED_NONPREEMPTIVE" ;
    
    size_t channel_size = 0;
    for(Task *t : tg->tasks_it()) {
        channel_size += t->successor_tasks().size();
    }
    if(channel_size > 0) 
        coordlib_configs["YAS_CHANNEL_SIZE"] = to_string(channel_size);
    
    if(!prehook.empty())
        coordlib_configs["YAS_HOOK_PRE_TASK_CALL"] = prehook+"\nvoid "+prehook+"(void*);";
    if(!posthook.empty())
        coordlib_configs["YAS_HOOK_POST_TASK_CALL"] = posthook+"\nvoid "+posthook+"(void*);";

    if (tg->has_hyperperiod())
        coordlib_configs["YAS_HYPERPERIOD"] = to_string(tg->hyperperiod());

    string sconfigs = "";
    for(pair<string, string> el : coordlib_configs)
        sconfigs += "#define "+el.first+" "+el.second+"\n";
    string filename = "yasmin_config.h";
    ofstream ofs((strategy->path() / filename).string());
    ofs << ba::replace_first_copy(strategy->tpl_getConfig(), "%CONFIGS%", sconfigs);
    ofs.close();
}

void Yasmin::generateOffLineSchedule(string &setup, string &decl_tasks, string &code_tasks) {
    Utils::DEBUG("Coordinate Runtime Library + Off-line scheduling");
    string coord_task_tpl = strategy->tpl_getTask();
    
    for(Task *t : tg->tasks_it()) {
        SchedJob *st = *(sched->schedjobs(t).begin());
        Version *v = (!st->selected_version().empty()) ? t->version(st->selected_version()) : t->versions()[0];
        decl_tasks += "void __cec_"+t->id()+"_"+v->id()+"(void*);\n";
        
        string inputs, outputs, args, decl;
        buffmgnt->generate_task_connection(t, &args, &decl, &inputs, &outputs);
        
        string structparams = "";
        string initparams = "";
        string sendinitparams = "NULL";
        string fncallable = gen_call_signature(t, v, args, &structparams, &initparams, &sendinitparams);
        
        code_tasks += ba::replace_all_copy(coord_task_tpl, txttaskname, t->id());
        ba::replace_all(code_tasks, txtversionname, v->id());
        ba::replace_all(code_tasks, txtperiod, to_string(t->T()));
        ba::replace_first(code_tasks, txtcallin, decl+inputs);
        ba::replace_first(code_tasks, txtcallable, fncallable);
        ba::replace_first(code_tasks, txtcallout, outputs);
        ba::replace_first(code_tasks, txtreleasesuc, "");
        ba::replace_first(code_tasks, txtwaitpred, "");
        setup += structparams;
        setup += initparams;
        
        for(SchedJob *st : sched->schedjobs(t)) {
            unsigned long long offset = start_iter_overhead + st->rt()+ t->predecessor_tasks().size()*thread_task_wrapper_overhead;
            setup += "yas_task_data_t yt"+t->id()+v->id()+to_string(st->min_rt_period())+"param;\n";
            setup += "yt"+t->id()+v->id()+to_string(st->min_rt_period())+"param.offset = "+to_string(offset)+";\n";
            setup += "yt"+t->id()+v->id()+to_string(st->min_rt_period())+"param.virt_core_id = "+proc_internal_id[sched->get_mapping(st)->core()]+";\n";
            
            setup += "yas_task_id_t yt"+t->id()+v->id()+to_string(st->min_rt_period())+"id = ";
            setup += "yas_task_decl(&yt"+t->id()+v->id()+to_string(st->min_rt_period())+"param, __cec_"+t->id()+"_"+v->id()+", "+sendinitparams+");\n";
        }
    }
    
    coordlib_configs["YAS_TASK_MAPPING"] = "YAS_MAPPING_STATIC_SCHEME";
    coordlib_configs["YAS_TASK_POOL_JOBS_SIZE"] = to_string(tg->tasks().size());
}
void Yasmin::generatePartitionedSchedule(string &setup, string &decl_tasks, string &code_tasks) {
    Utils::DEBUG("Coordinate Runtime Library + On-line Partitioned scheduling");
    generateGlobalOnLineSchedule(setup, decl_tasks, code_tasks);
    coordlib_configs["YAS_TASK_MAPPING"] = "YAS_MAPPING_PARTITIONED_SCHEME";
}
void Yasmin::generateGlobalOnLineSchedule(string &setup, string &decl_tasks, string &code_tasks) {
    Utils::DEBUG("Coordinate Runtime Library + On-line Global scheduling");
    
    string coord_task_tpl = strategy->tpl_getTask();
    
    set<ArchitectureProperties::ComputeUnitType> accelerators;
    
    size_t nb_periodic_task = 0, non_periodic_task_size = 0;
    size_t nbjobs = 0;
    size_t version_size = 0;
    bool with_prio = false;
    for(Task *t : tg->tasks_it()) {
        if(t->versions().size() > version_size)
            version_size = t->versions().size();
        
        setup += "yas_task_data_t yt"+t->id()+"param;\n";
        setup += "yas_build_task_name(yt"+t->id()+"param.name, \""+t->id()+"\");\n";
        SchedJob *st = *(sched->schedjobs(t).begin());
        if(st != nullptr) {
            if(st->priority() > 0) {
                setup += "yt"+t->id()+"param.priority = "+to_string(st->priority())+";\n";
                with_prio = true;
            }
            SchedCore *c = sched->get_mapping(st);
            if(c != NULL) {
                setup += "yt"+t->id()+"param.virt_core_id = "+proc_internal_id[c->core()]+";\n";
            }
        }
        nbjobs += tg->hyperperiod() / t->T();
        if(t->predecessor_tasks().size() == 0) {
            ++nb_periodic_task;
            setup += "yt"+t->id()+"param.period = "+to_string(t->T())+";\n";
        }
        else {
            ++non_periodic_task_size;
            setup += "yt"+t->id()+"param.period = 0;\n";
        }
        setup += "yt"+t->id()+"param.deadline = "+to_string(t->D())+";\n";
        
        setup += "yas_task_id_t yt"+t->id()+"id;\n";
        setup += "yt"+t->id()+"id = yas_task_decl(&yt"+t->id()+"param, NULL, NULL);\n";
        
        for(Version *v : t->versions()) {
            decl_tasks += "void __cec_"+t->id()+"_"+v->id()+"(void*);\n";
            
            string inputs, outputs, args, decl;
            buffmgnt->generate_task_connection(t, &args, &decl, &inputs, &outputs);
            
            string structparams = "";
            string initparams = "";
            string sendinitparams = "NULL";
            string fncallable = gen_call_signature(t, v, args, &structparams, &initparams, &sendinitparams);
            
            code_tasks += ba::replace_all_copy(coord_task_tpl, txttaskname, t->id());
            ba::replace_all(code_tasks, txtversionname, v->id());
            ba::replace_all(code_tasks, txtperiod, to_string(t->T()));
            ba::replace_first(code_tasks, txtcallin, decl+inputs);
            ba::replace_first(code_tasks, txtcallable, fncallable);
            ba::replace_first(code_tasks, txtcallout, outputs);
            
            setup += "yas_version_id_t ytv"+t->id()+v->id()+"id;\n";
            setup += structparams;
            setup += initparams;
            setup += "ytv"+t->id()+v->id()+"id = yas_version_decl(yt"+t->id()+"id, __cec_"+t->id()+"_"+v->id()+", "+sendinitparams+", NULL);\n";
            
            //workaround for hw accelerator resources
            // what happens if there is 2 gpus, and the task uses 1 ?
            for(ComputeUnit *p : v->force_mapping_proc()) {
                if(p->proc_class == ArchitectureProperties::ComputeUnitType::CUT_CPU) continue;
                
                accelerators.insert(p->proc_class);
                setup += "yas_hwaccel_use(yt"+t->id()+"id, ytv"+t->id()+v->id()+"id, yr"+ArchitectureProperties::to_string(p->proc_class)+"id);\n";
            }
        }
    }
    
    if(accelerators.size() > 0) {
        string hwaccel = "";
        for(ArchitectureProperties::ComputeUnitType e : accelerators) {
            hwaccel +="yas_hwaccel_id_t yr"+ArchitectureProperties::to_string(e)+"id = ";
            hwaccel += "yas_hwaccel_decl(\""+ArchitectureProperties::to_string(e)+"\");\n";
        }
        setup = hwaccel + setup;
        coordlib_configs["YAS_HWACCEL_SIZE"] = to_string(accelerators.size());
    }

    coordlib_configs["YAS_TASK_VERSION_SIZE"] = to_string(version_size);
    if(nb_periodic_task > 0)
        coordlib_configs["YAS_PERIODIC_TASK_SIZE"] = to_string(nb_periodic_task);
    if(non_periodic_task_size > 0)
        coordlib_configs["YAS_NONRECURRING_TASK_SIZE"] = to_string(non_periodic_task_size);
    coordlib_configs["YAS_TASK_MAPPING"] = "YAS_MAPPING_GLOBAL_SCHEME";
    coordlib_configs["YAS_TASK_POOL_JOBS_SIZE"] = to_string((nbjobs > tg->processors().size()*100) ? tg->processors().size()*100 : nbjobs);
    
    if(with_prio)
        coordlib_configs["YAS_PRIORITY_ASSIGNMENT"] = "YAS_TASK_PRIORITY_USERPRIO";
    else 
        coordlib_configs["YAS_PRIORITY_ASSIGNMENT"] = "YAS_TASK_PRIORITY_EDF";
    
    coordlib_configs["YAS_SCHEDULER_IMPLEM"] = "YAS_SCHED_IMPLEM_THR";
}

void Yasmin::generateBufferMgnt() {
    string deffile = buffmgnt->generate_definitions();
    ba::replace_first(deffile, txtinc, ccode->includes_to_string());
    ofstream ofs((strategy->path() / buffmgnt->getFileName(cmake->target_code_language())).string());
    ofs << deffile;
    ofs.close();
    ccode->addInclude(buffmgnt->getFileName(cmake->target_code_language()));
}

void Yasmin::fill_internal_procid() {
    size_t i = 0;
    for(ComputeUnit *p : tg->processors()) {
        proc_internal_id[p] = to_string(i++);
    }
}

string Yasmin::gen_call_signature(Task *t, Version *v, const string &args, string *structparams, string *initparams, string *sendinitparams) {
    string getparams = "";
    string callparams = "";
    string fncallable = "";
    
    if(v->cname().empty())
        v->cname(t->id()+"_"+v->id());
    if(!v->csignature().empty()) {
        string tmp = v->csignature();
        //we will see what I have for the signature inside the nfp, but for FMRadio I need it due to the replication of components
        //  for the tests against mollison, I need to pass an int
        vector<string> decls;
        boost::split(decls, tmp, boost::is_any_of(","));
        *structparams = "struct param_"+t->id()+" {\n";
        *initparams = "struct param_"+t->id()+" p_"+t->id()+";\n";
        *sendinitparams = "&p_"+t->id();
        for(size_t i = 0 ; i < decls.size() ; ++i) {
            string decl = decls[i];
            boost::trim(decl);
            vector<string> d;
            boost::split(d, decl, boost::is_any_of(" "));
            string type = d[0];
            string var = d[1];
            string val = d[2];
            *structparams += "\t"+type+" "+var+";\n";
            getparams += type+" "+var+" = *(("+type+" *) __cec_data);\n";
            if(i < decls.size()-1)
                getparams += "__cec_data = __cec_data + sizeof("+type+");\n";
            callparams += var+",";
            *initparams += "p_"+t->id()+"."+var+" = "+val+";\n";
        }
        *structparams +="};\n";
        callparams = callparams.substr(0, callparams.size()-1);

        Utils::WARN("Todo feature in "+string(__func__));
        fncallable = getparams;
        fncallable += v->cname()+"("+callparams; 
        if(!args.empty())
            fncallable += ", "+args.substr(0, args.size()-2);
        fncallable += ");";
    }
    else
        fncallable = v->cname()+"("+args.substr(0, args.size()-2)+");";

    return fncallable;
}

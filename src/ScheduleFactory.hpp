/*
 * Copyright (C) 2022 Benjamin Rouxel <benjamin.rouxel@unimore.it>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* 
 * File:   ScheduleFactory.hpp
 * Author: Benjamin Rouxel <benjamin.rouxel@unimore.it>
 *
 * Created on 26 mai 2022, 15:14
 */

#ifndef SCHEDULEFACTORY_HPP
#define SCHEDULEFACTORY_HPP

#include "Schedule.hpp"

class ScheduleFactory {
public:
    ScheduleFactory();
    ScheduleFactory(const ScheduleFactory& orig);
    virtual ~ScheduleFactory();
    
    /**
     * Build the list of #SchedJob and #SchedPacket to be scheduled
     */
    static void build_elements(Schedule *);

    /**
     * Build the list of #SchedTask and #SchedPacket to be scheduled
     * 
     * Compared to build_elements, it doesn't create all jobs. If a task generate
     * multiple jobs, only the first one is added
     */
    static void build_minimal_elements(Schedule *);

    static SchedJob *build_job(Schedule *, const Task *t, uint128_t release);
    static bool try_build_job(Schedule *, const Task *t, uint128_t release, SchedJob **);

    static SchedPacket *build_packet(Schedule *, SchedJob *t, timinginfos_t release, const std::string &dirlbl, SchedJob *senderOrDest, uint32_t packetnum);
    static bool try_build_packet(Schedule *, SchedJob *t, timinginfos_t release, const std::string &dirlbl, SchedJob *senderOrDest, uint32_t packetnum, SchedPacket **);
    
    /**
     * Add a core to the list of cores
     *
     * @remark A check is performed to seek if the core is not already added
     *
     * @param c
     * @return The either newly created #SchedCore or it's existing instance is returned.
     */
    static SchedCore *add_core(Schedule *, ComputeUnit *c);

    /**
     * Add a resource to the list of resources
     *
     * @remark A check is performed to seek if the resource is not already added
     *
     * @param c
     * @return The either newly created #SchedResource or it's existing instance is returned.
     */
    static SchedResource *add_resource(Schedule *, ResourceUnit *c);

    static SchedEltPtr shallow_copy(SchedElt *src);

protected:
    
    /**
     * Build the list of #SchedPacket for a #SchedJob when not using burst. Each communication
     * is a single packet.
     * @param SchedJob
     */
    static void build_packet_list_noburst(Schedule *, SchedJob *schedjob);

    /**
     * Populate the successors/predecessors list of #SchedJob and link read/write packets
     * @param st
     * @param pool
     */
    static void build_successors_noburst(Schedule *, SchedJob *st, std::map<Task *, SchedJob *> *pool);

    /**
     * Build the list of #SchedPacket for a #SchedJob when using burst communication.
     * Successors/predecessors are also populated here.
     *
     * @param schedjob
     * @param pool to connect packet and to/from #SchedJob
     */
    static void build_packet_list_burst(Schedule *, SchedJob *schedjob, std::map<Task *, SchedJob *> *pool);
};

#endif /* SCHEDULEFACTORY_HPP */


/* 
 * Copyright (C) 2019 Benjamin Rouxel <benjamin.rouxel@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "Schedule.hpp"
#include "Utils/Graph.hpp"
#include "ScheduleFactory.hpp"
#include <unordered_set>
#include <locale>
#include <boost/range/algorithm.hpp>
using namespace std;

void Schedule::copy_schedule(const Schedule& rhs) {
    _conf = rhs._conf;
    _tg = rhs._tg;

    _solvetime = rhs._solvetime;
    _makespan = rhs._makespan;
    _energy = rhs._energy;
    _status = rhs._status;

    _mapping_mode = rhs._mapping_mode;
    _priority_mode = rhs._priority_mode;
    _preemption_mode = rhs._preemption_mode;

    std::map<SchedElt*, SchedElt*> pool;
    for(const SchedEltPtr &original : rhs.elements()) {
        SchedEltPtr copy = ScheduleFactory::shallow_copy(original.get());
        pool[original.get()] = copy.get();
        _elements_lut[copy->id()] = std::move(copy);
    }
    for(const SchedEltPtr &original : rhs.elements()) {
        if(Utils::isa<SchedJob*>(original.get()))
            ((SchedJob*)pool[original.get()])->populate(*original, pool);
        else
            ((SchedPacket*)pool[original.get()])->populate(*original, pool);
    }

    for(SchedElt* el : rhs._scheduled_elements)
        _scheduled_elements.push_back(pool.at(el));

    for(SchedCore* c : rhs._schedcores) {
        SchedCore *nc = new SchedCore(*c); // will copy its mem region, which will copy its booking
        _schedcores.push_back(nc);
    }
    sort(_schedcores.begin(), _schedcores.end(), [](SchedCore *a, SchedCore *b) { return a->core()->id < b->core()->id; });

    for(pair<SchedElt*, SchedCore*> el : rhs._mapping) {
        SchedCore *nc = schedcore(el.second->core());
        _mapping[pool.at(el.first)] = nc;
    }

    for(pair<SchedElt*, SchedSpmRegion *> sm : rhs._spm_mapping) {
        SchedCore *nc = schedcore(sm.second->core()->core());
        SchedSpmRegion *nspm = nc->schedspmregion(sm.second->label());
        _spm_mapping[pool.at(sm.first)] = nspm;
    }

    for(SchedResource* el : rhs._schedresources) {
        SchedResource *sr = new SchedResource(*el); //will copy its booking
        _schedresources.push_back(sr);
    }

    _stats.insert(_stats.end(), rhs._stats.begin(), rhs._stats.end());
}

Schedule::Schedule(const Schedule& rhs) {
    copy_schedule(rhs);
}

Schedule::~Schedule() {
    clear();
}

void Schedule::clear() {
    clear_assigned();

    for (auto& c: _schedcores)
        delete c;
    for (auto& r: _schedresources)
        delete r;
    _elements_lut.clear();
    _schedcores.clear();
    _spm_mapping.clear();
    _schedresources.clear();
}

void Schedule::clear_assigned() {
    _scheduled_elements.clear();
    _mapping.clear();
    _stats.clear();
    _delay_impacted_elements.clear();
    this->status(ScheduleProperties::sched_statue_e::SCHED_NOTDONE);
}

Schedule& Schedule::operator= (const Schedule& rhs) {
    clear();
    copy_schedule(rhs);
    return *this;
}

void Schedule::compute_makespan() {
    _makespan = 0;
    for(SchedElt* el : _scheduled_elements) {
        if(el->rt() + el->wct() > _makespan)
            _makespan = el->rt() + el->wct();
    }
}

void Schedule::compute_energy() {
  // loop over tasks and cores. compute energy per task & selected core. including
    _energy = 0;
    frequencyinfo_t frequency = 0;
    for(SchedElt* el : _scheduled_elements) {
        _energy += el->wce();
    }

    timinginfos_t constUnitFactor = Utils::get_energy_units_per_power_time_unit(_tg->time_unit(), _tg->power_unit(), _tg->energy_unit());

    // Static power
    _energy += (_makespan * _tg->base_watt())/constUnitFactor;

    energycons_t temp_energy = 0;

    //computes energy consumption for each voltage island
    // TODO: the coprocessors are also voltage islands!
    for (auto vi: _tg->voltage_islands()){
        std::vector<frequencyinfo_t> frequency_time; // first the frequency and then a vector of start and end times

        //adds time to each frequency time
        for (size_t ts = 0; ts <= makespan(); ts++) {
            frequency_time.push_back(0);
            for (std::pair<SchedElt *, SchedCore *> element_mapping: _mapping) { // going over each mapped task and its picked core.
                if (element_mapping.first->rt() <= ts && ts < element_mapping.first->rt() + element_mapping.first->wct()) { //if ts is during the element
                    // grab all the cores on a voltage island and check if the task is mapped on this core
                    for (auto proc_id: vi->impacted_processors) {
                        // if the element is mapped onto the target processor proc_id; only one mapping possible
                        if (element_mapping.second->core() == proc_id) {
                            frequency = element_mapping.first->schedtask()->frequency();
                        }
                        // A gpu can be represented as either a compute unit (i.e. regular processor). In which case it falls into the category above.
                        // Or as a separate co-processor, in which case it falls into the category below.
                        else if(element_mapping.first->schedtask()->gpu_required() && proc_id->proc_class == ArchitectureProperties::CUT_SEPARATE_COPROCESSOR){
                            frequency = element_mapping.first->schedtask()->gpu_frequency();
                        }else{
                            frequency = 0;
                        }

                        if (frequency_time[ts] == 0 || frequency_time[ts] == frequency) {
                            frequency_time[ts] = frequency;
                        } else if (frequency_time[ts] != frequency && frequency != 0 ) {
                            throw CompilerException("Schedule::compute_energy in Schedule.cpp", "There are DVFS mismatches in the schedule. Check your scheduler.");
                        }
                    }
                }
            }
        }

        for (size_t ts = 0; ts <= makespan(); ts++) {
            //does the actual energy update for each frequency in each voltage island
            for (auto freq_watt: vi->frequency_watt){
                if(freq_watt.first == frequency_time[ts]){
                    temp_energy += freq_watt.second;
                }
            }
        }
    }

    _energy += temp_energy / constUnitFactor;
}
SchedCore* Schedule::get_mapping(const SchedElt* e) {
    SchedElt *ee = const_cast<SchedElt*>(e);
    if(_mapping.count(ee->schedtask()))
        return _mapping[ee->schedtask()];
    return nullptr;
}
SchedCore* Schedule::get_mapping(SchedElt* e) {
    if(_mapping.count(e->schedtask()))
        return _mapping[e->schedtask()];
    return nullptr;
}
SchedCore* Schedule::get_mapping(SchedElt* e) const {
    if(_mapping.count(e->schedtask()))
        return _mapping.at(e->schedtask());
    return nullptr;
}

void Schedule::map_core(SchedElt* e, SchedCore* p) {
    _mapping[e->schedtask()] = p;
}

void Schedule::map_spm(SchedElt* e, SchedSpmRegion* p, uint64_t start, uint64_t _end) {
    _spm_mapping[e] = p;
    if(p->size() < e->mem_data()) {
        p->core()->available_spm_size(p->core()->available_spm_size() - e->mem_data() - p->size());
        p->size(e->mem_data());
    }
    p->book(e->id(), start, _end);
}

SchedSpmRegion* Schedule::get_spm_mapping(SchedElt *e) {
     if(_spm_mapping.count(e))
        return _spm_mapping[e];
    return nullptr;
}
SchedSpmRegion* Schedule::get_spm_mapping(SchedElt *e) const {
     if(_spm_mapping.count(e))
        return _spm_mapping.at(e);
    return nullptr;
}

SchedCore *Schedule::schedcore(ComputeUnit *proc) const {
    for(SchedCore *c : _schedcores) {
        if(c->core() == proc)
            return c;
    }
    return nullptr;
}
SchedCore *Schedule::schedcore(ComputeUnit *t) {
    return const_cast<const Schedule*>(this)->schedcore(t);
}

SchedResource *Schedule::schedresource(const std::string &lbl) const {
    for(SchedResource *c : _schedresources) {
        if(c->label() == lbl)
            return c;
    }
    return nullptr;
}
SchedResource *Schedule::schedresource(const std::string &lbl) {
    return const_cast<const Schedule*>(this)->schedresource(lbl);
}

SchedJob *Schedule::schedjob(const string &jid) {
    return const_cast<const Schedule*>(this)->schedjob(jid);
}
SchedJob *Schedule::schedjob(const string &jid) const {
    if(_elements_lut.count(jid) && _elements_lut.at(jid)->type() == SchedElt::Type::TASK)
        return _elements_lut.at(jid)->schedtask();
    return nullptr;
}

void Schedule::schedule(SchedElt* e) {
    if(!_elements_lut.count(e->id()) || _elements_lut[e->id()].get() != e)
        throw CompilerException("Schedule::schedule", "Problem of ownership, trying to schedule an element that doesn't belong to the current schedule, check your different copies");

    e->is_scheduled(true);
    _scheduled_elements.push_back(e);
    sort(_scheduled_elements.begin(), _scheduled_elements.end(), [](SchedElt* a, SchedElt *b) {
        if(a->rt() == b->rt())
            return a->id() < b->id();
        return a->rt() < b->rt();
    });
}

bool Schedule::is_task_fully_scheduled(SchedJob *t) {
    if(!t->is_scheduled())
        return false;
    if(t->packet_list().size() == 0)
        return true;
    return all_of(t->packet_list().begin(), t->packet_list().end(), [this](SchedPacket *a) {
        return a->is_scheduled();
    });
}

bool Schedule::is_built() {
    return !this->_elements_lut.empty();
}

void Schedule::add_interference_impacted(SchedElt *element, timinginfos_t cost, std::string target){
    SchedElt * new_elt = _elements_lut[element->id()].get();
    SchedEltPtr old_elt = ScheduleFactory::shallow_copy(element);

    if (!element->equals(old_elt.get())) Utils::WARN("FLS", "Adding elements that got delayed didnt work");

    if (target == "rt"){
        new_elt->rt(new_elt->rt() + cost);
    }else if (target == "wct"){
        new_elt->wct(new_elt->wct() + cost);
    }

    _delay_impacted_elements.emplace_back(std::move(old_elt), new_elt);
}

void Schedule::add_interference_impacted(std::vector<std::pair<SchedJob*, SchedElt*>>& rhs_list) {
    for (auto old_new = rhs_list.begin(); old_new != rhs_list.end(); ){
        bool found = false;
        for (SchedEltPtr &current_el: elements()){
            if (current_el->equals(old_new->first)){
                SchedEltPtr old_elt = ScheduleFactory::shallow_copy(current_el.get());

                timinginfos_t new_rt = max(old_new->second->rt(), old_new->first->rt());
                timinginfos_t new_wct = max(old_new->second->wct(), old_new->first->wct());

                current_el->rt(new_rt) ;
                current_el->wct(new_wct);

                std::pair<SchedEltPtr, SchedElt*> old_new_element(std::move(old_elt), current_el.get());
                _delay_impacted_elements.push_back(std::move(old_new_element));

                found = true;
//                break;
            }
        }
        if (found) {
            rhs_list.erase(old_new);
        } else {
            old_new++;
        }
    }
}

std::vector<const SchedElt *> Schedule::elements_unowned() const {
    std::vector<const SchedElt *> result;
    result.reserve(this->_elements_lut.size());
    for (const SchedEltPtr &elt : this->elements()) {
        result.push_back(elt.get());
    }
    return result;
}

std::vector<SchedElt *> Schedule::elements_unowned() {
    std::vector<SchedElt *> result;
    result.reserve(this->_elements_lut.size());
    for (const SchedEltPtr &elt : this->elements()) {
        result.push_back(elt.get());
    }
    return result;
}

ostream& operator<< (ostream& os, const Schedule &inf) {
    os << "-------------------------------------------------------------" << endl;
    os << "Priority type " << ScheduleProperties::to_string(inf.priority_mode()) << endl;
    os << "Mapping type " << ScheduleProperties::to_string(inf.mapping_mode()) << endl;
    os << "Preemption type " << ScheduleProperties::to_string(inf.preemption_mode()) << endl;
    os << "\tMakespan: " << inf.makespan() << " -- Status: " << ScheduleProperties::to_string(inf.status()) << endl;
    for(SchedElt* e : inf.scheduled_elements()) {
        os << "\t\t\t" << e->toString() ;
        if(inf.get_mapping(e) != nullptr)
            os << " -- mapped on " << inf.get_mapping(e)->core()->id;
        os << " : " << endl;
        if(Utils::isa<SchedJob*>(e)) {
            os << "\t\t\t\t -- At CPU frequency: " << e->schedtask()->frequency() << inf.get_frequency_unit();
            os << " -- At GPU frequency: " << e->schedtask()->gpu_frequency() << inf.get_frequency_unit();
            os << ": ";
            for(pair<uint64_t, uint64_t> ec : e->schedtask()->exec_chunks())
                os << "RT:" << ec.first <<  inf.get_time_unit() << " - WCT: " << ec.second <<  inf.get_time_unit();
            os << endl;
        }
        else if(Utils::isa<SchedPacket*>(e))
            os << "\t\t\t\tRT " << e->rt() << " - WCT : " << e->wct() << " -- data: " << ((SchedPacket*)e)->data() << endl;
    }
    os << endl;

    for(SchedCore *core : inf.schedcores()) {
        os << "\t\t\t" << core->core()->id << " -> free spm size : " << core->available_spm_size() << endl;
        for(SchedSpmRegion *sr : core->memory_regions()) {
            if(sr->booking().size() > 0) {
                os << "\t\t\t\t" << sr->label() << " -- " << sr->size();
                for(SchedResource::booking_elt_t b : sr->booking())
                    os << " -- " << b.elt << "=" << b.start << "/" << b.end;
                os << endl;
            }
        }
    }
    os << "-------------------------------------------------------------" << endl;
    return os;
}


string Schedule::operator+ (const string &a) {
    stringstream ss;
    ss << this << a;
    return ss.str();
}
string Schedule::operator+ (const char* a) {
    stringstream ss;
    ss << this << a;
    return ss.str();
}

string operator+ (const string &a, const Schedule &b) {
    stringstream ss;
    ss << a << b;
    return ss.str();
}

string operator+ (const char* a, const Schedule &b) {
    stringstream ss;
    ss << a << b;
    return ss.str();
}

bool operator== (const Schedule *a, const Schedule &b) { return *a == b; }
bool operator== (const Schedule &a, const Schedule *b) { return a == *b; }
bool operator== (const Schedule &a, const Schedule &b) {
    if(a.status() != b.status()) {
        Utils::DEBUG("Status differ: "+ScheduleProperties::to_string(a.status())+" != "+ScheduleProperties::to_string(b.status()));
        return false;
    }
    if(a.makespan() != b.makespan()) {
        Utils::DEBUG("Makespan differ: "+to_string(a.makespan())+" != "+to_string(b.makespan()));
        return false;
    }
    if(a.elements_lut().size() != b.elements_lut().size()) {
        Utils::DEBUG("Number of elements differ: "+to_string(a.elements_lut().size())+" != "+to_string(b.elements_lut().size()));
        return false;
    }
    if(a.schedcores().size() != b.schedcores().size()) {
        Utils::DEBUG("Number of cores differ: "+to_string(a.schedcores().size())+" != "+to_string(b.schedcores().size()));
        return false;
    }
    if(a.schedresources().size() != b.schedresources().size()) {
        Utils::DEBUG("Number of resources differ: "+to_string(a.schedresources().size())+" != "+to_string(b.schedresources().size()));
        return false;
    }

    for(auto &elt : a.elements_lut()) {
        const SchedElt *element_a = elt.second.get();
        if(!b.elements_lut().count(elt.first)) {
            Utils::DEBUG("Missing element: "+element_a->toString());
            return false;
        }
        const SchedElt *element_b = b.elements_lut().at(elt.first).get();
        if(*element_a != *element_b) {
            Utils::DEBUG("Elements differ: "+element_a->toString()+" != "+element_b->toString());
            return false;
        }
    }

    vector<SchedCore*> a_cores = a.schedcores();
    vector<SchedCore*> b_cores = b.schedcores();
    sort(a_cores.begin(), a_cores.end(), [](SchedCore *a, SchedCore *b) { return a->core()->id < b->core()->id;});
    sort(b_cores.begin(), b_cores.end(), [](SchedCore *a, SchedCore *b) { return a->core()->id < b->core()->id;});
    for(size_t i=0 ; i < a_cores.size() ; ++i) {
        SchedCore *element_a = a_cores[i];
        SchedCore *element_b = b_cores[i];
        if(*element_a != *element_b) {
            Utils::DEBUG("Cores differ: "+element_a->core()->id+" != "+element_b->core()->id);
            return false;
        }
    }

    vector<SchedResource*> a_res = a.schedresources();
    vector<SchedResource*> b_res = b.schedresources();
    sort(a_res.begin(), a_res.end(), [](SchedResource *a, SchedResource *b) { return a->label() < b->label();});
    sort(b_res.begin(), b_res.end(), [](SchedResource *a, SchedResource *b) { return a->label() < b->label();});
    for(size_t i=0 ; i < a_res.size() ; ++i) {
        SchedResource *resource_a = a_res[i];
        SchedResource *resource_b = b_res[i];
        if(*resource_a != *resource_b){
            Utils::DEBUG("Resources differ: "+resource_a->label()+" != "+resource_b->label());
            return false;
        }
    }

    return true;
}

/******************************************************************************/
/*              SchedElt                                                      */
/******************************************************************************/
std::string SchedElt::get_id_for(const std::string &taskId, timinginfos_t minRtPeriod, uint32_t elementId, Type type) {
    return taskId + "_" + to_string(minRtPeriod) + (elementId == 0 ? "" : "_" +
            to_string(elementId)) + (type == Type::TASK ? "_t" : "");
}

SchedElt::SchedElt(const SchedElt::Type t, const Task *ta, timinginfos_t release, uint32_t element_id) : _type(t), _task(ta),
        _min_rt_period(release), _element_id(element_id) {
    if (ta->D() > 0)
        _min_rt_deadline += _min_rt_period + ta->D();
    _id = SchedElt::get_id_for(ta->id(), _min_rt_period, _element_id, _type);
}

SchedElt::SchedElt(const SchedElt &rhs, uint32_t element_id) : SchedElt(rhs.type(), rhs.task(), rhs.min_rt_period(), element_id) {
    // copy must be called by subclass
}
SchedElt::~SchedElt() {
    _successors.clear();
    _previous.clear();
    _siblings.clear();
}

void SchedElt::populate(const SchedElt &rhs, const map<SchedElt*, SchedElt*> &pool) {
    for (SchedElt *prev : rhs._previous) {
        _previous.push_back(pool.at(prev));
    }

    for (SchedElt *succ : rhs._successors) {
        _successors.push_back(pool.at(succ));
    }

    for (SchedElt *sib : rhs._siblings) {
        _siblings.push_back(sib);
    }
    dopopulate(rhs, pool);
}

void SchedElt::copy_values_from(const SchedElt &rhs) {
    if(_type != rhs.type())
        throw CompilerException("SchedElt::copy_values_from", "Forbidden copy");
    _task = rhs._task;
    _wce = rhs._wce;
    _mem_data = rhs._mem_data;
    _min_rt_period = rhs._min_rt_period;
    _min_rt_deadline = rhs._min_rt_deadline;
    _interference_applied = rhs._interference_applied;
    _is_scheduled = rhs._is_scheduled;
    _dirlbl = rhs._dirlbl;
    docopy_values_from(rhs);
}

void SchedElt::add_successor(SchedElt * succ){
    if(find(_successors.begin(), _successors.end(), succ) == _successors.end())
        _successors.push_back(succ);

    if(find(succ->_previous.begin(), succ->_previous.end(), this) == succ->_previous.end())
        succ->_previous.push_back(this);
}

void SchedElt::rem_successor(SchedElt * succ){
    vector<SchedElt*>::iterator pos1, pos2;
    if((pos1 = find(_successors.begin(), _successors.end(), succ)) != _successors.end())
        _successors.erase(pos1);

    if((pos2 = find(succ->_previous.begin(), succ->_previous.end(), this)) != succ->_previous.end())
        succ->_previous.erase(pos2);
}

string SchedElt::operator+ (const string &a) {
    return ilp_label()+a;
}
string SchedElt::operator+ (const char* a) {
    return ilp_label()+a;
}
string operator+ (const string &a, const SchedElt &b) {
    return a+b.ilp_label();
}
string operator+ (const string &a, const SchedElt *b) {
    return a+b->ilp_label();
}
string operator+ (const char* a, const SchedElt &b) {
    return a+b.ilp_label();
}

std::ostream& operator<< (std::ostream& os, const SchedElt &a) {
    return os << a.ilp_label();
}
std::ostream& operator<< (std::ostream& os, const SchedElt *a) {
    return os << a->ilp_label();
}
bool operator== (const SchedElt *a, const SchedElt &b) { return *a == b; }
bool operator== (const SchedElt &a, const SchedElt *b) { return a == *b; }
bool operator== (const SchedElt &a, const SchedElt &b) {
    if(a.type() != b.type()) {
        Utils::DEBUG("Element types differ: "+to_string(a.type())+" != "+to_string(b.type()));
        return false;
    }
    if(a.task()->id() != b.task()->id()){
        Utils::DEBUG("Element id differ: "+a.task()->id()+" != "+b.task()->id());
        return false;
    }
    if(a.wce() != b.wce()){
        Utils::DEBUG("Element wce differ: "+to_string(a.wce())+" != "+to_string(b.wce()));
        return false;
    }
    if(a.min_rt_period() != b.min_rt_period()){
        Utils::DEBUG("Element release time differ: "+to_string(a.min_rt_period())+" != "+to_string(b.min_rt_period()));
        return false;
    }
    if(a.min_rt_deadline() != b.min_rt_deadline()){
        Utils::DEBUG("Element deadline differ: "+to_string(a.min_rt_deadline())+" != "+to_string(b.min_rt_deadline()));
        return false;
    }
    if(a.rt() != b.rt()){
        Utils::DEBUG("Element start time differ: "+to_string(a.rt())+" != "+to_string(b.rt()));
        return false;
    }
    if(a.wct() != b.wct()){
        Utils::DEBUG("Element wcet differ: "+to_string(a.wct())+" != "+to_string(b.wct()));
        return false;
    }
    return true;
}
bool operator!= (const SchedElt *a, const SchedElt &b) { return *a != b; }
bool operator!= (const SchedElt &a, const SchedElt *b) { return a != *b; }
bool operator!= (const SchedElt &a, const SchedElt &b) {
    return !(a == b);
}

/******************************************************************************/
/*              SchedEltTask                                                  */
/******************************************************************************/
SchedJob::SchedJob(const Task *t, timinginfos_t release, uint32_t element_id) : SchedElt(SchedElt::Type::TASK, t, release,
        element_id) {
    _exec_chunks.resize(1);
    _exec_chunks[0].first = _exec_chunks[0].second = 0;
    _dirlbl = "e";
    rt(release);
}


SchedJob::~SchedJob() {
    _packet_list.clear();
    _packet_read.clear();
    _packet_write.clear();
}
//
//SchedEltPtr SchedJob::copy(uint32_t new_id) {
//    return std::make_unique<SchedJob>(*this, new_id);
//}

void SchedJob::docopy_values_from(const SchedElt &rhs) {
    const SchedJob &trhs = dynamic_cast<const SchedJob&>(rhs);
    _selected_version = trhs._selected_version;
    _frequency = trhs._frequency;
    _gpu_frequency =  trhs._gpu_frequency;
    _gpu_required = trhs._gpu_required;
    _task = trhs._task;
    _phase = trhs._phase;
    _are_successors_created = trhs._are_successors_created;
    _exec_chunks.clear();
    for(pair<uint64_t,uint64_t> ec : trhs._exec_chunks)
        _exec_chunks.push_back(ec);
    _current_chunk = trhs._current_chunk;
    _is_in_mutex = trhs._is_in_mutex;
    _priority = trhs._priority;
}

void SchedJob::dopopulate(const SchedElt &r, const map<SchedElt*, SchedElt*> &pool) {
    const SchedJob *rhs = dynamic_cast<const SchedJob*>(&r);
    for(SchedElt* prev : rhs->_packet_list)
        _packet_list.push_back(dynamic_cast<SchedPacket*>(pool.at(prev)));
    for(SchedElt* prev : rhs->_packet_read)
        _packet_read.push_back(dynamic_cast<SchedPacket*>(pool.at(prev)));
    for(SchedElt* prev : rhs->_packet_write)
        _packet_write.push_back(dynamic_cast<SchedPacket*>(pool.at(prev)));
}
void SchedJob::accept(SchedEltVisitor *v, Schedule *s) {
    v->visitSchedEltTask(s, this);
}
bool SchedJob::equals(SchedElt *a) {
    return (_type == a->type() && _task == a->schedtask()->_task && _min_rt_period == a->min_rt_period() && rt() == a->rt() && wct() == a->wct());
}

string SchedJob::toString() const {
    return "Task "+_task->id()+ +
            " Version :"+ _selected_version +
            " - iter "+to_string(_min_rt_period)+"/"+to_string(_min_rt_deadline)+
            " - ("+to_string(_current_chunk)+"/"+to_string(_exec_chunks.size())+")"+
            " priority: "+to_string(_priority)+
            " -- RT: "+to_string(_exec_chunks[_current_chunk].first)+
            ", WCT: "+to_string(_exec_chunks[_current_chunk].second)+
            ", end: "+to_string(_exec_chunks[_current_chunk].first+_exec_chunks[_current_chunk].second);
}

uint64_t SchedJob::get_release_first_read() {
    // in case of yoyo effect, maybe initialise r with rt
    uint64_t r = -1;
    for(SchedPacket *e : _packet_read) {
        if(e->rt() < r)
            r = e->rt();
    }
    return r;
}
uint64_t SchedJob::get_end_last_read() {
    // in case of yoyo effect, maybe initialise r with rt
    uint64_t r = 0;
    for(SchedPacket *e : _packet_read) {
        if(e->rt()+e->wct() > r)
            r = e->rt()+e->wct();
    }
    return r;
}
uint64_t SchedJob::get_release_first_write() {
    // in case of yoyo effect, maybe initialise r with rt
    uint64_t r = -1;
    for(SchedPacket *e : _packet_write) {
        if(e->rt() < r)
            r = e->rt();
    }
    return r;
}
uint64_t SchedJob::get_end_last_write() {
    // in case of yoyo effect, maybe initialise r with rt
    uint64_t r = 0;
    for(SchedPacket *e : _packet_write) {
        if(e->rt()+e->wct() > r)
            r = e->rt()+e->wct();
    }
    return r;
}

string SchedJob::ilp_label() const {
    return ""+*_task+"_"+to_string(_min_rt_period);
}

void SchedJob::reset() {
    _current_chunk = 0;
    _exec_chunks.resize(1);
    _exec_chunks[0].first = _exec_chunks[0].second = 0;
    _is_in_mutex = false;
}

void SchedJob::add_exec_chunk(uint64_t endchunk, uint64_t newrt) {
//    cout << "\t\t\t===> " << endchunk << " -- " << newrt << " --- " << rt() << " -- " << wct() << endl;
//    for(pair<uint64_t, uint64_t> ec : _exec_chunks)
//        cout  << "\t\t\t\t\t-----------> " << ec.first << " -- " << ec.second << endl;
    if((int64_t)(rt()+wct() - endchunk) < 0)
        throw CompilerException("schedule", "Wrong execution chunk ending, probably a problem with a preemption computation");

    uint64_t currt = rt();
    uint64_t curwct = wct();

    if((int64_t)(endchunk-rt()) <= 0) {
        rt(newrt);
        wct(currt+curwct-endchunk);
    }
    else if(endchunk-rt() > 0) {
        wct(endchunk-rt());
        _exec_chunks.push_back(make_pair(newrt, currt+curwct-endchunk));
        ++_current_chunk;
    }
}

bool SchedJob::move_exec_chunk(pair<timinginfos_t, timinginfos_t> chunk, timinginfos_t newrt) {
    vector<pair<timinginfos_t, timinginfos_t>>::iterator cid, et;
    for(cid = _exec_chunks.begin(), et=_exec_chunks.end() ; cid != et ; ++cid) {
        if(cid->first == chunk.first && cid->second == chunk.second)
            break;
    }
    if(cid == et)
        throw CompilerException("schedule", "Can't find chunk "+to_string(chunk.first)+"/"+to_string(chunk.second));

//    cout << "=========================================================> " << distance(_exec_chunks.begin(), cid) << " -- " << _exec_chunks.size() << " -- " << current_chunk << endl;
    if((unsigned)distance(_exec_chunks.begin(), cid) == _current_chunk) {
//        cout << "=========================================================> current chunk" << endl;
//        for(pair<uint64_t, uint64_t> ec : _exec_chunks)
//            cout  << "\t\t\t\t\t-----------> " << ec.first << " -- " << ec.second << endl;
        rt(newrt);
        return false;
    }

    if(newrt+cid->second < next(cid)->first) {
//        cout << "=========================================================> no overlap with next" << endl;
//        for(pair<uint64_t, uint64_t> ec : _exec_chunks)
//            cout  << "\t\t\t\t\t-----------> " << ec.first << " -- " << ec.second << endl;
        cid->first = newrt;
        return false;
    }
    //a previous exec is to be moved to run over the next
    vector<vector<pair<timinginfos_t, timinginfos_t>>::iterator> to_delete;
    for( ; next(cid) != et && cid->first+cid->second >= next(cid)->first ; ++cid) {
        next(cid)->second += cid->second;
        to_delete.push_back(cid);
    }
    for(vector<pair<timinginfos_t, timinginfos_t>>::iterator it : to_delete) {
        _exec_chunks.erase(it);
        --_current_chunk;
    }

//    cout << "=========================================================> overlapped" << endl;
//    for(pair<uint64_t, uint64_t> ec : _exec_chunks)
//        cout  << "\t\t\t\t\t-----------> " << ec.first << " -- " << ec.second << endl;
    return true;
}

const timinginfos_t SchedJob::rt() const {
    return _exec_chunks[_current_chunk].first;
}
timinginfos_t SchedJob::rt() {
    return _exec_chunks[_current_chunk].first;
}
void SchedJob::rt(timinginfos_t r) {
    _exec_chunks[_current_chunk].first = r;
}
const timinginfos_t SchedJob::wct() const {
    return _exec_chunks[_current_chunk].second;
}
timinginfos_t SchedJob::wct() {
    return _exec_chunks[_current_chunk].second;
}
void SchedJob::wct(timinginfos_t c) {
    _exec_chunks[_current_chunk].second = c;
}

SchedPacket * SchedJob::packet_list(size_t i) {
    return (i >= _packet_list.size()) ? nullptr : _packet_list[i];
}

void SchedJob::add_read_packet(SchedPacket *pac) {
    if(pac == nullptr) return;
    _packet_list.push_back(pac);
    _packet_read.push_back(pac);
    sort(_packet_read.begin(), _packet_read.end(), [](SchedPacket *a, SchedPacket *b) { return (a->packetnum() == b->packetnum()) ? a->id() < b->id() : a->packetnum() < b->packetnum();});
    sort(_packet_list.begin(), _packet_list.end(), [](SchedPacket *a, SchedPacket *b) { return a->id() < b->id();});
}

void SchedJob::add_write_packet(SchedPacket *pac) {
    if(pac == nullptr) return;
    _packet_list.push_back(pac);
    _packet_write.push_back(pac);
    sort(_packet_write.begin(), _packet_write.end(), [](SchedPacket *a, SchedPacket *b) { return (a->packetnum() == b->packetnum()) ? a->id() < b->id() : a->packetnum() < b->packetnum();});
    sort(_packet_list.begin(), _packet_list.end(), [](SchedPacket *a, SchedPacket *b) { return a->id() < b->id();});
}

SchedEltPtr SchedJob::copy(uint32_t new_id) {
    return std::unique_ptr<SchedElt>(new SchedJob(*this, new_id));
}

/******************************************************************************/
/*              SchedEltPacket                                                */
/******************************************************************************/
SchedPacket::SchedPacket(SchedJob* j, timinginfos_t release, const std::string& dirlbl)
  : SchedPacket(j, release, dirlbl, j, 0) {}

SchedPacket::SchedPacket(SchedJob* j, timinginfos_t release, const std::string& dirlbl, SchedJob *senderOrDest, uint32_t packetnum)
  : SchedElt(SchedElt::Type::PACKET, j->task(), release), _packet_id_begin(_id.length()), _packetnum(packetnum), _schedtask(j), _tofromschedtask(senderOrDest), _rt(release) {
    _id += "_"+dirlbl;
    _id += "_"+senderOrDest->task()->id();
    _id += "_"+to_string(_packetnum);
    _dirlbl = dirlbl;
}

SchedPacket::SchedPacket(const SchedPacket &rhs, uint32_t new_id) : SchedElt(rhs, new_id), _packet_id_begin(_id.length()) {
    this->copy_values_from(rhs);
    _id.append(rhs._id.begin() + rhs._packet_id_begin, rhs._id.end());
}

string SchedPacket::toString() const {
    return "Packet "+
        _schedtask->task()->id()+"<->"+
        _tofromschedtask->task()->id()+"-"+to_string(_packetnum)+"-"+_dirlbl
        + " - iter "+to_string(_min_rt_period);
}

void SchedPacket::docopy_values_from(const SchedElt &rhs) {
    const SchedPacket &prhs = dynamic_cast<const SchedPacket&>(rhs);
    _rt = prhs._rt;
    _wct = prhs._wct;
    _data = prhs._data;
    _datatype = prhs._datatype;
    _packetnum = prhs._packetnum;
    _transmission_nb_packet = prhs._transmission_nb_packet;
    _dirlbl = prhs._dirlbl;
    _concurrency = prhs._concurrency;
}

SchedEltPtr SchedPacket::copy(uint32_t id) {
    // ignore the id
    return std::unique_ptr<SchedElt>(new SchedPacket(*this));
}

void SchedPacket::accept(SchedEltVisitor *v, Schedule *s) {
    if(_dirlbl == "r")
        v->visitSchedEltPacketRead(s, this);
    else
        v->visitSchedEltPacketWrite(s, this);
}
void SchedPacket::dopopulate(const SchedElt &r, const map<SchedElt*, SchedElt*> &pool) {
    const SchedPacket *rhs = dynamic_cast<const SchedPacket*>(&r);

    if (rhs->_schedtask != NULL){
        for (pair<SchedElt *, SchedElt *> pool_el: pool) {
            if (rhs->_schedtask->equals(pool_el.first)) {
                _schedtask = dynamic_cast<SchedJob*>(pool.at(pool_el.first));
            }
        }
    }

    for (pair<SchedElt *, SchedElt *> pool_el: pool) {
        if (rhs->_tofromschedtask->equals(pool_el.first)) {
            _tofromschedtask = dynamic_cast<SchedJob*>(pool.at(pool_el.first));
        }
    }

//    _schedtask = (SchedJob*)pool.at(rhs->_schedtask);
//    _sortingschedtask = (SchedJob*)pool.at(rhs->_sortingschedtask);
//    _tofromschedtask = (SchedJob*)pool.at(rhs->_tofromschedtask);
}
bool SchedPacket::equals(SchedElt *a) {
    if(_type != a->type() || _schedtask != a->schedtask())
        return false;
    SchedPacket *ap = (SchedPacket*)a;
    return _dirlbl == ap->_dirlbl && _datatype == ap->_datatype && _packetnum == ap->_packetnum &&
            _transmission_nb_packet == ap->_transmission_nb_packet && _schedtask->equals(ap->_schedtask) &&
            _tofromschedtask->equals(ap->_tofromschedtask) && _element_id == ap->_element_id;
}
string SchedPacket::ilp_label() const {
    return "p"+_dirlbl+to_string(_packetnum)+"_"+_schedtask->ilp_label()+"_"+_tofromschedtask->ilp_label();
}

void SchedPacket::reset() {
    _rt = _wct = 0;
}

void SchedPacket::set_transmission(size_t trsize, dataqty_t data, string datatype, datamemsize_t trdelay) {
    _transmission_nb_packet = trsize;
    _data = data;
    _mem_data = data;
    _datatype = datatype;
    _wct = trdelay;
}

/******************************************************************************/
/*              SchedResource                                                */
/******************************************************************************/
SchedResource& SchedResource::operator= (const SchedResource& rhs) {
    _resource = rhs._resource;
    _label = rhs._label;
    _booking.clear();
    for(SchedResource::booking_elt_t resa : rhs._booking) {
        _booking.push_back(resa);
    }

    return *this;
}

bool SchedResource::is_booked(timinginfos_t start, timinginfos_t _end) {
    return any_of(_booking.begin(), _booking.end(), [start, _end](const SchedResource::booking_elt_t &el) {
        return (start < el.end && el.start < _end);
    });
}
timinginfos_t SchedResource::get_free_time() {
    if(_booking.empty())
        return 0;
    SchedResource::booking_elt_t last_booking = _booking.at(_booking.size()-1);
    return last_booking.end;
}

void SchedResource::book(std::string el, timinginfos_t start, timinginfos_t _end) {
    SchedResource::booking_elt_t book = {
        .elt = el,
        .start = start,
        .end = _end
    };
    _booking.push_back(book);
    sort(_booking.begin(), _booking.end(), [](const SchedResource::booking_elt_t &a, const SchedResource::booking_elt_t &b) {
        return a.start < b.start;
    });
}

bool operator== (const SchedResource &a, const string &b) { return a.label() == b; }
bool operator== (const SchedResource &a, const SchedResource &b) {
    if(a.label() != b.label())
        return false;
    if(a.resource()->id != b.resource()->id)
        return false;

    SchedResource::booking_t a_book = a.booking();
    SchedResource::booking_t b_book = b.booking();
    sort(a_book.begin(), a_book.end(), [](const SchedSpmRegion::booking_elt_t &a, const SchedSpmRegion::booking_elt_t &b) { return a.start < b.start;});
    sort(b_book.begin(), b_book.end(), [](const SchedSpmRegion::booking_elt_t &a, const SchedSpmRegion::booking_elt_t &b) { return a.start < b.start;});
    for(size_t i=0 ; i < a_book.size() ; ++i) {
        SchedResource::booking_elt_t &a = a_book[i];
        SchedResource::booking_elt_t &b = b_book[i];
        if(a.elt != b.elt || a.start != b.start || a.end != b.end)
            return false;
    }

    return true;
}

bool operator!= (const SchedResource *a, const SchedResource &b) { return *a != b; }
bool operator!= (const SchedResource &a, const SchedResource *b) { return a != *b; }
bool operator!= (const SchedResource &a, const SchedResource &b) {
    return !(a == b);
}

/******************************************************************************/
/*              SchedSpmRegion                                                */
/******************************************************************************/
SchedSpmRegion& SchedSpmRegion::operator= (const SchedSpmRegion& rhs) {
    SchedResource::operator= ((SchedResource)rhs);
    _core = rhs._core;
    _size = rhs._size;

    return *this;
}
bool operator== (const SchedSpmRegion &a, const string &b) { return a.label() == b; }
bool operator== (const SchedSpmRegion &a, const SchedSpmRegion &b) {
    if(a.label() != b.label())
        return false;
    if(a.size() != b.size()) {
        Utils::DEBUG("SPM size differ "+to_string(a.size())+" != "+to_string(b.size()));
        return false;
    }
    if(a.core()->core()->id != b.core()->core()->id) {
        Utils::DEBUG("SPM ownership differ "+a.core()->core()->id+"!="+b.core()->core()->id);
        return false;
    }
    if(a.booking().size() != b.booking().size()) {
        Utils::DEBUG("SPM booking differ "+to_string(a.booking().size())+" != "+to_string(b.booking().size()));
        return false;
    }

    SchedResource::booking_t a_book = a.booking();
    SchedResource::booking_t b_book = b.booking();
    sort(a_book.begin(), a_book.end(), [](const SchedResource::booking_elt_t &a, const SchedResource::booking_elt_t &b) { return a.start < b.start;});
    sort(b_book.begin(), b_book.end(), [](const SchedResource::booking_elt_t &a, const SchedResource::booking_elt_t &b) { return a.start < b.start;});
    for(size_t i=0 ; i < a_book.size() ; ++i) {
        SchedResource::booking_elt_t &a = a_book[i];
        SchedResource::booking_elt_t &b = b_book[i];
        if(a.elt != b.elt || a.start != b.start || a.end != b.end) {
            Utils::DEBUG("SPM booking elt time "+a.elt+" != "+b.elt+" || "+to_string(a.start)+" != "+to_string(b.start)+" || "+to_string(a.end)+" != "+to_string(b.end));
            return false;
        }
    }

    return true;
}
std::string operator+ (const std::string &a, const SchedSpmRegion &b) {
    return a+b.label();
}
std::string operator+ (const char* a, const SchedSpmRegion &b){
    return a+b.label();
}
bool operator!= (const SchedSpmRegion *a, const SchedSpmRegion &b) { return *a != b; }
bool operator!= (const SchedSpmRegion &a, const SchedSpmRegion *b) { return a != *b; }
bool operator!= (const SchedSpmRegion &a, const SchedSpmRegion &b) {
    return !(a == b);
}

/******************************************************************************/
/*              SchedCore                                                     */
/******************************************************************************/
SchedCore::SchedCore(ComputeUnit *p) {
    _core = p;
    _total_spm_size = p->spm_size;
    _available_spm_size = p->spm_size;
}
SchedCore::~SchedCore() {
    clear();
}

void SchedCore::clear() {
    for(SchedSpmRegion *sr : _memory_regions)
        delete sr;
    _memory_regions.clear();
}

SchedCore& SchedCore::operator= (const SchedCore& rhs) {
    clear();

    _core = rhs._core;
    _total_spm_size = rhs._total_spm_size;
    _available_spm_size = rhs._available_spm_size;

    for(SchedSpmRegion *sr : rhs._memory_regions) {
        SchedSpmRegion *cursr = new SchedSpmRegion(*sr);
        cursr->_core = this;
        _memory_regions.push_back(cursr);
    }

    return *this;
}

bool SchedCore::equals(SchedCore* c) {
    return c->_core == _core;
}

SchedSpmRegion * SchedCore::add_spmregion(const string &label, uint64_t size) {
    SchedSpmRegion *sr = new SchedSpmRegion(this, label, size);
    _memory_regions.push_back(sr);
    _available_spm_size -= size;
    sort(_memory_regions.begin(), _memory_regions.end(), [](SchedSpmRegion *a, SchedSpmRegion *b) {return a->label() < b->label();});
    return sr;
}
void SchedCore::add_spmregion(SchedSpmRegion *sr) {
    sr->_core = this;
    _memory_regions.push_back(sr);
    _available_spm_size -= sr->size();
}

SchedSpmRegion* SchedCore::schedspmregion(const string &lbl) {
    for(SchedSpmRegion *s : _memory_regions) {
        if(s->_label == lbl)
            return s;
    }
    return nullptr;
}
SchedSpmRegion* SchedCore::schedspmregion(const SchedSpmRegion &r) {
    for(SchedSpmRegion *s : _memory_regions) {
        if(s->_label == r._label)
            return s;
    }
    return nullptr;
}

SchedSpmRegion* SchedCore::get_available_spmregion(uint64_t start, uint64_t _end) {
    for(SchedSpmRegion *spm : _memory_regions) {
        if(!spm->is_booked(start, _end))
            return spm;
    }
    return nullptr;
}

string operator+ (const string &a, const SchedCore &b) {
    return a+b.core()->id;
}
string operator+ (const char* a, const SchedCore &b) {
    return a+b.core()->id;
}
bool operator==(const SchedCore& a, const SchedCore& b) {
    if(a.core() != a.core()) {
        Utils::DEBUG("Inner cores differ "+a.core()->id+" != "+b.core()->id);
        return false;
    }

    vector<SchedSpmRegion*> a_mem = a.memory_regions();
    vector<SchedSpmRegion*> b_mem = b.memory_regions();
    sort(a_mem.begin(), a_mem.end(), [](SchedSpmRegion *a, SchedSpmRegion *b) { return a->label() < b->label();});
    sort(b_mem.begin(), b_mem.end(), [](SchedSpmRegion *a, SchedSpmRegion *b) { return a->label() < b->label();});
    for(size_t i=0 ; i < a_mem.size() ; ++i) {
        SchedSpmRegion *a = a_mem[i];
        SchedSpmRegion *b = b_mem[i];
        if(*a != *b) {
            Utils::DEBUG("SPM region differs "+a->label()+" != "+b->label());
            return false;
        }
    }
    return true;
}
bool operator!= (const SchedCore *a, const SchedCore &b) { return *a != b; }
bool operator!= (const SchedCore &a, const SchedCore *b) { return a != *b; }
bool operator!= (const SchedCore &a, const SchedCore &b) {
    return !(a == b);
}
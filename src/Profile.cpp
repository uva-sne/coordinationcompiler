/*!
 * \file Profile.cpp
 * \copyright GNU Public License v3.
 * \date 2020
 * \author Benjamin Rouxel <Wouter Loeve <wouterloeve0@gmail.com> University of Amsterdam
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "Profile.hpp"

using namespace std;

Profile::Profile() {}

void Profile::addDefault(string name, Modifier modifier, bool modifierOnly) {
	vector<pair <string,string>> definedOpts;
	if (name == "nModular") {
		definedOpts = {
			make_pair("replicas", "3"), 
			make_pair("votingReplicas", "1"),
			make_pair("waitingTime", "30%"),
			make_pair("waitingJoin", "true")
		};
	} else if (name == "standby") {
		definedOpts = {
			make_pair("replicas", "2"), 
			make_pair("synchronization", "hot")
		};
	} else if (name == "nVersion") {
		// Nothing here yet.
	} else if (name == "checkpoint") {
		definedOpts = {
			make_pair("shrinking", "true")
		};
	}
	for (auto opt : definedOpts) {
		string option_name = name + "_" + opt.first;

		// Modifier is always added even if setting is already defined in the profile.
		if (modifier || modifierOnly) {
			this->addModifier(option_name, modifier);
		}
		if ((! this->hasMember(option_name) || this->getDefault(option_name)) && ! modifierOnly) {
			this->add(option_name, opt.second);
			this->setDefault(option_name, true);
		}
	}
}

ProfileOption Profile::defaultProfileOption(string value) {
	struct ProfileOption p;
	p.modifiers = {Modifier::empty};
	p.value = value;
	return p;
}

bool Profile::setDefault(string name, bool val) {
	if (this->hasMember(name)) {
		settings[name].defaultOption = val;
	} else {
		Utils::WARN("Profile", "setDefault cannot be executed on non-specified option: \'" + name +"\'");
	}

	return val;
}

bool Profile::getDefault(string name) {
	if (this->hasMember(name)) {
		return settings[name].defaultOption;
	} 
	Utils::WARN("Profile", "getDefault cannot be executed on non-specified option: \'" + name +"\'");
	return false;
}

//Leave std:: for the doc
void Profile::add(std::string name, std::string value) {
	bool add = true;
	// Dont have to set default options if they are already set
	if (value == "true" && this->get(name) != "true") {
		addDefault(name, Modifier::empty, false);
	}

	if (this->hasMember(name)) {
		if (settings[name].modifiers[0] == Modifier::vital) {
			Utils::WARN("Profile", "Cannot overwrite vital option: \'" + name + "\'");
			add = false;
		} else if (! this->getDefault(name)) {
			add = true;
		}
	}

	if (add) {
		settings[name] = defaultProfileOption(value);
	}
}

//Leave std:: for the doc
void Profile::add(std::string name, std::string value, std::string mod) {
	Modifier modifier = string_to_modifier(mod);
	bool add = true;
	// Dont have to set default (FT) options if they are already set
	if (value == "true" && this->get(name) != "true") {
		addDefault(name, modifier, false);
	} else if (value == "true" && this->get(name) == "true" && ! modifier == Modifier::empty && 
				this->getModifier(name) != modifier) {
		// However if the FT method modifier is different (and non-empty) we have to set all the modifiers to that
		addDefault(name, modifier, true);
	}

	if (this->hasMember(name)) {
		if (settings[name].modifiers[0] == Modifier::vital) {
			Utils::WARN("Profile", "Cannot overwrite vital option: \'" + name + "\'");
			add = false;
		} else if (this->getDefault(name)) {
			add = true;
		}
	}

	if (add) {
		settings[name] = defaultProfileOption(value);
	}
}

void Profile::remove(string name) {
    settings.erase(name);
    vector<string> to_remove;
    for(string e : Utils::mapkeys(settings)) {
        if(e.find(name+"_") == 0)
            to_remove.push_back(e);
    }
    for(string e : to_remove) 
        settings.erase(e);
}

void Profile::remove(const Profile &prof) {
    for(string n : Utils::mapkeys(prof.settings))
        remove(n);
}

void Profile::addRemove(string name) {
	if (this->hasMember(name)) {
		if (settings[name].modifiers[0] == Modifier::vital) {
			Utils::WARN("Profile", "Cannot remove vital option: \'" + name + "\'");
		} else {
			settings[name].remove = true;
		}
	} else {
		settings[name] = defaultProfileOption("false");
		settings[name].remove = "true";
	}
}

vector<string> Profile::getRemovals() const {
	vector<string> removals;
	for (pair<string, ProfileOption> x : settings) {
		if (x.second.remove) {
			removals.push_back(x.first);
		}
	}
	return removals;
}

string Profile::get(string name) const {
	map<string, ProfileOption>::const_iterator it = settings.find(name);
	if (it != settings.end()) {
		return it->second.value;
	}
	return "";
}

Modifier Profile::getModifier(string name) const {
	map<string, ProfileOption>::const_iterator it = settings.find(name);
	if (it != settings.end()) {
		return it->second.modifiers[0];
	}
	return Modifier::empty;
}

//Leave std:: for the doc
void Profile::addModifier(std::string name, Modifier value) {
	if (this->hasMember(name)) {
		if (settings[name].modifiers[0] == Modifier::empty) {
			settings[name].modifiers[0] = value;
		} else {
			settings[name].modifiers.push_back(value);
		}
	} else {
		CompilerException("Profile", "Option with name: \'" + name + "\' does not exist, so modifier cannot be retrieved");
	}
}
//Leave std:: for the doc
void Profile::addModifier(std::string name, std::string value) {
	Modifier modifier = string_to_modifier(value);
	this->addModifier(name, modifier);
}

Modifier Profile::string_to_modifier(string m) {
	Modifier result = Modifier::empty;
	if (m == "vital") {
		result = Modifier::vital;
	} else if (m.empty() || m == "empty") {
		result = Modifier::empty;
	}
	return result;
}

string Profile::modifier_to_string(Modifier m) {
	string result = "";
	switch(m) {
		case Modifier::vital:
			result = "vital";
			break;
		default:
			result = "";
			break;
	}
	return result;
}

string Profile::to_string() {
	string result = "";

	for (pair<string, ProfileOption> x : settings) {
		string modifier = (modifier_to_string(x.second.modifiers[0]));
		if (x.second.remove) {
			result += "remove " + x.first;
		} else {
			result += modifier + " " + x.first + " : " + x.second.value + "\n";
		}
	}

	return result;
}

string Profile::to_dot() {
	string result = "";

	for (pair<string, ProfileOption> x : settings) {
		result += " | {{" + x.first + "} | {" + x.second.value + "}}";
	}
	boost::replace_all(result, "\"", "'");

	return result;
}

bool Profile::hasMember(string key) const { return settings.find(key) != settings.end(); }

bool Profile::hasModifierMember(string key) const { return settings.find(key) != settings.end(); }

map<string, ProfileOption> Profile::getSettings() { return settings; }

void Profile::mergeOption(string addName, ProfileOption addOpt, Profile profile) {
	Modifier modifier = profile.getModifier(addName);
	// Overwrite when vital is not set and trickle down the modifier
	if (modifier == Modifier::vital) {
		// If the values are not equal and the original modifier is vital a warning is raised
		if (addOpt.value != this->get(addName)
			&& this->getModifier(addName) == Modifier::vital) {
			Utils::WARN("Coordination parser", "Vital option " + addName +
						" overwritten by vital option");
		} else if (addOpt.value != "0ns") {
			this->add(addName, addOpt.value);
		}
		this->addModifier(addName, Modifier::vital);
	} else if (profile.getDefault(addName) == false) {
		if (addOpt.value != "0ns" && this->getModifier(addName) != Modifier::vital) {
			this->add(addName, addOpt.value);
		}
	}
}

Profile Profile::mergeProfiles(vector<Profile> profiles) {
	for (Profile profile : profiles) {
            merge(profile);
	}
	return *this;
}

void Profile::merge(Profile &profile) {
        for (pair<string, ProfileOption> setting : profile.getSettings()) {
                mergeOption(setting.first, setting.second, profile);

                for (string remove : profile.getRemovals()) {
                        if (this->getModifier(remove) != Modifier::vital) {
                                vector<string> toRemove = {remove};
                                map<string, ProfileOption>::iterator it;

                                for ( it = settings.begin(); it != settings.end(); it++ ) {
                                        // FT method options start with their FT keyword, this is where we search for.

                                        if (it->first.rfind(remove) == 0 && this->getModifier(it->first) != Modifier::vital) {
                                                toRemove.push_back(it->first);
                                        }
                                }

                                for (string removing : toRemove) {
                                        this->remove(removing);
                                }
                        }
                }
        }

	/* Check whether ft methods occur multiple times. 
	 * Checkpoint is allowed in combination with any other method so it is not listed here.
	 */
	vector<string> ftMethods = {"nModular", "standby", "nVersion"};
	int num_methods = 0;
	for (string ftMethod : ftMethods) {
		if (this->get(ftMethod) == "true") {
			num_methods++;
		}
	}
	if (num_methods > 1) {
		CompilerException("Profile", "Cannot have two fault tolerance settings on the same component");
	}

}

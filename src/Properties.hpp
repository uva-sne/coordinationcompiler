/*
 * Copyright (C) 2021 brouxel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* 
 * File:   Properties.hpp
 * Author: brouxel
 *
 * Created on 30 juin 2021, 09:24
 */

#ifndef PROPERTIES_HPP
#define PROPERTIES_HPP

#include <string>
#include <boost/algorithm/string.hpp>

namespace TaskProperties {
    enum RecurringModel {
        REC_PERIODIC, REC_SPORADIC, REC_ARBITRARY
    };
    enum TaskSetType {
        TAS_INDEPENDENT, TAS_DAG, TAS_MULTI_PERIOD_DAG, TAS_MULTI_DAG , TAS_MULTI_PERIOD_MULTI_DAG
    };
    enum DeadlineModel {
        DEA_CONSTRAINED, DEA_IMPLICIT, DEA_ARBITRARY
    };
    enum TaskType {
        TT_REGULAR, TT_SUBMITTER, TT_SUBMITTEE
    };
    
    inline std::string to_string(const RecurringModel &v) {
        switch(v) {
            case RecurringModel::REC_PERIODIC: return "REC_PERIODIC";
            case RecurringModel::REC_SPORADIC: return "REC_SPORADIC";
            case RecurringModel::REC_ARBITRARY: return "REC_ARBITRARY";
            default: return "unknown";
        }
    }
    inline std::string to_string(const TaskSetType &v) {
        switch(v) {
            case TaskSetType::TAS_INDEPENDENT: return "TAS_INDEPENDENT";
            case TaskSetType::TAS_DAG: return "TAS_DAG";
            case TaskSetType::TAS_MULTI_PERIOD_DAG: return "TAS_MULTI_PERIOD_DAG";
            case TaskSetType::TAS_MULTI_DAG: return "TAS_MULTI_DAG";
            case TaskSetType::TAS_MULTI_PERIOD_MULTI_DAG: return "TAS_MULTI_PERIOD_MULTI_DAG";
            default: return "unknown";
        }
    }
    inline std::string to_string(const DeadlineModel &v) {
        switch(v) {
            case DeadlineModel::DEA_CONSTRAINED: return "DEA_CONSTRAINED";
            case DeadlineModel::DEA_IMPLICIT: return "DEA_IMPLICIT";
            case DeadlineModel::DEA_ARBITRARY: return "DEA_ARBITRARY";
            default: return "unknown";
        }
    }
    inline std::string to_string(const TaskType &v) {
        switch(v) {
            case TaskType::TT_REGULAR: return "TT_REGULAR";
            case TaskType::TT_SUBMITTER: return "TT_SUBMITTER";
            case TaskType::TT_SUBMITTEE: return "TT_SUBMITTEE";
            default: return "unknown";
        }
    }
}

namespace ScheduleProperties {
    /*!
    * List of allowed priority scheme
    */
   enum PriorityScheme_e {
       PRIO_FIXED, //!< Fixed/off-line priority assignment
       PRIO_DYNAMIC //!< Dynamic/on-line priority assignment
   };
    enum Mapping {
        MAP_GLOBAL, //!< Global Mapping
        MAP_PARTITIONED //!< Partitioned Mapping
    };
    enum PreemptionModel {
        PRE_NON, //!< Non-pre-emptiv
        PRE_FULL, //!< Fully pre-emptiv
        PRE_LIMITED //!< Limited pre-emptiv
    };
        /*! Reflect the status of the schedule
     */
    enum sched_statue_e {
        SCHED_COMPLETE, //!< Schedule is complete and successful
        SCHED_PARTIAL, //!< The schedule is in a partial state, mostly due to reaching a timeout
        SCHED_UNSCHEDULABLE, //!< Schedule is complete and unsuccessful
        SCHED_NOTDONE //!< No schedule has been done
    };
    inline std::string to_string(const PriorityScheme_e &v) {
        switch(v) {
            case PriorityScheme_e::PRIO_FIXED: return "PRIO_FIXED";
            case PriorityScheme_e::PRIO_DYNAMIC: return "PRIO_DYNAMIC";
            default: return "unknown";
        }
    }
    inline std::string to_string(const Mapping &v) {
        switch(v) {
            case Mapping::MAP_GLOBAL: return "MAP_GLOBAL";
            case Mapping::MAP_PARTITIONED: return "MAP_PARTITIONED";
            default: return "unknown";
        }
    }
    inline std::string to_string(const PreemptionModel &v) {
        switch(v) {
            case PreemptionModel::PRE_NON: return "PRE_NON";
            case PreemptionModel::PRE_FULL: return "PRE_FULL";
            case PreemptionModel::PRE_LIMITED: return "PRE_LIMITED";
            default: return "unknown";
        }
    }
    /*! Return the string representation of the Schedule status
     * @param e
     * @return
     */
    inline std::string to_string(const sched_statue_e &e) {
        switch(e) {
            case SCHED_COMPLETE: return "complete";
            case SCHED_PARTIAL: return "partial";
            case SCHED_UNSCHEDULABLE: return "unschedulable";
            case SCHED_NOTDONE: return "none";
            default: return "unknown";
        }
    }

    inline sched_statue_e status_from_string(const std::string &s) {
        if(s == "complete")
            return sched_statue_e::SCHED_COMPLETE;
        if(s == "partial")
            return sched_statue_e::SCHED_PARTIAL;
        if(s == "unschedulable")
            return sched_statue_e::SCHED_UNSCHEDULABLE;
        return sched_statue_e::SCHED_NOTDONE;
    }
}

namespace ArchitectureProperties {
    //! Type of processing unit
    enum ComputeUnitType {
        CUT_CPU, //!< cpu
        CUT_GPU, //!< gpu
        CUT_SEPARATE_COPROCESSOR, //!< a separate co-proceesor/gpu instead of a normal processor
        CUT_FPGA, //!< fpga
        CUT_UNDEFINED //!< undefined architecture
    };
    
    enum Cores {
        COR_NONE, COR_SINGLE, COR_MULTI_HOMOGENEOUS, COR_MULTI_HETEROGENEOUS
    };
    inline std::string to_string(const ComputeUnitType &v) {
        switch(v) {
            case ComputeUnitType::CUT_CPU: return "cpu";
            case ComputeUnitType::CUT_GPU: return "gpu";
            case ComputeUnitType::CUT_SEPARATE_COPROCESSOR: return "gpu";
            case ComputeUnitType::CUT_FPGA: return "fpga";
            default: return "unknown";
        }
    }
    inline ArchitectureProperties::ComputeUnitType from_string(const std::string &str) {
        std::string lstr = boost::algorithm::to_lower_copy(str);
        if(lstr == "cpu")
            return ComputeUnitType::CUT_CPU;
        if(lstr == "gpu")
            return ComputeUnitType::CUT_GPU;
        if(lstr == "fpga")
            return ComputeUnitType::CUT_FPGA;
        return ComputeUnitType::CUT_UNDEFINED;
    }
    inline std::string to_string(const Cores &v) {
        switch(v) {
            case Cores::COR_NONE: return "COR_NONE";
            case Cores::COR_SINGLE: return "COR_SINGLE";
            case Cores::COR_MULTI_HOMOGENEOUS: return "COR_MULTI_HOMOGENEOUS";
            case Cores::COR_MULTI_HETEROGENEOUS: return "COR_MULTI_HETEROGENEOUS";
            default: return "unknown";
        }
    }
}

namespace SolverProperties {
    //TODO: Multi property, including multi vs single phase etc
    enum SolverTargets{
        MAKESPAN, //!< makespan target
        ENERGY, //!< energy target
        SECURITY, //!< security target
        UNDEFINED
    };
    inline std::string to_string(const SolverTargets &v) {
        switch(v) {
            case SolverTargets::MAKESPAN: return "makespan";
            case SolverTargets::ENERGY: return "energy";
            case SolverTargets::SECURITY: return "security";
            default: return "unknown";
        }
    }
    inline SolverProperties::SolverTargets from_string(const std::string &str) {
        std::string lstr = boost::algorithm::to_lower_copy(str);
        if(lstr == "makespan")
            return SolverTargets::MAKESPAN;
        if(lstr == "energy")
            return SolverTargets::ENERGY;
        if(lstr == "security")
            return SolverTargets::SECURITY;
        return SolverTargets::UNDEFINED;
    }
}


#endif /* PROPERTIES_HPP */


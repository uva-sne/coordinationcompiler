/*
 * Copyright (C) 2020 Stephen Nicholas Swatman <s.n.swatman@uva.nl> 
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "Analyse/RegistryEntries.hpp"
#include "Generator/RegistryEntries.hpp"
#include "Solvers/RegistryEntries.hpp"
#include "Interconnect/RegistryEntries.hpp"
#include "Partitioning/RegistryEntries.hpp"
#include "Transform/RegistryEntries.hpp"
#include "InputParser/RegistryEntries.hpp"
#include "PriorityAssignment/RegistryEntries.hpp"
#include "Simulator/RegistryEntries.hpp"
#include "Exporter/RegistryEntries.hpp"
#include "StateMachine/RegistryEntries.hpp"
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   WorstFit.h
 * Author: brouxel
 *
 * Created on 19 juillet 2021, 10:48
 */

#ifndef WORSTFIT_H
#define WORSTFIT_H

#include "Partitioning.hpp"

class WorstFit : public Partitioning {
    //! Store the number of tasks allocated to each core
    std::map<ComputeUnit*, size_t> proc_groups;
    /**
     * True if all jobs need to be partitioned, False if processor assignment
     * are only done on tasks
     */
    bool is_full_schedule_required = false;

    const std::string get_uniqid_rtti() const override;
    
public:
    //! \brief Constructor
    WorstFit();
    void forward_params(const std::map<std::string,std::string>&args) override;
    std::string help() override;
    //! \brief Destructor
    ~WorstFit();
    
    virtual void check_dependencies();

protected:
    void do_partitioning() override;
};

REGISTER_PARTITIONING(WorstFit, "worst-fit")

#endif /* WORSTFIT_H */


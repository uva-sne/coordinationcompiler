/*
 * Copyright (C) 2019 Benjamin Rouxel <benjamin.rouxel@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GREEDY_H
#define GREEDY_H

#include "Partitioning.hpp"

/**
 * Perform a greedy partitioning
 * 
 * Assign all tasks to a core by balancing the number of tasks per core.
 * Tasks can be assigned only to the one that support them according to restriction
 * given by the coordiation/configuration files.
 * 
 */
class GreedyPartitioning : public Partitioning {
    //! Store the number of tasks allocated to each core
    std::map<ComputeUnit*, size_t> proc_groups;
    /**
     * True if all jobs need to be partitioned, False if processor assignment
     * are only done on tasks
     */
    bool is_full_schedule_required = false;

    const std::string get_uniqid_rtti() const override;
    
public:
    //! \brief Constructor
    GreedyPartitioning();
    void forward_params(const std::map<std::string,std::string>&args) override;
    std::string help() override;
    //! \brief Destructor
    ~GreedyPartitioning();
    
    virtual void check_dependencies();

protected:
    void do_partitioning() override;
    /**
     * Walk-through the graph following a Breed-First-Search algorithm
     * 
     * @note Graphs with cycles are accepted
     * 
     * @param tasks
     * @param visited
     */
    void BFS(const std::vector<Task*> &tasks, std::vector<Task*> *visited);
    
    /**
     * Auxiliary function for the BDS algorithm
     * 
     * @param visited
     * @param current
     * @param next
     */
    void BFS_aux(std::vector<Task*> *visited, Task *current, std::vector<Task*> *next);
    
    /**
     * Map the given task to a core following the greedy approach by balancing
     * the number of tasks on cores
     * @param t
     */
    void map_to_proc(Task *t);
};

REGISTER_PARTITIONING(GreedyPartitioning, "greedy")

#endif
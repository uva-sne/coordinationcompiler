/*
 * Copyright (C) 2019 Benjamin Rouxel <benjamin.rouxel@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "Greedy.hpp"
#include "Utils/Log.hpp"
#include "ScheduleFactory.hpp"

using namespace std;

const string GreedyPartitioning::get_uniqid_rtti() const { return "partitioning-greedy"; }

GreedyPartitioning::GreedyPartitioning() : Partitioning() {}

IMPLEMENT_HELP(GreedyPartitioning,
    Partitioned the tasks/components among cores
)
void GreedyPartitioning::forward_params(const map<string,string>&) {}

void GreedyPartitioning::check_dependencies() {
    is_full_schedule_required = conf->passes.find_scheduler_or_solver_before(this) == nullptr;
}

GreedyPartitioning::~GreedyPartitioning() {
}

void GreedyPartitioning::do_partitioning() {
    for(ComputeUnit *p: tg->processors())
        ScheduleFactory::add_core(schedule, p);

    if(is_full_schedule_required)
        ScheduleFactory::build_elements(schedule);
    else
        ScheduleFactory::build_minimal_elements(schedule);
    
    proc_groups.clear();
    for(ComputeUnit *p : tg->processors())
        proc_groups[p] = 0;
    
    vector<Task*> roots;
    for(Task *t : tg->tasks_it()) {
        if(t->predecessor_tasks().size() == 0)
            roots.push_back(t);
    }
    
    vector<Task*> visited;
    BFS(roots, &visited);
}

void GreedyPartitioning::BFS(const vector<Task*> &tasks, vector<Task*> *visited) {
    vector<Task*> next;
    for(Task *root : tasks) {
        visited->push_back(root);
        BFS_aux(visited, root, &next);

        map_to_proc(root);
    }
    if(next.empty())
        return;

    sort(next.begin(), next.end(), Utils::sort_Q_byWCET);
    BFS(next, visited);
}

void GreedyPartitioning::BFS_aux(vector<Task*> *visited, Task *current, vector<Task*> *next) {
    for(Task::dep_t els : current->successor_tasks()) {
        Task *s = els.first;
        if(find(visited->begin(), visited->end(), s) != visited->end())
            continue;
        if(all_of(s->predecessor_tasks().begin(), s->predecessor_tasks().end(), [visited](Task::dep_t elp) {
            return find(visited->begin(), visited->end(), elp.first) != visited->end();
        }))
            next->push_back(s);
    }
}

void GreedyPartitioning::map_to_proc(Task *t) {
    for(Version *v : t->versions()) {
        vector<pair<ComputeUnit*, size_t>> group;
        if(v->force_mapping_proc().size() > 0) {
            for(pair<ComputeUnit*, size_t> el : proc_groups) {
                if(find(v->force_mapping_proc().begin(), v->force_mapping_proc().end(), el.first) != v->force_mapping_proc().end())
                    group.push_back(el);
            }
        }
        else {
            for(pair<ComputeUnit*, size_t> el : proc_groups)
                group.push_back(el);
        }
        
        sort(group.begin(), group.end(), [](const pair<ComputeUnit*, size_t> &aa, const pair<ComputeUnit*, size_t> &bb) {
            return aa.second < bb.second;
        });
        proc_groups[group[0].first]++;
        for(SchedJob *j : schedule->schedjobs(t)) {
            schedule->map_core(j, schedule->schedcore(group[0].first));
            schedule->schedule(j);
            Utils::INFO("Map "+t->id()+"_"+v->id()+" on "+group[0].first->id);
        }
//        Utils::DEBUG(group[0].first->id+" - "+to_string(group[0].second)+" tasks currently mapped");
    }
}
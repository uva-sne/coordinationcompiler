/*
 * Copyright (C) 2019 Benjamin Rouxel <benjamin.rouxel@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "ProcessorUtilization.hpp"
#include "Utils/Log.hpp"
#include "Utils/Graph.hpp"
#include "ScheduleFactory.hpp"

using namespace std;


const string ProcessorUtilization::get_uniqid_rtti() const { return "partitioning-processor_utilization"; }

ProcessorUtilization::ProcessorUtilization() : Partitioning() {}

IMPLEMENT_HELP(ProcessorUtilization, 
    Partitioned the tasks/components among cores while balancing processor
    utilisation.\n
    max-utilization [0.0f .. 1.0f], default 0.8f: max utilisation per core
)
void ProcessorUtilization::forward_params(const map<string,string>&arg) {
    if(arg.count("max-utilization"))
        max_core_utilization = stof(arg.at("max-utilization"));
}

ProcessorUtilization::~ProcessorUtilization() {
}

void ProcessorUtilization::do_partitioning() {
    // init schedule
    for(ComputeUnit *p: tg->processors())
        ScheduleFactory::add_core(schedule,p);

    for(Task *t : tg->tasks_it()) {
        SchedJob *elt = ScheduleFactory::build_job(schedule, t, t->offset());
        elt->wct(t->C());
        schedule->schedule(elt);
    }
    
    //init current utilisation
    for(ComputeUnit *p: tg->processors()) 
        current_utilization[p] = 0.0;
    
    //init dags
    vector<vector<Task*>> dags;
    Utils::extractGraphs(tg, &dags);
    vector<size_t> graph_index(dags.size());
    vector<float> utilization(dags.size());
    
    //compute utilisation per dag
    size_t gr_index = 0;
    for(vector<Task*> dag : dags) {
        uint64_t wcet = 0;
        uint64_t period = 0;
        for(Task *t : dag) {
            wcet += t->C();
            if(period < t->T())
                period = t->T();
        }
        
        graph_index[gr_index] = gr_index;
        utilization[gr_index++] = wcet/(float)period;
    }
    //sort dags highest utilisation first
    sort(graph_index.begin(), graph_index.end(), [utilization](size_t a, size_t b) {
        return utilization[a] > utilization[b];
    });
    
    for(size_t i=0 ; i < dags.size() ; ++i) {
        vector<Task*> dag = dags[graph_index[i]];
        vector<Task*> roots;
        for(Task *t : dag) {
            if(t->predecessor_tasks().size() == 0)
                roots.push_back(t);
        }
        
        vector<Task*> visited;
        BFS(roots, &visited, tg->processors()[(i+1)%tg->processors().size()]);
    }
    
    for(vector<Task*> dag : dags) {
        for(Task* t : dag) {
            for(SchedJob *j : schedule->schedjobs(t))
                Utils::DEBUG("Task "+t->id()+" mapped on "+((schedule->get_mapping(j) != nullptr) ? schedule->get_mapping(j)->core()->id : "nullptr"));
        }
    }
    for(ComputeUnit *p : tg->processors()) 
        Utils::DEBUG("Utilization proc "+p->id+": "+to_string(current_utilization[p]));
}

void ProcessorUtilization::BFS(const vector<Task*> &tasks, vector<Task*> *visited, ComputeUnit *lastproc) {
    vector<Task*> next;
    ComputeUnit *nlastp = nullptr;
    for(Task *root : tasks) {
        visited->push_back(root);
        BFS_aux(visited, root, &next);
        
        for(Version *v : root->versions()) {
            ComputeUnit *p = get_proc(root, v, lastproc); //don't work for multiple version actually

            map_to_proc(root, p);
            nlastp = p;
        }
    }
    if(next.empty())
        return;

    if (nlastp == nullptr) {
        throw std::logic_error("No last processor selected!");
    }

    sort(next.begin(), next.end(), [](Task *a, Task *b) { return a->C()/a->T() > b->C()/b->T(); });
    BFS(next, visited, nlastp);
}

void ProcessorUtilization::BFS_aux(vector<Task*> *visited, Task *current, vector<Task*> *next) {
    for(Task::dep_t els : current->successor_tasks()) {
        Task *s = els.first;
        if(find(visited->begin(), visited->end(), s) != visited->end())
            continue;
        if(all_of(s->predecessor_tasks().begin(), s->predecessor_tasks().end(), [visited](Task::dep_t elp) {
            return find(visited->begin(), visited->end(), elp.first) != visited->end();
        }))
            next->push_back(s);
    }
}

ComputeUnit* ProcessorUtilization::get_proc(Task *t, Version *v, ComputeUnit *lastp) {
    vector<ComputeUnit*> group;
    if(v->force_mapping_proc().size() > 0)
        group.insert(group.begin(), v->force_mapping_proc().begin(), v->force_mapping_proc().end());
    else
        group.insert(group.begin(), tg->processors().begin(), tg->processors().end());

    if(find(group.begin(), group.end(), lastp) != group.end()) {
        if(current_utilization[lastp]+(v->C()/(float)t->T()) < max_core_utilization)
            return lastp;
    }

    sort(group.begin(), group.end(), [this](ComputeUnit *a, ComputeUnit *b) {
        return current_utilization.at(a) < current_utilization.at(b);
    });

    for(ComputeUnit *p : group) {
        if(current_utilization[p]+(v->C()/(float)t->T()) < max_core_utilization)
            return p;
    }
    
    throw CompilerException("partitioning", "Can't find a processor with a low enough utilization (max: "+to_string(max_core_utilization)+" ) for task "+t->id());
}

void ProcessorUtilization::map_to_proc(Task *t, ComputeUnit *p) {
    for(SchedJob *j : schedule->schedjobs(t)) {
        schedule->map_core(j, schedule->schedcore(p));
        current_utilization[p] += t->C()/(float)t->T();
    }
}

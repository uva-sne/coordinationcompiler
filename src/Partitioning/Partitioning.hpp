/*
 * Copyright (C) 2019 Benjamin Rouxel <benjamin.rouxel@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PARTITIONING_H
#define PARTITIONING_H

#include "config.hpp"
#include "SystemModel.hpp"
#include "Schedule.hpp"
#include "Utils.hpp"
#include "CompilerPass.hpp"
#include "Properties.hpp"

/*!
 * \brief Base class for partitioning algorithms
 * 
 * Run a partitioning algorithm to assign tasks to cores
 */
class Partitioning : public CompilerPass {
protected:
    //! IR of the current application
    const SystemModel *tg;
    //! the global configuration
    const config_t *conf;
    //! current state of the schedule
    Schedule *schedule;
    
public:
    //! Constructor
    explicit Partitioning() {}
    void run(SystemModel *m, config_t *c, Schedule *s) override {
        tg = m;
        conf = c;
        schedule = s;
        
        do_partitioning();
        schedule->status(ScheduleProperties::sched_statue_e::SCHED_PARTIAL);
        schedule->mapping_mode(ScheduleProperties::Mapping::MAP_PARTITIONED);
    }
    

protected:
    //! Perform the partitining
    virtual void do_partitioning() = 0;
};

/*!
 * \typedef PartitionRegistry
 * \brief Registry containing all partitioning algorithms
 */
using PartitionRegistry = registry::Registry<Partitioning, std::string>;

/*!
 * \brief Helper macro to register a new derived analysis
 */
#define REGISTER_PARTITIONING(ClassName, Identifier) \
  REGISTER_SUBCLASS(Partitioning, ClassName, std::string, Identifier)

#endif /* PARTITIONING_H */


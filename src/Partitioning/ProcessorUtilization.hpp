/*
 * Copyright (C) 2019 Benjamin Rouxel <benjamin.rouxel@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PROCESSORUTILIZATION_H
#define PROCESSORUTILIZATION_H

#include "Partitioning.hpp"

/**
 * Perform a partitioning based on processor utilisation
 *
 * As a reminder the utilisation of a task is \f[U(t) = \frac{C}{T}\f], and the utilisation
 * of a core is \f[U = \sum\limits_{t \in \tau} U(t)\f]
 * 
 * Assign all tasks to a core by filling each core to a maximal utilisation.
 * 
 * Tasks can be assigned only to the one that support them according to restriction
 * given by the coordiation/configuration files.
 * 
 */
class ProcessorUtilization : public Partitioning {
    //! Store the utilisation of each core
    std::map<ComputeUnit*, float> current_utilization;
    //! Maximal utilisation for a core
    float max_core_utilization = 0.8;
    
    //! \copydoc CompilerPass::get_uniqid_rtti
    const std::string get_uniqid_rtti() const override;
    
public:
    //! \brief Constructor
    ProcessorUtilization();
    void forward_params(const std::map<std::string,std::string>&args) override;
    std::string help() override;
    //! \brief Destructor
    ~ProcessorUtilization();

protected:
    void do_partitioning() override;
    
    /**
     * Walk-through the graph following a Breed-First-Search algorithm
     * 
     * @note Graphs with cycles are accepted
     * 
     * @param tasks
     * @param visited
     * @param lastproc
     */
    void BFS(const std::vector<Task*> &tasks, std::vector<Task*> *visited, ComputeUnit *lastproc);
    
    /**
     * Auxiliary function for the BDS algorithm
     * 
     * @param visited
     * @param current
     * @param next
     */void BFS_aux(std::vector<Task*> *visited, Task *current, std::vector<Task*> *next);
         
    /**
     * Get a processor with enough utilisation to handle the task
     * 
     * @param t
     * @param v
     * @param lastp
     * @return 
     */
    ComputeUnit* get_proc(Task *t, Version *v, ComputeUnit *lastp);
    /**
     * Map the given task to a core 
     * 
     * @param t
     * @param p
     */
    void map_to_proc(Task *t, ComputeUnit *p);
};

REGISTER_PARTITIONING(ProcessorUtilization, "processor_utilization")

#endif
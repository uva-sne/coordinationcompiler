/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   WorstFit.cpp
 * Author: brouxel
 * 
 * Created on 19 juillet 2021, 10:48
 */

#include "WorstFit.hpp"
#include "ScheduleFactory.hpp"

using namespace std;

const string WorstFit::get_uniqid_rtti() const { return "partitioning-worstfit"; }

WorstFit::WorstFit() : Partitioning() {}

IMPLEMENT_HELP(WorstFit,
    Partitioned the tasks/components among cores
)
void WorstFit::forward_params(const map<string,string>&) {}

void WorstFit::check_dependencies() {
    is_full_schedule_required = conf->passes.find_scheduler_or_solver_before(this) == nullptr;
}

WorstFit::~WorstFit() {
}

void WorstFit::do_partitioning() {
    for(ComputeUnit *p: tg->processors())
        ScheduleFactory::add_core(schedule,p);

    proc_groups.clear();
    for(ComputeUnit *p : tg->processors())
        proc_groups[p] = 0;
    
    map<Task*, timinginfos_t> length;
    map<Task*, float> density;
    map<Task*, float> utlisation;
    
//    vector<vector<Task*>> dags;
//    Utils::extractGraphs(tg, &dags);
//    for(vector<Task*> dag : dags) {
//        Utils::BFS(dag, [](Task *t) {});
//    }
    
    vector<Task*> roots;
    for(Task *t : tg->tasks_it()) {
        if(t->predecessor_tasks().size() == 0)
            roots.push_back(t);
    }
    
}

/*!
 * \file CompilerPass.hpp
 * \copyright GNU Public License v3.
 * \date 2019 
 * \author Benjamin Rouxel <benjamin.rouxel@uva.nl>, University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef COMPILERPASS_H
#define COMPILERPASS_H

#include <map>
#include <string>

class Schedule;
class SystemModel;
class StrategyStateMachine;

struct config_t;

/*!
 * \interface CompilerPass
 * \brief Base interface for all compiler passes
 */
class CompilerPass {
public:
    /*!
     * \brief Run the compiler pass
     * \param m IR of the current application
     * \param c the global configuration
     * \param ssm current state of the schedule
     */
    virtual void run(SystemModel *m, config_t *c, StrategyStateMachine *ssm);

    /*!
     * \brief Run the compiler pass
     * \param m IR of the current application
     * \param c the global configuration
     * \param s current state of the schedule
     */
    virtual void run(SystemModel *m, config_t *c, Schedule *s);

    /*!
     * \brief Run the compiler pass without schedule or strategy state machine
     * \param m IR of the current application
     * \param c the global configuration
     */
    virtual void run(SystemModel *m, config_t *c) {
        run(m, c, (Schedule *) nullptr);
    }

    /*!
     * \brief Set params extracted from the configuration file
     * \param args   map of parameters
     */
    virtual void set_params(const std::map<std::string, std::string> &args);
    
    /*!
     * \brief Allows RunTime Type Inference with a unique id (downcast)
     * \return a unique identifier
     */
    virtual const std::string get_uniqid_rtti() const = 0;
    
    /*!\brief Base destructor*/
    virtual ~CompilerPass() {}
    
    /*!
     * \brief Show help message 
     * \return help msg
     */
    virtual std::string help() = 0;
    virtual std::string more_help(const std::string &key) { return "";}
    
    virtual bool force_unschedulable() { return force_unsched; }
    
protected:
    /*!
     * \brief Check if depedencies are met
     * \throws CompilerException if a dependency is not met
     */
    virtual void check_dependencies() {};
    
    virtual void forward_params(const std::map<std::string, std::string> &) = 0;
    
private:
    bool force_unsched = false;
};

#endif /* COMPILERPASS_H */


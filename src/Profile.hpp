/*!
 * \file Profile.hpp
 * \copyright GNU Public License v3.
 * \date 2020
 * \author Benjamin Rouxel <Wouter Loeve <wouterloeve0@gmail.com> University of Amsterdam
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PROFILE_H
#define PROFILE_H

#include <string>
#include <vector>
#include <map>
#include <iostream>

#include "config.hpp"
#include "Utils/Log.hpp"
#include "Utils.hpp"

#include <boost/algorithm/string/replace.hpp>

//! Modifier
enum Modifier { empty, vital };

//! Describe an option
struct ProfileOption {
    std::string value = ""; //!< value of the option
    bool remove = false; //!< Should be remove
    std::vector<Modifier> modifiers; //!< Modifier of the option
    /*! \brief True if the option is the default one
     * \note Is this option part of a fault-tolerance default?
     */
    bool defaultOption = false;
};

/*! \brief Globally defined profile
 */
class Profile {
    private:
        std::map<std::string, ProfileOption> settings; //!< settings key=Option

        // // A vector of keys which stores the order in which the map is inserted.
        // vector<string> order;

        /**
         * \brief Add default value for a given name
         * @param name
         * @param modifier
         * @param modifierOnly
         */
        void addDefault(std::string name, Modifier modifier, bool modifierOnly);
    public:
        /**
         * Constructor
         */
        Profile();

        /**
         * Get default profile option
         * @param value
         * @return 
         */
        ProfileOption defaultProfileOption(std::string value);

        /**
         * \brief Set default value for the specified name
         * 
         * If setting is a fault-tolerance method, defaults are automatically initialised.
         * 
         * @param name
         * @param val
         * @return val
         */
        bool setDefault(std::string name, bool val);

        /**
         * Check if name uses the default value
         * @param name
         * @return 
         */
        bool getDefault(std::string name);

        /*! \brief Add an option, a new struct is created, modifiers, removal etc are not kept.
         * 
         * Cascades the modifier to the default values 
         * 
         * @param name
         * @param value
         */
        void add(std::string name, std::string value);
        /**
         * \brief Add an option, a new struct is created, modifiers, removal etc are not kept.
         * 
         * Cascades the modifier to the default values 
         * 
         * @param name
         * @param value
         * @param mod
         */
        void add(std::string name, std::string value, std::string mod);

        /*! \brief Remove an option
         * \param name
         */
        void remove(std::string name);
        
        void remove(const Profile &prof);

        /*! \brief Add option for removal
         * \param name
         */
        void addRemove(std::string name);

        /*! \brief Retrieve removal vector
         * \return
         */
        std::vector<std::string> getRemovals() const;

        /*! \brief Get option
         * \param name
         * \return
         */
        std::string get(std::string name) const;

        /*! \brief Get modifier.
         * Multi modifiers not yet defined.
         * @param name
         * @return 
         */
        Modifier getModifier(std::string name) const;
        
        /**
         * \brief Return the modifier representation from a string
         * @param m
         * @return 
         */
        Modifier string_to_modifier(std::string m);
        /**
         * \brief Return the string representation of a modifier
         * @param m
         * @return 
         */
        static std::string modifier_to_string(Modifier m);

        /*! \brief Add modifier
         * @param name
         * @param value
         */
        void addModifier(std::string name, Modifier value);

        /**
         * \brief Add modifier
         * @param name
         * @param value
         */
        void addModifier(std::string name, std::string value);

        /*! \brief Export to dot format
         * \return
         */
        std::string to_dot();

        /*! \brief Export to string
         * \return
         */
        std::string to_string();

        /*! \brief Has option
         * \param key
         * \return
         */
        bool hasMember(std::string key) const;

        /*! \brief Has modifier
         * \param key
         * \return
         */
        bool hasModifierMember(std::string key) const;
        
        /*! retrieve setting map
         * 
         * \return
         */
        std::map<std::string, ProfileOption> getSettings();
        
        /*! \brief Merge option
         * 
         * \param addName
         * \param addOpt
         * \param profile
         */
        void mergeOption(std::string addName, ProfileOption addOpt, Profile profile);

        /*! \brief Merges or cascades the given profile to one profile 
         * 
         * Order of the given profiles matters as the second overwrites the first, 
         *  the third overwrites the combination of the first and second etc.
         * Inline profile should be last in the vector as it should overwrite everything else.
         * 
         * \param profiles to merge
         * \return a new profile
         */
        Profile mergeProfiles(std::vector<Profile> profiles);
        
        /*! \brief Merges the given profile to the current one
         * 
         * \param profiles to merge
         */
        void merge(Profile &profile);
};

#endif /* PROFILE_H */

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   StatsReadyJobs.h
 * Author: brouxel
 *
 * Created on 9 novembre 2021, 16:11
 */

#ifndef STATSREADYJOBS_H
#define STATSREADYJOBS_H

#include "Simulator/Simulator.hpp"
#include "Simulator/OnlineSchedSim/BaseOnlineSchedSim.hpp"

class StatsReadyJobs : public SimulatorEventHandler {
    std::ofstream fstats;
public:
    StatsReadyJobs() : SimulatorEventHandler() {}
    
    virtual void event(const std::string &event, Simulator *caller) override;

    virtual void forward_params(const std::map<std::string, std::string> &params) override;
    const std::string get_uniqid_rtti() const override;
    virtual std::string help() override;
    
private:
    void fetch_job_stats(BaseOnlineSimulator *caller);
};

REGISTER_SIM_EVENT_HANDLER(StatsReadyJobs, "stats-ready-jobs")

#endif /* STATSREADYJOBS_H */


/*
 * Copyright (C) 2019 Benjamin Rouxel <benjamin.rouxel@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "GPUOrdered.hpp"
#include "Utils/Log.hpp"
#include "Utils/Graph.hpp"

using namespace std;

const string GPUOrdered_CritPath_EDF::get_uniqid_rtti() const { return "simulator-event-gpuordered"; }

IMPLEMENT_HELP(GPUOrdered_CritPath_EDF, 
    Assign dynamic priority to job by earliest deadline -- Earliest Deadline First EDF\n\n
    Assign priority to jobs following a EDF algorithm\n
    This priority scheme assigns priority to tasks dynamically (simulator) following the deadline
    of the task. Task with closest deadline gets highest priority.\n
    \n
    Priority are increasing, highest priority gets the smallest value of 1.
)

void GPUOrdered_CritPath_EDF::event(const string &event, Simulator *caller) {
    if(event != "ready-queue-computed")
        return;
    
    BaseOnlineSimulator *ccaller = dynamic_cast<BaseOnlineSimulator*>(caller);
    list<SchedElt*> elts = ccaller->ready_queue();
    
    map<SchedElt*, SchedElt*> submitters;
    vector<SchedElt*> submittees;
    
    for(SchedElt *t : elts) {
        string tname = t->schedtask()->task()->id();
        if(t->schedtask()->task()->type() != TaskProperties::TaskType::TT_SUBMITTEE) 
            continue;

        SchedElt *submitter = nullptr;
        for(SchedElt *p : t->previous()) {
            if(p->schedtask()->task()->type() == TaskProperties::TaskType::TT_SUBMITTER && p->min_rt_period() == t->min_rt_period()) {
                submitter = p;
            }
        }
        if(submitter == nullptr)
            throw CompilerException("MultiCoreOnlineSim", "missing submitter for "+tname);

        if(find(elts.begin(), elts.end(), submitter) != elts.end())
            continue;

        submitters[t] = submitter;
        submittees.push_back(t);
    }
    
    if(submittees.size() > 1) {
        //order submittees according to the submitters order
        sort(submittees.begin(), submittees.end(), [submitters](SchedElt *a, SchedElt *b){return submitters.at(a)->rt()+submitters.at(a)->wct() < submitters.at(b)->rt()+submitters.at(b)->wct(); });
        
        for(auto it=submittees.begin(), et=submittees.end() ; it+1 != et ; ++it) {
            SchedElt *src = *it;
            SchedElt *dst = *(it+1);

            // Add a dependency to force the order
            if(find(src->successors().begin(), src->successors().end(), dst) == src->successors().end())
                src->add_successor(dst);
        }
    }
}

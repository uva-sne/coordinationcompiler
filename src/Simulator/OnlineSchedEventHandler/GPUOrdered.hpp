/*
 * Copyright (C) 2019 Benjamin Rouxel <benjamin.rouxel@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GPUORDEREDBOOST_EDF_HPP
#define GPUORDEREDBOOST_EDF_HPP

#include "Simulator/Simulator.hpp"
#include "Simulator/OnlineSchedSim/BaseOnlineSchedSim.hpp"

/**
 * Perform a EDF priority assignment
 * 
 * GPU task priority follows the scheduling order of their submitter
 * 
 * \copydoc help
 * 
 */
class GPUOrdered_CritPath_EDF : public SimulatorEventHandler {
public:
    //! \brief Constructor
    GPUOrdered_CritPath_EDF() : SimulatorEventHandler() {}
    
    std::string help() override;
    const std::string get_uniqid_rtti() const override;
    
    virtual void event(const std::string &event, Simulator *caller) override;
    
protected:
};

REGISTER_SIM_EVENT_HANDLER(GPUOrdered_CritPath_EDF, "gpu-ordered")


#endif /* GPUORDEREDBOOST_EDF_HPP */


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   StatsReadyJobs.cpp
 * Author: brouxel
 * 
 * Created on 9 novembre 2021, 16:11
 */

#include "StatsReadyJobs.hpp"

using namespace std;

const string StatsReadyJobs::get_uniqid_rtti() const {
    return "simulator-event-stats-ready-jobs";
}
IMPLEMENT_HELP(StatsReadyJobs,
    Get statistics on the amount of jobs that are ready for execution each time
    the fetch job is called.    
)

void StatsReadyJobs::forward_params(const std::map<std::string, std::string> &params) {
}
    
void StatsReadyJobs::event(const string &event, Simulator *caller) {
    BaseOnlineSimulator *ccaller = dynamic_cast<BaseOnlineSimulator*>(caller);
    if(caller == nullptr)
        return;
    
    if(event == "pre-schedule") {
        fstats.open("fetch_job_stats.csv");
        fstats << "tic;core;#ready" << endl;
    }
    else if(event == "post-schedule") {
        fstats.close();
    }
    else if(event == "fetch-new-job") {
        fetch_job_stats(ccaller);
    }
}


void StatsReadyJobs::fetch_job_stats(BaseOnlineSimulator *caller) {
    list<SchedElt*> ready_queue = caller->ready_queue();
    SchedCore *current_target = caller->current_target();
    
    vector<SchedElt*> ready_to_exec;
    SchedElt *elt_is_ready = nullptr;
    while((elt_is_ready = caller->fetch_job(&ready_queue, current_target)) != nullptr) {
        ready_to_exec.push_back(elt_is_ready);
    }
    
    for(vector<SchedElt*>::reverse_iterator it=ready_to_exec.rbegin(), et=ready_to_exec.rend() ; it != et ; ++it)
        ready_queue.push_front(*it);
    
    fstats << current_target->clock() << ";" << current_target->core()->id << ";" << ready_to_exec.size() << endl;
}
/*!
 * \file OnlineSchedSim.hpp
 * \copyright GNU Public License v3.
 * \date 2020
 * \author Benjamin Rouxel <benjamin.rouxel@uva.nl>, University of Amsterdam
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "SingleCore.hpp"

using namespace std;

IMPLEMENT_HELP_WITH_CODE(SingleCoreOnlineSchedSim, {
    msg += "HTMLINDENT- priority, default EDF: priority assignment method, also determines if the priority are assigned statically or dynamically.\n";
    for(string p : PriorityAssignmentRegistry::keys())
        msg += "HTMLINDENT- "+p+"\n";
    msg +=  "HTMLINDENT- event-handler: if you want a attack/defense mechanism\n";
    for(string e : SimulatorEventHandlerRegistry::keys()) {
            msg += "HTMLINDENT- "+e+"\n";
    }
}, Simulate the behaviour of an on-line scheduler on a given task set. 
    As real execution times are not known, well we don`t execute the tasks, then
    execution times are randomly decided using a poisson distribution with 
    a mean at 50% of the respective WCET and 
    using the default CPP random generator.\n
    \n
    HTMLINDENT- preemption [true, false, limited], default false: activate/deactivate 
    the possibility to preempt a task when a more prioritary one arises. 
    Limited preemption (TODO!!) on the other hand, allow a higher level task to preempt
    a lower level one, only if by no preempting, the high level task misses its deadline.\n
    HTMLINDENT- force-wcet [bool], deafult false: if true, execution time is not random
    but forced to be the respective WCET.\n
    HTMLINDENT- hyperperiods [uint32_t], default 1: number of hyperperiod to simulate\n
)

void SingleCoreOnlineSchedSim::forward_params(const std::map<std::string,std::string>& args) {
    BaseOnlineSimulator::forward_params(args);
    
    preemption = preemption_e::NO;
    if(args.count("preemption")) {
        if(args.at("preemption") == "true")
            preemption = preemption_e::YES;
        else if(args.at("preemption") == "limited")
            preemption = preemption_e::LIMITED;
    }
}

void SingleCoreOnlineSchedSim::check_dependencies() {
    BaseOnlineSimulator::check_dependencies();
    
    if(tg->processors().size() > 1)
        throw Todo("Single-core Online simulator only supports uniprocessor");
}


SingleCoreOnlineSchedSim::~SingleCoreOnlineSchedSim() {
}

void SingleCoreOnlineSchedSim::do_simulate() {
    _current_target = sched->schedcores().at(0);
    _current_target->clock(0);
    
    priority->run(tg, conf, sched);
    
    for(timinginfos_t tic = 0 ; tic < force_sched_length || (empty_ready_queue && !_ready_queue.empty()) ; ++tic) {
//        cout << tic << "\r";
        if(tic % tg->hyperperiod() == 0)
            notify("start-of-hyperperiod");
        checkTimeout();

        compute_ready_queue(tic, &_ready_queue);
        if(_ready_queue.size() == 0) {
            tic += ellipsis(tic, _ready_queue)-1; //-1 as there will be a ++
            continue;
        }
        
        if(_current_target->clock() < tic)
            _current_target->clock(tic);
        if(_current_target->clock() > tic) {
            tic += ellipsis(tic, _ready_queue)-1; //-1 as there will be a ++
            continue;
        }

        priority->update(&_ready_queue);
        sort_ready_queue(&_ready_queue);

        SchedElt *elt = fetch_job(&_ready_queue, _current_target);
        _ready_queue.pop_front();

        elt->rt(_current_target->clock());
        sched->map_core(elt, _current_target);
        sched->schedule(elt); 
        
        Utils::DEBUG(to_string(tic)+" Sched "+elt->toString());
        
        _current_target->clock(_current_target->clock()+elt->wct());
        for(SchedElt *succ : elt->successors())
            succ->rt(_current_target->clock());
        
        if(elt->min_rt_deadline() < elt->rt()+elt->wct()) {
            if(deadline_miss_throw)
                throw Unschedulable(elt, "Deadline miss");
            else
                Utils::WARNONCE("Deadline miss "+elt->schedtask()->task()->id());
        }
    }
}

SchedCore* SingleCoreOnlineSchedSim::get_mapping(SchedElt *elt, SchedCore *current) {
    return current;
}
/*!
 * \file BaseOnlineSimulator.hpp
 * \copyright GNU Public License v3.
 * \date 2020
 * \author Benjamin Rouxel <benjamin.rouxel@uva.nl>, University of Amsterdam
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "BaseOnlineSchedSim.hpp"
#include "ScheduleFactory.hpp"

using namespace std;

IMPLEMENT_HELP_WITH_CODE(BaseOnlineSimulator, {
    msg += "HTMLINDENT- priority, default EDF: priority assignment method, also determines if the priority are assigned statically or dynamically.\n";
    for(string p : PriorityAssignmentRegistry::keys())
        msg += "HTMLINDENT- "+p+"\n";
    msg +=  "HTMLINDENT- event-handler: if you want a attack/defense mechanism\n";
    for(string e : SimulatorEventHandlerRegistry::keys()) {
            msg += "HTMLINDENT- "+e+"\n";
    }
}, Simulate the behaviour of an on-line scheduler on a given task set. 
    As real execution times are not known, well we don`t execute the tasks, then
    execution times are randomly decided using a poisson distribution with 
    a mean at 50% of the respective WCET and 
    using the default CPP random generator.\n
    \n
    HTMLINDENT- preemption [true, false, limited], default false: activate/deactivate 
    the possibility to preempt a task when a more prioritary one arises. 
    Limited preemption (TODO!!) on the other hand, allow a higher level task to preempt
    a lower level one, only if by no preempting, the high level task misses its deadline.\n
    HTMLINDENT- force-wcet [bool], deafult false: if true, execution time is not random
    but forced to be the respective WCET.\n
    HTMLINDENT- hyperperiods [uint32_t], default 1: number of hyperperiod to simulate\n
)

void BaseOnlineSimulator::forward_params(const std::map<std::string,std::string>& args) {
    
    if(args.count("force_sched_length"))
        force_sched_length = Utils::stringtime_to_ns(args.at("force_sched_length"));
    
    if(args.count("hyperperiod_qty"))
        hyperperiod_qty = stoi(args.at("hyperperiod_qty"));
    
    if(args.count("exectime")) {
        if(args.at("exectime") == "margin")
            exectime_algo = BaseOnlineSimulator::exectime_algo_e::MARGIN;
        else if(args.at("exectime") == "poisson")
            exectime_algo = BaseOnlineSimulator::exectime_algo_e::RAND_POISSON;
        else if(args.at("exectime") == "wcet")
            exectime_algo = BaseOnlineSimulator::exectime_algo_e::WCET;
        else
            throw CompilerException("OnlineSim", "Unknown exec time computation algo");
    }
    
    if(args.count("timeout"))
        timeout = Utils::stringtime_to_ns(args.at("timeout"));
    
    if(args.count("empty_ready_queue"))
        empty_ready_queue = args.at("empty_ready_queue") == CONFIG_TRUE;
    
    if(args.count("margin"))
        margin = stoi(args.at("margin"));
    
    if(args.count("seed")) {
        seed = stoi(args.at("seed"));
        if(seed == 0)
            throw CompilerException("OnlineSim", "You can't set a seed to 0 for the random generator");
    }
    
    if(args.count("deadline_miss"))
        deadline_miss_throw = (args.at("deadline_miss") != "warn");

    string prio = "EDF";
    if(args.count("priority"))
        prio = args.at("priority");
    priority = PriorityAssignmentRegistry::Create(prio);
}

void BaseOnlineSimulator::check_dependencies() {
    if(conf->runtime.rtt != runtime_task_e::SINGLEPHASE) 
        throw Todo("Online simulator only supports tasks with a single phase");
    
    if(tg->processors().size() < 1)
        throw CompilerException("online sched simulator", "Wrong number of processors");
}


BaseOnlineSimulator::~BaseOnlineSimulator() {
    if(priority != nullptr)
        delete priority;
    for(auto el : distributions)
        delete el.second;
}

void BaseOnlineSimulator::simulate() {
    tg->compute_hyperperiod();
    sched->clear();
    if(force_sched_length == 0)
        force_sched_length = hyperperiod_qty*tg->hyperperiod();
    
    for(ComputeUnit *p: tg->processors())
        ScheduleFactory::add_core(sched, p);
    
    for(ResourceUnit *r : tg->resources())
        ScheduleFactory::add_resource(sched, r);
    
    if(seed > 0)
        generator.seed(seed);
    
    for(Task *t : tg->tasks_it()) {
        repeat[t] = 0;
        job_counter[t] = 0;
    }
    for(Task *t : tg->tasks_it()) {
        if(t->predecessor_tasks().empty())
            roots.push_back(t);
    }

    Utils::DEBUG("Start On-line simulation for "+to_string(tg->hyperperiod())+"ns");
    notify("pre-schedule");
    
    do_simulate();
    
    sched->status(ScheduleProperties::sched_statue_e::SCHED_COMPLETE);
    sched->compute_makespan();
    
    notify("post-schedule");
    
//    Utils::DEBUG(""+*sched);
}

bool BaseOnlineSimulator::compute_ready_queue(timinginfos_t tic, list<SchedElt*> *rq) {
    if(tic >= force_sched_length) //we are overtime with a ready queue that is not over
        return 0;
    
    bool added_job = false;
    vector<SchedJob*> newjobs;
    for(Task *t : tg->tasks_it()) {
        if(t->parent_task() != nullptr && t->repeat() > 0 && repeat[t] == t->repeat()) {
            if(t->parent_task()->T() > 0 && tic >= t->parent_task()->T() && !(tic % t->parent_task()->T()) ) {
                if(t->parent_task()->repeat() == 0 || repeat[t->parent_task()] <= t->parent_task()->repeat()) {
                    // restart a new hyperepoch
                    repeat[t] = 0;
//                    timinginfos_t offset = t->T() > 0 ? t->offset() + (t->parent_task()->T() % t->T()) : t->offset() + t->parent_task()->T();
                    timinginfos_t offset = t->offset() + t->parent_task()->T();
                    t->offset(offset);
                    Utils::DEBUG(to_string(tic)+" Restart hyperepoch "+t->parent_task()->id()+" for "+t->id()+" -- "+to_string(t->parent_task()->T())+" -- "+to_string(t->offset()));
                    newjobs.push_back(ScheduleFactory::build_job(sched, t, t->offset()));
                }
            }
        }
        // repeat == 0 -> infinite repetition
        if((t->repeat() == 0 || repeat[t] < t->repeat()) && (tic == t->offset()+repeat[t]*t->T())) {
            SchedJob* job = ScheduleFactory::build_job(sched, t, t->offset()+repeat[t]*t->T());
            job->rt(tic);
            job->wct(generate_exec_time(t));
            
            if(t->versions().size()) {
                size_t vers_index = repeat[t] % t->versions().size();
                Version *v = t->versions().at(vers_index);
                job->selected_version(v->id());
            }
            
            added_job = try_release_job(rq, job) || added_job;
            newjobs.push_back(job);
            ++repeat[t];
            ++job_counter[t];
            
            job->priority(-1);
        }
    }
    //port precedence constraints from tasks to jobs
    // precedence, all tasks of a same task graph have the same period, so all jobs of a graph are added at the same time
    for(SchedJob *job : newjobs) {
        const Task *tj = job->schedtask()->task();
        for(const Task::dep_t elp : tj->predecessor_tasks()) {
            for(SchedElt *jp : active_jobs[elp.first])
                jp->add_successor(job);
        }
    }
    return added_job;
}

bool BaseOnlineSimulator::try_release_job(std::list<SchedElt*> *rq, SchedElt *job) {
    const Task *t = job->schedtask()->task();
    
    if(find(active_jobs[t].begin(), active_jobs[t].end(), job) == active_jobs[t].end())
        active_jobs[t].push_back(job);
    
    if(active_jobs[t].size() == 1) {
        if(find(rq->begin(), rq->end(), job) == rq->end()) {
            Utils::DEBUG(" Add job "+job->toString()+" -- repeat: "+to_string(repeat[job->schedtask()->task()]));
            rq->push_back(job);
            return true;
        }
    }
    return false;
}

bool BaseOnlineSimulator::commit_job(std::list<SchedElt*> *rq, SchedElt *job) {
    Task *t = const_cast<Task *>(job->schedtask()->task());
    SchedElt *a = active_jobs[t].front();
    
    if(a != job)
        throw CompilerException("On-line Simulator", "Try to commit a job that is not the current active one");
    
    // pop active
    active_jobs[t].pop_front();
    
    // activate next one
    if(active_jobs[t].size() > 0) {
        SchedElt *newjob = active_jobs[t].front();
        Utils::DEBUG(" Commit & Add job "+newjob->toString()+" -- repeat: "+to_string(repeat[job->schedtask()->task()]));
        rq->push_back(newjob);
        return true;
    }
    return false;
}

SchedElt *BaseOnlineSimulator::fetch_job(list<SchedElt*> *rq, SchedCore *target) {
    SchedElt *elt = nullptr;
    list<SchedElt*> to_push_back;
    while(rq->size() > 0) {
        elt = rq->front();
        rq->pop_front();
        SchedCore *c = get_mapping(elt, target);
        //if good mapping && min/init rt < current tic
        if(target == c && elt->rt() <= target->clock()) {
            bool dep_satisfied = true;
            for(SchedElt *prev : elt->previous()) {
                //if(!sched->is_task_fully_scheduled(prev->schedtask())) {
                // if the SchedElt is not mapped then it is not scheduled
                //   avoid scanning the scheduled element to find it as it becomes big
                if(sched->get_mapping(prev->schedtask()) == nullptr) {
                    dep_satisfied = false;
//                    cout << "Dep not satisfied for " << elt->schedtask()->task()->id() << " --- (" << prev->schedtask()->task()->id() << ")" << " ----- " << c->core()->id << " " << target->clock() << " ---- " << sched->is_task_fully_scheduled(prev->schedtask()) << endl; 
                    break;
                }
                if(prev->rt()+prev->wct() > target->clock()) {
                    // prev is on another core to whom the clock is in the future compare to target
                    dep_satisfied = false;
//                    cout << "Dep not satisfied for " << elt->schedtask()->task()->id() << " --- (" << prev->schedtask()->task()->id() << ")" << " ----- " << c->core()->id << " " << target->clock() << " -- need to wait " << (prev->rt()+prev->wct()) << endl; 
                    break;
                }
            }
            if(dep_satisfied) {
//                cout << "Dep satisfied for " << elt->schedtask()->task()->id() << " ----- " << c->core()->id << " " << target->clock() << endl; 
//                for(SchedElt *prev : elt->previous()) {
//                    cout << "\t" << prev->toString() << " -- " << prev->rt() << "+" << prev->wct() << "=" << (prev->rt()+prev->wct()) << endl;
//                }
                bool ress_dep_satisfied = true;
                for(ResourceUnit *r : elt->schedtask()->task()->resource_usage()) {
                    if(r->access == resource_access_type_e::MUTEX && sched->schedresource(r->id)->is_booked(target->clock(), target->clock()+elt->wct())) {
                        ress_dep_satisfied = false;
//                            cout << "\tRess " << r->id << " booked [" << target->clock() << "," << (target->clock()+elt->wct()) << "] by " << sched->schedresource(r->id)->booked_by(target->clock(), target->clock()+elt->wct())->toString() << endl;
                        break;
                    }
                }
                if(ress_dep_satisfied) {
//                        cout << "\t=> Resource satisfied" << endl;
//                        for(ResourceUnit *r : elt->schedtask()->task()->resource_usage()) {
//                            cout << "\t\t" << r->id << endl;
//                        }
                        break;
                }
//                    cout << "\t==> NOT resource ready" << endl;
            }
        }
//        if(c != nullptr) //no known mapping, mostly likely a hyperepoch metatask
            to_push_back.push_back(elt);
        elt = nullptr;
    }
    for(auto it=to_push_back.rbegin(); it != to_push_back.rend(); ++it) {
        SchedElt *a = *it;
        rq->push_front(a);
    }
    return elt;
}

// Lowest value -> higher priority
void BaseOnlineSimulator::sort_ready_queue(list<SchedElt*> *rq) {
    rq->sort([](SchedElt *a, SchedElt *b) {
        return a->schedtask()->priority() < b->schedtask()->priority();
    });
}

timinginfos_t BaseOnlineSimulator::ellipsis(timinginfos_t tic, const std::list<SchedElt*> &rq) {
    vector<timinginfos_t> el;
    
    //next time a task is activated
    for(Task *t : tg->tasks_it()) {
        if(tic < t->offset()) {//When we start, 1st job
            el.push_back(t->offset() - tic);
        }
        else if(t->T() > 0) {
            timinginfos_t relative_activation = t->T()+t->offset();
            if(tic < relative_activation) {//first period, 2nd job
                el.push_back(relative_activation - tic);
            }
            else {
                timinginfos_t last_period = floor((tic-t->offset()) / (double)t->T() ) * t->T();
                if(tic > last_period && tic < last_period+t->offset()) { // tic is the period and the activation offset
                    el.push_back(last_period+t->offset() - tic);
                }
                else {// tic is after the offset
                    timinginfos_t next_period = (ceil((tic-t->offset()) / (double)t->T() ) * t->T());
                    el.push_back(next_period + t->offset() - tic);
                }
            }
        }
    }
    
    //next time a proc is available
    for(SchedCore *c : sched->schedcores()) {
        if(tic < c->clock()) {
            el.push_back(c->clock() - tic);
        }
    }
    
    //next time a mutex resource is available
    for(SchedResource *r : sched->schedresources()) {
        if(r->resource()->access != resource_access_type_e::MUTEX) continue;
        
        SchedResource::booking_t booking = r->booking();
        
        for(auto it=booking.rbegin(), et=booking.rend() ; it != et && tic < (*it).end ; ++it) {
            if(!r->is_booked((*it).start, (*it).end+1)) {
                el.push_back((*it).end - tic);
            }
        }
//
//        for(tuple<SchedElt*, timinginfos_t, timinginfos_t> booked : r->booking()) {
//            if(tic < get<2>(booked) && !r->is_booked(get<2>(booked), get<2>(booked)+1)) {
//                el.push_back(get<2>(booked) - tic);
//            }
//        }
    }
    
    //next time a task has its pred ready
    for(SchedElt *elt : rq) {
        bool all_scheduled = true;
        timinginfos_t latest_end = 0;
        for(SchedElt *prev : elt->previous()) {
//            if(!sched->is_task_fully_scheduled(prev->schedtask())) {
            // if the SchedElt is not mapped then it is not scheduled
            //   avoid scanning the scheduled element to find it as it becomes big
            if(sched->get_mapping(prev) == nullptr) { 
                all_scheduled = false;
                break;
            }
            if(latest_end < prev->rt()+prev->wct())
                latest_end = prev->rt()+prev->wct();
        }
        if(all_scheduled && tic < latest_end) {
            el.push_back(latest_end - tic);
        }
    }
    
    if(el.size()) {
        sort(el.begin(), el.end(), [](timinginfos_t a, timinginfos_t b) {return a < b; });
        Utils::DEBUG("Ellipsis to "+to_string(tic)+" + "+to_string(el[0])+" = "+to_string(tic+el[0]));
        return (el[0] == 0) ? 1 : el[0];
    }
    Utils::DEBUG("Ellipsis to "+to_string(tic)+" + "+to_string(force_sched_length - tic)+" = "+to_string(tic+(force_sched_length - tic)));
    return force_sched_length - tic;
}

timinginfos_t BaseOnlineSimulator::generate_exec_time(Task *t) {
    
    if(exectime_algo == BaseOnlineSimulator::exectime_algo_e::MARGIN) {
        return margin >= 0
                ? t->C() + t->C() * margin / 100
                : t->C() - t->C() * abs(margin) / 100;
    }
    
    if(exectime_algo == BaseOnlineSimulator::exectime_algo_e::RAND_POISSON) {
        if(!distributions.count(t)) {
            distributions[t] = new poisson_distribution<uint64_t>(t->C());
        }
        timinginfos_t wcet = (*distributions[t])(generator);
        Utils::DEBUG("Task "+t->id()+" will run for "+Utils::nstime_to_string(wcet, "ms")+"ms");
        return wcet;
    }
    
    if(exectime_algo == BaseOnlineSimulator::exectime_algo_e::WCET)
        return t->C();
    
    throw CompilerException("BaseOnlineSimulator", "Can't determine how to decide exec time");
}
/*!
 * \file Nvidia.hpp
 * \copyright GNU Public License v3.
 * \date 2020
 * \author Benjamin Rouxel <benjamin.rouxel@uva.nl>, University of Amsterdam
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MULTICORESIM_HPP
#define MULTICORESIM_HPP

#include <random>
#include <list>
#include <chrono>
#include <set>
#include <fstream>

#include "BaseOnlineSchedSim.hpp"
#include "PriorityAssignment/RegistryEntries.hpp"
#include "Partitioning/RegistryEntries.hpp"
#include "Utils/Log.hpp"
#include "Utils/Graph.hpp"

/**
 * Simulator an online scheduler
 * 
 * Because actual execution time for tasks are not known, a random value is
 * picked using a poisson distribution with a mean at 50% of the WCET.
 */
class MultiCoreOnlineSchedSim : public BaseOnlineSimulator {
protected:
    enum map_e {
        global, partitioned
    };

    std::map<SchedElt*, uint32_t> select_mapping_index;
    
    map_e mapping_shm;
    bool single_stream = true;
    bool pipelining = false;
    std::map<Task*, SchedJob*> pipelining_jobs;
public:
    //! Constructor
    MultiCoreOnlineSchedSim() : BaseOnlineSimulator() {}
    //! Destructor
    virtual ~MultiCoreOnlineSchedSim();
    
    virtual void forward_params(const std::map<std::string, std::string> &) override;
    const std::string get_uniqid_rtti() const override { return "simulator-sched-multi-core"; }
    virtual void check_dependencies() override;
    
    virtual std::string help() override;
    
protected:
    virtual void do_simulate() override;
    
    SchedCore *get_mapping(SchedElt *elt, SchedCore *current);
    
    virtual bool compute_ready_queue(timinginfos_t tic, std::list<SchedElt*> *ready_queue) override;
};

REGISTER_SIMULATOR(MultiCoreOnlineSchedSim, "multi-core")
        
class Nvidia : public MultiCoreOnlineSchedSim {
public:
    Nvidia() : MultiCoreOnlineSchedSim() {}
};
REGISTER_SIMULATOR(Nvidia, "nvidia")

#endif /* NVIDIA_HPP */


/*!
 * \file OnlineSchedSim.hpp
 * \copyright GNU Public License v3.
 * \date 2020
 * \author Benjamin Rouxel <benjamin.rouxel@uva.nl>, University of Amsterdam
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SINGLECORESIM_HPP
#define SINGLECORESIM_HPP

#include <random>
#include <list>
#include <chrono>
#include <set>

#include "BaseOnlineSchedSim.hpp"
#include "PriorityAssignment/RegistryEntries.hpp"
#include "Partitioning/RegistryEntries.hpp"
#include "Utils/Log.hpp"
#include "Properties.hpp"

/**
 * Simulator an online scheduler
 * 
 * Because actual execution time for tasks are not known, a random value is
 * picked using a poisson distribution with a mean at 50% of the WCET.
 */
class SingleCoreOnlineSchedSim : public BaseOnlineSimulator {
protected:
    //! Type of preemtion model
    enum preemption_e {
        NO, //!< no preemption
        YES, //!< yes preemption
        LIMITED //!< limited preemption, \todo
    };
    preemption_e preemption; //!< selected preemption model
    
    Partitioning *mapping = nullptr; //!< mapping algorithm
    
    size_t nb_hp = 1; //!< what number of hyperperiod should be simulated
    
    std::default_random_engine generator; //!< random number generator
    std::map<Task*, std::poisson_distribution<uint64_t>*> distributions; //!< random distribution
    
    bool force_wcet = false; //!< no random execution time, force WCET
    
    std::vector<SchedJob*> active_tasks;
public:
    //! Constructor
    SingleCoreOnlineSchedSim() : BaseOnlineSimulator() {}
    //! Destructor
    virtual ~SingleCoreOnlineSchedSim();
    
    virtual void forward_params(const std::map<std::string, std::string> &) override;
    const std::string get_uniqid_rtti() const override { return "simulator-sched-sched_online"; }
    virtual void check_dependencies() override;
    
    virtual std::string help() override;
    
    
    virtual SchedCore *get_mapping(SchedElt *elt, SchedCore *current);
protected:
    virtual void do_simulate() override;
};

REGISTER_SIMULATOR(SingleCoreOnlineSchedSim, "single-core")

#endif /* ONLINESCHEDSIM_HPP */


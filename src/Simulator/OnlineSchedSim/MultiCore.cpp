/*!
 * \file Multicore.hpp
 * \copyright GNU Public License v3.
 * \date 2020
 * \author Benjamin Rouxel <benjamin.rouxel@uva.nl>, University of Amsterdam
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "MultiCore.hpp"

using namespace std;

IMPLEMENT_HELP_WITH_CODE(MultiCoreOnlineSchedSim, {
    msg += "HTMLINDENT- priority, default EDF: priority assignment method, also determines if the priority are assigned statically or dynamically.\n";
    for(string p : PriorityAssignmentRegistry::keys())
        msg += "HTMLINDENT- "+p+"\n";
    msg +=  "HTMLINDENT- event-handler: if you want a attack/defense mechanism\n";
    for(string e : SimulatorEventHandlerRegistry::keys()) {
            msg += "HTMLINDENT- "+e+"\n";
    }
}, Simulate the behaviour of an on-line scheduler on a given task set. 
    As real execution times are not known, well we don`t execute the tasks, then
    execution times are randomly decided using a poisson distribution with 
    a mean at 50% of the respective WCET and 
    using the default CPP random generator.\n
    \n
    HTMLINDENT- preemption [true, false, limited], default false: activate/deactivate 
    the possibility to preempt a task when a more prioritary one arises. 
    Limited preemption (TODO!!) on the other hand, allow a higher level task to preempt
    a lower level one, only if by no preempting, the high level task misses its deadline.\n
    HTMLINDENT- force-wcet [bool], deafult false: if true, execution time is not random
    but forced to be the respective WCET.\n
    HTMLINDENT- hyperperiod_qty [uint32_t], default 1: number of hyperperiod to simulate\n
    HTMLINDENT- exectime [wcet, poisson, margin], default forced:\n
    HTMLINDENT- margin [int], default 0: percentage margin to apply on given WCET values, can be negativ. 
            Only effective if exectime="margin" is also provided.
)

void MultiCoreOnlineSchedSim::forward_params(const std::map<std::string,std::string>& args) {
    BaseOnlineSimulator::forward_params(args);
    
    mapping_shm = MultiCoreOnlineSchedSim::map_e::global;
    if(args.count("mapping")) {
        if(args.at("mapping") == "partitioned") {
            mapping_shm = MultiCoreOnlineSchedSim::map_e::partitioned;
        }
        else if(args.at("mapping") != "global")
            throw CompilerException("MultiCoreOnlineSim", "Unknown partitioning scheme: global/partitioned");
    }
    
    if(args.count("pipelining"))
        pipelining = args.at("pipelining") == CONFIG_TRUE;
}

void MultiCoreOnlineSchedSim::check_dependencies() {
    //All tasks must have a period -> need to have propagate properties
    // All tasks in a task graph must have the same period
    BaseOnlineSimulator::check_dependencies();
}


MultiCoreOnlineSchedSim::~MultiCoreOnlineSchedSim() {
}

void MultiCoreOnlineSchedSim::do_simulate() {
    
    //-----
    //- Need to create all the jobs to reproduce the STM file
    //- this is due to the exceptional dependencies added 
    //- a job created at the beginning of thhe schedule might be dependent on
    //- a job created later, i.e. with a higher period/offset.
//    for (Task *t : tg->tasks_it()) {
//        sched->build_job(t, t->offset());
//    }
//    for(SchedJob *j : sched->schedjobs()) {
//        for(SchedJob *k : sched->schedjobs()) {
//            if(j == k) continue;
//            if(j->task()->predecessor_tasks().count(k->task()) == 0) continue;
//            
//            j->previous().push_back(k);
//            k->successors().push_back(j);
//        }
//    }
    //-----
    
    map<SchedCore *, bool> wait_for_new_tasks;
    
    for(SchedCore *c : sched->schedcores()) {
        c->clock(0);
        wait_for_new_tasks[c] = false;
    }
    
    priority->run(tg, conf, sched);
    
    notify("start-of-hyperperiod");
    for(timinginfos_t tic = 0 ; tic < force_sched_length || (empty_ready_queue && !_ready_queue.empty()) ; ++tic) {
//        cout << tic << "\r";
        if(tic % tg->hyperperiod() == 0)
            notify("start-of-hyperperiod");
        checkTimeout();
        
        bool new_tasks = compute_ready_queue(tic, &_ready_queue);
        if(!new_tasks) {
            // check if all cores are waiting for new tasks, or are already busy
            //   then there is no possibility to schedule anything until busy cores becomes available or new tasks are added
            if(all_of(wait_for_new_tasks.begin(), wait_for_new_tasks.end(), [this, tic](pair<SchedCore*, bool> el) { 
                vector<SchedCore*>::iterator it = find_if(sched->schedcores().begin(), sched->schedcores().end(), [el](SchedCore* cl) { return cl == el.first;});
                return el.second || (*it)->clock() > tic; //this core is waiting for new task to be added, or it is already busy at the current tic
            })) {
                tic += ellipsis(tic, _ready_queue)-1;
                for(SchedCore *c : sched->schedcores()) 
                    wait_for_new_tasks[c] = false;
                continue;
            }
        }
        else {
            for(SchedCore *c : sched->schedcores())
                wait_for_new_tasks[c] = false;
            priority->update(&_ready_queue);
        }
        
        notify("ready-queue-computed");
     
        // an ellipsis occured, some cores might need to catch up
        for(SchedCore* c : sched->schedcores()) {
            if(c->clock() < tic)
                 c->clock(tic);
        }
        
        sort(sched->schedcores().begin(), sched->schedcores().end(), [](SchedCore* a, SchedCore* b) { 
            if(a->clock() == b->clock())
                    return a->core()->id < b->core()->id;
            return a->clock() < b->clock();
        });
        
        for(SchedCore* c : sched->schedcores()) {
            _current_target = c;

//            if(wait_for_new_tasks[c])
//                continue;
            
            if(c->clock() > tic)
                continue;
            
            notify("fetch-new-job");

            SchedElt *elt = fetch_job(&_ready_queue, _current_target);
            if(elt == nullptr) {
//                cout << tic << " Nothing for "<<  c->core()->id << " -- " << c->clock() << " -- " << _ready_queue.size() << endl;
                wait_for_new_tasks[_current_target] = true;
                continue;
            }

            elt->rt(_current_target->clock());
            sched->map_core(elt, _current_target);
            sched->schedule(elt);
            for(ResourceUnit *r : elt->schedtask()->task()->resource_usage()) {
                sched->schedresource(r->id)->book(elt->id(), elt->rt(), elt->rt()+elt->wct());
            }

            Utils::DEBUG(to_string(tic)+" Sched "+elt->toString()+" on "+_current_target->core()->id);

            _current_target->clock(_current_target->clock()+elt->wct());
            wait_for_new_tasks[_current_target] = false;
            
            if(tic < force_sched_length || empty_ready_queue) {
                new_tasks = commit_job(&_ready_queue, elt);
                if(new_tasks) {
                    for(SchedCore *c : sched->schedcores())
                        wait_for_new_tasks[c] = false;
                    priority->update(&_ready_queue);
                }
            }
            
//            if(elt->min_rt_deadline() < elt->rt()+elt->wct()) {
//                if(deadline_miss_throw)
//                    throw Unschedulable(elt, "Deadline miss");
//                else
//                    Utils::WARNONCE("Deadline miss "+elt->schedtask()->task()->id());
//            }
        }
    }
    
    for(SchedCore *c : sched->schedcores())
        Utils::DEBUG(c->core()->id+": "+to_string(c->clock())+" ns;");
    
    Utils::INFO("ready queue: "+to_string(_ready_queue.size()));
}

bool MultiCoreOnlineSchedSim::compute_ready_queue(timinginfos_t tic, list<SchedElt*> *rq) {
    
    bool added = BaseOnlineSimulator::compute_ready_queue(tic, rq);
    if(!added)
        return false;
    
//    vector<SchedJob*> newjobs;
    
    size_t cnt = 0;
    for(list<SchedElt*>::reverse_iterator it=rq->rbegin(), et=rq->rend() ; it != et ; ++it, ++cnt) {
        select_mapping_index[*it] = repeat[const_cast<Task *>((*it)->schedtask()->task())];
//        newjobs.push_back(*it);
    }
    
    if(pipelining) {
//        vector<SchedElt*> elts = sched->elements();
//        for(SchedJob *parentjob : newjobs) {
//            if(parentjob->task()->parent_task() != nullptr) continue;
//            if(pipelining_jobs.count(parentjob->task()) == 0) continue; 
//            
//            for(Task *ch_n : parentjob->task()->child_tasks()) {
//                auto it = find_if(newjobs.begin(), newjobs.end(), [ch_n](SchedJob *a) { return a->task() == ch_n; });
//                if(it == newjobs.end()) {
//                    cout << "Weird can't find a child job of a just added parent job in the newjobs list " << parentjob->task()->id() << " -- " << ch_n->id() << endl;
//                    continue;
//                }
//                SchedJob *ch_n_j = *it;
//                for(Task *ch_prev : parentjob->task()->child_tasks()) {
//                    SchedJob *ch_prev_job = pipelining_jobs[ch_prev];
//                    
//                    ch_prev_job->successors().push_back(ch_n_j);
//                    ch_n_j->previous().push_back(ch_prev_job);
//                    
////                    cout << "Add dep " << ch_prev_job->task()->id() << "-" << ch_prev_job->min_rt_period() << " -> " << ch_n_j->task()->id() << "-" << ch_n_j->min_rt_period() << endl;
//                }
//            }
//        }
//        for(SchedJob *job : newjobs) {
//            pipelining_jobs[job->task()] = job;
//        }
    }
    
    return added;
}

SchedCore *MultiCoreOnlineSchedSim::get_mapping(SchedElt *elt, SchedCore *current) {
    Version *v = elt->schedtask()->task()->version(elt->schedtask()->selected_version());
    if(v != nullptr) {
        if(mapping_shm == MultiCoreOnlineSchedSim::map_e::global) {
            if(v->force_mapping_proc().size() == 0 || find(v->force_mapping_proc().begin(), v->force_mapping_proc().end(), current->core()) != v->force_mapping_proc().end())
                return current;
            else
                return nullptr;
        }
        //else partitioned
        if(v->force_mapping_proc().size() == 0)
            throw CompilerException("MultiCoreOnlineSim", "No partition found for "+elt->schedtask()->task()->id());
//        if(v->force_mapping_proc().size() > 0) {
            size_t index = select_mapping_index[elt] % v->force_mapping_proc().size();
            std::vector<ComputeUnit *> procs(v->force_mapping_proc().begin(),  v->force_mapping_proc().end());
            return sched->schedcore(procs[index]);
//        }
    }
    
//    throw CompilerException("MultiCoreOnlineSim", "Unknown task mapping "+elt->schedtask()->task()->id());
    //Hyperepoch goes by here
    Utils::WARN("Unknown task mapping "+elt->schedtask()->task()->id());
    return nullptr;
}

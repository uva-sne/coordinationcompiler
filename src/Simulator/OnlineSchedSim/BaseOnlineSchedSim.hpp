/*!
 * \file OnlineSchedSim.hpp
 * \copyright GNU Public License v3.
 * \date 2020
 * \author Benjamin Rouxel <benjamin.rouxel@uva.nl>, University of Amsterdam
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef BASEONLINESIM_HPP
#define BASEONLINESIM_HPP

#include <random>
#include <list>
#include <chrono>
#include <set>

#include "Simulator/Simulator.hpp"
#include "PriorityAssignment/RegistryEntries.hpp"
#include "Partitioning/RegistryEntries.hpp"
#include "Utils/Log.hpp"
/**
 * Simulator an online scheduler
 * 
 * Because actual execution time for tasks are not known, a random value is
 * picked using a poisson distribution with a mean at 50% of the WCET.
 */
class BaseOnlineSimulator : public Simulator {
protected:
    size_t hyperperiod_qty = 1; //!< what number of hyperperiod should be simulated
    
    timinginfos_t force_sched_length = 0;
    
    std::map<const Task*, uint32_t> repeat;
    std::map<const Task*, size_t> job_counter;
    
    std::default_random_engine generator; //!< random number generator
    std::map<const Task*, std::poisson_distribution<uint64_t>*> distributions; //!< random distribution
    uint32_t seed;
    
    bool empty_ready_queue = true;
    
    enum exectime_algo_e {
        WCET, MARGIN, RAND_POISSON
    };
    exectime_algo_e exectime_algo = exectime_algo_e::WCET;
    int32_t margin;
    
    std::function<bool(SchedElt *a, SchedElt *b)> ready_queue_ordering_func;
    
    std::list<SchedElt*> _ready_queue; // needs to be passed to function instead of directly using it, for the stats
    SchedCore* _current_target = nullptr;
    
    bool deadline_miss_throw = false;
    
    PriorityAssignment *priority = nullptr; //!< priority assignment algorithm
    
    std::vector<Task*> roots;
    std::map<const Task*, std::list<SchedElt*>> active_jobs;
public:
    //! Constructor
    BaseOnlineSimulator() : Simulator() {}
    //! Destructor
    virtual ~BaseOnlineSimulator();
    
    virtual void forward_params(const std::map<std::string, std::string> &) override;
    virtual void check_dependencies() override;
    
    virtual std::string help() override;
    
    ACCESSORS_R(Simulator, SINGLE_ARG(std::list<SchedElt*>&), ready_queue)
    ACCESSORS_R(Simulator, SchedCore*, current_target)
    
    // needs to be public to allow the stats to use it
    virtual SchedElt *fetch_job(std::list<SchedElt*> *rq, SchedCore *target);
    
    virtual SchedCore *get_mapping(SchedElt *elt, SchedCore *current) = 0;
    
protected:
    virtual void simulate() override;
    /**
     * Simulate an execution on a single given core
     * @param core
     */
    virtual void do_simulate() = 0;
    
    /**
     * Generate an execution time, either random or WCET
     * @param t
     * @return 
     */
    uint64_t generate_exec_time(Task *t);
    
    virtual bool compute_ready_queue(timinginfos_t tic, std::list<SchedElt*> *elts);
    
    // Lowest value -> higher priority
    void sort_ready_queue(std::list<SchedElt*> *elts);
    
    virtual timinginfos_t ellipsis(timinginfos_t tic, const std::list<SchedElt*> &ready_queue);
    
    SchedJob* build_job(timinginfos_t tic, Task *t, SchedElt *prev=nullptr);
    bool try_release_job(std::list<SchedElt*> *elts, SchedElt *job);
    bool commit_job(std::list<SchedElt*> *elts, SchedElt *job);
};

#endif /* ONLINESCHEDSIM_HPP */


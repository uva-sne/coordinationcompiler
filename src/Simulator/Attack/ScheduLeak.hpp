/*!
 * \file OnlineSchedSim.hpp
 * \copyright GNU Public License v3.
 * \date 2020
 * \author Benjamin Rouxel <benjamin.rouxel@uva.nl>, University of Amsterdam
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SCHEDULEAK_HPP
#define SCHEDULEAK_HPP

#include <iomanip>
#include <exception>

#include "Simulator/Simulator.hpp"
#include "Solvers/Solver.hpp"
#include "Utils/Log.hpp"

class Protected : public std::exception {
    
};

/**
 * Simulate the attack on a schedule name ScheduLeak
 * 
 * \@inproceedings{chen2019novel,
 * title={A Novel Side-Channel in Real-Time Schedulers},
 * author={Chen, Chien-Ying and Mohan, Sibin and Pellizzoni, Rodolfo and Bobba, Rakesh B and Kiyavash, Negar},
 * booktitle={2019 IEEE Real-Time and Embedded Technology and Applications Symposium (RTAS)},
 * pages={90--102},
 * year={2019},
 * organization={IEEE}
 * }
 */
class ScheduLeak : public SimulatorEventHandler {
public:
    //! Constructor
    ScheduLeak() : SimulatorEventHandler() {}
    //! Destructor
    virtual ~ScheduLeak() {}
    
    virtual void event(const std::string &event, Simulator *caller) override;
    virtual void forward_params(const std::map<std::string, std::string> &) override;
    const std::string get_uniqid_rtti() const override { return "simulator-event-attack-scheduleak"; }
    virtual void check_dependencies() override {};
    
    virtual std::string help() override;
    
protected:
    //! Convenient structure to construct the ladder
    struct interval {
        uint64_t start; //!< Start time of a exec slot
        uint64_t size; //!< Size of the exec slot
    };

    std::string victim_task_id = ""; //!< Victim task id
    Task *victim_task = nullptr; //!< Victim task ref
    std::string attack_task_id = ""; //!< Attacking task id
    
    //! \brief Convenient structure to temporarily store the attacker params
    struct attack_task_param_s {
        bool inconfig;
        uint64_t period;
        uint64_t deadline;
        uint64_t wcet;
    };
    attack_task_param_s attack_task_params = {false, 0, 0, 0}; //!< Attacker config
    Task *attack_task = nullptr; //!< Attacker task ref
    bool show_ladder = false; //!< Toggle display ladder
    
    //! Add the attacking task into the task set
    void add_attacking_task();
    /**
     * Compute the success ratio according to the actual offset of the victim
     * @param offset_victim
     * @return 
     */
    float compute_success_ratio(uint64_t offset_victim);
    /**
     * Get the true victim offset
     * @return 
     */
    uint64_t get_victim_offset();
    void print_ladder();
};

REGISTER_SIM_EVENT_HANDLER(ScheduLeak, "scheduleak")

#endif /* SCHEDULEAK_HPP */


/* 
 * Copyright (C) 2020 Benjamin Rouxel <benjamin.rouxel@uva.nl> 
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "ScheduLeak.hpp"

using namespace std;

IMPLEMENT_HELP(ScheduLeak, Simulate the attack describe in the paper 
    `ScheduLeak: A Novel Scheduler Side-Channel Attack 
    Against Real-Time Autonomous Control Systems`\n
    \n
    HTMLINDENT- victim [string], required: identifier of the victim task.\n
    \n
    Sub-XMLTag:\n
    attacking-task: describe the attacking-task parameters, either the task is already
    present in the coordination specification, then just the id of the task is necessary,
    or it is not then all other timing informations are required.\n
    HTMLINDENT- id [string]: an id for the task, should not be already used by an other task\n
    HTMLINDENT- period [string]: a period given in Hz, mHz, h, m, s, ms, us, ns, cycles\n
    HTMLINDENT- deadline [string]: a deadline given in Hz, mHz, h, m, s, ms, us, ns, cycles\n
    HTMLINDENT- wcet [string]: a wcet given in h, m, s, ms, us, ns, cycles\n
    HTMLINDENT- event-handler: if you want a defense mechanism\n
    @see Simulator\n
    @see SimulatorEventHandler
)

void ScheduLeak::forward_params(const std::map<std::string,std::string>& args) {
    if(!args.count("victim") || args.at("victim").empty())
        throw CompilerException("scheduleak", "Missing victim task id");
    victim_task_id = args.at("victim");
    if(victim_task_id.find(",") != string::npos) {
        vector<string> tmp;
        boost::split(tmp, victim_task_id, boost::is_any_of(","));
        for(string &s : tmp) {
            boost::trim(s);
            if(!s.empty()) {
                victim_task_id = s;
                break;
            }
        }
    }

    if(!args.count("attacking-task-id") || args.at("attacking-task-id").empty())
        throw CompilerException("scheduleak", "Missing attacking-task-id");
    attack_task_id = args.at("attacking-task-id");
    if(args.count("attacking-task-period") && args.count("attacking-task-wcet")) {
        attack_task_params.period = Utils::stringtime_to_ns(args.at("attacking-task-period"));
        attack_task_params.deadline = (args.count("attacking-task-deadline")) ? Utils::stringtime_to_ns(args.at("attacking-task-deadline")) : attack_task_params.period;
        attack_task_params.wcet = Utils::stringtime_to_ns(args.at("attacking-task-wcet"));
        attack_task_params.inconfig = true;
    }
    else if(args.count("attacking-task-period") || args.count("attacking-task-wcet"))
        throw CompilerException("scheduleak", "Either provide all infos for the attacking task attacking-task-period/(attacking-task-deadline)/attacking-task-wcet, or nothing and it will be found in the coordination file");

    if(args.count("showladder"))
        show_ladder = args.at("showladder") == CONFIG_TRUE;
}

void ScheduLeak::add_attacking_task() {
    attack_task = tg->task(attack_task_id);
    if(attack_task == nullptr) {
        attack_task = new Task(attack_task_id);
        tg->add_task(attack_task);
    }
    if(attack_task_params.inconfig) {
        attack_task->T(attack_task_params.period);
        attack_task->D(attack_task_params.deadline);
        attack_task->versions(CONFIG_DEF_VERSION_NAME)->C(attack_task_params.wcet);
    }
    if(attack_task->T() == 0 || attack_task->D() == 0) {
        throw CompilerException(get_uniqid_rtti(), "Period or deadline for attacking task is zero, which is not possible");
    }
    Utils::DEBUG("Attack task: id="+attack_task->id()+", period="+to_string(attack_task->T())+", deadline="+to_string(attack_task->D())+", wcet="+to_string(attack_task->C()));
}

uint64_t ScheduLeak::get_victim_offset() {
    map<uint64_t, bool> candidates;
    for(size_t i=0 ; i < victim_task->T() ; ++i)
        candidates[i] = true;
    
    for(SchedElt *j : sched->schedjobs(attack_task)) {
        for(pair<uint64_t, uint64_t> ec : j->schedtask()->exec_chunks()) {
            for(size_t st=ec.first, et=st+ec.second ; st < et ; ++st) {
                size_t t = st % victim_task->T();
                candidates[t] = false;
            }
        }
    }
    
    interval largest_interval = {0,0};
    interval current_interval = {0,0};
    bool last_value = false;
    for(size_t i=0 ; i < victim_task->T() ; ++i) {
        if(candidates[i]) {
            if(!last_value)
                current_interval.start = i;
            current_interval.size++;
        }
        else if(last_value) {
            Utils::DEBUG("Candidate interval: ["+to_string(current_interval.start)+";"+to_string(current_interval.start+current_interval.size)+"[");
            if(largest_interval.size < current_interval.size)
                largest_interval = current_interval;
            current_interval = {0,0};
        }
        last_value = candidates[i];
    }
    
    if(largest_interval.size == 0)
        throw Protected();
    
    return largest_interval.start;
}

float ScheduLeak::compute_success_ratio(uint64_t offset_victim) {
    if(offset_victim == victim_task->offset())
        return 100.0f;
    
    float epsilon =  abs((int64_t)(offset_victim - victim_task->offset()));
    float half_T = victim_task->T() / 2.0f;
//    cout << "\tepsilon: " << epsilon << " -- 1/2 T: " << half_T << endl;
//    cout << "\tratiod: " << ((1 - (victim_task->T() - epsilon) / half_T) * 100.0f) << " or " << ((1 - epsilon / half_T) * 100.0f) << endl;
    if(epsilon > half_T)
        return (1 - (victim_task->T() - epsilon) / half_T) * 100.0f;
    else
        return (1 - epsilon / half_T) * 100.0f;
}

void ScheduLeak::print_ladder() {
    map<size_t, bool> all_tics;
    map<uint64_t, bool> candidates;
    
    for(size_t i=0 ; i < victim_task->T() ; ++i)
        candidates[i] = true;
    
    for(size_t h = 0 ; h < tg->hyperperiod() ; ++h) {
        all_tics[h] = true;
    }
    
    for(SchedElt *j : sched->schedjobs(attack_task)) {
        for(pair<uint64_t, uint64_t> ec : j->schedtask()->exec_chunks()) {
            for(size_t st=ec.first, et=st+ec.second ; st < et ; ++st) {
                size_t t = st;
                all_tics[t] = false;
                size_t t2 = st % victim_task->T();
                candidates[t2] = false;
            }
        }
    }
    
    cout << "Schedule ladder: " << endl;
    cout << "X: attacking task executed" << endl;
    cout << ".: no attack execution" << endl;
    int width = to_string(victim_task->T()).size()+1;
    for(size_t i=0 ; i < victim_task->T() ; ++i) {
        cout << setw(width) << i << " ";
    }
    cout << endl;
    for(size_t h = 0; h < tg->hyperperiod() ; ++h) {
        cout << setw(width) << (all_tics[h] ? "." : "X") << " ";
        if(!((h+1) % victim_task->T()))
            cout << endl;
    }
    cout << "==================" << endl;
    for(size_t i=0 ; i < victim_task->T() ; ++i) {
        cout << setw(width) << (candidates[i] ? "." : "X") << " ";
    }
    cout << endl;
    cout << "==================" << endl;
}

void ScheduLeak::event(const string &event, Simulator *caller) {
    if(event == "pre-schedule") {
        victim_task = tg->task(victim_task_id);
        if(victim_task == nullptr) 
            throw CompilerException("scheduleak", "Victim task `"+victim_task_id+"' not found");

        add_attacking_task();
    }
    else if(event == "post-schedule") {
        if(show_ladder)
            print_ladder();

        float attack_ratio;
        try {
            uint64_t offset_victim = get_victim_offset();
            Utils::DEBUG("Found victim offset = "+to_string(offset_victim)+" -- actual offset = "+to_string(victim_task->offset()));
            attack_ratio = compute_success_ratio(offset_victim);
        }
        catch(Protected &e) {
            Utils::DEBUG("Offset not found");
            attack_ratio = 0;
        }
        Utils::DEBUG("Success ratio: "+to_string(attack_ratio));

        if(attack_ratio == 100.0f)
            Utils::INFO("Attack through ScheduLeak is very lickely successful");
        else if(attack_ratio >= 1.0f) 
            Utils::INFO("Attack through ScheduLeak is probably successful with ratio "+to_string(attack_ratio)+"%");
        else
            Utils::INFO("Attack through ScheduLeak will fail");
    }
}
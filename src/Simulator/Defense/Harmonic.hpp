/*
 * Copyright (C) 2020 brouxel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * File:   Harmonic.hpp
 * Author: brouxel
 *
 * Created on 17 août 2020, 13:30
 */

#ifndef HARMONIC_HPP
#define HARMONIC_HPP

#include <string>
#include <algorithm>
#include <chrono>
#include <limits>

#include "Simulator/Simulator.hpp"
#include "Utils/Log.hpp"
#include "Utils.hpp"

//! Type to represent the set of period
typedef std::vector<timinginfos_t> period_set_t;

/**
 * Protection against the ScheduLeak attack
 *
 * The ScheduLeak attack aims at determining what is the offset of tasks within
 * a dynamic schedule. This class adds counter-measure (CM) tasks in the system to protect the
 * potential victims of this attack. The timing parameters of this counter-measure
 * tasks must be selected carfully.
 *
 * Their can be one or several potential victims depending on user decision. Taint
 * mechanism is provided with a keyword in the TeamPlay specification file. If no
 * tasks are tainted, it is possible to fill attributes in the config file to specify
 * potential victims. See #help .
 *
 * The period is selected following harmonicity. A CM harmonic period is a period
 * that divides the victim tasks, thus not changing the overall hyperperiod of the
 * system.
 *
 * The wcet is fixed to 1.
 *
 * The offset is fixed to 0
 *
 * To determine the periods for the set of CM tasks (in case there is more than
 * one victim), we form a Hasse diagram with the victims period. The uppest layer of
 * the diagram corresponds to the maximum amount of CM tasks we would add to protect
 * the schedule, assigning the corresponding period to the CM task. This means, that
 * the basic case to protect a task, is to add a CM task with the same period. Then, for each
 * node in this diagram we compute a positive/negative gain. A positive gain on
 * a lower node in the Hasse diagram means that the node can replace its upper nodes
 * without impacting the hyperperiod, and keeping the same property. This mechanism
 * allows to reduce the amount of CM tasks to add.
 *
 * To compute the gain in the Hasse diagram, we rely on the utilisation. If the
 * utilisation of the whole system is higher when replacing several CM tasks with
 * a single one, then the gain is negative.
 *
 */
class Harmonic : public SimulatorEventHandler {
public:
    enum cm_period_gen_algo {
        TRIVIAL, GREEDY, EXHAUSTIV
    };
    enum offset_select {
        PERCENTAGE, VICTIM_OFFSET, FIRST_HALF_CM_PERIOD, SECOND_HALF_CM_PERIOD, CM_PERIOD, FIXED_VALUE, FIRST_HALF_VICTIM_OFFSET, SECOND_HALF_VICTIM_OFFSET,
        FIRST_CRITERIA, SECOND_CRITERIA, MULTI_CRITERIA
    };
    enum wcet_select {
        WCET_FIXED_VALUE, UTILISATION_BASED
    };
protected:

    std::vector<timinginfos_t> cm_wcet; //!< Uniform fixed WCET for CM
    std::vector<timinginfos_t> cm_offset;
    
    short cm_offset_perc = -1;
    std::vector<std::string> victims; //!< List of victims to protect

    cm_period_gen_algo cm_T_algo = cm_period_gen_algo::TRIVIAL;
    offset_select cm_offset_algo = offset_select::FIXED_VALUE;
    wcet_select cm_wcet_algo = wcet_select::UTILISATION_BASED;
    
    bool skip = false;
public:
    //! Constructor
    Harmonic() : SimulatorEventHandler() {}
    virtual void event(const std::string &event, Simulator *caller) override;

    virtual void forward_params(const std::map<std::string, std::string> &params) override;
    const std::string get_uniqid_rtti() const override;
    virtual std::string help() override;

protected:

    /**
     * Gather all periods from the different victims to protect
     * @return
     */
    virtual period_set_t get_periods_to_protect();
    virtual std::vector<timinginfos_t> get_offsets_victims();
    /**
     * Select the periods for the CM tasks
     *
     * Compute the gain for the different node of the Hasse diagram and select the
     * periods for the CM tasks
     *
     * @param initP list of initial victim periods
     * @param diag Hasse diagram
     * @param checked_periods recursive function, this holds the periods that have already been checked
     * @return
     */
    virtual period_set_t get_cm_periods(const period_set_t &initP);
    /**
     * Add counter measure with the given period to the system
     * @param periods
     */
    virtual void add_counter_measure(const period_set_t &periods);

    std::vector<period_set_t> harmonic_to_candidates(period_set_t harmonic, period_set_t nodes, std::vector<float> gains);
    period_set_t choose_nodes(period_set_t nodes, std::vector<float> gains);
    void get_positive_nodes_and_gains(period_set_t harmonic, period_set_t vertices, period_set_t *node, std::vector<float> *gains);
    float get_gain(period_set_t harmonic, timinginfos_t divisor);
    float get_utilization(period_set_t harmonic);
    period_set_t reduce_harmonic(period_set_t harmonic, timinginfos_t divisor);
    period_set_t find_multiples_of_divisor(period_set_t harmonic, timinginfos_t divisor);
    period_set_t remove_multiples(const period_set_t &periods);
    period_set_t get_divisors_of_list(period_set_t values, period_set_t interval);
    /**
     * Compute the list of all divisors of a value
     * @param v
     * @param divisors
     */
    period_set_t get_divisors(timinginfos_t a, period_set_t interval);
    bool divide(timinginfos_t a, timinginfos_t b);
    period_set_t union_lists(period_set_t list1, period_set_t list2);
    period_set_t diff_lists(period_set_t list1, period_set_t list2);
    std::vector<period_set_t> union_lists(std::vector<period_set_t> list1, std::vector<period_set_t> list2);
    
    timinginfos_t get_offset_1stcriteria(timinginfos_t period);
    timinginfos_t get_offset_2ndcriteria(timinginfos_t period, timinginfos_t wcet);
    timinginfos_t get_offset_multicriteria(timinginfos_t period, timinginfos_t wcet);
    void get_init_offset(timinginfos_t period, std::vector<timinginfos_t> &);
    void get_task_protected(timinginfos_t period, std::vector<Task*>&);
    void get_Irm_1stcriteria(timinginfos_t period, std::vector<Task*> task_protected, std::vector<timinginfos_t>&);
    void get_task_small(std::vector<Task*> task_protected, timinginfos_t wcet, std::vector<Task*>&);
    void get_Irm_2ndcriteria(timinginfos_t period, std::vector<Task*> task_small, timinginfos_t wcet, std::vector<timinginfos_t>&);
    
    
};

REGISTER_SIM_EVENT_HANDLER(Harmonic, "defenseharmonic")

#endif /* HARMONIC_HPP */


/*
 * Copyright (C) 2020 brouxel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * File:   Harmonic.cpp
 * Author: brouxel
 *
 * Created on 17 août 2020, 13:30
 */

#include "Harmonic.hpp"

using namespace std;

const string Harmonic::get_uniqid_rtti() const {
    return "simulator-event-defense-harmonic";
}
IMPLEMENT_HELP(Harmonic,
    Implement the defense mechanism agains ScheduLeak by adding tasks
    that obfuscates the schedule. These counter-measure tasks have a
    WCET set by a parameter, periods are deducted using an harmonic
    method, and T=D.\n
    \n
    HTMLINDENT- wcet [string], default 1ns: a wcet given in h, m, s, ms, us, ns, cycles\n
    HTMLINDENT- victims [string*]: comma separated list of tasks to protect\n
    HTMLINDENT- cm_period_gen_algo [trivial/greedy/exhaustiv], default trivial: algo to use
            to generate periods for counter-measure\n
)

void Harmonic::forward_params(const std::map<std::string, std::string> &params) {
    if(params.count("wcet")) {
        if(params.at("wcet").find(":") != string::npos) {
            vector<string> wcets;
            boost::split(wcets, params.at("wcet"), boost::is_any_of(":"));
            for(string wcet : wcets) {
                timinginfos_t w = Utils::stringtime_to_ns(wcet);
                if(w < 1)
                    throw CompilerException("Harmonic defense", "WCET of counter measure can't be below 1(given "+to_string(w)+")");
                cm_wcet.push_back(w);
            }
        }
        else {
            timinginfos_t w = Utils::stringtime_to_ns(params.at("wcet"));
            if(w < 1)
                throw CompilerException("Harmonic defense", "WCET of counter measure can't be below 1(given "+to_string(w)+")");
            cm_wcet.push_back(w);
        }
        cm_wcet_algo = wcet_select::WCET_FIXED_VALUE;
    }
    
    if(params.count("offset")) {
        if(params.at("offset").find("%") != string::npos) {
            cm_offset_algo = offset_select::PERCENTAGE;
            cm_offset_perc = stoi(params.at("offset"));
        }
        else if(params.at("offset") == "from victims offset") {
            cm_offset_algo = offset_select::VICTIM_OFFSET;
        }
        else if(params.at("offset") == "1st half from victims offset") {
            cm_offset_algo = offset_select::FIRST_HALF_VICTIM_OFFSET;
        }
        else if(params.at("offset") == "2nd half from victims offset") {
            cm_offset_algo = offset_select::SECOND_HALF_VICTIM_OFFSET;
        }
        else if(params.at("offset") == "from CM periods") {
            cm_offset_algo = offset_select::CM_PERIOD;
        }
        else if(params.at("offset") == "1st half CM periods") {
            cm_offset_algo = offset_select::FIRST_HALF_CM_PERIOD;
        }
        else if(params.at("offset") == "2nd half CM periods") {
            cm_offset_algo = offset_select::SECOND_HALF_CM_PERIOD;
        }
        else if(params.at("offset") == "1st criteria") {
            cm_offset_algo = offset_select::FIRST_CRITERIA;
        }
        else if(params.at("offset") == "2nd criteria") {
            cm_offset_algo = offset_select::SECOND_CRITERIA;
        }
        else if(params.at("offset") == "3-steps criteria") {
            cm_offset_algo = offset_select::MULTI_CRITERIA;
        }
        else {
            cm_offset_algo = offset_select::FIXED_VALUE;
            
            if(params.at("offset").find(":") != string::npos) {
                vector<string> offsets;
                boost::split(offsets, params.at("offset"), boost::is_any_of(":"));
                for(string offset : offsets) {
                    timinginfos_t o = Utils::stringtime_to_ns(offset);
                    cm_offset.push_back(o);
                }
            }
            else {
                timinginfos_t o = Utils::stringtime_to_ns(params.at("offset"));
                cm_offset.push_back(o);
            }
        }
    }
    if(params.count("skip")) {
        skip = params.at("skip") == CONFIG_TRUE;
    }
    if(params.count("victims")) {
        boost::split(victims, params.at("victims"), boost::is_any_of(","));
        for(string &a : victims) boost::trim(a);
        vector<string>::iterator ne = remove_if(victims.begin(), victims.end(), [](string &a) { return a.empty(); });
        victims.resize(distance(victims.begin(), ne));
    }
    if(params.count("cm_period_gen_algo")) {
        if(params.at("cm_period_gen_algo") == "greedy")
            cm_T_algo = cm_period_gen_algo::GREEDY;
        else if(params.at("cm_period_gen_algo") == "exhaustiv")
            cm_T_algo = cm_period_gen_algo::EXHAUSTIV;
        else if(params.at("cm_period_gen_algo") != "trivial")
            throw CompilerException("Harmonic defense", "Wrong algo to generate CM periods, available: trivial/greedy/exhaustiv");
    }
}

period_set_t Harmonic::get_periods_to_protect() {
    period_set_t periods;

    if(victims.size() > 0) {
        for(Task *t : tg->tasks_it()) {
            // we want all tasks that are not victims
            if(find(victims.begin(), victims.end(), t->id()) != victims.end()) continue;
            periods.push_back(t->T());
        }
    }
    else {
        for(Task *t : tg->tasks_it()) {
            if(t->tainted() == 0)
                periods.push_back(t->T());
        }
    }
    if(periods.size() == 0) {
        Utils::WARN("No non-sensitive tasks, neither specified victims in the config file, defending all tasks");
        for(Task *t : tg->tasks_it())
            periods.push_back(t->T());
    }

    sort(periods.begin(), periods.end());
//    // remove periods that are  multiple of other, keep the lowest
//    vector<period_set_t::iterator> to_delete;
//    for(period_set_t::iterator it = periods.begin(), et=periods.end() ; it != et ; ++it) {
//        for(period_set_t::iterator jt=it+1 ; jt != et ; ++jt) {
//            float tmp = (*it > *jt) ? *it / (float)*jt : *jt / (float)*it;
//            if(tmp - (float)((int)tmp) > 0.0f)
//                to_delete.push_back(it);
//        }
//    }
//    for(period_set_t::iterator it : to_delete)
//        periods.erase(it);

    return periods;
}

vector<timinginfos_t> Harmonic::get_offsets_victims() {
    vector<timinginfos_t> offsets;

    if(victims.size() > 0) {
        for(Task *t : tg->tasks_it()) {
            // we want all tasks that are not victims
            if(find(victims.begin(), victims.end(), t->id()) != victims.end()) continue;
            offsets.push_back(t->offset());
        }
    }
    else {
        for(Task *t : tg->tasks_it()) {
            if(t->tainted() == 0)
                offsets.push_back(t->offset());
        }
    }
    if(offsets.size() == 0) {
        Utils::WARN("No non-sensitive tasks, neither specified victims in the config file, defending all tasks");
        for(Task *t : tg->tasks_it())
            offsets.push_back(t->offset());
    }

    return offsets;
}

// difference of lists
period_set_t Harmonic::diff_lists(period_set_t list1, period_set_t list2) {
    period_set_t res(list1.size()+list2.size());
    auto it = set_difference(list1.begin(), list1.end(), list2.begin(), list2.end(), res.begin());
    res.resize(distance(res.begin(), it));
    return res;
}

period_set_t Harmonic::union_lists(period_set_t list1, period_set_t list2) {
    if(list1.size() == 0)
        return list2;
    if(list2.size() == 0)
        return list1;

    period_set_t res(list1.size()+list2.size());
    auto it = set_union(list1.begin(), list1.end(), list2.begin(), list2.end(), res.begin());
    res.resize(distance(res.begin(), it));
    return res;
}
vector<period_set_t> Harmonic::union_lists(vector<period_set_t> list1, vector<period_set_t> list2) {
    if(list1.size() == 0)
        return list2;
    if(list2.size() == 0)
        return list1;

    vector<period_set_t> res(list1.size()+list2.size());
    auto it = set_union(list1.begin(), list1.end(), list2.begin(), list2.end(), res.begin());
    res.resize(distance(res.begin(), it));
    return res;
}

//# test whether integer1 divide integer2
bool Harmonic::divide(timinginfos_t a, timinginfos_t b) {
    return (b % a) == 0;
}

//# return the sorted strict divisors of the integer
//# interval is the set of possible values for a divisor
//# if set to [] or by default, all the possible values are used
//# return the sorted divisors of the integer
period_set_t Harmonic::get_divisors(timinginfos_t a, period_set_t interval) {
    period_set_t divisors;

    if(interval.size() > 0) {
        for(timinginfos_t i : interval) {
            if(divide(i, a))
                divisors.push_back(i);
        }
    }
    else {
        for(timinginfos_t i=1, e=floor(a / (float)2) ; i <= e ; ++i) {
            if(divide(i, a))
                divisors.push_back(i);
        }
    }

    divisors.push_back(a);
    return divisors;
}

//# return the sorted divisors of a list of integers
period_set_t Harmonic::get_divisors_of_list(period_set_t values, period_set_t interval) {
    period_set_t divisors;
    for(timinginfos_t p : values) {
        divisors = union_lists(divisors, get_divisors(p, interval));
        sort(divisors.begin(), divisors.end());
    }
    return divisors;
}

//# remove multiples and multiple occurrences
//# periods_input is a sorted list of strictly positive integers
period_set_t Harmonic::remove_multiples(const period_set_t &initP) {
    period_set_t periods;

    // remove periods that are  multiple of other, keep the lowest
    for(period_set_t::const_iterator it = initP.begin(), et=initP.end() ; it != et ; ++it) {
        timinginfos_t period2 = *it;
        bool isMultiple = false;
        for(period_set_t::const_iterator jt=initP.begin() ; jt != it ; ++jt) {
            timinginfos_t period1 = *jt;
            if(divide(period1, period2))
                isMultiple = true;
        }
        if(!isMultiple)
            periods.push_back(*it);
    }
    return periods;
}

//
//# return amongst a list of periods the multiples of a given divisor
period_set_t Harmonic::find_multiples_of_divisor(period_set_t harmonic, timinginfos_t divisor) {
    period_set_t multiples;
    for(timinginfos_t period : harmonic) {
        if(divide(divisor, period)) {
            multiples.push_back(period);
        }
    }
    return multiples;
}

//# return the reduction of periods by the considered divisor
period_set_t Harmonic::reduce_harmonic(period_set_t harmonic, timinginfos_t divisor) {
    period_set_t multiples = find_multiples_of_divisor(harmonic, divisor);
    period_set_t res = diff_lists(harmonic, multiples);
    res.push_back(divisor);
    return res;
}

//# returns the utilization of a period, assuming a WCET == 1

float Harmonic::get_utilization(period_set_t harmonic) {
    float utilization = 0.0f;
    for(timinginfos_t period : harmonic)
        utilization = utilization + 1.0 / float(period);
    return utilization;
}

//# return the gain to replace in a harmonic the multiples by the divisor
float Harmonic::get_gain(period_set_t harmonic, timinginfos_t divisor) {
    period_set_t multiples = find_multiples_of_divisor(harmonic, divisor);
    period_set_t d;
    d.push_back(divisor);
    float gain = get_utilization(multiples) - get_utilization(d);
    return gain;
}

//# return the positive nodes and their corresponding gains
void Harmonic::get_positive_nodes_and_gains(period_set_t harmonic, period_set_t vertices, period_set_t *nodes, vector<float> *gains) {
    for(timinginfos_t node : vertices) {
        float gain = get_gain(harmonic, node);
        if(gain > 0.0) {
            nodes->push_back(node);
            gains->push_back(gain);
        }
    }
}

//# choice function for the positive nodes in the recursive call
//# assumes that gain[i] is the gain of nodes[i]
period_set_t Harmonic::choose_nodes(period_set_t nodes, vector<float> gains) {
    period_set_t res;
    if(nodes.size() == 0 || cm_T_algo == cm_period_gen_algo::TRIVIAL) {
        return res;
    }
    else if(cm_T_algo == cm_period_gen_algo::GREEDY) {
        float max_gain = 0.0;
        size_t index = -1;
        for(size_t i=0 ; i < gains.size() ; ++i) {
            if(gains[i] >  max_gain) {
                max_gain = gains[i];
                index = i;
            }
        }
        res.push_back(nodes[index]);
        return res;
    }
    else {
        return nodes;
    }
}

//# recursive call on the chosen positive nodes
//# if none then return the initial harmonic as candidate
//# next positive nodes are subset of previous positive nodes
vector<period_set_t> Harmonic::harmonic_to_candidates(period_set_t harmonic, period_set_t nodes, vector<float> gains) {
    period_set_t nodes_chosen = choose_nodes(nodes, gains);
    vector<period_set_t> candidates;
    if(nodes_chosen.size() > 0) {
        for(timinginfos_t divisor : nodes_chosen) {
            period_set_t harmonic_new = reduce_harmonic(harmonic, divisor);
            period_set_t divisors_new = get_divisors_of_list(harmonic_new, nodes);
            period_set_t nodes_new;
            vector<float> gains_new;
            // optimization: new nodes are computed amongst old ones and not all possible divisors
            get_positive_nodes_and_gains(harmonic_new, divisors_new, &nodes_new, &gains_new);
            vector<period_set_t> candidates_new = harmonic_to_candidates(harmonic_new, nodes_new, gains_new);
            candidates = union_lists(candidates, candidates_new);
        }
    }
    else { //nodes_chosen == [], e.g. trivial harmonic
        candidates.push_back(harmonic);
    }
    return candidates;
}

// return for a given list of periods to protect the periods of the considered countermeasure tasks
// periods_input is a list of strictly positive integers
period_set_t Harmonic::get_cm_periods(const period_set_t &initP) {
    period_set_t periods_cm;
    period_set_t nodes;
    vector<float> gains;

    period_set_t harmonic_trivial = remove_multiples(initP);
    period_set_t divisors = get_divisors_of_list(harmonic_trivial, nodes);

    get_positive_nodes_and_gains(harmonic_trivial, divisors, &nodes, &gains);
    vector<period_set_t> candidates = harmonic_to_candidates(harmonic_trivial, nodes, gains);
    if(candidates.size() >= 2) { // minimal harmonic amongst candidates

        vector<float> utilizations;
        for(period_set_t candidate : candidates) {
            utilizations.push_back(get_utilization(candidate));
        }
        float min_util = std::numeric_limits<float>::max();
        size_t index = -1;
        for(size_t i=0 ; i < utilizations.size() ; ++i) {
            if(utilizations[i] <  min_util) {
                min_util = utilizations[i];
                index = i;
            }
        }
        periods_cm = candidates[index];
    }
    else { // only one candidate
        periods_cm = candidates[0];
    }
    sort(periods_cm.begin(), periods_cm.end());
    return periods_cm;
}

void Harmonic::get_init_offset(timinginfos_t period, vector<timinginfos_t> & offsets) {
    for(timinginfos_t i=0 ; i < period-1 ; ++i) {
        offsets.push_back(i);
    }
}

void Harmonic::get_task_protected(timinginfos_t period, vector<Task*> & task_protected) {
    vector<Task*> task_sensitive;
    
    if(victims.size() > 0) {
        for(Task *t : tg->tasks_it()) {
            // we want all tasks that are not victims
            if(find(victims.begin(), victims.end(), t->id()) != victims.end()) continue;
            task_sensitive.push_back(t);
        }
    }
    else {
        for(Task *t : tg->tasks_it()) {
            if(t->tainted() == 0)
                task_sensitive.push_back(t);
        }
    }
    if(task_sensitive.size() == 0) {
        for(Task *t : tg->tasks_it())
            task_sensitive.push_back(t);
    }
    
    for(Task *t : task_sensitive) {
        if(!(t->T() % period))
            task_protected.push_back(t);
    }
}

void Harmonic::get_Irm_1stcriteria(timinginfos_t period, vector<Task*> task_protected, vector<timinginfos_t> & Irm) {
    for(Task *t : task_protected) {
        for(timinginfos_t i = t->offset() ; i <= t->offset() + t->C() ; ++i) {
            Irm.push_back(i % period);
        }
    }
    sort(Irm.begin(), Irm.end());
    vector<timinginfos_t>::iterator it = unique(Irm.begin(), Irm.end());
    Irm.resize(distance(Irm.begin(), it));
}

void Harmonic::get_task_small(vector<Task*> task_protected, timinginfos_t wcet, vector<Task*> & small) {
    for(Task *t : task_protected) {
        if(wcet > t->C())
            small.push_back(t);
    }
}

void Harmonic::get_Irm_2ndcriteria(timinginfos_t period, vector<Task*> task_small, timinginfos_t wcet, vector<timinginfos_t> & Irm) {
    for(Task *t : task_small) {
        if(wcet < t->offset()) {
            for(timinginfos_t i = t->offset() - wcet ; i <= t->offset() - 1 ; ++i) {
                Irm.push_back(i % period);
            }
        }
    }
    sort(Irm.begin(), Irm.end());
    vector<timinginfos_t>::iterator it = unique(Irm.begin(), Irm.end());
    Irm.resize(distance(Irm.begin(), it));
}

timinginfos_t Harmonic::get_offset_1stcriteria(timinginfos_t period) {
    vector<timinginfos_t> I, Irm;
    vector<Task*> task_protected;
    get_init_offset(period, I);
    get_task_protected(period, task_protected);
    
    get_Irm_1stcriteria(period, task_protected, Irm);
    vector<timinginfos_t> candidates(I.size());
    vector<timinginfos_t>::iterator it = set_difference(I.begin(), I.end(), Irm.begin(), Irm.end(), candidates.begin());
    candidates.resize(it-candidates.begin());
    if(candidates.size() == 0) {
        if(skip) {
            Utils::WARN("Offset CM, Can't protect the taskset, no offset available -- skipping");
            size_t i = rand() % I.size();
            return I[i];
        }
        throw CompilerException("harmonic", "Can't protect the taskset, no offset available");
    }
    
    size_t i = rand() % candidates.size();
    return candidates[i];
}
timinginfos_t Harmonic::get_offset_2ndcriteria(timinginfos_t period, timinginfos_t wcet) {
    vector<timinginfos_t> I, Irm;
    vector<Task*> task_protected, task_small;
    get_init_offset(period, I);
    get_task_protected(period, task_protected);
    get_task_small(task_protected, wcet, task_small);
    
    get_Irm_2ndcriteria(period, task_small, wcet, Irm);
    vector<timinginfos_t> candidates(I.size());
    vector<timinginfos_t>::iterator it = set_difference(I.begin(), I.end(), Irm.begin(), Irm.end(), candidates.begin());
    candidates.resize(it-candidates.begin());
    if(candidates.size() == 0) {
        if(skip) {
            Utils::WARN("Offset CM, Can't protect the taskset, no offset available -- skipping");
            size_t i = rand() % I.size();
            return I[i];
        }
        throw CompilerException("harmonic", "Can't protect the taskset, no offset available");
    }
    
    size_t i = rand() % candidates.size();
    return candidates[i];
}
timinginfos_t Harmonic::get_offset_multicriteria(timinginfos_t period, timinginfos_t wcet) {
    vector<timinginfos_t> I, Irm;
    vector<Task*> task_protected, task_small;
    get_init_offset(period, I);
    get_task_protected(period, task_protected);
    get_task_small(task_protected, wcet, task_small);
    
    get_Irm_1stcriteria(period, task_protected, Irm);
    vector<timinginfos_t> candidates(I.size());
    vector<timinginfos_t>::iterator it = set_difference(I.begin(), I.end(), Irm.begin(), Irm.end(), candidates.begin());
    candidates.resize(it-candidates.begin());
    if(candidates.size() == 0) {
        Utils::WARN("Offset HCM, First criteria is empty, skipping it");
        candidates = I;
    }
    get_Irm_2ndcriteria(period, task_small, wcet, Irm);
    vector<timinginfos_t> candidates2(candidates.size());
    vector<timinginfos_t>::iterator it2 = set_difference(candidates.begin(), candidates.end(), Irm.begin(), Irm.end(), candidates2.begin());
    candidates2.resize(it2-candidates2.begin());
    if(candidates2.size() == 0) {
        if(skip) {
            Utils::WARN("Offset CM, Can't protect the taskset, no offset available -- skipping");
            size_t i = rand() % I.size();
            return I[i];
        }
        throw CompilerException("harmonic", "Can't protect the taskset, no offset available");
    }
    
    size_t i = rand() % candidates2.size();
    return candidates2[i];
}

void Harmonic::add_counter_measure(const period_set_t &periods) {
    //wcet_cm = partie_entiere( 0.5*(1 - U(base)) / U(harmonique) )
    float U_Base = 0.0;
//    timinginfos_t smallest_nontainted_period = -1;
    for(Task *t : tg->tasks_it()) {
        U_Base += t->C() / (float)t->T();
//        if(t->tainted() == 0 && t->T() < smallest_nontainted_period)
//            smallest_nontainted_period = t->T();
    }

    if(cm_wcet_algo == wcet_select::UTILISATION_BASED) {
        float U_CM = 0;
        for(timinginfos_t period : periods) {
            U_CM += 1 / (float)period;
        }
        
        float tmp = floor( 0.5 * (0.99 - U_Base) / U_CM );
        if(tmp < 1) {
            Utils::WARN("We are very much close to the max utilisation, most likely adding CM will make the app unschedulable");
            tmp = 1;
        }
        if(tmp < 1)
            throw Unschedulable("Adding CM with WCET of 0 is a signe of unschedulability");

//        if(tmp >= smallest_nontainted_period)
//            throw CompilerException("harmonic defense", "WCET CM "+to_string(tmp)+" >= smallest non-tainted period "+to_string(smallest_nontainted_period));
        cm_wcet.push_back(tmp);
    }
    
    if(cm_wcet.empty())
        cm_wcet.push_back(1);
    if(cm_offset.empty())
        cm_offset.push_back(0);

    vector<timinginfos_t> possible_offset_bound = get_offsets_victims();
    sort(possible_offset_bound.begin(), possible_offset_bound.end());

    size_t index = 0;
    size_t wcet_index = 0;
    size_t offset_index = 0;
    for(timinginfos_t period : periods) {
        Task *cm = new Task("HCM"+Utils::number_to_string(period));
        tg->add_task(cm);

        timinginfos_t wcet = cm_wcet.at(wcet_index);
        cm->C(wcet);
        cm->T(period);
        cm->D(period);
        
        wcet_index = (wcet_index+1) % cm_wcet.size();

        timinginfos_t offset;
        if(cm_offset_algo == offset_select::FIXED_VALUE) {
            offset = cm_offset.at(offset_index);
            offset_index = (offset_index+1) % cm_offset.size();
        }
        else if(cm_offset_algo == offset_select::PERCENTAGE)
            offset = floor(period * cm_offset_perc / (float)100);
        else  if(cm_offset_algo == offset_select::CM_PERIOD) {
            offset = rand() % (period-1);
        }
        else if(cm_offset_algo == offset_select::FIRST_HALF_CM_PERIOD) {
            offset = rand() % (period/2);
        }
        else if(cm_offset_algo == offset_select::SECOND_HALF_CM_PERIOD) {
            offset = (period/2) + rand() % (period/2);
        }
        
        else if(cm_offset_algo == offset_select::FIRST_CRITERIA) {
            offset = get_offset_1stcriteria(period);
        }
        else if(cm_offset_algo == offset_select::SECOND_CRITERIA) {
            offset = get_offset_2ndcriteria(period, wcet);
        }
        else if(cm_offset_algo == offset_select::MULTI_CRITERIA) {
            offset = get_offset_multicriteria(period, wcet);
        }
        
        else if(cm_offset_algo == offset_select::VICTIM_OFFSET) {
            offset = rand() % (possible_offset_bound[index]+1);
        }
        else if(cm_offset_algo == offset_select::FIRST_HALF_VICTIM_OFFSET) {
            offset = rand() % ((possible_offset_bound[index]+1)/2);
        }
        else if(cm_offset_algo == offset_select::SECOND_HALF_VICTIM_OFFSET) {
            offset = ((possible_offset_bound[index]+1)/2) + rand() % ((possible_offset_bound[index]+1)/2);
        }
        
        
        
        cm->offset(offset);
        Utils::DEBUG("Added harmonic counter measure "+cm->id()+", with period "+to_string(period)+", WCET "+to_string(wcet)+", and offset "+to_string(offset));
        ++index;
    }
}

void Harmonic::event(const string &event, Simulator *caller) {
    if(event == "pre-schedule") {
        period_set_t periods = get_periods_to_protect();
        cout << "Periods to defend: " << endl;
        for(timinginfos_t p : periods)
            cout << p << " ";
        cout << endl;

        chrono::high_resolution_clock::time_point start = chrono::high_resolution_clock::now();
        period_set_t cm_periods = get_cm_periods(periods);
        chrono::high_resolution_clock::time_point stop = chrono::high_resolution_clock::now();
        chrono::duration<double,nano> diff = stop - start;
        Utils::DEBUG("CM periods computed in "+to_string(diff.count())+" ns");

        cout << "Final CM periods: " << endl;
        for(timinginfos_t p : cm_periods)
            cout << p << " ";
        cout << endl;

        add_counter_measure(cm_periods);
    }
}



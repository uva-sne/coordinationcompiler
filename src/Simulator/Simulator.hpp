/*!
 * \file Simulator.hpp
 * \copyright GNU Public License v3.
 * \date 2019 
 * \author Benjamin Rouxel <benjamin.rouxel@uva.nl>, University of Amsterdam
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SIMULATOR_HPP
#define SIMULATOR_HPP

#include <algorithm>
#include <string>
#include <vector>
#include <chrono>

#include "config.hpp"
#include "registry.hpp"
#include "CompilerPass.hpp"
#include "SystemModel.hpp"
#include "Schedule.hpp"
#include "Utils.hpp"


class Simulator;
/**
 * Base class to handle simulator events
 * 
 * Some simulators can fire event at certain point of the simulation. They need
 * to be catch and propagated to handlers.
 */
class SimulatorEventHandler : public CompilerPass {
protected:
    //! IR of the current application
    SystemModel *tg = nullptr;
    //! the global configuration
    config_t *conf = nullptr;
    //! Current state of the schedule
    Schedule *sched = nullptr;
    
public:
    SimulatorEventHandler() : CompilerPass() {}
    
    virtual void forward_params(const std::map<std::string, std::string> &params) override {}
    
    /**
     * Initialise the event handler
     * @param m
     * @param c
     * @param s
     */
    void init(SystemModel *m, config_t *c, Schedule *s) {
        tg = m;
        conf = c;
        sched = s;
    }
    
    /**
     * Handle an event
     * @param event
     */
    virtual void event(const std::string &event, Simulator *caller) = 0;
    
private:
    //! \remark This compiler pass can't be run like the other, but receives event from a simulator
    void run(SystemModel *, config_t *, Schedule *) override {};
};

/*!
 * \typedef SimulatorEventHandlerRegistry
 * \brief Registry containing all Simulator Event Handler
 */
using SimulatorEventHandlerRegistry = registry::Registry<SimulatorEventHandler, std::string>;

/*!
 * \brief Helper macro to register a new derived event handler
 */
#define REGISTER_SIM_EVENT_HANDLER(ClassName, Identifier) \
  REGISTER_SUBCLASS(SimulatorEventHandler, ClassName, std::string, Identifier)

/**
 * Base class for simulation
 */
class Simulator : public CompilerPass {
protected:
    //! IR of the current application
    SystemModel *tg;
    //! the global configuration
    config_t *conf;
    //! Current state of the schedule
    Schedule *sched;
    
    //! Event handler
    std::vector<SimulatorEventHandler *> _event_handlers;
    
    std::chrono::high_resolution_clock::time_point start_time;
    uint64_t timeout = 0;
    
public:
    virtual std::string more_help(const std::string &key) {
        if(SimulatorEventHandlerRegistry::IsRegistered(key))
            return SimulatorEventHandlerRegistry::Create(key)->help();
        return "Unknown ILP scheduling policy strategy `"+key+"'\n";
    }
    
    void run(SystemModel *m, config_t *c, Schedule *s) override {
        tg = m;
        conf = c;
        sched = s;
        for(SimulatorEventHandler *eh : _event_handlers)
            eh->init(m, c, s);
        
        check_dependencies();
        start_time = std::chrono::high_resolution_clock::now();
        simulate();
        std::chrono::duration<double,std::nano> running_time = std::chrono::high_resolution_clock::now() - start_time;
        sched->solvetime(running_time.count());
    }
    /**
     * Allow to have sub-tags in the XML configuration that will create sub-passes
     * This method will send them the proper parameters from the configuration
     * 
     * @param tag XML tag
     * @param params extracted from the configuration
     */
    virtual void setAuxilliaryParams(const std::string &tag, const std::map<std::string, std::string> &params) {
        if(tag == "event-handler") {
            if(!params.count("id"))
                throw CompilerException("scheduleak", "Missing defense id");
            SimulatorEventHandler *eh = SimulatorEventHandlerRegistry::Create(params.at("id"));
            eh->forward_params(params);
            _event_handlers.push_back(eh);
        }
        else
            throw CompilerException("xml parser", "Unknown subtag "+tag+" for simulator");
    }
    
    void notify(const std::string &e) {
        for(SimulatorEventHandler *eh : _event_handlers)
            eh->event(e, this);
    }
protected:
    /**
     * Launch a simulation
     */
    virtual void simulate() = 0;
    
    void checkTimeout() {
        std::chrono::duration<double,std::nano> running_time = std::chrono::high_resolution_clock::now() - start_time;
        if(timeout > 0 && timeout <=  running_time.count()) {
            sched->status(ScheduleProperties::sched_statue_e::SCHED_PARTIAL); // Time-out already reached
            throw Timeout();
        }
    }
};

/*!
 * \typedef SimulatorRegistry
 * \brief Registry containing all simulations
 */
using SimulatorRegistry = registry::Registry<Simulator, std::string>;

/*!
 * \brief Helper macro to register a new derived simulation
 */
#define REGISTER_SIMULATOR(ClassName, Identifier) \
  REGISTER_SUBCLASS(Simulator, ClassName, std::string, Identifier)

#endif /* SIMULATOR_HPP */


/*
 * Copyright (C) 2020 brouxel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* 
 * File:   Helper_macros.hpp
 * Author: brouxel
 *
 * Created on 2 novembre 2020, 15:07
 */

#ifndef HELPER_MACROS_HPP
#define HELPER_MACROS_HPP

#include <boost/algorithm/string.hpp>

#define HTMLINDENT &nbsp;&nbsp;
#define HTMLLF <br/>

#define STRINGIFY(X) #X

/*!
 * \brief Help message for the compiler pass
 * 
 * Generate the implementation of the help method, and generate the doxygen
 * text containing the same message.
 * 
 * \param CLASS class name in which to implement the function
 * \param ... text to show when calling help, and in doxygen file. Do not 
 *   surround the text with quotes this will preserve new lines and indentation.
 */
#define IMPLEMENT_HELP(CLASS, ...) \
    /*! \brief show help message */\
    /*! __VA_ARGS__ */\
    /*! \return the above help message*/\
    std::string CLASS::help() { \
        std::string msg = (std::string)STRINGIFY((__VA_ARGS__)); \
        msg = msg.substr(1,msg.size()-2);/*Remove trailing ()*/\
        msg = boost::algorithm::replace_all_copy(msg, (std::string)STRINGIFY(HTMLINDENT) , (std::string)"\t"); \
        msg = boost::algorithm::replace_all_copy(msg, (std::string)STRINGIFY(HTMLLF) , (std::string)"\n"); \
        return msg; \
    }

/*!
 * \brief Help message for the compiler pass
 * 
 * Generate the implementation of the help method, and generate the doxygen
 * text containing the same message. 
 * 
 * This version allows to modify the help message before returning it, access 
 * through variable `msg'.
 * 
 * \param CLASS class name in which to implement the function
 * \param CODE additional code to add before return the help message
 * \param ... text to show when calling help, and in doxygen file. Do not 
 *   surround the text with quotes this will preserve new lines and indentation.
 */
#define IMPLEMENT_HELP_WITH_CODE(CLASS, CODE, ...) \
    /*! \brief show help message */\
    /*! __VA_ARGS__ */\
    /*! \return the above help message*/\
    std::string CLASS::help() { \
        std::string msg = (std::string)STRINGIFY((__VA_ARGS__)); \
        msg = msg.substr(1,msg.size()-2);/*Remove trailing ()*/\
        CODE\
        msg = boost::algorithm::replace_all_copy(msg, (std::string)STRINGIFY(HTMLINDENT) , (std::string)"\t"); \
        return msg; \
    }

#define SINGLE_ARG(...) __VA_ARGS__

#define ACCESSORS_R(CLASS, TYPE, ATTR) \
    ACCESSORS_R_CONSTONLY(CLASS, SINGLE_ARG(TYPE), ATTR) \
    /*! @brief Access @copybrief CLASS ## ::_ ## ATTR */\
    /*! @return @copybrief CLASS ## ::_ ## ATTR */\
    inline TYPE ATTR() { return _ ## ATTR; }

#define ACCESSORS_R_CONSTONLY(CLASS, TYPE, ATTR) \
    /*! @brief Access @copybrief CLASS ## ::_ ## ATTR */\
    /*! @return @copybrief CLASS ## ::_ ## ATTR */\
    inline const TYPE ATTR() const { return _ ## ATTR; }
                                                 \
#define ACCESSORS_R_SMARTPTR_CONSTONLY(CLASS, TYPE, ATTR) \
    /*! @brief Access @copybrief CLASS ## ::_ ## ATTR */\
    /*! @return @copybrief CLASS ## ::_ ## ATTR */\
    inline const TYPE *ATTR() const { return _ ## ATTR.get(); }
                                                 \
#define ACCESSORS_R_SMARTPTR(CLASS, TYPE, ATTR) \
    ACCESSORS_R_SMARTPTR_CONSTONLY(CLASS, SINGLE_ARG(TYPE), ATTR) \
    /*! @brief Access @copybrief CLASS ## ::_ ## ATTR */\
    /*! @return @copybrief CLASS ## ::_ ## ATTR */\
    inline TYPE *ATTR() { return _ ## ATTR.get(); }

#define ACCESSORS_RW_SMARTPTR(CLASS, TYPE, ATTR) \
    ACCESSORS_R_SMARTPTR(CLASS, SINGLE_ARG(TYPE), ATTR) \
    /*! @brief Access @copybrief CLASS ## ::_ ## ATTR */\
    /*! @return @copybrief CLASS ## ::_ ## ATTR */\
    inline void ATTR(std::unique_ptr<TYPE> var) { _ ## ATTR = std::move(var); }

#define ACCESSORS_R_CONSTONLY(CLASS, TYPE, ATTR) \
    /*! @brief Access @copybrief CLASS ## ::_ ## ATTR */\
    /*! @return @copybrief CLASS ## ::_ ## ATTR */\
    inline const TYPE ATTR() const { return _ ## ATTR; } \

#define ACCESSORS_W(CLASS, TYPE, ATTR) \
    /*! @brief Access @copybrief CLASS ## ::_ ## ATTR */\
    /*! @param var @copybrief CLASS ## ::_ ## ATTR */\
    inline void ATTR(TYPE var) { _ ## ATTR = var; }

#define ACCESSORS_RW(CLASS, TYPE, ATTR) \
    ACCESSORS_R(CLASS, TYPE, ATTR) \
    ACCESSORS_W(CLASS, TYPE, ATTR)

#define ACCESSORS_MAPVALUES_R(CLASS, RTYPE, ATTR) \
    inline RTYPE ATTR() { return Utils::mapvalues(_ ## ATTR); } \
    inline RTYPE ATTR() const { return Utils::mapvalues(_ ## ATTR); }

#define ACCESSORS_MAPGETVAL_R(CLASS, KTYPE, RTYPE, ATTR) \
    inline RTYPE ATTR(KTYPE key) { return (_ ## ATTR .count(key)) ? _ ## ATTR .at(key) : nullptr; }\
    inline RTYPE ATTR(KTYPE key) const { return (_ ## ATTR .count(key)) ? _ ## ATTR .at(key) : nullptr; }


#define FORWARD_ACCESSORS_R_CONSTONLY(CLASS, TYPE, ATTR, FWFIELD) \
    /*! @brief Access @copybrief CLASS ## ::_ ## ATTR */\
    /*! @return @copybrief CLASS ## ::_ ## ATTR */\
    inline const TYPE ATTR() const { \
    if(_ ## FWFIELD.size() == 0) \
        throw CompilerException("FORWARD_ACCESSORS_R_CONSTONLY", "This " STRINGIFY(CLASS) "has no " STRINGIFY(FWFIELD) ", that's a big problem"); \
    if(_ ## FWFIELD.size() > 1) \
        Utils::WARNONCE("The current compiler pass hasn't been adapted for multi-FWFIELD, returning value for the 1st found FWFIELD."); \
    return (*(_ ## FWFIELD.begin())).second->ATTR(); \
}

#define FORWARD_ACCESSORS_R(CLASS, TYPE, ATTR, FWFIELD) \
    FORWARD_ACCESSORS_R_CONSTONLY(CLASS, SINGLE_ARG(TYPE), ATTR, FWFIELD) \
    /*! @brief Access @copybrief CLASS ## ::_ ## ATTR */\
    /*! @return @copybrief CLASS ## ::_ ## ATTR */\
    inline TYPE ATTR() { \
        if(_ ## FWFIELD.size() == 0) \
            throw CompilerException("FORWARD_ACCESSORS_R", "This " STRINGIFY(CLASS) "has no " STRINGIFY(FWFIELD) ", that's a big problem"); \
        if(_ ## FWFIELD.size() > 1) \
            Utils::WARNONCE("The current compiler pass hasn't been adapted for multi-FWFIELD, returning value for the 1st found FWFIELD."); \
        return (*(_ ## FWFIELD.begin())).second->ATTR(); \
    }

#define FORWARD_ACCESSORS_W(CLASS, TYPE, ATTR, FWFIELD) \
    /*! @brief Access @copybrief CLASS ## ::_ ## ATTR */\
    /*! @param var @copybrief CLASS ## ::_ ## ATTR */\
    void ATTR(TYPE val) { \
        for(auto e : FWFIELD()) \
            e->ATTR(val); \
    }

#define FORWARD_ACCESSORS_RW(CLASS, TYPE, ATTR, FWFIELD) \
    FORWARD_ACCESSORS_R(CLASS, TYPE, ATTR, FWFIELD) \
    FORWARD_ACCESSORS_W(CLASS, TYPE, ATTR, FWFIELD)

#define DECL_OPERATOR_OVERLOAD(RETTYPE, OP, TYPE, ...) \
    /*! __VA_ARGS__ */ \
    /*! @param a  */ \
    /*! @param b  */ \
    /*! @return  */ \
    RETTYPE operator OP(const TYPE *a, const TYPE &b); \
    /*! __VA_ARGS__ */ \
    /*! @param a  */ \
    /*! @param b  */ \
    /*! @return  */ \
    RETTYPE operator OP(const TYPE &a, const TYPE *b); \
    /*! __VA_ARGS__ */ \
    /*! @param a  */ \
    /*! @param b  */ \
    /*! @return  */ \
    RETTYPE operator OP(const TYPE &a, const TYPE &b);

#define DECL_OPERATOR_OVERLOAD2(RETTYPE, OP, TYPE1, TYPE2, ...) \
    /*! __VA_ARGS__ */ \
    /*! @param a  */ \
    /*! @param b  */ \
    /*! @return  */ \
    RETTYPE operator OP(const TYPE1 *a, const TYPE2 &b);\
    /*! __VA_ARGS__ */ \
    /*! @param a  */ \
    /*! @param b  */ \
    /*! @return  */ \
    RETTYPE operator OP(const TYPE1 &a, const TYPE2 *b);\
    /*! __VA_ARGS__ */ \
    /*! @param a  */ \
    /*! @param b  */ \
    /*! @return  */ \
    RETTYPE operator OP(const TYPE1 &a, const TYPE2 &b);

/**
 * Helper to declare a datatype in the configuration
 */
#define DECL_CONFIG_DATATYPE(S, I, C) {\
    .size_bits = S,\
    .initval = I,\
    .ctype = C\
}

/**
 * Helper to add a new datatype in the configuration
 */
#define ADD_CONFIG_DATATYPES(CONF, ID, S, I, C) CONF->datatypes[ID] = DECL_CONFIG_DATATYPE(S, I, C);

/**
 */

#ifdef ANTLR_ANY_API
#   define ANTLR_CAST_API(EXPR,TYPE) EXPR.as<TYPE>()
#   define ANTLR_ANY_TYPE antlrcpp::Any
#else
#   define ANTLR_CAST_API(EXPR,TYPE) std::any_cast<TYPE>(EXPR)
#   define ANTLR_ANY_TYPE std::any
#endif

#endif /* HELPER_MACROS_HPP */


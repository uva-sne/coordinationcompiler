/* 
 * Copyright (C) 2019 Benjamin Rouxel <benjamin.rouxel@uva.nl> 
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SCHEDULE_H
#define SCHEDULE_H


#include <string>
#include <sstream>
#include <map>
#include <utility>
#include <vector>

#include "config.hpp"
#include "SystemModel.hpp"
#include "Utils.hpp"
#include "Utils/Log.hpp"
#include "Properties.hpp"

#include <boost/range/adaptors.hpp>
#include <boost/range/any_range.hpp>

class SchedElt;
class SchedJob;
class SchedPacket;
class Schedule;
class SchedCore;
class SchedSpmRegion;
class Schedule;
class ScheduleFactory;

typedef std::unique_ptr<SchedElt>       SchedEltPtr;
typedef std::unique_ptr<SchedJob>       SchedJobPtr;
typedef std::unique_ptr<SchedPacket>    SchedPacketPtr;

typedef boost::any_range<SchedElt*, boost::bidirectional_traversal_tag, SchedElt*>          IterSchedEltPtr;
typedef boost::any_range<SchedJob*, boost::bidirectional_traversal_tag, SchedJob*>          IterSchedJobPtr;
typedef boost::any_range<SchedPacket*, boost::bidirectional_traversal_tag, SchedPacket*>    IterSchedPacketPtr;

/**
 * Interface to visit a schedule
 */
struct SchedEltVisitor {
    /**
     * Visit a SchedJob element
     * @param sched
     * @param t
     */
    virtual void visitSchedEltTask(Schedule* sched, SchedJob *t) = 0;
    /**
     * Visit a read packet
     * @param sched
     * @param p
     */
    virtual void visitSchedEltPacketRead(Schedule* sched, SchedPacket *p) = 0;
    /**
     * Visit a write packet
     * @param sched
     * @param p
     */
    virtual void visitSchedEltPacketWrite(Schedule* sched, SchedPacket *p) = 0;
};

/**
 * Base class for all scheduled element
 */
class SchedElt {
public:
    //! Type of the element
    enum Type {
        PACKET, //!< it is a packet
        TASK //!< it is a task
    };

    /*!
     * Return the char representation of the SchedElt type
     * @param e
     * @return
     */
    static char type_to_char(Type e) {
        switch(e) {
            case PACKET: return 'p';
            case TASK: return 't';
            default: return 'u';
        }
    }

protected:
    std::string _id; // TODO: this more than doubles the memory footprint of the IR -> do not create an "id" string, instead create a hash code or whatever
    const SchedElt::Type _type; //!< Type of the element
    const Task *_task = nullptr; //!< the related Task

    std::vector<SchedElt*> _successors; //!< all successing elements
    std::vector<SchedElt*> _previous; //!< all predecessing elements
    std::vector<SchedElt*> _siblings; //!< all sibling elements, for edge.packet[0], corresponds to all other edge.packet[0]

    energycons_t _wce = 0; //!< worst case energy
    datamemsize_t _mem_data = 0; //!< memory footprint
    timinginfos_t _min_rt_period = 0; //!< release time minimal for the period
    timinginfos_t _min_rt_deadline = 0; //!< relative deadline for the current period

    bool _interference_applied=false;
    bool _is_scheduled = false; //!<true if element is scheduled
    std::string _dirlbl = ""; //!< direction label, r (read), w (write), e (exec)
    const uint32_t _element_id = 0; //!< unique element id allowing multiple SchedElts per task for the same RT period / task

public:
    //! Desctructor
    virtual ~SchedElt();

    //! Copy constructor is deleted, to force explicit copying / find smart pointer bugs
    SchedElt(SchedElt&) = delete;

    /**
     * Return a string representation of the element
     * @return string
     */
    virtual std::string toString() const = 0;
    /**
     * Return the related element task is a task
     * @return
     */
    virtual SchedJob* schedtask() = 0;
    virtual const SchedJob* schedtask() const = 0;
    /**
     * Test if two elements are equals
     * @param a
     * @return true if they are equal, false otherwise
     */
    virtual bool equals(SchedElt* a) = 0;

    /**
     * Visit the scheduled elements
     * @param visitor
     * @param s all the schedule
     */
    virtual void accept(SchedEltVisitor* visitor, Schedule* s) = 0;

    /**
     * Concatene the element with a string
     * @param a
     * @return
     */
    virtual std::string operator+ (const std::string &a);
    /**
     * Concatene the element with a string
     * @param a
     * @return
     */
    virtual std::string operator+ (const char* a);

    /**
     * Get the ILP label
     * @return
     */
    virtual std::string ilp_label() const = 0;
    
    /**
     * Get the current release time of the element
     * @return
     */
    virtual const timinginfos_t rt() const = 0;
    virtual timinginfos_t rt() = 0;
    /**
     * Set the current release time of the element
     * @param r
     */
    virtual void rt(timinginfos_t r) = 0;
    /**
     * Get the current worst-case processing time
     * @return
     */
    virtual const timinginfos_t wct() const = 0;
    virtual timinginfos_t wct() = 0;
    /**
     * Set the current worst-case processing time
     * @param c
     */
    virtual void wct(timinginfos_t c) = 0;
    
    /**
     * Reset schedule info
     * clear rt and wct
     */
    virtual void reset() = 0;

    /**
     * Make a copy. This operation does not copy predecessor/successor edges, only internal data
     * @return a new instance
     */
    virtual SchedEltPtr copy(uint32_t new_id) = 0;

    /**
     * Add a successor element
     * @return
     */
     void add_successor(SchedElt * succ);

    /**
    * Remove a successor element
    * @return
    */
    void rem_successor(SchedElt * succ);

    /**
     * Perform a high-level copy
     * Do not fill siblings/previous/successors, use populate for that
     * @param rhs
     */
    void copy_values_from(const SchedElt &rhs);

    /**
     * Copy graph relations of rhs by mirroring its relations via the provided pool
     * @param rhs
     * @param pool
     */
    void populate(const SchedElt &rhs, const std::map<SchedElt*, SchedElt*> &pool);

    /**
     * Generate the id for a given SchedElt. Can be used to find elements in the LUT.
     * @param taskId
     * @param minRtPeriod
     * @param elementId
     * @param type
     * @return
     */
    static std::string get_id_for(const std::string &taskId, timinginfos_t minRtPeriod, uint32_t elementId, Type type);

    ACCESSORS_R(SchedElt, std::string, id)
    ACCESSORS_R_CONSTONLY(SchedElt, Task*, task)
    ACCESSORS_R(SchedElt, SchedElt::Type, type)
    ACCESSORS_RW(SchedPacket, std::string, dirlbl)
    ACCESSORS_R_CONSTONLY(SchedElt, std::vector<SchedElt*>&, successors)
    ACCESSORS_R_CONSTONLY(SchedElt, std::vector<SchedElt*>&, previous)
    ACCESSORS_R_CONSTONLY(SchedElt, std::vector<SchedElt*>&, siblings)
    ACCESSORS_RW(SchedElt, energycons_t, wce)
    ACCESSORS_RW(SchedElt, datamemsize_t, mem_data)
    ACCESSORS_RW(SchedElt, timinginfos_t, min_rt_period)
    ACCESSORS_RW(SchedElt, timinginfos_t, min_rt_deadline)
    ACCESSORS_RW(SchedElt, bool, interference_applied)
    ACCESSORS_R(SchedElt, uint32_t, element_id)
    ACCESSORS_RW(SchedElt, bool, is_scheduled)

protected:
    /**
     * Constructor
     * @param type
     * @param ta task
     * @param release min release time
     * @param element_id unique id (allowing duplicate SchedElts in the schedule)
     */
    explicit SchedElt(const SchedElt::Type t, const Task *ta, timinginfos_t release, uint32_t element_id = 0);

    /**
     * Copy constructor assigning a new id
     * @param rhs
     * @param element_id new id
     */
    explicit SchedElt(const SchedElt &rhs, uint32_t element_id);

    /**
     * Perform the low-level copy
     * @param rhs
     */
    virtual void docopy_values_from(const SchedElt &rhs) = 0;

    /**
     * Populate low-level
     * @param rhs right-hand-side
     * @param pool pool of elements to populate the new copy
     */
    virtual void dopopulate(const SchedElt &rhs, const std::map<SchedElt*, SchedElt*> &pool) = 0;
};
/**
 * Concatene the element with a string
 * @param a
 * @param b
 * @return
 */
std::string operator+ (const std::string &a, const SchedElt &b);
/**
 * Concatene the element with a string
 * @param a
 * @param b
 * @return
 */
std::string operator+ (const std::string &a, const SchedElt *b);
/**
 * Concatene the element with a string
 * @param a
 * @param b
 * @return
 */
std::string operator+ (const char* a, const SchedElt &b);
/**
 * Concatene the element with a output stream
 * @param os
 * @param a
 * @return
 */
std::ostream& operator<< (std::ostream& os, const SchedElt &a);
/**
 * Concatene the element with a output stream
 * @param os
 * @param a
 * @return
 */
std::ostream& operator<< (std::ostream& os, const SchedElt *a);

DECL_OPERATOR_OVERLOAD(bool, ==, SchedElt, Return true if the SchedElt are equal)
DECL_OPERATOR_OVERLOAD(bool, !=, SchedElt, Return false if the SchedElt are equal)

/**
 * Represent a schedule element of type Task
 */
class SchedJob : public SchedElt {
    friend class Schedule; //!< Element creation
    friend class ScheduleFactory;
protected:
    Phase *_phase = nullptr; //!< the related Phase
    bool _are_successors_created = false; //!< have successors been filled

    //DVFS for CPU and GPU
    uint128_t _frequency = 0;
    uint128_t _gpu_frequency = 0;
    uint8_t _gpu_required = 0;

    /*! The #SchedPacket list
     */
    std::vector<SchedPacket*> _packet_list;
    std::vector<SchedPacket*> _packet_read;
    std::vector<SchedPacket*> _packet_write;

    size_t _priority = 0; //!< priority in the schedule
    std::string _selected_version = ""; //!< if multiple version for the Task, which one has been selected by the scheduler

    size_t _current_chunk = 0; //!< current execution chunk
    std::vector<std::pair<timinginfos_t,timinginfos_t>> _exec_chunks; //!< execution chunk list in order

    bool _is_in_mutex = false; //!< Specific to the FLSContenationAware, Flag to determine if the scheduler scheduled it in mutex, which means its communication doesn't suffer from contention
public:
    /**
     * Constructor
     * @param ta task
     * @param release min release time
     * @param element_id unique id (allowing duplicate SchedElts in the schedule)
     */
    explicit SchedJob(const Task *ta, timinginfos_t release, uint32_t element_id = 0);
    
    /**
     * Add an execution chunk
     *
     * Their can be multiple execution chunks when using preemption
     *
     * @param endchunk end time of the current chunk
     * @param newrt release time of the next one
     */
    virtual void add_exec_chunk(timinginfos_t endchunk, timinginfos_t newrt);
    
    virtual bool move_exec_chunk(std::pair<timinginfos_t, timinginfos_t> chunk, timinginfos_t newrt);
    
    //! Destructor
    virtual ~SchedJob();

    //! \copydoc SchedElt::toString
    virtual std::string toString() const override;
    //! \copydoc SchedElt::schedtask
    virtual SchedJob* schedtask() override { return this;}
    virtual const SchedJob* schedtask() const override { return this;}
    //! \copydoc SchedElt::equals
    virtual bool equals(SchedElt* a) override;
    //! \copydoc SchedElt::accept
    virtual void accept(SchedEltVisitor *visitor, Schedule *s) override;

    /**
     * Get the release time of the first read packet
     * @return
     */
    timinginfos_t get_release_first_read();
    /**
     * Get the end time of the last read packet
     * @return
     */
    timinginfos_t get_end_last_read();
    /**
     * Get the release time of the first write packet
     * @return
     */
    timinginfos_t get_release_first_write();
    /**
     * Get the end time of the last write packet
     * @return
     */
    timinginfos_t get_end_last_write();

    //! \copydoc SchedElt::ilp_label
    virtual std::string ilp_label() const;
    
    //! \copydoc SchedElt::rt
    const timinginfos_t rt() const override;
    timinginfos_t rt() override;
    //! \copydoc SchedElt::rt(timinginfos_t r)
    void rt(timinginfos_t r) override;
    //! \copydoc SchedElt::wct
    const timinginfos_t wct() const override;
    timinginfos_t wct() override;
    //! \copydoc SchedElt::wct(timinginfos_t c)
    void wct(timinginfos_t c) override;
    
    virtual void reset();

    /**
     * Return the ith packet of the list
     * @param i
     * @return the packet, or nullptr if it doesn't exists or i is out of bound
     */
    SchedPacket *packet_list(size_t i);

    void add_read_packet(SchedPacket *pac);
    void add_write_packet(SchedPacket *pac);

    //! \copydoc SchedElt::copy
    SchedEltPtr copy(uint32_t new_id) override;

    ACCESSORS_RW(SchedJob, Phase*, phase)
    ACCESSORS_R_CONSTONLY(SchedJob, SINGLE_ARG(std::vector<SchedPacket*> &), packet_list)
    ACCESSORS_R_CONSTONLY(SchedJob, SINGLE_ARG(std::vector<SchedPacket*> &), packet_read)
    ACCESSORS_R_CONSTONLY(SchedJob, SINGLE_ARG(std::vector<SchedPacket*> &), packet_write)
    ACCESSORS_RW(SchedJob, size_t, priority)
    ACCESSORS_RW(SchedJob, std::string, selected_version)
    ACCESSORS_R_CONSTONLY(SchedJob, SINGLE_ARG(std::vector<std::pair<timinginfos_t,timinginfos_t>> &), exec_chunks)
    ACCESSORS_R(SchedJob, size_t, current_chunk)
    ACCESSORS_RW(SchedJob, uint128_t, frequency)
    ACCESSORS_RW(SchedJob, uint128_t, gpu_frequency)
    ACCESSORS_RW(SchedJob, uint8_t, gpu_required)
    ACCESSORS_RW(SchedJob, bool, is_in_mutex)
    ACCESSORS_RW(SchedJob, bool, are_successors_created)

private:
    //! \copydoc SchedElt::SchedElt(SchedElt&, uint32_t)
    explicit SchedJob(SchedJob &rhs, uint32_t element_id = 0) : SchedElt(rhs, element_id) {this->copy_values_from(rhs);}

    //! \copydoc SchedElt::docopy
    virtual void docopy_values_from(const SchedElt &rhs) override;

    //! \copydoc SchedElt::dopopulate
    virtual void dopopulate(const SchedElt &rhs, const std::map<SchedElt*, SchedElt*> &pool) override;
};

/**
 * Represent a communication to be scheduled on the bus
 *
 * When using a PREM or AER runtime task, we need to have communication phases
 * surrounding execution. This communication phases can be a single packet, or
 * split in many ordered packets if the mode burst for communication is activated.
 */
class SchedPacket : public SchedElt {
    friend class Schedule; //!< Element creation
    friend class ScheduleFactory;
protected:
    const size_t _packet_id_begin; //!< begin index of the packet-specific part of the id, for copying

    dataqty_t _data = 0; //!< data contained in to the transmitted packet
    std::string _datatype = ""; //!< type of data
    uint32_t _packetnum = 0; //!< number of the packet if many for the same communication
    uint32_t _transmission_nb_packet = 0; //!< number of packet in a single communication
    SchedJob *_schedtask = nullptr; //!< related SchedJob
    SchedJob *_tofromschedtask = nullptr; //!< to/from which SchedJob the packet is send/rec

    size_t _concurrency = 0; //!< amount of interference it suffers from

    timinginfos_t _rt = 0; //!< release time
    timinginfos_t _wct = 0; //!< worst communication cost

public:
    //! Destructor
    virtual ~SchedPacket() {}

    //! \copydoc SchedElt::toString
    std::string toString() const override;
    //! \copydoc SchedElt::equals
    bool equals(SchedElt* a) override;
    //! \copydoc SchedElt::accept
    void accept(SchedEltVisitor *visitor, Schedule *s) override;
    //! \copydoc SchedElt::copy
    SchedEltPtr copy(uint32_t new_id) override;

    //! \copydoc SchedElt::ilp_label
    virtual std::string ilp_label() const;
    
    ACCESSORS_RW(SchedPacket, dataqty_t, data)
    ACCESSORS_RW(SchedPacket, std::string, datatype)
    ACCESSORS_RW(SchedPacket, uint32_t, packetnum)
    ACCESSORS_RW(SchedPacket, uint32_t, transmission_nb_packet)
    ACCESSORS_RW(SchedPacket, SchedJob *, tofromschedtask)
    ACCESSORS_RW(SchedPacket, SchedJob*, schedtask)

    ACCESSORS_RW(SchedPacket, size_t, concurrency)

    ACCESSORS_RW(SchedPacket, timinginfos_t, rt)
    ACCESSORS_RW(SchedPacket, timinginfos_t, wct)
    
    virtual void reset();
    
    void set_transmission(size_t trsize, dataqty_t data, std::string datatype, datamemsize_t trdelay);
    /**
     * Copy constructor
     * @param rhs
     */
    explicit SchedPacket(const SchedPacket &rhs, uint32_t id=0);
private:
    //! Constructor
    explicit SchedPacket(SchedJob *t, timinginfos_t release, const std::string &dirlbl);
    explicit SchedPacket(SchedJob *t, timinginfos_t release, const std::string &dirlbl, SchedJob *senderOrDest, uint32_t packetnum);

    //! \copydoc SchedElt::docopy
    virtual void docopy_values_from(const SchedElt &rhs) override;
    //! \copydoc SchedElt::dopopulate
    virtual void dopopulate(const SchedElt &rhs, const std::map<SchedElt*, SchedElt*> &pool);
};

class SchedResource {
public:
    typedef struct {
        std::string elt;
        timinginfos_t start;
        timinginfos_t end;
    } booking_elt_t;
    typedef std::vector<booking_elt_t> booking_t;

protected:
    ResourceUnit *_resource; //!< The related resource in the IR
    std::string _label; //!< unique id for the core of the region
    booking_t _booking; //!< booking time list <start, end>

public:
    /**
     * Constructor
     * @param lbl \copybrief SchedResource::_label
     */
    explicit SchedResource(ResourceUnit *r, std::string lbl) : _resource(r), _label(std::move(lbl)) {}
    /**
     * Copy constructor
     * @param rhs
     */
    SchedResource(const SchedResource& rhs) { *this = rhs;}

    virtual ~SchedResource() {}

    /**
     * Copy constructor
     * @param rhs
     * @return
     */
    SchedResource& operator= (const SchedResource& rhs);
    
    ACCESSORS_R(SchedResource, std::string, label)
    ACCESSORS_R(SchedResource, SINGLE_ARG(booking_t&), booking)
    ACCESSORS_R(SchedResource, ResourceUnit*, resource)
    
    /**
     * Check if a time interval is booked
     * @param start time
     * @param _end time
     * @return true if time interval is already booked by some data
     */
    virtual bool is_booked(timinginfos_t start, timinginfos_t _end);
    
    virtual timinginfos_t get_free_time();
    
    void book(std::string, timinginfos_t start, timinginfos_t _end);
};

/**
 * Return true if the two given SPM region are equal
 * @param a
 * @param b
 * @return
 */
bool operator== (const SchedResource &a, const std::string &b);
/**
 * Return true if the two given SPM region are equal
 * @param a
 * @param b
 * @return
 */
bool operator== (const SchedResource &a, const SchedResource &b);

DECL_OPERATOR_OVERLOAD(bool, !=, SchedResource, Return false if the SchedResource are equal)

/**
 * Represent a SPM region to which data is mapped
 */
class SchedSpmRegion : public SchedResource {
    friend class SchedCore; //!< Friend SchedCore

protected:
    SchedCore *_core = nullptr; //!< related SchedCore
    datamemsize_t _size = 0; //!< size of the memory area

public:
    /**
     * Constructor
     * @param c \copybrief SchedSpmRegion::_core
     * @param lbl \copybrief SchedSpmRegion::_label
     */
    explicit SchedSpmRegion(SchedCore* c, const std::string &lbl) : SchedResource(nullptr, lbl), _core(c) {}
    /**
     * Constructor
     * @param c \copybrief SchedSpmRegion::_core
     * @param lbl \copybrief SchedSpmRegion::_label
     * @param s \copybrief SchedSpmRegion::_size
     */
    explicit SchedSpmRegion(SchedCore* c, const std::string &lbl, long s) : SchedResource(nullptr, lbl), _core(c),_size(s) {}

    /**
     * Copy constructor
     * @param rhs
     */
    SchedSpmRegion(const SchedSpmRegion& rhs) : SchedResource(rhs) { *this = rhs;}

    virtual ~SchedSpmRegion() {}
    /**
     * Copy constructor
     * @param rhs
     * @return
     */
    SchedSpmRegion& operator= (const SchedSpmRegion& rhs);

    ACCESSORS_RW(SchedSpmRegion, datamemsize_t, size)
    ACCESSORS_R(SchedSpmRegion, SchedCore*, core)
};
/**
 * Concatenate a string when printing the SpmRegion
 * @param a
 * @param b
 * @return
 */
std::string operator+ (const std::string &a, const SchedSpmRegion &b);
/**
 * Concatenate a string when printing the SpmRegion
 * @param a
 * @param b
 * @return
 */
std::string operator+ (const char* a, const SchedSpmRegion &b);
/**
 * Return true if the two given SPM region are equal
 * @param a
 * @param b
 * @return
 */
bool operator== (const SchedSpmRegion &a, const std::string &b);
/**
 * Return true if the two given SPM region are equal
 * @param a
 * @param b
 * @return
 */
bool operator== (const SchedSpmRegion &a, const SchedSpmRegion &b);

DECL_OPERATOR_OVERLOAD(bool, !=, SchedSpmRegion, Return false if the SchedSpmRegion are equal)

/**
 * Represent a computing unit used by the Schedule
 */
class SchedCore {
    ComputeUnit *_core; //!< The related core in the IR
    datamemsize_t _total_spm_size = 0; //!< The total size of the SPM for this core
    datamemsize_t _available_spm_size = -1; //!< The remaining available space in the SPM
    std::vector<SchedSpmRegion*> _memory_regions; //!< The region created in the SPM

    bool need_to_clean = false; //!< "this" is a deep copy of a schedule and must clean the memory, created with copy constructor

    timinginfos_t _clock;
public:
    /**
     * Constructor
     * @param p
     */
    explicit SchedCore(ComputeUnit *p);
    /**
     * Copy Constructor, performs a deep copy
     * @param rhs
     */
    explicit SchedCore(const SchedCore &rhs) { *this = rhs;}
    /**
     * Copy Constructor, performs a weak copy
     * @param rhs
     * @return
     */
    SchedCore& operator= (const SchedCore& rhs);
    /**
     * Destructor
     */
    virtual ~SchedCore();

    /**
     * Return true if the given SchedCore is equal
     * @param c
     * @return
     */
    bool equals(SchedCore *c);

    void clear();

    /**
     * Add and instanciate a SPM region to the core
     * @param label for the region
     * @param size of the region
     * @return
     */
    SchedSpmRegion* add_spmregion(const std::string &label, uint64_t size);

    /**
     * Add an already instantiated region to the core
     * @param sr
     */
    void add_spmregion(SchedSpmRegion *sr);

    /**
     * Get a SPM region
     * @param r
     * @return the #SchedSpmRegion or nullptr
     */
    SchedSpmRegion* schedspmregion(const std::string &r);
    //!\copydoc schedspmregion(const std::string &r)
    SchedSpmRegion* schedspmregion(const SchedSpmRegion &r);

    /**
     * Seek in the already added SPM region one that is free in the given time interval
     * @param start
     * @param end
     * @return nullptr if not found
     */
    SchedSpmRegion* get_available_spmregion(uint64_t start, uint64_t end);

    ACCESSORS_R(SchedCore, ComputeUnit*, core)
    ACCESSORS_R(SchedCore, datamemsize_t, total_spm_size)
    ACCESSORS_RW(SchedCore, datamemsize_t, available_spm_size);
    ACCESSORS_R(SchedCore, std::vector<SchedSpmRegion*>&, memory_regions);
    ACCESSORS_RW(SchedCore, timinginfos_t, clock)
};
/**
 * Concatenate a string and the core id
 * @param a
 * @param b
 * @return
 */
std::string operator+ (const std::string &a, const SchedCore &b);
/**
 * Concatenate a string and the core id
 * @param a
 * @param b
 * @return
 */
std::string operator+ (const char* a, const SchedCore &b);
/**
 * Return true if the two SchedCore are equals
 * @param lhs
 * @param rhs
 * @return
 */
bool operator== (const SchedCore& lhs, const SchedCore& rhs);

DECL_OPERATOR_OVERLOAD(bool, !=, SchedCore, Return false if the SchedCore are equal)

/**
 * Intermediate representation for a schedule
 */
class Schedule {
protected:
    const config_t *_conf;//!< the global configuration
    const SystemModel *_tg;//!< IR of the current application
    std::map<std::string, SchedEltPtr> _elements_lut;//!< The map of available elements to schedule, mapping e->id() to e
    std::vector<SchedElt *> _scheduled_elements; //!< The list of already scheduled elements
    std::vector<SchedCore *> _schedcores;//!< The list of available computing units
    std::map<SchedElt *, SchedCore *> _mapping; //!< Mapping between scheduled elements and computing units
    std::map<SchedElt *, SchedSpmRegion *> _spm_mapping; //!< Mapping of scheduled elements and SPM regions

    ScheduleProperties::Mapping _mapping_mode = ScheduleProperties::Mapping::MAP_GLOBAL;  //TODO: What is this???
    ScheduleProperties::PreemptionModel _preemption_mode = ScheduleProperties::PreemptionModel::PRE_FULL; //TODO: What is this???
    ScheduleProperties::PriorityScheme_e _priority_mode = ScheduleProperties::PriorityScheme_e::PRIO_DYNAMIC; //TODO: What is this???
    std::vector<SchedResource *> _schedresources; //!< List of resources
    std::vector<std::pair<std::string, std::string>> _stats; //TODO: What is this???

    std::vector<std::pair<SchedEltPtr, SchedElt *>> _delay_impacted_elements; //!< Mapping of elements that have already been scheduled and are impacted by a delay. This delay has to be propagated through the other schedules.

    //! Mark if this schedule copy requires a clean in the destructor
    globtiminginfos_t _makespan = 0; //!< Overall makespan of the schedule
    energycons_t _energy = 0; //!< Overall energy consumption of the schedule

    float _solvetime = 0; //!< Solving time
    ScheduleProperties::sched_statue_e _status = ScheduleProperties::sched_statue_e::SCHED_NOTDONE; //!< Current status of the schedule

public:
    /**
     * Constructor
     * @param t
     * @param c
     */
    explicit Schedule(const SystemModel *t, const config_t *c) : _conf(c), _tg(t) {};

    /**
     * Copy constructor
     *
     * @remark need to clean as it creates a deep copy with new object references
     *
     * @param rhs
     */
    Schedule(const Schedule &rhs);

    /**
     * Copy assignment operator
     * @remark Copy is deep, owned SchedElts will be freed
     * @param rhs
     * @return
     */
    Schedule &operator=(const Schedule &rhs);

    /**
     * Destructor
     */
    virtual ~Schedule();

    /**
     * Clear a schedule. This also removes all the SchedElt* elements, requiring a complete rebuild.
     */
    void clear();

    /**
     * Clear the assigned SchedElts in the schedule, but do not deallocate the SchedElts.
     */
    void clear_assigned();

    /**
     * Compute the overall makespan of the schedule in its current state
     */
    void compute_makespan();

    /**
     * Compute the estimated energy consumption of the schedule in its current state
     */
    void compute_energy();

    /**
     *
     * @return
     */
    void add_interference_impacted(SchedElt *element, timinginfos_t cost, std::string target);

    /**
     *
     * @return
     */
    void add_interference_impacted(std::vector<std::pair<SchedJob *, SchedElt *>> &rhs_list);

    /**
     *
     * @return
     */
    std::pair<timinginfos_t, timinginfos_t> get_timing_impacted_element(SchedElt *elt);

    /**
     * @return True if the schedule has elements.
     */
    bool is_built();

    ACCESSORS_R_CONSTONLY(Schedule, SystemModel *, tg)
    ACCESSORS_R_CONSTONLY(Schedule, config_t *, conf)
    ACCESSORS_RW(Schedule, globtiminginfos_t, makespan)
    ACCESSORS_RW(Schedule, energycons_t, energy)
    ACCESSORS_RW(Schedule, float, solvetime)
    ACCESSORS_R(Schedule, SINGLE_ARG(std::vector<std::pair<SchedEltPtr, SchedElt *>> & ), delay_impacted_elements)
    ACCESSORS_R(Schedule, SINGLE_ARG(std::map<SchedElt *, SchedCore *> & ), mapping)
    ACCESSORS_R(Schedule, SINGLE_ARG(std::map<std::string, SchedEltPtr> & ), elements_lut)
    ACCESSORS_R(Schedule, SINGLE_ARG(std::vector<SchedCore *> & ), schedcores)
    ACCESSORS_R(Schedule, SINGLE_ARG(std::vector<SchedResource *> & ), schedresources)
    ACCESSORS_R(Schedule, SINGLE_ARG(std::vector<std::pair<std::string, std::string>> & ), stats)
    ACCESSORS_R(Schedule, SINGLE_ARG(std::vector<SchedElt*> &), scheduled_elements)

    void stats(std::string lbl, std::string val) { _stats.push_back(std::make_pair(lbl, val)); }

    /**
     * Return the already scheduled elements sorted by increasing release time
     * @todo: I tried to have the type IterSchedEltPtr returned, this works on my machine, but not on Ubuntu 20.04
     * @todo: find a nice way to have a short return type instead of the ugly auto, then move the code back into the cpp file
     * @return
     */
    auto scheduled_elements(SchedCore *c) const {
        return boost::adaptors::filter(_scheduled_elements,
                [this, c](SchedElt* e) {return this->get_mapping(e) == c;}
            );
    }

    auto scheduled_elements(SchedCore *c) {
        return const_cast<const Schedule*>(this)->scheduled_elements(c);
    }

    SchedJob *schedjob(const std::string &j);
    SchedJob *schedjob(const std::string &j) const;

    /**
     * Get the first #SchedCore matching the given IR core
     * @param c
     * @return nullptr if not found
     */
    SchedCore *schedcore(ComputeUnit *c);

    //! @copydoc schedcore(ComputeUnit *c)
    SchedCore *schedcore(ComputeUnit *c) const;
    
    /**
     * Get the first #SchedResource matching the given IR resource
     * @param c
     * @return nullptr if not found
     */
    SchedResource *schedresource(const std::string &lbl);

    //! @copydoc schedcore(ResourceUnit *c)
    SchedResource *schedresource(const std::string &lbl) const;

    /**
     * Return all jobs of a #Task
     * @param t
     * @todo: I tried to have the type IterSchedJobPtr returned, this works on my machine, but not on Ubuntu 20.04
     * @todo: find a nice way to have a short return type instead of the ugly auto, then move the code back into the cpp file
     * @return
     */
    auto schedjobs(Task *t) const {
        return boost::adaptors::transform(
                boost::adaptors::filter(elements(),
                    [t](const SchedEltPtr &e) {return e->type() == SchedElt::Type::TASK && e->schedtask()->task() == t;}
                ),
                [](const SchedEltPtr &e) -> SchedJob* { return e->schedtask(); }
            );
    }
    auto schedjobs(Task *t) {
        return const_cast<const Schedule*>(this)->schedjobs(t);
    }

    /**
     * Return all #SchedJob from the list of known elements
     * @todo: I tried to have the type IterSchedJobPtr returned, this works on my machine, but not on Ubuntu 20.04
     * @todo: find a nice way to have a short return type instead of the ugly auto, then move the code back into the cpp file
     * @return
     */
    auto schedjobs() const {
        return boost::adaptors::transform(
            boost::adaptors::filter(elements(),
                [](const SchedEltPtr &e) {return e->type() == SchedElt::Type::TASK;}
            ),
            [](const SchedEltPtr &e) -> SchedJob* { return e->schedtask(); }
        );
    }
    auto schedjobs() {
        return const_cast<const Schedule*>(this)->schedjobs();
    }

    /**
     * Return all #SchedPacket from the list of known elements
     * @todo: I tried to have the type IterSchedPacketPtr returned, this works on my machine, but not on Ubuntu 20.04
     * @todo: find a nice way to have a short return type instead of the ugly auto, then move the code back into the cpp file
     * @return
     */
    auto schedpackets() const {
        return boost::adaptors::transform(
            boost::adaptors::filter(elements(),
                [](const SchedEltPtr &e) {return e->type() == SchedElt::Type::PACKET;}
            ),
            [](const SchedEltPtr &e) -> SchedPacket* { return static_cast<SchedPacket*>(e.get()); }
        );
    }
    auto schedpackets() {
        return const_cast<const Schedule*>(this)->schedpackets();
    }

    /**
     * Get the #SchedCore on which the param is mapped
     * @param e
     * @return
     */
    SchedCore* get_mapping(const SchedElt *e);
    SchedCore* get_mapping(SchedElt *e);
    //! \copydoc get_mapping(SchedElt *e)
    SchedCore* get_mapping(SchedElt *e) const;

    /**
     * Get the #SchedSpmRegion on which the param is mapped
     * @param e
     * @return
     */
    SchedSpmRegion* get_spm_mapping(SchedElt *e);
    //! \copydoc get_spm_mapping(SchedElt *e)
    SchedSpmRegion* get_spm_mapping(SchedElt *e) const;
    
    /**
     * Set the given element has scheduled
     *
     * @param e
     */
    void schedule(SchedElt *e);

    /**
     * Check if all jobs of a task have been scheduled
     * @param t
     * @return true if all jobs have been scheduled
     */
    bool is_task_fully_scheduled(SchedJob *t);

    /**
     * Map a #SchedElt to a #SchedCore
     * @param e
     * @param c
     */
    void map_core(SchedElt *e, SchedCore *c);

    /**
     * Map a #SchedElt to a SPM region for a certain time interval
     * @param e
     * @param s
     * @param start
     * @param _end
     */
    void map_spm(SchedElt *e, SchedSpmRegion *s, uint64_t start, uint64_t _end);

    /**
     * Concatenate a string when printing the schedule
     * @param a
     * @return
     */
    std::string operator+(const std::string &a);

    /**
     * Concatenate a string when printing the schedule
     * @param a
     * @return
     */
    std::string operator+(const char *a);

    ACCESSORS_RW(Schedule, ScheduleProperties::sched_statue_e, status)
    ACCESSORS_RW(Schedule, ScheduleProperties::Mapping, mapping_mode)
    ACCESSORS_RW(Schedule, ScheduleProperties::PreemptionModel, preemption_mode)
    ACCESSORS_RW(Schedule, ScheduleProperties::PriorityScheme_e, priority_mode)

    void copy_schedule(const Schedule &rhs);

    /**
     * Get a vector of pointers to all elements. The resulting vector is not the owner of the pointers, and is
     * freshly allocated for the caller, allowing it to be manipulated (sorted, e.g.).
     * @return vec of elements.
     */
    std::vector<SchedElt *> elements_unowned();

    //! @copydoc elements_unowned()
    std::vector<const SchedElt *> elements_unowned() const;

    /**
     * Get a reference to the owned values of the schedule.
     * @return vec of elements.
     */
    boost::select_second_mutable_range<std::map<std::string, std::unique_ptr<SchedElt>>> elements() {
        return boost::adaptors::values(this->_elements_lut);
    }

    /**
     * Get a reference to the owned values of the schedule.
     * @return vec of elements.
     */
    const boost::select_second_const_range<std::map<std::basic_string<char>, std::unique_ptr<SchedElt>>> elements() const {
        return boost::adaptors::values(this->_elements_lut);
    }

    const std::string get_frequency_unit() const {return _tg->frequency_unit();}
    const std::string get_time_unit() const {return _tg->time_unit();}
};

/**
 * Print a schedule
 * @param os
 * @param inf
 * @return
 */
std::ostream& operator<< (std::ostream& os, const Schedule &inf);
/**
 * Concatenate a string when printing the schedule
 * @param a
 * @param b
 * @return
 */
std::string operator+ (const std::string &a, const Schedule &b);
/**
 * Concatenate a string when printing the schedule
 * @param a
 * @param b
 * @return
 */
std::string operator+ (const char* a, const Schedule &b);

DECL_OPERATOR_OVERLOAD(bool, ==, Schedule, Return true if the Schedules are equal)

/**
 * Exception thrown the task set is unschedulable
 */
class Unschedulable : public std::exception {
    std::string msg= ""; //!< The message to carry
    const Task *t; //!< The task that is involved in the exception throwing

public:
    /**
     * Constructor
     * @param a
     * @param m
     */
    explicit Unschedulable(const Task *a, std::string m) : msg(m), t(a) {
        msg = "Unschedulable at task "+t->id()+" : "+m;
    }
    /**
     * Constructor
     * @param a
     * @param m
     */
    explicit Unschedulable(SchedElt *a, std::string m) : msg(m), t(a->schedtask()->task()) {
        msg = "Unschedulable at task "+t->id()+" : "+m;
    }
    /**
     * Constructor
     * @param m
     */
    explicit Unschedulable(std::string m) : msg(m) {
    }

    /**
     * Copy constructor
     * @param e
     */
    Unschedulable(const Unschedulable& e) {
        msg = e.msg;
        t = e.t;
    }

    virtual ~Unschedulable() NOEX {
    }

    /**
     * Return a char* representation of the exception
     * @return
     */
    virtual const char* what() const NOEX override {
        return msg.c_str();
    }
    /**
     * Return a string representation of the exception
     * @return
     */
    virtual const std::string str() const NOEX {
        return msg;
    }
};

/**
 * Exception thrown if the Scheduler is running out of space in SPMs
 * @param a
 * @param m
 */
class UnschedulableSPMSpace : public Unschedulable {
public:
    /**
     * Constructor
     * @param a
     * @param m
     */
    explicit UnschedulableSPMSpace(const Task *a, std::string m) : Unschedulable(a,m) {}
    /**
     * Constructor
     * @param m
     */
    explicit UnschedulableSPMSpace(std::string m) : Unschedulable(m) {}
    /**
     * Copy constructor
     * @param e
     */
    UnschedulableSPMSpace(const Unschedulable& e) : Unschedulable(e) {}
    //! Destructor
    virtual ~UnschedulableSPMSpace() NOEX {}
};
#endif /* SCHEDULE_H */

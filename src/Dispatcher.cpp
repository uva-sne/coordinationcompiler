/*
 * Copyright (C) 2022 Lukas Miedema <l.miedema@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "Dispatcher.hpp"

CompilerPass *Dispatcher::find_pass_before(const std::function<bool(const CompilerPass *)> &matcher, const CompilerPass *before) const {

    std::vector<std::unique_ptr<CompilerPass>>::const_iterator position;
    if (before == nullptr)
        position = this->_passes.end();
    else {
        position = find_if(this->_passes.begin(), this->_passes.end(), [before](const std::unique_ptr<CompilerPass> &p) { return p.get() == before; });
    }
    auto result = find_if(this->_passes.begin(), position, [&matcher](const std::unique_ptr<CompilerPass> &p) { return matcher(p.get()); });
    if (result == this->_passes.end())
        return nullptr;
    else
        return result->get();
}


CompilerPass *Dispatcher::find_pass_before(const std::string &rtti, const CompilerPass *before) const {
    return this->find_pass_before([&rtti](const CompilerPass *p) -> bool {
        return const_cast<CompilerPass *>(p)->get_uniqid_rtti() == rtti;
    }, before);
}

CompilerPass *Dispatcher::find_scheduler_or_solver_before(const CompilerPass *before) const {
    return this->find_pass_before([](const auto &p) { return p->get_uniqid_rtti().find("solver") || p->get_uniqid_rtti().find("simulator"); }, before);
}

void Dispatcher::dispatch(SystemModel *tg, config_t *conf, StrategyStateMachine *smg) {
    for (const auto &p: _passes) {
        Utils::DEBUG(std::string("--> calling ") + Utils::demangle(typeid(p.get()).name()));
        p->run(tg, conf, smg);
    }
}

const CompilerPass *Dispatcher::back() const {
    return this->passes().back().get();
}


CompilerPass* Dispatcher::add_pass(CompilerPass *pass) {
    this->_passes.push_back(std::unique_ptr<CompilerPass>(pass));
    return pass;
}

std::vector<CompilerPassPtr> *Dispatcher::operator->() {
    return &this->_passes;
}

std::vector<CompilerPassPtr>::iterator Dispatcher::begin() {
    return this->_passes.begin();
}

std::vector<CompilerPassPtr>::iterator Dispatcher::end() {
    return this->_passes.end();
}

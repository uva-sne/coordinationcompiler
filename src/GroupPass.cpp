/*
 * Copyright (C) 2023 Lukas Miedema <l.miedema@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "GroupPass.hpp"

void GroupPass::initialize() {
    // Make all nesting-aware compiler passes aware
    for (CompilerPassPtr &pass : this->_dispatcher.passes()) {
        auto nestingAware = dynamic_cast<GroupAwarePass<GroupPass> *>(pass.get());
        if (nestingAware != nullptr)
            nestingAware->setParent(this); // this will ignore us if we're the wrong type of parent
    }
}

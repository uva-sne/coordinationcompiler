/*
 * Copyright (C) 2019 Benjamin Rouxel <benjamin.rouxel@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CECILE_NFP_PARSER_HPP
#define CECILE_NFP_PARSER_HPP

#include <cstdlib>
#include <cstdio>
#include <cmath>
#include <string>
#include <iostream>
#include <fstream>
#include <boost/algorithm/string.hpp>
#include <algorithm>
#include <utility>

#include "config.hpp"
#include "InputParser.hpp"
#include "SystemModel.hpp"
#include "Utils.hpp"
#include "Utils/Log.hpp"
#include "Nfp.hpp"

/**
 * InputParser for the CSV-based file provided using the TeamPlay toolchain
 */
class NfpParser : public InputParser {
public:
    /**
     * Constructor
     * @param o #SystemModel
     * @param c current config
     * @param cmd_properties command line properties
     */
    NfpParser(SystemModel *o, config_t *c, CommandLineProperties *cmd_properties);

protected:
    virtual void parse(std::istream &input) override;
};

/**
 * CSV parser for the TeamPlay NFP format. This parser operates independently of the system model, allowing
 * the reuse of the parsed NFP content (relevant for unit tests).
 */
class NfpFileParser {
protected:
    std::vector<Nfp> _properties;
public:
    /**
     * Parse a CSV file.
     * @param input
     */
    NfpFileParser(std::istream &input);

    /**
     * Empty constructor.
     */
    explicit NfpFileParser() {}

    /**
     * Apply the parsed properties to a system model.
     * @param configuration
     * @param the system model
     */
    void apply(config_t &config, SystemModel &out, bool parseChildTasks);

    ACCESSORS_R(NfpFileParser, std::vector<Nfp>&, properties);
};

REGISTER_PARSER(NfpParser, ".nfp")

#endif

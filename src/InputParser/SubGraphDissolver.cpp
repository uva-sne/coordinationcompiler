/*
 * Copyright (C) 2020 Wouter Loeve <wouterloeve0@gmail.com>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "SubGraphDissolver.hpp"

using namespace std;

vector<EdgeHandler*> SubGraphDissolver::constructEdgeHandlers(vector<CoordinationLanguageParser::RuleEdgeContext *> edges) {
    vector<EdgeHandler*> newEdges;
    for (CoordinationLanguageParser::RuleEdgeContext* edge : edges) {
        EdgeHandler *e = new EdgeHandler(edge);
        newEdges.push_back(e);
    }

    return newEdges;
}

vector<EdgeHandler *> SubGraphDissolver::filterEdge(vector<EdgeHandler *> edges, string component, string function) {
    vector<EdgeHandler *> newEdges;
    for (EdgeHandler *edge : edges) {
        bool pred = false;
        if(function == "to") {
            pred = edge->isToEdge(component);
        }
        else if(function == "from") {
            pred = edge->isFromEdge(component);
        }
        if(pred) {
            newEdges.push_back(edge);
        }
    }

    return newEdges;
}

vector<EdgeHandler *> SubGraphDissolver::removeEdges(string component, string function,
        vector<EdgeHandler *> subGraphEdgesContext) {
    subGraphEdgesContext.erase(
            remove_if(
            subGraphEdgesContext.begin(),
            subGraphEdgesContext.end(),
            [component, function](EdgeHandler * edge) {
                if(function == "to") {
                    return edge->isToEdge(component);
                }
                return edge->isFromEdge(component);
            }
        ),
        subGraphEdgesContext.end()
    );
    return subGraphEdgesContext;
}

vector<EdgeHandler *> SubGraphDissolver::dissolveSubGraphEdges(
        vector<EdgeHandler *> current_scope_edges,
        vector<EdgeHandler *> subgraphEdges,
        CoordinationLanguageParser::RuleInstantiationContext * current_instantiation,
        CoordinationLanguageParser::RuleSubGraphContext *current_subgraph
        ) {
    string instance_name = current_subgraph->current.name;
    if(current_instantiation != NULL) {
        instance_name = current_instantiation->current.name;
    }
    // Get edges that go to this subgraph
    vector<EdgeHandler*> toSubgraph = SubGraphDissolver::filterEdge(current_scope_edges, instance_name, "to");

    // Get edges that go from the start of the subgraph to a component inside
    vector<EdgeHandler*> fromSubgraph = SubGraphDissolver::filterEdge(subgraphEdges, "in", "from");



    if(fromSubgraph.size() == 0 && current_subgraph->ruleInConnector().size() != 0) {
        Utils::WARN("No edges from the inside of the subgraph to its components");
        throw CompilerException("Coordination Parser", "NoEdgesFromIn");
    }

    // We can have multiple destinations inside the subgraph
    vector<CoordinationLanguageParser::ruleComponentRef_s> destInsideSubgraph;
    for (EdgeHandler *destsInsideSubgraph : fromSubgraph) {
        destInsideSubgraph = destsInsideSubgraph->getDest();
        // For all edges that go towards the subgraph
        for (EdgeHandler *edge : toSubgraph) {
            for (CoordinationLanguageParser::ruleComponentRef_s destFromOutsideSubgraph : edge->getDest()) {
                // We're only interested in destinations that actually go the subgraph (since there can be duplicate type edges)
                if(destFromOutsideSubgraph.comp == instance_name) {
                    // Take all destinations of this port at the edge of the subgraph to components in the subgraph.
                    for (CoordinationLanguageParser::ruleComponentRef_s destFromInsideSubgraph : destInsideSubgraph) {
                        // Replace the edges that go to the subgraph with the component they reference
                        string component_name = TeamplayCoordinationVisitor::templateComponentName(instance_name, destFromInsideSubgraph.comp);
                        bool replaced = edge->replaceToEdge(instance_name, destFromInsideSubgraph.conn, component_name);
                        // If the edge does go to a component in the subgraph, but there are no edges to replace (in case of duplicate ege), add it instead.
                        if(!replaced && edge->getType() == duplicate && destsInsideSubgraph->getType() == duplicate &&
                                destFromOutsideSubgraph.conn == destFromInsideSubgraph.conn) {
                            edge->addDestination(component_name, destFromInsideSubgraph.conn);
                        }
                    }
                }
            }
        }
    }

    // Get edges that start from this subgraph
    vector<EdgeHandler*> fromEdgeSubgraph = filterEdge(current_scope_edges, instance_name, "from");

    // Get edges that go to the edge of the subgraph (from inside)
    vector<EdgeHandler*> toEdgeSubgraph = filterEdge(subgraphEdges, "out", "to");

    if(toEdgeSubgraph.size() == 0 && current_subgraph->ruleOutConnector().size() != 0) {
        Utils::WARN("Subgraph has an output but is not used");
    }
    else if(toEdgeSubgraph.size() != 0 && current_subgraph->ruleOutConnector().size() == 0) {
        Utils::WARN("Internal subgraph edges lead to non-existing out connector");
    }
    else {
        /* Replace component references of the instantiated subgraph to the right component 
         *   according to the edge that goes from the subgraph to a component outside.
         */
        for (EdgeHandler *srcInsideSubGraph : toEdgeSubgraph) {
            for (EdgeHandler *edge : fromEdgeSubgraph) {
                for (CoordinationLanguageParser::ruleComponentRef_s src : edge->getSrc()) {
                    edge->replaceFromEdge(instance_name, src.conn,
                            TeamplayCoordinationVisitor::templateComponentName(instance_name, srcInsideSubGraph->getSrc(0).comp));
                }
            }
        }
    }

    // Remove the edges that still refer to the subgraph from the original list
    subgraphEdges = removeEdges("in", "from", subgraphEdges);
    subgraphEdges = removeEdges("out", "to", subgraphEdges);
    return subgraphEdges;
}
/*
 * Copyright (C) 2019 Benjamin Rouxel <benjamin.rouxel@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <boost/filesystem.hpp>
#include <string>

#include "NfpParser.hpp"

using namespace std;

NfpParser::NfpParser(SystemModel *o, config_t *c, CommandLineProperties *cmd_properties) : InputParser(o, c, cmd_properties) {}

void NfpParser::parse(std::istream &input) {
    NfpFileParser(input).apply(*this->conf, *this->out, true);
    out->compute_sequential_schedule_length(); // why is this here?
}

NfpFileParser::NfpFileParser(std::istream &input) {
    string line;
    vector<string> res;
    size_t i, j, column_count;

    getline(input, line); //header with column name

    boost::trim(line);
    boost::split(res, line, boost::is_any_of(","));
    column_count = res.size();
    int nfp_to_csv_column[NfpColumn::COLUMN_COUNT]; // maps NFP::PROPERTY_x to the index in csv row array
    for (i = 0; i < NfpColumn::COLUMN_COUNT; i++)
        nfp_to_csv_column[i] = -1;
    for (i = 0; i < res.size(); i++) {
        string needle = boost::trim_copy(res[i]);
        for (j = 0; j < NfpColumn::COLUMN_COUNT; j++) {
            if (find(column_id_to_strs[j].begin(),  column_id_to_strs[j].end(), needle) != column_id_to_strs[j].end()) {
                if (nfp_to_csv_column[j] != -1)
                    throw CompilerException("parser-nfp", "Duplicate column definition '" + needle + "'");
                nfp_to_csv_column[j] = (int) i;
            }
        }
    }
    i = 1;
    while (!input.eof()) {
        ++i;
        getline(input, line);
        boost::trim(line);
        if (line.empty())
            continue; // will skip the first empty line w/ optional unused global information
        vector<string> res;
        boost::split(res, line, boost::is_any_of(","));
        if (res.size() != column_count)
            throw CompilerException("parser-nfp", "Number of values inconsistent with header on line " + to_string(i));

        this->_properties.emplace_back();
        Nfp *nfp = &this->_properties.back();

#define DECLARE_PROPERTY(symbol_name_upper, symbol_name, csv_name, id) \
        nfp->symbol_name = (nfp_to_csv_column[id] == -1 ? "" : res[nfp_to_csv_column[id]]);
#include "NfpColumns.hpp"
#undef DECLARE_PROPERTY
    }
}

void NfpFileParser::apply(config_t& config, SystemModel& out, bool parseChildTasks) {
    for (auto nfp : this->_properties) {
        Task *t = out.task(nfp.componentName);
        if(t == nullptr) {
            if (parseChildTasks) {
                // Lukas: This code here is very questionable and O(n^2) complexity
                // I'd like to see it gone
                for (Task *a: out.tasks_it()) {
                    if (a->parent_task() != nullptr) {
                        if (a->id().substr(a->parent_task()->id().size() + 1) == nfp.componentName) {
                            t = a;
                            break;
                        }
                    }
                    Version *v = a->version(nfp.componentName);
                    if (v != nullptr) {
                        t = a;
                        nfp.componentVersion = nfp.componentName;
                        break;
                    }

                }
            }
            if(t == nullptr) {
                continue;
            }
        }

        Version *v = t->version(nfp.componentVersion);
        if(v == nullptr) {
            // if version not found in task make a new version.
            v = new Version(t, nfp.componentVersion);
            t->add_version(v);
        }

        if(!nfp.wcet.empty()) v->C(Utils::stringtime_to_time(nfp.wcet, out.time_unit())); // System wide time unit; default=ns or defined in config file
        if(!nfp.wcec.empty()) v->C_E(Utils::stringenergy_to_energy(nfp.wcec, out.energy_unit())); // System wide energy unit; default=J or defined in config file
        if(!nfp.fileName.empty()) v->cfile(nfp.fileName);
        if(!nfp.binary.empty()) v->cbinary(nfp.binary);
        if(!nfp.callable.empty()) v->cname(nfp.callable);
        if(!nfp.signature.empty()) v->csignature(nfp.signature);
        if(!nfp.cpu_frequency.empty()) v->frequency(Utils::stringfrequency_to_frequency(nfp.cpu_frequency, out.frequency_unit())); // transform to the System wide frequency unit; default=Hz or defined in config file
        if(!nfp.gpu_frequency.empty()) v->gpu_frequency(Utils::stringfrequency_to_frequency(nfp.gpu_frequency, out.frequency_unit())); // transform to the System wide frequency unit
        if(!nfp.gpu_frequency.empty()) v->gpu_required((!nfp.gpu_frequency.empty() && v->gpu_frequency() > 0));
        if(!nfp.architecture.empty()) {
            // "nfp.architecture" (annoyingly) does two things
            // 1. allows forcing a task to one particular processor
            // 2. sets the architecture of the task, limiting it to a set of processors (heterogeneous scheduling)
            // As such, you can set either the architecture name (e.g. amd64) or the processor name (e.g. P0) as nfp.architecture

            ComputeUnit *p = out.processor(nfp.architecture);
            if (p != nullptr) {
                // Case 1: lookup as a particular processor (by id)
                v->force_mapping_proc().insert(p);
            } else {
                // Case 2: assign all processors that match its architecture
                if (!v->force_mapping_proc().empty()) {
                    // Why is this not allowed: each version carries only one WCET value, not multiple
                    // And the WCET values will almost certainly be different for different architectures
                    // Worse: allowing this will silently overwrite a WCET value set in an earlier NFP entry,
                    // while retaining its processor mapping
                    throw CompilerException("parser-nfp", "Specifying multiple architectures for the same task version " +
                            nfp.componentName + "/" + nfp.componentVersion + " is not allowed");
                }

                vector<ComputeUnit*>& procs = out.processors();
                if(!any_of(procs.begin(), procs.end(), [&nfp](ComputeUnit *a) {return a->type == nfp.architecture; })) {
                    Utils::WARN("parser-nfp: ignoring " + nfp.componentName + "/" + nfp.componentVersion + " using unknown processor/processor type " + nfp.architecture);
                    
                    // Even if the version was not created here with "new", it needs to be deleted completely as it cannot
                    // be used as it does not have a hw platform that is valid
                    t->rem_version(v);
                    continue;
                }
                for(ComputeUnit *cu : procs) {
                    if (cu->type == nfp.architecture)
                        v->force_mapping_proc().insert(cu);
                }
            }
        }
    }
}


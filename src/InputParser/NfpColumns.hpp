// NFPColumns.hpp
// To use this, define the DECLARE_PROPERTIES macro and include this file
// DECLARE_PROPERTIES(symbol_name_upper, symbol_name, csv_names, id)

/**
 * Name of the component (task or phase), mandatory.
 */
DECLARE_PROPERTY(COMPONENT_NAME, componentName, {"Component Name"}, 0)

/**
 * Version of the component (if multiple versions exist).
 */
DECLARE_PROPERTY(COMPONENT_VERSION, componentVersion, {"Component Version"}, 1)

/**
 * Unused
 */
DECLARE_PROPERTY(VARIABLE_NAME, variableName, {"Variable Name"}, 2)

/**
 * Unused
 */
DECLARE_PROPERTY(LINE_NUMBER, lineNumber, {"Line Number"}, 3)

/**
 * Unused
 */
DECLARE_PROPERTY(FILENAME, fileName, {"Filename"}, 4)

/**
 * Worst-Case Execution Time estimate of this component. Assumed to never be exceeded.
 */
DECLARE_PROPERTY(WCET, wcet, {"WCET"}, 5)

/**
 * Worst-Case Energy Consumption estimate of this component.
 */
DECLARE_PROPERTY(WCEC, wcec, {"WCEngT"}, 6)

/**
 * Average energy consumption.
 */
DECLARE_PROPERTY(AET, aet, {"AET"}, 7)

/**
 * ?
 */
DECLARE_PROPERTY(ACEC, acec, {"AEngT"}, 8)

/**
 * Integer security level.
 */
DECLARE_PROPERTY(SECURITY_LEVEL, securityLevel, {"Security Level"}, 9)

/**
 * Architecture for this version. Ignored if it does not exist in the configuration XML.
 */
DECLARE_PROPERTY(ARCHITECTURE, architecture, {"Architecture"}, 10)

/**
 * Binary name (for linking).
 */
DECLARE_PROPERTY(BINARY, binary, {"Binary"}, 11)

/**
 * Function name of the task for code generation (without arguments)
 */
DECLARE_PROPERTY(CALLABLE, callable, {"Callable Name"}, 12)

/**
 * Default arguments to provide to the function (literal)
 */
DECLARE_PROPERTY(SIGNATURE, signature, SINGLE_ARG("Signature", "Additional Arguments", "Callable"), 13)

/**
 * Unused
 */
DECLARE_PROPERTY(CONFIDENCE, confidence, {"Confidence"}, 14)

/**
 * Default arguments to provide to the function (literal)
 */
DECLARE_PROPERTY(CPU_FREQUENCY, cpu_frequency, {"CPU Frequency"}, 15)

/**
 * Default arguments to provide to the function (literal)
 */
DECLARE_PROPERTY(GPU_FREQUENCY, gpu_frequency, {"GPU Frequency"}, 16)

// Add new columns to the end. Columns must be kept in ascending order without gaps in id
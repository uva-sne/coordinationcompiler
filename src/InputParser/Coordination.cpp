/*
 * Copyright (C) 2019 Benjamin Rouxel <benjamin.rouxel@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "Coordination.hpp"
#include "Helper_macros.hpp"

using namespace std;

class AntlrErrorListener : public antlr4::BaseErrorListener {
    virtual void syntaxError(antlr4::Recognizer *recognizer, antlr4::Token * offendingSymbol, size_t line, size_t charPositionInLine,
                             const string &msg, std::exception_ptr e) override
    {
        ostringstream s;
        s << "Line(" << line << ":" << charPositionInLine << ") Error(" << msg << ")";
        throw invalid_argument(s.str());
    }
};

void TeamplayCoordinationVisitor::parse(std::istream &filename) {
    Utils::WARN("Coordination file (.coord) are depreciated, a new language has been "
    "created called TeamPlay coordination language with extension .tey.\n"
    "Old coordination language is not maintained anymore.");
    antlr4::ANTLRInputStream inputstr(filename);
    CoordinationLanguageLexer lexer(&inputstr);
    antlr4::CommonTokenStream tokens(&lexer);
    tokens.fill();
    
    CoordinationLanguageParser parser(&tokens);
    // Adding error handler for parser
    parser.removeErrorListeners();
    AntlrErrorListener errorListner;
    parser.addErrorListener(&errorListner);
    parser.setBuildParseTree(true);
    
    parser.ruleApplication()->accept(this);
    
    out->clean_previous();
    out->populate_memory_usage();

    out->compute_sequential_schedule_length();
    
    out->populate_conditionality();
    out->compute_hyperperiod();
    out->classify();
    out->link_processors_and_voltage_islands();
}

ANTLR_ANY_TYPE TeamplayCoordinationVisitor::visitRuleApplication(CoordinationLanguageParser::RuleApplicationContext* ctx) {
    if(!ctx->current.deadline.empty())
        out->global_deadline(Utils::stringtime_to_time(ctx->current.deadline, out->time_unit()));
    if(!ctx->current.period.empty())
        out->global_period(Utils::stringtime_to_time(ctx->current.period, out->time_unit()));
    if(!ctx->current.energy.empty())
        out->global_available_energy(Utils::stringenergy_to_energy(ctx->current.energy, out->energy_unit()));

    for(CoordinationLanguageParser::RuleProfileContext * profiles : ctx->ruleProfile())
        profiles->accept(this);

    // Constructed here because they are used in the subgraph disolving.
    current_scope_edges = SubGraphDissolver::constructEdgeHandlers(ctx->ruleEdge());

    for(CoordinationLanguageParser::RuleDatatypeContext * datatypes : ctx->ruleDatatype())
        datatypes->accept(this);
    
    /* Go through al ltemplates and add them to the map, so we can reference them when we encounter an instantiation.
     */
    for(CoordinationLanguageParser::RuleTemplatesContext * templs : ctx->ruleTemplates()) {
        if (templs->ruleComponent()) {
            templates[templs->ruleComponent()->current.name] = templs;
        } else if (templs->ruleSubGraph()) {
            templates[templs->ruleSubGraph()->current.name] = templs;
        }
    }

    for(CoordinationLanguageParser::RuleComponentsContext * comps : ctx->ruleComponents()) {
        handleComponents(comps);
    }
    
    for(EdgeHandler *edge : current_scope_edges) {
        visitRuleEdge(edge);
    }
    
    return nullptr;
}

void TeamplayCoordinationVisitor::handleComponents(CoordinationLanguageParser::RuleComponentsContext *comps) {
    if (comps->ruleComponent()) {
        // Normal component
        out->add_task(ANTLR_CAST_API(comps->accept(this), Task*));   
    } else if (comps->ruleSubGraph()) {
        current_subgraph = comps->ruleSubGraph();
        current_subgraph->accept(this);
        subgraph_profiles.clear();
        current_subgraph = NULL;
    } else if (comps->ruleInstantiation()) {
        // Templates are parsed when an instantiation is encountered.
        current_instantiation = comps->ruleInstantiation();
        string templateName = current_instantiation->current.template_name;

        if (templates.count(templateName) == 0) {
            Utils::WARN("Template key \"" + templateName + "\" does not exist");
            throw CompilerException("Coordination parser", "Template key does not exist");
        }
        current_template = templates[templateName];
        
        // Parse subgraph or component
        current_template->accept(this);
        subgraph_profiles.clear();
        // Reset parameter
        current_template = NULL;
    }
}

/* 
 * Sets the individual setting values in the profile and returns it. 
 * Replaces any parameters set in params by 
 */
Profile setSettingValue(Profile p, CoordinationLanguageParser::ruleSettings_s s, vector<string> params, vector<string> values) {
    vector<pair <string,string>> definedOpts = {
        make_pair(s.period, "period"), 
//        make_pair(s.cname, "cname"),
        make_pair(s.deadline, "deadline"),
        make_pair(s.remove, "remove"),
        make_pair(s.offset, "offset")
    };

    if (params.size() != values.size()) {
        CompilerException("Coordination parser", "Number of parameters and number of values don't line up");
    }

    for (pair <string,string> pair: definedOpts) {
        if (! pair.first.empty()) {
            
            vector<string>::iterator it = find(params.begin(), params.end(), pair.first);
            string option_value = "";
            if( it != params.end()) {
                int index = distance(params.begin(), it);
                option_value = values[index];
            } else {
                option_value = pair.first;
            }
            if (pair.second == "remove") {
                p.addRemove(pair.first);
            } else {
                p.add(pair.second, option_value);
            }

            // Add possible type modifiers
            for (string type : s.types) {
                p.addModifier(pair.second, type);
            }
        }
    }

    return p;
}

/*! 
 * Sets the individual setting values in the profile and returns it.
 * @param p
 * @param s
 * @return Profile
 */
Profile setSettingValue(Profile p, CoordinationLanguageParser::ruleSettings_s s) {
    vector<string> emptyVector;
    return setSettingValue(p, s, emptyVector, emptyVector);
}

/*!
 * Adds fault-tolerance properties to profile given pairs of 
 * defined options (values) and the name of the options (to put into profiles)
 * These properties follow the name of the fault-tolerance method.
 * @param p
 * @param name
 * @param definedOpts
 * @param settingTypes
 * @param params
 * @param values
 * @param FTMod
 * @return Profile
 */
Profile addFTProps(Profile p, string name, vector<pair <string,string>> &definedOpts, 
        vector<string> settingTypes, vector<string> params, vector<string> values, string FTMod) {
    for (pair <string,string> pair: definedOpts) {
        
        if (! pair.first.empty()) {
            vector<string>::iterator it = find(params.begin(), params.end(), pair.first);
            string option_value = "";
            if( it != params.end()) {
                int index = distance(params.begin(), it);
                option_value = values[index];
            } else {
                option_value = pair.first;
            }
            p.add(name + "_" + pair.second, option_value);

            for (string type : settingTypes) {
                p.addModifier(pair.second, type);
            } 
        }
    }
    p.add(name, "true", FTMod);
    p.addModifier(name, FTMod);

    return p;
}

/*! 
 * Adds present fault-tolerance options to the given profile and returns it.
 * @param p
 * @param ctx
 * @param params
 * @param values
 * @param FTType
 */
Profile setFTSettingValue(Profile p, CoordinationLanguageParser::RuleFTSettingsContext *ctx, 
        vector<string> params, vector<string> values, string FTType) {
    if (ctx->ruleCheckpointSetting().size() >= 1 ) {
        for (CoordinationLanguageParser::RuleCheckpointSettingContext * options : ctx->ruleCheckpointSetting()) {
            CoordinationLanguageParser::ruleCheckpointSetting_s currentOpts = options->current;
            vector<pair <string,string>> definedOpts = {
                make_pair(currentOpts.shrinking, "shrinking")
            };
            p = addFTProps(p, "checkpoint", definedOpts, ctx->current.types, params, values, FTType);
        }
    }
    if (ctx->ruleStandbySetting().size() >= 1 ) {
        for (CoordinationLanguageParser::RuleStandbySettingContext * options : ctx->ruleStandbySetting()) {
            CoordinationLanguageParser::ruleStandbySetting_s currentOpts = options->current;
            vector<pair <string,string>> definedOpts = {
                make_pair(currentOpts.replicas, "replicas"),
                make_pair(currentOpts.sync, "sync")
            };
            p = addFTProps(p, "standby", definedOpts, ctx->current.types, params, values, FTType);
        }
    }
    if (ctx->ruleVersionSettings().size() >= 1 ) {
        for (CoordinationLanguageParser::RuleVersionSettingsContext * options : ctx->ruleVersionSettings()) {
            string name = "nVersion";

            // This option does not support parameters.
            CoordinationLanguageParser::ruleVersionSettings_s currentOpts = options->current;
            vector<pair <string,string>> definedOpts;
            for (long unsigned int i = 0; i < currentOpts.version.size(); i++) {
                // These options are saved a little differently since they can have multiple values
                // format is:  key: version_versions_<version> value: <amount>.
                // Used to set nModular redundancy involving different versions.
                string option_name = name + "_" + "versions_" + currentOpts.version[i];
                p.add(
                    option_name, 
                    currentOpts.value[i]
                );
                p.addModifier(option_name, FTType);
            }
            p.addModifier(name, FTType);
            p.add(name, "true");
        }
    }
    if (ctx->ruleNModularSetting().size() >= 1 ) {
        for (CoordinationLanguageParser::RuleNModularSettingContext * options : ctx->ruleNModularSetting()) {
            CoordinationLanguageParser::ruleNModularSetting_s currentOpts = options->current;
            vector<pair <string,string>> definedOpts = {
                make_pair(currentOpts.replicas, "replicas"), 
                make_pair(currentOpts.votingReplicas, "votingReplicas"),
                make_pair(currentOpts.waitingTime, "waitingTime"),
//                make_pair(currentOpts.waitingJoin, "waitingJoin")
            };
            p = addFTProps(p, "nModular", definedOpts, ctx->current.types, params, values, FTType);
        }
    }
    //! \todo types

    return p;
}

ANTLR_ANY_TYPE TeamplayCoordinationVisitor::visitRuleProfile(CoordinationLanguageParser::RuleProfileContext* context) {
    Profile profile;
    for (CoordinationLanguageParser::RuleSettingsContext *setting : context->ruleSettings()) {
        profile = setSettingValue(profile, setting->current);
    }
    
    if (context->ruleFTSettings()) {
        CoordinationLanguageParser::RuleFTSettingsContext *ftsettings = context->ruleFTSettings();
        string fftype = "";
        if (ftsettings->current.FTType.size() > 0) {
            fftype = ftsettings->current.FTType[0];
        }
        profile = setFTSettingValue(profile, ftsettings, {}, {}, fftype);
    }

    out->global_profile_by_name(context->current.name, profile);

    return nullptr;
}

ANTLR_ANY_TYPE TeamplayCoordinationVisitor::visitRuleTemplates(CoordinationLanguageParser::RuleTemplatesContext* context)  {
    if (context->ruleComponent()) {
        // Instantiate the component with the appropriate naming
        context->ruleComponent()->accept(this);
    } else if (context->ruleSubGraph()) {
        // Instantiate subgraph with the appropriate naming
        current_subgraph = context->ruleSubGraph(); 
        current_subgraph->accept(this);
        subgraph_profiles.clear();
        current_subgraph = NULL;
    }

    return nullptr;
}

ANTLR_ANY_TYPE TeamplayCoordinationVisitor::visitRuleSubGraph(CoordinationLanguageParser::RuleSubGraphContext* context) {
    string name = context->current.name;
    if (current_template == NULL && context->current.params.size() > 0) {
        Utils::WARN("Cannot use parameters in non-template subgraph \"" + name + "\" ignoring");
    }
    
    // Any parameters in the profiles section are replaced here.
    vector<string> params = current_subgraph->current.params;
    vector<string> values;
    // Only called for templates
    if (current_instantiation != NULL) {
        for (CoordinationLanguageParser::RuleConstantContext *item : current_instantiation->ruleConstant()) {
            if (item != NULL) {
                values.push_back(parseConstant(item));
            }
        }
    }
    for (string s : current_subgraph->current.profiles) {
        vector<string>::iterator it = find(params.begin(), params.end(), s);
        string profile_name = "";
        if( it != params.end()) {
            int index = distance(params.begin(), it);
            profile_name = values[index];
        } else {
            profile_name = s;
        }
        subgraph_profiles.push_back(out->global_profile_by_name(profile_name));
    }

    bool hasInlineSettings = false;
    Profile subgraphInlineProfile;
    for(CoordinationLanguageParser::RuleSettingsContext *setting : current_subgraph->ruleSettings()) {
        subgraphInlineProfile = setSettingValue(subgraphInlineProfile, setting->current);
        hasInlineSettings = true;
    }
    if (current_subgraph->ruleFTSettings()) {
        CoordinationLanguageParser::RuleFTSettingsContext *ftsettings = current_subgraph->ruleFTSettings();
        string fftype = "";
        if (ftsettings->current.FTType.size() > 0) {
            fftype = ftsettings->current.FTType[0];
        }
        subgraphInlineProfile = setFTSettingValue(subgraphInlineProfile, ftsettings, params, values, fftype);
        hasInlineSettings = true;
    }

    if (hasInlineSettings) {
        subgraph_profiles.push_back(subgraphInlineProfile);
    }

    for (CoordinationLanguageParser::RuleComponentsContext *ctx : current_subgraph->ruleComponents()) {
    out->add_task(ANTLR_CAST_API(ctx->accept(this), Task*));   
    }

    // Dissolve edges from subgraph
    vector<EdgeHandler *> handlers = SubGraphDissolver::constructEdgeHandlers(current_subgraph->ruleEdge());
    handlers = SubGraphDissolver::dissolveSubGraphEdges(current_scope_edges, handlers, current_instantiation, current_subgraph);
    
    for(EdgeHandler *handler : handlers) {    
        visitRuleEdge(handler);
    }

    return nullptr;
}

string TeamplayCoordinationVisitor::templateComponentName(string subgraph_name, string componentName) {
    return subgraph_name + "_" + componentName;
}

ANTLR_ANY_TYPE TeamplayCoordinationVisitor::visitRuleComponent(CoordinationLanguageParser::RuleComponentContext* context) {
    string id = "";
    if (current_template != NULL) {
        // Template component, name of the component is set to its instantiation name plus the component name
        id = templateComponentName(current_instantiation->current.name, context->current.name);
    } else if (current_subgraph != NULL) {
        // TODO nested subgraph naming support
        string subgraph_name = current_subgraph->current.name;
        id = subgraph_name + "_" + context->current.name;
    } else {
        // Normal component
        id = context->current.name;
// Ben: I don't like this profile things, it is only used for FT stuffs
//        uint64_t ns = Utils::stringtime_to_ns(t->profile().get("period"));
//
//        // Adding ns so that it can be recognised when turned back into a number
//        string ns_str = Utils::nstime_to_string(ns, "ns") + "ns";
//        t->profile().add("period", ns_str);
    }
    Task *t = new Task(id);

    vector<Profile> appliedProfiles = subgraph_profiles;
    // appliedProfiles.insert(appliedProfiles.begin(), subgraph_profiles.begin(), subgraph_profiles.end());
    for (string s : context->current.profiles) {
        appliedProfiles.push_back(out->global_profile_by_name(s));
    }

    // In case subgraph params exist
    vector<string> params;
    vector<string> values;
    if (current_subgraph != NULL && current_instantiation != NULL) {
        params = current_subgraph->current.params;

        for (vector<string>::size_type i = 0; i < params.size(); i++) {
            CoordinationLanguageParser::RuleConstantContext *constant = current_instantiation->ruleConstant(i);
            if (constant != NULL) {
                values.push_back(parseConstant(constant));
            }
        }
    }

    // Propagating properties is a bit weird, this line, should ensure that the global period/deadline are propagated
    // But tests fail, we should look into this.
    // Profile profile = Profile(out->global_deadline(), out->global_period());
    Profile profile;
    // Add the name on the profile immediately so it can be overwritten by cname parameters in the settings.
    profile.add("cname", t->id());
    for(CoordinationLanguageParser::RuleSettingsContext *setting : context->ruleSettings()) {
        // create profile and push back
        profile = setSettingValue(profile, setting->current, params, values);
    }
    if (context->ruleFTSettings()) {
        CoordinationLanguageParser::RuleFTSettingsContext *ftsettings = context->ruleFTSettings();
        string fftype = "";
        if (ftsettings->current.FTType.size() > 0) {
            fftype = ftsettings->current.FTType[0];
        }
        profile = setFTSettingValue(profile, ftsettings, params, values, fftype);
    }
    appliedProfiles.push_back(profile);

    Profile first = appliedProfiles[0];
    appliedProfiles.erase(appliedProfiles.begin());
    Profile __p = first.mergeProfiles(appliedProfiles);
    t->profile().merge(__p);

    for(CoordinationLanguageParser::RuleVersionContext *v : context->ruleVersion()) {
        ANTLR_ANY_TYPE parsed = v->accept(this);
#ifdef ANTLR_ANY_API
        if (parsed.isNotNull()) {
#else
        if(parsed.has_value()) {
#endif
            Version *version = ANTLR_CAST_API(parsed, Version*);
            version->task(t);
            t->add_version(version);
        }
    }

    for(CoordinationLanguageParser::RuleInConnectorContext *c : context->ruleInConnector()) {
        Connector *con = ANTLR_CAST_API(c->ruleConnector()->accept(this),Connector*);
        t->inputs().push_back(con);
    }
    for(CoordinationLanguageParser::RuleOutConnectorContext *c : context->ruleOutConnector()) {
        Connector *con = ANTLR_CAST_API(c->ruleConnector()->accept(this),Connector*);
        t->outputs().push_back(con);
    }
    return t;
}

ANTLR_ANY_TYPE TeamplayCoordinationVisitor::visitRuleVersion(CoordinationLanguageParser::RuleVersionContext* context) {
    Version *v = new Version(nullptr, context->current.name);

    if (!context->current.wcet.empty()) {
        v->C(Utils::stringtime_to_time(context->current.wcet, out->time_unit()));
    }

    if (!context->current.wcec.empty()) {
        v->C_E(Utils::stringenergy_to_energy(context->current.wcec, out->energy_unit()));
    }

//    if(!context->current.secu.empty())
//        v->security_lvl(stoi(context->current.secu));
    for(string arch : context->current.targetArch) {
        arch = arch.substr(1, arch.length()-2); // remove surrounding quote or double quote 
        ComputeUnit *proc = out->processor(arch);
        if(proc == nullptr) {
            if(any_of(out->processors().begin(), out->processors().end(), [arch](ComputeUnit *a) {return a->type == arch; })) {
                vector<ComputeUnit*>::iterator et = partition(out->processors().begin(), out->processors().end(), [arch](ComputeUnit *a) {return a->type == arch; });
                for(auto it=out->processors().begin(); it != et ; ++it)
                    v->force_mapping_proc().insert(*it);
            }
            else {
                ArchitectureProperties::ComputeUnitType p_class = ArchitectureProperties::from_string(arch);
                if(any_of(out->processors().begin(), out->processors().end(), [p_class](ComputeUnit *a) {return a->proc_class == p_class; })) {
                    vector<ComputeUnit*>::iterator et = partition(out->processors().begin(), out->processors().end(), [p_class](ComputeUnit *a) {return a->proc_class == p_class; });
                    for(auto it=out->processors().begin(); it != et ; ++it)
                        v->force_mapping_proc().insert(*it);
                }
                else {
                    Utils::WARN("Coord parser: can't find a processor name/type/class " + arch + " in the configuration: ignoring version");
                    return nullptr;
                }
            }
        }
        else
            v->force_mapping_proc().insert(proc);
    }
    return v;
}

ANTLR_ANY_TYPE TeamplayCoordinationVisitor::visitRuleDatatype(CoordinationLanguageParser::RuleDatatypeContext* context) {
    if(context->current.type.at(0) == '"' || context->current.type.at(0) == '\'')
        context->current.type = context->current.type.substr(1, context->current.type.size()-2);
    
    datatypes[context->current.name] = context->current.type;
    
    if(!context->current.init.empty() && (context->current.init.at(0) == '"' || context->current.init.at(0) == '\''))
        context->current.init = context->current.init.substr(1, context->current.init.size()-2);
    
    ADD_CONFIG_DATATYPES(conf, context->current.type, Utils::stringmem_to_bit(context->current.sizeInBits), context->current.init, context->current.type)
    return nullptr;
}

string TeamplayCoordinationVisitor::parseConstant(CoordinationLanguageParser::RuleConstantContext* context) {
    CoordinationLanguageParser::ruleConstant_s current = context->current;
    string result = "";
    if (! current.str.empty()) {
        result = current.str;
    } else if (! current.boolean.empty()) {
        result =  current.boolean;
    } else if (! current.integer.empty()) {
        result = current.integer;
    } else if (! current.id.empty()) {
        result = current.id;
    } else if (! current.time.empty()) {
        result = current.time;
    } else if (! current.energy.empty()) {
        result = current.energy;
    } else {
        CompilerException("Coordination parser", "ConstantEmpty");
    }
    return result;
}

ANTLR_ANY_TYPE TeamplayCoordinationVisitor::visitRuleConnector(CoordinationLanguageParser::RuleConnectorContext* context) {
    if(!datatypes.count(context->current.type))
        throw CompilerException("parser", "Undefined datatypes \""+context->current.type+"\"");
    
    return new Connector(context->current.name, stoul(context->current.tokens), datatypes[context->current.type], false);
}

ANTLR_ANY_TYPE TeamplayCoordinationVisitor::visitRuleEdge(EdgeHandler *handler) {
    try {
        switch(handler->getType()) {
            case simple:
                return visitRuleSimpleEdge(handler);
            case duplicate:
                return visitRuleDuplicateEdge(handler);
            case dataor:
                return visitRuleDataOrEdge(handler);
            case schedOr:
                return visitRuleDataOrEdge(handler);
            case envOr:
                return visitRuleEnvOrEdge(handler);
            default:
                Utils::WARN("Edge type could not be established");
                throw CompilerException("Coordination parser", "EdgeTypeNotFound");
        }
    }
    catch(CompilerException &e) {
        Utils::WARN(e.what());
        Utils::WARN("Ignored parsing error, keep going");
    }
    return nullptr;
}

ANTLR_ANY_TYPE TeamplayCoordinationVisitor::visitRuleSimpleEdge(EdgeHandler *handler) {
    add_dependency(handler->getSrc(0), handler->getDest(0), handler->getDelay());
    return nullptr;
}

// @todo: figure out what could be the wcet of such task, it probably depends on the architecture, in config?
// @todo: there is probably a need to force mapping of such task, in config ?
ANTLR_ANY_TYPE TeamplayCoordinationVisitor::visitRuleDuplicateEdge(EdgeHandler *handler) {
    struct CoordinationLanguageParser::ruleComponentRef_s srcref = handler->getSrc(0);
    
    Task *tsrc = out->task(srcref.comp);
    if(tsrc == nullptr)
        throw CompilerException("parser", "Can't find task "+srcref.comp);
    vector<Connector*>::iterator cit = find_if(tsrc->outputs().begin(), tsrc->outputs().end(), [srcref](Connector *a) {return a->id() == srcref.conn; });
    if(cit == tsrc->outputs().end())
        throw CompilerException("parser", "Can't find "+srcref.conn);
    Connector *csrc = *cit;
    csrc->type(Connector::type_e::DUPLICATE);

    for (CoordinationLanguageParser::ruleComponentRef_s dest : handler->getDest()) {
        try {
            add_dependency(srcref, dest, handler->getDelay());
        }
        catch(CompilerException &e) {
            Utils::WARN(string(e.what())+" -- ignoring");
        }
    }
    return nullptr;
}

ANTLR_ANY_TYPE TeamplayCoordinationVisitor::visitRuleDataOrEdge(EdgeHandler *handler) {
    uint32_t dtok = handler->getDelay();
    
    CompilerException *ee = nullptr;
    vector<Task*> group;
    for(vector<CoordinationLanguageParser::ruleComponentRef_s>::size_type i = 0 ; i >= handler->getDest().size()-1 ; i++) {     
        try {
            add_dependency(handler->getSrc(i), handler->getDest(i), dtok);
            group.push_back(out->task(handler->getDest(i).comp));
        }
        catch(CompilerException &e) {
            Utils::WARN(e.what());
            ee = &e;
        }
    }
    
    string srcComp = handler->getSrc(0).comp;
    Task* src = out->task(srcComp);
    if(src == nullptr)
        throw CompilerException("parser", "Can't find task "+srcComp);
    conditional_edge_t ce = {.cond = conditional_edge_e::DATA, .succs = group};
    src->conditional_out_groups().push_back(ce);
    
    if(ee != nullptr)
        throw *ee;
    return nullptr;
}

ANTLR_ANY_TYPE TeamplayCoordinationVisitor::visitRuleSchedOrEdge(EdgeHandler *handler) {
    throw Todo("visitRuleSchedOrEdge");
    return nullptr;
}

ANTLR_ANY_TYPE TeamplayCoordinationVisitor::visitRuleEnvOrEdge(EdgeHandler *handler) {
    throw Todo("visitRuleEnvOrEdge");
    return nullptr;
}

void TeamplayCoordinationVisitor::add_dependency(const CoordinationLanguageParser::ruleComponentRef_s& srcref, const CoordinationLanguageParser::ruleComponentRef_s& dstref, uint32_t delay_tokens) {
    Task* src = out->task(srcref.comp);
    if(src == nullptr)
        throw CompilerException("parser", "Can't find task src \""+srcref.comp+"\" for an edge");
    Task* dst = out->task(dstref.comp);

    if(dst == nullptr)
        throw CompilerException("parser", "Can't find task sink \""+dstref.comp+"\" for an edge");
    
    if(all_of(src->outputs().begin(), src->outputs().end(), [srcref](Connector *a) { return a->id() != srcref.conn; }))
        throw CompilerException("parser", "Can't find connector \""+srcref.conn+"\" for task "+srcref.comp+" in an edge (source)");

    if(all_of(dst->inputs().begin(), dst->inputs().end(), [dstref](Connector *a) { return a->id() != dstref.conn; }))
        throw CompilerException("parser", "Can't find connector \""+dstref.conn+"\" for task "+dstref.comp+" in an edge (sink)");

    out->add_dependency(src, dst, srcref.conn, dstref.conn);
    
    if((!srcref.versi.empty() && dstref.versi.empty()) || (srcref.versi.empty() && !dstref.versi.empty()))
        throw CompilerException("parser", "When forcing versions dependencies both src and dst must have a version name see simple edge "+*src+" -> "+*dst);
    else if(!srcref.versi.empty()) {
        if(src->versions(srcref.versi) == nullptr)
            throw CompilerException("parser", "Unknow version \""+srcref.versi+"\" for task "+*src);
        if(dst->versions(dstref.versi) == nullptr)
            throw CompilerException("parser", "Unknow version \""+dstref.versi+"\" for task "+*dst);
        Version *vsrc = src->versions(srcref.versi);
        Version *vdst = dst->versions(dstref.versi);
        vsrc->force_successors().push_back(vdst);
        vdst->force_previous().push_back(vsrc);
        Utils::WARN("TeamplayCoordinationVisitor: Version dependency is a work in progress");
    }

    dst->delay_tokens()[src].push_back(delay_tokens); // also push 0 as the length must match number of connections
}

// TODO: check if still needed!
//void TeamplayCoordinationVisitor::propagate_global_deadline(string tmp_global_deadline) {
//    if(!tmp_global_deadline.empty())
//        out->global_deadline(stoul(tmp_global_deadline));
//
//    out->compute_sequential_schedule_length();
//    out->compute_best_sequential_length_based_on_energy();
//}

/*
 * Copyright (C) 2020 Stephen Nicholas Swatman <s.n.swatman@uva.nl>
 *                    Univeristy of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "TeamPlay.hpp"

using namespace std;


namespace {

    class CoordinationGuardRejectionException : public runtime_error {
    public:

        explicit CoordinationGuardRejectionException() :
            runtime_error("Failed to assert truth of guard statement.") {
        }

        const char* what() const throw () {
            return "Failed to assert truth of guard statement.";
        }
    };
}

TeamPlayInputParser::TeamPlayInputParser(SystemModel *o, config_t *c, CommandLineProperties *cmd_properties) :
    InputParser(o, c, cmd_properties), TeamPlayBaseVisitor() {}

void TeamPlayInputParser::parse(std::istream &input) {
    antlr4::ANTLRInputStream inputstr(input);
    TeamPlayLexer lexer(&inputstr);

    antlr4::CommonTokenStream tokens(&lexer);
    tokens.fill();

    TeamPlayParser parser(&tokens);
    parser.setBuildParseTree(true);
    parser.ruleapp()->accept(this);

    
    
    for(TemplateInstance *tpl_inst : top_level_tpl_instance) {
        map<Task*, Task*> tasks;
        vector<Channel*> chans;
        resolveTemplateInstanciation(tpl_inst, &tasks, &chans);
        for(Task *t : Utils::mapvalues(tasks))
            out->add_task(t);
        for(Channel *c : chans) 
            out->add_dependency(c->src, c->sink, c->csrc, c->csink);
        
        vector<Task*> tmp = Utils::mapkeys(tpl_inst->predecessor_tasks());
        for(Task* el : tmp) 
            out->rem_dependency(el, tpl_inst);
        tmp = Utils::mapkeys(tpl_inst->successor_tasks());
        for(Task* el : tmp) 
            out->rem_dependency(tpl_inst, el);
        
        out->rem_task(tpl_inst);
        
        //delete tpl_inst;
    }
    
    for(TemplateInstance *tpl_inst : top_level_subgraphs) {
        map<Task*, Task*> tasks;
        vector<Channel*> chans;
        resolveSubgraphs(tpl_inst, &tasks, &chans);
        for(Task *t : Utils::mapvalues(tasks)) {
            t->parent_task(tpl_inst->parent);
            out->add_task(t);
            t->parent_task()->child_tasks().push_back(t);
        }
        for(Channel *c : chans) 
            out->add_dependency(c->src, c->sink, c->csrc, c->csink);
        
        // I don't remove the parent when it is a subgraph, this allows to keep
        // the metatask corresponding to the Hyperepoch from Nvidia
        // they will be scheduled as well, but as they have a WCET=0, it shouldn't
        // interfere with anything
//        vector<Task*> tmp = Utils::mapkeys(tpl_inst->parent->predecessor_tasks());
//        for(Task* el : tmp) 
//            out->rem_dependency(el, tpl_inst->parent);
//        tmp = Utils::mapkeys(tpl_inst->parent->successor_tasks());
//        for(Task* el : tmp) 
//            out->rem_dependency(tpl_inst->parent, el);
//        
//        out->rem_task(tpl_inst->parent);
        
        //delete tpl_inst;
    }
    
    //delete [] top_level_tpl_instance; ??
    //delete [] templates; ??
    
    for(pair<Task *, vector<string>> apply_prof : profile_settings)
        for(string profile : apply_prof.second)
            resolveProfile(apply_prof.first, profile);
    
    out->clean_previous();
    
    out->populate_memory_usage();
    out->populate_conditionality();
    out->compute_hyperperiod();
    out->compute_sequential_schedule_length();
    out->classify();
    out->link_processors_and_voltage_islands();
}

ANTLR_ANY_TYPE TeamPlayInputParser::visitRuleappSettingComponents(TeamPlayParser::RuleappSettingComponentsContext *context) {
    for (TeamPlayParser::RulemetaComponentContext * c : context->rulemetaComponent()) {
        if(c->rulecomponent())
            out->add_task(ANTLR_CAST_API(c->rulecomponent()->accept(this), Task*));   
        else if(c->ruletemplateInstantiation()) {
            TemplateInstance *tpl_inst = ANTLR_CAST_API(c->ruletemplateInstantiation()->accept(this), TemplateInstance*);
            out->add_task(tpl_inst);
            top_level_tpl_instance.push_back(tpl_inst);
        }
    }
    return nullptr;
}

ANTLR_ANY_TYPE TeamPlayInputParser::visitRuletemplateInstantiation(TeamPlayParser::RuletemplateInstantiationContext *context) {
    if(context->current.name == "in" || context->current.name == "out")
        throw CompilerException("parser-tey", "in/out are reserved keyword, they can't be used to name a component/template/template instanciation");
    
    vector<Template*>::iterator it = find_if(templates.begin(), templates.end(), [context](Template *a) { return a->id() == context->current.template_name; });
    if(it == templates.end())
        throw CompilerException("parser-tey", "Can't instantiate template "+context->current.template_name+" into "+context->current.name);
    Template *tpl = *it;
    
    if(tpl->params.size() != context->rulesubgraphParams().size())
        throw CompilerException("parser-tey", "Wrong number of arguments when instantiating template "+context->current.template_name+" into "+context->current.name);
    
    map<string, ANTLR_ANY_TYPE> arguments;
    for(size_t i=0 ; i < tpl->params.size() ; ++i) {
        arguments[tpl->params[i]] = context->rulesubgraphParams(i)->accept(this);
#ifdef ANTLR_ANY_API
        if(arguments[tpl->params[i]].isNull())
#else
        if(!arguments[tpl->params[i]].has_value())
#endif
            throw CompilerException("parser-tey", "Null argument given for parameter "+tpl->params[i]+" for template instanciation "+context->current.name);
    }
    
    TemplateInstance *tpl_inst = new TemplateInstance(context->current.name);
    tpl_inst->clone(tpl);
    for(pair<string, vector<function<void(Task*, ANTLR_ANY_TYPE)>>> el : tpl_inst->param_to_apply_component) {
        for(function<void(Task*, ANTLR_ANY_TYPE)> f : el.second) {
            f(tpl_inst, move(arguments[el.first]));
        }
    }
    
    if(profile_settings.count(tpl)) {
        for(string profile : profile_settings[tpl]) {
            if(profiles.count(profile)) {
                Profile *p = profiles[profile];
                Profile *np = new Profile;
                np->merge(*p);
                for(pair<string, vector<function<void(Profile*, ANTLR_ANY_TYPE)>>> el : tpl_inst->param_to_apply_profile) {
                    for(function<void(Profile*, ANTLR_ANY_TYPE)> f : el.second) {
                        f(np, arguments[el.first]);
                    }
                }
                profile += "__tplinst__"+tpl_inst->id();
                profiles[profile] = np;
            }
            profile_settings[tpl_inst].push_back(profile);
        }
    }
    
    return tpl_inst;
}

ANTLR_ANY_TYPE TeamPlayInputParser::visitRulesubgraphParams(TeamPlayParser::RulesubgraphParamsContext *context) {
    if(context->ruleintValue())
        return context->ruleintValue()->accept(this);
    if(context->ruleboolValue())
        return context->ruleboolValue()->accept(this);
    if(context->ruleId())
        return context->ruleId()->accept(this);
    if(context->rulestringValue())
        return context->rulestringValue()->accept(this);
    if(context->ruletimeValue())
        return context->ruletimeValue()->accept(this);
    if(context->ruleenergyValue())
        return context->ruleenergyValue()->accept(this);
    if(context->rulepercentValue())
        return context->rulepercentValue()->accept(this);
    return nullptr;
}

ANTLR_ANY_TYPE TeamPlayInputParser::visitRuletemplates(TeamPlayParser::RuletemplatesContext *context) {
    if(context->current.name == "in" || context->current.name == "out")
        throw CompilerException("parser-tey", "in/out are reserved keyword, they can't be used to name a component/template");
    
    Template *tpl = new Template(context->current.name);
    current_template.push(tpl);
    current_component.push(tpl);
    
    tpl->params = context->current.params;
    
    for(TeamPlayParser::RulesubgraphSettingsContext *c : context->rulesubgraphSettings())
        c->accept(this);
    
    vector<Task*> srcs;
    vector<Task*> sinks;
    for(Task *t : tpl->components) {
        if(t->predecessor_tasks().size() == 0) {
            srcs.push_back(t);
            for(Connector *in : tpl->inputs()) {
                if(t->input(in->id()) == nullptr) {
                    Connector *nin = new Connector(*in);
                    t->inputs().push_back(nin);
                }
            }
        }
        if(t->successor_tasks().size() == 0) {
            sinks.push_back(t);
            for(Connector *out : tpl->outputs()) {
                if(t->output(out->id()) == nullptr) {
                    Connector *nout = new Connector(*out);
                    t->outputs().push_back(nout);
                }
            }
        }
    }
    
    current_component.pop();
    current_template.pop();
    templates.push_back(tpl);
    return nullptr;
}

ANTLR_ANY_TYPE TeamPlayInputParser::visitRulesubgraphComponentSettings(TeamPlayParser::RulesubgraphComponentSettingsContext *context) {
    for (TeamPlayParser::RulemetaComponentContext * c : context->rulemetaComponent()) {
        if(c->rulecomponent())
            current_template.top()->components.push_back(ANTLR_CAST_API(c->rulecomponent()->accept(this), Task*));
        else if(c->ruletemplateInstantiation()) {
            TemplateInstance *tpl_inst = ANTLR_CAST_API(c->ruletemplateInstantiation()->accept(this), TemplateInstance*);
            current_template.top()->components.push_back(tpl_inst);
            current_template.top()->tpl_instance.push_back(tpl_inst);
        }
    }
    return nullptr;
}

ANTLR_ANY_TYPE TeamPlayInputParser::visitRulecomponentSettingSubComponents(TeamPlayParser::RulecomponentSettingSubComponentsContext *context) {
    vector<Version*> v = current_component.top()->versions();
    if(v.size() > 0 && !(v.size() == 1 && v[0]->id() == (CONFIG_DEF_VERSION_NAME)))
        throw CompilerException("parser-tey", "Component "+current_component.top()->id()+" can't both contain a subgraph and versions as it will not be instantiated.");
    
    TemplateInstance *tpl_inst = new TemplateInstance(current_component.top()->id());
    tpl_inst->parent = current_component.top();
    tpl_inst->deep_clone(tpl_inst->parent);
    
    for(string prof_name : profile_settings[tpl_inst->parent])
        profile_settings[tpl_inst].push_back(prof_name);
    
    current_component.push(tpl_inst);
    current_template.push(tpl_inst);
    
    context->rulesubgraphComponentSettings()->accept(this);
    if(context->ruletemplateSettingChannels())
        context->ruletemplateSettingChannels()->accept(this);
    
    current_template.pop();
    current_component.pop();
    
    // Add tpl_inst to subgraph at the end, to mimic a DFS and resolve deeper nested subgraph first
    if(!current_template.empty())
        current_template.top()->subgraphs.push_back(tpl_inst);
    else
        top_level_subgraphs.push_back(tpl_inst); 
    
    return nullptr;
}


ANTLR_ANY_TYPE TeamPlayInputParser::visitRulemode(TeamPlayParser::RulemodeContext *context) {
//    string mode_name = context->RULE_ID()->getText();
//
//    if(context->ruleintValue() != nullptr) {
//        mode_type[mode_name] = ModeType::INTEGER;
//        mode_int_values[mode_name] = ANTLR_CAST_API(context->ruleintValue()->accept(this), int64_t)();
//    }
//    else if(context->ruleenumDecl() != nullptr) {
//        mode_type[mode_name] = ModeType::ENUM;
//        vector<string> mode_values = ANTLR_CAST_API(context->ruleenumDecl()->accept(this), SINGLE_ARG(vector < string >)) ();
//        mode_enum[mode_name] = mode_values;
//        mode_enum_values[mode_name] = mode_values.front();
//    }
    throw Todo("Need to properly handle scheduling mode, parsing/handling in scheduling/code generation");
    return nullptr;
}

ANTLR_ANY_TYPE TeamPlayInputParser::visitRuleenumDecl(TeamPlayParser::RuleenumDeclContext *context) {
    vector<string> rv;
//
//    for (antlr4::tree::TerminalNode * t : context->RULE_ID()) {
//        rv.push_back(t->getText());
//    }
    throw Todo("Need to properly handle scheduling mode, parsing/handling in scheduling/code generation");
    return rv;
}

ANTLR_ANY_TYPE TeamPlayInputParser::visitRuleguard(TeamPlayParser::RuleguardContext *context) {
//    bool v = ANTLR_CAST_API(context->ruleguardExpr()->accept(this), bool)();
//
//    if(v)
//        return nullptr;
//
//    throw CoordinationGuardRejectionException();
    throw Todo("Need to properly handle guards in parsing/scheduling/code generation");
}

ANTLR_ANY_TYPE TeamPlayInputParser::visitRuleguardEnumExpr(TeamPlayParser::RuleguardEnumExprContext *context) {
//    string mode_name = context->RULE_ID(0)->getText();
//    string mode_val = context->RULE_ID(1)->getText();
//    string true_val;
//
//    try {
//        ModeType mode_type = this->mode_type.at(mode_name);
//
//        if(mode_type != ModeType::ENUM) {
//            throw runtime_error("Mode " + mode_name + " is not an enum mode.");
//        }
//
//        true_val = mode_enum_values[mode_name];
//    } catch (out_of_range &) {
//        throw runtime_error("Mode " + mode_name + " was not specified.");
//    }
//
//    string op = context->RULE_COMPARISON_OP()->getText();
//
//    if(op == "==") {
//        if(!(true_val == mode_val)) {
//            return false;
//        }
//    }
//    else if(op == "!=") {
//        if(!(true_val != mode_val)) {
//            return false;
//        }
//    }
//    else {
//        throw runtime_error("Impossible enumerator operation \"" + op + "\"!");
//    }

    throw Todo("Need to properly handle guards in parsing/scheduling/code generation");
    return true;
}

ANTLR_ANY_TYPE TeamPlayInputParser::visitRuleguardIntExpr(TeamPlayParser::RuleguardIntExprContext *context) {
//    string mode_name = context->RULE_ID()->getText();
//    long mode_val = ANTLR_CAST_API(context->ruleintValue()->accept(this), int64_t)();
//    long true_val;
//
//    try {
//        ModeType mode_type = this->mode_type.at(mode_name);
//
//        if(mode_type != ModeType::INTEGER) {
//            throw runtime_error("Mode " + mode_name + " is not an int mode.");
//        }
//
//        true_val = mode_int_values[mode_name];
//    } catch (out_of_range &) {
//        throw runtime_error("Mode " + mode_name + " was not specified.");
//    }
//
//    string op = context->RULE_COMPARISON_OP()->getText();
//
//    if(op == "==") {
//        if(!(true_val == mode_val)) {
//            return false;
//        }
//    }
//    else if(op == "!=") {
//        if(!(true_val != mode_val)) {
//            return false;
//        }
//    }
//    else if(op == ">") {
//        if(!(true_val > mode_val)) {
//            return false;
//        }
//    }
//    else if(op == ">=") {
//        if(!(true_val >= mode_val)) {
//            return false;
//        }
//    }
//    else if(op == "<") {
//        if(!(true_val < mode_val)) {
//            return false;
//        }
//    }
//    else if(op == "<=") {
//        if(!(true_val <= mode_val)) {
//            return false;
//        }
//    }
//    else {
//        throw runtime_error("Impossible integer operation \"" + op + "\"!");
//    }

    throw Todo("Need to properly handle guards in parsing/scheduling/code generation");
    return true;
}

ANTLR_ANY_TYPE TeamPlayInputParser::visitRuleguardExpr(TeamPlayParser::RuleguardExprContext *context) {
    bool lhs;

//    if(context->ruleguardEnumExpr() != nullptr) {
//        lhs = ANTLR_CAST_API(context->ruleguardEnumExpr()->accept(this), bool)();
//    }
//    else if(context->ruleguardIntExpr() != nullptr) {
//        lhs = ANTLR_CAST_API(context->ruleguardIntExpr()->accept(this), bool)();
//    }
//    else {
//        throw logic_error("Impossible state reached.");
//    }
//
//    if(context->ruleguardExpr() != nullptr) {
//        bool rhs = ANTLR_CAST_API(context->ruleguardExpr()->accept(this), bool)();
//        string op = context->RULE_BOOLEAN_OP()->getText();
//
//        if(op == "&&") {
//            return lhs && rhs;
//        }
//        else if(op == "||") {
//            return lhs || rhs;
//        }
//        else {
//            throw logic_error("Impossible state reached.");
//        }
//    }

    throw Todo("Need to properly handle guards in parsing/scheduling/code generation");
    return lhs;
}

ANTLR_ANY_TYPE TeamPlayInputParser::visitRuleappSettingDeadline(TeamPlayParser::RuleappSettingDeadlineContext *context) {
    timinginfos_t v = ANTLR_CAST_API(context->ruletimeValue()->accept(this), timinginfos_t);
    out->global_deadline(v);
    return nullptr;
}

ANTLR_ANY_TYPE TeamPlayInputParser::visitRuleappSettingPeriod(TeamPlayParser::RuleappSettingPeriodContext *context) {
    timinginfos_t v = ANTLR_CAST_API(context->ruletimeValue()->accept(this), timinginfos_t);
    out->global_period(v);
    return nullptr;
}

ANTLR_ANY_TYPE TeamPlayInputParser::visitRuleappSettingEnergy(TeamPlayParser::RuleappSettingEnergyContext *context) {
    energycons_t v = ANTLR_CAST_API(context->ruleenergyValue()->accept(this),energycons_t);
    out->global_available_energy(v);
    return nullptr;
}

ANTLR_ANY_TYPE TeamPlayInputParser::visitRulecomponent(TeamPlayParser::RulecomponentContext *context) {
    if(context->current.name == "in" || context->current.name == "out")
        throw CompilerException("parser-tey", "in/out are reserved keyword, they can't be used to name a component/template");
    
    Task * t = new Task(context->current.name);
    current_component.push(t);

    for (TeamPlayParser::RulecomponentSettingContext * c : context->rulecomponentSetting())
        c->accept(this);

    current_component.pop();
    return t;
}

ANTLR_ANY_TYPE TeamPlayInputParser::visitRulecomponentSettingOutports(TeamPlayParser::RulecomponentSettingOutportsContext *context) {
    for (TeamPlayParser::RuleportDeclContext * c : context->ruleportDecl()) {
        current_component.top()->outputs().push_back(ANTLR_CAST_API(c->accept(this),Connector *));
    }
    return nullptr;
}

ANTLR_ANY_TYPE TeamPlayInputParser::visitRulecomponentSettingInports(TeamPlayParser::RulecomponentSettingInportsContext *context) {
    for (TeamPlayParser::RuleportDeclContext * c : context->ruleportDecl()) {
        current_component.top()->inputs().push_back(ANTLR_CAST_API(c->accept(this), Connector *));
    }
    return nullptr;
}
ANTLR_ANY_TYPE TeamPlayInputParser::visitRulecomponentSettingState(TeamPlayParser::RulecomponentSettingStateContext *context) {
//    for (TeamPlayParser::RuleportDeclContext * c : context->ruleportDecl()) {
//        current_component.top()->states().push_back(ANTLR_CAST_API(c->accept(this), Connector *));
//    }
    throw Todo("Need to properly handle states parsing/scheduling/code generation");
}

ANTLR_ANY_TYPE TeamPlayInputParser::visitRulecomponentSettingVersions(TeamPlayParser::RulecomponentSettingVersionsContext *context) {
    for (TeamPlayParser::RuleversionContext * c : context->ruleversion()) {
        /*!
         * \remark This will call visitRuleversion.
         */
        ANTLR_CAST_API(c->accept(this), Version *);
    }
    return nullptr;
}

ANTLR_ANY_TYPE TeamPlayInputParser::visitRulecomponentSettingSecurity(TeamPlayParser::RulecomponentSettingSecurityContext *context) {
    uint32_t secu_level;
    string paramname;
    tie(secu_level, paramname) = ANTLR_CAST_API(context->ruleintValueOrParamName()->accept(this), SINGLE_ARG(pair<int64_t, string>));
    storeComponentParameterResolution("security", secu_level, paramname, [](Task *t, ANTLR_ANY_TYPE v) { t->security_lvl(ANTLR_CAST_API(v, uint32_t));});
    return nullptr;
}

ANTLR_ANY_TYPE TeamPlayInputParser::visitRulecomponentSettingDeadline(TeamPlayParser::RulecomponentSettingDeadlineContext *context) {
    timinginfos_t deadline;
    string paramname;
    tie(deadline, paramname) = ANTLR_CAST_API(context->ruletimeValueOrParamName()->accept(this), SINGLE_ARG(pair<timinginfos_t, string>));
    storeComponentParameterResolution("deadline", deadline, paramname, [](Task *t, ANTLR_ANY_TYPE v) { t->D(ANTLR_CAST_API(v, timinginfos_t));});
    return nullptr;
}

ANTLR_ANY_TYPE TeamPlayInputParser::visitRulecomponentSettingWCET(TeamPlayParser::RulecomponentSettingWCETContext *context) {
    timinginfos_t WCET;
    string paramname;
    tie(WCET, paramname) = ANTLR_CAST_API(context->ruletimeValueOrParamName()->accept(this), SINGLE_ARG(pair<timinginfos_t, string>));
    storeComponentParameterResolution("WCET", WCET, paramname, [](Task *t, ANTLR_ANY_TYPE v) { t->C(ANTLR_CAST_API(v, timinginfos_t));});
    return nullptr;
}

ANTLR_ANY_TYPE TeamPlayInputParser::visitRulecomponentSettingWCEC(TeamPlayParser::RulecomponentSettingWCECContext *context) {
    energycons_t WCEC;
    string paramname;
    tie(WCEC, paramname) = ANTLR_CAST_API(context->ruleenergyValueOrParamName()->accept(this), SINGLE_ARG(pair<energycons_t, string>));
    storeComponentParameterResolution("WCEC", WCEC, paramname, [](Task *t, ANTLR_ANY_TYPE v) { t->C_E(ANTLR_CAST_API(v, energycons_t));});
    return nullptr;
}

ANTLR_ANY_TYPE TeamPlayInputParser::visitRulecomponentSettingPeriod(TeamPlayParser::RulecomponentSettingPeriodContext *context) {
    timinginfos_t period;
    string paramname;
    tie(period, paramname) = ANTLR_CAST_API(context->ruletimeValueOrParamName()->accept(this), SINGLE_ARG(pair<timinginfos_t, string>));
    storeComponentParameterResolution("period", period, paramname, [](Task *t, ANTLR_ANY_TYPE v) { t->T(ANTLR_CAST_API(v, timinginfos_t));});
    return nullptr;
}

ANTLR_ANY_TYPE TeamPlayInputParser::visitRulecomponentSettingCName(TeamPlayParser::RulecomponentSettingCNameContext *context) {
    string cname;
    string paramname;
    tie(cname, paramname) = ANTLR_CAST_API(context->rulestringValueOrParamName()->accept(this), SINGLE_ARG(pair<string, string>));
    storeComponentParameterResolution("cname", cname, paramname, [](Task *t, ANTLR_ANY_TYPE v) {
        t->cname(ANTLR_CAST_API(v, string));
    });
    return nullptr;
}

ANTLR_ANY_TYPE TeamPlayInputParser::visitRulecomponentSettingCSignature(TeamPlayParser::RulecomponentSettingCSignatureContext *context) {
    string csignature;
    string paramname;
    tie(csignature, paramname) = ANTLR_CAST_API(context->rulestringValueOrParamName()->accept(this), SINGLE_ARG(pair<string, string>));
    storeComponentParameterResolution("csignature", csignature, paramname, [](Task *t, ANTLR_ANY_TYPE v) { t->csignature(ANTLR_CAST_API(v, string));});
    return nullptr;
}

ANTLR_ANY_TYPE TeamPlayInputParser::visitRulecomponentSettingCodeSize(TeamPlayParser::RulecomponentSettingCodeSizeContext *context) {
    datamemsize_t codesize;
    string paramname;
    tie(codesize, paramname) = ANTLR_CAST_API(context->rulememValueOrParamName()->accept(this), SINGLE_ARG(pair<datamemsize_t, string>));
    storeComponentParameterResolution("codesize", codesize, paramname, [](Task *t, ANTLR_ANY_TYPE v) { t->memory_code_size(ANTLR_CAST_API(v, datamemsize_t));});
    return nullptr;
}

ANTLR_ANY_TYPE TeamPlayInputParser::visitRulecomponentSettingStackSize(TeamPlayParser::RulecomponentSettingStackSizeContext *context) {
    datamemsize_t stacksize;
    string paramname;
    tie(stacksize, paramname) = ANTLR_CAST_API(context->rulememValueOrParamName()->accept(this), SINGLE_ARG(pair<datamemsize_t, string>));
    storeComponentParameterResolution("stacksize", stacksize, paramname, [](Task *t, ANTLR_ANY_TYPE v) { t->local_memory_storage(ANTLR_CAST_API(v, datamemsize_t));});
    return nullptr;
}

ANTLR_ANY_TYPE TeamPlayInputParser::visitRulecomponentSettingArch(TeamPlayParser::RulecomponentSettingArchContext *context) {
    string arch;
    string paramname;
    tie(arch, paramname) = ANTLR_CAST_API(context->rulestringValueOrParamName()->accept(this), SINGLE_ARG(pair<string, string>));
    storeComponentParameterResolution("component arch", arch, paramname, [this](Task *t, ANTLR_ANY_TYPE v) {
        string arch = ANTLR_CAST_API(v, std::string);
        bool found = false;

        ArchitectureProperties::ComputeUnitType pclass = ArchitectureProperties::ComputeUnitType::CUT_UNDEFINED;
        try {
            pclass = ArchitectureProperties::from_string(arch);
        }
        catch(...) {}

        // First check processors
        for (ComputeUnit * p : this->out->processors()) {
            if (p->id == arch || p->proc_class == pclass || p->type == arch || p->type+"."+p->id == arch  || boost::algorithm::to_lower_copy(p->type+"."+ArchitectureProperties::to_string(p->proc_class)) == boost::algorithm::to_lower_copy(arch)) {
                t->force_mapping_proc().insert(p);
                found = true;
            }
        }

        // Second check coprocessors for possible missing architecture
        if (!found){
            for (ComputeUnit * p : this->out->coprocessors()) {
                if (p->id == arch || p->proc_class == pclass || p->type == arch || p->type+"."+p->id == arch  || boost::algorithm::to_lower_copy(p->type+"."+ArchitectureProperties::to_string(p->proc_class)) == boost::algorithm::to_lower_copy(arch)) {
                    t->force_mapping_proc().insert(p);
                    found = true;
                }
            }
        }


        if (!found)
            throw CompilerException("parser-tey", "Processor name, or architecture class/type \"" + arch + "\" not found.");

    });
    return nullptr;
}

ANTLR_ANY_TYPE TeamPlayInputParser::visitRulecomponentSettingResource(TeamPlayParser::RulecomponentSettingResourceContext *context) {
    string resource;
    string paramname;
    tie(resource, paramname) = ANTLR_CAST_API(context->rulestringValueOrParamName()->accept(this), SINGLE_ARG(pair<string, string>));
    storeComponentParameterResolution("component resource", resource, paramname, [this](Task *t, ANTLR_ANY_TYPE v) {
        string resource = ANTLR_CAST_API(v, std::string);
        bool found = false;

        for (ResourceUnit * p : this->out->resources()) {
            if (p->id == resource) {
                t->resource_usage().push_back(p);
                found = true;
            }
        }

        if (!found)
            throw CompilerException("parser-tey", "Resource name \"" + resource + "\" not found.");

    });
    return nullptr;
}

ANTLR_ANY_TYPE TeamPlayInputParser::visitRulecomponentSettingRepeat(TeamPlayParser::RulecomponentSettingRepeatContext *context) {
    int64_t repeat;
    string paramname;
    tie(repeat, paramname) = ANTLR_CAST_API(context->ruleintValueOrParamName()->accept(this), SINGLE_ARG(pair<int64_t, string>));
    storeComponentParameterResolution("repeat", repeat, paramname, [](Task *t, ANTLR_ANY_TYPE v) { t->repeat(ANTLR_CAST_API(v, int64_t));});
    return nullptr;
}

ANTLR_ANY_TYPE TeamPlayInputParser::visitRulecomponentSettingTaint(TeamPlayParser::RulecomponentSettingTaintContext *context) {
    current_component.top()->tainted(1);
    return nullptr;
}

ANTLR_ANY_TYPE TeamPlayInputParser::visitRulecomponentSettingOffset(TeamPlayParser::RulecomponentSettingOffsetContext *context) {
    timinginfos_t offset;
    string paramname;
    tie(offset, paramname) = ANTLR_CAST_API(context->ruletimeValueOrParamName()->accept(this), SINGLE_ARG(pair<timinginfos_t, string>));
    storeComponentParameterResolution("offset", offset, paramname, [](Task *t, ANTLR_ANY_TYPE v) { t->offset(ANTLR_CAST_API(v, timinginfos_t));});
    return nullptr;
}

ANTLR_ANY_TYPE TeamPlayInputParser::visitRulecomponentSettingsProfileInstanciation(TeamPlayParser::RulecomponentSettingsProfileInstanciationContext *context) {
    string prof_name;
    string paramname;
    tie(prof_name, paramname) = ANTLR_CAST_API(context->rulestringValueOrParamName()->accept(this), SINGLE_ARG(pair<string, string>));
    storeComponentParameterResolution("profile", prof_name, paramname, [this](Task *t, ANTLR_ANY_TYPE v) {this->profile_settings[t].push_back(ANTLR_CAST_API(v, string));});
    return nullptr;
}

ANTLR_ANY_TYPE TeamPlayInputParser::visitRulecomponentSettingRemoveSubgraphProp(TeamPlayParser::RulecomponentSettingRemoveSubgraphPropContext *context) {
    string remove_name;
    string paramname;
    tie(remove_name, paramname) = ANTLR_CAST_API(context->rulestringValueOrParamName()->accept(this), SINGLE_ARG(pair<string, string>));
    storeComponentParameterResolution("remove", remove_name, paramname, [this](Task *t, ANTLR_ANY_TYPE v) { this->profile_settings[t].push_back("!"+ANTLR_CAST_API(v, string));});
    return nullptr;
}

ANTLR_ANY_TYPE TeamPlayInputParser::visitRulecomponentSettingProfiles(TeamPlayParser::RulecomponentSettingProfilesContext *context) {
    if(context->rulecomponentSettingsProfileInstanciation()) {
        context->rulecomponentSettingsProfileInstanciation()->accept(this);
    }
    else if(context->ruleprofileFTSettings() != nullptr) {
        string profname = "__"+current_component.top()->id()+"__special_profile__";
        Profile *p = new Profile();
        profile_settings[current_component.top()].push_back(profname);
        profiles[profname] = p;
        current_profile = p;

        context->ruleprofileFTSettings()->accept(this);

        current_profile = nullptr;
    }
    return nullptr;
}

ANTLR_ANY_TYPE TeamPlayInputParser::visitRuleversion(TeamPlayParser::RuleversionContext *context) {
    if(!current_template.empty() && current_component.top() == current_template.top())
        throw CompilerException("parser-tey", "Component "+current_component.top()->id()+" can't both contain a subgraph and versions as it will not be instantiated.");
    
    Version * v = new Version(current_component.top(), context->current.name);
    current_component.top()->add_version(v);
    current_version = v;
    current_phase = v->phases(CONFIG_DEF_VERSION_NAME);

    for (TeamPlayParser::RuleversionSettingContext * c : context->ruleversionSetting())
        c->accept(this);

    current_phase = nullptr;
    current_version = nullptr;
    return v;
}

ANTLR_ANY_TYPE TeamPlayInputParser::visitRulephases(TeamPlayParser::RulephasesContext *context) {
    Phase *p = new Phase(current_version, context->current.name);
    current_version->add_phase(p);
    current_phase = p;
    
    for (TeamPlayParser::RulephaseSettingContext * c : context->rulephaseSetting()) {
        /*!
         * \remark This will call visitRulephaseSetting, but that has a default
         * implementation which will instead call visitRuleversionSettingWCET,
         * visitRuleversionSettingSecurity, etc. There is also the
         * visitRuleversionSettingGuard setting, but this is also not
         * explicitly implemented. This means that a three-way indirect call to
         * visitRuleguard* will be made, which can throw a
         * CoordinationGuardRejectionException to indicate that the guard is
         * not accepted.
         */
        try {
            c->accept(this);
        } catch (CoordinationGuardRejectionException &) {
            return nullptr;
        }
    }
    
    current_phase = nullptr;
    return nullptr;
}

ANTLR_ANY_TYPE TeamPlayInputParser::visitRuleversionSettingWCET(TeamPlayParser::RuleversionSettingWCETContext *context) {
    timinginfos_t WCET;
    string paramname;
    tie(WCET, paramname) = ANTLR_CAST_API(context->ruletimeValueOrParamName()->accept(this), SINGLE_ARG(pair<timinginfos_t, string>));
    Version *version = current_version;
    storeComponentParameterResolution("version WCET", WCET, paramname, [version](Task *t, ANTLR_ANY_TYPE v) { t->versions(version->id())->C(ANTLR_CAST_API(v, timinginfos_t));});
    return nullptr;
}

ANTLR_ANY_TYPE TeamPlayInputParser::visitRuleversionSettingWCEC(TeamPlayParser::RuleversionSettingWCECContext *context) {
    energycons_t WCEC;
    string paramname;
    tie(WCEC, paramname) = ANTLR_CAST_API(context->ruleenergyValueOrParamName()->accept(this), SINGLE_ARG(pair<energycons_t, string>));
    Version *version = current_version;
    storeComponentParameterResolution("version WCEC", WCEC, paramname, [version](Task *t, ANTLR_ANY_TYPE v) { t->versions(version->id())->C_E(ANTLR_CAST_API(v, energycons_t));});
    return nullptr;
}

ANTLR_ANY_TYPE TeamPlayInputParser::visitRuleversionSettingCodeSize(TeamPlayParser::RuleversionSettingCodeSizeContext *context) {
    datamemsize_t codesize;
    string paramname;
    tie(codesize, paramname) = ANTLR_CAST_API(context->rulememValueOrParamName()->accept(this), SINGLE_ARG(pair<datamemsize_t, string>));
    Version *version = current_version;
    storeComponentParameterResolution("version codesize", codesize, paramname, [version](Task *t, ANTLR_ANY_TYPE v) { t->versions(version->id())->memory_code_size(ANTLR_CAST_API(v, datamemsize_t));});
    return nullptr;
}

ANTLR_ANY_TYPE TeamPlayInputParser::visitRuleversionSettingStackSize(TeamPlayParser::RuleversionSettingStackSizeContext *context) {
    datamemsize_t stacksize;
    string paramname;
    tie(stacksize, paramname) = ANTLR_CAST_API(context->rulememValueOrParamName()->accept(this), SINGLE_ARG(pair<datamemsize_t, string>));
    Version *version = current_version;
    storeComponentParameterResolution("version stacksize", stacksize, paramname, [version](Task *t, ANTLR_ANY_TYPE v) { t->versions(version->id())->local_memory_storage(ANTLR_CAST_API(v, datamemsize_t));});
    return nullptr;
}

ANTLR_ANY_TYPE TeamPlayInputParser::visitRuleversionSettingArch(TeamPlayParser::RuleversionSettingArchContext *context) {
    string arch;
    string paramname;
    tie(arch, paramname) = ANTLR_CAST_API(context->rulestringValueOrParamName()->accept(this), SINGLE_ARG(pair<string, string>));
    Version *version = current_version;
    storeComponentParameterResolution("version arch", arch, paramname, [this, version](Task *t, ANTLR_ANY_TYPE v) {
        string arch = ANTLR_CAST_API(v, std::string);
        bool found = false;

        ArchitectureProperties::ComputeUnitType pclass = ArchitectureProperties::ComputeUnitType::CUT_UNDEFINED;
        try {
            pclass = ArchitectureProperties::from_string(arch);
        }
        catch(...) {}

        // First check processors
        for (ComputeUnit * p : this->out->processors()) {
            if (p->id == arch || p->proc_class == pclass || p->type == arch || boost::algorithm::to_lower_copy(p->type+"."+ArchitectureProperties::to_string(p->proc_class)) == boost::algorithm::to_lower_copy(arch)) {
                t->versions(version->id())->force_mapping_proc().insert(p);
                found = true;
            }
        }

        // Second check coprocessors for possible missing architecture
        if (!found){
            for (ComputeUnit * p : this->out->coprocessors()) {
                if (p->id == arch || p->proc_class == pclass || p->type == arch || p->type+"."+p->id == arch  || boost::algorithm::to_lower_copy(p->type+"."+ArchitectureProperties::to_string(p->proc_class)) == boost::algorithm::to_lower_copy(arch)) {
                    t->force_mapping_proc().insert(p);
                    found = true;
                }
            }
        }


        if (!found)
            throw CompilerException("parser-tey", "Processor name, or architecture class/type \"" + arch + "\" not found.");

    });
    return nullptr;
}

ANTLR_ANY_TYPE TeamPlayInputParser::visitRuleversionSettingResource(TeamPlayParser::RuleversionSettingResourceContext *context) {
    string resource;
    string paramname;
    tie(resource, paramname) = ANTLR_CAST_API(context->rulestringValueOrParamName()->accept(this), SINGLE_ARG(pair<string, string>));
    Version *version = current_version;
    storeComponentParameterResolution("component resource", resource, paramname, [this, version](Task *t, ANTLR_ANY_TYPE v) {
        string resource = ANTLR_CAST_API(v, std::string);
        bool found = false;

        for (ResourceUnit * p : this->out->resources()) {
            if (p->id == resource) {
                t->versions(version->id())->resource_usage().push_back(p);
                found = true;
            }
        }

        if (!found)
            throw CompilerException("parser-tey", "Resource name \"" + resource + "\" not found.");

    });
    return nullptr;
}

ANTLR_ANY_TYPE TeamPlayInputParser::visitRuleversionSettingSecurity(TeamPlayParser::RuleversionSettingSecurityContext *context) {
    uint32_t secu_level;
    string paramname;
    tie(secu_level, paramname) = ANTLR_CAST_API(context->ruleintValueOrParamName()->accept(this), SINGLE_ARG(pair<int64_t, string>));
    Version *version = current_version;
    storeComponentParameterResolution("version security level", secu_level, paramname, [version](Task *t, ANTLR_ANY_TYPE v) { t->versions(version->id())->security_lvl(ANTLR_CAST_API(v, uint32_t));});
    return nullptr;
}

ANTLR_ANY_TYPE TeamPlayInputParser::visitRuleversionSettingCName(TeamPlayParser::RuleversionSettingCNameContext *context) {
    string cname;
    string paramname;
    tie(cname, paramname) = ANTLR_CAST_API(context->rulestringValueOrParamName()->accept(this), SINGLE_ARG(pair<string, string>));
    Version *version = current_version;
    storeComponentParameterResolution("version cname", cname, paramname, [version](Task *t, ANTLR_ANY_TYPE v) { t->versions(version->id())->cname(ANTLR_CAST_API(v, string));});
    return nullptr;
}

ANTLR_ANY_TYPE TeamPlayInputParser::visitRuleversionSettingCSignature(TeamPlayParser::RuleversionSettingCSignatureContext *context) {
    string csignature;
    string paramname;
    tie(csignature, paramname) = ANTLR_CAST_API(context->rulestringValueOrParamName()->accept(this), SINGLE_ARG(pair<string, string>));
    Version *version = current_version;
    storeComponentParameterResolution("version csignature", csignature, paramname, [version](Task *t, ANTLR_ANY_TYPE v) { t->versions(version->id())->csignature(ANTLR_CAST_API(v, string));});
    return nullptr;
}

ANTLR_ANY_TYPE TeamPlayInputParser::visitRuleversionSettingGuard(TeamPlayParser::RuleversionSettingGuardContext *context) {
    throw Todo("Guard on version are not handled, yet");
    return nullptr;
}

ANTLR_ANY_TYPE TeamPlayInputParser::visitRulephaseSettingWCET(TeamPlayParser::RulephaseSettingWCETContext *context) {
    timinginfos_t WCET;
    string paramname;
    tie(WCET, paramname) = ANTLR_CAST_API(context->ruletimeValueOrParamName()->accept(this), SINGLE_ARG(pair<timinginfos_t, string>));
    Version *version = current_version;
    Phase *phase = current_phase;
    storeComponentParameterResolution("phase WCET", WCET, paramname, [version, phase](Task *t, ANTLR_ANY_TYPE v) { t->versions(version->id())->phases(phase->id())->C(ANTLR_CAST_API(v, timinginfos_t));});
    return nullptr;
}

ANTLR_ANY_TYPE TeamPlayInputParser::visitRulephaseSettingWCEC(TeamPlayParser::RulephaseSettingWCECContext *context) {
    energycons_t WCEC;
    string paramname;
    tie(WCEC, paramname) = ANTLR_CAST_API(context->ruleenergyValueOrParamName()->accept(this), SINGLE_ARG(pair<energycons_t, string>));
    Version *version = current_version;
    Phase *phase = current_phase;
    storeComponentParameterResolution("phase WCEC", WCEC, paramname, [version, phase](Task *t, ANTLR_ANY_TYPE v) { t->versions(version->id())->phases(phase->id())->C_E(ANTLR_CAST_API(v, energycons_t));});
    return nullptr;
}

ANTLR_ANY_TYPE TeamPlayInputParser::visitRulephaseSettingCodeSize(TeamPlayParser::RulephaseSettingCodeSizeContext *context) {
    datamemsize_t codesize;
    string paramname;
    tie(codesize, paramname) = ANTLR_CAST_API(context->rulememValueOrParamName()->accept(this), SINGLE_ARG(pair<datamemsize_t, string>));
    Version *version = current_version;
    Phase *phase = current_phase;
    storeComponentParameterResolution("phase codesize", codesize, paramname, [version, phase](Task *t, ANTLR_ANY_TYPE v) { t->versions(version->id())->phases(phase->id())->memory_code_size(ANTLR_CAST_API(v, datamemsize_t));});
    return nullptr;
}

ANTLR_ANY_TYPE TeamPlayInputParser::visitRulephaseSettingStackSize(TeamPlayParser::RulephaseSettingStackSizeContext *context) {
    datamemsize_t stacksize;
    string paramname;
    tie(stacksize, paramname) = ANTLR_CAST_API(context->rulememValueOrParamName()->accept(this), SINGLE_ARG(pair<datamemsize_t, string>));
    Version *version = current_version;
    Phase *phase = current_phase;
    storeComponentParameterResolution("phase stacksize", stacksize, paramname, [version, phase](Task *t, ANTLR_ANY_TYPE v) { t->versions(version->id())->phases(phase->id())->local_memory_storage(ANTLR_CAST_API(v, datamemsize_t));});
    return nullptr;
}

ANTLR_ANY_TYPE TeamPlayInputParser::visitRulephaseSettingArch(TeamPlayParser::RulephaseSettingArchContext *context) {
    string arch;
    string paramname;
    tie(arch, paramname) = ANTLR_CAST_API(context->rulestringValueOrParamName()->accept(this), SINGLE_ARG(pair<string, string>));
    Version *version = current_version;
    Phase *phase = current_phase;
    storeComponentParameterResolution("phase arch", arch, paramname, [this, version, phase](Task *t, ANTLR_ANY_TYPE v) {
        string arch = ANTLR_CAST_API(v, std::string);
        bool found = false;

        ArchitectureProperties::ComputeUnitType pclass = ArchitectureProperties::ComputeUnitType::CUT_UNDEFINED;
        try {
            pclass = ArchitectureProperties::from_string(arch);
        }
        catch(...) {}

        // First check processors
        for (ComputeUnit * p : this->out->processors()) {
            if (p->id == arch || p->proc_class == pclass || p->type == arch || boost::algorithm::to_lower_copy(p->type+"."+ArchitectureProperties::to_string(p->proc_class)) == boost::algorithm::to_lower_copy(arch)) {
                t->versions(version->id())->phases(phase->id())->force_mapping_proc().insert(p);
                found = true;
            }
        }

        // Second check coprocessors for possible missing architecture
        if (!found){
            for (ComputeUnit * p : this->out->coprocessors()) {
                if (p->id == arch || p->proc_class == pclass || p->type == arch || p->type+"."+p->id == arch  || boost::algorithm::to_lower_copy(p->type+"."+ArchitectureProperties::to_string(p->proc_class)) == boost::algorithm::to_lower_copy(arch)) {
                    t->force_mapping_proc().insert(p);
                    found = true;
                }
            }
        }


        if (!found)
            throw CompilerException("parser-tey", "Processor name, or architecture class/type \"" + arch + "\" not found.");

    });
    return nullptr;
}

ANTLR_ANY_TYPE TeamPlayInputParser::visitRulephaseSettingResource(TeamPlayParser::RulephaseSettingResourceContext *context) {
    string resource;
    string paramname;
    tie(resource, paramname) = ANTLR_CAST_API(context->rulestringValueOrParamName()->accept(this), SINGLE_ARG(pair<string, string>));
    Version *version = current_version;
    Phase *phase = current_phase;
    storeComponentParameterResolution("component resource", resource, paramname, [this, version, phase](Task *t, ANTLR_ANY_TYPE v) {
        string resource = ANTLR_CAST_API(v, std::string);
        bool found = false;

        for (ResourceUnit * p : this->out->resources()) {
            if (p->id == resource) {
                t->versions(version->id())->phases(phase->id())->resource_usage().push_back(p);
                found = true;
            }
        }

        if (!found)
            throw CompilerException("parser-tey", "Resource name \"" + resource + "\" not found.");

    });
    return nullptr;
}

ANTLR_ANY_TYPE TeamPlayInputParser::visitRulephaseSettingCName(TeamPlayParser::RulephaseSettingCNameContext *context) {
    string cname;
    string paramname;
    tie(cname, paramname) = ANTLR_CAST_API(context->rulestringValueOrParamName()->accept(this), SINGLE_ARG(pair<string, string>));
    Version *version = current_version;
    Phase *phase = current_phase;
    storeComponentParameterResolution("phase cname", cname, paramname, [version, phase](Task *t, ANTLR_ANY_TYPE v) { t->versions(version->id())->phases(phase->id())->cname(ANTLR_CAST_API(v, string));});
    return nullptr;
}

ANTLR_ANY_TYPE TeamPlayInputParser::visitRulephaseSettingCSignature(TeamPlayParser::RulephaseSettingCSignatureContext *context) {
    string csignature;
    string paramname;
    tie(csignature, paramname) = ANTLR_CAST_API(context->rulestringValueOrParamName()->accept(this), SINGLE_ARG(pair<string, string>));
    Version *version = current_version;
    Phase *phase = current_phase;
    storeComponentParameterResolution("phase csignature", csignature, paramname, [version, phase](Task *t, ANTLR_ANY_TYPE v) { t->versions(version->id())->phases(phase->id())->csignature(ANTLR_CAST_API(v, string));});
    return nullptr;
}

ANTLR_ANY_TYPE TeamPlayInputParser::visitRuleprofile(TeamPlayParser::RuleprofileContext *context) {
    Profile *p = new Profile();
    profiles[context->current.name] = p;
    current_profile = p;
    
    for(TeamPlayParser::RuleprofileFTSettingsContext *setting : context->ruleprofileFTSettings())
        setting->accept(this);

    current_profile = nullptr;
    return nullptr;
}

ANTLR_ANY_TYPE TeamPlayInputParser::visitRuleprofileFTSettings(TeamPlayParser::RuleprofileFTSettingsContext *context) {
    string fftype = (!context->current.type.empty()) ? context->current.type : "";
    
    for(TeamPlayParser::RulecheckpointSettingContext * options : context->rulecheckpointSetting()) {
        options->accept(this);
        current_profile->add("checkpoint", "true", fftype);
        current_profile->addModifier("checkpoint", fftype);
    }
    for(TeamPlayParser::RulestandbySettingContext * options : context->rulestandbySetting()) {
        options->accept(this);
        current_profile->add("standby", "true", fftype);
        current_profile->addModifier("standby", fftype);
    }
    for(TeamPlayParser::RulenVersionSettingContext * options : context->rulenVersionSetting()) {
        options->accept(this);
        current_profile->add("nVersion", "true", fftype);
        current_profile->addModifier("nVersion", fftype);
    }
    for(TeamPlayParser::RulenModularSettingContext * options : context->rulenModularSetting()) {
        options->accept(this);
        current_profile->add("nModular", "true", fftype);
        current_profile->addModifier("nModular", fftype);
    }
    return nullptr;
}

ANTLR_ANY_TYPE TeamPlayInputParser::visitRulenModularSettingRep(TeamPlayParser::RulenModularSettingRepContext *context) {
    int replicas;
    string paramname;
    tie(replicas, paramname) = ANTLR_CAST_API(context->ruleintValueOrParamName()->accept(this), SINGLE_ARG(pair<int64_t,string>));
    
    storeProfileParameterResolution("nModular_replicas", context->current.type, replicas, paramname, [](Profile *p, const string & key, const string &modifier, ANTLR_ANY_TYPE v) {
        p->add(key, to_string(ANTLR_CAST_API(v, int)));
        p->addModifier(key, modifier);
    });
    return nullptr;
}
ANTLR_ANY_TYPE TeamPlayInputParser::visitRulenModularSettingvotRep(TeamPlayParser::RulenModularSettingvotRepContext *context) {
    int voting;
    string paramname;
    tie(voting, paramname) = ANTLR_CAST_API(context->ruleintValueOrParamName()->accept(this), SINGLE_ARG(pair<int64_t,string>));
    
    storeProfileParameterResolution("nModular_votingReplicas", context->current.type, voting, paramname, [](Profile *p, const string & key, const string &modifier, ANTLR_ANY_TYPE v) {
        p->add(key, to_string(ANTLR_CAST_API(v, int)));
        p->addModifier(key, modifier);
    });
    return nullptr;
}
ANTLR_ANY_TYPE TeamPlayInputParser::visitRulenModularSettingWT(TeamPlayParser::RulenModularSettingWTContext *context) {
    int waiting;
    string paramname;
    tie(waiting, paramname) = ANTLR_CAST_API(context->rulepercentValueOrParamName()->accept(this), SINGLE_ARG(pair<int,string>));
    
    storeProfileParameterResolution("nModular_waitingTime", context->current.type, waiting, paramname, [](Profile *p, const string & key, const string &modifier, ANTLR_ANY_TYPE v) {
        p->add(key, to_string(ANTLR_CAST_API(v, int)));
        p->addModifier(key, modifier);
    });
    return nullptr;
}
ANTLR_ANY_TYPE TeamPlayInputParser::visitRulenModularSettingWS(TeamPlayParser::RulenModularSettingWSContext *context) {
    string waiting_start;
    string paramname;
    tie(waiting_start, paramname) = ANTLR_CAST_API(context->rulewaitStartValueOrParamName()->accept(this), SINGLE_ARG(pair<string,string>));
    
    storeProfileParameterResolution("nModular_waitingStart", context->current.type, waiting_start, paramname, [](Profile *p, const string & key, const string &modifier, ANTLR_ANY_TYPE v) {
        p->add(key, ANTLR_CAST_API(v, string));
        p->addModifier(key, modifier);
    });
    return nullptr;
}
ANTLR_ANY_TYPE TeamPlayInputParser::visitRulestandbySettingRep(TeamPlayParser::RulestandbySettingRepContext *context) {
    int replicas;
    string paramname;
    tie(replicas, paramname) = ANTLR_CAST_API(context->ruleintValueOrParamName()->accept(this), SINGLE_ARG(pair<int64_t,string>));
    
    storeProfileParameterResolution("standby_replicas", context->current.type, replicas, paramname, [](Profile *p, const string & key, const string &modifier, ANTLR_ANY_TYPE v) {
        p->add(key, to_string(ANTLR_CAST_API(v, int)));
        p->addModifier(key, modifier);
    });
    return nullptr;
}
ANTLR_ANY_TYPE TeamPlayInputParser::visitRulestandbySettingSync(TeamPlayParser::RulestandbySettingSyncContext *context) {
    string sync;
    string paramname;
    tie(sync, paramname) = ANTLR_CAST_API(context->rulesyncValueOrParamName()->accept(this), SINGLE_ARG(pair<string,string>));
    
    storeProfileParameterResolution("standby_synchronization", context->current.type, sync, paramname, [](Profile *p, const string & key, const string &modifier, ANTLR_ANY_TYPE v) {
        p->add(key, ANTLR_CAST_API(v, string));
        p->addModifier(key, modifier);
    });
    return nullptr;
}
ANTLR_ANY_TYPE TeamPlayInputParser::visitRulecheckpointSetting(TeamPlayParser::RulecheckpointSettingContext *context) {
    bool shrinking;
    string paramname;
    tie(shrinking, paramname) = ANTLR_CAST_API(context->ruleboolValueOrParamName()->accept(this), SINGLE_ARG(pair<bool,string>));
    
    storeProfileParameterResolution("checkpoint_shrinking", context->current.type, shrinking, paramname, [](Profile *p, const string & key, const string &modifier, ANTLR_ANY_TYPE v) {
        p->add(key, ANTLR_CAST_API(v, bool) ? "true" : "false");
        p->addModifier(key, modifier);
    });
    return nullptr;
}
ANTLR_ANY_TYPE TeamPlayInputParser::visitRulenVersionSetting(TeamPlayParser::RulenVersionSettingContext *context) {
    int nversion;
    string paramname;
    tie(nversion, paramname) = ANTLR_CAST_API(context->ruleintValueOrParamName()->accept(this), SINGLE_ARG(pair<int64_t,string>));
    
    storeProfileParameterResolution("nVersion_versions_"+context->current.version, context->current.type, nversion, paramname, [](Profile *p, const string & key, const string &modifier, ANTLR_ANY_TYPE v) {
        p->add(key, to_string(ANTLR_CAST_API(v, int)));
        p->addModifier(key, modifier);
    });
    return nullptr;
}

ANTLR_ANY_TYPE TeamPlayInputParser::visitRulechannel(TeamPlayParser::RulechannelContext *context) {
    Connector::type_e src_ctype, related_output_type;
    vector<pair < Task *, Connector *>> src, dst;

    tie(src_ctype, src) = ANTLR_CAST_API(context->ruleportOutSpec()->accept(this), SINGLE_ARG(pair<Connector::type_e, vector<pair < Task *, Connector*>>>));
    tie(related_output_type, dst) = ANTLR_CAST_API(context->ruleportInSpec()->accept(this), SINGLE_ARG(pair<Connector::type_e, vector<pair < Task *, Connector*>>>));

    vector<Channel*> *chans = new vector<Channel*>;
    for (pair<Task *, Connector *> & i : src) {
        i.second->type(related_output_type);
        
        for (pair<Task *, Connector *> & j : dst) {
            j.second->type(src_ctype);
            
            Channel *chan = new Channel(i.first, j.first, i.second, j.second);
            chans->push_back(chan);
        }
    }

    return chans;
}

ANTLR_ANY_TYPE TeamPlayInputParser::visitRuleappSettingChannels(TeamPlayParser::RuleappSettingChannelsContext *context) {
    for(TeamPlayParser::RulechannelContext *c: context->rulechannel()) {
        vector<Channel*> *chans = ANTLR_CAST_API(c->accept(this), SINGLE_ARG(vector<Channel*>*));
        for(Channel *chan : *chans) 
            out->add_dependency(chan->src, chan->sink, chan->csrc, chan->csink);
//        delete [] chans;
    }
    return nullptr;
}

ANTLR_ANY_TYPE TeamPlayInputParser::visitRuletemplateSettingChannels(TeamPlayParser::RuletemplateSettingChannelsContext *context) {
    for(TeamPlayParser::RulechannelContext *c: context->rulechannel()) {
        vector<Channel*> *chans = ANTLR_CAST_API(c->accept(this), SINGLE_ARG(vector<Channel*>*));
        current_template.top()->channels.insert(current_template.top()->channels.end(), chans->begin(), chans->end());
        delete chans;
    }
    return nullptr;
}

ANTLR_ANY_TYPE TeamPlayInputParser::visitRuleportDecl(TeamPlayParser::RuleportDeclContext *context) {
    uint32_t tokens = 1;
    
    Connector *c = new Connector(context->current.name, tokens, context->current.type, context->current.tainted);
    if(conf->datatypes.count(context->current.type) == 0)
        throw CompilerException("TeamPlay parser", "Unknown type "+context->current.type+" for connector "+context->current.name);
    
    if(context->ruleintValueOrParamName() != nullptr) {
        string paramname;
        tie(tokens, paramname) = ANTLR_CAST_API(context->ruleintValueOrParamName()->accept(this), SINGLE_ARG(pair<int64_t, string>));
        storeComponentParameterResolution("input token", tokens, paramname, [c](Task *t, ANTLR_ANY_TYPE v) {
            c->tokens(ANTLR_CAST_API(v, uint32_t));
        });
    }
    
    return c;
}

ANTLR_ANY_TYPE TeamPlayInputParser::visitRuleportRef(TeamPlayParser::RuleportRefContext *context) {
    string task_name = context->current.comp;
    Task * t;
    bool subgraph_inner_connection = false;
    if(task_name == "in" || task_name == "out") {
        if(current_template.empty())
            throw CompilerException("parser-tey", "Reserved keyword in/out used in channel outside of a template");
        //this is a connector to get in/out of a template/subgraph
        t = current_template.top();
        subgraph_inner_connection = true;
    }
    else
        t = out->task(task_name);
    
    if(t == nullptr) {
        if(!current_template.empty()) {
            t = current_template.top()->find_components(task_name);
        }
        if(t == nullptr)
            throw CompilerException("parser-tey", "Task " + task_name + " does not exist.");
    }

    return make_tuple<Task *, string, bool>(move(t), move(context->current.port), move(subgraph_inner_connection));
}

ANTLR_ANY_TYPE TeamPlayInputParser::visitRuleportInRef(TeamPlayParser::RuleportInRefContext *context) {
    Task * t;
    string p;
    Connector * r = nullptr;
    bool subgraph_inner_connection = false;
    
    tie(t, p, subgraph_inner_connection) = ANTLR_CAST_API(context->ruleportRef()->accept(this), SINGLE_ARG(tuple < Task *, string, bool >));
    
    if (context->ruleguard() != nullptr)
        context->ruleguard()->accept(this);
    
    vector<Connector*> connectors = (subgraph_inner_connection) ? t->outputs() : t->inputs(); 
    
    if(p.empty()) {
        if(connectors.size() != 1) {
            throw CompilerException("parser-tey", "Lack of inport name on task " + t->id() + " is ambiguous.");
        }
        r = connectors[0];
    }
    else {
        vector<Connector*>::iterator it = find_if(connectors.begin(), connectors.end(), [p](Connector *c) { return c->id() == p; });
        if(it == connectors.end())
            throw CompilerException("parser-tey", "Task " + t->id() + " does not have inport " + p);
        r = *it;
    }
    
    return make_pair<Task *, Connector *>(move(t), move(r));
}

ANTLR_ANY_TYPE TeamPlayInputParser::visitRuleportOutRef(TeamPlayParser::RuleportOutRefContext *context) {
    Task * t;
    string p;
    Connector * r = nullptr;
    bool subgraph_inner_connection = false;
    
    tie(t, p, subgraph_inner_connection) = ANTLR_CAST_API(context->ruleportRef()->accept(this), SINGLE_ARG(tuple < Task *, string, bool >));

    if (context->ruleguard() != nullptr)
        context->ruleguard()->accept(this);
    
    vector<Connector*> connectors = (subgraph_inner_connection) ? t->inputs() : t->outputs(); 

    if(p.empty()) {
        if(connectors.size() != 1)
            throw CompilerException("parser-tey", "Lack of outport name on task " + t->id() + " is ambiguous.");

        r = connectors[0];
    }
    else {
        vector<Connector*>::iterator it = find_if(connectors.begin(), connectors.end(), [p](Connector *c) { return c->id() == p; });
        if(it == connectors.end())
            throw CompilerException("parser-tey", "Task " + t->id() + " does not have outport " + p);
        r = *it;
    }

    return make_pair<Task *, Connector *>(move(t), move(r));
}

ANTLR_ANY_TYPE TeamPlayInputParser::visitRuleportOutSpec(TeamPlayParser::RuleportOutSpecContext *context) {
    if(context->ruleportOutSpec() != nullptr) {
        throw CompilerException("parser-tey", "Multiple AND or OR source ports are not yet supported.");
    }

    vector<pair < Task *, Connector*>> rv;

    rv.push_back(ANTLR_CAST_API(context->ruleportOutRef()->accept(this), SINGLE_ARG(pair < Task *, Connector *>)));

    return make_pair<Connector::type_e, vector<pair < Task *, Connector*>>>(
        Connector::type_e::REGULAR,
        move(rv)
    );
}

ANTLR_ANY_TYPE TeamPlayInputParser::visitRuleportInSpec(TeamPlayParser::RuleportInSpecContext *context) {
    vector<pair < Task *, Connector*>> rv;
    Connector::type_e related_output_type = Connector::type_e::REGULAR;

    rv.push_back(ANTLR_CAST_API(context->ruleportInRef()->accept(this), SINGLE_ARG(pair < Task *, Connector *>)));

    if(context->ruleportInSpec() != nullptr) {
        if(context->current.op == "&") {
            related_output_type = Connector::type_e::DUPLICATE;
        }
        else if(context->current.op == "|") {
            throw CompilerException("parser-tey", "OR destination edges are not yet supported.");
        }
        else {
            throw CompilerException("parser-tey", "Impossible port sequencing specification!");
        }

        vector<pair < Task *, Connector*>> rhs_rv;
        Connector::type_e rhs_related_output_type;

        tie(rhs_related_output_type, rhs_rv) = ANTLR_CAST_API(context->ruleportInSpec()->accept(this), SINGLE_ARG(pair < Connector::type_e, vector<pair < Task *, Connector*>>>));

        if(related_output_type != rhs_related_output_type && rhs_related_output_type != Connector::type_e::REGULAR) {
            throw runtime_error("Cannot combine AND and OR ports in destination spec.");
        }

        rv.insert(rv.end(), rhs_rv.begin(), rhs_rv.end());
    }

    return make_pair<Connector::type_e, vector<pair < Task *, Connector*>>>(
            move(related_output_type),
            move(rv)
            );
}

ANTLR_ANY_TYPE TeamPlayInputParser::visitRuletimeValue(TeamPlayParser::RuletimeValueContext *context) {
    return Utils::stringtime_to_time(context->current.val+" "+context->current.unit, this->out->time_unit());
}

ANTLR_ANY_TYPE TeamPlayInputParser::visitRuleenergyValue(TeamPlayParser::RuleenergyValueContext *context) {
    return Utils::stringenergy_to_joule(context->current.entier+"."+context->current.mantisse+" "+context->current.unit);
}

ANTLR_ANY_TYPE TeamPlayInputParser::visitRulememValue(TeamPlayParser::RulememValueContext *context) {
    return Utils::stringmem_to_bit(context->current.val+" "+context->current.unit);
}

ANTLR_ANY_TYPE TeamPlayInputParser::visitRuleintValue(TeamPlayParser::RuleintValueContext *context) {
    return stol(context->current.val);
}
ANTLR_ANY_TYPE TeamPlayInputParser::visitRuleboolValue(TeamPlayParser::RuleboolValueContext *context) {
    return context->current.val == "true"; 
}

ANTLR_ANY_TYPE TeamPlayInputParser::visitRulestringValue(TeamPlayParser::RulestringValueContext *context) {
    string s = context->current.val;
    return s.substr(1, s.length() - 2);
}
ANTLR_ANY_TYPE TeamPlayInputParser::visitRulewaitStartValue(TeamPlayParser::RulewaitStartValueContext *context) {
    return context->current.val;
}
ANTLR_ANY_TYPE TeamPlayInputParser::visitRulesyncValue(TeamPlayParser::RulesyncValueContext *context) {
    return context->current.val;
}

ANTLR_ANY_TYPE TeamPlayInputParser::visitRuletimeValueOrParamName(TeamPlayParser::RuletimeValueOrParamNameContext *context) {
    string paramname = (context->ruleparamNameValue() != nullptr) ? ANTLR_CAST_API(context->ruleparamNameValue()->accept(this), string) : "";
    timinginfos_t val = (context->ruletimeValue() != nullptr) ? ANTLR_CAST_API(context->ruletimeValue()->accept(this), timinginfos_t) : 0;
    return make_pair(val, paramname);
}
ANTLR_ANY_TYPE TeamPlayInputParser::visitRuleenergyValueOrParamName(TeamPlayParser::RuleenergyValueOrParamNameContext *context) {
    string paramname = (context->ruleparamNameValue() != nullptr) ? ANTLR_CAST_API(context->ruleparamNameValue()->accept(this), string) : "";
    energycons_t val = (context->ruleenergyValue() != nullptr) ? ANTLR_CAST_API(context->ruleenergyValue()->accept(this), energycons_t) : 0;
    return make_pair(val, paramname);
}
ANTLR_ANY_TYPE TeamPlayInputParser::visitRulememValueOrParamName(TeamPlayParser::RulememValueOrParamNameContext *context) {
    string paramname = (context->ruleparamNameValue() != nullptr) ? ANTLR_CAST_API(context->ruleparamNameValue()->accept(this), string) : "";
    datamemsize_t val = (context->rulememValue() != nullptr) ? ANTLR_CAST_API(context->rulememValue()->accept(this), datamemsize_t) : 0;
    return make_pair(val, paramname);
}
ANTLR_ANY_TYPE TeamPlayInputParser::visitRuleintValueOrParamName(TeamPlayParser::RuleintValueOrParamNameContext *context) {
    string paramname = (context->ruleparamNameValue() != nullptr) ? ANTLR_CAST_API(context->ruleparamNameValue()->accept(this), string) : "";
    int64_t val = (context->ruleintValue() != nullptr) ? ANTLR_CAST_API(context->ruleintValue()->accept(this), int64_t) : 0;
    return make_pair(val, paramname);
}
ANTLR_ANY_TYPE TeamPlayInputParser::visitRulepercentValueOrParamName(TeamPlayParser::RulepercentValueOrParamNameContext *context) {
    string paramname = (context->ruleparamNameValue() != nullptr) ? ANTLR_CAST_API(context->ruleparamNameValue()->accept(this), string) : "";
    int64_t val = (context->rulepercentValue() != nullptr) ? ANTLR_CAST_API(context->rulepercentValue()->accept(this), int64_t) : 0;
    return make_pair(val, paramname);
}
ANTLR_ANY_TYPE TeamPlayInputParser::visitRulestringValueOrParamName(TeamPlayParser::RulestringValueOrParamNameContext *context) {
    string paramname = (context->ruleparamNameValue() != nullptr) ? ANTLR_CAST_API(context->ruleparamNameValue()->accept(this), string) : "";
    string val = (context->rulestringValue() != nullptr) ? ANTLR_CAST_API(context->rulestringValue()->accept(this), string) : "";
    return make_pair(val, paramname);
}
ANTLR_ANY_TYPE TeamPlayInputParser::visitRuleboolValueOrParamName(TeamPlayParser::RuleboolValueOrParamNameContext *context) {
    string paramname = (context->ruleparamNameValue() != nullptr) ? ANTLR_CAST_API(context->ruleparamNameValue()->accept(this), string) : "";
    bool val = (context->ruleboolValue() != nullptr) ? ANTLR_CAST_API(context->ruleboolValue()->accept(this), bool) : false;
    return make_pair(val, paramname);
}
ANTLR_ANY_TYPE TeamPlayInputParser::visitRulewaitStartValueOrParamName(TeamPlayParser::RulewaitStartValueOrParamNameContext *context) {
    string paramname = (context->ruleparamNameValue() != nullptr) ? ANTLR_CAST_API(context->ruleparamNameValue()->accept(this), string) : "";
    string val = (context->rulewaitStartValue() != nullptr) ? ANTLR_CAST_API(context->rulewaitStartValue()->accept(this), string) : 0;
    return make_pair(val, paramname);
}
ANTLR_ANY_TYPE TeamPlayInputParser::visitRulesyncValueOrParamName(TeamPlayParser::RulesyncValueOrParamNameContext *context) {
    string paramname = (context->ruleparamNameValue() != nullptr) ? ANTLR_CAST_API(context->ruleparamNameValue()->accept(this), string) : "";
    string val = (context->rulesyncValue() != nullptr) ? ANTLR_CAST_API(context->rulesyncValue()->accept(this), string) : 0;
    return make_pair(val, paramname);
}
ANTLR_ANY_TYPE TeamPlayInputParser::visitRuleparamNameValue(TeamPlayParser::RuleparamNameValueContext *context) {
    return context->current.param;
}

void TeamPlayInputParser::resolveTemplateInstanciation(TemplateInstance *tpl_inst, map<Task*, Task*> *tasks_to_add, vector<Channel*> *chans_to_add) {
    
    // resolve nested template instanciation: a template instanciating a template
    // to get the list of all nested final components/channels
    for(TemplateInstance *sub_tpl_inst : tpl_inst->tpl_instance) {
        map<Task*, Task*> tasks;
        vector<Channel*> chans;
        resolveTemplateInstanciation(sub_tpl_inst, &tasks, &chans);
        
        for(Task *t : Utils::mapvalues(tasks))
            tpl_inst->components.push_back(t);
        for(Channel *c : chans) 
            tpl_inst->channels.push_back(c);
        
        for(Task::dep_t el : sub_tpl_inst->predecessor_tasks()) {
            vector<Channel*>::iterator it = find_if(tpl_inst->channels.begin(), tpl_inst->channels.end(), [el, sub_tpl_inst](Channel *a) { return a->src == el.first && a->sink == sub_tpl_inst; });
            tpl_inst->channels.erase(it);
        }
        for(Task::dep_t el : sub_tpl_inst->successor_tasks())  {
            vector<Channel*>::iterator it = find_if(tpl_inst->channels.begin(), tpl_inst->channels.end(), [el, sub_tpl_inst](Channel *a) { return a->src == sub_tpl_inst && a->sink == el.first; });
            tpl_inst->channels.erase(it);
        }
        
        vector<Task*>::iterator it = find(tpl_inst->components.begin(), tpl_inst->components.end(), sub_tpl_inst);
        tpl_inst->components.erase(it);
        
        //delete tpl_inst;
    }
    
    //delete [] tpl_inst->tpl_instance
    
    
    for(TemplateInstance *sub_tpl_inst : tpl_inst->subgraphs) {
        map<Task*, Task*> tasks;
        vector<Channel*> chans;
        resolveSubgraphs(sub_tpl_inst, &tasks, &chans);
        
        for(Task *t : Utils::mapvalues(tasks))
            tpl_inst->components.push_back(t);
        for(Channel *c : chans) 
            tpl_inst->channels.push_back(c);
        
        for(Task::dep_t el : sub_tpl_inst->predecessor_tasks()) {
            vector<Channel*>::iterator it = find_if(tpl_inst->channels.begin(), tpl_inst->channels.end(), [el, sub_tpl_inst](Channel *a) { return a->src == el.first && a->sink == sub_tpl_inst; });
            tpl_inst->channels.erase(it);
        }
        for(Task::dep_t el : sub_tpl_inst->successor_tasks())  {
            vector<Channel*>::iterator it = find_if(tpl_inst->channels.begin(), tpl_inst->channels.end(), [el, sub_tpl_inst](Channel *a) { return a->src == sub_tpl_inst && a->sink == el.first; });
            tpl_inst->channels.erase(it);
        }
        
        vector<Task*>::iterator it = find(tpl_inst->components.begin(), tpl_inst->components.end(), sub_tpl_inst);
        tpl_inst->components.erase(it);
        
        //delete tpl_inst;
    }
    
    // Duplicate inner components
    for(Task *t : tpl_inst->components) {
        Task *nt = new Task(tpl_inst->id()+"_"+t->id());
        (*tasks_to_add)[t] = nt;
        
        for(string prof_name : profile_settings[tpl_inst])
            profile_settings[nt].push_back(prof_name);
        for(string prof_name : profile_settings[t])
            profile_settings[nt].push_back(prof_name);
        
        nt->clone(tpl_inst); //add default properties from template
        nt->clone(t); //add specific properties from task
        
        for(Connector *el : t->inputs()) {
            Connector *c = new Connector(*el);
            nt->inputs().push_back(c);
        }
        for(Connector *el : t->outputs()) {
            Connector *c = new Connector(*el);
            nt->outputs().push_back(c);
        }
    }
    
    // Reconnect inner components
    for(Channel *chan : tpl_inst->channels) {
        if(chan->src == tpl_inst) continue;
        if(chan->sink == tpl_inst) continue;
        Task *src = (*tasks_to_add)[chan->src];
        Task *sink = (*tasks_to_add)[chan->sink];
        chans_to_add->push_back(new Channel(src, sink, src->output(chan->csrc->id()), sink->input(chan->csink->id())));
    }
    
    // Connect incoming channels from the outside to inner components
    for(Task::dep_t elp : tpl_inst->predecessor_tasks()) {
        for (Connection *conn : elp.second) {
            Connector *inport = conn->to();
            Connector *outport = conn->from();
            for (Channel *chan: tpl_inst->channels) {
                if (chan->src == tpl_inst && chan->csrc->id() == inport->id()) {
                    chans_to_add->push_back(new Channel(elp.first, (*tasks_to_add)[chan->sink],
                            outport, (*tasks_to_add)[chan->sink]->input(chan->csink->id())));
                }
            }
        }
    }
    // Connect outgoing channels from inner components to the ouside
    for(Task::dep_t els : tpl_inst->successor_tasks()) {
        for(Channel *chan : tpl_inst->channels) {
            for (Connection *conn : els.second) {
                Connector *inport = conn->to();
                Connector *outport = conn->from();
                if (chan->sink == tpl_inst && chan->csink->id() == outport->id()) {
                    chans_to_add->push_back(
                            new Channel((*tasks_to_add)[chan->src], els.first, (*tasks_to_add)[chan->src]->output(chan->csrc->id()), inport));
                }
            }
        }
    }
    
    // delete [] tpl_inst->components
    // delete [] tpl_inst->channels
}
void TeamPlayInputParser::resolveSubgraphs(TemplateInstance *tpl_inst, map<Task*, Task*> *tasks_to_add, std::vector<Channel*> *chans_to_add) {
    resolveTemplateInstanciation(tpl_inst, tasks_to_add, chans_to_add);
    
    // Connect incoming channels from the outside to inner components
    for(Task::dep_t elp : tpl_inst->parent->predecessor_tasks()) {
        for(Channel *chan : tpl_inst->channels) {
            for (Connection *conn : elp.second) {
                Connector *inport = conn->to();
                Connector *outport = conn->from();
                if (chan->src == tpl_inst && chan->csrc->id() == inport->id()) {
                    chans_to_add->push_back(new Channel(elp.first, (*tasks_to_add)[chan->sink], outport,
                            (*tasks_to_add)[chan->sink]->input(chan->csink->id())));
                }
            }
        }
    }
    // Connect outgoing channels from inner components to the ouside
    for (Task::dep_t els: tpl_inst->parent->successor_tasks()) {
        for (Connection *conn: els.second) {
            Connector *inport = conn->to();
            Connector *outport = conn->from();
            for (Channel *chan: tpl_inst->channels) {
                if (chan->sink == tpl_inst && chan->csink->id() == outport->id()) {
                    chans_to_add->push_back(new Channel((*tasks_to_add)[chan->src], els.first, (*tasks_to_add)[chan->src]->output(chan->csrc->id()), inport));
                }
            }
        }
    }
}

void TeamPlayInputParser::resolveProfile(Task *t, const string &prof_name) {
    
    if(prof_name.at(0) == '!') {
        string prop = prof_name.substr(1,prof_name.size());
        if(profiles.count(prop))
            t->profile().remove(*profiles[prop]);
        else
            t->profile().remove(prop);
        return;
    }
         
    if(profiles.count(prof_name) == 0)
        throw CompilerException("parser-tey", "Unknown profile "+prof_name+" to apply to task "+t->id());
    Profile *p = profiles[prof_name];
    t->profile().merge(*p);
}

void TeamPlayInputParser::storeComponentParameterResolution(const string &desc, ANTLR_ANY_TYPE value, const string &paramname, function<void(Task *, ANTLR_ANY_TYPE)> f) {
    if(paramname.empty()) {
        f(current_component.top(), move(value));
    }
    else {
        if(current_template.empty())
            throw CompilerException("parser-tey", "You are trying to set a variable `"+desc+"' name outside of a template");
        Template *tpl = current_template.top();
        if(find(tpl->params.begin(), tpl->params.end(), paramname) == tpl->params.end())
            throw CompilerException("parser-tey", "Unknown parameter "+paramname+" in template "+tpl->id());
        
        tpl->param_to_apply_component[paramname].push_back(f);
    }
}

void TeamPlayInputParser::storeProfileParameterResolution(const string &key, const string &type, ANTLR_ANY_TYPE value, const string &paramname, function<void(Profile *, const string &, const string &, ANTLR_ANY_TYPE)> f) {
    if(paramname.empty()) {
        f(current_profile, move(key), move(type), move(value));
    }
    else {
        if(current_template.empty())
            throw CompilerException("parser-tey", "You are trying to set a variable `"+key+"' name outside of a template");
        Template *tpl = current_template.top();
        if(find(tpl->params.begin(), tpl->params.end(), paramname) == tpl->params.end())
            throw CompilerException("parser-tey", "Unknown parameter "+paramname+" in template "+tpl->id());
        
        current_profile->add(key, "__deferred__");
        tpl->param_to_apply_profile[paramname].push_back([key,type,f](Profile *p, ANTLR_ANY_TYPE v) {
            if(p->get(key) == "__deferred__")
                f(p, key, type, v);
        });
    }
}

void TeamPlayInputParser::Template::clone(Template *rhs) {
    deep_clone(rhs);

    tpl_instance.clear();
    for(TemplateInstance *tpl_inst : rhs->tpl_instance) {
        TemplateInstance *nt = new TemplateInstance(tpl_inst->id());
        nt->clone(tpl_inst);
        tpl_instance.push_back(nt);
    }

    subgraphs.clear();
    for(TemplateInstance *tpl_inst : rhs->subgraphs) {
        TemplateInstance *nt = new TemplateInstance(tpl_inst->id());
        nt->clone(tpl_inst);
        subgraphs.push_back(nt);
    }

    components.clear();
    for(Task *t : rhs->components) {
        Task *nt = new Task(t->id());
        nt->deep_clone(t);
        components.push_back(nt);
    }
    params.clear();
    params.insert(params.end(), rhs->params.begin(), rhs->params.end());

    channels.clear();
    for(Channel *c : rhs->channels) {
        Channel *nc = new Channel;
        if(c->src == rhs) {
            nc->src = this;
            nc->csrc = input(c->csrc->id());
        }
        else {
            nc->src = *(find_if(components.begin(), components.end(), [c](Task* a) { return a->id() == c->src->id(); }));
            nc->csrc = nc->src->output(c->csrc->id());
        }
        if(c->sink == rhs) {
            nc->sink = this;
            nc->csink = output(c->csink->id());
        }
        else {
            nc->sink = *(find_if(components.begin(), components.end(), [c](Task* a) { return a->id() == c->sink->id(); }));
            nc->csink = nc->sink->input(c->csink->id());
        }
        channels.push_back(nc);
    }

    param_to_apply_component.clear();
    param_to_apply_component.insert(rhs->param_to_apply_component.begin(), rhs->param_to_apply_component.end());
    param_to_apply_profile.clear();
    param_to_apply_profile.insert(rhs->param_to_apply_profile.begin(), rhs->param_to_apply_profile.end());
}

/*
 * Copyright (C) 2020 Stephen Nicholas Swatman <s.n.swatman@uva.nl>
 *                    Univeristy of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TEAMPLAYNEWCOORDINATION_H
#define TEAMPLAYNEWCOORDINATION_H

#include <stack>

#include "antlr4-runtime.h"
#include "TeamPlayParser.h"
#include "TeamPlayLexer.h"
#include "TeamPlayBaseVisitor.h"

#include "InputParser.hpp"
#include "SystemModel.hpp"

/**
 * Parser for the language TeamPlay
 *
 * Create the IR representing the #SystemModel
 * This parser uses the lexer generated from ANTLR, following the Visitor design pattern.
 * ANTLR creates an AST which is then scanned. On each element of the AST,
 * the specific visitor function is called.
 */
class TeamPlayInputParser : public InputParser, public TeamPlayBaseVisitor {
protected:
        virtual void parse(std::istream &input) override;

public:
    /**
     * Constructor
     * @param o #SystemModel
     * @param c current config
     * @param cmd_properties command line properties
     */
    TeamPlayInputParser(SystemModel *o, config_t *c, CommandLineProperties *cmd_properties);

    /**
     * Visit the top-level element.
     *
     * DSL keyword: app
     *
     * @param context
     * @return nullptr
     */
//    virtual ANTLR_ANY_TYPE visitRuleapp(TeamPlayParser::RuleappContext *context) override;
    /**
     * Add task/component
     *
     * DSL keyword: app -> components
     *
     * @param context
     * @return nullptr
     */
    virtual ANTLR_ANY_TYPE visitRuleappSettingComponents(TeamPlayParser::RuleappSettingComponentsContext *context) override;
    
    virtual ANTLR_ANY_TYPE visitRuleappSettingChannels(TeamPlayParser::RuleappSettingChannelsContext *context) override;
    
    /**
     * Add task/component
     *
     * DSL keyword: app -> components
     *
     * @param context
     * @return nullptr
     */
    virtual ANTLR_ANY_TYPE visitRulesubgraphComponentSettings(TeamPlayParser::RulesubgraphComponentSettingsContext *context) override;

    /**
     * Attach the global deadline
     *
     * DSL keyword: app -> deadline
     *
     * @param context
     * @return nullptr
     */
    virtual ANTLR_ANY_TYPE visitRuleappSettingDeadline(TeamPlayParser::RuleappSettingDeadlineContext *context) override;
    /**
     * Attach the global period
     *
     * DSL keyword: app -> period
     *
     * @param context
     * @return nullptr
     */
    virtual ANTLR_ANY_TYPE visitRuleappSettingPeriod(TeamPlayParser::RuleappSettingPeriodContext *context) override;
    /**
     * Attach the global energy available
     *
     * DSL keyword: app -> energy-available
     *
     * @param context
     * @return nullptr
     */
    virtual ANTLR_ANY_TYPE visitRuleappSettingEnergy(TeamPlayParser::RuleappSettingEnergyContext *context) override;

    /**
     * Visit and create a new component/task
     *
     * DSL keyword: app -> components -> *
     *
     * @param context
     * @return nullptr
     */
    virtual ANTLR_ANY_TYPE visitRulecomponent(TeamPlayParser::RulecomponentContext *context) override;
    /**
     * Create output ports to be added to a component
     *
     * DSL keyword: app -> components -> * -> outports
     *
     * @param context
     * @return #Task*
     */
    virtual ANTLR_ANY_TYPE visitRulecomponentSettingOutports(TeamPlayParser::RulecomponentSettingOutportsContext *context) override;
    /**
     * Create input ports to be added to a component
     *
     * DSL keyword: app -> components -> * -> inports
     *
     * @param context
     * @return nullptr
     */
    virtual ANTLR_ANY_TYPE visitRulecomponentSettingInports(TeamPlayParser::RulecomponentSettingInportsContext *context) override;
    virtual ANTLR_ANY_TYPE visitRulecomponentSettingState(TeamPlayParser::RulecomponentSettingStateContext *context) override;
    
    virtual ANTLR_ANY_TYPE visitRulecomponentSettingVersions(TeamPlayParser::RulecomponentSettingVersionsContext *context) override;
    /**
     * @todo
     *
     * @param context
     * @return nullptr
     */
    virtual ANTLR_ANY_TYPE visitRulecomponentSettingSecurity(TeamPlayParser::RulecomponentSettingSecurityContext *context) override;
    /**
     * Attach a deadline to the current task
     *
     * DSL keyword: app -> components -> * -> deadline
     *
     * @param context
     * @return nullptr
     */
    virtual ANTLR_ANY_TYPE visitRulecomponentSettingDeadline(TeamPlayParser::RulecomponentSettingDeadlineContext *context) override;
    /**
     * Attach a period to the current task
     *
     * DSL keyword: app -> components -> * -> period
     *
     * @param context
     * @return nullptr
     */
    virtual ANTLR_ANY_TYPE visitRulecomponentSettingPeriod(TeamPlayParser::RulecomponentSettingPeriodContext *context) override;
    
    virtual ANTLR_ANY_TYPE visitRulecomponentSettingWCET(TeamPlayParser::RulecomponentSettingWCETContext *context) override;
    virtual ANTLR_ANY_TYPE visitRulecomponentSettingWCEC(TeamPlayParser::RulecomponentSettingWCECContext *context) override;
    /**
     * @todo
     *
     * @param context
     * @return nullptr
     */
    virtual ANTLR_ANY_TYPE visitRulecomponentSettingCName(TeamPlayParser::RulecomponentSettingCNameContext *context) override;
    virtual ANTLR_ANY_TYPE visitRulecomponentSettingCSignature(TeamPlayParser::RulecomponentSettingCSignatureContext *context) override;
    
    virtual ANTLR_ANY_TYPE visitRulecomponentSettingArch(TeamPlayParser::RulecomponentSettingArchContext *context) override;
    virtual ANTLR_ANY_TYPE visitRulecomponentSettingResource(TeamPlayParser::RulecomponentSettingResourceContext *context) override;
    
    virtual ANTLR_ANY_TYPE visitRulecomponentSettingRepeat(TeamPlayParser::RulecomponentSettingRepeatContext *context) override;
    /**
     * Mark a component/task as tainted
     * @param context
     * @return 
     */
    virtual ANTLR_ANY_TYPE visitRulecomponentSettingTaint(TeamPlayParser::RulecomponentSettingTaintContext *context) override;
    /**
     * Set the starting offset of a component/task
     * @param context
     * @return 
     */
    virtual ANTLR_ANY_TYPE visitRulecomponentSettingOffset(TeamPlayParser::RulecomponentSettingOffsetContext *context) override;
    
    virtual ANTLR_ANY_TYPE visitRulecomponentSettingCodeSize(TeamPlayParser::RulecomponentSettingCodeSizeContext *context) override;
    virtual ANTLR_ANY_TYPE visitRulecomponentSettingStackSize(TeamPlayParser::RulecomponentSettingStackSizeContext *context) override;

    /**
     * Create a new scheduling mode
     *
     * \todo Initial work for multi-mode scheduling
     *
     * @param context
     * @return nullptr
     */
    virtual ANTLR_ANY_TYPE visitRulemode(TeamPlayParser::RulemodeContext *context) override;
    /**
     * ???
     *
     * \todo Initial work for multi-mode scheduling
     *
     * @param context
     * @return std::vector<std::string>
     */
    virtual ANTLR_ANY_TYPE visitRuleenumDecl(TeamPlayParser::RuleenumDeclContext *context) override;

    /**
     * Create a new token firing guard
     *
     * \todo Initial work for token firing guard
     *
     * @param context
     * @return nullptr
     */
    virtual ANTLR_ANY_TYPE visitRuleguard(TeamPlayParser::RuleguardContext *context) override;
    /**
     * ???
     *
     * \todo Initial work for token firing guard
     *
     * @param context
     * @return bool
     */
    virtual ANTLR_ANY_TYPE visitRuleguardEnumExpr(TeamPlayParser::RuleguardEnumExprContext *context) override;
    /**
     * ???
     *
     * \todo Initial work for token firing guard
     *
     * @param context
     * @return bool
     */
    virtual ANTLR_ANY_TYPE visitRuleguardIntExpr(TeamPlayParser::RuleguardIntExprContext *context) override;
    /**
     * ???
     *
     * \todo Initial work for token firing guard
     *
     * @param context
     * @return bool
     */
    virtual ANTLR_ANY_TYPE visitRuleguardExpr(TeamPlayParser::RuleguardExprContext *context) override;

    /**
     * Visit and create a new version
     *
     * DSL keyword: app -> components -> * -> versions ->
     *
     * @param context
     * @return #Version *
     */
    virtual ANTLR_ANY_TYPE visitRuleversion(TeamPlayParser::RuleversionContext *context) override;
    /**
     * Attach a WCET to the current version
     *
     * DSL keyword: app -> components -> * -> versions -> * -> WCET
     *
     * @param context
     * @return nullptr
     */
    virtual ANTLR_ANY_TYPE visitRuleversionSettingWCET(TeamPlayParser::RuleversionSettingWCETContext *context) override;
    /**
     * Attach a WCEC to the current version
     *
     * DSL keyword: app -> components -> * -> versions -> * -> WCEC
     *
     * @param context
     * @return nullptr
     */
    virtual ANTLR_ANY_TYPE visitRuleversionSettingWCEC(TeamPlayParser::RuleversionSettingWCECContext *context) override;
    /**
     * Specify which architecture the current version is targeting.
     *
     * The architecture can be equivalent to the following from the configuration file:
     *   - a core id
     *   - a class of architecture
     *
     * DSL keyword: app -> components -> * -> versions -> * -> arch
     *
     * @param context
     * @return nullptr
     */
    virtual ANTLR_ANY_TYPE visitRuleversionSettingArch(TeamPlayParser::RuleversionSettingArchContext *context) override;
    virtual ANTLR_ANY_TYPE visitRuleversionSettingResource(TeamPlayParser::RuleversionSettingResourceContext *context) override;
    /**
     * Attach a security level to the current version
     *
     * DSL keyword: app -> components -> * -> versions -> * -> security
     *
     * @param context
     * @return nullptr
     */
    virtual ANTLR_ANY_TYPE visitRuleversionSettingSecurity(TeamPlayParser::RuleversionSettingSecurityContext *context) override;
    /**
     * Attach a callable name to the current version
     *
     * \see Version::_c_callable
     *
     * DSL keyword: app -> components -> * -> versions -> * -> WCEC
     *
     * @param context
     * @return nullptr
     */
    virtual ANTLR_ANY_TYPE visitRuleversionSettingCName(TeamPlayParser::RuleversionSettingCNameContext *context) override;
    virtual ANTLR_ANY_TYPE visitRuleversionSettingCSignature(TeamPlayParser::RuleversionSettingCSignatureContext *context) override;
    /**
     * Attach a WCEC to the current version
     *
     * DSL keyword: app -> components -> * -> versions -> * -> WCEC
     *
     * @param context
     * @return nullptr
     */
    virtual ANTLR_ANY_TYPE visitRuleversionSettingCodeSize(TeamPlayParser::RuleversionSettingCodeSizeContext *context) override;
    /**
     * Attach a WCEC to the current version
     *
     * DSL keyword: app -> components -> * -> versions -> * -> WCEC
     *
     * @param context
     * @return nullptr
     */
    virtual ANTLR_ANY_TYPE visitRuleversionSettingStackSize(TeamPlayParser::RuleversionSettingStackSizeContext *context) override;

    /**
     * Create a connection between two tasks
     *
     * DSL keyword: app -> channels -> *
     *
     * @param context
     * @return nullptr
     */
    virtual ANTLR_ANY_TYPE visitRulechannel(TeamPlayParser::RulechannelContext *context) override;

    /**
     * Create a connector
     *
     * DSL keyword: app -> components -> outports/inports -> *
     *
     * @param context
     * @return #Connector *
     */
    virtual ANTLR_ANY_TYPE visitRuleportDecl(TeamPlayParser::RuleportDeclContext *context) override;

    /**
     * Get the task and related connector for a connection
     *
     * @param context
     * @return std::pair<Task *, std::string>
     */
    virtual ANTLR_ANY_TYPE visitRuleportRef(TeamPlayParser::RuleportRefContext *context) override;
    /**
     * Get sink part of a connection
     *
     * @param context
     * @return std::pair<Task *, Connector*>
     */
    virtual ANTLR_ANY_TYPE visitRuleportOutRef(TeamPlayParser::RuleportOutRefContext *context) override;
    /**
     * Get source part of a connection
     *
     * @param context
     * @return std::pair<Task *, Connector*>
     */
    virtual ANTLR_ANY_TYPE visitRuleportInRef(TeamPlayParser::RuleportInRefContext *context) override;
    /**
     * Create the whole sink part of a connection
     *
     * @param context
     * @return std::pair<Connector::type_e, std::vector<std::pair < Task *, Connector*>>>
     */
    virtual ANTLR_ANY_TYPE visitRuleportOutSpec(TeamPlayParser::RuleportOutSpecContext *context) override;
    /**
     * Create the whole source part of a connection
     *
     * @param context
     * @return std::pair<Connector::type_e, std::vector<std::pair < Task *, Connector*>>>
     */
    virtual ANTLR_ANY_TYPE visitRuleportInSpec(TeamPlayParser::RuleportInSpecContext *context) override;

    /**
     * Convert time value into nanosecond
     * @param context
     * @return globtiminginfos_t
     */
    virtual ANTLR_ANY_TYPE visitRuletimeValue(TeamPlayParser::RuletimeValueContext *context) override;
    /**
     * Convert energy value into Joules
     * @param context
     * @return energycons_t
     */
    virtual ANTLR_ANY_TYPE visitRuleenergyValue(TeamPlayParser::RuleenergyValueContext *context) override;
    /**
     * Convert memory size value into Joules
     * @param context
     * @return datamemsize_t
     */
    virtual ANTLR_ANY_TYPE visitRulememValue(TeamPlayParser::RulememValueContext *context) override;
    /**
     * Convert int value to long
     * @param context
     * @return long
     */
    virtual ANTLR_ANY_TYPE visitRuleintValue(TeamPlayParser::RuleintValueContext *context) override;
    /**
     * Remove quotes around string
     * @param context
     * @return std::string
     */
    virtual ANTLR_ANY_TYPE visitRulestringValue(TeamPlayParser::RulestringValueContext *context) override;

//    virtual ANTLR_ANY_TYPE visitRulemetaComponent(TeamPlayParser::RulemetaComponentContext *context) override;
    virtual ANTLR_ANY_TYPE visitRulecomponentSettingsProfileInstanciation(TeamPlayParser::RulecomponentSettingsProfileInstanciationContext *context) override;
    virtual ANTLR_ANY_TYPE visitRulecomponentSettingRemoveSubgraphProp(TeamPlayParser::RulecomponentSettingRemoveSubgraphPropContext *context)  override;
    
    virtual ANTLR_ANY_TYPE visitRulecomponentSettingSubComponents(TeamPlayParser::RulecomponentSettingSubComponentsContext *context) override;
    
    virtual ANTLR_ANY_TYPE visitRulecomponentSettingProfiles(TeamPlayParser::RulecomponentSettingProfilesContext *context) override;
    
    virtual ANTLR_ANY_TYPE visitRuletemplates(TeamPlayParser::RuletemplatesContext *context) override;
    virtual ANTLR_ANY_TYPE visitRuletemplateSettingChannels(TeamPlayParser::RuletemplateSettingChannelsContext *context) override;
    
    virtual ANTLR_ANY_TYPE visitRuleprofile(TeamPlayParser::RuleprofileContext *context) override;
    virtual ANTLR_ANY_TYPE visitRuleprofileFTSettings(TeamPlayParser::RuleprofileFTSettingsContext *context) override;
    virtual ANTLR_ANY_TYPE visitRulenModularSettingRep(TeamPlayParser::RulenModularSettingRepContext *ctx) override;
    virtual ANTLR_ANY_TYPE visitRulenModularSettingvotRep(TeamPlayParser::RulenModularSettingvotRepContext *context) override;
    virtual ANTLR_ANY_TYPE visitRulenModularSettingWT(TeamPlayParser::RulenModularSettingWTContext *context) override;
    virtual ANTLR_ANY_TYPE visitRulenModularSettingWS(TeamPlayParser::RulenModularSettingWSContext *context) override;
    virtual ANTLR_ANY_TYPE visitRulestandbySettingRep(TeamPlayParser::RulestandbySettingRepContext *context) override;
    virtual ANTLR_ANY_TYPE visitRulestandbySettingSync(TeamPlayParser::RulestandbySettingSyncContext *context) override;
    virtual ANTLR_ANY_TYPE visitRulecheckpointSetting(TeamPlayParser::RulecheckpointSettingContext *context) override;
    virtual ANTLR_ANY_TYPE visitRulenVersionSetting(TeamPlayParser::RulenVersionSettingContext *context) override;
    
    virtual ANTLR_ANY_TYPE visitRuleversionSettingGuard(TeamPlayParser::RuleversionSettingGuardContext *context) override;
    
    virtual ANTLR_ANY_TYPE visitRulephaseSettingWCET(TeamPlayParser::RulephaseSettingWCETContext *context) override;
    virtual ANTLR_ANY_TYPE visitRulephaseSettingWCEC(TeamPlayParser::RulephaseSettingWCECContext *context) override;
    virtual ANTLR_ANY_TYPE visitRulephaseSettingArch(TeamPlayParser::RulephaseSettingArchContext *context) override ;
    virtual ANTLR_ANY_TYPE visitRulephaseSettingResource(TeamPlayParser::RulephaseSettingResourceContext *context) override ;
    virtual ANTLR_ANY_TYPE visitRulephaseSettingCName(TeamPlayParser::RulephaseSettingCNameContext *context) override ;
    virtual ANTLR_ANY_TYPE visitRulephaseSettingCSignature(TeamPlayParser::RulephaseSettingCSignatureContext *context) override;

    /**
     * Attach a code size to the current phase
     *
     * DSL keyword: app -> components -> * -> versions -> * -> phases -> * -> code size
     *
     * @param context
     * @return nullptr
     */
    virtual ANTLR_ANY_TYPE visitRulephaseSettingCodeSize(TeamPlayParser::RulephaseSettingCodeSizeContext *context) override;
    /**
     * Attach a code size to the current phase
     *
     * DSL keyword: app -> components -> * -> versions -> * -> phases -> * -> WCEC
     *
     * @param context
     * @return nullptr
     */
    virtual ANTLR_ANY_TYPE visitRulephaseSettingStackSize(TeamPlayParser::RulephaseSettingStackSizeContext *context) override;

    virtual ANTLR_ANY_TYPE visitRuletemplateInstantiation(TeamPlayParser::RuletemplateInstantiationContext *context) override;
    
    virtual ANTLR_ANY_TYPE visitRuletimeValueOrParamName(TeamPlayParser::RuletimeValueOrParamNameContext *context) override;
    virtual ANTLR_ANY_TYPE visitRuleenergyValueOrParamName(TeamPlayParser::RuleenergyValueOrParamNameContext *context) override;
    virtual ANTLR_ANY_TYPE visitRulememValueOrParamName(TeamPlayParser::RulememValueOrParamNameContext *context) override;
    virtual ANTLR_ANY_TYPE visitRuleintValueOrParamName(TeamPlayParser::RuleintValueOrParamNameContext *context) override;
    virtual ANTLR_ANY_TYPE visitRulepercentValueOrParamName(TeamPlayParser::RulepercentValueOrParamNameContext *context) override;
    virtual ANTLR_ANY_TYPE visitRulestringValueOrParamName(TeamPlayParser::RulestringValueOrParamNameContext *context) override;
    virtual ANTLR_ANY_TYPE visitRuleboolValueOrParamName(TeamPlayParser::RuleboolValueOrParamNameContext *context) override;
    virtual ANTLR_ANY_TYPE visitRulewaitStartValueOrParamName(TeamPlayParser::RulewaitStartValueOrParamNameContext *context) override;
    virtual ANTLR_ANY_TYPE visitRulesyncValueOrParamName(TeamPlayParser::RulesyncValueOrParamNameContext *context) override;
    virtual ANTLR_ANY_TYPE visitRuleparamNameValue(TeamPlayParser::RuleparamNameValueContext *context) override;
    virtual ANTLR_ANY_TYPE visitRulewaitStartValue(TeamPlayParser::RulewaitStartValueContext *context) override;
    virtual ANTLR_ANY_TYPE visitRulesyncValue(TeamPlayParser::RulesyncValueContext *context) override;
    virtual ANTLR_ANY_TYPE visitRuleboolValue(TeamPlayParser::RuleboolValueContext *context) override;
    virtual ANTLR_ANY_TYPE visitRulesubgraphParams(TeamPlayParser::RulesubgraphParamsContext *context) override;
    
    virtual ANTLR_ANY_TYPE visitRulephases(TeamPlayParser::RulephasesContext *context) override;

private:
    /*! ???
     *
     * \todo Initial work for multi-mode scheduling
     */
    enum class ModeType {
        INTEGER, //! \todo Initial work for multi-mode scheduling
        ENUM //! \todo Initial work for multi-mode scheduling
    };

    std::map<std::string, ModeType> mode_type; //!< \todo Initial work for multi-mode scheduling
    std::map<std::string, std::vector<std::string>> mode_enum; //!< \todo Initial work for multi-mode scheduling
    std::map<std::string, long> mode_int_values; //!< \todo Initial work for multi-mode scheduling
    std::map<std::string, std::string> mode_enum_values; //!< \todo Initial work for multi-mode scheduling

    std::stack<Task *> current_component; //!< hold the current uncomplete task
    Version * current_version = nullptr; //!< hold the current uncomplete version
    
    struct Channel {
        Task *src;
        Task *sink;
        Connector *csrc;
        Connector *csink;
        Channel() {}
        Channel(Task* psrc, Task* psink, Connector *psrccon, Connector *psinkcon): src(psrc), sink(psink), csrc(psrccon), csink(psinkcon) {}
    };
    
    struct TemplateInstance;
    struct Template : public Task {
        std::vector<TemplateInstance*> tpl_instance;
        std::vector<Task*> components;
        std::vector<std::string> params;
        std::map<std::string, std::vector<std::function<void(Task*, ANTLR_ANY_TYPE)>>> param_to_apply_component;
        std::map<std::string, std::vector<std::function<void(Profile*, ANTLR_ANY_TYPE)>>> param_to_apply_profile;
        
        std::vector<TemplateInstance*> subgraphs;
        
        std::vector<Channel*> channels;
        
        Template(const std::string &id) : Task(id) {}
        
        virtual void clone(Template *rhs) ;
        
        Task* find_components(const std::string &tname) { 
            std::vector<Task*>::iterator it = std::find_if(components.begin(), components.end(), [tname](Task *a) { return a->id() == tname; });
            return it == components.end() ? nullptr : *it;
        }
    };
    struct TemplateInstance : public Template {
        Task *parent;
        TemplateInstance(const std::string &id) : Template(id) {}
    };
    std::vector<Template*> templates;
    std::stack<Template *> current_template;
    std::vector<TemplateInstance*> top_level_tpl_instance;
    
    std::map<std::string, Profile*> profiles;
    std::map<Task *, std::vector<std::string>> profile_settings;
    Profile *current_profile;
    
    std::vector<TemplateInstance*> top_level_subgraphs;
    
    void resolveTemplateInstanciation(TemplateInstance *tpl_inst, std::map<Task*, Task*> *tasks_to_add, std::vector<Channel*> *chans_to_add);
    void resolveProfile(Task *t, const std::string &prof_name);
    
    void resolveSubgraphs(TemplateInstance *tpl_inst, std::map<Task*, Task*> *tasks_to_add, std::vector<Channel*> *chans_to_add);
    
    void storeComponentParameterResolution(const std::string &desc, ANTLR_ANY_TYPE value, const std::string &paramname, std::function<void(Task *, ANTLR_ANY_TYPE)> f);
    void storeProfileParameterResolution(const std::string &desc, ANTLR_ANY_TYPE value, const std::string &paramname, std::function<void(Profile *, ANTLR_ANY_TYPE)> f);
    void storeProfileParameterResolution(const std::string &key, const std::string &type, ANTLR_ANY_TYPE value, const std::string &paramname, std::function<void(Profile *, const std::string &, const std::string &, ANTLR_ANY_TYPE)> f);
    Phase * current_phase = nullptr;  //!< hold the current uncomplete phase
};

REGISTER_PARSER(TeamPlayInputParser, ".tey");

#endif
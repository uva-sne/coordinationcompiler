/* 
 * Copyright (C) 2020 Benjamin Rouxel <benjamin.rouxel@uva.nl>, University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "InputParser.hpp"
#include <boost/iostreams/char_traits.hpp>
#include <boost/iostreams/device/file_descriptor.hpp>
#include <boost/iostreams/filtering_stream.hpp>
using namespace std;

namespace io = boost::iostreams;

void InputParser::parse(const boost::filesystem::path &path) {
    if(!boost::filesystem::exists(path))
        throw CompilerException("parser", path.string() + " doesn't exist");
    
    std::ifstream file_stream(path.string());
    io::filtering_istream filtered_stream;
    filtered_stream.push(CliSubstitutionIStream(cmd_properties));
    filtered_stream.push(file_stream);

    parse(filtered_stream);
    
    file_stream.close();
}

InputParser::~InputParser() {
    // delete out;
    // delete conf;
    // delete cmd_properties;
}
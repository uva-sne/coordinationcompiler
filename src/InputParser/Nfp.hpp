/*
 * Copyright (C) 2022 Lukas Miedema <l.miedema@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CECILE_NFP_HPP
#define CECILE_NFP_HPP

#include <string>
#include <vector>
#include <config.hpp>
#include <SystemModel.hpp>
#include "CliSubstitutionIStream.hpp"

/**
 * Represents a single line in the NFP file (a vector of NFPs for one task/version/arch combination).
 * Data is without interpretation (i.e. all are strings as presented in the CSV).
 */
struct Nfp {
public:
#define DECLARE_PROPERTY(symbol_name_upper, symbol_name, csv_name, id) std::string symbol_name;
#include "NfpColumns.hpp"
#undef DECLARE_PROPERTY
};

/**
 * All NFP columns
 */
enum NfpColumn {
#define DECLARE_PROPERTY(symbol_name_upper, symbol_name, csv_name, id) \
    PROPERTY_ ## symbol_name_upper = id,
#include "NfpColumns.hpp"

#undef DECLARE_PROPERTY
    COLUMN_COUNT
};

/**
 * Maps a column id to a
 */
const std::vector<std::string> column_id_to_strs[] = {
#define DECLARE_PROPERTY(symbol_name_upper, symbol_name, csv_name, id) { csv_name },
#include "NfpColumns.hpp"

#undef DECLARE_PROPERTY
};

#endif //CECILE_NFP_HPP

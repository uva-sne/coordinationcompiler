/*
 * Copyright (C) 2020 Lukas Miedema <l.miedema@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CLISUBSTITUTIONISTREAM_HPP
#define CLISUBSTITUTIONISTREAM_HPP

#include <cstdio>
#include <streambuf>
#include <istream>
#include <map>
#ifndef EOF
#define EOF (-1)
#endif
#include <boost/iostreams/char_traits.hpp> // This include raises and undeclared EOF on my computer, dunno why
#include <boost/iostreams/concepts.hpp>
#include <boost/iostreams/operations.hpp>
#include "Utils/Log.hpp"

#define MODE_LOOKING_FOR_DOLLAR -1
#define MODE_HAS_LOOKAHEAD -2

using CommandLineProperties = std::map<const std::string, const std::string>;

/**
 * Preprocess any input stream.
 *
 * Replace all occurrences of ${prop} with the value of "prop" taken from the cmd_properties map.
 * Any ${..} occurrences for which no key exists in the cmd_properties map will lead to an exception. Any stray
 * $-signs will lead to an exception. Occurrences of $ can be ignored by the escape sign (\).
 */
class CliSubstitutionIStream : public boost::iostreams::input_filter {
public:
    /**
     * Constructor
     * @param cmd_properties
     */
    explicit CliSubstitutionIStream(CommandLineProperties *cmd_properties) :
            cmd_properties(cmd_properties),
            mode(-1),
            value(nullptr) {}

    /**
     * Get a char from the source
     * 
     * Either it returns a char from the source, or a char from the replacement string
     * 
     * @param src
     * @return 
     */
    template<typename Source>
    int get(Source& src) {
        if (mode == MODE_HAS_LOOKAHEAD) {
            // there's a character read from the src stream that has not been sent to the reader yet
            // this happens after reading a $, the next character must be read.
            mode = MODE_LOOKING_FOR_DOLLAR;
            return this->lookahead;
        }
        else if (mode == MODE_LOOKING_FOR_DOLLAR) {
            // Normal mode -> seek for '$'
            int next = boost::iostreams::get(src);
            if (next == '\\')
                return boost::iostreams::get(src);
            if (next == '$') {
                std::string key;
                next = boost::iostreams::get(src);
                if (next == '{') {
                    // Read until we find '}'
                    next = boost::iostreams::get(src);
                    while (next != '}') {
                        key.push_back(next);
                        next = boost::iostreams::get(src);
                    }
                    this->mode = 0;
                    std::map<const std::string, const std::string>::iterator value = this->cmd_properties->find(key);
                    if (value == this->cmd_properties->end()) {
                        // Throwing an exception does not mean the SaxParser (or any other parser) actually stops
                        // and relays the message to CECILE code. So we cannot just throw a CompilerException here
                        // as the message may get lost.
                        Utils::ERROR("input parser", "Unrecognized property '${" + key +
                                                                "}'. Please provide a value using '-p " + key +
                                                                ":value'");
                        throw std::runtime_error("Invalid input");
                    }
                    this->value = &value->second;
                    this->mode = 0;
                    return get(src);
                } else {
                    // no variable found, but we read two characters now ($ and "next"). Send $ to the reader, and set the lookahead
                    lookahead = next;
                    mode = MODE_HAS_LOOKAHEAD;
                    return '$';
                }
            } else {
                return next;
            }
        } else {
            int c = this->value->at(this->mode);
            if (++this->mode >= (long int) this->value->length())
                this->mode = -1;
            return c;
        }
    }
private:
    CommandLineProperties *cmd_properties; //!< Replacement properties given on the command line
    long int mode; //!< When a key was found, this is the index in the value. Otherwise one of the mode constants
    int lookahead; //!< When mode is MODE_HAS_LOOKAHEAD, this value will be returned first
    const std::string *value; //!< Current value associated with the last found key, or null
};

#endif //CLISUBSTITUTIONISTREAM_HPP

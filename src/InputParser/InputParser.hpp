/* 
 * Copyright (C) 2017 Benjamin Rouxel <benjamin.rouxel@inria.fr> 
 *                    Irisa/INRIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef INPUT_PARSER_H
#define INPUT_PARSER_H

#include <iostream>
#include <string>

#ifndef ANTLR_ANY_API
#include <any>
#endif

#include "config.hpp"
#include "registry.hpp"
#include "CliSubstitutionIStream.hpp"

/**
 * General class for parser
 *
 * The template parameter defines which object the parser will fill
 */
class InputParser {
protected:
    SystemModel *out; //!< Object populated during parsing
    config_t *conf; //!< current configuration
    CommandLineProperties *cmd_properties; //!< Replacement properties given on the command line

    /**
     * Constructor
     * @param o
     * @param c
     * @param prop command line properties
     */
    explicit InputParser(SystemModel *o, config_t *c, CommandLineProperties *prop) :
        out(o), conf(c), cmd_properties(prop) {};
    /**
     * Parse the given input stream
     * @param input
     */
    virtual void parse(std::istream &input) = 0;
public:
    virtual ~InputParser();

    /**
     * Creates an input stream from the given path and provides that to parse
     * 
     * Parse the file contents with the provided class_name ParserRegistry entry.
     * 
     * @param path
     */
    void parse(const boost::filesystem::path &path);
};

using ParserRegistry = registry::Registry<InputParser, std::string, SystemModel *, config_t *, CommandLineProperties*>;

#define REGISTER_PARSER(ClassName, Identifier) \
  REGISTER_SUBCLASS(InputParser, ClassName, std::string, Identifier, SystemModel *, config_t *, CommandLineProperties*)

#endif /* INPUT_PARSER_H */


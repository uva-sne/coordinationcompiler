/* 
 * Copyright (C) 2017 Benjamin Rouxel &lt;benjamin.rouxel@inria.fr&gt;
 *                    Irisa/INRIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see &lt;http://www.gnu.org/licenses/&gt;.
 */

#ifndef SAXPARSER_H
#define SAXPARSER_H

#include <cstdlib>
#include <cstdio>
#include <string>
#include <iostream>
#include <functional>
#include <libxml++/libxml++.h>
#include <stack>
#include <boost/range/combine.hpp>
#include <GroupPass.hpp>

#include "InputParser.hpp"
#include "SystemModel.hpp"
#include "Interconnect/CoreMemInterConnect.hpp"
#include "Utils.hpp"
#include "Analyse/Analyse.hpp"
#include "Transform/Transform.hpp"
#include "Generator/Generator.hpp"
#include "Solvers/Solver.hpp"
#include "Partitioning/Partitioning.hpp"
#include "PriorityAssignment/PriorityAssignment.hpp"
#include "Simulator/Simulator.hpp"
#include "Exporter/Exporter.hpp"
#include "Exporter/UppaalTrait/UppaalTrait.hpp"
#include "StateMachine/Generator/StrategyGenerator.hpp"
#include "StateMachine/Transform/StrategyTransform.hpp"
#include "StateMachine/Linker/StrategyLinker.hpp"
#include "StateMachine/Exporter/StrategyExporter.hpp"
#include "Properties.hpp"

struct dispatcher_context_t {
    Dispatcher *dispatcher; // always exists
    GroupPass *parent = nullptr; // or null, if this is the root

    bool is_root() const {
        return parent == nullptr;
    }

    Dispatcher * operator ->() const {
        return dispatcher;
    }
};

/**
 * Parser for the configuration file
 *
 * It parses the XML configuration file following a Simple API for XML (SAX) parsing mechanism.
 * This idea is to not load the full Document Object Model (DOM) as most of XML parser will do.
 * This allows to not waste time/memory by creating this DOM.
 *
 * The document is parsed in depth, each time a new opening tag is read from the file,
 * the `on_start_element' handler is called, and we can parse the attribute of this XML tag
 * to populate the configuration.
 *
 * @todo create a XSD
 */
class ConfigXmlSaxParser : public InputParser, public xmlpp::SaxParser {
    std::stack<std::string> last_open_tag; //!< hold the stack of open tags, but not closed yet
public:
    ConfigXmlSaxParser() = delete;
    ConfigXmlSaxParser(const ConfigXmlSaxParser&) = delete;
    
    /**
     * Constructor
     * @param o #SystemModel
     * @param c
     * @param cmd_properties command line properties
     */
    explicit ConfigXmlSaxParser(SystemModel *o, config_t *c, CommandLineProperties *cmd_properties) :
        InputParser(o, c, cmd_properties), SaxParser() {
        this->_scopes.push_back({ .dispatcher = &c->passes }); // root
    }
    
    //! Destructor
    ~ConfigXmlSaxParser() override = default;
    
protected:
    virtual void parse(std::istream &input) override {
        xmlpp::SaxParser::parse_stream(input);
    }

    /**
     * Called at the beginning of the parsing, before the first XML tag
     */
    void on_start_document() override;
    /**
     * Called at the end of the parsing, after the last XML tag closing
     */
    void on_end_document() override;
    /**
     * Called at each opening XML tag
     *
     * @param name of the XML tag
     * @param properties the list of XML attributes
     */
    void on_start_element(const Glib::ustring& name, const AttributeList& properties) override;
    /**
     * Called at each closing XML tag
     * @param name of the XML tag
     */
    void on_end_element(const Glib::ustring& name) override;
    //  void on_characters(const Glib::ustring& characters) override;
    //  void on_comment(const Glib::ustring& text) override;
    //  void on_warning(const Glib::ustring& text) override;
    /**
     * Called when a parsing error occurs
     * @param text
     */
    void on_error(const Glib::ustring& text) override;
    /**
     * Called when a fatal parsing error occurs
     * @param text
     */
    void on_fatal_error(const Glib::ustring& text) override;
    
private:
    std::vector<dispatcher_context_t> _scopes; // stack of scopes within which compiler passes are added

    /**
     * Current scope
     * @return
     */
    dispatcher_context_t &scope();

    void handle_config(const AttributeList& properties);
    
    /**
     * Some tags must be children of others
     *
     * This method checks that the given tag is a children of another specific one
     *
     * @param name of the XML tag
     * @param properties the list of XML attributes
     * @return true if matching the subtag was successful, false otherwise
     */
    bool manage_subtags(const Glib::ustring& name, const AttributeList& properties);

    /**
     * Handle the &lt;data-type .../&gt; tag
     * This tag is a child of &lt;cecile /&gt;
     * @see config_t
     * @param properties the list of attributes
     */
    void handle_config_mem_datatype(const AttributeList& properties);
    
    /**
     * Handle the &lt;communication .../&gt; tag
     * This tag is a child of &lt;architecture /&gt;
     * @see CoreMemInterConnect
     * @param properties the list of attributes
     */
    void handle_config_communication(const AttributeList& properties);
    /**
     * Handle the &lt;spm .../&gt; tag
     * This tag is a child of &lt;architecture /&gt;
     * @see CoreMemInterConnect
     * @param properties the list of attributes
     */
    void handle_config_spm_mgnt(const AttributeList& properties);
    /**
     * Handle the &lt;processor .../&gt; tag
     * This tag is a child of &lt;architecture /&gt;
     * @see meth_processor_t
     * @param properties the list of attributes
     */
    void handle_config_compute_units(const AttributeList& properties, bool separate_coprocessor = false);
    /**
     * Handle the &lt;voltage_island .../&gt; tag
     * This tag is a child of &lt;config /&gt;
     * @see voltage_island
     * @param properties the list of attributes
     */
    void handle_voltage_island(const AttributeList& properties);
    /**
     * Handle the base watt of a system
     * @param properties the list of attributes
     */
    void handle_base_watt(const AttributeList& properties);

    void handle_config_resource(const AttributeList& properties);
    
    /**
     * Handle the &lt;mainargs .../&gt; tag
     *
     * This tag is a child of &lt;generator /&gt;
     * @see Generator
     * @param properties the list of attributes
     */
    void handle_generator_mainargs(const AttributeList& properties);
    /**
     * Handle the &lt;component .../&gt; tag
     * This tag is a child of &lt;generator /&gt;
     * @see Generator
     * @param properties the list of attributes
     */
    void handle_generator_component(const AttributeList& properties);
    /**
     * Handle the &lt;version .../&gt; tag
     * This tag is a child of &lt;generator ...&gt;&lt;component ..&gt;
     * @see Generator
     * @param properties the list of attributes
     */
    void handle_generator_component_version(const AttributeList& properties);

    void handle_simulator_subtag(const std::string &subtag, const AttributeList& properties);
    
    void handle_analyse_stats_task(const std::string &subtag, const AttributeList& properties);
    
    void handle_exporter_marker(const std::string &subtag, const AttributeList& properties);

    void handle_uppaal_trait(const AttributeList &properties);
    
    void handle_task_model_subtag_let(const AttributeList& properties);

    void handle_group_open(const AttributeList &properties);

    void handle_group_close();

    /**
     * Handle a compiler pass tag. Several specializations exist for specific types of passes.
     * @tparam RegistryType the registry to search in
     * @param name user-readable name for error reporting
     * @param properties list of attributes
     */
    template<typename RegistryType>
    void handle(const std::string& name, const AttributeList &properties) {
        std::map<std::string, std::string> params = extract_params(name, properties);
        std::string id = params["id"];
        handle<RegistryType>(name, params, id);
    }

    /**
     * Handle a compiler pass tag
     * @tparam RegistryType the registry to search in
     * @param name user-readable name for error reporting
     * @param properties list of attributes
     * @param id requested id
     */
    template<typename RegistryType>
    void handle(const std::string& name, std::map<std::string, std::string> params, std::string id) {
        if(!RegistryType::IsRegistered(id))
            throw CompilerException("xml", name + " " + id + " not found");

        CompilerPass *p = RegistryType::Create(id);
        p->set_params(params);
        this->_scopes.back()->add_pass(p);
    }

    /**
     * Handle a strategy pass, for a particular strategy manager
     * @tparam StrategyPassType the type of the strategy pass (e.g. StrategyTransform), must be a template accepting
     * a StrategyManager
     * @param name tag name
     * @param properties list of attributes
     */
    template<template<typename> typename StrategyPassType>
    void handle_strategy_pass(const std::string &name, const xmlpp::SaxParser::AttributeList &properties);

    /**
     * Extract XML parameters and validate the presence of an "id".
     * @param name
     * @param properties
     * @return
     */
    static std::map<std::string, std::string> extract_params(const std::string& name, const AttributeList &properties);
};

/**
 * Handle the <Generator> pass tag
 * @param name user-readable name for error reporting
 * @param properties list of attributes
 */
template<> void ConfigXmlSaxParser::handle<GeneratorRegistry>(const std::string&, const AttributeList &);

/**
 * Handle the <Solver> pass tag
 * @param name user-readable name for error reporting
 * @param properties list of attributes
 */
template<> void ConfigXmlSaxParser::handle<SolverRegistry>(const std::string &, const AttributeList &);

REGISTER_PARSER(ConfigXmlSaxParser, ".xml")

#endif /* SAXPARSER_H */


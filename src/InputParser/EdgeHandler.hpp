
/*
 * Copyright (C) 2020 Wouter Loeve <wouterloeve0@gmail.com>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef EDGE_HANDLER_H
#define EDGE_HANDLER_H

#include <cstdlib>
#include <cstdio>
#include <string>
#include <iostream>

#include "CoordinationLanguageLexer.h"
#include "CoordinationLanguageParser.h"
#include "CoordinationLanguageBaseVisitor.h"

#include "Utils.hpp"
#include "Utils/Log.hpp"
#include "config.hpp"

//! Type of edge
enum EdgeType {
    simple, //!< Regular edge connection to components
    duplicate, //!< 1-to-N connection where tokens from the source get duplicated to all sinks
    dataor, //! @todo
    schedOr, //! @todo 
    envOr //! @todo
};

/*!
 * This class is meant as a wrapper for the internal representation of an edge in ANTLR.
 * This class can take an existing edge or create a new one.
 * Working in the IR is a pain due to all the different edge types, 
 * 	the idea here is that you insert an edge, the class takes out all important 
 * 	variables and makes it easier to work on them.
 * Then by calling export it exports the changes to the supplied edge and returns it.
 */
class EdgeHandler {
private:
    CoordinationLanguageParser::RuleEdgeContext *_edge_ref; //!< Edge reference
    EdgeType _edge_type; //!< Type of the edge
    std::vector<CoordinationLanguageParser::RuleComponentRefContext*> _src; //!< Source of the edge
    std::vector<CoordinationLanguageParser::RuleComponentRefContext*> _dest; //!< Sink of the edge
    uint32_t _delay; //!< Delay token
    CoordinationLanguageParser::ruleDuplicateEdge_s duplicateObj; //!< ??
    CoordinationLanguageParser::ruleSimpleEdge_s simpleObj; //!< ??
    CoordinationLanguageParser::ruleDataOrEdge_s dataorObj; //!< ??

    //! All comprefs, necessary because we can't access
    //!	 the member of the original since it will create a new object.
    std::vector<CoordinationLanguageParser::RuleComponentRefContext*> _comprefs;

    /*!
     * Get end points
     * @param list
     * @return 
     */
    std::vector<CoordinationLanguageParser::ruleComponentRef_s> getEndpoints(
            std::vector<CoordinationLanguageParser::RuleComponentRefContext *> list);

    /*!
     * Adds a component reference to the given side (destination or source)
     * @param component
     * @param connector
     * @param side
     * @param versi
     * @return
     */
    CoordinationLanguageParser::RuleComponentRefContext* addCompRef(std::string component, std::string connector, std::string side, std::string versi);
    /*! Adds a component reference given a component reference object
     * @param ref
     * @param side
     * @return
     */
    CoordinationLanguageParser::RuleComponentRefContext* addCompRef(CoordinationLanguageParser::ruleComponentRef_s ref, std::string side);

    /*! Adds a component reference to a list (called in addCompRef)
     * @param side
     * @param toAdd
     * @return
     */
    CoordinationLanguageParser::RuleComponentRefContext* addCompRefToList(std::string side, CoordinationLanguageParser::RuleComponentRefContext* toAdd);
public:

    /*! Take existing edge
     * @param edge
     */
    EdgeHandler(CoordinationLanguageParser::RuleEdgeContext *edge);

    /*!  Return edge type
     * @return
     */
    EdgeType getType();

    /*! Retrieves the edge delay
     * @return
     */
    uint32_t getDelay();

    /*!
     * Retrieves all component refs (both source and dest)
     * @return
     */
    std::vector<CoordinationLanguageParser::RuleComponentRefContext*> getCompRefs();

    /*!
     * Get all source refs
     * @return
     */
    std::vector<CoordinationLanguageParser::ruleComponentRef_s> getSrc();

    /*! Get source ref at index i
     * @param i
     * @return
     */
    CoordinationLanguageParser::ruleComponentRef_s getSrc(size_t i);

    /*!
     * Get all destination refs
     * @return
     */
    std::vector<CoordinationLanguageParser::ruleComponentRef_s> getDest();

    /*! Retrieves destination compref at index i
     * @param  i
     * @return
     */
    CoordinationLanguageParser::ruleComponentRef_s getDest(size_t i);

    /*! 
     * Which referenced components from this edge match with the given component
     * @param  list
     * @param component
     * @return
     */
    std::vector<CoordinationLanguageParser::RuleComponentRefContext*> matchEndpoints(std::vector<CoordinationLanguageParser::RuleComponentRefContext*> list, std::string component);

    /*!
     * Replace the end points ?
     * @param list
     * @param component
     * @param connector
     * @param replaceComponent
     * @return 
     */
    bool replaceEndpoints(std::vector<CoordinationLanguageParser::RuleComponentRefContext*> list,
            std::string component, std::string connector, std::string replaceComponent);

    /*! 
     * Replaces edges to the given component matching the given connector with replaceComponent
     * Returns: Boolean indicating whether something has been replaced.
     * @param component
     * @param connector
     * @param replaceComponent
     * @return
     */
    bool replaceToEdge(std::string component, std::string connector, std::string replaceComponent);

    /*! 
     * Replaces edges from the given component matching the given connector with replaceComponent
     * Returns: Boolean indicating whether something has been replaced.
     * @param component
     * @param connector
     * @param replaceComponent
     * @return
     */
    bool replaceFromEdge(std::string component, std::string connector, std::string replaceComponent);

    /*! 
     * Is this edge an edge to the given component or subgraph?
     * @param component
     * @return
     */
    bool isToEdge(std::string component);

    /*! 
     * Is this edge an edge from the given component or subgraph?
     * @param component
     * @return
     */
    bool isFromEdge(std::string component);

    /*!
     * Adds a source ref to this edge
     * @param component
     * @param connector
     * @return
     */
    CoordinationLanguageParser::RuleComponentRefContext* addSource(std::string component, std::string connector);

    /*!
     * Adds a source ref to this edge (with version)
     * @param  component
     * @param connector
     * @param versi
     * @return
     */
    CoordinationLanguageParser::RuleComponentRefContext* addSource(std::string component, std::string connector, std::string versi);

    /*!
     * Adds a destination ref to this edge
     * @param component
     * @param connector
     * @return
     */
    CoordinationLanguageParser::RuleComponentRefContext* addDestination(std::string component, std::string connector);

    /*!
     * Adds a destination ref to this edge by a compref_s struct.
     * @param ref
     * @return
     */
    CoordinationLanguageParser::RuleComponentRefContext* addDestination(CoordinationLanguageParser::ruleComponentRef_s ref);

    /*!
     * Return a string representation of an Edge
     * @return 
     */
    std::string to_string();

    /*!
     * Return a string representation of a list of component reference
     * @param list
     * @return 
     */
    std::string comp_ref_list_to_string(std::vector<CoordinationLanguageParser::RuleComponentRefContext*> list);
};
#endif /* EDGE_HANDLER_H */

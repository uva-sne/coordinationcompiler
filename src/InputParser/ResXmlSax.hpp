/* 
 * Copyright (C) 2017 Benjamin Rouxel &lt;benjamin.rouxel@inria.fr&gt;
 *                    Irisa/INRIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see &lt;http://www.gnu.org/licenses/&gt;.
 */

#ifndef RESXMLSAXPARSER_H
#define RESXMLSAXPARSER_H

#include <cstdlib>
#include <cstdio>
#include <string>
#include <iostream>
#include <functional>
#include <libxml++/libxml++.h>
#include <stack>

#include "InputParser.hpp"
#include "SystemModel.hpp"
#include "Utils.hpp"
#include "Schedule.hpp"

/**
 * Parser for the result XML file
 *
 * It parses the XML result file following a Simple API for XML (SAX) parsing mechanism.
 * This idea is to not load the full Document Object Model (DOM) as most of XML parser will do.
 * This allows to not waste time/memory by creating this DOM.
 *
 * The document is parsed in depth, each time a new opening tag is read from the file,
 * the `on_start_element' handler is called, and we can parse the attribute of this XML tag
 * to populate the configuration.
 *
 * @todo create a XSD
 */
class ResXMLSaxParser : public InputParser, private xmlpp::SaxParser {
    std::stack<std::string> last_open_tag; //!< hold the stack of open tags, but not closed yet
    Schedule *_schedule = nullptr;
    SchedJob *current_job = nullptr;
    SchedCore *current_core = nullptr;
    SchedSpmRegion *current_spm_region = nullptr;
    SchedResource *current_resource = nullptr;
    
    std::vector<std::pair<SchedJob*, AttributeList>> trans_delayed;
    std::vector<std::pair<SchedSpmRegion*, AttributeList>> mem_book_delayed;
    std::vector<std::pair<SchedResource*, AttributeList>> resource_delayed;
public:
    ResXMLSaxParser() = delete;
    ResXMLSaxParser(const ResXMLSaxParser&) = delete;
    
    /**
     * Constructor
     * @param o #SystemModel
     * @param c
     * @param cmd_properties command line properties
     */
    explicit ResXMLSaxParser(SystemModel *tg, config_t *c, CommandLineProperties *cmd_properties) :
        InputParser(tg, c, cmd_properties), SaxParser() {}
    
    //! Destructor
    ~ResXMLSaxParser() override;
    
    ACCESSORS_RW(ResXMLSaxParser, Schedule *, schedule)
    
protected:
    virtual void parse(std::istream &input) override {
        xmlpp::SaxParser::parse_stream(input);
    }

    /**
     * Called at the beginning of the parsing, before the first XML tag
     */
    void on_start_document() override;
    /**
     * Called at the end of the parsing, after the last XML tag closing
     */
    void on_end_document() override;
    /**
     * Called at each opening XML tag
     *
     * @param name of the XML tag
     * @param properties the list of XML attributes
     */
    void on_start_element(const Glib::ustring& name, const AttributeList& properties) override;
    /**
     * Called at each closing XML tag
     * @param name of the XML tag
     */
    void on_end_element(const Glib::ustring& name) override;
    //  void on_characters(const Glib::ustring& characters) override;
    //  void on_comment(const Glib::ustring& text) override;
    //  void on_warning(const Glib::ustring& text) override;
    /**
     * Called when a parsing error occurs
     * @param text
     */
    void on_error(const Glib::ustring& text) override;
    /**
     * Called when a fatal parsing error occurs
     * @param text
     */
    void on_fatal_error(const Glib::ustring& text) override;
    
private:
    /**
     * Some tags must be children of others
     *
     * This method checks that the given tag is a children of another specific one
     *
     * @param name of the XML tag
     * @param properties the list of XML attributes
     * @return true if matching the subtag was successful, false otherwise
     */
    bool manage_subtags(const Glib::ustring& name, const AttributeList& properties);

    void handle_schedule(const AttributeList& properties);
    void handle_job(const AttributeList& properties);
    void handle_core(const AttributeList& properties);
    void handle_resource(const AttributeList& properties);
    void handle_stat(const AttributeList& properties);
    void handle_transmission(const AttributeList& properties);
    void handle_mem_region(const AttributeList& properties);
    void handle_mem_region_booking(const AttributeList& properties);
    void handle_resource_booking(const AttributeList& properties);
};

REGISTER_PARSER(ResXMLSaxParser, "res-xml")

#endif /* RESXMLSAXPARSER_H */


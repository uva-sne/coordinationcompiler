/*
 * Copyright (C) 2019 Benjamin Rouxel <benjamin.rouxel@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TEAMPLAYCOORDINATION_H
#define TEAMPLAYCOORDINATION_H

#include <cstdlib>
#include <cstdio>
#include <string>
#include <iostream>

#include <boost/algorithm/string/trim.hpp>
#include <antlr4-runtime.h>

#include "CoordinationLanguageLexer.h"
#include "CoordinationLanguageParser.h"
#include "CoordinationLanguageBaseVisitor.h"
#include "config.hpp"
#include "InputParser.hpp"
#include "SystemModel.hpp"
#include "Utils.hpp"
#include "Utils/Log.hpp"

#include "InputParser/EdgeHandler.hpp"
#include "InputParser/SubGraphDissolver.hpp"

/**
 * @deprecated
 * Parser for the language Coordination
 *
 * Create the IR representing the #SystemModel
 * This parser uses the lexer generated from ANTLR, following the Visitor design pattern.
 * ANTLR creates an AST which is then scanned. On each element of the AST,
 * the specific visitor function is called.
 */
class TeamplayCoordinationVisitor : public InputParser, public CoordinationLanguageBaseVisitor {
protected:
    //! Datatypes correspondances between label and real type
    std::map<std::string, std::string> datatypes;

    /*!
     * Template is saved in the map when encountering one
     * The map is referenced again when an instantiation is found.
     */
    std::map<std::string, CoordinationLanguageParser::RuleTemplatesContext *> templates;

    /* For the coming pointers be carefull not to ask for a new pointer on the object, 
        because the old one will be invalid.
     */

    //! Pointer to current subgraph, used when parsing components inside.
    CoordinationLanguageParser::RuleSubGraphContext *current_subgraph = NULL;

    /*! Pointer to current template, used when parsing components inside.
     * Can either be a component or subgraph.
     */
    CoordinationLanguageParser::RuleTemplatesContext *current_template = NULL;

    /*!
     * Pointer to current instantiation.
     */
    CoordinationLanguageParser::RuleInstantiationContext *current_instantiation = NULL;

    //! list of available sub graph profiles
    std::vector<Profile> subgraph_profiles;

    //! Edges from current scope.
    std::vector<EdgeHandler *> current_scope_edges;
    
    virtual void parse(std::istream &input) override;
    
public:
    /**
     * Constructor
     * @param o #SystemModel
     * @param c current config
     * @param cmd_properties command line properties
     */
    TeamplayCoordinationVisitor(SystemModel *o, config_t *c, CommandLineProperties *cmd_properties) : InputParser(o, c, cmd_properties), CoordinationLanguageBaseVisitor() {}
    
    /**
     * Visit the top-level element.
     *
     * @param context
     * @return nullptr
     */
    ANTLR_ANY_TYPE visitRuleApplication(CoordinationLanguageParser::RuleApplicationContext* context) override;
    /**
     * Visit and create a new component/task
     *
     * @param context
     * @return nullptr
     */
    ANTLR_ANY_TYPE visitRuleComponent(CoordinationLanguageParser::RuleComponentContext* context) override;
    /**
     * Visits the profile rules and adds the profile definitions to global scope.
     *
     * @param context
     * @return nullptr
     */
    ANTLR_ANY_TYPE visitRuleProfile(CoordinationLanguageParser::RuleProfileContext* context) override;
    /**
     * Visit and create a new component/task
     *
     * @param context
     * @return nullptr
     */
    ANTLR_ANY_TYPE visitRuleSubGraph(CoordinationLanguageParser::RuleSubGraphContext* context) override;
    /**
     * Visit and create a new component/task
     *
     * @param context
     * @return nullptr
     */
    ANTLR_ANY_TYPE visitRuleTemplates(CoordinationLanguageParser::RuleTemplatesContext* context) override;

    /**
     * Visit and create a new component/task
     *
     * @param context
     * @return nullptr
     */
    ANTLR_ANY_TYPE visitRuleVersion(CoordinationLanguageParser::RuleVersionContext* context) override;
    /**
     * Visit and create a new component/task
     *
     * @param context
     * @return nullptr
     */
    ANTLR_ANY_TYPE visitRuleDatatype(CoordinationLanguageParser::RuleDatatypeContext* context) override;
    /**
     * Visit and create a new component/task
     *
     * @param context
     * @return nullptr
     */
    ANTLR_ANY_TYPE visitRuleConnector(CoordinationLanguageParser::RuleConnectorContext *context) override;

    /**
     * Visit and create a new component/task
     *
     * @param handler
     * @return nullptr
     */
    ANTLR_ANY_TYPE visitRuleEdge(EdgeHandler *handler);
    /**
     * Visit and create a new component/task
     *
     * @param handler
     * @return nullptr
     */
    ANTLR_ANY_TYPE visitRuleSimpleEdge(EdgeHandler *handler);
    /**
     * Visit and create a new component/task
     *
     * @param handler
     * @return nullptr
     */
    ANTLR_ANY_TYPE visitRuleDuplicateEdge(EdgeHandler *handler);
    /**
     * Visit and create a new component/task
     *
     * @param handler
     * @return nullptr
     */
    ANTLR_ANY_TYPE visitRuleDataOrEdge(EdgeHandler *handler);
    /**
     * Visit and create a new component/task
     *
     * @param handler
     * @return nullptr
     */
    ANTLR_ANY_TYPE visitRuleSchedOrEdge(EdgeHandler *handler);
    /**
     * Visit and create a new component/task
     *
     * @param handler
     * @return nullptr
     */
    ANTLR_ANY_TYPE visitRuleEnvOrEdge(EdgeHandler *handler);

    /*!
     * Parse constant rule to string
     * @param context
     * @return nullptr
     */
    std::string parseConstant(CoordinationLanguageParser::RuleConstantContext* context);

    /*!
     * Handles the components suberobject which contains both normal components and subgraphs.
     * @param comps
     */
    void handleComponents(CoordinationLanguageParser::RuleComponentsContext *comps);
    /*!
     * Create template component name
     * @param subgraph_name
     * @param componentName
     * @return
     */
    static std::string templateComponentName(std::string subgraph_name, std::string componentName);
private:

    /**
     * Add dependency between src and dst. Can account for a delay_tokens with back edges
     *
     * @param src
     * @param dst
     * @param delay_tokens
     */
    void add_dependency(const CoordinationLanguageParser::ruleComponentRef_s &src, const CoordinationLanguageParser::ruleComponentRef_s &dst, uint32_t delay_tokens=0);
};

REGISTER_PARSER(TeamplayCoordinationVisitor, ".coord")

#endif /* TEAMPLAYCOORDINATION_H */


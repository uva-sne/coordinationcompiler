/* 
 * Copyright (C) 2017 Benjamin Rouxel <benjamin.rouxel@inria.fr> 
 *                    Irisa/INRIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <Exporter/FaultToleranceUppaalExporter.hpp>
#include <StateMachine/Group/StrategyScope.hpp>
#include "ConfigXmlSax.hpp"
#include "Analyse/ScheduleStats.hpp"
#include "Exporter/SVGViewer.hpp"

using namespace std;

void ConfigXmlSaxParser::on_start_document() {
    this->set_throw_messages(false);
}

void ConfigXmlSaxParser::on_end_document() {
    out->interconnect(InterConnectRegistry::Create(conf->archi.interconnect.type, const_cast<SystemModel&>(*out), const_cast<config_t&>(*conf)));
    sort(out->processors().begin(), out->processors().end(), [](ComputeUnit *a, ComputeUnit *b) { return a->id < b->id; });
}

void ConfigXmlSaxParser::on_start_element(const Glib::ustring& name, const AttributeList& properties) {
    if(name == "scheduler") {
        handle<SolverRegistry>(name, properties);
    }
    else if(name == "generator") {
        handle<GeneratorRegistry>(name, properties);
    }
    else if(name == "exporter") {
        handle<ExporterRegistry>(name, properties);
    }
    else if(name == "processor") {
        handle_config_compute_units(properties);
    }
    else if(name == "coprocessor") {
        handle_config_compute_units(properties, true);
    }
    else if(name == "resource") {
        handle_config_resource(properties);
    }
    else if(name == "voltage_island"){
        handle_voltage_island(properties);
    }
    else if(name == "base_watt"){
        handle_base_watt(properties);
    }
    else if(name == "communication") {
        handle_config_communication(properties);
    }
    else if(name == "analyse") {
        handle<AnalyseRegistry>(name, properties);
    }
    else if(name == "transform") {
        handle<TransformRegistry>(name, properties);
    }
    else if(name == "partition") {
        handle<PartitionRegistry>(name, properties);
    }
    else if(name == "priority") {
        handle<PriorityAssignmentRegistry>(name, properties);
    }
    else if(name == "simulator") {
        handle<SimulatorRegistry>(name, properties);
    }
    else if (name == "strategy-generator")
        handle_strategy_pass<StrategyGenerator>(name, properties);
    else if (name == "strategy-transform")
        handle_strategy_pass<StrategyTransform>(name, properties);
    else if (name == "strategy-exporter")
        handle_strategy_pass<StrategyExporter>(name, properties);
    else if (name == "strategy-linker")
        handle_strategy_pass<StrategyLinker>(name, properties);
    else if(name == "group")
        handle_group_open(properties);
    else if(name == "trait")
        handle_uppaal_trait(properties);
    //TODO: Additional tag for config
//    else if(name == "")
    else if(!manage_subtags(name, properties) && 
            name != "cecile" && name != "processors" && name != "datatypes" && name != "architecture" && name != "resources" && name != "voltage_islands" && name != "coprocessors" && name != "taskmodel") {
        if(name == "appl") {
            Utils::DEPRECATED("root tag `appl' is deprecated, use `cecile' instead. This will be removed in a near future.");
        }
        else if(name == "config") {
            Utils::DEPRECATED("tag `config' has been removed.");
            handle_config(properties);
        }
        else
            throw CompilerException("xml", "Unknow node: "+name);
    } else if (name == "cecile")
        handle_config(properties);
    last_open_tag.push(name);
}

bool ConfigXmlSaxParser::manage_subtags(const Glib::ustring& name, const AttributeList& properties) {
    if(!last_open_tag.size()) return false;
    if(last_open_tag.top() == "generator") {
        if(name == "mainargs") {
            handle_generator_mainargs(properties);
            return true;
        }
        else if(name == "component") {
            handle_generator_component(properties);
            return true;
        }
    }
    else if(last_open_tag.top() == "component" && name == "version") {
        handle_generator_component_version(properties);
        return true;
    }
    else if(last_open_tag.top() == "simulator") {
        handle_simulator_subtag(name, properties);
        return true;
    } 
    else if(last_open_tag.top() == "datatypes") {
        if(name == "datatype") {
            handle_config_mem_datatype(properties);
            return true;
        }
    }
    else if(last_open_tag.top() == "analyse") {
        if(name == "task" && Utils::isa<ScheduleStats*>(this->scope()->back())) {
            handle_analyse_stats_task(name, properties);
            return true;
        }
        else if(name == "length" && Utils::isa<ScheduleStats*>(this->scope()->back())) {
            handle_analyse_stats_task(name, properties);
            return true;
        }
    }
    else if(last_open_tag.top() == "exporter") {
        if(name == "marker" && Utils::isa<SVGViewer*>(this->scope()->back())) {
            handle_exporter_marker(name, properties);
            return true;
        }
    }
    else if(last_open_tag.top() == "architecture") {
        if(name == "processors" || name == "resources")
            return true;
        if(name == "communication") {
            handle_config_communication(properties);
            return true;
        }
        if(name == "spm-mgnt") {
            handle_config_spm_mgnt(properties);
            return true;
        }
    }
    else if(last_open_tag.top() == "processors") {
        if(name == "processor") {
            handle_config_compute_units(properties);
            return true;
        }
    }
    else if(last_open_tag.top() == "resources") {
        if(name == "resource") {
            handle_config_resource(properties);
            return true;
        }
    }
    else if(last_open_tag.top() == "taskmodel") {
        if(name == "let") {
            handle_task_model_subtag_let(properties);
            return true;
        }
    }
    return false;
}

void ConfigXmlSaxParser::on_end_element(const Glib::ustring& name) {
    if (last_open_tag.top() != name)
        throw CompilerException("xml", "Unbalanced xml, expected </" + last_open_tag.top() + "> but got </" + name + ">");

    if(name == "group")
        handle_group_close();

    last_open_tag.pop();
}

void ConfigXmlSaxParser::on_error(const Glib::ustring& text) {
    throw CompilerException("xml", text);
}

void ConfigXmlSaxParser::on_fatal_error(const Glib::ustring& text) {
    throw CompilerException("xml", text);
}

void ConfigXmlSaxParser::handle_config(const AttributeList& properties) {
    string time_unit, energy_unit, power_unit, frequency_unit;

    //defined in the config tag.
    for(Attribute attr : properties) {
        if(attr.name == "time_unit")
            out->time_unit(attr.value);
        else if(attr.name == "energy_unit")
            out->energy_unit(attr.value);
        else if(attr.name == "power_unit")
            out->power_unit(attr.value);
        else if(attr.name == "frequency_unit")
            out->frequency_unit(attr.value);
        else if(attr.name == "energy_model_time_step")
            out->energy_model_time_step(stoi(attr.value));
    }
}

template<> void ConfigXmlSaxParser::handle<GeneratorRegistry>(const std::string &name, const AttributeList& properties) {
    string id;
    map<string, string> params;
    for(const Attribute& attr : properties) {
        if(attr.name == "id")
            id = attr.value;
        else
            params[attr.name] = attr.value;
    }
    if(id.empty())
        throw CompilerException("parser-xml", "Generator given without id");

    if(!GeneratorRegistry::IsRegistered(id)) {
        if(!ExporterRegistry::IsRegistered(id))
            throw CompilerException("xml", "Generator "+id+" not found");
        else {
            Utils::WARN("DEPRECATED: please use <exporter id=\""+id+"\" .../> in your configuration file instead of <generator id=\""+id+"\" .../>");
            handle<ExporterRegistry>("exporter", properties);
            return;
        }
    }

    Generator *p = GeneratorRegistry::Create(id);
    p->set_params(params);
    this->scope()->add_pass(p);
}

template<> void ConfigXmlSaxParser::handle<SolverRegistry>(const std::string &name, const AttributeList& properties) {
    string id;
    map<string, string> params;

    for(const Attribute& attr : properties) {
        if(attr.name == "id")
            id = attr.value;
        else if(attr.name == "solver") {
            id = attr.value;
            Utils::DEPRECATED("Use attribute `id' instead of `solver'");
        }
        else
            params[attr.name] = attr.value;
    }

    if(id.empty())
        throw CompilerException("parser-xml", "Scheduler given without id attribute");

    if(!SolverRegistry::IsRegistered(id))
        throw CompilerException("xml", "Generator "+id+" not found");

    Solver *p = SolverRegistry::Create(id);
    p->set_params(params);
    this->scope()->add_pass(p);
}

void ConfigXmlSaxParser::handle_config_mem_datatype(const AttributeList& properties) {
    string name, sval="", ctype="", ival="";
    for(const Attribute& attr : properties) {
        if(attr.name == "id")
            name = attr.value;
        else if(attr.name == "size")
            sval = attr.value;
        else if(attr.name == "ctype")
            ctype = attr.value;
        else if(attr.name == "initval")
            ival = attr.value;
    }
    if(name.empty()) {
        Utils::WARN("missing config data-type attribute id");
        return;
    }
    boost::algorithm::replace_all(ctype, "&lt;", "<");
    boost::algorithm::replace_all(ctype, "&gt;", ">");
    boost::algorithm::replace_all(ctype, "&amp;", "&");
    boost::algorithm::replace_all(ival, "&lt;", "<");
    boost::algorithm::replace_all(ival, "&gt;", ">");
    boost::algorithm::replace_all(ival, "&amp;", "&");
    ADD_CONFIG_DATATYPES(conf, name, Utils::stringmem_to_bit(sval), ival, ctype)
}



void ConfigXmlSaxParser::handle_uppaal_trait(const AttributeList& properties) {
    string id = "";
    map<string, string> params;
    for(const Attribute& attr : properties) {
        if(attr.name == "id")
            id = attr.value;
        else
            params[attr.name] = attr.value;
    }
    if(id.empty())
        throw CompilerException("parser-xml", "Exporter given without id");

    if(!UppaalTraitRegistry::IsRegistered(id))
        throw CompilerException("xml", "Exporter "+id+" not found");

    CompilerPass *p = this->scope()->back();
    if (FaultToleranceUppaalExporter* uppaal = dynamic_cast<FaultToleranceUppaalExporter*>(p)) {
        UppaalTrait *t = UppaalTraitRegistry::Create(id);
        uppaal->traits.push_back(t);
    } else {
        throw CompilerException("xml", "Traits can only be registered for the UPPAAL exporter pass");
    }
}

void ConfigXmlSaxParser::handle_config_communication(const AttributeList& properties) {
    for(const Attribute& attr : properties) {
        if(attr.name == "type")
            conf->archi.interconnect.type = attr.value;
        else if(attr.name == "arbiter")
            conf->archi.interconnect.arbiter = attr.value;
        else if(attr.name == "active-window-time" && !attr.value.empty())
            conf->archi.interconnect.active_ress_time = stoul(attr.value);
        else if(attr.name == "bit-per-timeunit" && !attr.value.empty())
            conf->archi.interconnect.bit_per_timeunit = stoul(attr.value);
        else if(attr.name == "mechanism")
            conf->archi.interconnect.mechanism = attr.value;
        else if(attr.name == "behavior")
            conf->archi.interconnect.behavior = attr.value;
        else if(attr.name == "burst")
            conf->archi.interconnect.burst = attr.value;
    }
}

void ConfigXmlSaxParser::handle_config_spm_mgnt(const AttributeList& properties) {
    for(const Attribute& attr : properties) {
        if(attr.name == "prefetch_code")
            conf->archi.spm.prefetch_code = (attr.value == "true");
        else if(attr.name == "assign_region")
            conf->archi.spm.assign_region = (attr.value == "true");
        else if(attr.name == "store_code" || attr.name == "store-code")
            conf->archi.spm.store_code = (attr.value == "true");
    }
}

void ConfigXmlSaxParser::handle_config_compute_units(const AttributeList& properties, bool separate_coprocessor) {
    // Handles all kinds of compute units including general purpose (i.e. CPU) and accelerators (i.e. GPU).
    string id, type, sysid, _class, voltage_island_id;
    uint64_t mem_size = -1;
    for(const Attribute& attr : properties) {
        if(attr.name == "id")
            id = attr.value;
        else if(attr.name == "spm-size")
            mem_size = Utils::stringmem_to_bit(attr.value);
        else if(attr.name == "type")
            type = attr.value;
        else if(attr.name == "system-id")
            sysid = attr.value;
        else if(attr.name == "class")
            _class = attr.value;
        else if(attr.name == "voltage_island_id")
            voltage_island_id = attr.value;
    }
    if(id.empty())
        throw CompilerException("xml", "Bad parameter number for processor, missing id");
    if(out->processor(id) != nullptr)
        throw CompilerException("xml", "Processor must have a unique identifier -- "+id);

    auto *p = new ComputeUnit();
    p->id = id;
    p->spm_size = mem_size;
    p->type = type;
    if(!_class.empty())
        p->proc_class = ArchitectureProperties::from_string(_class);
    else {
        Utils::WARN("class attribute missing for processor core "+id+", default to cpu -- available cpu/gpu/fpga/other");
        p->proc_class = ArchitectureProperties::ComputeUnitType::CUT_CPU;
    }
    if(!sysid.empty()) {
        try {
            p->sysID = stoul(sysid);
        } catch(std::invalid_argument &) {
            Utils::WARN("Can't assign a system ID for cpu "+id+" -- can be ignored if no mapping needed");
            p->sysID = 0;
        }
    }
    if (!voltage_island_id.empty()) {
        p->voltage_island_id = voltage_island_id;
    }

    // Both CPUs and any kind of co-processor/accelerator are compute units.
    // Co-processors can be both processors or co-processors depending on the scheduling desired by the user.
    if(separate_coprocessor){
        p->proc_class = ArchitectureProperties::CUT_SEPARATE_COPROCESSOR;
        out->coprocessors().push_back(p);
    }else{
        out->processors().push_back(p);
    }
}

void ConfigXmlSaxParser::handle_config_resource(const AttributeList& properties) {
    string id = "", access = "";
    for(const Attribute& attr : properties) {
        if(attr.name == "id")
            id = attr.value;
        else if(attr.name == "access")
            access = attr.value;
    }
    if(id.empty())
        throw CompilerException("xml", "Bad parameter number for resource, missing id");

    if(out->resource(id) != nullptr)
        throw CompilerException("xml", "Resource must have a unique identifier -- "+id);


    ResourceUnit *r = new ResourceUnit();
    if(r == NULL)
        throw CompilerException("xml", "no more mem proc_t");

    r->id = id;
    if(access == "mutex")
        r->access = resource_access_type_e::MUTEX;
    else if(access != "free")
        throw CompilerException("xml", "Unknown access type for a resource: "+access);
    out->resources().push_back(r);
}

void ConfigXmlSaxParser::handle_voltage_island(const AttributeList& properties){
    string id, type, sysid;
    vector<pair<frequencyinfo_t , powerinfo_t>> frequency_watt;
    vector<frequencyinfo_t> frequency;
    vector<powerinfo_t> watt;
    vector<string> temp_string;

    for(Attribute attr : properties) {
        if(attr.name == "id")
            id = attr.value;
        else if(attr.name == "type")
            type = attr.value;
        else if(attr.name == "system-id")
            sysid = attr.value;
        else if(attr.name == "frequencies"){
            temp_string.clear();
            boost::algorithm::split(temp_string, attr.value, boost::is_any_of(","));
            for(auto &&item: temp_string){
                frequency.push_back(Utils::stringfrequency_to_frequency(item, out->frequency_unit()));
            }
        }else if(attr.name == "watt_increase"){
            temp_string.clear();
            boost::algorithm::split(temp_string, attr.value, boost::is_any_of(","));
            for(auto &&item: temp_string){
                watt.push_back(Utils::stringwatt_to_power(item, out->power_unit()));
            }
        }
    }
    if(id.empty())
        throw CompilerException("xml", "Bad parameter number for voltage_island, missing id");

    if(out->voltage_islands(id) != nullptr)
        throw CompilerException("xml", "Voltage Islands must have a unique identifier -- "+id);

    /* number of frequencies and watt has to be the same. the watt represents the increase in watt over the base
     consumption at idle */
    if (frequency.size() != watt.size())
        throw CompilerException("xml", "The number of frequencies ("+ to_string(frequency.size())
        +") does not match the number of watt increases ("+ to_string(watt.size()) + ").");

    frequencyinfo_t max_freq = 0;
    for(auto&& tup: boost::combine(frequency, watt)){
        frequencyinfo_t f;
        powerinfo_t w;
        boost::tie(f,w) = tup;
        frequency_watt.emplace_back(f,w);
        if (f > max_freq) max_freq = f;
    }

    auto *v = new voltage_island;
    if (v == NULL)
        throw CompilerException("xml", "no more mem");

    v->id = id;
    v->frequency_watt = frequency_watt;
    v->max_frequency = max_freq;
    if(!type.empty()){
        v->type = type;
    }
    if(!sysid.empty()){
        try{
            v->sysID = stoul(sysid);
        }catch(std::invalid_argument &) {
            Utils::WARN(
                    "Can't assign a system ID for voltage islands " + id + " -- can be ignored if no mapping needed");
            v->sysID = -1;
        }
    }
    out->voltage_islands().push_back(v);
}

void ConfigXmlSaxParser::handle_base_watt(const AttributeList& properties){
    uint128_t watt = 0;
    for(Attribute attr : properties) {
        if(attr.name == "watt")
            watt = Utils::stringwatt_to_power(attr.value, out->power_unit());
    }
    out->base_watt(watt);
}

void ConfigXmlSaxParser::handle_generator_mainargs(const AttributeList& properties) {
    CompilerPass *p = this->scope()->back();
    if(!Utils::isa<Generator*>(p))
        throw CompilerException("xml", "Mismatch between last added compiler pass and last xml open tag");

    map<string, string> args;
    for(const Attribute& attr : properties)
        args[attr.name] = attr.value;

    Generator *g = (Generator*)p;
    g->setAuxilliaryParams("mainargs", args);
}

void ConfigXmlSaxParser::handle_generator_component(const AttributeList& properties) {
    CompilerPass *p = this->scope()->back();
    if(!Utils::isa<Generator*>(p))
        throw CompilerException("xml", "Mismatch between last added compiler pass and last xml open tag");

    map<string, string> args;
    for(const Attribute& attr : properties)
        args[attr.name] = attr.value;

    Generator *g = (Generator*)p;
    g->setAuxilliaryParams("component", args);
}

void ConfigXmlSaxParser::handle_generator_component_version(const AttributeList& properties) {
    CompilerPass *p = this->scope()->back();
    if(!Utils::isa<Generator*>(p))
        throw CompilerException("xml", "Mismatch between last added compiler pass and last xml open tag");

    map<string, string> args;
    for(const Attribute& attr : properties)
        args[attr.name] = attr.value;

    Generator *g = (Generator*)p;
    g->setAuxilliaryParams("version", args);
}

void ConfigXmlSaxParser::handle_simulator_subtag(const string &subtag, const AttributeList& properties) {
    CompilerPass *p = this->scope()->back();
    if (!Utils::isa<Simulator *>(p))
        throw CompilerException("xml", "Mismatch between last added compiler pass and last xml open tag");

    map<string, string> args;
    for (const Attribute& attr: properties)
        args[attr.name] = attr.value;

    Simulator *g = (Simulator *) p;
    g->setAuxilliaryParams(subtag, args);
}

void ConfigXmlSaxParser::handle_analyse_stats_task(const string &subtag, const AttributeList& properties) {
    CompilerPass *p = this->scope()->back();
    if(!Utils::isa<ScheduleStats*>(p))
        throw CompilerException("xml", "Mismatch between last added compiler pass and last xml open tag");

    map<string, string> args;
    for(const Attribute& attr : properties)
        args[attr.name] = attr.value;

    ScheduleStats* s = (ScheduleStats*)p;
    s->setAuxilliaryParams(subtag, args);
}

void ConfigXmlSaxParser::handle_exporter_marker(const string &subtag, const AttributeList& properties) {
    CompilerPass *p = this->scope()->back();
    if(!Utils::isa<SVGViewer*>(p))
        throw CompilerException("xml", "Mismatch between last added compiler pass and last xml open tag");

    map<string, string> args;
    for(const Attribute& attr : properties)
        args[attr.name] = attr.value;

    SVGViewer* s = (SVGViewer*)p;
    s->setAuxilliaryParams(subtag, args);
}

void ConfigXmlSaxParser::handle_task_model_subtag_let(const AttributeList& properties) {
    for(const Attribute& attr : properties) {
        if(attr.name == "type") {
            if(attr.value == "AER" || attr.value == "aer")
                conf->runtime.rtt = runtime_task_e::AER;
            else if(attr.value == "PREM" || attr.value == "prem")
                conf->runtime.rtt = runtime_task_e::PREM;
            else if(attr.value == "SINGLE" || attr.value == "single")
                conf->runtime.rtt = runtime_task_e::SINGLEPHASE;
            else
                Utils::WARN("Unknow property value for the Logical Execution Time (LET) setting: "+attr.value);
        }
    }
}

void ConfigXmlSaxParser::handle_group_open(const xmlpp::SaxParser::AttributeList &properties) {
    string id;
    map<string, string> params;
    for(const Attribute& attr : properties)
        params[attr.name] = attr.value;

    id = params["id"];
    if(id.empty())
        throw CompilerException("parser-xml", "group given without id");

    if(!GroupRegistry::IsRegistered(id))
        throw CompilerException("xml", "Group with id '" + id + "' not found");

    GroupPass *p = GroupRegistry::Create(id);
    p->set_params(params);
    this->scope()->add_pass(p);

    this->_scopes.push_back({
        .dispatcher = &p->dispatcher(),
        .parent = p
    });
}

void ConfigXmlSaxParser::handle_group_close() {
    if (this->_scopes.size() > 1) {
        this->_scopes.back().parent->initialize();
        this->_scopes.pop_back();
    }
    else
        throw CompilerException("parser-xml", "Unexpected group close tag");
}

dispatcher_context_t &ConfigXmlSaxParser::scope() {
    return this->_scopes.back();
}

template<template<typename> typename StrategyPassType>
void ConfigXmlSaxParser::handle_strategy_pass(const string &name, const xmlpp::SaxParser::AttributeList &properties) {
    std::map<std::string, std::string> params = ConfigXmlSaxParser::extract_params(name, properties);
    auto[prefix, id] = StrategyStateMachine::split_id(params["id"]);

#define DECLARE_MANAGER(managerId, type)  \
    if (prefix == managerId) {            \
        return handle<typename StrategyPassType<type>::registry_t>(name, params, id); \
    }
#include "StateMachine/StrategyManagers.hpp"
#undef DECLARE_MANAGER

    throw CompilerException("parser-xml", "Unknown strategy state machine type '" + prefix + "'");
}

std::map<std::string, std::string> ConfigXmlSaxParser::extract_params(const string &name, const xmlpp::SaxParser::AttributeList &properties) {
    std::string id;
    std::map<string, string> params;
    for (const Attribute& attr : properties) {
        if(attr.name == "id")
            id = attr.value;
        params[attr.name] = attr.value;
    }
    if(id.empty())
        throw CompilerException("parser-xml", name + " given without id");
    return params;
}

/*
 * Copyright (C) 2020 Wouter Loeve <wouterloeve0@gmail.com>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SUBGRAPH_DISSOLVER_H
#define SUBGRAPH_DISSOLVER_H

#include "EdgeHandler.hpp"

#include <cstdlib>
#include <cstdio>
#include <string>
#include <iostream>

#include "CoordinationLanguageLexer.h"
#include "CoordinationLanguageParser.h"
#include "CoordinationLanguageBaseVisitor.h"
#include "InputParser/Coordination.hpp"

#include "Utils.hpp"
#include "Utils/Log.hpp"
#include "config.hpp"

/*! 
 * This class provides the functionality of dissolving subgraphs.
 */
class SubGraphDissolver {
public:
    /**
     * Replace component references of the instantiated subgraph to the right component 
     *   according to the edge that goes from the subgraph start to a component.
     * 
     * @param current_scope_edges
     * @param subgraphEdges
     * @param current_instantiation
     * @param current_subgraph
     * @return 
     */
    static std::vector<EdgeHandler *> dissolveSubGraphEdges(
        std::vector<EdgeHandler *> current_scope_edges, 
        std::vector<EdgeHandler *> subgraphEdges, 
        CoordinationLanguageParser::RuleInstantiationContext *current_instantiation,
        CoordinationLanguageParser::RuleSubGraphContext *current_subgraph
    );
    /*! Constructs edge handlers given a vector of RuleEdgeContext
     * 
     * Constructs a vector of EdgeHandler objects from a vector of Edge Contexts.
     * 
     * @param edges
     * @return
     */
    static std::vector<EdgeHandler*> constructEdgeHandlers(std::vector<CoordinationLanguageParser::RuleEdgeContext *> edges);

    /*!
     * Removes edges that reference the component string. 
     * 
     * Remove edges that reference the component name found in the component variable.
     * References in the source will be inspected if function is "from".
     * Likewise, references in the destination will be inspected if the function is "to".
     * 
     * The function string determines whether an edge to the component or from the component will be removed
     * @param component
     * @param function
     * @param subGraphEdgesContext
     * @return
     */
    static std::vector<EdgeHandler *> removeEdges(std::string component, std::string function, std::vector<EdgeHandler *> subGraphEdgesContext);

    /*!
     * Retrieves all edges from or to (based on the value of function) the given component string.
     * 
     * Returns only the edges that reference components corresponding to the given function.
     * If function is "from", the sources will be inspected.
     * Likewise if function is "to", the destinations will be looked at.
     * 
     * @param edges
     * @param component
     * @param function
     * @return
     */
    static std::vector<EdgeHandler *> filterEdge(std::vector<EdgeHandler *> edges, std::string component, std::string function);
};
#endif /* SUBGRAPH_DISSOLVER_H */

/*
 * Copyright (C) 2020 Wouter Loeve <wouterloeve0@gmail.com>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "EdgeHandler.hpp"

using namespace std;

EdgeHandler::EdgeHandler(CoordinationLanguageParser::RuleEdgeContext *edge) {
    if(edge->ruleSimpleEdge()) {
        _edge_type = simple;
        CoordinationLanguageParser::RuleSimpleEdgeContext *e = edge->ruleSimpleEdge();
        simpleObj = e->current;
        _comprefs = e->ruleComponentRef();
        _src.push_back(_comprefs[0]);
        _dest.push_back(_comprefs[1]);
        _delay = (!e->current.delay.empty()) ? stoul(e->current.delay) : 0;
    }
    else if(edge->ruleDuplicateEdge()) {
        _edge_type = duplicate;
        CoordinationLanguageParser::RuleDuplicateEdgeContext *e = edge->ruleDuplicateEdge();
        duplicateObj = e->current;
        _comprefs = e->ruleComponentRef();
        // Taken from the visitor of DuplicateEdge
        _src.push_back(_comprefs[0]);

        for (size_t i = e->ruleComponentRef().size() - 1; i > 0; --i) {
            _dest.push_back(_comprefs[i]);
        }
        _delay = (!e->current.delay.empty()) ? stoul(e->current.delay) : 0;
    }
    else if(edge->ruleDataOrEdge()) {
        CoordinationLanguageParser::RuleDataOrEdgeContext *e = edge->ruleDataOrEdge();
        dataorObj = e->current;
        _edge_type = dataor;

        for (int i = dataorObj.out.size() - 1; i >= 0; --i) {
            addSource(dataorObj.source, dataorObj.out[i], dataorObj.versi);
            addDestination(e->ruleComponentRef(i)->current);
        }

        if(getDest().size() != getSrc().size()) {
            CompilerException("EdgeHandler", "DataOrSources don't match number of destinations");
        }
        
        _delay = (!e->current.delay.empty()) ? stoul(e->current.delay) : 0;
    }
    else if(edge->ruleSchedOrEdge()) {
        throw Todo("visitSchedOrEdge");
    }
    else if(edge->ruleEnvOrEdge()) {
        throw Todo("visitRuleEnvOrEdge");
    }
}

vector<CoordinationLanguageParser::RuleComponentRefContext*> EdgeHandler::matchEndpoints(vector<CoordinationLanguageParser::RuleComponentRefContext*> list,
        string component) {
    vector<CoordinationLanguageParser::RuleComponentRefContext*> matched;

    for (CoordinationLanguageParser::RuleComponentRefContext* ref : list) {
        if(ref->current.comp == component) {
            matched.push_back(ref);
        }
    }

    return matched;
}

vector<CoordinationLanguageParser::RuleComponentRefContext*> EdgeHandler::getCompRefs() {
    return _comprefs;
}

CoordinationLanguageParser::RuleComponentRefContext* EdgeHandler::addCompRef(string component, string connector, string side, string versi) {
    CoordinationLanguageParser::RuleComponentRefContext* toAdd =
            (CoordinationLanguageParser::RuleComponentRefContext*) calloc(1, sizeof (CoordinationLanguageParser::RuleComponentRefContext));
    toAdd->current.comp = component;
    toAdd->current.conn = connector;

    if(!versi.empty()) {
        toAdd->current.versi = versi;
    }

    return addCompRefToList(side, toAdd);
}

CoordinationLanguageParser::RuleComponentRefContext* EdgeHandler::addCompRef(CoordinationLanguageParser::ruleComponentRef_s ref, string side) {
    CoordinationLanguageParser::RuleComponentRefContext* toAdd =
            (CoordinationLanguageParser::RuleComponentRefContext*) calloc(1, sizeof (CoordinationLanguageParser::RuleComponentRefContext));
    toAdd->current = ref;

    return addCompRefToList(side, toAdd);
}

CoordinationLanguageParser::RuleComponentRefContext* EdgeHandler::addCompRefToList(string side, CoordinationLanguageParser::RuleComponentRefContext* toAdd) {
    if(side == "src" || side == "source") {
        _src.push_back(toAdd);
    }
    else if(side == "dst" || side == "dest" || side == "destination") {
        _dest.push_back(toAdd);
    }
    else {
        CompilerException("EdgeHandler", "Side must be destination or source");
    }
    return toAdd;
}

CoordinationLanguageParser::RuleComponentRefContext* EdgeHandler::addSource(string component, string connector) {
    return addCompRef(component, connector, "source", "");
}

CoordinationLanguageParser::RuleComponentRefContext* EdgeHandler::addSource(string component, string connector, string versi) {
    return addCompRef(component, connector, "source", versi);
}

CoordinationLanguageParser::RuleComponentRefContext* EdgeHandler::addDestination(string component, string connector) {
    return addCompRef(component, connector, "destination", "");
}

CoordinationLanguageParser::RuleComponentRefContext* EdgeHandler::addDestination(CoordinationLanguageParser::ruleComponentRef_s ref) {
    return addCompRef(ref, "destination");
}

bool EdgeHandler::replaceEndpoints(vector<CoordinationLanguageParser::RuleComponentRefContext*> list,
        string component, string connector, string replaceComponent) {
    bool replaced = false;
    for (CoordinationLanguageParser::RuleComponentRefContext* ref : list) {
        if(ref->current.comp == component && (connector.empty() || ref->current.conn == connector)) {
            replaced = true;
            ref->current.comp = replaceComponent;
        }
    }

    return replaced;
}

bool EdgeHandler::replaceFromEdge(string component, string connector, string replaceComponent) {
    return replaceEndpoints(_src, component, connector, replaceComponent);
}

bool EdgeHandler::replaceToEdge(string component, string connector, string replaceComponent) {
    return replaceEndpoints(_dest, component, connector, replaceComponent);
}

bool EdgeHandler::isToEdge(string component) {
    return matchEndpoints(_dest, component).size() > 0;
}

bool EdgeHandler::isFromEdge(string component) {
    return matchEndpoints(_src, component).size() > 0;
}

uint32_t EdgeHandler::getDelay() {
    return _delay;
}

vector<CoordinationLanguageParser::ruleComponentRef_s> EdgeHandler::getSrc() {
    return getEndpoints(_src);
}

CoordinationLanguageParser::ruleComponentRef_s EdgeHandler::getDest(size_t i) {
    if(getDest().size() <= i) {
        throw CompilerException("EdgeHandler", "Index does not exist in destination edgerefs");
    }
    return getDest()[i];
}

CoordinationLanguageParser::ruleComponentRef_s EdgeHandler::getSrc(size_t i) {
    if(getSrc().size() <= i) {
        throw CompilerException("EdgeHandler", "Index does not exist in source edgerefs");
    }
    return getSrc()[i];
}

vector<CoordinationLanguageParser::ruleComponentRef_s> EdgeHandler::getDest() {
    return getEndpoints(_dest);
}

vector<CoordinationLanguageParser::ruleComponentRef_s> EdgeHandler::getEndpoints(
        vector<CoordinationLanguageParser::RuleComponentRefContext *> list) {

    vector<CoordinationLanguageParser::ruleComponentRef_s> endpoints;
    for (CoordinationLanguageParser::RuleComponentRefContext *s : list) {
        endpoints.push_back(s->current);
    }

    return endpoints;
}

EdgeType EdgeHandler::getType() {
    return _edge_type;
}

string EdgeHandler::comp_ref_list_to_string(vector<CoordinationLanguageParser::RuleComponentRefContext*> list) {
    string result;
    for (CoordinationLanguageParser::RuleComponentRefContext* ref : list) {
        result += ref->current.comp + "." + ref->current.conn + "\n";
    }

    return result;
}

string EdgeHandler::to_string() {
    string result = "source: \n" + comp_ref_list_to_string(_src);
    result += "destinations: \n" + comp_ref_list_to_string(_dest);
    return result;
}
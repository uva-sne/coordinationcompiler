/* 
 * Copyright (C) 2017 Benjamin Rouxel <benjamin.rouxel@inria.fr> 
 *                    Irisa/INRIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <libxml++-2.6/libxml++/parsers/saxparser.h>

#include "ResXmlSax.hpp"
#include "ScheduleFactory.hpp"

using namespace std;

ResXMLSaxParser::~ResXMLSaxParser() {
}

void ResXMLSaxParser::on_start_document() {
    if(_schedule == nullptr)
        throw CompilerException("result XML", "For that particular parser, you need to set the schedule first");
    this->set_throw_messages(false);
}

void ResXMLSaxParser::on_end_document() {
    for(pair<SchedJob*, AttributeList> el : trans_delayed) {
        current_job = el.first;
        handle_transmission(el.second);
    }
    for(pair<SchedSpmRegion*, AttributeList> el : mem_book_delayed) {
        current_spm_region = el.first;
        handle_mem_region_booking(el.second);
    }
    for(pair<SchedResource*, AttributeList> el : resource_delayed) {
        current_resource = el.first;
        handle_resource_booking(el.second);
    }
}

void ResXMLSaxParser::on_start_element(const Glib::ustring& name, const AttributeList& properties) {
    if(name == "schedule")
        handle_schedule(properties);
    else if(name == "job")
        handle_job(properties);
    else if(name == "core")
        handle_core(properties);
    else if(name == "shared_resource")
        handle_resource(properties);
    else if(name == "stat")
        handle_stat(properties);
    else if(!manage_subtags(name, properties)) 
        throw CompilerException("xml", "Unknow node: "+name);

    last_open_tag.push(name);
}

bool ResXMLSaxParser::manage_subtags(const Glib::ustring& name, const AttributeList& properties) {
    if(!last_open_tag.size()) return true;

    if(last_open_tag.top() == "job") {
        if(name == "transmission") {
            trans_delayed.push_back(make_pair(current_job, properties));
//            handle_transmission(properties);
            return true;
        }
    }
    else if(last_open_tag.top() == "core") {
        if(name == "memory_region") {
            handle_mem_region(properties);
            return true;
        }
    }
    else if(last_open_tag.top() == "memory_region") {
        if(name == "booked") {
            mem_book_delayed.push_back(make_pair(current_spm_region, properties));
//            handle_mem_region_booking(properties);
            return true;
        }
    }
    else if(last_open_tag.top() == "shared_resource") {
        if(name == "booked") {
            resource_delayed.push_back(make_pair(current_resource, properties));
//            handle_resource_booking(properties);
            return true;
        }
    }
    
    return false;
}

void ResXMLSaxParser::on_end_element(const Glib::ustring& name) {
    last_open_tag.pop();
    if(name == "job")
        current_job = nullptr;
    else if(name == "core")
        current_core = nullptr;
    else if(name == "memory_region")
        current_spm_region = nullptr;
    else if(name == "shared_resource")
        current_resource = nullptr;
}

void ResXMLSaxParser::on_error(const Glib::ustring& text) {
    throw CompilerException("xml", text);
}

void ResXMLSaxParser::on_fatal_error(const Glib::ustring& text) {
    throw CompilerException("xml", text);
}

//void ResXMLSaxParser::handle_scheduler(const AttributeList& properties) {
//    string id;
//    map<string, string> params;
//    
//    for(const Attribute &attr : properties) {
//        if(attr.name == "id")
//            id = attr.value;
//        else if(attr.name == "solver") {
//            id = attr.value;
//            Utils::DEPRECATED("Use attribute `id' instead of `solver'");
//        }
//        else
//            params[attr.name] = attr.value;
//    }
//    
//    if(id.empty())
//        throw CompilerException("parser-xml", "Scheduler given without id attribute");
//    
//    if(!SolverRegistry::IsRegistered(id))
//        throw CompilerException("xml", "Generator "+id+" not found");
//    
//    Solver *p = SolverRegistry::Create(id);
//    p->set_params(params);
//    conf->passes.push_back(p);
//}

void ResXMLSaxParser::handle_schedule(const AttributeList& properties) {
    for(const Attribute &attr : properties) {
        if(attr.name == "time")
            _schedule->solvetime(stof(attr.value));
        else if(attr.name == "status") 
            _schedule->status(ScheduleProperties::status_from_string(attr.value));
        else if(attr.name == "length")
            _schedule->makespan(Utils::stringtime_to_ns(attr.value));
    }
}
void ResXMLSaxParser::handle_job(const AttributeList& properties) {
    string id, task, version, release, start, exectime, endtime, coreid;
    for(const Attribute &attr : properties) {
        if(attr.name == "id")
            id = attr.value;
        else if(attr.name == "task")
            task = attr.value;
        else if(attr.name == "version")
            version = attr.value;
        else if(attr.name == "exectime")
            exectime = attr.value;
        else if(attr.name == "release")
            release = attr.value;
        else if(attr.name == "start")
            start = attr.value;
        else if(attr.name == "endtime")
            endtime = attr.value;
        else if(attr.name == "core")
            coreid = attr.value;
    }
    
    ComputeUnit *c = out->processor(coreid);
    if(c == nullptr)
        throw CompilerException("result XML", "Can't find core id "+coreid);
    SchedCore *sc = ScheduleFactory::add_core(_schedule, c);
    
    Task *t = out->task(task);
    if(t == nullptr)
        throw CompilerException("result XML", "Can't find task id "+task);
    
    SchedJob *j = ScheduleFactory::build_job(_schedule, t, Utils::stringtime_to_ns(release));
    j->selected_version(version);
    j->rt(Utils::stringtime_to_ns(start));
    j->wct(Utils::stringtime_to_ns(exectime));
    _schedule->schedule(j);
    _schedule->map_core(j, sc);
    
    current_job = j;
    
    if(id != j->id())
        Utils::WARN("Job ID differs: "+id+" != "+j->id()+" --- might general problems later");
}
void ResXMLSaxParser::handle_core(const AttributeList& properties) {
    string coreid;
    for(const Attribute &attr : properties) {
        if(attr.name == "id")
            coreid = attr.value;
    }
    ComputeUnit *c = out->processor(coreid);
    if(c == nullptr)
        throw CompilerException("result XML", "Can't find core id "+coreid);
    current_core = ScheduleFactory::add_core(_schedule, c);
}
void ResXMLSaxParser::handle_resource(const AttributeList& properties) {
    string resid;
    for(const Attribute &attr : properties) {
        if(attr.name == "id")
            resid = attr.value;
    }
    ResourceUnit *r = out->resource(resid);
    if(r == nullptr)
        throw CompilerException("result XML", "Can't find resource id "+resid);
    current_resource = ScheduleFactory::add_resource(_schedule, r);
}
void ResXMLSaxParser::handle_stat(const AttributeList& properties) {
    string label, value;
    for(const Attribute &attr : properties) {
        if(attr.name == "label")
            label = attr.value;
        else if(attr.name == "value")
            value = attr.value;
    }
    _schedule->stats(label, value);
}
void ResXMLSaxParser::handle_transmission(const AttributeList& properties) {
    if(current_job == nullptr)
        throw CompilerException("result XML", "transmission called without being in a task");
    string type, release, delay, job, data, datatype, conc, packetnum;
    for(const Attribute &attr : properties) {
        if(attr.name == "type")
            type = attr.value;
        else if(attr.name == "release")
            release = attr.value;
        else if(attr.name == "delay")
            delay = attr.value;
        else if(attr.name == "job")
            job = attr.value;
        else if(attr.name == "data")
            data = attr.value;
        else if(attr.name == "datatype")
            datatype = attr.value;
        else if(attr.name == "conc")
            conc = attr.value;
        else if(attr.name == "packetnum")
            packetnum = attr.value;
    }
    
    SchedJob *st = current_job;
    if(!job.empty()) {
        st = _schedule->schedjob(job);
        if(st == nullptr)
            throw CompilerException("result XML", "Can't find job "+job);
    }
    
    if(packetnum.empty())
        packetnum = "0";
    
    SchedPacket *p = ScheduleFactory::build_packet(_schedule, current_job, current_job->min_rt_period(), type, st, stoi(packetnum));
    _schedule->schedule(p);
    p->schedtask(current_job);
    p->rt(Utils::stringtime_to_ns(release));
    p->wct(Utils::stringtime_to_ns(delay));
    
    p->data(Utils::stringmem_to_bit(data));
    p->datatype(datatype);
//    p->concurrency(stoul(conc));
}
void ResXMLSaxParser::handle_mem_region(const AttributeList& properties) {
    if(current_core == nullptr)
        throw CompilerException("result XML", "Mem region found outside of a core");
    string id, size;
    for(const Attribute &attr : properties) {
        if(attr.name == "id")
            id = attr.value;
        else if(attr.name == "size")
            size = attr.value;
    }
    
    current_spm_region = current_core->add_spmregion(id, Utils::stringmem_to_bit(size));
}
void ResXMLSaxParser::handle_mem_region_booking(const AttributeList& properties) {
    if(current_spm_region == nullptr)
        throw CompilerException("result XML", "Mem region booking found outside of a mem region");
    string by, start, end;
    for(const Attribute &attr : properties) {
        if(attr.name == "by")
            by = attr.value;
        else if(attr.name == "start")
            start = attr.value;
        else if(attr.name == "end")
            end = attr.value;
    }
    
    current_spm_region->book(by, Utils::stringtime_to_ns(start), Utils::stringtime_to_ns(end));
}
void ResXMLSaxParser::handle_resource_booking(const AttributeList& properties) {
    if(current_resource == nullptr)
        throw CompilerException("result XML", "Resource booking found outside of a resource");
    string by, start, end;
    for(const Attribute &attr : properties) {
        if(attr.name == "by")
            by = attr.value;
        else if(attr.name == "start")
            start = attr.value;
        else if(attr.name == "end")
            end = attr.value;
    }
    
    current_resource->book(by, Utils::stringtime_to_ns(start), Utils::stringtime_to_ns(end));
}
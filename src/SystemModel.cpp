/*!
 * \file SystemModel.cpp
 * \copyright GNU Public License v3.
 * \date 2017
 * \author Benjamin Rouxel <benjamin.rouxel@inria.fr> Irisa/INRIA
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "SystemModel.hpp"
#include "Utils/Graph.hpp"

using namespace std;

size_t Task::task_counter = 0;
size_t Version::version_counter = 0;
size_t Phase::phase_counter = 0;

const std::function<Connector* (Connection *)> Task::connector_from = [](Connection* conn) { return conn->from(); };
const std::function<Connector* (Connection *)> Task::connector_to = [](Connection* conn) { return conn->to(); };

/******************************************************************************/

ostream& operator<< (ostream& os, const Task &a) { return os << a.ilp_label(); }
ostream& operator<< (ostream& os, const Task *a) { return os << a->ilp_label(); }

string operator+ (const string *a, const Task &b) { return *a+ b.ilp_label(); }
string operator+ (const string &a, const Task *b) { return a+ b->ilp_label(); }
string operator+ (const string &a, const Task &b) { return a+ b.ilp_label(); }
string operator+ (const Task *b, const string &a) { return b->ilp_label() + a; }
string operator+ (const Task &b, const string *a) { return b.ilp_label() + *a; }
string operator+ (const Task &b, const string &a) { return b.ilp_label() + a; }

bool operator== (const Task *a, const Task &b) { return a->id() == b.id(); }
bool operator== (const Task &a, const Task *b) { return a.id() == b->id(); }
bool operator== (const Task &a, const Task &b) { return a.id() == b.id(); }

bool operator!= (const Task *a, const Task &b) { return a->id() != b.id(); }
bool operator!= (const Task &a, const Task *b) { return a.id() != b->id(); }
bool operator!= (const Task &a, const Task &b) { return a.id() != b.id(); }

bool operator== (const Task *a, const string &b) { return a->id() == b; }
bool operator== (const Task &a, const string *b) { return a.id() == *b; }
bool operator== (const Task &a, const string &b) { return a.id() == b; }

//------------------------------------------------------------------------------

ostream& operator<< (ostream& os, const Version &a) { return os << a.ilp_label(); }
ostream& operator<< (ostream& os, const Version *a) { return os << a->ilp_label(); }

string operator+ (const string *a, const Version &b) { return *a+ b.ilp_label(); }
string operator+ (const string &a, const Version *b) { return a+ b->ilp_label(); }
string operator+ (const string &a, const Version &b) { return a+ b.ilp_label(); }

string operator+ (const Version *b, const string &a) { return b->ilp_label() + a; }
string operator+ (const Version &b, const string *a) { return b.ilp_label() + *a; }
string operator+ (const Version &b, const string &a) { return b.ilp_label() + a; }

bool operator== (const Version *a, const Version &b) { return a->task() == b.task() && a->id() == b.id(); }
bool operator== (const Version &a, const Version *b) { return a.task() == b->task() && a.id() == b->id(); }
bool operator== (const Version &a, const Version &b) { return a.task() == b.task() && a.id() == b.id(); }

bool operator!= (const Version *a, const Version &b) { return a->task() != b.task() || a->id() != b.id(); }
bool operator!= (const Version &a, const Version *b) { return a.task() != b->task() || a.id() != b->id(); }
bool operator!= (const Version &a, const Version &b) { return a.task() != b.task() || a.id() != b.id(); }

bool operator== (const Version *a, const string &b) { return a->id() == b; }
bool operator== (const Version &a, const string *b) { return a.id() == *b; }
bool operator== (const Version &a, const string &b) { return a.id() == b; }

//------------------------------------------------------------------------------

bool operator== (const Connector *a, const string &b) { return a->id() == b; }
bool operator== (const Connector &a, const string *b) { return a.id() == *b; }
bool operator== (const Connector &a, const string &b) { return a.id() == b; }

bool operator!= (const Connector *a, const string &b) { return a->id() != b; }
bool operator!= (const Connector &a, const string *b) { return a.id() != *b; }
bool operator!= (const Connector &a, const string &b) { return a.id() != b; }

//------------------------------------------------------------------------------
ostream& operator<< (ostream& os, const Phase &a) { return os << a.ilp_label(); }
ostream& operator<< (ostream& os, const Phase *a) { return os << a->ilp_label(); }

string operator+ (const string *a, const Phase &b) { return *a+ b.ilp_label(); }
string operator+ (const string &a, const Phase *b) { return a+ b->ilp_label(); }
string operator+ (const string &a, const Phase &b) { return a+ b.ilp_label(); }

string operator+ (const Phase *b, const string &a) { return b->ilp_label() + a; }
string operator+ (const Phase &b, const string *a) { return b.ilp_label() + *a; }
string operator+ (const Phase &b, const string &a) { return b.ilp_label() + a; }

bool operator== (const Phase *a, const Phase &b) { return a->version() == b.version() && a->id() == b.id(); }
bool operator== (const Phase &a, const Phase *b) { return a.version() == b->version() && a.id() == b->id(); }
bool operator== (const Phase &a, const Phase &b) { return a.version() == b.version() && a.id() == b.id(); }

bool operator!= (const Phase *a, const Phase &b) { return a->version() != b.version() || a->id() != b.id(); }
bool operator!= (const Phase &a, const Phase *b) { return a.version() != b->version() || a.id() != b->id(); }
bool operator!= (const Phase &a, const Phase &b) { return a.version() != b.version() || a.id() != b.id(); }

bool operator== (const Phase *a, const string &b) { return a->id() == b; }
bool operator== (const Phase &a, const string *b) { return a.id() == *b; }
bool operator== (const Phase &a, const string &b) { return a.id() == b; }

//------------------------------------------------------------------------------

ostream& operator<< (ostream& os, const ComputeUnit& a) { return os << a.id; }
ostream& operator<< (ostream& os, const ComputeUnit* a) { return os << a->id; }

string operator+ (const string *a, const ComputeUnit &b) { return *a+ b.id; }
string operator+ (const string &a, const ComputeUnit *b) { return a+ b->id; }
string operator+ (const string &a, const ComputeUnit &b) { return a+ b.id; }

string operator+ (const ComputeUnit *a, const string &b) { return a->id + b; }
string operator+ (const ComputeUnit &a, const string *b) { return a.id + *b; }
string operator+ (const ComputeUnit &a, const string &b) { return a.id + b; }

bool operator== (const ComputeUnit *a, const ComputeUnit &b) { return a->id == b.id; }
bool operator== (const ComputeUnit &a, const ComputeUnit *b) { return a.id == b->id; }
bool operator== (const ComputeUnit &a, const ComputeUnit &b) { return a.id == b.id; }

bool operator!= (const ComputeUnit *a, const ComputeUnit &b) { return a->id != b.id; }
bool operator!= (const ComputeUnit &a, const ComputeUnit*b) { return a.id != b->id; }
bool operator!= (const ComputeUnit &a, const ComputeUnit &b) { return a.id != b.id; }

bool operator == (const ComputeUnit *a, const string &b) { return a->id == b; }
bool operator == (const ComputeUnit &a, const string *b) { return a.id == *b; }
bool operator == (const ComputeUnit &a, const string &b) { return a.id == b; }

//------------------------------------------------------------------------------

ostream& operator<< (ostream& os, const ResourceUnit& a) { return os << a.id; }
ostream& operator<< (ostream& os, const ResourceUnit* a) { return os << a->id; }

string operator+ (const string *a, const ResourceUnit &b) { return *a+ b.id; }
string operator+ (const string &a, const ResourceUnit *b) { return a+ b->id; }
string operator+ (const string &a, const ResourceUnit &b) { return a+ b.id; }

string operator+ (const ResourceUnit *a, const string &b) { return a->id + b; }
string operator+ (const ResourceUnit &a, const string *b) { return a.id + *b; }
string operator+ (const ResourceUnit &a, const string &b) { return a.id + b; }

bool operator== (const ResourceUnit *a, const ComputeUnit &b) { return a->id == b.id; }
bool operator== (const ResourceUnit &a, const ComputeUnit *b) { return a.id == b->id; }
bool operator== (const ResourceUnit &a, const ComputeUnit &b) { return a.id == b.id; }

bool operator!= (const ResourceUnit *a, const ComputeUnit &b) { return a->id != b.id; }
bool operator!= (const ResourceUnit &a, const ComputeUnit*b) { return a.id != b->id; }
bool operator!= (const ResourceUnit &a, const ComputeUnit &b) { return a.id != b.id; }

bool operator == (const ResourceUnit *a, const string &b) { return a->id == b; }
bool operator == (const ResourceUnit &a, const string *b) { return a.id == *b; }
bool operator == (const ResourceUnit &a, const string &b) { return a.id == b; }

//------------------------------------------------------------------------------
ostream& operator<< (ostream& os, const voltage_island& a) { return os << a.id; }
ostream& operator<< (ostream& os, const voltage_island* a) { return os << a->id; }
string operator+ (const string &a, const voltage_island &b) { return a+ b.id; }
string operator+ (const voltage_island &b, const string &a) { return b.id + a; }
string operator+ (const string &a, const voltage_island *b) { return a+ b->id; }
string operator+ (const voltage_island *b, const string &a) { return b->id + a; }
string operator+ (const char* a, const voltage_island &b) { return a+ b.id; }
string operator+ (const voltage_island &b, const char* a) { return b.id + a;}

bool operator== (const voltage_island *a, const voltage_island &b) { return a->id == b.id; }
bool operator== (const voltage_island &a, const voltage_island *b) { return a.id == b->id; }
bool operator== (const voltage_island &a, const voltage_island &b) { return a.id == b.id; }

bool operator!= (const voltage_island *a, const voltage_island &b) { return a->id != b.id; }
bool operator!= (const voltage_island &a, const voltage_island*b) { return a.id != b->id; }
bool operator!= (const voltage_island &a, const voltage_island &b) { return a.id != b.id; }

bool operator== (const voltage_island *a, const string &b) { return a->id == b; }
bool operator== (const voltage_island &a, const string *b) { return a.id == *b; }
bool operator== (const voltage_island &a, const string &b) { return a.id == b; }

/******************************************************************************/

#include <cstdio>
#include <cstdlib>
#include <iostream>

void Connector::clone(const Connector& rhs) {
    _id = rhs._id;
    _tokens = rhs._tokens;
    _token_type = rhs._token_type;
    _part_of_cond_edge = rhs._part_of_cond_edge;
    _type = rhs._type;
    _tainted = rhs._tainted;
}

Connector::~Connector() {
}

Phase::Phase(Version *v, string i) : _version(v), _id(i) {
    if(Utils::getLevel() == Utils::Level::DEBUG) {
        _ilp_label = i;
    }
    else {
        _ilp_label = "p"+to_string(++phase_counter);
    }
    _memory_code_size = 0;
    _cname = v->task()->id();
}

Phase::~Phase() {
    _version = nullptr;
}

void Phase::clone(const Phase &rhs) {
    _version = rhs._version;
    _id = rhs._id;
    _C = rhs._C;
    _C_E = rhs._C_E;
//    _ilp_label = rhs._ilp_label;
    _memory_code_size = rhs._memory_code_size;
    _memory_footprint = rhs._memory_footprint;
    _local_memory_storage = rhs._local_memory_storage;
    
    _type = rhs._type;

    _order = rhs._order;
    
    _cname = rhs._cname;
    _cfile = rhs._cfile;
    _csignature = rhs._csignature;
    _cbinary = rhs._cbinary;
    
    _force_mapping_proc.clear();
    for(ComputeUnit *p : rhs._force_mapping_proc)
        _force_mapping_proc.insert(p);
    for(ResourceUnit *p : rhs._resource_usage)
        _resource_usage.push_back(p);
}

void Phase::force_mapping_proc(ComputeUnit *p) {
    _force_mapping_proc.insert(p);
}

void Phase::resource_usage(ResourceUnit *p) {
    if(find(_resource_usage.begin(), _resource_usage.end(), p) == _resource_usage.end())
        _resource_usage.push_back(p);
}

bool Phase::canRunOn(ComputeUnit *cu) {
    // No proc mapping -> runs everywhere
    return this->_force_mapping_proc.empty() || (this->_force_mapping_proc.count(cu) == 1);
}

void Phase::classify() {
    if(_force_mapping_proc.size() > 0 && all_of(_force_mapping_proc.begin(), _force_mapping_proc.end(), [](ComputeUnit *c) {return c->proc_class != ArchitectureProperties::ComputeUnitType::CUT_CPU;})) {
        _type = TaskProperties::TaskType::TT_SUBMITTEE;
        vector<Phase*> pred = predecessor_phases();
        for(Phase *ps : pred) {
            if(ps->_type == TaskProperties::TaskType::TT_REGULAR)
                ps->_type = TaskProperties::TaskType::TT_SUBMITTER;
        }
    }
}

vector<Phase*> Phase::successor_phases() {
    vector<Phase*> succ;
    for(Phase *p : _version->phases()) {
        if(p->order() == _order+1) {
            succ.push_back(p);
            break;
        }
    }
    
    if(succ.empty()) {
        Task *t = _version->task();
        for(Task::dep_t els : t->successor_tasks()) {
            for(Version *v : els.first->versions()) {
                for(Phase *p : v->phases()) {
                    if(p->order() == 0) {
                        succ.push_back(p);
                        break;
                    }
                }
            }
        }
    }
    
    return succ;
}

vector<Phase*> Phase::predecessor_phases() {
    vector<Phase*> pred;
    for(Phase *p : _version->phases()) {
        if(p->order() == _order-1) {
            pred.push_back(p);
            break;
        }
    }
    
    if(pred.empty()) {
        Task *t = _version->task();
        for(Task::dep_t elp : t->predecessor_tasks()) {
            for(Version *v : elp.first->versions()) {
                for(Phase *p : v->phases()) {
                    if(p->order() == v->phases().size()-1) {
                        pred.push_back(p);
                        break;
                    }
                }
            }
        }
    }
    
    return pred;
}

Version::Version(Task *t, string i) : _task(t), _id(i) {
    Phase *p = new Phase(this, CONFIG_DEF_VERSION_NAME);
    _phases[CONFIG_DEF_VERSION_NAME] = p;
    _security_lvl = 0;
    if(Utils::getLevel() == Utils::Level::DEBUG) {
        _ilp_label = i;
    }
    else {
        _ilp_label = "v"+to_string(++version_counter);
    }
}

Version::~Version() {
    _force_previous.clear();
    _force_successors.clear();
    _task = nullptr;
}

void Version::clone(const Version &rhs) {
    _task = rhs._task;
    _id = rhs._id;
//    _ilp_label = rhs._ilp_label;
    
    _security_lvl = rhs._security_lvl;
    
    _force_previous.clear();
    for(Version *v : rhs._force_previous)
        _force_previous.push_back(v);
    
    _force_successors.clear();
    for(Version *v : rhs._force_successors)
        _force_successors.push_back(v);
    
    _phases.clear();
    for(pair<string, Phase*> elp : rhs._phases) {
        Phase *np = new Phase(*(elp.second));
        np->_version = this;
        _phases[elp.first] = np;
    }
}

void Version::task(Task *nt) {
    if(_task != nullptr)
        throw CompilerException("version", "You shoudln't change the task assigned to a version");
    _task = nt;
}

void Version::add_phase(Phase *p) {
    // The default phase is here for retro-compatibility, and for task model
    // that do not deal with phases.
    // If the task model deals with phases, then there is no need to keep the default
    // added one, many should be added
    if(_phases.count(CONFIG_DEF_VERSION_NAME)) {
        rem_phase(_phases[CONFIG_DEF_VERSION_NAME]);
    }
    p->_order = _phases.size();
    _phases[p->_id] = p;
}

void Version::rem_phase(Phase *p) {
    _phases.erase(p->id());
    delete p; //???
}

vector<Phase*> Version::phases() {
    // TODO don't make so many needless copies of internal data structures
    vector<Phase*> tmp = Utils::mapvalues(_phases);
    sort(tmp.begin(), tmp.end(), [](Phase *a, Phase *b) {return a->order() < b->order(); });
    return tmp;
}

void Version::force_mapping_proc(ComputeUnit* val) {
    for(Phase *p : phases())
        p->force_mapping_proc(val);
}
void Version::resource_usage(ResourceUnit* val) {
    for(Phase *p : phases())
        p->resource_usage(val);
}

void Version::classify() {
    for(Phase *p : phases())
        p->classify();
}

bool Version::canRunOn(ComputeUnit *cu) {
    for (const pair<const basic_string<char>, Phase *>& phase : this->_phases) {
        if (!phase.second->canRunOn(cu))
            return false;
    }
    return true;
}

void Task::clone(Task *rhs) {
// no clonage of ids as it will mess up stuffs, they are attributed at the construction of the task
//    _id = rhs->_id; 
//    _ilp_label = rhs->_ilp_label;
    
    _T = rhs->_T;
    _D = rhs->_D;
    _offset = rhs->_offset;
    _tainted = rhs->_tainted;
    
    _repeat = rhs->_repeat;
    
    _data_read = rhs->_data_read;
    _data_written = rhs->_data_written;
    
    _force_read_delay = rhs->_force_read_delay;
    _force_write_delay = rhs->_force_write_delay;
    
    _versions.clear();
    for(Version *u : rhs->versions()) {
        Version *v = new Version(*u);
        v->_task = this;
        _versions.insert({v->id(), v});
    }
    
    _stateful = rhs->_stateful;
    _delay_tokens.clear();
    for(Task::dtok_t el : rhs->_delay_tokens)
        _delay_tokens.insert({el.first, el.second});
        
    _conditional_out_groups.clear();
    for(conditional_edge_t ce : rhs->_conditional_out_groups) {
        conditional_edge_t nce = {.cond = ce.cond, .succs = ce.succs };
        _conditional_out_groups.push_back(nce);
    }
    
    _is_conditional_task = rhs->_is_conditional_task;
    
    _profile.merge(rhs->_profile);
    
    _parent_task = rhs->parent_task();
    _child_tasks.clear();
    _child_tasks.insert(_child_tasks.begin(), rhs->child_tasks().begin(), rhs->child_tasks().end());
}

void Task::deep_clone(Task *rhs) {
    clone(rhs);
    
    _inputs.clear();
    for(Connector *el : rhs->_inputs) {
        Connector *c = new Connector(*el);
        _inputs.push_back(c);
    }
    
    _outputs.clear();
    for(Connector *el : rhs->_outputs) {
        Connector *c = new Connector(*el);
        _outputs.push_back(c);
    }
    
    _predecessor_tasks.clear();
    for(Task::dep_t el : rhs->_predecessor_tasks) {
        for (Connection *conn : el.second) {
            auto inport = find_if(_inputs.begin(), _inputs.end(), [conn](Connector *a) { return a->id() == conn->to()->id(); });
            if (inport == _inputs.end())
                throw CompilerException("SM", "Can't find " + conn->to()->id());

            auto outport = find_if(el.first->_outputs.begin(), el.first->_outputs.end(), [conn](Connector *a) { return a->id() == conn->from()->id(); });
            if (outport == el.first->outputs().end())
                throw CompilerException("SM", "Can't find " + conn->from()->id());

            auto *newconn = new Connection(*outport, *inport);
            this->_predecessor_tasks[el.first].push_back(newconn);
            el.first->_successor_tasks[this].push_back(newconn);
        }
    }

    for(Task::dep_t el : rhs->_previous_virtual) {
        for (Connection *conn : el.second) {
            auto inport = find_if(_inputs.begin(), _inputs.end(), [conn](Connector *a) { return a->id() == conn->to()->id(); });
            if (inport == _inputs.end())
                throw CompilerException("SM", "Can't find " + conn->to()->id());

            auto outport = find_if(el.first->_outputs.begin(), el.first->_outputs.end(), [conn](Connector *a) { return a->id() == conn->from()->id(); });
            if (outport == el.first->outputs().end())
                throw CompilerException("SM", "Can't find " + conn->from()->id());

            auto *newconn = new Connection(*outport, *inport);
            this->_previous_virtual[el.first].push_back(newconn);
            el.first->_successors_virtual[this].push_back(newconn);
        }
    }
    _previous_transitiv.clear();
    for(Task *el : rhs->_previous_transitiv) {
        _previous_transitiv.push_back(el);
        el->_successors_transitiv.push_back(this);
    }
    
    _successor_tasks.clear();
    for(Task::dep_t el : rhs->_successor_tasks) {
        for (Connection *conn : el.second) {
            auto inport = find_if(el.first->_inputs.begin(), el.first->_inputs.end(), [conn](Connector *a) { return a->id() == conn->to()->id(); });
            if (inport == _inputs.end())
                throw CompilerException("SM", "Can't find " + conn->to()->id());

            auto outport = find_if(_outputs.begin(), _outputs.end(), [conn](Connector *a) { return a->id() == conn->from()->id(); });
            if (outport == el.first->outputs().end())
                throw CompilerException("SM", "Can't find " + conn->from()->id());

            auto *newconn = new Connection(*outport, *inport);
            this->_successor_tasks[this].push_back(newconn);
            el.first->_predecessor_tasks[el.first].push_back(newconn);
        }
    }
    
    _successors_virtual.clear();
    for(Task::dep_t el : rhs->_successors_virtual) {
        for (Connection *conn : el.second) {
            auto inport = find_if(el.first->_inputs.begin(), el.first->_inputs.end(), [conn](Connector *a) { return a->id() == conn->to()->id(); });
            if (inport == _inputs.end())
                throw CompilerException("SM", "Can't find " + conn->to()->id());

            auto outport = find_if(_outputs.begin(), _outputs.end(), [conn](Connector *a) { return a->id() == conn->from()->id(); });
            if (outport == el.first->outputs().end())
                throw CompilerException("SM", "Can't find " + conn->from()->id());

            auto *newconn = new Connection(*outport, *inport);
            this->_successors_virtual[this].push_back(newconn);
            el.first->_previous_virtual[el.first].push_back(newconn);
        }
    }
    
    _successors_transitiv.clear();
    for(Task *el : rhs->_successors_transitiv) {
        _successors_transitiv.push_back(el);
        el->_previous_transitiv.push_back(this);
    }
}

bool Task::is_parallel(const Task *b) const {
    if(this == b)
        return false;

    for(Task *ta : _previous_transitiv) {
        if(ta == b)
            return false;
    }

    for(Task *tb : b->_previous_transitiv) {
        if(tb == this)
            return false;
    }

    return true;
}

Task::Task(const string &id)  {
    _id = id;
    if(Utils::getLevel() == Utils::Level::DEBUG) {
        _ilp_label = id;
    }
    else {
        _ilp_label = "t"+to_string(++task_counter);
    }
    Version *v = new Version(this, CONFIG_DEF_VERSION_NAME);
    _versions[CONFIG_DEF_VERSION_NAME] = v;
}

Task::~Task() {
    _predecessor_tasks.clear();
    _previous_transitiv.clear();
    _successor_tasks.clear();
    _successors_transitiv.clear();
    for(Version *v : versions()) {
        delete v;
    }
    _versions.clear();
    
    for(Connector *i : _inputs)
        delete i;
    _inputs.clear();
    for(Connector *o : _outputs)
        delete o;
    _outputs.clear();
    
    _delay_tokens.clear();
}


Connection* SystemModel::add_dependency(Task* src, Task* sink, string srcconname, string sinkconname) {
    vector<Connector*>::iterator sinkconn, srcconn;
    sinkconn = find_if(sink->inputs().begin(), sink->inputs().end(), [sinkconname](Connector *a) {return a->id() == sinkconname; });
    if(sinkconn == sink->inputs().end())
        throw CompilerException("SM", "Can't find "+sinkconname);
    srcconn = find_if(src->outputs().begin(), src->outputs().end(), [srcconname](Connector *a) {return a->id() == srcconname; });
    if(srcconn == src->outputs().end())
        throw CompilerException("SM", "Can't find "+srcconname);

    return add_dependency(src, sink, *srcconn, *sinkconn);
}
Connection* SystemModel::add_dependency(Task* src, Task* sink, Connector *srccon, Connector *sinkcon) {
    // Ignore if connected to the same connectors, check token count when connected to different connectors
    auto existingconns = sink->predecessor_tasks().find(src);
    if (existingconns != sink->predecessor_tasks().end()) {
        auto it = std::find_if(existingconns->second.begin(), existingconns->second.end(), [&srccon, &sinkcon](Connection *conn) {
            return conn->from() == srccon && conn->to() == sinkcon;
        });
        if (it != existingconns->second.end()) {
            // complete duplicate edge: ignore
            return *it;
        } else {
            // multiple edges between tasks are allowed, token count must match existing connection
            // TODO: this could be relaxed to support common factors, e.g. a [1] -> [2] edge could be complimented with a [2] -> [4] edge
            Connection *existingconn = existingconns->second[0]; // grab any connection
            if ((existingconn->from()->tokens() != srccon->tokens()) || (existingconn->to()->tokens() != sinkcon->tokens()))
                throw CompilerException("SM", "Task " + src->id() + " and task " + sink->id() + " cannot share multiple connectors with different token counts");
        }
    }

    auto *conn = new Connection(srccon, sinkcon); // TODO: store this somewhere for memory management
    sink->predecessor_tasks()[src].push_back(conn);
    sink->eff_prev_rec()[src].push_back(sinkcon->tokens());

    src->successor_tasks()[sink].push_back(conn);
    src->eff_suc_send()[sink].push_back(srccon->tokens());
    return conn;
}

void SystemModel::add_task(Task* task) {
    _tasks[task->id()] = task;
}

void SystemModel::rem_dependency(Task *src, Task *sink) {
    if(src->successor_tasks().count(sink))
        src->successor_tasks().erase(sink);
    if(sink->predecessor_tasks().count(src))
        sink->predecessor_tasks().erase(src);
}

void SystemModel::rem_task(Task *t) {
    _tasks.erase(t->id());
}

uint64_t Task::data_read(const Task* from) const {
    uint64_t count = 0;
    if (_predecessor_tasks.count(from)) {
        for (Connection *conn : _predecessor_tasks.find(from)->second)
            count += conn->to()->tokens(); // TODO multiply by token size?
    }
    return count;
}

uint64_t Task::data_written(const Task *to) const {
    uint64_t count = 0;
    if (_predecessor_tasks.count(to)) {
        for (Connection *conn : _successor_tasks.find(to)->second)
            count += conn->from()->tokens(); // TODO multiply by token size?
    }
    return count;
}

void Task::force_mapping_proc(ComputeUnit* val) {
    for(Version *p : versions())
        p->force_mapping_proc(val);
}
void Task::resource_usage(ResourceUnit* val) {
    for(Version *p : versions())
        p->resource_usage(val);
}

void Task::classify() {
    for(Version *p : versions())
        p->classify();
}

void Task::add_version(Version *v) {
    // We add a default version at the beginning
    // if during parsing we add a version here, it means the task model will
    // provide the versions, hence we don't need the default one anymore
    if(_versions.count(CONFIG_DEF_VERSION_NAME))
        rem_version(_versions[CONFIG_DEF_VERSION_NAME]);
    _versions[v->id()] = v;
}

void Task::rem_version(Version *v) {
    _versions.erase(v->id());
    delete v; //???
}

void Task::clear_versions() {
    for (auto&& v : _versions | boost::adaptors::map_values) {
        delete v;
    }
    _versions.clear();
}

Connector * Task::input(std::string & name) const {
    for (Connector * c : _inputs) {
        if (c->id() == name) {
            return c;
        }
    }

    return nullptr;
}

Connector * Task::output(std::string & name) const {
    for (Connector * c : _outputs) {
        if (c->id() == name) {
            return c;
        }
    }

    return nullptr;
}

energycons_t Task::static_dynamic_energy() {
    energycons_t C_E = 0;
    for(std::pair<std::string, Version*> v: _versions) {
        if(v.second->C_E() + v.second->C() * v.second->watt() > C_E)
            C_E = v.second->C_E() + v.second->C() * v.second->watt();
    }
    return C_E;
}

SystemModel::SystemModel() = default;

SystemModel::~SystemModel() {
    //TODO:
//    for(std::pair<std::string, Task*> t: _tasks)
//        delete t.second;
//    _tasks.clear();
//
//    for (ComputeUnit *cu: _processors) {
//        delete cu;
//    }
//    _processors.clear();
//
//    for(ComputeUnit* coproc: _coprocessors) {
//        delete coproc;
//    }
//    _coprocessors.clear();
//
//    for(voltage_island* island: _voltage_islands) {
//        delete island;
//    }
//    _voltage_islands.clear();

/*
    for(meth_processor_t *p : _processors)
        delete p;
    _processors.clear();
    
    delete _interconnect;*/
}
Task* SystemModel::task(const string &id) const {
    return (_tasks.count(id)) ? _tasks.at(id) : nullptr;
}

size_t SystemModel::nbedges() const {
    size_t nbedges = 0;
    for(const auto& pair : _tasks)
        nbedges += pair.second->successor_tasks().size();
    return nbedges;
}

ComputeUnit* SystemModel::processor(const std::string &p) const {
    /*vector<ComputeUnit*>::iterator*/ auto iter = std::find(_processors.begin(), _processors.end(), p);
    return (iter == _processors.end()) ? nullptr : *iter; 
}

ComputeUnit* SystemModel::coprocessor(const std::string &p) const {
    /*vector<voltage_island*>::iterator*/ auto iter = std::find(_coprocessors.begin(), _coprocessors.end(), p);
    return (iter == _coprocessors.end()) ? nullptr : *iter; 
}

voltage_island* SystemModel::voltage_islands(const std::string &p) const {
    /*vector<_voltage_islands*>::iterator*/ auto iter = std::find(_voltage_islands.begin(), _voltage_islands.end(), p);
    return (iter == _voltage_islands.end()) ? nullptr : *iter; 
}

ResourceUnit* SystemModel::resource(const std::string &p) const {
    /*vector<ResourceUnit*>::iterator*/ auto iter = std::find(_resources.begin(), _resources.end(), p);
    return (iter == _resources.end()) ? nullptr : *iter; 
}

void SystemModel::clean_previous() {
    for(auto pair : _tasks) {
        vector<Task*> to_delete;
        for(auto & ip : pair.second->predecessor_tasks()) {
            if(ip.first == nullptr)
                to_delete.push_back(ip.first);
        }

        for(auto id : to_delete) {
            pair.second->predecessor_tasks().erase(id);
        }
    }
}

void SystemModel::compute_sequential_schedule_length() {
    _sequential_length = 0;
    for (std::pair<std::string, Task*> i: _tasks) {
        uint64_t  temp_C = 0;
        for(Version* v : i.second->versions()){ //more realistic sequential runtime / worst case across all versions of a task
            if (v->C() > temp_C){
                temp_C = v->C();
            }
        }
        _sequential_length += temp_C;
        for(Task::dep_t el : i.second->predecessor_tasks())
            for (Connection *conn : el.second)
                _sequential_length += _interconnect->comm_delay(processors().size()-1, conn->to()->tokens(), conn->to()->token_type());
        for(Task::dep_t el : i.second->successor_tasks())
            for (Connection *conn : el.second)
                _sequential_length += _interconnect->comm_delay(processors().size()-1, conn->from()->tokens(), conn->from()->token_type());
    }
}

void SystemModel::compute_best_sequential_length_based_on_energy() {
    _sequential_length_energy_based = 0;
    for (std::pair<std::string, Task*> i: _tasks) {
        timinginfos_t  temp_C = 0;
        energycons_t temp_CE = -1;
        for(Version* v : i.second->versions()){ //more realistic sequential runtime TODO: make work with other units!
            energycons_t temp_energy = (v->C() * _base_watt)/10 + v->C_E(); // divided by 10 because mW * ds = 10*mJ and C_E is in mJ. this is dynamic eneryg heavy so it will favor longer running tasks that have lower dynamic energy
            if (temp_energy < temp_CE){ //taking the version consuming the least energy running sequentially and then updating the runtime based on that version
                temp_CE = temp_energy;
                temp_C = v->C();
            }
        }
        _sequential_length += temp_C;
        for(Task::dep_t el : i.second->predecessor_tasks())
            for (Connection *conn : el.second)
                _sequential_length += _interconnect->comm_delay(processors().size()-1, conn->to()->tokens(), conn->to()->token_type());
        for(Task::dep_t el : i.second->successor_tasks())
            for (Connection *conn : el.second)
                _sequential_length += _interconnect->comm_delay(processors().size()-1, conn->from()->tokens(), conn->from()->token_type());
    }
}

void SystemModel::populate_memory_usage() {
    for(auto pair : _tasks) {
        Task *t = pair.second;
        t->_data_read = 0;
        for(Task::dep_t el : t->predecessor_tasks()) {
            for (Connection *conn : el.second)
                t->_data_read += conn->to()->tokens();
        }
        t->_data_written = 0;
        for(Task::dep_t el : t->successor_tasks()) {
            for (Connection *conn : el.second)
                t->_data_written += conn->from()->tokens();
        }
        
        for(Version *v : t->versions()) {
            v->memory_footprint(t->_data_read + v->local_memory_storage() + t->_data_written);
            if(v->memory_code_size() == 0)
               v->memory_footprint(v->memory_footprint() + v->memory_code_size());
            
            for(Phase *p : v->phases()) {
                p->memory_footprint(t->_data_read + p->local_memory_storage() + t->_data_written);
                if(p->memory_code_size() == 0)
                   p->memory_footprint(p->memory_footprint() + p->memory_code_size());
            }
        }
    }
}

size_t SystemModel::get_critical_path_aux(vector<Task*> &Qdone, Task *current, size_t heaviest, vector<Task*> *heaviestpath) const {
    if(find(Qdone.begin(), Qdone.end(), current) != Qdone.end()) {
        heaviestpath->push_back(current);
        return heaviest+current->C();
    }
    Qdone.push_back(current);

    size_t max = 0;
    vector<Task*> maxpath;
    for(Task::dep_t s : current->successor_tasks()) {
        vector<Task*> path;
        size_t pathweight = get_critical_path_aux(Qdone, s.first, heaviest, &path);
        if(pathweight > max) {
            max = pathweight;
            maxpath.clear();
            maxpath.insert(maxpath.end(), path.begin(), path.end());
        }
    }
    heaviestpath->insert(heaviestpath->end(), maxpath.begin(), maxpath.end());
    heaviestpath->push_back(current);
    return max+current->C();
}

vector<Task*> SystemModel::tasks_as_vector() const {
    return Utils::mapvalues(_tasks);
}

boost::select_second_mutable_range<map<std::string, Task *>> SystemModel::tasks_it() {
    return _tasks | boost::adaptors::map_values;
}

boost::select_second_const_range<map<std::string, Task *>> SystemModel::tasks_it() const {
    return _tasks | boost::adaptors::map_values;
}

vector<Task*> SystemModel::get_critical_path(vector<Task*> graph) const {
    uint128_t heaviest = 0;
    vector<Task*> heaviestpath;
    for(Task *root : graph) {
        if(root->predecessor_tasks().size() > 0) continue;
        
        vector<Task*> Qdone;
        Qdone.push_back(root);
        heaviestpath.push_back(root);

        for(Task::dep_t el : root->successor_tasks()) {
            vector<Task*> path;
            uint128_t pathweight = get_critical_path_aux(Qdone, el.first, 0, &path);
            if(pathweight > heaviest) {
                heaviest = pathweight;
                heaviestpath.clear();
                heaviestpath.insert(heaviestpath.end(), path.begin(), path.end());
            }
        }
    }
    return heaviestpath;
}

vector<Task*> SystemModel::get_critical_path() const {
    return get_critical_path(tasks_as_vector());
}

bool SystemModel::is_conditional_edge(Task *t, Connector *c, conditional_edge_e type) {
    return any_of(t->conditional_out_groups().begin(), t->conditional_out_groups().end(), [t, c, type](conditional_edge_t g) {
        return g.cond == type && any_of(g.succs.begin(), g.succs.end(), [t,c](Task *s) {
            std::vector<Connection *> &succ = t->successor_tasks()[s];
            return any_of(succ.begin(),  succ.end(), [c](Connection *conn) { return c == conn->from(); });
        });
    });
}

void SystemModel::is_conditional_task(vector<Task*> &Qdone, Task *current, conditional_edge_e type) {
    if(find(Qdone.begin(), Qdone.end(), current) != Qdone.end())
        return;
    Qdone.push_back(current);
    
    if(current->predecessor_tasks().size() == 0) {
        current->is_conditional_task(false);
        return;
    }

    current->is_conditional_task(true);
    for (Task::dep_t el: current->predecessor_tasks()) {
        is_conditional_task(Qdone, el.first, type);
        current->is_conditional_task(
                current->is_conditional_task() && (el.first->is_conditional_task() || all_of(el.second.begin(), el.second.end(), [this, &el, &type](Connection *c) {
                    return is_conditional_edge(el.first, c->from(), type);
                })));
    }
}

void SystemModel::populate_conditionality() {
    for(auto const &i : _tasks) {
        Task *t = i.second;
        for(Connector *c : t->outputs())
            c->part_of_cond_edge(is_conditional_edge(t, c, conditional_edge_e::DATA));
    }
    
    vector<Task*> done;
    for(auto const &i : _tasks) {
        Task *t = i.second;
        is_conditional_task(done, t, conditional_edge_e::DATA);
    }
}

void SystemModel::compute_hyperperiod() {
    vector<timinginfos_t> periods;
    for(auto const &i : _tasks) {
        Task *t = i.second;
        if(t->parent_task() == nullptr && t->T() > 0)
            periods.push_back(t->T());
    }

    // if tasks have no period, hyperperiod is set to max uint_128
    if (periods.empty()){
        _hyperperiod = std::numeric_limits<globtiminginfos_t>::max();
    }
    else if(periods.size() == 1) {
        _hyperperiod = periods[0];
    }
    else{
        _hyperperiod = Utils::lcm<vector<timinginfos_t>, uint128_t>(periods);
    }
}

bool SystemModel::has_hyperperiod() const {
    return hyperperiod() != 0 && hyperperiod() != std::numeric_limits<uint128_t>::max();
}

bool SystemModel::has_global_deadline() const {
    return _global_deadline != 0;
}

void SystemModel::classify() {
    vector<vector<Task*>> DAGs;
    Utils::extractGraphs(this, &DAGs);

    bool has_multiple_indep_tasks = DAGs.size() > 1;
    bool has_DAG_tasks = false;
    bool has_DAG_with_multi_period = false;
    bool all_deadline_constrained = true;
    bool all_deadline_implicit = true;
    bool no_period_given = true;
    bool all_period_strict = true;

    for(vector<Task*> dag : DAGs) {
        has_DAG_tasks = has_DAG_tasks || (dag.size() > 1);
        timinginfos_t last_known_period = 0;
        for(Task *t : dag) {
            if(t->T() == 0) continue;

            if(last_known_period > 0)
                has_DAG_with_multi_period = true;
            else
                last_known_period = t->T();

            if(t->D() > 0) {
                all_deadline_constrained = all_deadline_constrained && t->D() < t->T();
                all_deadline_implicit = all_deadline_implicit && t->D() <= t->T();
            }

            no_period_given = false;
            all_period_strict = all_period_strict && !t->is_sporadic();
        }
    }

    if(!has_multiple_indep_tasks && !has_DAG_tasks) // 1 lonely task in the taskset
        _taskset_type = TaskProperties::TaskSetType::TAS_INDEPENDENT;
    else if(has_multiple_indep_tasks && !has_DAG_tasks)
        _taskset_type = TaskProperties::TaskSetType::TAS_INDEPENDENT;
    else if(!has_multiple_indep_tasks && has_DAG_tasks && !has_DAG_with_multi_period)
        _taskset_type = TaskProperties::TaskSetType::TAS_DAG;
    else if(!has_multiple_indep_tasks && has_DAG_tasks && has_DAG_with_multi_period)
        _taskset_type = TaskProperties::TaskSetType::TAS_MULTI_PERIOD_DAG;
    else if(has_multiple_indep_tasks && has_DAG_tasks && !has_DAG_with_multi_period)
        _taskset_type = TaskProperties::TaskSetType::TAS_MULTI_DAG;
    else if(has_multiple_indep_tasks && has_DAG_tasks && has_DAG_with_multi_period)
        _taskset_type = TaskProperties::TaskSetType::TAS_MULTI_PERIOD_MULTI_DAG;

    if(all_deadline_constrained)
        _taskset_deadline_type = TaskProperties::DeadlineModel::DEA_CONSTRAINED;
    else if(all_deadline_implicit)
        _taskset_deadline_type = TaskProperties::DeadlineModel::DEA_IMPLICIT;
    else
        _taskset_deadline_type = TaskProperties::DeadlineModel::DEA_ARBITRARY;

    if(no_period_given)
        _taskset_rec_model = TaskProperties::RecurringModel::REC_ARBITRARY;
    else if(all_period_strict)
        _taskset_rec_model = TaskProperties::RecurringModel::REC_PERIODIC;
    else
        _taskset_rec_model = TaskProperties::RecurringModel::REC_SPORADIC;

    if(_processors.size() == 1)
        _cu_type = ArchitectureProperties::Cores::COR_SINGLE;
    else if(_processors.size() > 1) {
        bool is_homogeneous = true;
        ArchitectureProperties::ComputeUnitType last_known_type = _processors[0]->proc_class;
        for(ComputeUnit *c : _processors) {
            is_homogeneous = is_homogeneous && last_known_type == c->proc_class;
        }
        _cu_type = is_homogeneous ? ArchitectureProperties::Cores::COR_MULTI_HOMOGENEOUS : ArchitectureProperties::Cores::COR_MULTI_HETEROGENEOUS;
    }
    for(auto const &i : _tasks)
        i.second->classify();

    Utils::DEBUG("Task set type: "+TaskProperties::to_string(_taskset_type));
    Utils::DEBUG("Deadline type: "+TaskProperties::to_string(_taskset_deadline_type));
    Utils::DEBUG("Recurring type: "+TaskProperties::to_string(_taskset_rec_model));
    Utils::DEBUG("Arch type: "+ArchitectureProperties::to_string(_cu_type));
}


void SystemModel::link_processors_and_voltage_islands() {
    for(voltage_island *v: _voltage_islands){
        for(ComputeUnit *p: _processors){
            if (p->voltage_island_id == v->id){
                v->impacted_processors.push_back(p);
                p->target_voltage_island = v;
            }
        }
        for(ComputeUnit *p: _coprocessors){
            if (p->voltage_island_id == v->id){
                v->impacted_processors.push_back(p);
                p->target_voltage_island = v;
            }
        }
    }
}

void SystemModel::convert_all_to_milli() {
    //global deadline
    _global_deadline = Utils::nano_to_milli(_global_deadline);
    //_global_available_energy
    _global_available_energy = Utils::nano_to_milli(_global_available_energy);
    //_sequential_length
    _sequential_length = Utils::nano_to_milli(_sequential_length);
    //_base_watt
    _base_watt = Utils::nano_to_milli(_base_watt);
    SystemModel::compute_hyperperiod();

    // nano watt in voltage islands to milli; Hz to kHz
    for(voltage_island *v: _voltage_islands){
        for(auto p: v->frequency_watt){
            p.first = Utils::Hz_to_kHz(p.first);
            p.second = Utils::nano_to_milli(p.second);
        }
    }

    //fix time, energy, frequency in versions
    for(std::pair<std::string, Task*> t: _tasks){
        //frequency
        t.second->frequency(Utils::Hz_to_kHz(t.second->frequency()));
        //gpu_frequency
        t.second->gpu_frequency(Utils::Hz_to_kHz(t.second->gpu_frequency()));

        //versions
        for(Version* v: t.second->versions()){
            v->frequency(Utils::Hz_to_kHz(v->frequency()));
            v->gpu_frequency(Utils::Hz_to_kHz(v->gpu_frequency()));
            v->C(Utils::nano_to_milli(v->C()));
            v->C_E(Utils::nano_to_milli(v->C_E()));
        }

    }

}
/*
 * Copyright (C) 2022 Lukas Miedema <l.miedema@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "StrategySummaryViewer.hpp"
#include "StateMachine/FaultTolerance/FaultToleranceStrategy.hpp"

IMPLEMENT_HELP(StrategySummaryViewer,
Writes a textual summary of the strategy state machine to a file.\n
Note that the strategy-exporter "summary" is conceptually different from the regular "summary" exporter, which shows a single schedule.\n
To show a schedule, combine the <strategy id="..."> tag with the "summary" exporter to view associated schedules.\n
See: SummaryViewer, StrategyDotViewer\n
    - filename [string]: optional name for the generated file. Omit to write to stdout.
)

void StrategySummaryViewer::forward_params(const std::map<std::string, std::string> &args) {
    if(args.count("filename") && !args.at("filename").empty())
        filename = args.at("filename");
}

std::string StrategySummaryViewer::to_length(std::string str, uint32_t length) {
    if (str.size() > length)
        str = str.substr(0,  length - 2) + "..";
    else
        str.append(length - str.size(), ' ');
    return str;
}

void StrategySummaryViewer::do_export(std::ostream& out) {
    using probability::probability_t;

    const int32_t cell_width = 10;
    bool has_ssm = !ssm->is_trivial();

    out << "--- Strategy State Machine ---" << std::endl;
    out << "Strategies:     " << ssm->strategies().size() << std::endl;
    out << "Scheduler:      " << (has_ssm ? ssm->strategy_manager()->solver()->get_uniqid_rtti() : "n/a") << std::endl;

    std::string linkedStatus;
    switch (ssm->compute_linked_status()) {
        case SSM_FULLY_LINKED:
            linkedStatus = "fully linked";
            break;
        case SSM_PARTIALLY_LINKED:
            linkedStatus = "partially linked";
            break;
        case SSM_NONE_LINKED:
            linkedStatus = "none linked / no results";
            break;
    }
    out << "Linked Status:  " << linkedStatus << std::endl;
    out << "Interpretation: " << (has_ssm ? ssm->strategy_manager()->description() : "none") << std::endl;

    // Interpretation-specific
#ifdef EIGEN3
    if (typeid(*ssm->strategy_manager()) == typeid(FaultToleranceStrategyManager)) {
        const auto ftManager = dynamic_cast<const FaultToleranceStrategyManager *>(ssm->strategy_manager());
        auto *faultRate = ftManager->ft_protected()->fault_rate();
        auto steadyState = ftManager->compute_dtmc_rate(ssm);
        out << "- Fault rate: " << faultRate->to_fraction() << " (" << faultRate->to_string() << ") per time unit" << std::endl;
        out << "- SS rate:    " << (double) steadyState.fault_rate << " per iteration" << std::endl;
        out << "- Energy:     " << (steadyState.energy.has_value() ? std::to_string(steadyState.energy.value()) : "n/a") << " " << tg->energy_unit() << " per iteration" << std::endl;
    }
#endif

    out << std::endl;
    out << "--- Strategies ---" << std::endl;
    const std::vector<SchedElt *> &elements = ssm->schedule()->elements_unowned();
    out << to_length("Strategy", cell_width) << "|";
    out << to_length("Result", cell_width) << "|";
    out << to_length("Makespan", cell_width) << "|";
    out << to_length("Energy", cell_width) << "|";
    out << to_length("Successor", cell_width) << "|";
    uint64_t length = (cell_width + 1) * 5;
    for (const SchedElt *e : elements) {
        std::string str = e->id();
        out << to_length(e->id(), cell_width) << "|";
        length += cell_width + 1; // +1 for the "|"
    }
    out << std::endl;
    for (uint64_t i = 0; i < length; i++)
        out << '-';
    out << std::endl;

    auto &strategies = ssm->strategies();
    for (const auto &s : strategies) {
        auto ftStrategy = dynamic_cast<FaultToleranceStrategy *>(s.get());

        std::string index_str = to_string(s->id());
        out << to_length(index_str, cell_width) << "|";
        out << to_length("-", cell_width) << "|"; // result
        out << to_length(std::to_string(ftStrategy->makespan()) + tg->time_unit(), cell_width) << "|"; // makespan
        out << to_length(std::to_string(ftStrategy->energy()) + tg->energy_unit(), cell_width) << "|"; // energy
        out << to_length("-", cell_width) << "|"; // successor

        for (uint64_t task_index = 0; task_index < elements.size(); task_index++) {
            std::string role = to_length(s->get_role_for_elt(task_index, *(ssm->strategy_manager())), cell_width);
            out << role << "|";
        }
        out << std::endl;

        // Print results (each on a separate row)
        if (typeid(*ssm->strategy_manager()) == typeid(FaultToleranceStrategyManager)) {
            const auto sft = dynamic_cast<FaultToleranceStrategy *>(s.get());
            const auto protectedElements = sft->protected_elements();

            for (auto &ftResult : sft->results() | boost::adaptors::map_values) {
                out << to_length("", cell_width) << "|"; // strategy
                out << to_length(ftResult->id(), cell_width) << "|";
                out << to_length("-", cell_width) << "|"; // makespan
                out << to_length("-", cell_width) << "|"; // energy
                out << to_length(ftResult->successor() == nullptr ? "-" : to_string(ftResult->successor()->id()), cell_width) << "|";

                auto &resultVector = ftResult->result();
                uint64_t resultIndex = 0;
                for (uint64_t taskIndex = 0; taskIndex < elements.size(); taskIndex++) {
                    if (protectedElements[taskIndex]) {
                        out << to_length(resultVector[resultIndex] ? "success" : "fail", cell_width) << "|";
                        resultIndex++;
                    } else {
                        out << to_length("-", cell_width) << "|";
                    }
                }
                out << std::endl;
            }
            for (uint64_t i = 0; i < length; i++)
                out << '-';
            out << std::endl;
        }
    }

    out << std::endl;
    if (typeid(*ssm->strategy_manager()) == typeid(FaultToleranceStrategyManager)) {
        out << "--- Interpretation: Fault-Tolerance ---" << std::endl;
        out << "--- Direct success-probabilities" << std::endl;
        auto ftManager = dynamic_cast<const FaultToleranceStrategyManager *>(ssm->strategy_manager());

        out << to_length("Strategy", cell_width) << "|";
        length = cell_width + 1;
        for (const SchedElt *e : elements) {
            std::string str = e->id();
            out << to_length(e->id(), cell_width) << "|";
            length += cell_width + 1; // +1 for the "|"
        }
        out << std::endl;
        for (uint64_t i = 0; i < length; i++)
            out << '-';
        out << std::endl;

        const std::map<Strategy *, std::vector<probability_t>> &basicTable = ftManager->compute_basic_ft_table(ssm);
        for (const auto &s : strategies) {
            std::string index_str = to_string(s->id());
            out << to_length(index_str, cell_width) << "|";

            const std::vector<probability_t>& values = basicTable.at(s.get());
            for (uint64_t task_index = 0; task_index < elements.size(); task_index++) {
                std::string data = to_length(values[task_index].operator double(), "%|2.7|", cell_width);
                out << data << "|";
            }
            out << std::endl;
        }

        out << std::endl;
        out << "--- Location-dependent success-probabilities" << std::endl;
        out << to_length("Strategy", cell_width) << "|";
        length = cell_width + 1;
        for (const SchedElt *e : elements) {
            std::string str = e->id();
            out << to_length(e->id(), cell_width) << "|";
            length += cell_width + 1; // +1 for the "|"
        }
        out << std::endl;
        for (uint64_t i = 0; i < length; i++)
            out << '-';
        out << std::endl;

        const std::map<Strategy *, std::vector<probability_t>> &relationalTable = ftManager->compute_relational_ft_table(ssm, basicTable);
        for (const auto &s : strategies) {
            std::string index_str = to_string(s->id());
            out << to_length(index_str, cell_width) << "|";

            const std::vector<probability_t>& values = relationalTable.at(s.get());
            for (uint64_t task_index = 0; task_index < elements.size(); task_index++) {
                std::string data = to_length(values[task_index].operator double(), "%|2.7|", cell_width);
                out << data << "|";
            }
            out << std::endl;
        }

        // Results
        out << std::endl;
        for (uint64_t i = 0; i < length; i++)
            out << '-';
        out << std::endl;
        out << to_length("Result", cell_width) << "|";
        length = cell_width + 1;
        for (const SchedElt *e : elements) {
            std::string str = e->id();
            out << to_length(e->id(), cell_width) << "|";
            length += cell_width + 1; // +1 for the "|"
        }
        out << std::endl;
        for (uint64_t i = 0; i < length; i++)
            out << '-';
        out << std::endl;

        for (const auto &s : strategies) {
            for (const auto &r : ftManager->results(s.get())) {
                auto ftResult = dynamic_cast<FaultToleranceResult *>(r);
                std::string index_str = ftResult->id();
                out << to_length(index_str, cell_width) << "|";

                auto &values = ftResult->probability_table();
                for (uint64_t task_index = 0; task_index < elements.size(); task_index++) {
                    std::string data = to_length(values[task_index].operator double(), "%|2.7|", cell_width);
                    out << data << "|";
                }
                out << std::endl;
            }
        }
    }

    out << std::endl;
}

const std::string StrategySummaryViewer::get_uniqid_rtti() const {
    return "exporter-ssm-viewer";
}

void StrategySummaryViewer::check_dependencies() {
    // None
}

void StrategySummaryViewer::do_export(const boost::filesystem::path& fn) {
    std::ofstream fout(fn.string());
    do_export(fout);
    fout.close();
    Utils::INFO("SSM written to '" + fn.string() + "'");
}

void StrategySummaryViewer::do_export() {
    if(!filename.empty())
        if (filename.at(0) == '/') // is absolute path?
            do_export(filename);
        else
            do_export(conf->files.output_folder / filename);
    else
        do_export(std::cout);
}
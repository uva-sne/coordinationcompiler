/*
 * Copyright (C) 2022 Lukas Miedema <l.miedema@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "StrategyDotViewer.hpp"

IMPLEMENT_HELP(StrategyDotViewer, Export a dot file representation of the strategy state machine);

void StrategyDotViewer::do_export(const boost::filesystem::path &f) {
    std::ofstream out(f.string());
    using std::endl;

    out << "digraph SSM {" << endl;
    out << "  // " << ssm->strategies().size() << " strategies" << endl;
    out << "  node [shape=record];";

    for (const StrategyPtr &strategy : ssm->strategies()) {
        // TODO include strategy data (protected tasks, etc)
        out << "  s_" << strategy->id() << " [label=\"S" << strategy->id() << "\"];" << endl;

        for (const Result *result : manager->results(strategy.get())) {
            out << "  r_" << result->id() << " [label=\"R" << result->id() << "\"];" << endl;
            out << "  s_" << strategy->id() << " -> r_" << result->id() << "[color=blue];" << endl;
        }
        out << endl;
    }

    out << endl << "  // Result -> strategy mapping" << endl;
    for (const StrategyPtr &strategy : ssm->strategies()) {
        for (Result *result : ssm->strategy_manager()->results(strategy.get())) {
            Strategy *successor = result->successor(strategy.get(), *ssm);
            out << "  r_" << result->id() << " -> s_" << successor->id() << ";" << endl;
        }
    }
    out << "}";
}

void StrategyDotViewer::forward_params(const std::map<std::string, std::string> &args) {
    if(args.count("filename") && !args.at("filename").empty())
        filename = args.at("filename");
}


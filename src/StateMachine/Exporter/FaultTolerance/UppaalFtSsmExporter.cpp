/*
 * Copyright (C) 2022 Lukas Miedema <l.miedema@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <iostream>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/date_time/microsec_time_clock.hpp>

#include <Utils/UppaalUtils.hpp>
#include "UppaalFtSsmExporter.hpp"

IMPLEMENT_HELP(UppaalFtSsmExporter, Export the fault-tolerance SSM as a UPPAAL SMC model.\n
     - filename, outDir/appname-uppaal.xml: name with or without path for the generated file.\n
     - queries_dir: Set to export separate verifta query files. With or without path, otherwise relative to outDir.\n
)

void UppaalFtSsmExporter::forward_params(const std::map<std::string, std::string> &args) {
    if(args.count("filename") && !args.at("filename").empty())
        filename = args.at("filename");
    if(args.count("queries_dir") && !args.at("queries_dir").empty())
        queries_dir = args.at("queries_dir");
}

void UppaalFtSsmExporter::do_export(const boost::filesystem::path& fn) {
    using std::string;
    namespace ba = boost::algorithm;

    std::vector<SchedEltPtr> elements = ssm->get_schedule_elements_exclusive();
    auto strategies = ssm->strategies_unowned<FaultToleranceStrategy>();
    auto results = ssm->results_unowned<FaultToleranceResult>();

    // Write files
    string uppaal_model = get_template();
    ba::replace_all(uppaal_model, "%SYSTEM_DECL%", this->create_system_decls(elements, strategies, results));
    ba::replace_all(uppaal_model, "%RESULT_ARRAY%", this->create_result_array(elements, strategies, results));
    ba::replace_all(uppaal_model, "%SSM%", create_ssm(elements, strategies, results));

    ba::replace_all(uppaal_model, "%DAG_NAME%", conf->files.coord_basename.string());
    ba::replace_all(uppaal_model, "%TIMESTAMP%", get_timestamp());
    ba::replace_all(uppaal_model, "%FAULT_RATE%", manager->ft_protected()->fault_rate()->to_string());
    ba::replace_all(uppaal_model, "%TASK_COUNT%", to_string(elements.size()));
    ba::replace_all(uppaal_model, "%PROC_COUNT%", to_string(this->tg->processors().size()));
    ba::replace_all(uppaal_model, "%STRATEGY_COUNT%", to_string(strategies.size()));
    ba::replace_all(uppaal_model, "%RESULT_COUNT%", to_string(results.size()));

    ba::replace_all(uppaal_model, "%QUERIES%", this->get_embedded_queries());

    std::ofstream out(fn.string());
    out << uppaal_model;
    out.close();

    if(!queries_dir.empty()) {
        boost::filesystem::path queries_path;
        if (queries_dir.at(0) == '/') // is absolute path?
            queries_path = queries_dir;
        else
            queries_path = conf->files.output_folder / queries_dir;
        if (boost::filesystem::exists(queries_path)) {
            if (!boost::filesystem::is_directory(queries_path)) {
                Utils::WARN(queries_path.string() +
                        " already exists and is not a directory. Skipping verifyta query generation");
                return;
            }
        }
        boost::filesystem::create_directories(queries_path);
        std::string basename = fn.leaf().string();
        if (boost::algorithm::ends_with(basename, ".uppaal.xml"))
            basename = basename.substr(0, basename.length() - std::string(".uppaal.xml").length());

        {
            uint64_t iterations = 1<<14;
            boost::filesystem::path query_fn = queries_path / (basename + "." + to_string(iterations) + ".q");
            std::ofstream query(query_fn.string());
            query << get_verifyta_query(iterations);
            query.close();
        }
    }

    ssm->set_schedule_elements(std::move(elements));
}

std::string UppaalFtSsmExporter::get_template() {
    return
#include "StateMachine/Exporter/Templates/UppaalFtSsmTemplate.tpl"
            ;
}

std::string UppaalFtSsmExporter::get_timestamp() {
    using namespace boost::posix_time;
    return to_iso_extended_string(microsec_clock::universal_time());
}

std::string UppaalFtSsmExporter::create_result_array(
        std::vector<SchedEltPtr> &elements,
        std::vector<FaultToleranceStrategy *> &strategies,
        std::vector<FaultToleranceResult *> &results) {

    struct result_entry_t {
        std::string str;
        uint64_t strategy_id;
        uint64_t result_id;
    };

    std::vector<result_entry_t> entries; // needs to be sorted
    bool detectAllFaults = manager->ft_unprotected()->has_detect_capability();

    for (size_t strategyIndex = 0; strategyIndex < strategies.size(); strategyIndex++) {
        auto &strategy = strategies[strategyIndex];
        const std::vector<bool> &protectedElements = strategy->protected_elements();

        for (auto &result : strategy->results() | boost::adaptors::map_values) {
            const std::vector<bool> &resultElements = result->result();
            result_entry_t &entry = entries.emplace_back();
            entry.strategy_id = strategy->id();
            entry.result_id = result->result_id();

            // Create a result lookup map for resolving predecessor relations
            std::map<SchedElt *, bool> resultMap;
            size_t resultIndex = 0;
            for (size_t i = 0; i < elements.size(); i++)
                if (protectedElements[i])
                    resultMap[elements[i].get()] = resultElements[resultIndex++];

            entry.str.append(" {");
            resultIndex = 0;
            for (size_t i = 0; i < elements.size(); i++) {
                if (detectAllFaults || protectedElements[i]) {
                    if (resultElements[resultIndex]) {
                        entry.str.append("SUCCESS");
                    } else {
                        entry.str.append("FAULTED");
                    }
                    resultIndex++;
                } else {
                    // The entry may not be "UNKNOWN" -> an (indirect) predecessor may have FAULTED, thus also failing this
                    std::set<SchedElt *> visited;
                    std::vector<SchedElt *> queue;
                    SchedElt *element = elements[i].get();
                    queue.insert(queue.end(), element->previous().begin(),  element->previous().end());
                    visited.insert(element->previous().begin(), element->previous().end());

                    bool hasFailedPredecessor = false;
                    while (!queue.empty() && !hasFailedPredecessor) {
                        SchedElt *back = queue.back();
                        queue.pop_back();

                        for (SchedElt *predecessor : back->previous()) {
                            if (!visited.count(predecessor)) {
                                queue.push_back(predecessor);
                                visited.insert(predecessor);
                            }
                        }

                        if (resultMap.count(back) && !resultMap[back]) {
                            // Has known failed predecessor
                            hasFailedPredecessor = true;
                        }
                    }

                    if (hasFailedPredecessor)
                        entry.str.append("FAULTED");
                    else
                        entry.str.append("UNKNOWN");
                }
                if (i + 1 < elements.size())
                    entry.str.append(", ");
            }
            entry.str.append("}");
        }
    }

    // Sort such that table[i] refers to result i
    std::sort(entries.begin(), entries.end(), [](const result_entry_t &a, const result_entry_t &b) -> bool {
        if (a.strategy_id == b.strategy_id)
            return a.result_id < b.result_id;
        return a.strategy_id < b.strategy_id;
    });

    /*
    // Strategy 0
    {SUCCESS, UNKNOWN}, // result 0
    {FAULTED, UNKNOWN}, // result 1

    // Strategy 1
    {UNKNOWN, SUCCESS}, // result 2
    {UNKNOWN, FAULTED} // result 3
     */

    std::string out;
    for (size_t ri = 0; ri < entries.size(); ri++) {
        assert (ri == entries[ri].result_id); // order must match for indexing
        if (ri == 0 || entries[ri-1].strategy_id != entries[ri].strategy_id)
            out.append("\n  // Strategy ").append(std::to_string(entries[ri].strategy_id)).append("\n");
        out.append(entries[ri].str);
        if (ri < entries.size() - 1)
            out.append(",");
        out.append("// result ").append(std::to_string(entries[ri].result_id)).append("\n");
    }
    return out;
}

std::string UppaalFtSsmExporter::create_system_decls(
        std::vector<SchedEltPtr> &baseElements,
        std::vector<FaultToleranceStrategy *> &strategies,
        std::vector<FaultToleranceResult *> &results) {
    std::string out;

    Schedule *schedule = this->ssm->schedule();
    std::vector<std::string> instantiations;

    out.append("// Processors\n");
    for (size_t procId = 0; procId < schedule->schedcores().size(); procId++) {
        std::string& instantiation = instantiations.emplace_back();
        instantiation.append("P" + std::to_string(procId));
        out.append(instantiation);
        out.append(" = Processor(" + std::to_string(procId) + ");\n");
    }
    out.append("\n");

    uint64_t resultIndex = 0; // count results
    for (uint64_t strategyIndex = 0; strategyIndex < strategies.size(); strategyIndex++) {
        auto strategy = strategies[strategyIndex];

        out.append("\n// Strategy " + std::to_string(strategy->id()) + "\n");
        std::string prefix = "S" + std::to_string(strategy->id());

        strategy->schedule(
                const_cast<SystemModel &>(*this->tg),
                const_cast<config_t &>(*this->conf),
                *this->manager, baseElements,*schedule);

        // This has populated the "schedule"
        auto &protectedElements = strategy->protected_elements();
        for (size_t elementIndex = 0; elementIndex < protectedElements.size(); elementIndex++) {
            SchedElt *element = baseElements[elementIndex].get();
            out.append(create_task_decl_for(prefix, strategyIndex, elementIndex, element,
                    protectedElements[elementIndex] ? manager->ft_protected() : manager->ft_unprotected(), schedule, instantiations));
        }
        // Clear for the next iteration
        schedule->clear_assigned();
        schedule->elements_lut().clear();

        out.append("\n");
        out.append("// Results\n");
        for (const auto &resultEntry : strategy->results()) {
            out.append(create_resultmatcher_for(prefix, strategyIndex, resultIndex, resultEntry.second.get(), instantiations));
            resultIndex++; // must match index in create_result_array output
        }
        out.append("\n");
    }

    out.append("\n");
    out.append("// Per-task (global, not strategy bound) task monitors\n");

    std::map<SchedElt *, uint64_t> taskToIdMap;
    for (uint64_t taskIndex = 0; taskIndex < baseElements.size(); taskIndex++) {
        std::string& instantiation = instantiations.emplace_back();
        instantiation
            .append("M")
            .append(std::to_string(taskIndex));
        out
            .append(instantiation)
            .append(" = TaskMonitor(")
            .append(std::to_string(taskIndex))
            .append(");\n");
        taskToIdMap[baseElements[taskIndex].get()] = taskIndex;
    }

    out.append("\n");
    out.append("// Edges and edge monitors\n");
    for (uint64_t taskIndex = 0; taskIndex < baseElements.size(); taskIndex++) {
        SchedElt *task = baseElements[taskIndex].get();
        for (SchedElt *successor : task->successors()) {
            uint64_t successorIndex = taskToIdMap[successor];

            std::string& edgeInstance = instantiations.emplace_back();
            edgeInstance
                    .append("E")
                    .append(std::to_string(taskIndex))
                    .append("_")
                    .append(std::to_string(successorIndex));

            std::string& edgeMonitorInstance = instantiations.emplace_back();
            edgeMonitorInstance
                    .append("EM")
                    .append(std::to_string(taskIndex))
                    .append("_")
                    .append(std::to_string(successorIndex));

            out
                    .append(edgeInstance)
                    .append(" = Edge(")
                    .append(std::to_string(taskIndex))
                    .append(", ")
                    .append(std::to_string(successorIndex))
                    .append(");\n");
            out
                    .append(edgeMonitorInstance)
                    .append(" = EdgeMonitor(")
                    .append(std::to_string(taskIndex))
                    .append(", ")
                    .append(std::to_string(successorIndex))
                    .append(");\n");
        }
    }

    // Iteration monitor
    instantiations.emplace_back("IM");
    out.append("IM = IterationMonitor(")
        .append(std::to_string(NS_TO_S(tg->global_deadline())))
        .append(");\n");

    // System declaration
    out.append("\n");
    out.append("system\n    ");

    for (std::string &instantiation: instantiations)
        out.append(instantiation).append(", ");

    out.append("\n    StrategyStateMachine;");

    return out;
}


std::string UppaalFtSsmExporter::create_task_decl_for(std::string &prefix, uint64_t strategyIndex, uint64_t elementIndex,
        SchedElt *element, const FaultToleranceScheme *scheme, Schedule *schedule, std::vector<std::string> &instantiations) {
    // "element" does not exist in the schedule, rather a FT-scheme managed copy of the object exists
    // there may even be multiple (multiple replicas)
    // we do not care about the voter task, which was also scheduled (assuming NMR)
    assert(element->element_id() == 0); // must be dealing with the "plain" version
    assert(scheme->replication_count() >= 1); // must be dealing with NMR

    std::string out;
    auto &lut = schedule->elements_lut();
    for (uint32_t replicaId = 0; replicaId < scheme->replication_count(); replicaId++) {
        // replicaId is always +1 to make sure 0 isn't used
        std::string key = SchedElt::get_id_for(element->task()->id(), element->min_rt_period(), replicaId + 1, element->type());
        SchedElt *replica = lut[key].get();
        assert(replica != nullptr);

        // Task(strategyId, taskId, rt, wcet, procId)
        // Build the string
        std::string& instantiation = instantiations.emplace_back();
        instantiation
            .append(prefix)
            .append("T")
            .append(std::to_string(elementIndex))
            .append("R")
            .append(std::to_string(replicaId));

        out.append(instantiation)
            .append(" = ");
        out.append("Task(")
            .append(std::to_string(strategyIndex))
            .append(",")
            .append(std::to_string(elementIndex))
            .append(",")
            .append(std::to_string(NS_TO_S(replica->rt())))
            .append(", ")
            .append(std::to_string(NS_TO_S(replica->wct())))
            .append(", ")
            .append(std::to_string(schedule->get_mapping(replica)->core()->sysID))
            .append(");\n");
    }

    // Build the voter + monitor
    for (bool isMonitor : {false, true}) {
        std::string suffix = isMonitor ? "Monitor" : "";
        std::string instantiationChar = isMonitor ? "M" : "";

        std::string& instantiation = instantiations.emplace_back();
        instantiation.append(prefix)
            .append("V")
            .append(instantiationChar)
            .append(std::to_string(elementIndex));
        out.append(instantiation)
            .append(" = ");

        if (scheme->replication_count() > 1) {
            // NmrVoter
            out.append("NmrVoter")
                .append(suffix)
                .append("(")
                .append(std::to_string(strategyIndex))
                .append(",")
                .append(std::to_string(elementIndex))
                .append(",")
                .append(std::to_string(scheme->replication_count()))
                .append(");\n");
        } else {
            // NopVoter
            out.append("NopVoter")
                .append(suffix)
                .append("(")
                .append(std::to_string(strategyIndex))
                .append(",")
                .append(std::to_string(elementIndex))
                .append(");\n");
        }
    }

    return out;
}

std::string UppaalFtSsmExporter::create_resultmatcher_for(std::string &prefix, uint64_t strategyIndex, uint64_t resultIndex, FaultToleranceResult *result, std::vector<std::string> &instantiations) {
    std::string& instantiation = instantiations.emplace_back();
    instantiation
        .append(prefix)
        .append("R")
        .append(std::to_string(resultIndex));

    std::string out;
    out
        .append(instantiation)
        .append(" = ResultMatcher(")
        .append(std::to_string(strategyIndex))
        .append(", ")
        .append(std::to_string(resultIndex))
        .append(");\n");
    return out;
}

std::string UppaalFtSsmExporter::create_ssm(
        std::vector<SchedEltPtr> &elements,
        std::vector<FaultToleranceStrategy *> &strategies,
        std::vector<FaultToleranceResult *> &results) {

    auto model = Uppaal::ModelBuilder();

    // Within the UPPAAL model, each strategy gets a consecutive id used for synchronization.
    // These are _not_ the strategy->id() ID's, as those may not be consecutive. Instead, they are indices in strategies[..]
    // For strategy labels, strategy->id() is used.
    std::map<FaultToleranceStrategy *, uint64_t> reverseIdMap;

    // Create all locations
    uint64_t resultId = 0;
    for (size_t i = 0; i < strategies.size(); i++) {
        auto *strategy = strategies[i];
        reverseIdMap[strategy] = i;

        model.enterGroup();
        Uppaal::location_t *strategyLoc = model.createLocation("S" + std::to_string(strategy->id()));
        for (auto &entry : strategy->results()) {
            model.enterGroup();
            Uppaal::location_t *resultLoc = model.createLocation("R" + std::to_string(resultId));
            resultLoc->urgent = true;
            Uppaal::transition_t *resultTransition = model.createTransition(strategyLoc, resultLoc);
            resultTransition->synchronization = "result[" + std::to_string(resultId) + "]?";
            model.exitGroup();
            resultId++;
            (void) entry; // quench the warning about unused variables
        }

        model.exitGroup();
    }

    // Create all result -> strategy transitions
    resultId = 0;
    for (auto strategy : strategies) {
        for (auto &entry : strategy->results()) {
            FaultToleranceResult *result = entry.second.get();
            Uppaal::location_t *resultLoc = model.locationByLabel("R" + std::to_string(resultId));
            Uppaal::location_t *successor = model.locationByLabel("S" + std::to_string(result->successor()->id()));
            Uppaal::transition_t *successorTransition = model.createTransition(resultLoc, successor);

            // Synchronize on the strategy UPPAAL id
            successorTransition->synchronization = "strategy[" + std::to_string(reverseIdMap[result->successor()]) + "]!";
            resultId++;
        }
    }

    // Initial location, which has to activate the first strategy
    model.enterGroup();
    Uppaal::location_t *init = model.createLocation("Init", true);
    init->committed = true;
    Uppaal::transition_t *initTransition = model.createTransition(init, model.locationByLabel("S" + std::to_string(strategies[0]->id())));
    initTransition->synchronization = "strategy[0]!";
    model.exitGroup();

    return model.toModel();
}

std::string UppaalFtSsmExporter::get_embedded_queries() {
    std::string queries;
    timinginfos_t global_deadline = tg->has_global_deadline() ? NS_TO_S(tg->global_deadline()) : 1000UL;

    for (uint64_t iterations = 1; iterations < (1 << 30); iterations <<= 2) {
        // Faults query
        queries.append("<query><formula>");
        queries.append("E [&lt;=" + std::to_string(global_deadline * iterations) + ";32] (max:catastrophic_faults)");
        queries.append("</formula><comment>");
        queries.append("Number of catastrophic faults (" + std::to_string(iterations) + " iterations)");
        queries.append("</comment></query>\n");

        queries.append("<query><formula>");
        queries.append("E [&lt;=" + std::to_string(global_deadline * iterations) + ";32] (max:unmitigated_faults)");
        queries.append("</formula><comment>");
        queries.append("Number of unmitigated faults (" + std::to_string(iterations) + " iterations)");
        queries.append("</comment></query>\n");

        queries.append("<query><formula>");
        queries.append("E [&lt;=" + std::to_string(global_deadline * iterations) + ";32] (max:faults)");
        queries.append("</formula><comment>");
        queries.append("Number of faults (" + std::to_string(iterations) + " iterations)");
        queries.append("</comment></query>\n");

        // Makespan query
        queries.append("<query><formula>");
        queries.append("Pr [&lt;=" + std::to_string(global_deadline * iterations) + "] (&lt;&gt; iterations == " + std::to_string(iterations) + ")");
        queries.append("</formula><comment>");
        queries.append("Iteration validation (for " + std::to_string(iterations) + " iterations)");
        queries.append("</comment></query>\n");
    }
    return queries;
}

std::string UppaalFtSsmExporter::get_verifyta_query(uint64_t iterations) {
    timinginfos_t global_deadline = tg->has_global_deadline() ? NS_TO_S(tg->global_deadline()) : 1000UL;
    return"simulate [<=" + std::to_string(iterations * global_deadline) +  ";1] {iterations == " + std::to_string(iterations) + ", faults, unmitigated_faults, catastrophic_faults}\n";
}
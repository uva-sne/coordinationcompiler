/*
 * Copyright (C) 2022 Lukas Miedema <l.miedema@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once
#ifdef EIGEN3

#include "StateMachine/Exporter/StrategyExporter.hpp"
#include <StateMachine/FaultTolerance/FaultToleranceStrategy.hpp>
#include <StateMachine/StrategyStateMachine.hpp>
#include "Helper_macros.hpp"

class CsvRowWriter : public StrategyExporter<FaultToleranceStrategyManager> {
public:
    CsvRowWriter() = default;
    ~CsvRowWriter() override = default;

    const std::string get_uniqid_rtti() const override {
        return "exporter-ssm-csv";
    }

    std::string help() override;

protected:
    void forward_params(const std::map<std::string, std::string> &map) override;

    void do_export(const boost::filesystem::path &f) override;

    const std::string file_extension() override {
        return ".ssm.csv";
    }
};

REGISTER_SM_EXPORTER(CsvRowWriter, FaultToleranceStrategyManager, "ssm-csv");

#endif

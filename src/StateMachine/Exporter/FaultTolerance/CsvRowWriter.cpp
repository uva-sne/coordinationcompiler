/*
 * Copyright (C) 2022 Lukas Miedema <l.miedema@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef EIGEN3
#include "CsvRowWriter.hpp"
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/date_time/microsec_time_clock.hpp>

IMPLEMENT_HELP(CsvRowWriter,
Utility to help with creating scripts involving large amount of experiments involving the (fault-tolerance) SSM.\n
It appends metadata about the generated strategy population to a CSV file, allowing multiple invocations of Cecile\n
to build a result file. Currently, the following columns are exported:\n
    * name: the filename of the task graph\n
    * timestamp: unix timestamp\n
    * global_deadline: the deadline, from the tey\n
    * element_count: the number of elements in the base schedule (no strategies)\n
    * strategy_count: the number of strategies\n
    * result_count: the number of results\n
    * scheduler: the used scheduler\n
    * linked_status: linked status\n
    * interpretation: the strategy implementation (always fault-tolerance)\n
    * ft_rate: the expected analytically-evaluated fault rate without strategies\n
    * ft_ss_rate: log of the fault rate\n
    * ft_ss_rate_log: the log of the analytically-evaluated fault rate with strategies\n
);

void CsvRowWriter::forward_params(const std::map<std::string, std::string> &args) {
    if (args.count("filename") && !args.at("filename").empty())
        filename = args.at("filename");
}

void CsvRowWriter::do_export(const boost::filesystem::path& fn) {
    std::vector<SchedElt *> elements = this->ssm->get_schedule_elements();
    auto strategies = ssm->strategies_unowned<Strategy>();
    auto results = ssm->results_unowned<Result>();

    std::string linkedStatus;
    switch (const_cast<StrategyStateMachine *>(ssm)->compute_linked_status()) {
        case SSM_FULLY_LINKED:
            linkedStatus = "fully linked";
            break;
        case SSM_PARTIALLY_LINKED:
            linkedStatus = "partially linked";
            break;
        case SSM_NONE_LINKED:
            linkedStatus = "none linked / no results";
            break;
    }
    std::string hasSsm = ssm->is_trivial() ? "none" : ssm->strategy_manager()->description();
    std::string scheduler =  ssm->is_trivial() ? "n/a" : ssm->strategy_manager()->solver()->get_uniqid_rtti();
    std::string interpretation = ssm->is_trivial() ? "none" : ssm->strategy_manager()->description();

    std::ofstream file;
    if (boost::filesystem::exists(fn))
        file.open(fn.c_str(), std::ios::app);
    else {
        file.open(fn.c_str());
        file << "name,timestamp,global_deadline,element_count,strategy_count,result_count,scheduler,linked_status,interpretation,ft_rate,ft_ss_rate,ft_ss_rate_log,ss_energy" << std::endl;
    }
    file << conf->files.coord_basename.c_str() << ",";
    file << to_iso_extended_string(boost::posix_time::microsec_clock::universal_time()) << ",";
    file << tg->global_deadline() << tg->time_unit() << ",";
    file << elements.size() << ",";
    file << strategies.size() << ",";
    file << results.size() << ",";
    file << scheduler << ",";
    file << linkedStatus << ",";
    file << interpretation << ",";

    // Interpretation-specific
    if (typeid(*ssm->strategy_manager()) == typeid(FaultToleranceStrategyManager)) {
        const auto ftManager = dynamic_cast<const FaultToleranceStrategyManager *>(ssm->strategy_manager());
        auto *faultRate = ftManager->ft_protected()->fault_rate();
        auto ss = ftManager->compute_dtmc_rate(ssm);
        file << faultRate->to_string() << ",";
        file << (double) ss.fault_rate << "," << ss.fault_rate.data() << ",";
        file << (ss.energy.has_value() ? std::to_string(ss.energy.value()) + this->tg->energy_unit() : "-") << std::endl;
    } else {
        file << ",,," << std::endl;
    }
    file.close();
}
#endif

/*
 * Copyright (C) 2022 Lukas Miedema <l.miedema@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <StateMachine/FaultTolerance/FaultToleranceStrategy.hpp>
#include <StateMachine/Exporter/StrategyExporter.hpp>
#include <StateMachine/StrategyStateMachine.hpp>
#include "Helper_macros.hpp"

class UppaalFtSsmExporter : public StrategyExporter<FaultToleranceStrategyManager> {
protected:
    void forward_params(const std::map<std::string, std::string> &map) override;

    void do_export(const boost::filesystem::path &f) override;

    const std::string file_extension() override {
        return ".ssm.uppaal.xml";
    }
public:
    UppaalFtSsmExporter() = default;
    ~UppaalFtSsmExporter() override = default;

    const std::string get_uniqid_rtti() const override {
        return "exporter-ssm-ft-uppaal";
    }

    std::string help() override;

private:
    std::string queries_dir = "";

    std::string get_template();
    std::string get_embedded_queries();
    std::string get_verifyta_query(uint64_t iterations);

    static std::string get_timestamp();

    std::string create_system_decls(std::vector<SchedEltPtr> &baseElements,
            std::vector<FaultToleranceStrategy *> &strategies,
            std::vector<FaultToleranceResult *> &results);

    std::string create_result_array(std::vector<SchedEltPtr> &elements,
            std::vector<FaultToleranceStrategy *> &strategies,
            std::vector<FaultToleranceResult *> &results);

    static std::string create_ssm(std::vector<SchedEltPtr> &elements,
            std::vector<FaultToleranceStrategy *> &strategies,
            std::vector<FaultToleranceResult *> &results);

    std::string create_task_decl_for(std::string &, uint64_t strategyIndex, uint64_t elementIndex, SchedElt *, const FaultToleranceScheme *, Schedule *, std::vector<std::string> &instantiations);

    std::string create_resultmatcher_for(std::string &prefix, uint64_t strategyIndex, uint64_t resultIndex, FaultToleranceResult *result, std::vector<std::string> &instantiations);
};

REGISTER_SM_EXPORTER(UppaalFtSsmExporter, FaultToleranceStrategyManager, "uppaal");

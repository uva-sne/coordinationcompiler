/*
 * Copyright (C) 2022 Lukas Miedema <l.miedema@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <StateMachine/StateMachinePass.hpp>

/*!
 * \brief Base class for exporters
 * 
 * Exporters output can be based on any of the two IR: #SystemModel or #Schedule
 */
template<typename SM = StrategyManager>
class StrategyExporter : public StateMachinePass<SM> {
protected:
    //! IR of the current application
    const SystemModel *tg;
    //! the global configuration
    const config_t *conf;
    //! current state of the schedule
    StrategyStateMachine *ssm;
    //! the state machine manager (of the SSM)
     SM *manager;
    //! relative or absolute path/filename given in the configuration
    std::string filename = "";
    
public:
    //! Constructor
    explicit StrategyExporter() {};

    void run(SystemModel *m, config_t *c, StrategyStateMachine *ssm, SM *manager) override {
        this->tg = m;
        this->conf = c;
        this->ssm = ssm;
        this->manager = manager;
        do_export();
    }
    using StateMachinePass<SM>::run;
    using registry_t = registry::Registry<StrategyExporter<SM>, std::string>;

    virtual ~StrategyExporter() = default;

protected:
    /**
     * Return the file extension for this kind of exporter. This is used to generate the default file name,
     * which is the same as the
     * @return
     */
   virtual const std::string file_extension() = 0;

    /**
     * Do the export with either given path, or generate a new file based on the basename of the coordination file.
     */
    virtual void do_export() {
        if(!filename.empty())
            if (filename.at(0) == '/') // is absolute path?
                do_export(filename);
            else
                do_export(conf->files.output_folder / filename);
        else
            do_export(conf->files.output_folder / boost::filesystem::path(conf->files.coord_basename.string() + this->file_extension()));
    }
    /**
     * Perform the export, output should in f
     * 
     * @param f
     */
    virtual void do_export(const boost::filesystem::path &f) = 0;
};

#define REGISTER_SM_EXPORTER(ClassName, SMType, Identifier) \
    REGISTER_SM_SUBCLASS(StrategyExporter, ClassName, SMType, Identifier)


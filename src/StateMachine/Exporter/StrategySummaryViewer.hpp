/*
 * Copyright (C) 2022 Lukas Miedema <l.miedema@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <boost/format.hpp>
#include "StateMachine/Exporter/StrategyExporter.hpp"

class StrategySummaryViewer : public StrategyExporter<> {
public:
    void forward_params(const std::map<std::string, std::string> &args) override;
    std::string help() override;

    explicit StrategySummaryViewer() : StrategyExporter<>() {}
    const std::string get_uniqid_rtti() const override;

protected:
    void check_dependencies() override;
    void do_export() override;
    void do_export(const boost::filesystem::path& f) override;
    const std::string file_extension() override {
        return ".txt";
    }

    /**
     * Export the summary to an output stream (which can be std::cout).
     * @param out
     */
    void do_export(std::ostream& out);

private:
    /**
     * Pad a string to length
     * @param str
     * @param length
     * @return
     */
    static std::string to_length(std::string str, uint32_t length);

    /**
     * Pad a value to length.
     * @tparam T
     * @param value
     * @param format
     * @param length
     * @return
     */
    template<typename T>
    std::string to_length(T value, const std::string &format, uint32_t length) {
        std::string formatted = (boost::format(format) % value).str();
        if (formatted.size() > length)
            return formatted.substr(0, length);
        else return StrategySummaryViewer::to_length(formatted, length);
    }
};

REGISTER_SM_EXPORTER(StrategySummaryViewer, StrategyManager, "summary");
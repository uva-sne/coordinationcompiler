/*
 * Copyright (C) 2022 Lukas Miedema <l.miedema@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <Schedule.hpp>
#include <StateMachine/StateMachinePass.hpp>
#include <StateMachine/Strategy.hpp>

/**
 * Last step in the SSM food chain: compute the successors for each strategy, completing the strategy state machine.
 */
template<typename SM = StrategyManager>
class StrategyLinker : public StateMachinePass<SM> {
protected:
    //! IR of the current application
    SystemModel *tg = nullptr;
    //! the global configuration
    config_t *conf = nullptr;

    void forward_params(const std::map<std::string, std::string> &map) override = 0;

public:
    //! Constructor
    explicit StrategyLinker() = default;

    //! Link the state machine
    virtual void link(StrategyStateMachine *ssm, SM *sm, std::vector<SchedElt *> &elements, std::vector<Result *> &results) = 0;

    //! \copydoc CompilerPass::run
    void run(SystemModel *m, config_t *c, StrategyStateMachine *ssm, SM *sm) override {
        tg = m;
        conf = c;

        auto elements = ssm->get_schedule_elements();
        std::vector<Result *> results = ssm->strategy_manager()->build_results(*ssm, elements);

        this->link(ssm, sm, elements, results);

        // Sanity checking
        for (auto &s : ssm->strategies()) {
            for (auto &r : ssm->strategy_manager()->results(s.get())) {
                assert(r->is_linked(s.get(), *ssm));
                (void) r; // quench the warning about unused variables
            }
        }
    }
    using StateMachinePass<SM>::run; //! Bring in the rest from parent
    using registry_t = registry::Registry<StrategyLinker<SM>, std::string>;
};

#define REGISTER_SM_LINKER(ClassName, SMType, Identifier) \
    REGISTER_SM_SUBCLASS(StrategyLinker, ClassName, SMType, Identifier)

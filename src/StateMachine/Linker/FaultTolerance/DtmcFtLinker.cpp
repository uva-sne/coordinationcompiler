/*
 * Copyright (C) 2022 Lukas Miedema <l.miedema@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef EIGEN3

#include <StateMachine/FaultTolerance/FaultToleranceStrategy.hpp>
#include <StateMachine/FaultTolerance/FtUtils.hpp>
#include <eigen3/Eigen/Dense>
#include <StateMachine/FaultTolerance/DtmcEvaluator.hpp>

#include "DtmcFtLinker.hpp"
#include "Helper_macros.hpp"
#include "StateMachine/StrategyStateMachine.hpp"

IMPLEMENT_HELP(DtmcFtLinker, Link fault-tolerance strategies by evaluating all possible transition functions and picking\n
the one with the lowest steady-state fault rate. This solution is optimal, but very inefficient and fails to work for\n
even moderately large task sets.
);

void DtmcFtLinker::link(StrategyStateMachine *ssm, FaultToleranceStrategyManager *ftManager,
        std::vector<SchedElt *> &elements, std::vector<Result *> &results) {

    // Compute probabilities
    std::map<Strategy *, std::vector<probability_t>> relationalFtTable = ftManager->
            compute_relational_ft_table(ssm, ftManager->compute_basic_ft_table(ssm));

    auto strategies = ssm->strategies_unowned<FaultToleranceStrategy>();
    Utils::INFO("Preparing to link " + to_string(results.size()) + " results onto " + to_string(strategies.size()));

    // Make a 0th mapping
    std::vector<uint32_t> mapping(results.size(), 0); // results[i] maps to strategies[mapping[i]]
    std::vector<uint32_t> bestMapping(results.size(), 0); // copy of mapping

    // Prepare for DTMC steady-state evaluation
    const uint32_t limit = strategies.size();
    // transitionMatrix[indexMap[from], indexMap[to]] is the rate strategy from to strategy to

    DtmcEvaluator evaluator(strategies, relationalFtTable);
    probability_t bestMappingP = evaluator.evaluate_dtmc(mapping).fault_rate;

    double totalEvaluations = pow((double) limit, (double) mapping.size());
    uint64_t performedEvaluations = 0;
    uint64_t lastPrintPerformedEvaluations = 0;
    auto lastPrint = std::chrono::system_clock::now();
    auto interval = std::chrono::seconds(1);

    // And get going
    while (FtUtils::increment(mapping, limit)) {
        if (UnionFind::has_disjoined(mapping, strategies))
            continue;

        // Evaluate mapping
        probability_t p = evaluator.evaluate_dtmc(mapping).fault_rate;
        if (p < bestMappingP) {
            bestMappingP = p;
            bestMapping = mapping;
        }
        performedEvaluations++;

        auto now = std::chrono::system_clock::now();
        if (now - lastPrint > interval) {
            lastPrint = now;
            Utils::INFO("DTMC evaluation " + std::to_string(performedEvaluations * 100.0 / totalEvaluations)
                + "% (" + std::to_string(performedEvaluations) + " / " + std::to_string(totalEvaluations) + ") at "
                + std::to_string(performedEvaluations - lastPrintPerformedEvaluations) + " evaluations/second");
            lastPrintPerformedEvaluations = performedEvaluations;
        }
    }

    // Apply best mapping
    for (uint32_t i = 0; i < results.size(); i++) {
        auto ftResult = dynamic_cast<FaultToleranceResult *>(results[i]);
        auto ftSuccessor = dynamic_cast<FaultToleranceStrategy *>(strategies[bestMapping[i]]);
        ftResult->successor(ftSuccessor);
    }

    Utils::INFO("Best SSM linked with a steady-state DTMC fault rate of " + std::to_string(bestMappingP.data()) + " (log) = " + std::to_string(bestMappingP.operator double()));
}

void DtmcFtLinker::forward_params(const std::map<std::string, std::string> &map) {
}

UnionFind::union_find_t *UnionFind::find_root(union_find_t *x) {
    if (x->parent != nullptr) {
        x->parent = find_root(x->parent);
        return x->parent;
    }
    return x;
}

void UnionFind::union_set(union_find_t *x, union_find_t *y) {
    if (x == y)
        return;
    if (x->size < y->size)
        std::swap(x, y);
    y->parent = x;
    x->size += y->size;
}

bool UnionFind::has_disjoined(std::vector<uint32_t> &successorMapping, std::vector<FaultToleranceStrategy *> &strategies) {
    std::map<FaultToleranceStrategy *, union_find_t> data;

    // Create data structure
    for (auto *x : strategies) {
        union_find_t *xData = &data[x];
        for (const auto &result : x->results() | boost::adaptors::map_values) {
            if (result->p() != 0) { // bugfix: ignore when the probability becomes 0
                union_find_t *yData = &data[strategies[successorMapping[result->result_id()]]];
                UnionFind::union_set(UnionFind::find_root(xData), UnionFind::find_root(yData));
            }
        }
    }

    // Test if they all share one root
    union_find_t *root = nullptr;
    for (auto *x : strategies) {
        union_find_t *otherRoot = UnionFind::find_root(&data[x]);
        if (root == nullptr)
            root = otherRoot;
        else if (root != otherRoot) {
            return true; // disjoinedness found
        }
    }

    return false; // everything shares one root
}

#endif
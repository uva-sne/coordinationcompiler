/*
 * Copyright (C) 2022 Lukas Miedema <l.miedema@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef EIGEN3

#include "StateMachine/FaultTolerance/DtmcEvaluator.hpp"
#include "StateMachine/StrategyStateMachine.hpp"
#include "GreedyFtLinker.hpp"

IMPLEMENT_HELP(GreedyFtLinker, Link fault-tolerance strategies exclusively based on the lowest fault between the current\n
result and the next strategy. Note that this is not the optimal solution: for tasks that have succeeded per the result,\n
it does not make any attempt to protect them in the next iteration (after all, they succeeded already), which may make\n
it more difficult to protect them in the iteration after that.
);

void GreedyFtLinker::link(StrategyStateMachine *ssm, FaultToleranceStrategyManager *ftManager,
        std::vector<SchedElt *> &elements, std::vector<Result *> &results) {
    // Compute probabilities
    std::map<Strategy *, std::vector<probability_t>> relationalFtTable = ftManager->compute_relational_ft_table(ssm, ftManager->compute_basic_ft_table(ssm));

    auto strategies = ssm->strategies_unowned<FaultToleranceStrategy>();
    Utils::INFO("Preparing to link " + to_string(results.size()) + " results onto " + to_string(strategies.size()));

    for (Result *result : results) {
        log_double_t bestCost = 1;
        Strategy *bestSuccessor = nullptr;
        auto *ftResult = dynamic_cast<FaultToleranceResult *>(result);

        for (Strategy *candidateSuccessor : strategies) {
            log_double_t cost = DtmcEvaluator::compute_transition_cost(relationalFtTable[candidateSuccessor], ftResult->probability_table());
            if (bestSuccessor == nullptr || cost < bestCost) {
                bestCost = cost;
                bestSuccessor = candidateSuccessor;
            }
        }

        ftResult->successor(dynamic_cast<FaultToleranceStrategy *>(bestSuccessor));
    }
}

void GreedyFtLinker::forward_params(const std::map<std::string, std::string> &map) {
}

#endif
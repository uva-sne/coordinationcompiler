/*
 * Copyright (C) 2022 Lukas Miedema <l.miedema@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once
#ifdef EIGEN3

#include "StateMachine/FaultTolerance/DtmcEvaluator.hpp"
#include <StateMachine/FaultTolerance/FaultToleranceStrategy.hpp>
#include <StateMachine/Linker/StrategyLinker.hpp>

class EnergyGreedyFtLinker : public StrategyLinker<FaultToleranceStrategyManager> {
protected:
    double _energy_budget; // !< Steady-state energy budget per iteration of the DAG
    void forward_params(const std::map<std::string, std::string> &map) override;

public:
    ~EnergyGreedyFtLinker() override = default;

    const std::string get_uniqid_rtti() const override {
        return "strategy-linker-energy-greedy-ft";
    }

    std::string help() override;

    void link(StrategyStateMachine *ssm, FaultToleranceStrategyManager *sm, std::vector<SchedElt *> &elements,
            std::vector<Result *> &results) override;

    static void link_with_factor(const std::vector<FaultToleranceStrategy *> &strategies, const std::map<Strategy *, std::vector<probability_t>> &relationalFtTable,
            std::vector<uint32_t> &mapping, double factor);

    static double compute_energy_cost(FaultToleranceStrategy *from, FaultToleranceStrategy *to);

    static double calculate_ss_energy(std::vector<FaultToleranceStrategy *> &strategies, ss_dist_t &dist);
};

REGISTER_SM_LINKER(EnergyGreedyFtLinker, FaultToleranceStrategyManager, "energy-greedy");
#endif

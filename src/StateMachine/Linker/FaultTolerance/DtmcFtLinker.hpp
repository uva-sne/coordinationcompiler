/*
 * Copyright (C) 2022 Lukas Miedema <l.miedema@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once
#ifdef EIGEN3

#include <boost/numeric/ublas/matrix.hpp>
#include <StateMachine/Linker/StrategyLinker.hpp>
#include <StateMachine/FaultTolerance/FaultToleranceStrategy.hpp>


// Implement union find
namespace UnionFind {
    struct union_find_t {
        union_find_t *parent = nullptr;
        uint32_t size = 0;
    };

    /**
     * Find for union-find, returns shared root
     * @param x
     * @param y
     */
    union_find_t *find_root(union_find_t *x);

    /**
     * Union for union-find
     * @param x
     * @param y
     */
    void union_set(union_find_t *x, union_find_t *y);

    /**
     * Determine if there are disjoined components in the graph. Note that no disjoined components does not mean
     * every strategy is reachable from every other strategy, rather that for any two strategies there is a common
     * strategy both can reach.
     * @param successorMapping
     * @param strategies
     * @return
     */
    bool has_disjoined(std::vector<uint32_t> &successorMapping, std::vector<FaultToleranceStrategy *> &strategies);
};

/**
 * Strategy linker for fault-tolerant state machines.
 * Links strategies by full state space exploration of all possible successor relations.
 * Possible successor relations are evaluated by computing the steady-state using the Discrete-Time Markov Chain (DTMC)
 * steady-state.
 */
class DtmcFtLinker : public StrategyLinker<FaultToleranceStrategyManager> {
protected:
    void forward_params(const std::map<std::string, std::string> &map) override;

public:

    ~DtmcFtLinker() override = default;

    const std::string get_uniqid_rtti() const override {
        return "strategy-linker-dtmc-ft";
    }

    std::string help() override;

    void link(StrategyStateMachine *, FaultToleranceStrategyManager *, std::vector<SchedElt *> &, std::vector<Result *> &) override;
};

REGISTER_SM_LINKER(DtmcFtLinker, FaultToleranceStrategyManager, "dtmc-exhaustive");
#endif

/*
 * Copyright (C) 2022 Lukas Miedema <l.miedema@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef EIGEN3

#include "StateMachine/StrategyStateMachine.hpp"
#include "EnergyAnnealingFtLinker.hpp"
#include "DtmcFtLinker.hpp"

IMPLEMENT_HELP(EnergyAnnealingFtLinker, Link fault-tolerance strategies by iterative optimization\n
result and the next strategy. Note that this is not the optimal solution: for tasks that have succeeded per the result,\n
it does not make any attempt to protect them in the next iteration (after all, they succeeded already), which may make\n
it more difficult to protect them in the iteration after that.)

#define EPSILON 0.000000000000001

void EnergyAnnealingFtLinker::link(StrategyStateMachine *ssm, FaultToleranceStrategyManager *ftManager,
        std::vector<SchedElt *> &elements, std::vector<Result *> &results) {


    // Compute probabilities
    std::map<Strategy *, std::vector<probability_t>> relationalFtTable = ftManager->compute_relational_ft_table(ssm, ftManager->compute_basic_ft_table(ssm));
    std::vector<FaultToleranceStrategy *> strategies = ssm->strategies_unowned<FaultToleranceStrategy>();
    Utils::INFO("Preparing to link " + to_string(results.size()) + " results onto " + to_string(strategies.size()) + ", with " + std::to_string(this->_iteration_limit) +
            " iterations and a population size of " + std::to_string(this->_population_limit));
    DtmcEvaluator evaluator(strategies, relationalFtTable);

    // Setup annealing
    Annealing::population_member_t bestMember(std::vector<uint32_t>(results.size(), 0), evaluator, strategies);

    std::mt19937 generator(this->_rng_seed);

    for (size_t i = 0; i < this->_iteration_limit; i++) {
        double heat = std::pow(0.5 * (((double) this->_iteration_limit - i) / this->_iteration_limit), 3);

        // Seed population from last bestMember
        std::vector<Annealing::population_member_t> population;
        population.reserve(this->_population_limit + 1);

        for (size_t j = 0; j < this->_population_limit; j++) {
            std::vector<uint32_t> newMapping = bestMember.mutate(generator, heat, strategies.size());
            if (!UnionFind::has_disjoined(newMapping, strategies))
                population.emplace_back(std::move(newMapping), evaluator, strategies);
        }

        // Seed from single-strategy solutions (if this is the first iteration)
        if (i == 0) {
            for (auto &strategy: strategies) {
                auto &protect = strategy->protected_elements();
                if (
                        std::all_of(protect.begin(),  protect.end(), [](bool b) -> bool { return b; }) ||
                        std::none_of(protect.begin(),  protect.end(), [](bool b) -> bool { return b; })
                        ) {
                    // Seed with the all-protecting and the none-protecting degenerate strategy
                    population.emplace_back(std::vector<uint32_t>(results.size(), strategy->id()), evaluator, strategies);
                }
            }
        } else
            population.emplace_back(std::move(bestMember)); // retain previous best

        // Sort them by quality
        // TODO: we only care about the best, so this could be done in O(n) instead of O(n log n)
        std::sort(population.begin(), population.end(), [this](Annealing::population_member_t const &lhs, Annealing::population_member_t const &rhs) -> bool {
            return lhs.is_better_than(rhs, this->_energy_budget);
        });

        bestMember = std::move(population.front());
        Utils::INFO("- iteration: " + std::to_string(i) + ", current best: energy = " + std::to_string(bestMember.energy) + ", fault rate (log) = " +
                std::to_string(bestMember.fault_rate.data()) + ", energy budget = " + std::to_string(_energy_budget) + ", heat: " + std::to_string(heat) + ", population: " +
                std::to_string(population.size()));
    }

    // Present final result
    ftManager->energy(bestMember.energy);

    size_t resultIndex = 0;
    for (FaultToleranceStrategy *fromStrategy: strategies) {
        for (FaultToleranceResultPtr &ftResult: fromStrategy->results() | boost::adaptors::map_values) {
            ftResult->successor(strategies[bestMember.mapping[resultIndex++]]);
        }
    }
}

void EnergyAnnealingFtLinker::forward_params(const std::map<std::string, std::string> &map) {
    if (!map.count("energy-budget"))
        throw CompilerException("energy-annealing-ft-linker", "Must specify steady-state energy budget");
    this->_energy_budget = std::stod(map.find("energy-budget")->second);

    if (!map.count("iteration-count"))
        throw CompilerException("energy-annealing-ft-linker", "Must specify iteration-count");
    this->_iteration_limit = std::stoll(map.find("iteration-count")->second);

    if (!map.count("population-count"))
        throw CompilerException("energy-annealing-ft-linker", "Must specify population-count");
    this->_population_limit = std::stoll(map.find("population-count")->second);

    if (map.count("seed"))
        this->_rng_seed = std::stoll(map.find("seed")->second);
    else
        Utils::INFO("Using default seed for energy-annealing of " + std::to_string(this->_rng_seed));
}

double EnergyAnnealingFtLinker::calculate_ss_energy(std::vector<FaultToleranceStrategy *> &strategies, ss_dist_t &dist) {
    double energy = 0;
    for (size_t i = 0; i < strategies.size(); i++)
        energy += strategies[i]->energy() * dist.ss[i];
    return energy;
}

std::vector<uint32_t> Annealing::population_member_t::mutate(Annealing::rng &generator, double heat, uint32_t strategyCount) const {
    // Create a complete copy
    auto newMapping = this->mapping;
    auto heatDist = Annealing::dist(0, 1);
    auto ssDist = std::uniform_int_distribution<uint32_t>(0, strategyCount - 2); // range is inclusive, and another minus one to block self-assignment

    // Randomly change some values
    for (unsigned int &mappedValue : newMapping) {
        if (heatDist(generator) < heat) {
            uint32_t newValue = ssDist(generator);
            if (newValue >= mappedValue)
                newValue++; // prevent self-assignment
            mappedValue = newValue;
        }
    }

    // Ensure we change at least one value in case the heat is very low
    uint32_t &mappedValue = newMapping[std::uniform_int_distribution<uint32_t>(0, newMapping.size() - 1)(generator)];
    uint32_t newValue = ssDist(generator);
    if (newValue >= mappedValue)
        newValue++; // prevent self-assignment
    mappedValue = newValue;

    return newMapping;
}

Annealing::population_member_t::population_member_t(std::vector<uint32_t> mapping, DtmcEvaluator &evaluator,
        std::vector<FaultToleranceStrategy *> &strategies) : mapping(std::move(mapping)) {
    ss_dist_t ss = evaluator.evaluate_dtmc(this->mapping);
    this->energy = EnergyAnnealingFtLinker::calculate_ss_energy(strategies, ss);
    this->fault_rate = ss.fault_rate;
}

bool Annealing::population_member_t::is_better_than(Annealing::population_member_t const &other, double energyBudget) const {
    // First: focus on the energy budget
    if (energy > energyBudget || other.energy > energyBudget)
        return energy < other.energy;
    // Then: focus on SS, then energy
    if (fault_rate == other.fault_rate)
        return energy < other.energy;
    return fault_rate < other.fault_rate;
}
#endif

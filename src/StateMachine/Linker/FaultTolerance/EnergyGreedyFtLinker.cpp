/*
 * Copyright (C) 2022 Lukas Miedema <l.miedema@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef EIGEN3

#include "StateMachine/StrategyStateMachine.hpp"
#include "EnergyGreedyFtLinker.hpp"

IMPLEMENT_HELP(EnergyGreedyFtLinker, Link fault-tolerance strategies exclusively based on the lowest fault between the current\n
result and the next strategy. The trade-off between energy and reliablity is made based on a unitless factor, which is binary
searched across. A DTMC evaluator is used to determine the steady-state energy consumption (with unit), and to adjust it accordingly.
Note that this is not the optimal solution: for tasks that have succeeded per the result,\n
it does not make any attempt to protect them in the next iteration (after all, they succeeded already), which may make\n
it more difficult to protect them in the iteration after that.
);

#define EPSILON 0.000000000000001

void EnergyGreedyFtLinker::link(StrategyStateMachine *ssm, FaultToleranceStrategyManager *ftManager,
        std::vector<SchedElt *> &elements, std::vector<Result *> &results) {

    // Compute probabilities
    std::map<Strategy *, std::vector<probability_t>> relationalFtTable = ftManager->compute_relational_ft_table(ssm, ftManager->compute_basic_ft_table(ssm));
    auto strategies = ssm->strategies_unowned<FaultToleranceStrategy>();
    Utils::INFO("Preparing to link " + to_string(results.size()) + " results onto " + to_string(strategies.size()));
    DtmcEvaluator evaluator(strategies, relationalFtTable);

    // Setup bin search
    // factor indicates how important energy is, factor of 1 optimizes for energy, factor of 0 optimizes for reliability
    float factor = 0.5;
    float delta_factor = 0.25;
    float best_factor = 0.0;
    std::vector<uint32_t> mapping(results.size(), 0); // results[i] maps to strategies[mapping[i]]

    while (delta_factor > EPSILON) {
        Utils::DEBUG("\t---- factor: " + std::to_string(factor));
        EnergyGreedyFtLinker::link_with_factor(strategies, relationalFtTable, mapping, factor);

        // Calculate steady-state energy
        ss_dist_t ss = evaluator.evaluate_dtmc(mapping);
        double ssEnergy = EnergyGreedyFtLinker::calculate_ss_energy(strategies, ss);

        Utils::INFO("Linking with factor " + std::to_string(factor) + ", energy = " + std::to_string(ssEnergy) +
            ", fault rate (log) = " + std::to_string(ss.fault_rate.data()) + ", energy budget = " + std::to_string(_energy_budget));

        if (ssEnergy >= this->_energy_budget) { // uses too much energy
            factor += delta_factor;
        } else {
            best_factor = factor;
            factor -= delta_factor;
        }
        delta_factor /= 2;
    }

    // Solution may not be found if energy_budget is unschedulable
    if (factor >= 1 - 2 * EPSILON)
        best_factor = 1;

    // Present final result
    EnergyGreedyFtLinker::link_with_factor(strategies, relationalFtTable, mapping, best_factor);
    ss_dist_t ss = evaluator.evaluate_dtmc(mapping);
    double ssEnergy = EnergyGreedyFtLinker::calculate_ss_energy(strategies, ss);
    ftManager->energy(ssEnergy);

    size_t resultIndex = 0;
    for (FaultToleranceStrategy *fromStrategy : strategies) {
        for (FaultToleranceResultPtr &ftResult: fromStrategy->results() | boost::adaptors::map_values) {
            ftResult->successor(strategies[mapping[resultIndex++]]);
        }
    }
}

void EnergyGreedyFtLinker::link_with_factor(
        const std::vector<FaultToleranceStrategy *> &strategies,
        const std::map<Strategy *, std::vector<probability_t>> &relationalFtTable,
        std::vector<uint32_t> &mapping, double factor) {

    size_t resultIndex = 0;
    for (FaultToleranceStrategy *fromStrategy : strategies) {
        for (FaultToleranceResultPtr &ftResult: fromStrategy->results() | boost::adaptors::map_values) {
            double bestCost = -1;
            size_t bestSuccessor = 0;

            for (size_t i = 0; i < strategies.size(); i++) {
                auto *candidateSuccessor = strategies[i];
                log_double_t faultRate = DtmcEvaluator::compute_transition_cost(relationalFtTable.find(candidateSuccessor)->second, ftResult->probability_table());
                double energyCost = ((double) fromStrategy->energy()) + ((double) candidateSuccessor->energy());
                double cost = (std::max<double>(-100, faultRate.data())) * (1 - factor) + energyCost * factor; // take the data, i.e. the log of the fault rate because its values are so small
                // make sure it is higher than -100, to deal with cases where the fault rate is 0 (log(0) = minus infinity)

                if (bestCost == -1 || cost < bestCost) {
                    bestCost = cost;
                    bestSuccessor = i;
                }
            }

            mapping[resultIndex++] = bestSuccessor;
        }
    }
}

void EnergyGreedyFtLinker::forward_params(const std::map<std::string, std::string> &map) {
    if (!map.count("energy-budget"))
        throw CompilerException("energy-greedy-ft-linker", "Must specify steady-state energy budget");
    this->_energy_budget = std::stod(map.find("energy-budget")->second);
}

double EnergyGreedyFtLinker::calculate_ss_energy(std::vector<FaultToleranceStrategy *> &strategies, ss_dist_t &dist) {
    double energy = 0;
    for (size_t i = 0; i < strategies.size(); i++)
        energy += strategies[i]->energy() * dist.ss[i];
    return energy;
}

#endif
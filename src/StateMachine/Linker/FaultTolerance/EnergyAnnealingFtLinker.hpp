/*
 * Copyright (C) 2022 Lukas Miedema <l.miedema@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once
#ifdef EIGEN3

#include "StateMachine/FaultTolerance/DtmcEvaluator.hpp"
#include <StateMachine/FaultTolerance/FaultToleranceStrategy.hpp>
#include <StateMachine/Linker/StrategyLinker.hpp>
#include <random>

namespace Annealing {
    using rng = std::mt19937;
    using dist = std::uniform_real_distribution<float>;


    struct population_member_t {
        std::vector<uint32_t> mapping;
        double energy = 0;
        log_double_t fault_rate = 0;

        population_member_t(const std::vector<uint32_t> mapping, DtmcEvaluator &evaluator, std::vector<FaultToleranceStrategy *> &strategies);
        bool is_better_than(population_member_t const &other, double energyBudget) const;
        std::vector<uint32_t> mutate(rng &generator, double heat, uint32_t strategyCount) const;
    };
}

class EnergyAnnealingFtLinker : public StrategyLinker<FaultToleranceStrategyManager> {
protected:
    double _energy_budget; // !< Steady-state energy budget per iteration of the DAG
    size_t _iteration_limit; // !< Number of iterations
    size_t _population_limit; // !< Number of population members per iteration
    uint64_t _rng_seed = 42; // !< Seed for the RNG, for reproducibility

    void forward_params(const std::map<std::string, std::string> &map) override;

public:
    ~EnergyAnnealingFtLinker() override = default;

    const std::string get_uniqid_rtti() const override {
        return "strategy-linker-energy-greedy-ft";
    }

    std::string help() override;

    void link(StrategyStateMachine *ssm, FaultToleranceStrategyManager *sm, std::vector<SchedElt *> &elements,
            std::vector<Result *> &results) override;

    static double calculate_ss_energy(std::vector<FaultToleranceStrategy *> &strategies, ss_dist_t &dist);
};

REGISTER_SM_LINKER(EnergyAnnealingFtLinker, FaultToleranceStrategyManager, "energy-annealing");
#endif

/*
 * Copyright (C) 2022 Lukas Miedema <l.miedema@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <SystemModel.hpp>
#include <Solvers/Solver.hpp>
#include <StateMachine/FaultTolerance/NMR.hpp>
#include "Strategy.hpp"


/* -------------
 * Strategy
 * -------------
 */
void Strategy::schedule(
        SystemModel &tg,
        config_t &config,
        StrategyManager &manager,
        std::vector<SchedEltPtr> &baseElements,
        Schedule &schedule) const {
    assert(!schedule.is_built()); // must be removed by the caller
    assert(!baseElements.empty());
    assert(tg.has_global_deadline());

    // Create FT sched elements and transfer to the schedule
    std::map<std::string, SchedEltPtr> ftElements = this->export_to_schedule(baseElements, manager);
    schedule.elements_lut() = std::move(ftElements);
    manager.solver()->run(&tg, &config, &schedule);
}

/* -------------
 * EmptyStrategy
 * -------------
 */
std::map<std::string, SchedEltPtr> EmptyStrategy::export_to_schedule(std::vector<SchedEltPtr> &elements, StrategyManager &manager) const {
    // Do not allocate new elements, instead move existing elements
    std::map<std::string, SchedEltPtr> out;
    for (auto &element : elements) {
        out[element->id()] = std::move(element);
    }
    elements.clear(); // they're all null now
    return out;
}

std::string EmptyStrategy::get_role_for_elt(uint64_t i, const StrategyManager &manager) {
    return "as-is";
}

schedulability_result_t EmptyStrategy::is_schedulable(SystemModel &tg, config_t &config, StrategyManager &manager, std::vector<SchedEltPtr> &baseElements, Schedule &schedule) const {
    assert(!schedule.is_built());
    assert(!baseElements.empty());
    assert(tg.has_global_deadline());

    std::map<std::string, SchedEltPtr> ftElements = this->export_to_schedule(baseElements, manager);
    schedule.elements_lut() = std::move(ftElements);

    manager.solver()->run(&tg, &config, &schedule);
    schedulability_result_t result = {
        .is_schedulable = schedule.makespan() <= tg.global_deadline() && schedule.status() == ScheduleProperties::SCHED_COMPLETE,
        .makespan = schedule.makespan()
    };

    // EmptyStrategy moves elements instead of allocating them, so move them back
    for (SchedEltPtr &elt : schedule.elements_lut() | boost::adaptors::map_values)
        baseElements.push_back(std::move(elt));

    schedule.clear_assigned();
    schedule.elements_lut().clear();
    return result;
}
/*
 * Copyright (C) 2022 Lukas Miedema <l.miedema@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "StrategyStateMachine.hpp"
#include "ScheduleFactory.hpp"

StrategyStateMachine::operator Schedule *() {
    if (this->_active_scope == nullptr && !is_trivial())
        throw CompilerException("ssm", "Cannot convert non-trivial unscoped SSM to a single schedule");
    return this->_schedule.get();
}

StrategyStateMachine::StrategyStateMachine(std::unique_ptr<Schedule> schedule) : _schedule(std::move(schedule)), _strategy_manager(nullptr) {
    assert(!_schedule->is_built());
    this->_strategies.push_back(std::make_unique<EmptyStrategy>());
}

bool StrategyStateMachine::is_trivial() const {
    if (this->_strategies.size() != 1 || this->_strategy_manager != nullptr)
        return false;
    if (this->_strategies[0]->is_trivial())
        return true;
    return false;
}

void StrategyStateMachine::clear() {
    this->_strategies.clear();
    this->_schedule->clear();
    this->_strategy_manager = nullptr;
}


void StrategyStateMachine::enter_scope(Strategy *activeStrategy, SystemModel *tg, config_t *c) {
    assert(this->_active_scope == nullptr);
    assert(!activeStrategy->is_trivial());
    this->_active_scope = activeStrategy;

    if (this->_strategy_manager == nullptr || this->is_trivial())
        throw CompilerException("ssm", "No strategy manager is set. Scopes only make sense on non-trivial SSMs");

    // Non-trivial scope rebuilds the schedule elements -> ensure its there
    this->_schedule->clear();
    ScheduleFactory::build_elements(this->_schedule.get());

    std::vector<SchedEltPtr> elements = Utils::movevalues(this->_schedule->elements_lut());
    this->_schedule->elements_lut() = activeStrategy->export_to_schedule(elements, *this->strategy_manager());
    this->_strategy_manager->solver()->run(tg, c, this->_schedule.get());
}

void StrategyStateMachine::exit_scope() {
    assert(this->_active_scope != nullptr);
    this->_active_scope = nullptr;
    this->_schedule->clear();
}

std::vector<SchedElt *> StrategyStateMachine::get_schedule_elements() {
    assert(this->_schedule->is_built());
    return this->_schedule->elements_unowned();
}

std::vector<SchedEltPtr> StrategyStateMachine::get_schedule_elements_exclusive() {
    assert(this->_schedule->is_built());
    std::map<std::string, SchedEltPtr> &lut = this->_schedule->elements_lut();
    std::vector<SchedEltPtr> elements = Utils::movevalues(lut);
    lut.clear();
    return elements;
}

void StrategyStateMachine::set_schedule_elements(std::vector<SchedEltPtr> elements) {
    // FIXME: this may break the order of the elements as they're in a map and not in a vector...
    // The API here and interaction with Schedule#build() needs some rethought
    assert(!this->_schedule->is_built());
    std::map<std::string, SchedEltPtr> &lut = this->_schedule->elements_lut();
    for (SchedEltPtr &e : elements)
        lut[e->id()] = std::move(e);
}

ssm_linked_status_t StrategyStateMachine::compute_linked_status() {
    if (this->_strategy_manager == nullptr)
        return SSM_NONE_LINKED;

    bool oneLinked = false;
    bool allLinked = true;

    for (auto const &strategy : strategies()) {
        auto results = strategy_manager()->results(strategy.get());
        if (results.empty())
            allLinked = false;
        for (auto result : results) {
            if (result->is_linked(strategy.get(), *this)) {
                oneLinked = true;
            } else {
                allLinked = false;
            }
        }
    }

    if (allLinked)
        return SSM_FULLY_LINKED;
    if (oneLinked)
        return SSM_PARTIALLY_LINKED;
    return SSM_NONE_LINKED;
}

void StrategyStateMachine::initialize_ssm(std::unique_ptr<StrategyManager> manager) {
    assert(manager != nullptr);
    if (this->_schedule->is_built())
        throw CompilerException("ssm", "Cannot create a strategy state machine when a singular schedule already exists");

    if (this->_strategy_manager != nullptr)
        throw CompilerException("ssm", "Cannot create a new strategy state machine: manager already exists");

    // Clear the trivial strategy
    _strategies.clear();
    ScheduleFactory::build_elements(_schedule.get());
    this->strategy_manager(std::move(manager));
}

std::pair<std::string, std::string> StrategyStateMachine::split_id(const std::string &fullId) {
    auto splitAt = fullId.find("::");
    if (splitAt == std::string::npos)
        throw CompilerException("strategy-state-machine", "Unrecognized strategy id " + fullId +
        ". For strategy passes, use <type>::<pass name> in the 'id' field, e.g. <strategy-generator id='ft::greedy'/>");
    std::string prefix = fullId.substr(0, splitAt);
    std::string id = fullId.substr(splitAt + 2, std::string::npos);
    return {prefix, id};
}


/*
 * Copyright (C) 2022 Lukas Miedema <l.miedema@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <typeinfo>

#include "CompilerPass.hpp"
#include "Strategy.hpp"
#include "StrategyStateMachine.hpp"

/**
 * Base for state machine passes
 * @tparam SM the required strategy manager, will be validated prior to calling "run"
 * @tparam TRequiresSM true if the strategy manager may not be null. Set to false for generating compiler passes.
 */
template<typename SM, bool TRequiresSM = true>
class StateMachinePass : public CompilerPass {
    static_assert(std::is_base_of<StrategyManager, SM>::value, "SM must inherit from StrategyManager");

public:
    ~StateMachinePass() override {}

    /**
     * Typecheck and prepare the SSM for calling run with the strategy manager.
     * @param m
     * @param c
     * @param ssm
     */
    void run(SystemModel *m, config_t *c, StrategyStateMachine *ssm) override {
         StrategyManager *x = ssm->strategy_manager();
         if (TRequiresSM && x == nullptr)
             throw CompilerException("state-machine", "Compiler pass " + this->get_uniqid_rtti() +
             " requires a strategy state machine, but none generated. Use a generator pass before.");
         SM *sm = dynamic_cast<SM *>(x); // allows subtypes of SM
         if (x != sm)
             throw CompilerException("state-machine", "Incompatible state machine types, pass " +
             this->get_uniqid_rtti() + " expected a (subtype of) " + std::string(typeid(SM).name()) +
             " but got " + std::string(typeid(x).name()));
         this->run(m, c, ssm, sm);
    }

    virtual void run(SystemModel *m, config_t *c, StrategyStateMachine *ssm, SM *i) = 0;
    using CompilerPass::run; //! Bring in the rest from CompilerPass

    void set_params(const std::map<std::string, std::string> &args) override { CompilerPass::set_params(args); };
    const std::string get_uniqid_rtti() const override = 0;
    std::string help() override = 0;

protected:
    void forward_params(const std::map<std::string, std::string> &map) override { CompilerPass::set_params(map); };
};


/*!
 * \brief Base macro to register state machine passes, must not be called directly
 */
#define REGISTER_SM_SUBCLASS(DerivedType, ClassName, SMType, Identifier) \
  REGISTER_SUBCLASS(SINGLE_ARG(DerivedType < SMType >), ClassName, std::string, Identifier)

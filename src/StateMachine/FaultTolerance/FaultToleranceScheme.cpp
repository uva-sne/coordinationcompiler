/*
 * Copyright (C) 2022 Lukas Miedema <l.miedema@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "FaultToleranceScheme.hpp"

void UnresolvedScheduleElement::add_predecessor(SchedElt *element) {
    predecessors.push_back(element);
}

void UnresolvedScheduleElement::add_successor(SchedElt *element) {
    successors.push_back(element);
}

void ResolvedScheduleElement::add_predecessor(SchedElt *element) {
    for (SchedElt *e : this->elements)
        element->add_successor(e);
}

void ResolvedScheduleElement::add_successor(SchedElt *element) {
    for (SchedElt *e : this->elements)
        e->add_successor(element);
}

ResolvedScheduleElement::ResolvedScheduleElement(
        UnresolvedScheduleElement* unresolved,
        std::vector<SchedElt *>& elements) {
    this->elements = elements;
    if (unresolved != nullptr) {
        for (SchedElt *e: this->elements) {
            for (SchedElt *predecessor: unresolved->predecessors)
                predecessor->add_successor(e);
            for (SchedElt *successor: unresolved->successors)
                e->add_successor(successor);
        }
    }
}

FaultRateSpec FaultToleranceScheme::DEFAULT_FAULT_RATE = FaultRateSpec(1, 1000 * 1000 * 1000);
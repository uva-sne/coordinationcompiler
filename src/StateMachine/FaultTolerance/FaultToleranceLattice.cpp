/*
 * Copyright (C) 2022 Lukas Miedema <l.miedema@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "FaultToleranceLattice.hpp"
#include "FtUtils.hpp"

FaultToleranceLattice::FaultToleranceLattice(uint32_t element_count) : _element_count(element_count) {
}

void FaultToleranceLattice::add(FaultToleranceStrategyPtr strategy) {
    assert(strategy->protected_elements().size() == this->_element_count);
    const auto *elements = &strategy->protected_elements();
    this->_strategies[elements] = {.state = STATE_UNKNOWN, .strategy = std::move(strategy)};
}

void FaultToleranceLattice::visit_parents_recursive(FaultToleranceStrategy *strategy, strategyvisitor_t &visitor) {
    const std::vector<bool>& mask = strategy->protected_elements();
    std::vector<bool> counter = strategy->protected_elements();
    while (FtUtils::increment_with_mask(counter, mask))
        visitor(this->_strategies.at(&counter));
}

void FaultToleranceLattice::visit_children_recursive(FaultToleranceStrategy *strategy, strategyvisitor_t &visitor) {
    std::vector<bool> mask = strategy->protected_elements();
    for (auto &&i : mask)
        i = !i; // invert mask to target values "below" us
    std::vector<bool> counter = strategy->protected_elements();
    while (FtUtils::increment_with_mask(counter, mask))
        visitor(this->_strategies.at(&counter));
}

void FaultToleranceLattice::visit_all(strategyvisitor_t& visitor) {
    std::vector<bool> counter(this->_element_count, false);
    do
        visitor(this->_strategies.at(&counter));
    while (FtUtils::increment(counter));
}


void FaultToleranceLattice::visit_parents(FaultToleranceStrategy *strategy, strategyvisitor_t &visitor) {
    std::vector<bool> counter = strategy->protected_elements();
    for (uint32_t i = 0; i < this->_element_count; i++) {
        if (counter[i])
            continue;
        counter[i] = true;
        visitor(this->_strategies.at(&counter));
        counter[i] = false;
    }
}

void FaultToleranceLattice::visit_children(FaultToleranceStrategy *strategy, strategyvisitor_t &visitor) {
    std::vector<bool> counter = strategy->protected_elements();
    for (uint32_t i = 0; i < this->_element_count; i++) {
        if (!counter[i])
            continue;
        counter[i] = false;
        visitor(this->_strategies.at(&counter));
        counter[i] = true;
    }
}

latticenode_t *FaultToleranceLattice::top() {
    std::vector<bool> topKey(this->_element_count, true);
    return &this->_strategies.at(&topKey);
}

latticenode_t *FaultToleranceLattice::bottom() {
    std::vector<bool> bottomKey(this->_element_count, false);
    return &this->_strategies.at(&bottomKey);
}

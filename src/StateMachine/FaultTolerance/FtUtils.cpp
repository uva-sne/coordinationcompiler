/*
 * Copyright (C) 2022 Lukas Miedema <l.miedema@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "FtUtils.hpp"

using std::size_t;

bool FtUtils::increment(std::vector<bool> &vec) {
    bool overflow = true;
    for (size_t i = 0; i < vec.size() && overflow; i++) {
        overflow = vec[i];
        vec[i].flip();
    }
    return !overflow;
}

bool FtUtils::increment(std::vector<uint32_t> &vec, uint32_t limit) {
    bool overflow = true;
    for (size_t i = 0; i < vec.size() && overflow; i++) {
        overflow = ++vec[i] >= limit;
        if (overflow)
            vec[i] = 0;
    }
    return !overflow;
}

bool FtUtils::increment_with_mask(std::vector<bool> &vec, const std::vector<bool> &mask) {
    bool overflow = true;
    for (size_t i = 0; i < vec.size() && overflow; i++) {
        if (mask[i])
            continue;
        overflow = vec[i];
        vec[i].flip();
    }
    return !overflow;
}

uint64_t FtUtils::count_ones(const std::vector<bool> &vec) {
    uint64_t c = 0;
    for (const auto b: vec)
        c += b;
    return c;
}

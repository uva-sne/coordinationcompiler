/*
 * Copyright (C) 2022 Lukas Miedema <l.miedema@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <StateMachine/Strategy.hpp>
#include "FaultToleranceScheme.hpp"
#include "Helper_macros.hpp"
#include "FtUtils.hpp"

class FaultToleranceStrategy;
class FaultToleranceResult;

typedef std::unique_ptr<FaultToleranceStrategy> FaultToleranceStrategyPtr;
typedef std::unique_ptr<FaultToleranceResult> FaultToleranceResultPtr;

struct ft_ss_t {
    log_double_t fault_rate;
    std::optional<double> energy;
    std::vector<double> ss; // P(strategy[i]) = ss[i]
};

/*!
 * Result of a schedulability test (which is done w/ construction)
 */
struct energy_schedulability_result_t  {
    bool is_schedulable;
    timinginfos_t makespan;
    energycons_t energy; //<! Schedule energy
    std::vector<ft_version_info> versions; //<! Frequency and WCET affects fault rate
};

/**
 * FaultTolerance implementation of a Result. Explicitly tracks its successor.
 */
class FaultToleranceResult : public Result {
    friend class FaultToleranceStrategyManager;
private:
    uint32_t _result_id; // unique across the SSM
    /*
     * Encoding of the result: the vector has length which
     *  - if the "unprotected" FT scheme provides no info, the length is the number of 1's in the owning strategy vector
     *  - if the "unprotected" FT scheme provides a detection probability, the length is equal to the owning strategy vector
     */
    std::vector<bool> _result;
    probability_t _p; // chance of getting chosen, sum over all results for one strategy it's 1
    FaultToleranceStrategy *_successor = nullptr;
    std::vector<probability_t> _probability_table;

    /**
     * Construct a new Result. Moves the result vector.
     * @param id globally sequential id
     * @param p chance of getting chosen such that the sum of all results associated with one strategy is 1
     * @param result success/fail for each element under protection
     * @param probability_table success probability
     * @param result
     */
    explicit FaultToleranceResult(uint32_t id, probability_t p, std::vector<bool> result, std::vector<probability_t> probability_table) :
        _result_id(id), _result(result), _p(p), _probability_table(probability_table) {};

public:
    const std::string id() const override;
    Strategy *successor(Strategy *owner, StrategyStateMachine &ssm) override;
    ACCESSORS_RW(FaultToleranceResult, FaultToleranceStrategy *, successor);
    ACCESSORS_RW(FaultToleranceResult, probability_t, p);
    ACCESSORS_R(FaultToleranceResult, uint32_t, result_id);
    ACCESSORS_R(FaultToleranceResult, std::vector<probability_t>&, probability_table);
    ACCESSORS_R(FaultToleranceResult, std::vector<bool>&, result);
};

/**
 * \brief A strategy protecting a task using fault tolerance. The exact FT technique depends on the FaultToleranceScheme.
 */
class FaultToleranceStrategy : public Strategy {
    friend class FaultToleranceStrategyManager;
protected:
    std::map<std::vector<bool> *, FaultToleranceResultPtr> _results; // not populated prior to linking
    std::vector<bool> _protected_elements; //<! Elements protected by this strategy
    energycons_t _energy = -1; //<! Energy consumption of this strategy, must be set externally
    timinginfos_t _makespan; //<! Makespan of this strategy, must be set externally

public:
    explicit FaultToleranceStrategy(uint64_t id, std::vector<bool> protectedElements) : Strategy(id), _protected_elements(std::move(protectedElements)) {}
    FaultToleranceStrategy(FaultToleranceStrategy &&rhs) noexcept : Strategy(rhs.id()), _protected_elements(std::move(rhs._protected_elements)) {}
    FaultToleranceStrategy& operator=(FaultToleranceStrategy&&) = default;
    virtual ~FaultToleranceStrategy() = default;

    /**
     * @return True if this strategy protects nothing (trivial strategy)
     */
    bool is_trivial() override {
        for (bool e : _protected_elements)
            if (e)
                return false;
        return true;
    }

    bool has_energy() const {
        return _energy != ((energycons_t) -1);
    }

    std::string get_role_for_elt(uint64_t i, const StrategyManager &manager) override;

    schedulability_result_t is_schedulable(SystemModel &tg, config_t &config, StrategyManager &manager, std::vector<SchedEltPtr> &baseElements, Schedule &emptySchedule) const override;

    std::map<std::string, SchedEltPtr> export_to_schedule(std::vector<SchedEltPtr> &elements, StrategyManager &manager) const override;

    /**
     * Produce an element list for the schedule.
     * @param parent the parent SSM
     * @param ftProtected the FT scheme to apply to protected tasks
     * @param ftUnprotected the FT scheme for the rest
     */
    std::map<std::string, SchedEltPtr> export_to_schedule(std::vector<SchedEltPtr> &elements, FaultToleranceScheme &ftProtected, FaultToleranceScheme &ftUnprotected) const;

    bool has_results();

    ACCESSORS_R(FaultToleranceStrategy, SINGLE_ARG(std::map<std::vector<bool> *, FaultToleranceResultPtr>&), results);
    ACCESSORS_RW(FaultToleranceStrategy, energycons_t, energy);
    ACCESSORS_RW(FaultToleranceStrategy, timinginfos_t, makespan);
    ACCESSORS_R_CONSTONLY(FaultToleranceStrategy, std::vector<bool>&, protected_elements);

    friend bool operator<(const FaultToleranceStrategy &lhs, const FaultToleranceStrategy &rhs) {
        return lhs.protected_elements() < rhs.protected_elements();
    }
};

class EnergyFaultToleranceStrategy : public FaultToleranceStrategy {
private:
    std::vector<ft_version_info> _versions;
public:
    explicit EnergyFaultToleranceStrategy(uint64_t id, std::vector<bool> protectedElements) :
        FaultToleranceStrategy(id, std::move(protectedElements)) {};
    ACCESSORS_RW(EnergyFaultToleranceStrategy, std::vector<ft_version_info> &, versions);

    energy_schedulability_result_t is_schedulable_energy(SystemModel &tg, config_t &config, StrategyManager &manager, std::vector<SchedEltPtr> &baseElements, Schedule &schedule) const;
};

class FaultToleranceStrategyManager : public StrategyManager {
private:
    FaultToleranceScheme *_ft_protected; // owned by the compiler pass
    FaultToleranceScheme *_ft_unprotected;

    double _energy = -1; //<! Steady state energy
public:
    explicit FaultToleranceStrategyManager(FaultToleranceScheme *ftProtected, FaultToleranceScheme *ftUnprotected, Solver *solver) : StrategyManager(solver),
            _ft_protected(ftProtected), _ft_unprotected(ftUnprotected) {};

    std::string description() const override {
        return "fault tolerance (" + this->_ft_protected->describe() + "/" + this->_ft_unprotected->describe() + ")";
    }

    std::map<Strategy *, std::vector<probability_t>> compute_basic_ft_table(const StrategyStateMachine *ssm) const;
    std::map<Strategy *, std::vector<probability_t>> compute_relational_ft_table(const StrategyStateMachine *ssm,
            const std::map<Strategy *, std::vector<probability_t>> &basic_table) const;

    std::vector<Result *> results(const Strategy *strategy) const override;
    std::vector<Result *> collect_results(const StrategyStateMachine *ssm) const;

    /**
     * Build all results for the SSM. This operation may only be invoked once across the lifetime of the SSM.
     * Ownership of the results is transferred to the strategies, as such deleting a strategy will delete the associated
     * results. As such, the resulting vector should not be retained.
     * @param ssm
     * @param elements
     * @return vec of results, where result[i].result_id() == i
     */
    std::vector<Result *> build_results(StrategyStateMachine &ssm, const std::vector<SchedElt *> &elements) override;

    ACCESSORS_RW(FaultToleranceStrategyManager, FaultToleranceScheme *, ft_protected);
    ACCESSORS_RW(FaultToleranceStrategyManager, FaultToleranceScheme *, ft_unprotected);
    ACCESSORS_RW(FaultToleranceStrategyManager, double, energy);

#ifdef EIGEN3
    ft_ss_t compute_dtmc_rate(const StrategyStateMachine *ssm) const;
#endif
};
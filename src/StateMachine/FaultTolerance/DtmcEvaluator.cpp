/*
 * Copyright (C) 2022 Lukas Miedema <l.miedema@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef EIGEN3

#include "DtmcEvaluator.hpp"
#include <eigen3/Eigen/Dense>

ss_dist_t DtmcEvaluator::evaluate_dtmc(std::vector<uint32_t> &mapping) {
    using std::cout;
    using std::endl;
    using namespace Eigen;

    uint32_t size = this->_strategies.size();
    MatrixXd p(size, size);

    // Populate in transition matrix
    p.setZero();
    for (uint32_t from = 0; from < this->_strategies.size(); from++) {
        for (const FaultToleranceResultPtr &result: this->_strategies[from]->results() | boost::adaptors::map_values) {
            uint32_t to = mapping[result->result_id()];
            double flow = result->p().operator double();
            p(from, to) += flow;
        }
    }

//    for (uint32_t i = 0; i < size; i++) {
//        for (uint32_t j = 0; j < size; j++) {
//            std::cout << "[" << i << "," << j << "] = " << p(i, j) << std::endl;
//        }
//    }
//    std::cout << std::endl;

    // TODO: this code is sus
    MatrixXd q = p - MatrixXd::Identity(size, size);
    VectorXd ones = VectorXd::Ones(size);
    q.conservativeResize(NoChange, size + 1);
    q.col(size) = ones;

    MatrixXd QTQ = q * q.transpose();
    VectorXd bQT = VectorXd::Ones(size);
    VectorXd ss = FullPivLU<MatrixXd>(QTQ).solve(bQT);

//    for (uint32_t i = 0; i < q.rows(); i++) {
//        for (uint32_t j = 0; j < q.cols(); j++) {
//            cout << q(i,j) << "|";
//        }
//        cout << endl;
//    }
//    std::cout << ss << std::endl;

    ss_dist_t result = {
        .fault_rate = 0,
    };

    for (uint32_t from = 0; from < size; from++) {
        result.ss.push_back(ss(from));
        if (ss(from) > 0) { // optimalization, we'll multiply by this
            auto *strategy = this->_strategies[from];

            log_double_t delta = 0; // for the strategy
            for (const FaultToleranceResultPtr &result: strategy->results() | boost::adaptors::map_values) {
                auto *strategyNext = this->_strategies[mapping[result->result_id()]];
                auto &strategyTable = this->_success_probabilities[strategyNext];
                auto &resultTable = result->probability_table(); // FIXME move this to the FT manager or something
                assert(strategyTable.size() == resultTable.size());

                log_double_t deltaTransition = compute_transition_cost(strategyTable, resultTable);

                delta += deltaTransition * log_double_t(result->p());
//                std::cout << " from s" << strategy->id() << " to r" << result->result_id() << " = " << deltaTransition << std::endl;
            }
//            std::cout << strategy->id() << " delta=" << delta << " ss=" << ss(from) << " togther=" << (delta * ss(from)) << std::endl;
            result.fault_rate += delta * std::clamp(ss(from), 0.0, 1.0); // clamp against fp errors
        }
    }
    // cout << "Fault rate (log): " << faultRate.data() << endl;
    return result; // E[# catastrophic faults] per steady-state iteration of the DAG
}

log_double_t DtmcEvaluator::compute_transition_cost(const std::vector<probability_t> &strategyTable, const std::vector<probability_t> &resultTable) {
    log_double_t deltaTransition = 0;
    for (uint32_t i = 0; i < strategyTable.size(); i++) {
        // For each elt, catastrophic failure occurs when it both fails in the result (nth iteration)
        // and in the next strategy (n+1th iteration) = 2 consecutive failures
        probability_t catastrophicFailure = (1 - strategyTable[i]) * (1 - resultTable[i]);

        // Total delta is either this one fails, or this one succeeds and another fails
        deltaTransition += catastrophicFailure;
    }
    return deltaTransition;
}

#endif

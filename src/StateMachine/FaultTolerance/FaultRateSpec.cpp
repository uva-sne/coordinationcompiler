/*
 * Copyright (C) 2022 Lukas Miedema <l.miedema@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <boost/algorithm/string/split.hpp>
#include <vector>
#include <boost/algorithm/string/classification.hpp>
#include <config.hpp>
#include "FaultRateSpec.hpp"

FaultRateSpec::FaultRateSpec(const std::string& raw, std::string systemTimeUnit) : time_unit(std::move(systemTimeUnit)) {
    std::vector<std::string> parts;
    boost::split(parts, raw, boost::is_any_of(":"));
    if (parts.size() != 2 || !boost::all(parts[0], boost::is_digit()) || !boost::all(parts[1], boost::is_digit()))
        throw CompilerException("fault-rate-spec", raw + " is not a valid fractional fault rate specification");
    this->_numerator = std::stoll(parts[0]);
    this->_denominator = Utils::stringtime_to_time(parts[1] + "s", this->time_unit);
}

std::string FaultRateSpec::to_string() const {
    auto denominatorS = Utils::stringtime_to_time(std::to_string(_denominator) + this->time_unit, "s");
    return std::to_string(_numerator) + ":" + std::to_string(denominatorS);
}

double FaultRateSpec::to_fraction() const {
    return (double) (((long double) _numerator) / ((long double) _denominator));
}

double DvfsFaultRateSpec::to_fraction(frequencyinfo_t f) {
    if (_f_min == _f_max)
        return FaultRateSpec::to_fraction();
    double fnorm = ((double) f) / ((double) _f_max);
    double fmin = ((double) _f_min) / ((double) _f_max);
    return FaultRateSpec::to_fraction() * std::pow(10, (1 - fnorm) / (1 - fmin));
}

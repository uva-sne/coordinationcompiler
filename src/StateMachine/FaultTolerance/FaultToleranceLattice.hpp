/*
 * Copyright (C) 2022 Lukas Miedema <l.miedema@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include "Helper_macros.hpp"
#include "FaultToleranceStrategy.hpp"

enum latticestate_t {
    STATE_UNKNOWN, //!< Strategy is unvisited
    STATE_UNSCHEDULABLE, //!< Strategy is not schedulable -> must be removed
    STATE_SCHEDULABLE, //!< Strategy is schedulable -> can be retained
    STATE_SCHEDULABLE_BOUNDARY, // !< Is schedulable and has at least one unschedulable parent (or no parents)
};

struct latticenode_t {
    latticestate_t state;
    FaultToleranceStrategyPtr strategy; // double pointer to allow std::move-ing it out of here
};

typedef std::function<void (latticenode_t &)> strategyvisitor_t;

template<class T> struct ptr_compare {
    bool operator()(const T* lhs, const T* rhs) const {
        return *lhs < *rhs;
    }
};

class FaultToleranceLattice {
private:
    std::map<const std::vector<bool> *, latticenode_t, ptr_compare<std::vector<bool>>> _strategies; // !< key points to within the Strategy
    uint32_t _element_count; // !< number of elements protected by each strategy (must be the same for all strategies)
public:
    /**
     * Construct a new lattice
     * @param element_count number of elements each strategy protects.
     */
    FaultToleranceLattice(uint32_t element_count);
    explicit FaultToleranceLattice(FaultToleranceLattice &) = delete;

    void visit_parents_recursive(FaultToleranceStrategy *, strategyvisitor_t &);
    void visit_parents(FaultToleranceStrategy *, strategyvisitor_t &);
    void visit_children_recursive(FaultToleranceStrategy *, strategyvisitor_t &);
    void visit_children(FaultToleranceStrategy *, strategyvisitor_t &);
    void visit_all(strategyvisitor_t &);

    /**
     * Top element in the lattice. Can be null, as it just looks for the strategy that protects everything.
     * @return
     */
    latticenode_t *top();

    /**
     * Bottom element in the lattice. Can be null, as it just looks for the strategy that protects nothing.
     * @return
     */
    latticenode_t *bottom();

    /**
     * Add a strategy and transfer ownership.
     * All strategies (the full bitset) must be added before any visitor is called on this container.
     * @param strategy
     */
    void add(FaultToleranceStrategyPtr strategy);

    ACCESSORS_R(FaultToleranceLattice, SINGLE_ARG(std::map<const std::vector<bool> *, latticenode_t, ptr_compare<std::vector<bool>>>)&, strategies);
};

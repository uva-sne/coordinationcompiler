/*
 * Copyright (C) 2022 Lukas Miedema <l.miedema@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <memory>
#include <boost/math/special_functions/binomial.hpp>
#include "NMR.hpp"

using probability::probability_t;

template<uint32_t replicas>
NMR<replicas>::NMR() {
    this->_voter_task = std::make_unique<Task>(to_string(replicas) + "mr-voter-task");
    this->_voter_task->C(0); // unrealistic fast voting, make configurable?
}

template<uint32_t replicas>
void NMR<replicas>::apply(SchedElt *element, std::map<std::string, SchedEltPtr> *result,
        ft_state_t *state, mapping_state_t *mapping) const {
    assert(element->type() != SchedElt::PACKET);
    assert(element->element_id() == 0);

    // TMR: create voter task
    SchedJobPtr voter = replicas > 1 ? std::make_unique<SchedJob>(this->_voter_task.get(), 0, state->voter_element_count++) : nullptr;

    // All original predecessors must connect to all replicas
    std::vector<SchedElt *> predecessorTargets;

    // NMR: add N copies of the task + a voter task
    std::array<SchedEltPtr, replicas> replicaArr;
    for (uint32_t i = 0; i < replicas; i++) {
        replicaArr[i] = element->copy(i + 1);
        predecessorTargets.push_back(replicaArr[i].get());

        // Connect tasks to voter task
        if (replicas > 1)
            replicaArr[i]->add_successor(voter.get());
    }

    std::unique_ptr<ScheduleElementRef> resolvedPredecessor = std::make_unique<ResolvedScheduleElement>(
            dynamic_cast<UnresolvedScheduleElement*>(mapping->mapping_predecessors[element].get()),
            predecessorTargets);

    // And all original successors must connect to the voter
    std::vector<SchedElt *> successorTargets;
    successorTargets.push_back(replicas == 1 ? replicaArr[0].get() : voter.get());

    std::unique_ptr<ScheduleElementRef> resolvedSuccessor = std::make_unique<ResolvedScheduleElement>(
            dynamic_cast<UnresolvedScheduleElement*>(mapping->mapping_successors[element].get()),
            successorTargets);

    mapping->mapping_predecessors[element] = std::move(resolvedPredecessor);
    mapping->mapping_successors[element] = std::move(resolvedSuccessor);

    // Add our links to other elements
    for (SchedElt *predecessor : element->previous()) {
        // we are a successor of the predecessor, so look the predecessor up in the successors map
        std::unique_ptr<ScheduleElementRef> &ref = mapping->mapping_successors[predecessor];
        if (ref == nullptr)
            ref = std::make_unique<UnresolvedScheduleElement>();
        for (SchedElt *target : predecessorTargets)
            ref->add_successor(target);
    }

    for (SchedElt *successor : element->successors()) {
        std::unique_ptr<ScheduleElementRef> &ref = mapping->mapping_predecessors[successor];
        if (ref == nullptr)
            ref = std::make_unique<UnresolvedScheduleElement>();
        for (SchedElt *target : successorTargets)
            ref->add_predecessor(target);
    }

    // Move to LUT
    for (uint32_t i = 0; i < replicas; i++)
        (*result)[replicaArr[i]->id()] = std::move(replicaArr[i]);
    if (replicas > 1)
        (*result)[voter->id()] = std::move(voter);
}

template<uint32_t replicas>
std::unique_ptr<ft_state_t> NMR<replicas>::new_state() {
    return std::make_unique<ft_state_t>();
}

template<uint32_t replicas>
std::string NMR<replicas>::describe() const {
    return std::to_string(replicas) + "MR";
}

template<uint32_t replicas>
probability_t NMR<replicas>::task_success_probability(double wcet, ft_version_info *versions) const {
    // TODO: write this using probability_t
    if (versions == nullptr) {
        double none_succeed(::pow(1.0 - ::exp(wcet * -this->_fault_rate->to_fraction()), replicas));
        double one_succeed(::pow(1.0 - ::exp(wcet * -this->_fault_rate->to_fraction()), replicas - 1) // all but one fail
                * ::exp(wcet * -this->_fault_rate->to_fraction()) // one succeeds
                * replicas); // (n choose k)
        return {1.0 - (none_succeed + one_succeed)};
    } else {
        auto *dvfsRate = dynamic_cast<DvfsFaultRateSpec* >(this->_fault_rate);
        assert(dvfsRate != nullptr);
        double none_succeed = 1;
        for (Version *version : versions->versions)
            none_succeed *= 1.0 - ::exp(version->C() * -dvfsRate->to_fraction(version->frequency()));

        double one_succeed = 0;
        for (Version *curr : versions->versions) {
            double curr_succeed = ::exp(curr->C() * -dvfsRate->to_fraction(curr->frequency()));
            for (Version *other : versions->versions)
                curr_succeed *= 1.0 - ::exp(other->C() * -dvfsRate->to_fraction(other->frequency()));;
            one_succeed += curr_succeed;
        }
        return {1.0 - (none_succeed + one_succeed)};
    }
}

//template<uint32_t replicas>
//probability::probability_t NMR<replicas>::task_detect_probability(double wcet) const {
//    return { 1.0 }; // realistic? Voting may fail. TODO base this on the execution time of the voting task
//}

template<uint32_t replicas>
bool NMR<replicas>::has_detect_capability() const {
    return true;
}

template<uint32_t replicas>
ft_version_info
NMR<replicas>::get_version_info_for(SchedEltPtr &baseElement, mapping_state_t &mapping) const {
    ft_version_info result;
    // Use predecessor map as it will contain the replicas, the successor map only contains the voter
    auto *ref = dynamic_cast<ResolvedScheduleElement *>(mapping.mapping_predecessors[baseElement.get()].get());
    assert(ref != nullptr);
    for (SchedElt *replica : ref->elements) {
        auto version = baseElement->task()->version(replica->schedtask()->selected_version());
        result.versions.push_back(version);
    }
    return result;
}

/* ------------------
 * Specializations
 * ------------------
 */
template<> std::string NMR<1>::describe() const {
    return "none";
}

template<> probability_t NMR<1>::task_success_probability(double wcet, ft_version_info *versions) const {
    // Bit questionable, doesn't have the same semantics as the NMR variant
    // There, failure must be detectable, whereas here it is not

    if (versions == nullptr) {
        return {::exp(wcet * -this->_fault_rate->to_fraction())};
    } else {
        auto *dvfsRate = dynamic_cast<DvfsFaultRateSpec *>(this->_fault_rate);
        assert(dvfsRate != nullptr);
        return { ::exp(versions->versions[0]->C() * -dvfsRate->to_fraction(versions->versions[0]->frequency()))};
    }
}

//template<> probability_t NMR<1>::task_detect_probability(double wcet) const {
//    return { 0 };
//}

template<> bool NMR<1>::has_detect_capability() const {
    return false;
}

/*
 * Copyright (C) 2022 Lukas Miedema <l.miedema@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <Schedule.hpp>
#include <SystemModel.hpp>
#include <probability.hpp>
#include <StateMachine/Strategy.hpp>
#include "FaultRateSpec.hpp"

using probability::probability_t;

class ScheduleElementRef {
public:
    virtual void add_predecessor(SchedElt *element) = 0;
    virtual void add_successor(SchedElt *element) = 0;
    virtual ~ScheduleElementRef() = default;
};
//
class UnresolvedScheduleElement : public ScheduleElementRef {
    friend class ResolvedScheduleElement;
private:
    std::vector<SchedElt *> successors;
    std::vector<SchedElt *> predecessors;
public:
    void add_predecessor(SchedElt *element) override;
    void add_successor(SchedElt *element) override;
    ~UnresolvedScheduleElement() override = default;
};

class ResolvedScheduleElement : public ScheduleElementRef {
public:
    // A single original SchedElt can map to multiple SchedElts under fault tolerance under NMR
    std::vector<SchedElt *> elements;
    explicit ResolvedScheduleElement(UnresolvedScheduleElement*, std::vector<SchedElt *>&);

    void add_predecessor(SchedElt *element) override;
    void add_successor(SchedElt *element) override;
    ~ResolvedScheduleElement() override = default;
};

class FaultToleranceScheme;
typedef std::unique_ptr<FaultToleranceScheme> FaultToleranceSchemePtr;

struct ft_state_t {
    template<uint32_t T> friend class NMR;
public:
    ft_state_t() : voter_element_count(0) {};
    ~ft_state_t() = default;

private:
    uint32_t voter_element_count;
};

struct mapping_state_t {
    std::map<const SchedElt *, std::unique_ptr<ScheduleElementRef>> mapping_predecessors;
    std::map<const SchedElt *, std::unique_ptr<ScheduleElementRef>> mapping_successors;
};

/**
 * Version info about a fault-tolerance mapping, read from the schedule
 */
struct ft_version_info {
    std::vector<Version *> versions; // version of each replica, size = no of replicas
};

/**
 * A FaultToleranceScheme is an implementation of task-level (i.e. can be applied on the task level) fault-tolerance.
 * Examples may include duplicating a task (2MR, 3MR) with voting.
 * The scheme must also provide information regarding the impact on timing of the solution.
 */
class FaultToleranceScheme {
protected:
    //! Fault rate lambda parameter P(no fault) = e^(-lambda * C), where lambda is "fault rate"
    static FaultRateSpec DEFAULT_FAULT_RATE;
    FaultRateSpec *_fault_rate = &DEFAULT_FAULT_RATE;
public:
    /**
     * Create a new state object specific to this implementation. The lifecycle of the state object is tied to the
     * generation of a single Schedule, and allows for tracking of previously-mapped SchedElts.
     * @return
     */
    virtual std::unique_ptr<ft_state_t> new_state() = 0;

    /**
     * Apply fault-tolerance to an element.
     * @param element
     * @param result LUT of SchedElt* to append to
     * @param state ptr
     * @param mappingState ptr, shared with other schemes
     * @return
     */
    virtual void apply(SchedElt *element, std::map<std::string, SchedEltPtr> *result,
            ft_state_t *state, mapping_state_t *mappingState) const = 0;

    /**
     * Human-readable name for a SchedElt under protection
     * @return
     */
    virtual std::string describe() const = 0;

    /**
     * Return the chance that the given task will not experience a fault during its run under this scheme.
     * @param wcet
     * @return
     */
    virtual probability_t task_success_probability(double wcet, ft_version_info *frequencies) const = 0;

    /**
     * Return the chance that, given the task experiences a fault, it will be detected when run under this scheme.
     * @param wcet
     * @return
     */
    //virtual probability_t task_detect_probability(double wcet) const = 0; FIXME this blows up the number of results considerably figure out what the impact is

    /**
     * Return true if the scheme can detect faults. When false is returned, task_detect_probability(double) must always
     * return 0.
     * @return
     */
    virtual bool has_detect_capability() const = 0;

    /**
     * Number of replications (only makes sense if this is a replication-based scheme).
     * @return
     */
    virtual uint32_t replication_count() const = 0;

    virtual ft_version_info get_version_info_for(SchedEltPtr &baseElement, mapping_state_t &mapping) const = 0;

    ACCESSORS_RW(FaultToleranceScheme, FaultRateSpec *, fault_rate);
};

/*!
 * \typedef SolverRegistry
 * \brief Registry containing all solvers
 */
using FaultToleranceRegistry = registry::Registry<FaultToleranceScheme, std::string>;

/*!
 * \brief Helper macro to register a new derived Fault Tolerance schemes
 */
#define REGISTER_FT(ClassName, Identifier) \
  REGISTER_SUBCLASS(FaultToleranceScheme, ClassName, std::string, Identifier)

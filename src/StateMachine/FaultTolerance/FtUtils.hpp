/*
 * Copyright (C) 2022 Lukas Miedema <l.miedema@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <vector>
#include <map>
#include <cstdint>
#include <probability.hpp>

using log_double_t = probability::log_double_t;

namespace FtUtils {
    /**
     * Interpret a bool vector as integer and increment it by one. Returns true if there is no overflow (i.e. the
     * increment op was successful)
     * @param vec
     * @return true if successful, false otherwise
     */
    bool increment(std::vector<bool> &vec);

    /**
     * Interpret a vector as a number and increment it by one. Returns true if there is no overflow (i.e. the
     * increment op was successful)
     * @param vec
     * @param limit lowest non-legal value
     * @return true if successful, false otherwise
     */
    bool increment(std::vector<uint32_t> &vec, uint32_t limit);

    /**
     * Interpret a bool vector as integer and increment it by one, but do not touch bits protected by the mask.
     * @param vec
     * @param mask
     * @return true if successful, false if overflow
     */
    bool increment_with_mask(std::vector<bool> &vec, const std::vector<bool> &mask);

    /**
     * Count the number of 1's (trues) in the bool array.
     * @param vec
     * @return
     */
    uint64_t count_ones(const std::vector<bool> &vec);
};



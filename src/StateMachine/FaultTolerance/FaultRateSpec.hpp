/*
 * Copyright (C) 2022 Lukas Miedema <l.miedema@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once
#include <string>
#include <utility>
#include "Utils.hpp"
#include "Helper_macros.hpp"

/**
 * Expresses a fractional fault-rate notation (numerator:denominator) in nanoseconds (!)
 * This is used internally in UPPAAL, so preserving this "raw" (no parsing to a double) keeps precision
 * for when it is needed there.
 *
 */
class FaultRateSpec {
private:
    timinginfos_t _numerator;
    timinginfos_t _denominator;
    std::string time_unit; //<! internal time unit

public:
    /**
     * Construct a new FaultRateSpec from a string (string is always in seconds for UPPAAL compatibility)
     * @param raw
     * @param systemTimeUnit actual system unit, for internal conversion
     */
    explicit FaultRateSpec(const std::string& raw, std::string systemTimeUnit);
    virtual ~FaultRateSpec() = default;

    /**
     * Construct a new FaultRateSpec
     * @param numerator in nanoseconds
     * @param denominator in nanoseconds
     */
    FaultRateSpec(timinginfos_t numerator, timinginfos_t denominator) : _numerator(numerator), _denominator(denominator) {}

    /**
     * Reconstruct the original string used to create the FaultRateSpec (in seconds)
     */
    std::string to_string() const;

    /**
     * Provide as a fractional double for the system time unit
     * @return
     */
    double to_fraction() const;

    ACCESSORS_RW(FaultRateSpec, timinginfos_t, numerator);
    ACCESSORS_RW(FaultRateSpec, timinginfos_t, denominator);
};

/**
 * DVFS-dependent fault-rate spec. Extends a regular FaultRateSpec which is used as lambda_0.
 */
class DvfsFaultRateSpec : public FaultRateSpec {
private:
    frequencyinfo_t _f_min;
    frequencyinfo_t _f_max;

public:
    DvfsFaultRateSpec(const std::string& raw, std::string systemTimeUnit, frequencyinfo_t fMin, frequencyinfo_t fMax) :
        FaultRateSpec(raw, std::move(systemTimeUnit)), _f_min(fMin), _f_max(fMax) {};

    ~DvfsFaultRateSpec() override = default;

    double to_fraction(frequencyinfo_t f);

    ACCESSORS_RW(DvfsFaultRateSpec, frequencyinfo_t, f_min);
    ACCESSORS_RW(DvfsFaultRateSpec, frequencyinfo_t, f_max);
};
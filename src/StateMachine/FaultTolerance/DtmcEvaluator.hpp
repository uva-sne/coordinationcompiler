/*
 * Copyright (C) 2022 Lukas Miedema <l.miedema@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once
#ifdef EIGEN3

#include <vector>
#include <map>
#include "probability.hpp"
#include "FaultToleranceStrategy.hpp"
#include "FtUtils.hpp"

/**
 * Output of the steady state distribution calculation of DTMC
 */
struct ss_dist_t {
    log_double_t fault_rate;
    std::vector<double> ss; // P(strategy[i]) = ss[i]
};

/**
 * Utility to evaluate the steady-state performance of an FT SSM.
 */
class DtmcEvaluator {
private:
    const std::vector<FaultToleranceStrategy *> &_strategies;
    std::map<Strategy *, std::vector<probability_t>> &_success_probabilities;

public:
    DtmcEvaluator(const std::vector<FaultToleranceStrategy *> &strategies,
            std::map<Strategy *, std::vector<probability_t>> &successProbabilities) : _strategies(strategies),
            _success_probabilities(successProbabilities) {};

    /**
     * Evaluate using Discrete-Time Markov Chain steady-state computation.
     * @param successorMapping
     * @param strategies
     * @param successProbabilities
     * @return
     */
    ss_dist_t evaluate_dtmc(std::vector<uint32_t> &mapping);

    static log_double_t compute_transition_cost(const std::vector<probability_t> &strategyTable, const std::vector<probability_t> &resultTable) ;
};

#endif

/*
 * Copyright (C) 2022 Lukas Miedema <l.miedema@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "FaultToleranceStrategy.hpp"
#include "StateMachine/StrategyStateMachine.hpp"
#include "FtUtils.hpp"
#include "DtmcEvaluator.hpp"

/*----
 * FaultToleranceResult
 *----
 */
Strategy *FaultToleranceResult::successor(Strategy *owner, StrategyStateMachine &ssm) {
    return this->_successor;
}

const std::string FaultToleranceResult::id() const {
    return std::to_string(this->_result_id);
}

/*----
 * FaultToleranceStrategy
 *----
 */
std::map<std::string, SchedEltPtr> FaultToleranceStrategy::export_to_schedule(
        std::vector<SchedEltPtr> &elements,
        FaultToleranceScheme &ftProtected, FaultToleranceScheme &ftUnprotected) const {

    std::map<std::string, SchedEltPtr> result;
    std::unique_ptr<ft_state_t> protectedState = ftProtected.new_state();
    std::unique_ptr<ft_state_t> unprotectedState = ftProtected.new_state();
    mapping_state_t mapping;
    for (size_t i = 0; i < elements.size(); i++) {
        if (i < this->_protected_elements.size() && this->_protected_elements[i]) {
            ftProtected.apply(elements[i].get(), &result, protectedState.get(), &mapping);
        } else {
            ftUnprotected.apply(elements[i].get(), &result, unprotectedState.get(), &mapping);
        }
    }
    return result;
}

std::map<std::string, SchedEltPtr> FaultToleranceStrategy::export_to_schedule(
        std::vector<SchedEltPtr> &elements,
        StrategyManager &manager) const {
    auto &ftManager = dynamic_cast<FaultToleranceStrategyManager &>(manager);
    return this->export_to_schedule(elements, *ftManager.ft_protected(), *ftManager.ft_unprotected());
}

schedulability_result_t FaultToleranceStrategy::is_schedulable(
        SystemModel &tg,
        config_t &config,
        StrategyManager &manager,
        std::vector<SchedEltPtr> &baseElements,
        Schedule &schedule) const {

    assert(!schedule.is_built()); // must be removed by the caller
    assert(!baseElements.empty());
    assert(tg.has_global_deadline());

    this->schedule(tg, config, manager, baseElements, schedule);
    schedulability_result_t result = {
            .is_schedulable = schedule.makespan() <= tg.global_deadline() && schedule.status() == ScheduleProperties::SCHED_COMPLETE,
            .makespan = schedule.makespan()
    };

    schedule.clear_assigned();
    schedule.elements_lut().clear();
    return result;
}

std::string FaultToleranceStrategy::get_role_for_elt(uint64_t i, const StrategyManager &sm) {
    auto &ftsm = dynamic_cast<const FaultToleranceStrategyManager &>(sm);
    if (this->_protected_elements[i])
        return ftsm.ft_protected()->describe();
    else
        return ftsm.ft_unprotected()->describe();
}

bool FaultToleranceStrategy::has_results() {
    return !this->_results.empty();
}

/*----
 * EnergyFaultToleranceStrategy
 *----
 */
energy_schedulability_result_t EnergyFaultToleranceStrategy::is_schedulable_energy(
        SystemModel &tg,
        config_t &config,
        StrategyManager &manager,
        std::vector<SchedEltPtr> &baseElements,
        Schedule &schedule) const {

    assert(!schedule.is_built()); // must be removed by the caller
    assert(!baseElements.empty());
    assert(tg.has_global_deadline());

    // Apply conversion
    auto &ftManager = dynamic_cast<FaultToleranceStrategyManager &>(manager);
    std::map<std::string, SchedEltPtr> result;
    std::unique_ptr<ft_state_t> protectedState = ftManager.ft_protected()->new_state();
    std::unique_ptr<ft_state_t> unprotectedState = ftManager.ft_unprotected()->new_state();
    mapping_state_t mapping;
    for (size_t i = 0; i < baseElements.size(); i++) {
        if (i < this->_protected_elements.size() && this->_protected_elements[i]) {
            ftManager.ft_protected()->apply(baseElements[i].get(), &result, protectedState.get(), &mapping);
        } else {
            ftManager.ft_unprotected()->apply(baseElements[i].get(), &result, unprotectedState.get(), &mapping);
        }
    }
    schedule.elements_lut() = std::move(result);
    manager.solver()->run(&tg, &config, &schedule);

    // Use conversion to track back eneryg
    energy_schedulability_result_t output = {
            .is_schedulable = schedule.makespan() <= tg.global_deadline() && schedule.status() == ScheduleProperties::SCHED_COMPLETE,
            .makespan = schedule.makespan(),
            .energy = schedule.energy()
    };

    for (size_t i = 0; i < baseElements.size(); i++) {
        auto *scheme = this->_protected_elements[i] ? ftManager.ft_protected() : ftManager.ft_unprotected();
        output.versions.emplace_back(scheme->get_version_info_for(baseElements[i], mapping));
    }

    schedule.clear_assigned();
    schedule.elements_lut().clear();
    return output;
}


/*----
 * FaultToleranceStrategyManager
 *----
 */
std::map<Strategy *, std::vector<probability_t>> FaultToleranceStrategyManager::compute_basic_ft_table(const StrategyStateMachine *ssm) const {
    auto elements = ssm->schedule()->elements_unowned();
    std::map<Strategy *, std::vector<probability_t>> table;
    for (const StrategyPtr &strategy : ssm->strategies()) {
        auto ftStrategy = dynamic_cast<FaultToleranceStrategy *>(strategy.get());
        assert(ftStrategy != nullptr);
        assert(ftStrategy->protected_elements().empty() || ftStrategy->protected_elements().size() == elements.size());

        std::vector<ft_version_info> *allVersionInfo = nullptr;
        auto eStrategy = dynamic_cast<EnergyFaultToleranceStrategy *>(ftStrategy);
        if (eStrategy != nullptr)
            allVersionInfo = &eStrategy->versions();

        std::vector<probability_t> values;
        values.reserve(elements.size());

        for (uint64_t i = 0; i < elements.size(); i++) {
            ft_version_info *versionInfo = allVersionInfo == nullptr ? nullptr : &allVersionInfo->at(i);
            probability_t p = ftStrategy->protected_elements()[i] ?
                    this->_ft_protected->task_success_probability((double) elements[i]->wct(), versionInfo) :
                    this->_ft_unprotected->task_success_probability((double) elements[i]->wct(), versionInfo);
            values.push_back(p);
        }
        table[strategy.get()] = std::move(values);
    }
    return table;
}

std::map<Strategy *, std::vector<probability_t>> FaultToleranceStrategyManager::compute_relational_ft_table(
        const StrategyStateMachine *ssm, const std::map<Strategy *, std::vector<probability_t>> &basic_table) const {
    auto elements = ssm->schedule()->elements_unowned();

    // Compute transitive closure using BFS
    // O(n^2) memory complexity :/
    // TODO: make this map available for other operations needing it or add some visitor pattern for this
    std::map<const SchedElt *, std::set<const SchedElt *>> closureMap;
    auto queue = std::queue<const SchedElt *>();

    for (const SchedElt *element : elements) {
        if (element->previous().empty())
            queue.push(element);
    }

    // BFS-explore the graph
    while (!queue.empty()) {
        const SchedElt *head = queue.front();
        queue.pop();

        std::set<const SchedElt *> closure;
        for (const SchedElt *predecessor : head->previous()) {
            auto &predecessorClosure = closureMap[predecessor];
            closure.insert(predecessor);
            closure.insert(predecessorClosure.begin(),  predecessorClosure.end());
        }
        closureMap[head] = std::move(closure);

        for (const SchedElt *successor : head->successors()) {
            // only add if all predecessors (heads siblings) have been visited (same logic as FLSSorting, works because DAG)
            bool allVisited = true;
            for (const SchedElt *sibling : successor->previous())
                if (!closureMap.count(sibling))
                    allVisited = false;
            if (allVisited)
                queue.push(successor);
        }
    }

    std::map<Strategy *, std::vector<probability_t>> table;
    for (const StrategyPtr &strategy : ssm->strategies()) {
        const std::vector<probability_t> &basicRow = basic_table.at(strategy.get());
        std::map<const SchedElt *, probability_t> basicMap;
        for (uint64_t i = 0; i < elements.size(); i++)
            basicMap[elements[i]] = basicRow[i];

        std::vector<probability_t> relationalRow;
        relationalRow.reserve(elements.size());

        for (uint64_t i = 0; i < elements.size(); i++) {
            const SchedElt *element = elements[i];
            probability_t p = basicRow[i];
            std::set<const SchedElt *> &closure = closureMap[element];
            for (const SchedElt *pred : closure)
                p *= basicMap[pred];
            relationalRow.push_back(p);
        }
        table[strategy.get()] = std::move(relationalRow);
    }
    return table;
}

std::vector<Result *> FaultToleranceStrategyManager::build_results(StrategyStateMachine &ssm, const std::vector<SchedElt *> &elements) {
    uint32_t resultCounter = 0;
    std::vector<Result *> allResults;

    for (auto &strategy : ssm.strategies()) {
        auto ftStrategy = dynamic_cast<FaultToleranceStrategy *>(strategy.get());
        assert(ftStrategy != nullptr);
        assert(ftStrategy->_protected_elements.size() == elements.size());
        assert(!ftStrategy->has_results());

        std::vector<ft_version_info> *allVersionInfo = nullptr;
        auto eStrategy = dynamic_cast<EnergyFaultToleranceStrategy *>(ftStrategy);
        if (eStrategy != nullptr)
            allVersionInfo = &eStrategy->versions();

        auto &protectedElements = ftStrategy->_protected_elements;
        std::map<std::vector<bool> *, FaultToleranceResultPtr> &results = ftStrategy->_results;
        // FIXME integrate detection probability here, this code now assumes a detection P of 1
        /*
         * Two modes for making strategies
         *  1. with unprotected task information
         *  2. without unprotected task information
         *
         * Case 1 provides 2^n results, where n is the number of elements
         * Case 2 provides 2^p results, where p is the number of protected tasks by the strategy
         *
         * Case 1 is selected if the "ft_unprotected" technique provides fault detection, otherwise case 2 applies.
         */
        uint64_t protectedCount = this->_ft_unprotected->has_detect_capability() ? protectedElements.size() : FtUtils::count_ones(protectedElements);
        std::vector<bool> counter(protectedCount, false);
        do {
            probability_t p = 1;
            std::vector<probability_t> successTable;
            std::vector<probability_t> locationAwareSuccessTable;
            successTable.reserve(protectedElements.size());
            locationAwareSuccessTable.reserve(protectedElements.size());

            // Create direct FT result table
            uint64_t counterIndex = 0;
            std::map<SchedElt *, uint64_t> reverseIndexMap; // maps elements[i] -> i
            for (uint64_t i = 0; i < elements.size(); i++) {
                FaultToleranceScheme *ft = protectedElements[i] ? this->_ft_protected : this->_ft_unprotected;
                auto wcet = (double) elements[i]->wct();
                ft_version_info *versionInfo = allVersionInfo == nullptr ? nullptr : &allVersionInfo->at(i);
                probability_t successP = ft->task_success_probability(wcet, versionInfo);
                probability_t guaranteedFail = 0.0;
                probability_t guaranteedSuccess = 1.0;
                reverseIndexMap[elements[i]] = i;

                if (this->_ft_unprotected->has_detect_capability() || protectedElements[i]) {
                    if (counter[counterIndex]) {
                        // this is the result for when the task succeeds
                        p *= successP;
                        successTable.push_back(guaranteedSuccess);
                    } else {
                        // this is the result for when the task fails
                        p *= (1 - successP);
                        successTable.push_back(guaranteedFail);
                    }
                    counterIndex++;
                } else {
                    // No information known about it
                    successTable.push_back(successP);
                }
            }

            // Create FT location-aware table
            for (SchedElt *element : elements) {
                std::vector<SchedElt *> queue;
                std::set<SchedElt *> predecessors; // includes "element"
                queue.push_back(element);

                while (!queue.empty()) {
                    SchedElt *back = queue.back();
                    predecessors.insert(back);
                    queue.pop_back();

                    // Add predecessors
                    for (SchedElt *predecessor: back->previous())
                        if (!predecessors.count(predecessor))
                            queue.push_back(predecessor);
                }

                probability_t probability = 1;
                for (SchedElt *predecessor: predecessors)
                    probability *= successTable[reverseIndexMap[predecessor]];
                locationAwareSuccessTable.push_back(probability);
            }

            auto result = std::unique_ptr<FaultToleranceResult>(new FaultToleranceResult(resultCounter++, p, counter, std::move(locationAwareSuccessTable)));
            allResults.push_back((results[&result->_result] = std::move(result)).get());
        } while (FtUtils::increment(counter));
    }
    return allResults;
}

std::vector<Result *> FaultToleranceStrategyManager::results(const Strategy *strategy) const {
    auto const ftStrategy = dynamic_cast<const FaultToleranceStrategy *>(strategy);
    assert(ftStrategy != nullptr);
    if (ftStrategy->_results.empty())
        return {};

    std::vector<Result *> result; // maybe don't re-alloc this every time (?)
    for (auto &resultPtr : ftStrategy->_results | boost::adaptors::map_values)
        result.push_back(resultPtr.get());
    return result;
}

std::vector<Result *> FaultToleranceStrategyManager::collect_results(const StrategyStateMachine *ssm) const {
    std::vector<Result *> results;
    for (auto const &strategy : ssm->strategies()) {
        auto strategyResults = this->results(strategy.get());
        results.insert(results.end(), strategyResults.begin(),  strategyResults.end());
    }
    return results;
}

#ifdef EIGEN3
#include "DtmcEvaluator.hpp"
ft_ss_t FaultToleranceStrategyManager::compute_dtmc_rate(const StrategyStateMachine *ssm) const {
    std::map<Strategy *, std::vector<probability_t>> relationalFtTable = this->compute_relational_ft_table(ssm, this->compute_basic_ft_table(ssm));
    auto strategies = ssm->strategies_unowned<FaultToleranceStrategy>();
    auto results = this->collect_results(ssm);

    double energy = 0;
    bool hasEnergy = true;

    // Convert result mapping to vector
    std::map<FaultToleranceStrategy *, uint32_t> indexMap;
    for (size_t i = 0; i < strategies.size(); i++)
        indexMap[strategies[i]] = (uint32_t) i;
    std::vector<uint32_t> mapping(results.size(), 0);

    for (size_t i = 0; i < results.size(); i++)
        mapping[i] = indexMap[dynamic_cast<FaultToleranceResult *>(results[i])->successor()];

    DtmcEvaluator evaluator(strategies, relationalFtTable);
    ss_dist_t dist = evaluator.evaluate_dtmc(mapping);

    for (size_t i = 0; i < strategies.size(); i++) {
        hasEnergy &= strategies[i]->has_energy();
        energy += strategies[i]->energy() * ((double) dist.ss[i]);
    }

    return {
        .fault_rate = dist.fault_rate,
        .energy = hasEnergy ? energy : std::optional<double>(),
        .ss = std::move(dist.ss)
    };
}

#endif

/*
 * Copyright (C) 2022 Lukas Miedema <l.miedema@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <vector>
#include "boost/variant.hpp"
#include <Schedule.hpp>
#include "FaultToleranceScheme.hpp"
#include "probability.hpp"

template<uint32_t replicas>
class NMR : public FaultToleranceScheme {
private:
    std::unique_ptr<Task> _voter_task;
public:
    explicit NMR();

    std::unique_ptr<ft_state_t> new_state() override;

    void apply(SchedElt *element, std::map<std::string, SchedEltPtr> *result, ft_state_t *state, mapping_state_t *mapping) const override;

    std::string describe() const override;

    probability::probability_t task_success_probability(double wcet, ft_version_info *versions) const override;

    // probability::probability_t task_detect_probability(double wcet) const override;
    bool has_detect_capability() const override;

    uint32_t replication_count() const override {
        return replicas;
    }

    ft_version_info get_version_info_for(SchedEltPtr &baseElement, mapping_state_t &mapping) const override;
};

// Explicit instantiations for registry and also needed to allow template functions defined in NMR.cpp
using NullFaultTolerance = NMR<1>;
using NMR_2 = NMR<2>;
using NMR_3 = NMR<3>;
using NMR_4 = NMR<4>;
using NMR_5 = NMR<5>;
using NMR_100 = NMR<100>;

REGISTER_FT(NullFaultTolerance, "none");
REGISTER_FT(NMR_2, "2MR");
REGISTER_FT(NMR_3, "3MR");
REGISTER_FT(NMR_4, "4MR");
REGISTER_FT(NMR_5, "5MR");
REGISTER_FT(NMR_100, "100MR");

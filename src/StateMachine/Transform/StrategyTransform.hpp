/*
 * Copyright (C) 2022 Lukas Miedema <l.miedema@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <StateMachine/StateMachinePass.hpp>
#include <vector>
#include <Schedule.hpp>

template<typename SM = StrategyManager>
class StrategyTransform : public StateMachinePass<SM> {
protected:
    //! IR of the current application
    SystemModel *tg = nullptr;
    //! the global configuration
    config_t *conf = nullptr;

    void forward_params(const std::map<std::string, std::string> &map) override = 0;

public:
    //! Constructor
    explicit StrategyTransform() = default;

    //! Generate the state machine
    virtual void transform(StrategyStateMachine *ssm, SM *manager) = 0;

    //! \copydoc CompilerPass::run
    void run(SystemModel *m, config_t *c, StrategyStateMachine *ssm, SM *manager) override {
        tg = m;
        conf = c;
        this->transform(ssm, manager);
    }
    using StateMachinePass<SM>::run;
    using registry_t = registry::Registry<StrategyTransform<SM>, std::string>;
};

#define REGISTER_SM_TRANSFORM(ClassName, SMType, Identifier) \
    REGISTER_SM_SUBCLASS(StrategyTransform, ClassName, SMType, Identifier)

/*
 * Copyright (C) 2022 Lukas Miedema <l.miedema@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "PruneRedundantFtTransform.hpp"
#include "StateMachine/StrategyStateMachine.hpp"
#include "StateMachine/FaultTolerance/FaultToleranceStrategy.hpp"

IMPLEMENT_HELP(PruneRedundantFtTransform,
Remove redundant fault-tolerance strategies. A strategy is redundant if a strategy that protects a superset of\n
it also exists, e.g. if s_A protects task A and s_A,B protects task A and B, s_A is removed.
);

void PruneRedundantFtTransform::transform(StrategyStateMachine *ssm, FaultToleranceStrategyManager *manager) {
    const auto table = manager->compute_relational_ft_table(ssm, manager->compute_basic_ft_table(ssm));

    // Take ownership of all strategies
    std::vector<StrategyPtr> strategies = std::move(ssm->strategies());

    // TODO: there may be some clever trickery that can be done w/ the lattice relation
    // But for now, simply do the O(n^2) thing...

    size_t originalCount = strategies.size();
    for (StrategyPtr &outer : strategies) {
        if (outer == nullptr)
            continue; // already removed, this code sets the smartptr to null
        const std::vector<probability_t> &outerP = table.at(outer.get());
        for (StrategyPtr &inner : strategies) {
            if (inner == nullptr || inner.get() == outer.get())
                continue;
            const std::vector<probability_t> &innerP = table.at(inner.get());
            assert(innerP.size() == outerP.size());

            bool allLeq = true;
            for (uint64_t i = 0; i < innerP.size() && allLeq; i++)
                allLeq &= innerP[i] <= outerP[i];
            if (allLeq) {
                // inner is equal or worse in every way to outer -> remove
                inner = nullptr;
            }
        }
    }

    // Transfer all back that were retained
    for (StrategyPtr &strategy : strategies) {
        if (strategy != nullptr)
            ssm->strategies().push_back(std::move(strategy));
    }

    Utils::INFO("Pruned " + to_string(originalCount - ssm->strategies().size()) + " strategies (from " + to_string(originalCount) + " to " +
        to_string(ssm->strategies().size()) + " strategies)");
}
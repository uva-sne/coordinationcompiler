/*
 * Copyright (C) 2022 Lukas Miedema <l.miedema@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <StateMachine/FaultTolerance/FtUtils.hpp>
#include <StateMachine/FaultTolerance/FaultRateSpec.hpp>
#include <boost/date_time/posix_time/posix_time_types.hpp>
#include <Solvers/eFFLS/eFFLS.hpp>
#include "StateMachine/FaultTolerance/FaultToleranceStrategy.hpp"
#include "StateMachine/FaultTolerance/FaultToleranceLattice.hpp"
#include "EnergyBoostTransform.hpp"
#include "StateMachine/StrategyStateMachine.hpp"

IMPLEMENT_HELP(EnergyBoostTransform, TODO
)

void EnergyBoostTransform::transform(StrategyStateMachine *ssm, FaultToleranceStrategyManager *manager) {
    auto steadyState = manager->compute_dtmc_rate(ssm);

    // Move the elements out of the schedule
    std::vector<SchedEltPtr> elements = ssm->get_schedule_elements_exclusive();
    double targetEnergy = this->_energy_budget;
    double actualEnergy = steadyState.energy.value();
    double slack = targetEnergy - actualEnergy;

    Utils::INFO("Target energy budget: " + std::to_string(targetEnergy) + ", actual energy budget: " + std::to_string(actualEnergy) + ". Slack: " + std::to_string(slack));
    if (slack <= 0) {
        Utils::INFO(" - Slack is negative. Skipping");
        ssm->set_schedule_elements(std::move(elements));
        return;
    }

    // Now comes the real work
    Schedule *schedule = ssm->schedule();

    uint64_t performedEvaluations = 0;
    uint64_t lastPrintPerformedEvaluations = 0;
    uint64_t stateSpace = ssm->strategies().size();
    auto lastPrint = std::chrono::system_clock::now();
    auto interval = std::chrono::seconds(1);

    for (StrategyPtr &raw : ssm->strategies()) {
        auto strategy = dynamic_cast<EnergyFaultToleranceStrategy *>(raw.get());

        // Give the scheduler a new criterion, with the calculated slack
        auto criteriaAwareScheduler = dynamic_cast<CriteriaAware *>(manager->solver());
        if (criteriaAwareScheduler == nullptr)
            throw CompilerException("energy-opportunistic-boost-ft", "Requires a criteria-aware scheduler to reschedule for makespan, and " + manager->solver()->get_uniqid_rtti());
        criteriaAwareScheduler->criteria(std::make_unique<OptimizationTarget::Deadline>(this->tg->global_deadline(), strategy->energy() + slack));

        // Do the rest as usual
        energy_schedulability_result_t result = strategy->is_schedulable_energy(
                *this->tg,
                *this->conf,
                *manager,
                elements,
                *schedule);

        if (!result.is_schedulable)
            throw CompilerException("energy-opportunistic-boost-ft", "Existing strategy suddenly unschedulable after boosting energy budget");

        Utils::INFO(" - Strategy " + std::to_string(performedEvaluations) + " energy changed from " + std::to_string(strategy->energy()) + " to " + std::to_string(result.energy));

        strategy->energy(result.energy);
        strategy->makespan(result.makespan);
        strategy->versions() = std::move(result.versions);

        auto now = std::chrono::system_clock::now();
        if (now - lastPrint > interval) {
            lastPrint = now;
            Utils::INFO("Rescheduling " + std::to_string(performedEvaluations * 100.0 / stateSpace)
                    + "% (" + std::to_string(performedEvaluations) + " / " + std::to_string(stateSpace) + ") at "
                    + std::to_string(performedEvaluations - lastPrintPerformedEvaluations) + " scheduling ops/second");
            lastPrintPerformedEvaluations = performedEvaluations;
        }

        performedEvaluations++;
    }

    // Transfer the elements back to the state machine
    ssm->set_schedule_elements(std::move(elements));
}

void EnergyBoostTransform::forward_params(const std::map<std::string, std::string> &map) {
    if (!map.count("energy-budget"))
        throw CompilerException("energy-opportunistic-boost-transform", "Must specify steady-state energy budget");
    this->_energy_budget = std::stod(map.find("energy-budget")->second);
}

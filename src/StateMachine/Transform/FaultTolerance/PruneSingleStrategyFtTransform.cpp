/*
 * Copyright (C) 2022 Lukas Miedema <l.miedema@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "PruneSingleStrategyFtTransform.hpp"
#include "StateMachine/StrategyStateMachine.hpp"
#include "StateMachine/FaultTolerance/FaultToleranceStrategy.hpp"

IMPLEMENT_HELP(PruneSingleStrategyFtTransform,
Retain only one strategy, the one that provides the best fault-tolerance characteristics without switching. This is\n
useful for creating comparisons between implementations.
);

void PruneSingleStrategyFtTransform::transform(StrategyStateMachine *ssm, FaultToleranceStrategyManager *ftManager) {
    const auto table = ftManager->compute_relational_ft_table(ssm, ftManager->compute_basic_ft_table(ssm));

    // Take ownership of all strategies
    std::vector<StrategyPtr> strategies = std::move(ssm->strategies());

    // Find the best strategy
    size_t originalCount = strategies.size();
    size_t bestIndex = 0;

    double bestFaultRate = std::numeric_limits<double>::infinity();
    for (size_t i = 0; i < strategies.size(); i++) {
        long double strategyFaultRate = 0;
        for (auto p: table.at(strategies[i].get()))
            strategyFaultRate += pow(p.operator double(), 2);
        if (strategyFaultRate < bestFaultRate) {
            bestFaultRate = strategyFaultRate;
            bestIndex = i;
        }
    }

    // Transfer thes best one back
    ssm->strategies().push_back(std::move(strategies[bestIndex]));

    Utils::INFO("Pruned " + to_string(originalCount - ssm->strategies().size()) + " strategies (from " + to_string(originalCount) + " to " +
        to_string(ssm->strategies().size()) + " strategy)");
}
/*
 * Copyright (C) 2022 Lukas Miedema <l.miedema@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include "StateMachine/Transform/StrategyTransform.hpp"
#include "StateMachine/FaultTolerance/FaultToleranceStrategy.hpp"
#include "Helper_macros.hpp"

class EnergyBoostTransform : public StrategyTransform<FaultToleranceStrategyManager> {

private:
    double _energy_budget;
protected:
    void forward_params(const std::map<std::string, std::string> &map) override;
public:
    //! Generate the SM
    virtual void transform(StrategyStateMachine *ssm, FaultToleranceStrategyManager *sm) override;

    std::string help() override;

    const std::string get_uniqid_rtti() const override {
        return "strategy-opportunistic-boost-ft-energy";
    }
};

REGISTER_SM_TRANSFORM(EnergyBoostTransform, FaultToleranceStrategyManager, "energy-boost-transform");

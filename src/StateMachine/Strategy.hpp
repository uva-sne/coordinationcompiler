/*
 * Copyright (C) 2022 Lukas Miedema <l.miedema@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <memory>
#include <util.hpp>
#include "SystemModel.hpp"
#include "Solvers/Solver.hpp"

class Result;
class Strategy;
class StrategyManager;

typedef std::unique_ptr<Strategy> StrategyPtr;
typedef std::unique_ptr<Result> ResultPtr;
typedef std::unique_ptr<StrategyManager> StrategyManagerPtr;

/*!
 * \brief Base class for results in the SSM. Results can be enumerated by the strategy, and in a linked SSM each
 * result must have a successor strategy. Results are not explicitly created when a strategy is created, instead
 * they are managed by their owning strategy.
 */
class Result {
public:
    virtual ~Result() = default;

    /**
     * Obtain the successor assigned to this result. The method of acquiring this is implementation-defined (e.g.
     * it may be computed on-demand).
     * @return the strategy, or null if no link has been determined yet.
     */
    virtual Strategy *successor(Strategy *owner, StrategyStateMachine &ssm) = 0;

    /**
     * Determine if this result has a linked. When all results are linked, the IR is in the "linked" state.
     * @param owner
     * @param ssm
     * @return true if linked, false otherwise.
     */
    virtual bool is_linked(Strategy *owner, StrategyStateMachine &ssm) {
        return successor(owner, ssm) != nullptr;
    }

    /**
     * Return a globally unique identifier for this result.
     * @return
     */
    virtual const std::string id() const = 0;
};

/*!
 * Result of a schedulability test (which is done w/ construction)
 */
struct schedulability_result_t {
    bool is_schedulable;
    timinginfos_t makespan;

    operator bool() const {
        return is_schedulable;
    }
};

/*!
 * \brief A strategy in the SSM. A strategy is owned and operated by a StrategyManager.
 * All strategies within an SSM must share the same StrategyManager, which is set on the SSM.
 */
class Strategy {
private:
    uint64_t _id; //!< ID of the Strategy, always 0 for the trivial strategy
public:
    explicit Strategy(uint64_t id) : _id(id) {}
    explicit Strategy(Strategy &) = delete;
    virtual ~Strategy() = default;

    /**
     * @return True if this strategy modifies nothing (trivial strategy)
     */
    virtual bool is_trivial() = 0;

    /**
     * Determine if the strategy is schedulable
     * @param tg task graph
     * @param config config
     * @param manager creator of this Strategy
     * @param baseElements list of SchedElts from a built schedule
     * @param emptySchedule schedule to work with
     * @return
     */
    virtual schedulability_result_t is_schedulable(SystemModel &tg, config_t &config, StrategyManager &manager, std::vector<SchedEltPtr> &baseElements, Schedule &emptySchedule) const = 0;

    /**
     * @param i the SchedElt index
     * @param manager the creator of this Strategy
     * @return a human readable string description of the treatment of element i, for printing
     */
    virtual std::string get_role_for_elt(uint64_t i, const StrategyManager& manager) = 0;

    /**
     * Produce an element list for the schedule.
     * @param elements the schedule
     * @param manager the creator of this Strategy
     */
    virtual std::map<std::string, SchedEltPtr> export_to_schedule(std::vector<SchedEltPtr> &elements, StrategyManager &manager) const = 0;

    /**
     * Apply this strategy to a schedule. The provided schedule must be empty and will be modified. The provided baseElements won't be touched (TODO make const)
     * @param tg
     * @param config
     * @param manager
     * @param baseElements
     * @param schedule
     */
    void schedule(SystemModel &tg, config_t &config, StrategyManager &manager, std::vector<SchedEltPtr> &baseElements, Schedule &schedule) const;

    ACCESSORS_R(Strategy, uint64_t, id);
};

/*
 * \brief StrategyManager gives interpretation to the Strategy State Machine (SSM) and defines the relations between
 * the strategies.
 * Each State Machine Generators (SMG) typically supports just one kind of Strategy Manager.
 */
class StrategyManager {
private:
    Solver *_solver; //!< Owned by the pass that created the StrategyManager

public:
    explicit StrategyManager(Solver *solver) : _solver(solver) {}
    virtual ~StrategyManager() = default;

    /**
     * Human-readable name of this strategy manager.
     * @return
     */
    virtual std::string description() const = 0;

    /**
     * Get all possible results for the provided strategy. Must be constructed by build_results.
     * @param strategy the strategy
     * @return
     */
    virtual std::vector<Result *> results(const Strategy *strategy) const = 0;

    /**
     * Create result entries for each strategy.
     * @param ssm the SSM
     * @param elements set of elements
     * @return the results (can be empty)
     */
    virtual std::vector<Result *> build_results(StrategyStateMachine &ssm, const std::vector<SchedElt *> &elements) = 0;

    ACCESSORS_R(StrategyManager, Solver *, solver);
};

class EmptyStrategy : public Strategy {
public:
    EmptyStrategy() : Strategy(0) {};
    ~EmptyStrategy() override = default;

    bool is_trivial() override {
        return true;
    }

    std::string get_role_for_elt(uint64_t i, const StrategyManager& manager) override;
    schedulability_result_t is_schedulable(SystemModel &tg, config_t &config, StrategyManager &manager, std::vector<SchedEltPtr> &baseElements, Schedule &schedule) const override;
    std::map<std::string, SchedEltPtr> export_to_schedule(std::vector<SchedEltPtr> &elements, StrategyManager &manager) const override;
};


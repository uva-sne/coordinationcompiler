/*
 * Copyright (C) 2022 Lukas Miedema <l.miedema@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <utility>
#include <vector>
#include "Helper_macros.hpp"
#include "SystemModel.hpp"
#include "Schedule.hpp"
#include <StateMachine/FaultTolerance/FaultToleranceScheme.hpp>
#include <Solvers/Solver.hpp>
#include <StateMachine/Strategy.hpp>

/*
 * The Strategy State Machine is a feature of the Cecile IR which expresses design points as "strategies". A strategy
 * is an additional parameter to the scheduler. There is only one schedule in the IR, but as multiple strategies can exist
 * each strategy in effect has its own schedule, and a relation to other schedules (hence "state machine").
 * The state machine is associated with a "strategy manager", which is responsible for the management of the strategies.
 * Because strategies need to be expressed as a schedule potentially many times, the strategy manager must be able to
 * do so, including knowing what scheduler to use and how to interpret the data in the strategy.
 *
 * Compiler passes operating on the SSM generally support only one type of strategy manager.
 *
 * The mechanism works as follows:
 *  - Generator [1]: a set of strategies is generated by a _generator_, and an associated strategy manager is set
 *    The strategy manager, among others, contains a Scheduler
 *  - Transform [*]: the set of strategies is transformed by 0 or more transformation passes. These must support the
 *    set strategy manager, as the strategy manager determines the interpretation of the strategies
 *  - Linker [0..1]: the relation between strategies is established with _result_ states. Result states are maintained
 *    by the strategy manager, not by the strategy state machine itself. ssm_linked_status moves to SSM_FULLY_LINKED
 *    Note that not all implementations of a strategy manager need to support this -- some may just consider the SSM
 *    as a std::vector of strategies.
 *  - Transform [*]: more transformations can happen
 *  - Exporter [*]: the resulting SSM can be exported
 *
 * CompilerPasses can request the schedule, at which point it is scheduled (needs a fast scheduler).
 * Scheduling works as following:
 *  1. ScheduleElements are built (ScheduleFactory)
 *  --- the following steps can be repeated
 *  2. A particular Strategy is selected (schedules only exist in the context of a Strategy)
 *  3. The collection of ScheduleElements are modified (copied -- original remain untouched) using the StrategyManager
 *  and Strategy. The old collection of ScheduleElements is retained
 *  4. The scheduler (from the StrategyManager) is supplied the new set of ScheduleElements, and schedules it.
 *  5. Any operations can be done on the schedule by the compiler pass
 *  6. The new schedule elements are discarded, and the elements from step 1 are put back
 */

/**
 * Linked status of the SSM.
 */
enum ssm_linked_status_t {
    /**
     * All results have a successor
     */
    SSM_FULLY_LINKED,

    /**
     * Some results have successors
     */
    SSM_PARTIALLY_LINKED,

    /**
     * No results have successors (or there are no results)
     */
    SSM_NONE_LINKED
};

class StrategyStateMachine;
/*!
 * \brief IR part representing the SSM for multi-period fault-aware scheduling
 */
class StrategyStateMachine {
    friend Strategy;

private:
    std::vector<StrategyPtr> _strategies; //!< All strategies, must be homogeneous (no polymorphic mixing)
    std::unique_ptr<Schedule> _schedule; //!< Schedule, owned by the SSM
    StrategyManagerPtr _strategy_manager = nullptr; //!< The strategy manager, responsible for scheduling
    Strategy *_active_scope = nullptr; //!< Scope, or null. If the scope is null, conversion to a schedule is only possible on the trivial SSM
public:
    explicit StrategyStateMachine(std::unique_ptr<Schedule> schedule);

    /**
     * @return True if this SSM is degenerate (has only one strategy)
     */
    bool is_trivial() const;

    /**
     * Cast the SSM to a schedule. Only possible when "::is_trivial()" returns true.
     * @return pointer to the wrapped schedule.
     */
    operator Schedule *();

    /**
     * First step in the SSM realization process: create the base ScheduleElements (set in schedule) clear the trivial
     * status, and set the manager.
     */
    void initialize_ssm(std::unique_ptr<StrategyManager> manager);

    void clear();

    void enter_scope(Strategy *, SystemModel *, config_t *);
    void exit_scope();

    /**
     * Move the SchedElts out of the associated schedule (must be build) and return them in a vector.
     * @return
     */
    std::vector<SchedEltPtr> get_schedule_elements_exclusive();

    /**
     * Obtain a vector of pointers from the associated schedule without transfering ownership.
     * @return
     */
    std::vector<SchedElt *> get_schedule_elements();

    /**
     * Move SchedElts back into the schedule. The schedule prior to this call must not have elements. The vector
     * is cleared.
     */
    void set_schedule_elements(std::vector<SchedEltPtr>);

    /**
     * Obtain a vector of strategies, casted to S.
     * @tparam S
     * @return
     */
    template<typename S>
    std::vector<S *> strategies_unowned() const {
        std::vector<S *> result;
        for (auto &raw : this->_strategies) {
            S *strategy = dynamic_cast<S *>(raw.get());
            assert(strategy != nullptr);
            result.push_back(strategy);
        }
        return result;
    }

    /**
     * Obtain a vector of all results, casted to R
     * @tparam R type of result
     * @return
     */
    template<typename R>
    std::vector<R *> results_unowned() const {
        std::vector<R *> results;
        if (this->strategy_manager() != nullptr) {
            for (auto &strategy: this->_strategies) {
                for (auto &resultRaw: this->strategy_manager()->results(strategy.get())) {
                    R *result = dynamic_cast<R *>(resultRaw);
                    assert(result != nullptr);
                    results.push_back(result);
                }
            }
        }
        return results;
    }

    /**
     * Compute the linked status of the SSM. Returns SSM_NONE_LINKED when there is no SSM.
     * @return
     */
    ssm_linked_status_t compute_linked_status();

    ACCESSORS_R(StrategyStateMachine, std::vector<StrategyPtr>&, strategies);
    ACCESSORS_R_SMARTPTR(StrategyStateMachine, Schedule, schedule);
    ACCESSORS_RW_SMARTPTR(StrategyStateMachine, StrategyManager, strategy_manager);

    /**
     * Split the full id (e.g. "ft::exhaustive_dtmc") into the type and the pass name ("ft" and "exhaustive_dtmc").
     * fullId
     * @return
     */
    static std::pair<std::string, std::string> split_id(const std::string& fullId);
};

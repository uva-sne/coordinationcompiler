/*
 * Copyright (C) 2022 Lukas Miedema <l.miedema@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <probability.hpp>
#include "StateMachine/Generator/StrategyGenerator.hpp"
#include "Helper_macros.hpp"

/**
 * Trivial fault-tolerance generator
 * It generates a "trivial" FT SSM in which a single strategy exists that protects nothing, or rather, protects
 * everything with the "unprotected" ft scheme (which could be set to 3MR or something of course).
 * The result in every way is a FT SSM so regular FT SSM operations apply.
 *
 * Used for comparison against other FT SSM algorithms as a baseline.
 */
class TrivialFtGenerator : public StrategyGenerator<FaultToleranceStrategyManager> {

private:
    //! Active fault tolerance scheme for the rest
    std::unique_ptr<FaultToleranceScheme> _ft_unprotected = nullptr;
protected:
    void forward_params(const std::map<std::string, std::string> &map) override;

public:
    //! Generate the SM
    virtual void generate_sm(StrategyStateMachine &result, std::vector<SchedEltPtr> &baseElements,
            FaultToleranceStrategyManager *) override;

    std::unique_ptr<FaultToleranceStrategyManager> build_manager() override;

    std::string help() override;

    const std::string get_uniqid_rtti() const override {
        return "strategy-generator-trivial";
    }

    ACCESSORS_R_SMARTPTR(StrategyGenerator, FaultToleranceScheme, ft_unprotected);
};

REGISTER_SM_GENERATOR(TrivialFtGenerator, FaultToleranceStrategyManager, "trivial")

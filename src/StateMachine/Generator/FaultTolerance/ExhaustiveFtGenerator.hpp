/*
 * Copyright (C) 2022 Lukas Miedema <l.miedema@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include "StateMachine/Generator/StrategyGenerator.hpp"
#include "StateMachine/FaultTolerance/FaultToleranceStrategy.hpp"
#include "Helper_macros.hpp"

class ExhaustiveFtGenerator : public StrategyGenerator<FaultToleranceStrategyManager> {

private:
    //! Active fault tolerance scheme for protected tasks
    std::unique_ptr<FaultToleranceScheme> _ft_protected = nullptr;
    //! Active fault tolerance scheme for the rest
    std::unique_ptr<FaultToleranceScheme> _ft_unprotected = nullptr;
    //! Fault rate string
    std::string _fault_rate;
    //! Fault rate spec, parsed
    std::unique_ptr<FaultRateSpec> _fault_rate_spec;
protected:
    void forward_params(const std::map<std::string, std::string> &map) override;

public:
    //! Generate the SM
    virtual void generate_sm(StrategyStateMachine &result, std::vector<SchedEltPtr> &baseElements, FaultToleranceStrategyManager *sm) override;

    std::string help() override;

    const std::string get_uniqid_rtti() const override {
        return "strategy-generator-exhaustive";
    }

    std::unique_ptr<FaultToleranceStrategyManager> build_manager() override;

    ACCESSORS_R_SMARTPTR(StrategyGenerator, FaultToleranceScheme, ft_protected);
    ACCESSORS_R_SMARTPTR(StrategyGenerator, FaultToleranceScheme, ft_unprotected);
    ACCESSORS_R_SMARTPTR(StrategyGenerator, FaultRateSpec, fault_rate_spec);
};

REGISTER_SM_GENERATOR(ExhaustiveFtGenerator, FaultToleranceStrategyManager, "full-state-space");

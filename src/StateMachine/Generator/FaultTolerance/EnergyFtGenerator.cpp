/*
 * Copyright (C) 2022 Lukas Miedema <l.miedema@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <StateMachine/FaultTolerance/FtUtils.hpp>
#include <StateMachine/FaultTolerance/FaultRateSpec.hpp>
#include <boost/date_time/posix_time/posix_time_types.hpp>
#include "StateMachine/FaultTolerance/FaultToleranceStrategy.hpp"
#include "StateMachine/FaultTolerance/FaultToleranceLattice.hpp"
#include "EnergyFtGenerator.hpp"
#include "StateMachine/StrategyStateMachine.hpp"

IMPLEMENT_HELP(EnergyFtGenerator, Computes an FT state machine by full strategy state space exploration for energy\n
Note that this state machine may contain considerably more states, as this generator avoids choosing between energy
and reliability and instead just keeps all strategies.\n
    - timeout [int], default -: time in ms to continue searching\n
)

void EnergyFtGenerator::generate_sm(StrategyStateMachine &result, std::vector<SchedEltPtr>& elements,
        FaultToleranceStrategyManager *manager) {

    std::set<frequencyinfo_t> freqs;

    for (Task *task : tg->tasks_it()) {
        for (Version *version : task->versions()) {
            freqs.insert(version->frequency());
        }
    }

    this->_fault_rate_spec = std::make_unique<DvfsFaultRateSpec>(this->_fault_rate, tg->time_unit(), *freqs.begin(), *freqs.rbegin());
    Utils::INFO("Frequency range: " + std::to_string(*freqs.begin()) + " to " + std::to_string(*freqs.rbegin()));
    for (frequencyinfo_t v : freqs)
        Utils::INFO(" - Frequency " + std::to_string(v) + " has fault rate " + std::to_string(this->_fault_rate_spec->to_fraction(v)) + " errors/" + this->tg->time_unit());


    this->_ft_protected->fault_rate(fault_rate_spec());
    this->_ft_unprotected->fault_rate(fault_rate_spec());

    if (elements.size() >= 36)
        throw CompilerException("energy-strategy-generator", "State spaces larger than 36 tasks cannot be explored exhaustively");
    if (elements.size() >= 24)
        Utils::WARN("energy-strategy-generator", "Very large strategy state space: exhaustive exploration will most likely fail");

    uint64_t taskCount = elements.size();
    uint64_t stateSpace = (uint64_t)1 << taskCount;
    Utils::INFO("Starting exhaustive strategy generation. State space size = " + to_string(stateSpace));

    // Try everything
    std::vector<bool> state(taskCount, false);
    FaultToleranceLattice lattice(taskCount); // lattice is only used to quickly discard strategies that are unschedulable
    // "redundant" strategies are not removed, as they may save energy and thus be worthwhile to use

    uint64_t id = 0;
    do {
        lattice.add(std::make_unique<EnergyFaultToleranceStrategy>(id++, state));
    } while (FtUtils::increment(state));


    Utils::INFO("Allocated " + to_string(lattice.strategies().size()) + " strategies");

    // Now comes the real work
    Schedule *schedule = result.schedule();

    uint64_t performedEvaluations = 0;
    uint64_t lastPrintPerformedEvaluations = 0;
    auto lastPrint = std::chrono::system_clock::now();
    auto interval = std::chrono::seconds(1);

    // Unschedulable visitor propagates up
    strategyvisitor_t usv = [&lattice, &usv, &performedEvaluations](latticenode_t &key) {
        if (key.state == STATE_UNKNOWN) {
            key.state = STATE_UNSCHEDULABLE;
            lattice.visit_parents(key.strategy.get(), usv);
            performedEvaluations++;
        }
    };

    uint64_t evaluated = 0;
    strategyvisitor_t visitor = [&](latticenode_t &key) {
        if (key.state != STATE_UNKNOWN)
            return;

        auto strategy = dynamic_cast<EnergyFaultToleranceStrategy *>(key.strategy.get());
        energy_schedulability_result_t result = strategy->is_schedulable_energy(
                *this->tg,
                *this->conf,
                *manager,
                elements,
                *schedule);
        if (result.is_schedulable) {
            key.state = STATE_SCHEDULABLE;
            strategy->energy(result.energy);
            strategy->makespan(result.makespan);
            strategy->versions() = std::move(result.versions);
            // do NOT cascade to children, because then we do not know the energy / makespan
        } else {
            key.state = STATE_UNSCHEDULABLE;
            lattice.visit_parents(key.strategy.get(), usv);
        }

        auto now = std::chrono::system_clock::now();
        if (now - lastPrint > interval) {
            lastPrint = now;
            Utils::INFO("Exploration " + std::to_string(performedEvaluations * 100.0 / stateSpace)
                    + "% (" + std::to_string(performedEvaluations) + " / " + std::to_string(stateSpace) + ") at "
                    + std::to_string(performedEvaluations - lastPrintPerformedEvaluations) + " evaluations/second");
            lastPrintPerformedEvaluations = performedEvaluations;
        }

        performedEvaluations++;
        evaluated++;
    };
    lattice.visit_all(visitor);

    // Count for logging
    uint64_t strategiesSchedulable = 0;
    uint64_t strategiesUnschedulable = 0;

    strategyvisitor_t counter = [&strategiesSchedulable, &strategiesUnschedulable](latticenode_t &key) {
        if (key.state == STATE_SCHEDULABLE)
            strategiesSchedulable++;
        else if (key.state == STATE_UNSCHEDULABLE)
            strategiesUnschedulable++;
        else
            assert(false);
    };
    lattice.visit_all(counter);

    Utils::INFO("Marked " + to_string(strategiesSchedulable) + " strategies as schedulable and " + to_string(strategiesUnschedulable) +
                    " as unschedulable."
                    + std::to_string((double) evaluated * 100.0 / ((double) stateSpace)) + "% of state space evaluated (" + to_string(evaluated) + "/" + to_string(stateSpace) + ")." );

    // Export all schedulable to SSM
    strategyvisitor_t to_ssm = [&result](latticenode_t &key) {
        if (key.state == STATE_SCHEDULABLE) {
            result.strategies().push_back(std::move(key.strategy));
        }
    };
    lattice.visit_all(to_ssm);

    Utils::INFO("Pruned strategy set to " + to_string(result.strategies().size()) + " strategies");
    if (result.strategies().size() == 1)
        Utils::WARN("Strategy state machine is degenerate (only one strategy remains)");
}

void EnergyFtGenerator::forward_params(const std::map<std::string, std::string> &args) {
    StrategyGenerator<FaultToleranceStrategyManager>::forward_params(args);

    if (!args.count("ft-protected"))
        throw CompilerException("config", "No ft-protected specified");
    if (!args.count("ft-unprotected"))
        throw CompilerException("config", "No ft-unprotected specified");
    if (!args.count("fault-rate"))
        throw CompilerException("config", "No fault-rate specified");

    this->_ft_protected = std::unique_ptr<FaultToleranceScheme>(FaultToleranceRegistry::Create(args.at("ft-protected")));
    this->_ft_unprotected = std::unique_ptr<FaultToleranceScheme>(FaultToleranceRegistry::Create(args.at("ft-unprotected")));

    this->_fault_rate = args.at("fault-rate");
}

std::unique_ptr<FaultToleranceStrategyManager> EnergyFtGenerator::build_manager() {
    return std::make_unique<FaultToleranceStrategyManager>(this->_ft_protected.get(), this->_ft_unprotected.get(), this->_solver.get());
}

/*
 * Copyright (C) 2022 Lukas Miedema <l.miedema@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <StateMachine/FaultTolerance/FtUtils.hpp>
#include <StateMachine/FaultTolerance/FaultRateSpec.hpp>
#include <boost/date_time/posix_time/posix_time_types.hpp>
#include "StateMachine/FaultTolerance/FaultToleranceStrategy.hpp"
#include "StateMachine/FaultTolerance/FaultToleranceLattice.hpp"
#include "TrivialFtGenerator.hpp"
#include "StateMachine/StrategyStateMachine.hpp"

IMPLEMENT_HELP(TrivialFtGenerator, Create a fault-tolerance SSM with just a single strategy that protects nothing.\n
Useful for compare scripts\n
    * timeout [int], default -: time in ms to continue searching
)

void TrivialFtGenerator::generate_sm(StrategyStateMachine &result, std::vector<SchedEltPtr>& elements,
        FaultToleranceStrategyManager *) {
    Utils::INFO("Creating trivial ft strategy");
    result.strategies().push_back(std::make_unique<FaultToleranceStrategy>(0, std::vector<bool>(elements.size(), false)));
}

void TrivialFtGenerator::forward_params(const std::map<std::string, std::string> &args) {
    StrategyGenerator<FaultToleranceStrategyManager>::forward_params(args);

    if (!args.count("ft-unprotected"))
        throw CompilerException("config", "No ft-unprotected specified");
    if (!args.count("fault-rate"))
        throw CompilerException("config", "No fault-rate specified");

    this->_ft_unprotected = std::unique_ptr<FaultToleranceScheme>(FaultToleranceRegistry::Create(args.at("ft-unprotected")));

    //FaultRateSpec faultRate(args.at("fault-rate"), tg->time_unit());
    //this->_ft_unprotected->fault_rate(faultRate);
    // TODO: fix this, unittest this, this is actively broken
}

std::unique_ptr<FaultToleranceStrategyManager> TrivialFtGenerator::build_manager() {
    return std::make_unique<FaultToleranceStrategyManager>(this->_ft_unprotected.get(), this->_ft_unprotected.get(), this->_solver.get());
}

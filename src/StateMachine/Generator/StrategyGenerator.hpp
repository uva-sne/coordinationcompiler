/*
 * Copyright (C) 2022 Lukas Miedema <l.miedema@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include "StateMachine/StrategyStateMachine.hpp"
#include <StateMachine/StateMachinePass.hpp>
#include "StrategyGenerator.hpp"
#include <Solvers/Solver.hpp>
#include <StateMachine/FaultTolerance/FaultToleranceStrategy.hpp>
#include "ScheduleFactory.hpp"

/**
 * Entry point to the IR + SSM
 */
template<typename SM = StrategyManager>
class StrategyGenerator : public StateMachinePass<SM, false> {
protected:
    //! IR of the current application
    SystemModel *tg = nullptr;
    //! the global configuration
    config_t *conf = nullptr;
    //! Timeout of the SMG
    uint64_t _timeout = 0;
    //! Start time for SMG computation
    std::chrono::high_resolution_clock::time_point _start_time;
    //! Active solver
    std::unique_ptr<Solver> _solver = nullptr;

    void forward_params(const std::map<std::string, std::string> &args) override {
        if (args.count("timeout"))
            timeout(Utils::stringtime_to_ns(args.at("timeout")));
        if (!args.count("solver"))
            throw CompilerException("config", "No solver id specified");
        this->_solver = std::unique_ptr<Solver>(SolverRegistry::Create(args.at("solver")));
    }

public:
    //! Constructor
    explicit StrategyGenerator() = default;

    //! Generate the state machine
    virtual void generate_sm(StrategyStateMachine &result, std::vector<SchedEltPtr> &baseElements, SM *sm) = 0;

    //! Create the strategy manager associated with this type of generator
    virtual std::unique_ptr<SM> build_manager() = 0;

    void run(SystemModel *m, config_t *c, StrategyStateMachine *ssm, SM *manager) override {
        tg = m;
        conf = c;
        _start_time = std::chrono::high_resolution_clock::now();

        // IR goes from Schedule to SSM mode
        ssm->initialize_ssm(this->build_manager());

        // Remove base items from the schedule and provide them to the generator
        // This gives the generator the option to also modify the base set of schedule elements

        std::vector<SchedEltPtr> elements = ssm->get_schedule_elements_exclusive();
        generate_sm(*ssm, elements, dynamic_cast<SM *>(ssm->strategy_manager()));

        // Transfer the elements back to the state machine
        ssm->set_schedule_elements(std::move(elements));

        std::chrono::duration<double> diff = std::chrono::high_resolution_clock::now() - _start_time;
        Utils::INFO("==> SSM constructed in " + to_string(diff.count()) + " seconds");
    }
    using StateMachinePass<SM, false>::run; //! Bring in the rest from parent
    using registry_t = registry::Registry<StrategyGenerator<SM>, std::string>;

    ACCESSORS_RW(StrategyGenerator, uint64_t, timeout)
    ACCESSORS_R_SMARTPTR(StrategyGenerator, Solver, solver);
};

#define REGISTER_SM_GENERATOR(ClassName, SMType, Identifier) \
    REGISTER_SM_SUBCLASS(StrategyGenerator, ClassName, SMType, Identifier)

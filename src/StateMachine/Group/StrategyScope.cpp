/*
 * Copyright (C) 2022 Lukas Miedema <l.miedema@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <config.hpp>
#include "StrategyScope.hpp"
#include <StateMachine/StrategyStateMachine.hpp>

IMPLEMENT_HELP(StrategyScope ,
Run specified passes while a particular strategy is active, allowing the analysis of the schedule.\n
The specified passes will be run once for each strategy.\n
    - strategy: A comma-separated list of integers refering to strategies
)

void StrategyScope::run(SystemModel *m, config_t *c, StrategyStateMachine *ssm) {
    for (uint64_t id : this->_strategies) {
        bool found = false;
        for (auto const &strategy : ssm->strategies()) {
            if (strategy->id() == id) {
                ssm->enter_scope(strategy.get(), m, c);
                this->_dispatcher.dispatch(m, c, ssm);
                ssm->exit_scope();
                found = true;
                break;
            }
        }
        if (!found)
            Utils::WARN("No strategy with id '" + to_string(id) + "' in the state machine");
    }
}

void StrategyScope::forward_params(const std::map<std::string, std::string> &args) {
    if (args.count("strategy")) {
        std::string strategiesStr = args.at("strategy");
        std::vector<std::string> result;
        boost::split(result, strategiesStr, boost::is_any_of(","));

        for (const auto &strategyId : result) {
            try {
                uint64_t strategy = std::stol(strategyId);
                this->_strategies.push_back(strategy);
            } catch (std::invalid_argument &) {
                throw CompilerException("strategy", strategyId + " is not an integer");
            }
        }
    }
    if (this->_strategies.empty())
        throw CompilerException("strategy", "Required 'strategy' attribute empty or missing");
}

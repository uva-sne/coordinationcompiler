/*
 * Copyright (C) 2022 Lukas Miedema <l.miedema@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <CompilerPass.hpp>
#include <vector>
#include <Dispatcher.hpp>
#include <GroupPass.hpp>

/**
 * Strategy scope allows selecting and examinating a single strategy, which is available as the "schedule".
 * Scope semantics are a bit weird, as a Strategy modifies the ScheduleElements directly.
 * As such
 *  - Transforms (modifying the SystemModel) are illegal
 *  - The schedule within a scope is built (i.e. elements present), as such Scheduler are also illegal
 *  To achieve this behavior, the Schedule is built and transformed by the Strategy when the scope is applied.
 *  It is cleared when the scope exits.
 */
class StrategyScope : public GroupPass {
private:
    std::vector<uint64_t> _strategies;

protected:
    void forward_params(const std::map<std::string, std::string> &map) override;

public:
    void run(SystemModel *m, config_t *c, StrategyStateMachine *ssm) override;
    const std::string get_uniqid_rtti() const override { return "strategy-scope"; }

    std::string help() override;
};

REGISTER_GROUP(StrategyScope, "strategy-scope")

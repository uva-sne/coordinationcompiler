// StrategyManagers.hpp
// To use this, define the DECLARE_TYPE macro and include this file, which allows for iterating over all
// types of strategies.
// DECLARE_MANAGER(id, class name)

/**
 * Fault-tolerance strategy manager
 */
DECLARE_MANAGER("ft", FaultToleranceStrategyManager)

/**
 * Base strategy manager -- passes are registered here if they support multiple managers
 */
DECLARE_MANAGER("any", StrategyManager)
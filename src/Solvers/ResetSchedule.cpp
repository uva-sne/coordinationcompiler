/*
 * Copyright (C) 2021 brouxel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* 
 * File:   ResetSchedule.cpp
 * Author: brouxel
 * 
 * Created on 1 juillet 2021, 10:57
 */

#include "ResetSchedule.hpp"

IMPLEMENT_HELP(ResetSchedule, 
        Reset the current schedule to allow to run a new one
);

void ResetSchedule::solve(Schedule *sched) {
    sched->clear();
}

const std::string ResetSchedule::get_uniqid_rtti() const {
    return "scheduler-reset";
}

void ResetSchedule::forward_params(const std::map<std::string, std::string> &) {
}

/*
 * Copyright (C) 2023 Lukas Miedema <l.miedema@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <Utils.hpp>
#include <Solvers/Solver.hpp>

/**
 * A criteria-aware solver implements an interface which allows setting the criteria of the solver.
 * It assumes binary search is used, choosing between timeliness and energy consumption.
 *
 * Currently, the following criterias can be modeled:
 *  - minimize energy consumption, within the deadline
 *  - minimize makespan, within an energy budget
 *  - minimize energy
 *  - minimize makespan
 */


namespace OptimizationTarget {
    class Criteria {
    protected:
        globtiminginfos_t _global_deadline;
    public:
        explicit Criteria(globtiminginfos_t globalDeadline) : _global_deadline(globalDeadline) {};

        /**
         * Should the factor (i.e. energy consumption) be increased?
         * @param result
         * @return
         */
        virtual bool can_use_less_energy(Schedule *result) const = 0;

        /**
         * Is this result acceptable?
         * @param result
         * @return
         */
        virtual bool is_acceptable(Schedule *result) const = 0;

        virtual ~Criteria() = default;
    };

    class Energy : public Criteria {
    public:
        explicit Energy(globtiminginfos_t globalDeadline) : Criteria(globalDeadline) {};

        /**
         * Lower factor for as long as the makespan is met.
         * @param result
         * @return
         */
        bool can_use_less_energy(Schedule *result) const override;

        bool is_acceptable(Schedule *result) const override;
    };

    class Deadline : public Criteria {
    private:
        energycons_t _budget;
    public:
        explicit Deadline(globtiminginfos_t globalDeadline, energycons_t budget) : Criteria(globalDeadline), _budget(budget) {};

        /**
         * Lower the deadline as long as an internal energy budget is met.
         * @param result
         * @return
         */
        bool can_use_less_energy(Schedule *result) const override;

        bool is_acceptable(Schedule *result) const override;
    };
}

class CriteriaAware {
protected:
    //! Criteria to use for the solve call, or nullptr for the default criteria
    std::unique_ptr<OptimizationTarget::Criteria> _criteria = nullptr;
public:
    /**
     * Solve with a criteria.
     * @param result
     * @param criteria
     */
    virtual void solve(Schedule *result, OptimizationTarget::Criteria* criteria) = 0;

    ACCESSORS_RW_SMARTPTR(EnergyFFLS, OptimizationTarget::Criteria, criteria);
};
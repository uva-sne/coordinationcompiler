/*!
 * \file SchedAnalysis.cpp
 * \copyright GNU Public License v3.
 * \date 2019 
 * \author Benjamin Rouxel <benjamin.rouxel@uva.nl>, University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "SchedAnalysis.hpp"

using namespace std;

/*! \remark depends on #NormalizeSrcSink
 * \remark depends on #PropagateProperties
 * \remark depends on #PriorityAssignment
 */
void SchedulabilityAnalysis::check_dependencies() {
    if(!conf->passes.find_pass_before("priority-assignment", this))
        throw  CompilerException("schedulability-analysis", "Dependency unsatisfied, need a priority-assignment first");
    if(!conf->passes.find_pass_before("transform-normalize-src-sink", this))
        throw  CompilerException("schedulability-analysis", "Dependency unsatisfied, need to call transform-normalize-src-sink first");
    if(!conf->passes.find_pass_before("transform-propagate_properties", this))
        throw  CompilerException("schedulability-analysis", "Dependency unsatisfied, need to call transform-propagate_properties first");
}

IMPLEMENT_HELP_WITH_CODE(SchedulabilityAnalysis, {
    std::string types = "";
    for(SchedulabilityAnalysisRegistry::key_t t : SchedulabilityAnalysisRegistry::keys())
        types += ((std::string)t)+", ";
    types = types.substr(0, types.size()-2);
        
    msg = string("Schedulability analysis:\n") +
            "type ["+types+"]: type of scheduling strategy\n\n"+
            Solver::help();
}, Schedulability analysis: \n
    HTMLINDENT- type [@see Solver]: type of scheduling strategy\n
    For more help of one of these types call with help-pass argument `scheduler:sched_anal:TYPE`
)


string SchedulabilityAnalysis::more_help(const std::string &key) {
    if(SchedulabilityAnalysisRegistry::IsRegistered(key))
        return SchedulabilityAnalysisRegistry::Create(key, *tg, *conf)->help();
    return "Unknown schedulability analysis strategy `"+key+"'\n";
}

void SchedulabilityAnalysis::presolve(Schedule *sched) {
    if(type().empty())
        throw CompilerException("sched-ana", "You need to precise what type of solver you want, (check doc?)");

    strategy = SchedulabilityAnalysisRegistry::Create(type(), *tg, *conf);
}

void SchedulabilityAnalysis::solve(Schedule *result) {
    if(strategy->is_applicable(result))
        strategy->solve(result, timeout());
    else
        Utils::ERROR("The selected schedulability analysis is not applicable in this context");
}

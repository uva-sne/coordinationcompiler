/*
 * Copyright (C) 2021 brouxel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* 
 * File:   MicaelaVerucchiSchedAnalyses.hpp
 * Author: brouxel
 *
 * Created on 28 juin 2021, 10:36
 */

#ifndef MICAELAVERUCCHISCHEDANALYSES_HPP
#define MICAELAVERUCCHISCHEDANALYSES_HPP

#define REPRODUCIBLE 1

#include <vector>
#include <map>

#include "SchedAnalysis.hpp"

#include "utils.h"
#include "Taskset.h"
#include "DAGTask.h"
#include "tests.h"

class MicaelaVerucchiSchedAnalyses : public SchedulabilityAnalysisStrategyBase {
public:
    MicaelaVerucchiSchedAnalyses(const SystemModel &s, const config_t &c) : SchedulabilityAnalysisStrategyBase(s, c) {}
    
    /**
     * \brief Perform the schedulability analysis
     * 
     * @param result the resulting schedule
     * @param t to stop the analysis
     */
    virtual void solve(Schedule *result, uint64_t t) override;
    
    /*!
     * \brief Show help message 
     * \return help msg
     */
    virtual std::string help() = 0;
    
    
    /*! \copydoc Solver::presolve
     * 
     * It only instanciates the strategy here.
     */
    virtual void presolve(Schedule *sched);
    virtual void postsolve(Schedule *sched, bool schedulable);
    
protected:
    Taskset taskset;
    
    std::vector<int> typed_core;
    
    virtual bool do_analysis(Schedule *result) = 0;
};

#define ADD_MSA(Name) \
class MSA_ ## Name : public MicaelaVerucchiSchedAnalyses { \
public: \
    MSA_ ## Name(const SystemModel &s, const config_t &c) : MicaelaVerucchiSchedAnalyses(s,c) {} \
    virtual std::string help() override; \
protected: \
    bool do_analysis(Schedule *result) override; \
    virtual bool is_applicable(Schedule *sched) override; \
}; \
\
REGISTER_SCHEDANALYSIS(MSA_ ## Name, #Name)

ADD_MSA(Baruah2012)
ADD_MSA(Han2019)
ADD_MSA(He2019)
ADD_MSA(Graham1969)
ADD_MSA(Bonifaci2013)
ADD_MSA(Li2013)
ADD_MSA(Qamhieh2013)
ADD_MSA(Baruah2014)
ADD_MSA(Melani2015)
ADD_MSA(Pathan2017)
ADD_MSA(Fonseca2017)
ADD_MSA(Fonseca2019)
ADD_MSA(Serrano16)
ADD_MSA(Nasri2019)
ADD_MSA(Fonseca2016)
ADD_MSA(Casini2018)

#endif /* MICAELAVERUCCHISCHEDANALYSES_HPP */


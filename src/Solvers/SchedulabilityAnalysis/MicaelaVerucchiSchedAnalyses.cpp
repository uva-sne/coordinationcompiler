/*
 * Copyright (C) 2021 brouxel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* 
 * File:   MicaelaVerucchiSchedAnalyses.cpp
 * Author: brouxel
 * 
 * Created on 28 juin 2021, 10:36
 */

#include "MicaelaVerucchiSchedAnalyses.hpp"

using namespace std;

void MicaelaVerucchiSchedAnalyses::presolve(Schedule *sched) {
    sched->status(ScheduleProperties::sched_statue_e::SCHED_NOTDONE);
    vector<vector<Task*>> DAGS;
    Utils::extractGraphs(&tg, &DAGS);
    
    map<ArchitectureProperties::ComputeUnitType, size_t> typed_core_size;
    map<ArchitectureProperties::ComputeUnitType, size_t> typed_core_index;
    size_t index = 0;
    for(ComputeUnit *cu : tg.processors()) {
        if(typed_core_size.count(cu->proc_class)) {
            typed_core_size[cu->proc_class] = 1;
            typed_core_index[cu->proc_class] = index++;
        }
        else
            typed_core_size[cu->proc_class]++;
    }
    typed_core.resize(typed_core_size.size());
    for(pair<ArchitectureProperties::ComputeUnitType, size_t> el : typed_core_size) {
        typed_core[typed_core_index[el.first]] = el.second;
    }
    
    map<Task*, SubTask*> pool;
    for(vector<Task*> dag : DAGS) {
        DAGTask dt;
        
        vector<SubTask*> nodes;
        for(Task *t : dag) {
            SubTask *node = new SubTask;
            node->id = nodes.size();
            pool[t] = node;
            nodes.push_back(node);
            node->c = t->C();
            
            if(t->predecessor_tasks().size() == 0) {
                dt.setDeadline(t->D());
                dt.setPeriod(t->T());
            }
            
            if(t->force_mapping_proc().size() > 0) {
                ArchitectureProperties::ComputeUnitType last_known_core_type = ArchitectureProperties::ComputeUnitType::CUT_UNDEFINED;
                for(ComputeUnit *cu : t->force_mapping_proc()) {
                    if(last_known_core_type != ArchitectureProperties::ComputeUnitType::CUT_UNDEFINED) {
                        if(last_known_core_type != cu->proc_class)
                            Utils::ERROR("Task "+t->id()+" supports multiple type of cores, last one will be picked");
                    }
                    last_known_core_type = cu->proc_class;
                }
                node->gamma = typed_core_index[last_known_core_type];
            }
            else
                node->gamma = typed_core_index[typed_core_index.begin()->first];
            
            auto tmp = sched->schedjobs(t);
            if(boost::size(tmp) > 1)
                throw CompilerException("RTA Verucchi", "Don't know what to do when there is multiple job for 1 task");
            if(boost::size(tmp) == 1 && sched->get_mapping(*(tmp.begin())) != nullptr) 
                node->core = sched->get_mapping(*(tmp.begin()))->core()->sysID;
        }
        dt.setVertices(nodes);
        
        for(Task *t : dag) {
            SubTask *src = pool[t];
            for(Task::dep_t els : t->successor_tasks()) {
                SubTask *sink = pool[els.first];
                src->succ.push_back(sink);
                sink->pred.push_back(src);
            }
        }
        
        dt.transitiveReduction();

        dt.computeWorstCaseWorkload();
        dt.computeVolume();
        dt.computeLength();
        dt.computeUtilization();
        dt.computeDensity();
        
        taskset.tasks.push_back(dt);
    }
    
    taskset.computeUtilization();
    taskset.computeHyperPeriod();
    taskset.computeMaxDensity();
    
//    std::cout<<"Assignment: "<< WorstFitProcessorsAssignment(taskset, tg.processors().size())<<std::endl;
    
//    for(size_t i=0; i<taskset.tasks.size();++i){
//        std::cout<<taskset.tasks[i]<<std::endl;
//    }
    
//    std::cout<<"Taskset utilization: "<<taskset.getUtilization()<<std::endl;
//    std::cout<<"Taskset Hyper-Period: "<<taskset.getHyperPeriod()<<std::endl;
//    std::cout<<"Taskset max Density: "<<taskset.getMaxDensity()<<std::endl<<std::endl;
}

void MicaelaVerucchiSchedAnalyses::solve(Schedule *result, uint64_t t) {
    presolve(result);
    bool schedulable = do_analysis(result);
    postsolve(result, schedulable);
}

void MicaelaVerucchiSchedAnalyses::postsolve(Schedule *result, bool schedulable) {
    if(schedulable) {
        result->status(ScheduleProperties::sched_statue_e::SCHED_COMPLETE);
        Utils::INFO("\t====> Task set is schedulable");
    }
    else {
        result->status(ScheduleProperties::sched_statue_e::SCHED_UNSCHEDULABLE);
        Utils::INFO("\t====> Task set is unschedulable");
    }
}

IMPLEMENT_HELP(MSA_Baruah2012, 
    From 'Baruah et al., A generalized parallel task model for recurrent real-time processes' (RTSS 2012)\n
    Context: single-DAG, G-EDF, constrained/arbitrary deadlines
)

bool MSA_Baruah2012::do_analysis(Schedule *result) {
    if(taskset.tasks.size() == 0)
        throw CompilerException("Baruah2012", "There should be at least one task to run this schedulability analysis");
    
    DAGTask dt = taskset.tasks[0];
    if(tg.taskset_deadline_type() == TaskProperties::DeadlineModel::DEA_CONSTRAINED)
        return GP_FP_EDF_Baruah2012_C(dt, tg.processors().size());
    
    return GP_FP_EDF_Baruah2012_A(dt, tg.processors().size());
}

bool MSA_Baruah2012::is_applicable(Schedule *sched) {
    return 
        tg.taskset_type() == TaskProperties::TaskSetType::TAS_DAG
    && (tg.taskset_rec_model() == TaskProperties::RecurringModel::REC_PERIODIC || tg.taskset_rec_model() == TaskProperties::RecurringModel::REC_SPORADIC)
    && sched->mapping_mode() == ScheduleProperties::Mapping::MAP_GLOBAL
    && sched->priority_mode() == ScheduleProperties::PriorityScheme_e::PRIO_DYNAMIC
    && sched->preemption_mode() == ScheduleProperties::PreemptionModel::PRE_FULL
    && (tg.cu_type() == ArchitectureProperties::Cores::COR_SINGLE || tg.cu_type() == ArchitectureProperties::Cores::COR_MULTI_HOMOGENEOUS);
}

IMPLEMENT_HELP(MSA_Han2019,
From \n
Context:
)

bool MSA_Han2019::do_analysis(Schedule *result) {
    if(taskset.tasks.size() == 0)
        throw CompilerException("Baruah2012", "There should be at least one task to run this schedulability analysis");
    
    DAGTask dt = taskset.tasks[0];
    return GP_FP_Han2019_C_1(dt, typed_core);
}

bool MSA_Han2019::is_applicable(Schedule *sched) {
    return 
        tg.taskset_type() == TaskProperties::TaskSetType::TAS_DAG
    && sched->priority_mode() == ScheduleProperties::PriorityScheme_e::PRIO_DYNAMIC
    && sched->preemption_mode() == ScheduleProperties::PreemptionModel::PRE_FULL
    && tg.cu_type() == ArchitectureProperties::Cores::COR_MULTI_HETEROGENEOUS;
}

IMPLEMENT_HELP(MSA_He2019, 
    From 'Qingqiang He et al., Intra-task priority assignmentin real-time scheduling of dag tasks on multi-cores. (IEEE Transactions on Parallel and Distributed Systems 2019)'
    Context: single-/multi-DAG, G-FP, constrained deadlines
)

bool MSA_He2019::do_analysis(Schedule *result) {
    if(tg.taskset_type() == TaskProperties::TaskSetType::TAS_DAG) {
        DAGTask dt = taskset.tasks[0];
        return GP_FP_He2019_C(dt, tg.processors().size());
    }
    
    return GP_FP_FTP_He2019_C(taskset, tg.processors().size());
}

bool MSA_He2019::is_applicable(Schedule *sched) {
    return (tg.taskset_type() == TaskProperties::TaskSetType::TAS_DAG 
    || (tg.taskset_type() == TaskProperties::TaskSetType::TAS_MULTI_DAG
        && tg.taskset_rec_model() == TaskProperties::RecurringModel::REC_PERIODIC
        && tg.taskset_deadline_type() == TaskProperties::DeadlineModel::DEA_CONSTRAINED
    )) 
    && sched->priority_mode() == ScheduleProperties::PriorityScheme_e::PRIO_DYNAMIC /*The algo uses a specific priority assignment*/
    && sched->mapping_mode() == ScheduleProperties::Mapping::MAP_GLOBAL
    && sched->preemption_mode() == ScheduleProperties::PreemptionModel::PRE_FULL
    && (tg.cu_type() == ArchitectureProperties::Cores::COR_SINGLE || tg.cu_type() == ArchitectureProperties::Cores::COR_MULTI_HOMOGENEOUS);
}

IMPLEMENT_HELP(MSA_Graham1969, 
From 'Graham, Bounds on Multiprocessing Timing Anomalies, SIAM Journal on Applied Mathematics 1969'
Context: single-DAG, G-FP, arbitrary deadlines
)

bool MSA_Graham1969::do_analysis(Schedule *result) {
    if(taskset.tasks.size() == 0)
        throw CompilerException("Baruah2012", "There should be at least one task to run this schedulability analysis");
    
    DAGTask dt = taskset.tasks[0];
    return Graham1969(dt, tg.processors().size());
}

bool MSA_Graham1969::is_applicable(Schedule *sched) {
    return tg.taskset_type() == TaskProperties::TaskSetType::TAS_DAG 
    && tg.taskset_rec_model() == TaskProperties::RecurringModel::REC_PERIODIC
    && (tg.cu_type() == ArchitectureProperties::Cores::COR_SINGLE || tg.cu_type() == ArchitectureProperties::Cores::COR_MULTI_HOMOGENEOUS);
}

IMPLEMENT_HELP(MSA_Bonifaci2013,
From Bonifaci et al., Feasibility Analysis in the Sporadic DAG Task Model, ECRTS 2013\n
Context:
)

bool MSA_Bonifaci2013::do_analysis(Schedule *result) {
    if(result->priority_mode() == ScheduleProperties::PriorityScheme_e::PRIO_FIXED) {
        if(tg.taskset_deadline_type() == TaskProperties::DeadlineModel::DEA_CONSTRAINED)
            return GP_FP_DM_Bonifaci2013_C(taskset, tg.processors().size());
        else
            return GP_FP_DM_Bonifaci2013_A(taskset, tg.processors().size());
    }
    
    return GP_FP_EDF_Bonifaci2013_A(taskset, tg.processors().size());
}

bool MSA_Bonifaci2013::is_applicable(Schedule *sched) {
    return  tg.taskset_type() == TaskProperties::TaskSetType::TAS_MULTI_DAG
    && (tg.taskset_rec_model() == TaskProperties::RecurringModel::REC_PERIODIC || tg.taskset_rec_model() == TaskProperties::RecurringModel::REC_SPORADIC)
    && sched->mapping_mode() == ScheduleProperties::Mapping::MAP_GLOBAL
    && sched->preemption_mode() == ScheduleProperties::PreemptionModel::PRE_FULL
    && (tg.cu_type() == ArchitectureProperties::Cores::COR_SINGLE || tg.cu_type() == ArchitectureProperties::Cores::COR_MULTI_HOMOGENEOUS);
}

IMPLEMENT_HELP(MSA_Li2013,
From Li et al., Analysis of Global EDF for Parallel Tasks, ECRTS 2013\n
Context:
Note: extra version available in the paper for arbitrary deadlines
)

bool MSA_Li2013::do_analysis(Schedule *result) {
    return GP_FP_EDF_Li2013_I(taskset, tg.processors().size());
}

bool MSA_Li2013::is_applicable(Schedule *sched) {
    return  tg.taskset_type() == TaskProperties::TaskSetType::TAS_MULTI_DAG
    && (tg.taskset_deadline_type() == TaskProperties::DeadlineModel::DEA_CONSTRAINED || tg.taskset_deadline_type() == TaskProperties::DeadlineModel::DEA_IMPLICIT)
    && (tg.taskset_rec_model() == TaskProperties::RecurringModel::REC_PERIODIC || tg.taskset_rec_model() == TaskProperties::RecurringModel::REC_SPORADIC)
    && sched->mapping_mode() == ScheduleProperties::Mapping::MAP_GLOBAL
    && sched->priority_mode() == ScheduleProperties::PriorityScheme_e::PRIO_DYNAMIC
    && sched->preemption_mode() == ScheduleProperties::PreemptionModel::PRE_FULL
    && (tg.cu_type() == ArchitectureProperties::Cores::COR_SINGLE || tg.cu_type() == ArchitectureProperties::Cores::COR_MULTI_HOMOGENEOUS);
}


IMPLEMENT_HELP(MSA_Qamhieh2013,
From Qamhieh et al., Global EDF scheduling of directed acyclic graphs on multiprocessor systems, RTNS 2013\n
Context:
)

bool MSA_Qamhieh2013::do_analysis(Schedule *result) {
    return GP_FP_EDF_Qamhieh2013_C(taskset, tg.processors().size());
}

bool MSA_Qamhieh2013::is_applicable(Schedule *sched) {
    return  tg.taskset_type() == TaskProperties::TaskSetType::TAS_MULTI_DAG
    && tg.taskset_deadline_type() == TaskProperties::DeadlineModel::DEA_CONSTRAINED
    && (tg.taskset_rec_model() == TaskProperties::RecurringModel::REC_PERIODIC || tg.taskset_rec_model() == TaskProperties::RecurringModel::REC_SPORADIC)
    && sched->mapping_mode() == ScheduleProperties::Mapping::MAP_GLOBAL
    && sched->priority_mode() == ScheduleProperties::PriorityScheme_e::PRIO_DYNAMIC
    && sched->preemption_mode() == ScheduleProperties::PreemptionModel::PRE_FULL
    && (tg.cu_type() == ArchitectureProperties::Cores::COR_SINGLE || tg.cu_type() == ArchitectureProperties::Cores::COR_MULTI_HOMOGENEOUS);
}

IMPLEMENT_HELP(MSA_Baruah2014,
From Baruah, Improved Multiprocessor Global Schedulability Analysis of Sporadic DAG Task Systems, ECRTS 2014\n
Context:
)

bool MSA_Baruah2014::do_analysis(Schedule *result) {
    return GP_FP_EDF_Baruah2014_C(taskset, tg.processors().size());
}

bool MSA_Baruah2014::is_applicable(Schedule *sched) {
    return  tg.taskset_type() == TaskProperties::TaskSetType::TAS_MULTI_DAG
    && tg.taskset_deadline_type() == TaskProperties::DeadlineModel::DEA_CONSTRAINED
    && (tg.taskset_rec_model() == TaskProperties::RecurringModel::REC_PERIODIC || tg.taskset_rec_model() == TaskProperties::RecurringModel::REC_SPORADIC)
    && sched->mapping_mode() == ScheduleProperties::Mapping::MAP_GLOBAL
    && sched->priority_mode() == ScheduleProperties::PriorityScheme_e::PRIO_DYNAMIC
    && sched->preemption_mode() == ScheduleProperties::PreemptionModel::PRE_FULL
    && (tg.cu_type() == ArchitectureProperties::Cores::COR_SINGLE || tg.cu_type() == ArchitectureProperties::Cores::COR_MULTI_HOMOGENEOUS);
}

IMPLEMENT_HELP(MSA_Melani2015,
From Melani et al., Response-time analysis of conditional dag tasks in multiprocessor systems, ECRTS 2015\n
Context:
)

bool MSA_Melani2015::do_analysis(Schedule *result) {
    if(result->priority_mode() == ScheduleProperties::PriorityScheme_e::PRIO_FIXED)
        return GP_FP_FTP_Melani2015_C(taskset, tg.processors().size());
    else
        return GP_FP_EDF_Melani2015_C(taskset, tg.processors().size());
}

bool MSA_Melani2015::is_applicable(Schedule *sched) {
    return  tg.taskset_type() == TaskProperties::TaskSetType::TAS_MULTI_DAG
    && tg.taskset_deadline_type() == TaskProperties::DeadlineModel::DEA_CONSTRAINED
    && (tg.taskset_rec_model() == TaskProperties::RecurringModel::REC_PERIODIC || tg.taskset_rec_model() == TaskProperties::RecurringModel::REC_SPORADIC)
    && sched->mapping_mode() == ScheduleProperties::Mapping::MAP_GLOBAL
    && sched->preemption_mode() == ScheduleProperties::PreemptionModel::PRE_FULL
    && (tg.cu_type() == ArchitectureProperties::Cores::COR_SINGLE || tg.cu_type() == ArchitectureProperties::Cores::COR_MULTI_HOMOGENEOUS);
}

IMPLEMENT_HELP(MSA_Pathan2017,
From\n
Context:
)

bool MSA_Pathan2017::do_analysis(Schedule *result) {
    return GP_FP_DM_Pathan2017_C(taskset, tg.processors().size());
}

bool MSA_Pathan2017::is_applicable(Schedule *sched) {
    return  tg.taskset_type() == TaskProperties::TaskSetType::TAS_MULTI_DAG
    && tg.taskset_deadline_type() == TaskProperties::DeadlineModel::DEA_CONSTRAINED
    && (tg.taskset_rec_model() == TaskProperties::RecurringModel::REC_PERIODIC || tg.taskset_rec_model() == TaskProperties::RecurringModel::REC_SPORADIC)
    && sched->mapping_mode() == ScheduleProperties::Mapping::MAP_GLOBAL
    && sched->priority_mode() == ScheduleProperties::PriorityScheme_e::PRIO_FIXED
    && sched->preemption_mode() == ScheduleProperties::PreemptionModel::PRE_FULL
    && (tg.cu_type() == ArchitectureProperties::Cores::COR_SINGLE || tg.cu_type() == ArchitectureProperties::Cores::COR_MULTI_HOMOGENEOUS);
}

IMPLEMENT_HELP(MSA_Fonseca2017,
From\n
Context:
)

bool MSA_Fonseca2017::do_analysis(Schedule *result) {
    return GP_FP_FTP_Fonseca2017_C(taskset, tg.processors().size());
}

bool MSA_Fonseca2017::is_applicable(Schedule *sched) {
    return  tg.taskset_type() == TaskProperties::TaskSetType::TAS_MULTI_DAG
    && tg.taskset_deadline_type() == TaskProperties::DeadlineModel::DEA_CONSTRAINED
    && (tg.taskset_rec_model() == TaskProperties::RecurringModel::REC_PERIODIC || tg.taskset_rec_model() == TaskProperties::RecurringModel::REC_SPORADIC)
    && sched->mapping_mode() == ScheduleProperties::Mapping::MAP_GLOBAL
    && sched->priority_mode() == ScheduleProperties::PriorityScheme_e::PRIO_FIXED
    && sched->preemption_mode() == ScheduleProperties::PreemptionModel::PRE_FULL
    && (tg.cu_type() == ArchitectureProperties::Cores::COR_SINGLE || tg.cu_type() == ArchitectureProperties::Cores::COR_MULTI_HOMOGENEOUS);
}

IMPLEMENT_HELP(MSA_Fonseca2019,
From\n
Context:
)

bool MSA_Fonseca2019::do_analysis(Schedule *result) {
    if(tg.taskset_deadline_type() == TaskProperties::DeadlineModel::DEA_CONSTRAINED)
        return GP_FP_FTP_Fonseca2019(taskset, tg.processors().size());
    else
        return GP_FP_FTP_Fonseca2019(taskset, tg.processors().size(), false);
}

bool MSA_Fonseca2019::is_applicable(Schedule *sched) {
    return  tg.taskset_type() == TaskProperties::TaskSetType::TAS_MULTI_DAG
    && (tg.taskset_rec_model() == TaskProperties::RecurringModel::REC_PERIODIC || tg.taskset_rec_model() == TaskProperties::RecurringModel::REC_SPORADIC)
    && sched->mapping_mode() == ScheduleProperties::Mapping::MAP_GLOBAL
    && sched->priority_mode() == ScheduleProperties::PriorityScheme_e::PRIO_FIXED
    && sched->preemption_mode() == ScheduleProperties::PreemptionModel::PRE_FULL
    && (tg.cu_type() == ArchitectureProperties::Cores::COR_SINGLE || tg.cu_type() == ArchitectureProperties::Cores::COR_MULTI_HOMOGENEOUS);
}

IMPLEMENT_HELP(MSA_Serrano16,
From\n
Context:
)

bool MSA_Serrano16::do_analysis(Schedule *result) {
    return GP_LP_FTP_Serrano16_C(taskset, tg.processors().size());
}

bool MSA_Serrano16::is_applicable(Schedule *sched) {
    return  tg.taskset_type() == TaskProperties::TaskSetType::TAS_MULTI_DAG
    && tg.taskset_deadline_type() == TaskProperties::DeadlineModel::DEA_CONSTRAINED
    && (tg.taskset_rec_model() == TaskProperties::RecurringModel::REC_PERIODIC || tg.taskset_rec_model() == TaskProperties::RecurringModel::REC_SPORADIC)
    && sched->mapping_mode() == ScheduleProperties::Mapping::MAP_GLOBAL
    && sched->priority_mode() == ScheduleProperties::PriorityScheme_e::PRIO_FIXED
    && sched->preemption_mode() == ScheduleProperties::PreemptionModel::PRE_FULL
    && (tg.cu_type() == ArchitectureProperties::Cores::COR_SINGLE || tg.cu_type() == ArchitectureProperties::Cores::COR_MULTI_HOMOGENEOUS);
}

IMPLEMENT_HELP(MSA_Nasri2019,
From\n
Context:
)

bool MSA_Nasri2019::do_analysis(Schedule *result) {
    return G_LP_FTP_Nasri2019_C(taskset, tg.processors().size());
}

bool MSA_Nasri2019::is_applicable(Schedule *sched) {
    return  tg.taskset_type() == TaskProperties::TaskSetType::TAS_MULTI_DAG
    && tg.taskset_deadline_type() == TaskProperties::DeadlineModel::DEA_CONSTRAINED
    && (tg.taskset_rec_model() == TaskProperties::RecurringModel::REC_PERIODIC || tg.taskset_rec_model() == TaskProperties::RecurringModel::REC_SPORADIC)
    && sched->mapping_mode() == ScheduleProperties::Mapping::MAP_GLOBAL
    && sched->priority_mode() == ScheduleProperties::PriorityScheme_e::PRIO_FIXED
    && sched->preemption_mode() == ScheduleProperties::PreemptionModel::PRE_FULL
    && (tg.cu_type() == ArchitectureProperties::Cores::COR_SINGLE || tg.cu_type() == ArchitectureProperties::Cores::COR_MULTI_HOMOGENEOUS);
}

IMPLEMENT_HELP(MSA_Fonseca2016,
From\n
Context:
)

bool MSA_Fonseca2016::do_analysis(Schedule *result) {
    return P_FP_FTP_Fonseca2016_C(taskset, tg.processors().size());
}

bool MSA_Fonseca2016::is_applicable(Schedule *sched) {
    return  tg.taskset_type() == TaskProperties::TaskSetType::TAS_MULTI_DAG
    && tg.taskset_deadline_type() == TaskProperties::DeadlineModel::DEA_CONSTRAINED
    && (tg.taskset_rec_model() == TaskProperties::RecurringModel::REC_PERIODIC || tg.taskset_rec_model() == TaskProperties::RecurringModel::REC_SPORADIC)
    && sched->mapping_mode() == ScheduleProperties::Mapping::MAP_GLOBAL
    && sched->priority_mode() == ScheduleProperties::PriorityScheme_e::PRIO_FIXED
    && sched->preemption_mode() == ScheduleProperties::PreemptionModel::PRE_FULL
    && (tg.cu_type() == ArchitectureProperties::Cores::COR_SINGLE || tg.cu_type() == ArchitectureProperties::Cores::COR_MULTI_HOMOGENEOUS);
}

IMPLEMENT_HELP(MSA_Casini2018,
From\n
Context:
)

bool MSA_Casini2018::do_analysis(Schedule *result) {
    return P_LP_FTP_Casini2018_C(taskset, tg.processors().size());
}

bool MSA_Casini2018::is_applicable(Schedule *sched) {
    return  tg.taskset_type() == TaskProperties::TaskSetType::TAS_MULTI_DAG
    && tg.taskset_deadline_type() == TaskProperties::DeadlineModel::DEA_CONSTRAINED
    && (tg.taskset_rec_model() == TaskProperties::RecurringModel::REC_PERIODIC || tg.taskset_rec_model() == TaskProperties::RecurringModel::REC_SPORADIC)
    && sched->mapping_mode() == ScheduleProperties::Mapping::MAP_GLOBAL
    && sched->priority_mode() == ScheduleProperties::PriorityScheme_e::PRIO_FIXED
    && sched->preemption_mode() == ScheduleProperties::PreemptionModel::PRE_FULL
    && (tg.cu_type() == ArchitectureProperties::Cores::COR_SINGLE || tg.cu_type() == ArchitectureProperties::Cores::COR_MULTI_HOMOGENEOUS);
}
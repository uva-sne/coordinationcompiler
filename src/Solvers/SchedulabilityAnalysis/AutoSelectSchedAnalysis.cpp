/*
 * Copyright (C) 2021 brouxel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* 
 * File:   AutoSelectSchedAnalysis.cpp
 * Author: brouxel
 * 
 * Created on 1 juillet 2021, 10:20
 */

#include "AutoSelectSchedAnalysis.hpp"

using namespace std;

IMPLEMENT_HELP(AutoSelectSchedAnalysis, 
    Run all schedulability analysis applicable to the input configuration
)

bool AutoSelectSchedAnalysis::is_applicable(Schedule *) {
    return true;
}

void AutoSelectSchedAnalysis::solve(Schedule *result, uint64_t t) {
    bool at_least_one = false;
    size_t tested = 0;
    for(string sa_name : SchedulabilityAnalysisRegistry::keys()) {
        if(sa_name == "auto") continue;
        
        SchedulabilityAnalysisStrategyBase *s = SchedulabilityAnalysisRegistry::Create(sa_name, tg, conf);
        Schedule copy_sched = *result;
        if(s->is_applicable(&copy_sched)) {
            ++tested;
            Utils::INFO(">>> Trying sched analysis id: "+sa_name);
            s->solve(&copy_sched, t);
            at_least_one = at_least_one || copy_sched.status() == ScheduleProperties::sched_statue_e::SCHED_COMPLETE;
        }
        delete s;
    }
    Utils::INFO("With the current configuration "+to_string(tested)+" analyses were tested, and "+to_string(SchedulabilityAnalysisRegistry::keys().size()-tested)+" were skipped");
    if(at_least_one)
        result->status(ScheduleProperties::sched_statue_e::SCHED_COMPLETE);
    
}
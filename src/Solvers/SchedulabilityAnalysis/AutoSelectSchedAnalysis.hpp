/*
 * Copyright (C) 2021 brouxel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* 
 * File:   AutoSelectSchedAnalysis.hpp
 * Author: brouxel
 *
 * Created on 1 juillet 2021, 10:20
 */

#ifndef AUTOSELECTSCHEDANALYSIS_HPP
#define AUTOSELECTSCHEDANALYSIS_HPP

#include "SchedAnalysis.hpp"

class AutoSelectSchedAnalysis : public SchedulabilityAnalysisStrategyBase {
public:
    AutoSelectSchedAnalysis(const SystemModel &s, const config_t &c) : SchedulabilityAnalysisStrategyBase(s, c) {}
    
    virtual void solve(Schedule *result, uint64_t t) override;
    virtual std::string help() override;
    virtual bool is_applicable(Schedule *sched) override;
};

REGISTER_SCHEDANALYSIS(AutoSelectSchedAnalysis, "auto")

#endif /* AUTOSELECTSCHEDANALYSIS_HPP */


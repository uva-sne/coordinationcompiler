/*!
 * \file SchedAnalysis.hpp
 * \copyright GNU Public License v3.
 * \date 2019 
 * \author Benjamin Rouxel <benjamin.rouxel@uva.nl>, University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SCHEDANALYSIS_H
#define SCHEDANALYSIS_H

#include <vector>
#include <chrono>
#include <type_traits>

#include "config.hpp"
#include "SystemModel.hpp"
#include "Schedule.hpp"
#include "Utils.hpp"
#include "Solvers/Solver.hpp"
#include "CompilerPass.hpp"
#include "Utils/Log.hpp"
#include "Utils/Graph.hpp"

/*! \brief Base class for all schedulability analysis strategies
 */
class SchedulabilityAnalysisStrategyBase {
protected:
    //! IR of the current application
    const SystemModel &tg;
    //! the global configuration
    const config_t &conf;
    
    //! Start time to check timeout
    std::chrono::high_resolution_clock::time_point start_time;
    
    uint64_t timeout = 0; //!< timeout to not overrun
public:
    /**
     * Constructor
     * @param s \copybrief SchedulabilityAnalysisStrategyBase::tg
     * @param c \copybrief SchedulabilityAnalysisStrategyBase::conf
     */
    explicit SchedulabilityAnalysisStrategyBase(const SystemModel &s, const config_t &c) : tg(s), conf(c) {}
    
    virtual ~SchedulabilityAnalysisStrategyBase() {}
    
    /**
     * \brief Perform the schedulability analysis
     * 
     * @param result the resulting schedule
     * @param t to stop the analysis
     */
    virtual void solve(Schedule *result, uint64_t t) = 0;
    
    /*!
     * \brief Show help message 
     * \return help msg
     */
    virtual std::string help() = 0;
    
    virtual bool is_applicable(Schedule *sched) = 0;
};

/*!
 * \brief Base class for all schedulability analysis
 * 
 * When using an online scheduler, it is required to prove in advance that the
 * scheduler will be able to find a schedule where all tasks meet their deadline.
 * 
 * The schedulability analysis computes the Worst-Case Response Time (WCRT) for 
 * each task, accounting for priorities, mapping, dependencies, preemptions, and
 * any other blocking time. This WCRT is then compared to the deadline and if
 * inferior for all tasks, then the task set is schedulable using the selected
 * scheduling policy on the target architecture.
 * 
 */
class SchedulabilityAnalysis : public Solver {
    //! Reference to the selected strategy
    SchedulabilityAnalysisStrategyBase *strategy;
    
public:
    //! Constructor
    explicit SchedulabilityAnalysis() : Solver() {};
    
    virtual void solve(Schedule *result) override;
    /*! \copydoc Solver::presolve
     * 
     * It only instanciates the strategy here.
     */
    virtual void presolve(Schedule *sched) override;
    
    const std::string get_uniqid_rtti() const override { return "solver-sched_anal" ;}
    
    /*! \copydoc CompilerPass::help
     * \see #Solver::help for a general help on solvers
     * \see #SchedulabilityAnalysisStrategyBase subclasses for a list of available scheduling strategy
     */
    virtual std::string help() override;
    
    virtual std::string more_help(const std::string &key) override;
    
protected:
    virtual void check_dependencies() override;
};

/*!
 * \typedef SchedulabilityAnalysisRegistry
 * \brief Registry containing all schedulability analysis strategy
 */
using SchedulabilityAnalysisRegistry = registry::Registry<SchedulabilityAnalysisStrategyBase, std::string, const SystemModel &, const config_t &>;

/*!
 * \brief Helper macro to register a new derived strategy
 */
#define REGISTER_SCHEDANALYSIS(ClassName, Identifier) \
  REGISTER_SUBCLASS(SchedulabilityAnalysisStrategyBase, ClassName, std::string, Identifier, const SystemModel &, const config_t &)

REGISTER_SOLVER(SchedulabilityAnalysis, "schedulability_analysis")

#endif
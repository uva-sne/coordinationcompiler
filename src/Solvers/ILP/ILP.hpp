/*!
 * \file ILP.hpp
 * \copyright GNU Public License v3.
 * \date 2020 
 * \author Benjamin Rouxel <benjamin.rouxel@uva.nl>, University of Amsterdam
 * \author Stephen Nicholas Swatman <s.n.swatman@uva.nl> University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PROXYILPSOLVER_H
#define PROXYILPSOLVER_H

#include <boost/multiprecision/cpp_int.hpp>
#include <memory>
#include <vector>
#include <string>
#include <map>

using boost::multiprecision::int256_t;

/**
 * @brief A variable within an ILP problem.
 *
 * This class respresents a single variable within an ILP problem. It has a
 * name and a type, which can be either GENERAL (meaning it can take any
 * integer value) or BINARY (meaning it can take only 0 or 1).
 */
class ILPVariable {
public:
    //! Type of the variable
    enum class Type {
        BINARY, //!< Binary variable
        GENERAL //!< General variable, usually an int
    };

    /**
     * Constructor 
     * @param name \copybrief ILPVariable::name
     * @param type \copybrief ILPVariable::type
     */
    ILPVariable(
        std::string name,
        Type type
    );

    /**
     * @brief Set a variable to binary.
     *
     * Calling this method on a variable sets it to be binary instead of
     * general.
     */
    void binary();

    const std::string name; //!< it's unique name
    Type type; //!< it's type
};

/**
 * @brief A base class for ILP expressions.
 *
 * An ILP expression is the non-numeric part of a constraint. For example, in
 * the constraint "A + 2B >= 5", "A + 2B" is an expression. It consists, in
 * turn, of other expressions.
 */
class ILPExpression {
public:
    /**
     * Clone an ILPExpression
     * @return a unique pointer to the new one
     */
    virtual std::unique_ptr<ILPExpression> clone() const = 0;

    /**
     * @brief Flatten an ILP expression into a map of coefficients.
     *
     * Having an internal syntax tree for expressions is nice and all, but it
     * doesn't always play nicely with LP library APIs. Many of those work in
     * terms of matrices, so to translate to that we need to extract a flat
     * coefficient mapping from our expression. This is a map of type:
     *
     * string -> int256_t
     *
     * Such that the value for string i is the coefficient for the variable
     * with name i. For example, the expression "-x + 7 y" translates to the
     * map {x: -1, y: 7}
     *
     * This method handles any combination of expression types, so it can also
     * handle expressions like "7 (x - 11 y)".
     * 
     * \return map of coefficient
     */
    virtual std::map<const std::string, int256_t> flatten() const = 0;
};

/**
 * @brief An ILP expression for single variables.
 *
 * In the expression "A + 2B", both "A" and "B" are variable expressions. They
 * simple refer to a variable within the problem, much like ILPVariable, but
 * they can be used as an expression too.
 */
class ILPExpressionVar : public ILPExpression {
public:
    /**
     * Constructor
     * @param var \copybrief ILPExpressionVar::var
     */
    ILPExpressionVar(ILPVariable & var);
    //! \copydoc ILPExpression::clone
    virtual std::unique_ptr<ILPExpression> clone() const;
    
    //! \copydoc ILPExpression::flatten
    virtual std::map<const std::string, int256_t> flatten() const override;

    ILPVariable & var; //!< ILP variable
};

/**
 * @brief An ILP expression for binary operators.
 *
 * This class can represent addition and subtraction operations. It has a left
 * hand side and a right hand side, each of which must be an ILP expression in
 * and of itself. In the expression "A + 2B", the entire expression is a
 * binary operator, with "A" being the LHS and "2B" being the RHS.
 */
class ILPExpressionBinop : public ILPExpression {
public:
    //! Type of the binary expression
    enum class Type {
        ADD, //!< addition
        SUB //!< susbtraction
    };

    /**
     * Constructor
     * @param op \copybrief ILPExpressionBinop::type
     * @param lhs \copybrief ILPExpressionBinop::lhs
     * @param rhs \copybrief ILPExpressionBinop::rhs
     */
    ILPExpressionBinop(
        Type op,
        ILPExpression && lhs,
        ILPExpression && rhs
    );
    /**
     * Copy constructor
     * @param orig expression to copy
     */
    ILPExpressionBinop(const ILPExpressionBinop &orig);
    //! \copydoc ILPExpression::clone
    virtual std::unique_ptr<ILPExpression> clone() const;
    //! \copydoc ILPExpression::flatten
    virtual std::map<const std::string, int256_t> flatten() const override;

    std::unique_ptr<ILPExpression> lhs; //!< left hand side expression
    std::unique_ptr<ILPExpression> rhs; //!< right hand side expression
    Type type;  //!< operator 
};

/**
 * @brief An ILP expression for muliplying a variable by a scalar.
 *
 * Most ILP solvers support expressions of the type "2B", which means the
 * variable "B" multiplied by the scalar 2. This class represents such scaling
 * operations.
 */
class ILPExpressionScale : public ILPExpression {
public:
    /**
     * Constructor
     * @param scalar \copybrief ILPExpressionScale::scalar
     * @param subject \copybrief ILPExpressionScale::subject
     */
    ILPExpressionScale(
        int256_t scalar,
        ILPExpression && subject
    );
    /**
     * Copy constructor
     * @param orig expression to copy
     */
    ILPExpressionScale(const ILPExpressionScale &orig);
    //! \copydoc ILPExpression::clone
    virtual std::unique_ptr<ILPExpression> clone() const;
    //! \copydoc ILPExpression::flatten
    virtual std::map<const std::string, int256_t> flatten() const override;

    int256_t scalar; //!< scalar number
    std::unique_ptr<ILPExpression> subject; //!< expression to multipy
};

/**
 * @brief Operator overload for constructing add binary operator expressions.
 *
 * We can exploit C++ overator overloading to make constructing binary operator
 * expressions easier. Say A and B are both ILP expressions, we can now do
 * (A + B) to get a new expression.
 * \param lhs left hand side
 * \param rhs right hand side
 * \return a binary ilp expression
 */
ILPExpressionBinop operator+(ILPExpression && lhs, ILPExpression && rhs);

/**
 * @brief Operator overload for constructing sub binary operator expressions.
 *
 * \copydetails operator+(ILPExpression && lhs, ILPExpression && rhs)
 */
ILPExpressionBinop operator-(ILPExpression && lhs, ILPExpression && rhs);

/**
 * @brief Operator overload for constructing scalar multiplication expressions.
 *
 * Similar to the construction methods for binary operators above, but for
 * scalar multiplication. For example, if A is an expression, we can now do
 * "2 * A".
 * 
 * \param scalar
 * \param subject 
 * \return an expression
 */
ILPExpressionScale operator*(int256_t scalar, ILPExpression && subject);

/**
 * @brief An ILP expression produced from a flat variable to coefficient map.
 *
 * In some cases, using the overloading interface described above is actually
 * a little inconvenient, for example when you need to add up a number of
 * expressions which is not known a priori. For this use case, we support a
 * flat map type, which is constructed using a mapping from ILPVariable * to
 * an integer, representing the coefficients of those variables.
 */
class ILPExpressionSum : public ILPExpression {
public:
    /**
     * Constructor
     * @param coeff \copybrief ILPExpressionFlat::values
     */
    ILPExpressionSum(std::map<ILPVariable *, int256_t> coeff);
    /**
     * Copy constructor
     * @param orig expression to copy
     */
    ILPExpressionSum(const ILPExpressionSum &orig);
    //! \copydoc ILPExpression::clone
    virtual std::unique_ptr<ILPExpression> clone() const;
    //! \copydoc ILPExpression::flatten
    virtual std::map<const std::string, int256_t> flatten() const override;

    std::map<ILPVariable *, int256_t> values; //!< list of coefficients
};

/**
 * @brief A bound within an ILP problem.
 *
 * This class represents a lower and an upper bound for a free variable in the
 * ILP problem. For example, "5 <= X <= 10" is a bound which can be represented
 * in this way.
 */
class ILPBound {
public:
    /**
     * Constructor 
     * @param subject \copybrief ILPBound::subject
     * @param lower \copybrief ILPBound::lower
     * @param upper \copybrief ILPBound::upper
     */
    ILPBound(
        ILPVariable & subject,
        int256_t lower,
        int256_t upper
    );

    ILPVariable & subject; //!< ILP variable
    int256_t lower, //!< lower bound
             upper; //!< upper bound
};

/**
 * @brief A constraint within an ILP problem.
 *
 * This class is used to represent a constraint within the ILP problem, and it
 * consists of an ILP expression (see above), a value, and a relationship
 * between the expression and that value. The relationship can either be
 * greater-or-equal, equal, or less-or-equal. For example, the constraint
 * "A + 2B >= 5" has expression "A + 2B", value 5, and operator
 * greater-or-equal.
 */
class ILPConstraint {
public:
    //! Type of the constraint
    enum class Type {
        LEQ, //!< inferior or equal
        EQ, //!< equal
        GEQ, //!< superior or equal
        GE
    };

    /**
     * Constructor 
     * @param expr \copybrief ILPConstraint::subject
     * @param type \copybrief ILPConstraint::type
     * @param value \copybrief ILPConstraint::value
     * @param name \copybrief ILPConstraint::name
     */
    ILPConstraint(
        ILPExpression && expr,
        ILPConstraint::Type type,
        int256_t value,
        std::string name = ""
    );

    /**
     * @brief Set a name for this constraint.
     *
     * Optionally, constraints can have a name. This method can be used to set
     * a name for an existing constraint if it did not have one set at time of
     * construction.
     * \param new_name
     */
    void set_name(std::string new_name);

    std::string name; //!< name of the constraint, useful for debugging
    std::unique_ptr<ILPExpression> subject; //!< the expression
    Type type; //!< type of the constaint
    int256_t value; //!< to what is compared the expression
};

/**
 * @brief Operator overloaded constructor for GEQ constraints.
 *
 * As with expressions, we can use C++'s operator overloading to make the
 * construction of constraints very natural. For example, if A is a variable,
 * we can write "A >= 5" to get a constraint for A being greater than 5.
 * \param expr the expression
 * \param value the compared value
 * \return a constraint
 */
ILPConstraint operator>=(ILPExpression && expr, int256_t value);

/**
 * @brief Operator overloaded constructor for EQ constraints.
 *
 * \copydetails operator>=(ILPExpression && expr, int256_t value)
 */
ILPConstraint operator==(ILPExpression && expr, int256_t value);

/**
 * @brief Operator overloaded constructor for LEQ constraints.
 *
 * \copydetails operator>=(ILPExpression && expr, int256_t value)
 */
ILPConstraint operator<=(ILPExpression && expr, int256_t value);

/**
 * @brief An entire ILP problem, with variables, bounds, and constraints.
 *
 * This class represents an ILP problem as a whole. It contains a bunch of
 * variables, constraints, and bounds. It is designed to be rendered into a
 * format that an ILP solver like CPLEX can use.
 */
class ILPProblem {
public:
    //! Type of the problem
    enum Objective {
        MINIMIZE, //!< minimize
        MAXIMIZE //!< maximize
    };

    /**
     * @brief Get a variable accessor from the ILP problem.
     *
     * This method is used to get a pointer (more precicely a ILPExpressionVar)
     * to a variable in the ILP problem. It can then be used in constraints,
     * for example. If the variable does not exist, it will be created.
     * \param name
     * \return an expression
     */
    ILPExpressionVar var(
        std::string name
    );
    
    /**
     * @brief Get a variable accessor from the ILP problem.
     *
     * This method is used to get a pointer (more precicely a ILPExpressionVar)
     * to a variable in the ILP problem. It can then be used in constraints,
     * for example. If the variable does not exist, it will be created.
     * \param name
     * \return an expression
     */
    ILPExpressionVar binvar(
        std::string name
    );

    /**
     * @brief Set the objective of the ILP problem.
     *
     * This method is designed to set the objective of an ILP problem. That is
     * to say, whether to minimize to maximize, and which variable.
     * \param var
     * \param goal
     */
    void set_objective(
        ILPVariable & var,
        Objective goal
    );

    /**
     * @brief Add a constraint to the ILP problem.
     *
     * This method adds a constraint to the ILP problem. It takes an r-value
     * reference as the argument, into which you can pass a value created by
     * the operator overloads described earlier. For example, you might write
     * "problem.add_constraint(A + 2 * B >= 5)", which is intuitive and easy.
     * You can also optionally name your constraint.
     * \param cons
     * \param name
     */
    void add_constraint(
        ILPConstraint && cons,
        std::string name = ""
    );

    /**
     * @brief Add a bound to the ILP problem.
     *
     * This methods binds a variable with a lower and an upper bound.
     * \param var
     * \param lower
     * \param upper
     */
    void add_bounds(
        ILPVariable & var,
        int256_t lower,
        int256_t upper
    );

    /**
     * Return the objective of the problem and variables in the objective function
     * @return 
     */
    std::pair<Objective, ILPVariable *> objective() const;

    std::map<std::string, ILPVariable> variables; //!< list of variables in the problem
    std::vector<ILPConstraint> constraints; //!< list of constraints in the problem
    std::vector<ILPBound> bounds; //!< list of boundaries in the problem
    std::vector<std::string> header_comments; //!< some comment to place in the header, useful for debugging
private:
    Objective objective_goal = Objective::MINIMIZE; //!< type of problem
    ILPVariable * objective_var = nullptr; //!< variables used in the objective function
};

#endif /* PROXYILPSOLVER_H */


/*!
 * \file CplexLP.hpp
 * \copyright GNU Public License v3.
 * \date 2020 
 * \author Stephen Nicholas Swatman <s.n.swatman@uva.nl> University of Amsterdam
 * \author Benjamin Rouxel <benjamin.rouxel@uva.nl>, University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <fstream>
#include <string>

#include <boost/filesystem.hpp>

#include "LP.hpp"
#include "Solvers/ILP/ILP.hpp"

using namespace std;

namespace ILP {
namespace Renderer {
namespace LP {
    void render_to_file(boost::filesystem::path path, const ILPProblem & problem) {
        ofstream out(path.string());
        out << ILP::Renderer::LP::render(problem);
        out.close();
    }

    string render(const ILPProblem & problem) {
        stringstream os;

        ILPProblem::Objective obj;
        ILPVariable * fun;

        tie(obj, fun) = problem.objective();

        os << "\\Energy and time is in milli seconds/joule" << std::endl;
        for (const std::string & s : problem.header_comments) {
            os << "\\" << s << std::endl;
        }

        if (obj == ILPProblem::Objective::MINIMIZE) {
            os << "minimize" << endl;
        } else {
            os << "maximize" << endl;
        }

        os << "obj: " << fun->name << endl << endl;

        os << "Subject To" << endl;

        for (const ILPConstraint & c : problem.constraints) {
            if (!c.name.empty()) {
                os << c.name << ": ";
            }

            os << render_expression(c.subject.get()) << " " << render_operator(c.type) << " " << c.value << endl;
        }

        os << std::endl << "Bounds" << std::endl;
        for (const ILPBound & c : problem.bounds) {
//            os << c.lower << " <= " << c.subject.name << " <= " << INT16_MAX << std::endl;
            os << (int32_t) c.lower << " <= " << c.subject.name << " <= " << (int32_t) c.upper << std::endl;
        }

        os << endl << "Binary" << endl;

        for (const pair<const string, ILPVariable> & p : problem.variables) {
            if (p.second.type == ILPVariable::Type::BINARY) {
                os << p.first << endl;
            }
        }

        os << endl << "Generals" << endl;

        for (const pair<const string, ILPVariable> & p : problem.variables) {
            if (p.second.type == ILPVariable::Type::GENERAL) {
                os << p.first << endl;
            }
        }

        os << endl << "End" << endl;

        return os.str();
    }

    string render_expression(const ILPExpression * expr) {
        map<const string, int256_t> flat = expr->flatten();
        string res = "";
        bool first = true;

        for (pair<const string, int256_t> & p : flat) {
            int256_t v = p.second;

            if (first) {
                first = false;
                res += v.convert_to<string>() + " " + p.first;
            } else {
                if (p.second > 0) {
                    res += " + ";
                } else {
                    res += " - ";
                    v *= -1;
                }

                if (v != 1) {
                    res += v.convert_to<string>() + " ";
                }

                res += p.first;
            }
        }

        return res;
    }

    string render_operator(const ILPConstraint::Type o) {
        switch (o) {
            case ILPConstraint::Type::EQ:
                return "=";
            case ILPConstraint::Type::GEQ:
                return ">=";
            case ILPConstraint::Type::GE:
                return ">";
            case ILPConstraint::Type::LEQ:
            default:
                return "<=";
        }
    }
}
}
}
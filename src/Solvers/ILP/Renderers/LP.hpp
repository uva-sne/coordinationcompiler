/*!
 * \file CplexLP.hpp
 * \copyright GNU Public License v3.
 * \date 2020 
 * \author Stephen Nicholas Swatman <s.n.swatman@uva.nl> University of Amsterdam
 * \author Benjamin Rouxel <benjamin.rouxel@uva.nl>, University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ILP_RENDERER_CPLEXLP
#define ILP_RENDERER_CPLEXLP

#include <boost/filesystem.hpp>

#include "Solvers/ILP/ILP.hpp"

namespace ILP {
namespace Renderer {
namespace LP {
    /**
     * Export the ILP problem to a file
     * @param path where to store the file
     * @param problem the ilp problem to export
     */
    void render_to_file(boost::filesystem::path path, const ILPProblem & problem);

    /**
     * Export the problem into a string
     * @param problem
     * @return string of the problem
     */
    std::string render(const ILPProblem & problem);

    /**
     * Export an expresion into a string
     * @param expr
     * @return string
     */
    std::string render_expression(const ILPExpression * expr);

    /**
     * Export an operator into a string
     * @param o
     * @return string
     */
    std::string render_operator(ILPConstraint::Type o);
}
}
}

#endif
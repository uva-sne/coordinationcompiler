/*!
 * \file Cplex.hpp
 * \copyright GNU Public License v3.
 * \date 2017
 * \author Benjamin Rouxel <benjamin.rouxel@uva.nl>, University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef CPLEX

#include "Cplex.hpp"
#include "Solvers/ILP/Renderers/LP.hpp"

#include <ilcplex/ilocplex.h>
#include <ilcplex/cplex.h>
#include <ilconcert/ilotupleset.h>
#include <ilconcert/ilosys.h>

using namespace std;

Cplex::Cplex() : ILPSolver() {}

IMPLEMENT_HELP_WITH_CODE(Cplex, {msg += ILPSolver::help();},
    CPLEX Solver  https:/ /www.ibm.com/analytics/cplex-optimizer:\n
    cores [int], default 0: number of cores the solver should use, 
    set to 0 to use all available cores\n\n
    @see ILPSolver::help
)

void Cplex::forward_params(const map<string, string> &args) {
    ILPSolver::forward_params(args);
    
    if(args.count("cores"))
        cores = stoul(args.at("cores"));
}
const string Cplex::get_uniqid_rtti() const { return "solver-ilpsolver-cplex"; }

void Cplex::solve(Schedule *result) {
    boost::filesystem::path filename = conf->files.output_folder / conf->files.coord_basename;
    filename += ".lp";
    boost::filesystem::path paramfile = filename;
    paramfile += ".param";
    boost::filesystem::path solfile = filename;
    solfile += ".sol";

    ILP::Renderer::LP::render_to_file(filename, generator->problem());

    IloEnv env;
    IloModel model(env);
    IloCplex cplex(env);
    IloNumVarArray var(env);
    try {
        IloObjective   obj;
        IloRangeArray  rng(env);

        cplex.importModel(model, filename.c_str(), obj, var, rng);
        cplex.extract(model);
        
        if(clean)
            unlink(filename.c_str());
        unlink(solfile.c_str());
        
        if(boost::filesystem::exists(paramfile))
            cplex.readParam(paramfile.c_str());
        
        IloCplex::ParameterSet paramset(env);
        paramset = cplex.getParameterSet();
        
        if(timeout() > 0) {
            timeout(timeout() / 1000000000); //timeout is stored in ns
            cplex.setParam(IloCplex::NumParam::TiLim, timeout());
            cplex.setParam(IloCplex::NumParam::TuningTiLim, timeout()/2);
        paramset.setParam(IloCplex::Param::MIP::Tolerances::MIPGap, 0.0);
        paramset.setParam(IloCplex::Param::MIP::Tolerances::AbsMIPGap, 0.0);
        }
        
        if(cores > 0) {
            cplex.setParam(IloCplex::IntParam::Threads, cores);
        }
        
        // Tunning time do not account for number of threads
        /*if(tg.tasks().size() > 15) {
            cplex.setParam(IloCplex::IntParam::TuningDisplay, 1);
            cplex.setParam(IloCplex::IntParam::TuningRepeat, 1);

//            IloCplex::TuneParamHandle handler = cplex.tuneParam(paramset, true);
//            handler.joinTuneParam();
            cplex.tuneParam(paramset);
        }*/
        Utils::INFO("Solver timeout: " + to_string(timeout()) + " seconds");
        Utils::INFO("Solver used cores: " + to_string(cores) + " cores");
        
        cplex.setParam(IloCplex::IntParam::DataCheck, 2);
        cplex.setParam(IloCplex::BoolParam::PreInd, 0);
        
        cplex.solve();


        tmp_results.clear();
        uint32_t nbvars = var.getSize();
        for(uint i=0 ; i < nbvars ; ++i) {
            tmp_results.insert({var[i].getName(), round(cplex.getValue(var[i]))});
        }

        result->makespan((uint128_t)round(cplex.getObjValue()));
        if(cplex.getStatus() == IloAlgorithm::Status::Optimal)
            result->status(ScheduleProperties::sched_statue_e::SCHED_COMPLETE);
        else if(cplex.getStatus() == IloAlgorithm::Status::Feasible)
            result->status(ScheduleProperties::sched_statue_e::SCHED_PARTIAL);
        Utils::INFO("solution status is " + to_string((cplex.getStatus() == IloAlgorithm::Status::Optimal)) + " - " + to_string(cplex.getStatus()));
        Utils::INFO("solution value  is " + to_string(cplex.getObjValue()));
        Utils::INFO("Gap is " + to_string(cplex.getMIPRelativeGap()*100));

        if(!clean) {
            cplex.writeParam(paramfile.c_str());
            cplex.writeSolution(solfile.c_str(), 0);
        }
        env.end();
    }
    catch(IloException& e) {
        if(cplex.getStatus() == IloAlgorithm::Status::Infeasible) {
            result->makespan(-1);
            result->status(ScheduleProperties::sched_statue_e::SCHED_UNSCHEDULABLE);
            env.end();
        }
        else {
            //cplex.exportModel(filename.c_str());
            //cplex.writeParam(paramfile.c_str());
            env.end();
            throw CompilerException("solver", e.getMessage());
        }
    }
}

#endif //CPLEX

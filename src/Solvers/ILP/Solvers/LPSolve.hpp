/*!
 * \file LPSolve.hpp
 * \copyright GNU Public License v3.
 * \date 2020 
 * \author Stephen Nicholas Swatman <s.n.swatman@uva.nl> University of Amsterdam
 * \author Benjamin Rouxel <benjamin.rouxel@uva.nl>, University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LPSOLVE_H
#define LPSOLVE_H

#include "Solvers/ILP/ILP.hpp"
#include "ILPSolver.hpp"

#ifdef ILP_SOLVER_LPSOLVE
#include <lpsolve/lp_lib.h>

#include <lpsolve/lp_lib.h>

/**
 * \brief Interface to the LP solve solver
 * 
 * https://sourceforge.net/projects/lpsolve/
 */
class LPSolve : public ILPSolver {
public:
    //! Constructor
    explicit LPSolve();
    //! \copydoc CompilerPass::get_uniqid_rtti
    const std::string get_uniqid_rtti() const override;
    //! \copydoc CompilerPass::help
    virtual std::string help() override;
protected:
    //! \copydoc Solver::solve
    void solve(Schedule *result) override;
private:
    /**
     * Add a constraint to the problem
     * @param cons contraint to add
     * @param lp lp library
     * @param colmap column mapping
     */
    void add_constraint(ILPConstraint & cons, lprec & lp, std::map<const std::string, int> & colmap);

    /**
     * @brief Translate from internal ILP constraint types to lp_solve API
     * constraint types.
     * \param t internal constraint type
     * \return lp_solve API constraint id
     */
    int constraint_type(ILPConstraint::Type t) ;
};

REGISTER_SOLVER(LPSolve, "lp_solve")

#endif
#endif

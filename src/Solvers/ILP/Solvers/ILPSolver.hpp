/*!
 * \file ILPSolver.hpp
 * \copyright GNU Public License v3.
 * \date 2017
 * \author Benjamin Rouxel <benjamin.rouxel@uva.nl>, University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ILPSOLVER_H
#define ILPSOLVER_H

#include "Solvers/ILP/Strategies/BaseILPStrategy.hpp"
#include "Solvers/Solver.hpp"

/**
 * Base class for ILP solvers
 */
class ILPSolver : public Solver {
protected:
    bool optimal = true; //!< Should we look for the optimal solution or just a solution
    bool clean = false; //!< should delete temporary files
    std::map<std::string, long long> tmp_results; //!< Store temporary results before post-processing

    BaseILPStrategy * generator; //!< the ILP form generator that will encode the problem

public:
    //! constructor
    explicit ILPSolver() : Solver() {};
    //! \copydoc CompilerPass::setParams
    virtual void forward_params(const std::map<std::string, std::string> &args) override;
    //! \copydoc CompilerPass::help
    virtual std::string help() override;
    
    virtual std::string more_help(const std::string &key) override;
    
    /*! \copydoc Solver::presolve
     * Call the ILPFormGen to create the LP problem file
     */
    virtual void presolve(Schedule *sched) override;
    /*! \copydoc Solver::postsolve
     * From the result of the solver, populate the schedule
     */
    virtual void postsolve(Schedule *sched) override;
};

#endif
/*!
 * \file LPSolve.cpp
 * \copyright GNU Public License v3.
 * \date 2020 
 * \author Stephen Nicholas Swatman <s.n.swatman@uva.nl> University of Amsterdam
 * \author Benjamin Rouxel <benjamin.rouxel@uva.nl>, University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifdef ILP_SOLVER_LPSOLVE

#include "LPSolve.hpp"
#include "Solvers/ILP/Renderers/LP.hpp"


using namespace std;

int LPSolve::constraint_type(ILPConstraint::Type t) {
    switch (t) {
        case ILPConstraint::Type::GEQ:
            return GE;
        case ILPConstraint::Type::LEQ:
            return LE;
        default:
            return EQ;
    }
}

LPSolve::LPSolve() : ILPSolver() {}

const std::string LPSolve::get_uniqid_rtti() const {
    return "solver-ilpsolver-lpsolve";
}

IMPLEMENT_HELP_WITH_CODE(LPSolve, {msg += ILPSolver::help();},
    Cplex Solve: https:/ /sourceforge.net/projects/lpsolve/\n\n
    @see ILPSolver::help
)

void LPSolve::solve(Schedule *result) {
    ILPProblem & problem = generator->problem();

    /*
     * Dump the problem to disk in CPLEX LP format.
     */
    boost::filesystem::path filename = conf->files.output_folder / conf->files.coord_basename;
    filename += ".lp";
    ILP::Renderer::LP::render_to_file(filename, problem);

    ILPProblem::Objective obj;
    ILPVariable * fun;
    std::tie(obj, fun) = problem.objective();

    /*
     * Construct a mapping from variable names to column indices in our matrix,
     * and also count the total number of columns.
     */
    std::map<const std::string, int> colmap;
    int numcol = 0;

    for (std::pair<const std::string, ILPVariable> & p : problem.variables) {
        colmap[p.first] = ++numcol;
    }

    /*
     * Initialize the LP model and configure some of its parameters.
     */
    lprec *lp;
    lp = make_lp(0, numcol);
#ifdef NDEBUG
    set_verbose(lp, IMPORTANT);
#else
    set_verbose(lp, FULL);
#endif
    if(timeout() > 0) {
        set_timeout(lp, timeout()/1000000000); // timeout is in ns;
    }
    set_scaling(lp, SCALE_GEOMETRIC | SCALE_POWER2 | SCALE_EQUILIBRATE | SCALE_INTEGERS | SCALE_DYNUPDATE);
    set_presolve(lp, PRESOLVE_ROWS | PRESOLVE_COLS | PRESOLVE_LINDEP | PRESOLVE_ELIMEQ2 | PRESOLVE_SOS  | PRESOLVE_ROWDOMINATE | PRESOLVE_COLDOMINATE | PRESOLVE_IMPLIEDFREE | PRESOLVE_REDUCEGCD | PRESOLVE_PROBEFIX | PRESOLVE_BOUNDS , get_presolveloops(lp));
    
    /*
     * Set the name and type for each of the columns in our LP problem.
     */
    for (std::pair<const std::string, ILPVariable> & p : problem.variables) {
        set_col_name(lp, colmap[p.first], const_cast<char*>(p.first.c_str()));

        if (p.second.type == ILPVariable::Type::GENERAL) {
            set_int(lp, colmap[p.first], true);
        } else {
            set_binary(lp, colmap[p.first], true);
        }
    }
    
    /*
     * Set the function to optimize and whether to minimize or maximize it.\
     */
    if (obj == ILPProblem::Objective::MAXIMIZE) {
        set_maxim(lp);
    } else {
        set_minim(lp);
    }
    
    int obj_col[1] = {colmap.at(fun->name)};
    double obj_val[1] = {1.0};
    set_obj_fnex(lp, 1, obj_val, obj_col);

    /*
     * Add each of the constraints in our model, using our helper function.
     */
    for (ILPConstraint & c : problem.constraints) {
        add_constraint(c, *lp, colmap);
    }

    /*
     * Add our bounds to the model.
     */
    for (ILPBound & b : problem.bounds) {
        set_bounds(
            lp, colmap[b.subject.name],
            b.lower.convert_to<double>(), b.upper.convert_to<double>()
        );
    }

    /*
     * Solve our ILP problem using lp_solve, then handle the possible return
     * values.
     */
    int numrow = get_Nrows(lp); //get the numrow before solving as the presolve will change it (for primal results)
    int ret = ::solve(lp);

    if (ret == OPTIMAL) {
        Utils::INFO("Model is FEASIBLE, and result is OPTIMAL!");
        result->status(ScheduleProperties::sched_statue_e::SCHED_COMPLETE);
    } else if (ret == SUBOPTIMAL) {
        Utils::INFO("Model is FEASIBLE, but result is SUBOPTIMAL.");
        result->status(ScheduleProperties::sched_statue_e::SCHED_PARTIAL);
    } else if (ret == INFEASIBLE) {
        Utils::INFO("Model is INFEASIBLE!");
        result->status(ScheduleProperties::sched_statue_e::SCHED_UNSCHEDULABLE);
    }
    else if (ret == TIMEOUT) {
        Utils::INFO("Solver timed-out!");
        delete_lp(lp);
        return;
    } else {
        throw std::runtime_error("Unexpected return value from lp_solve! => "+to_string(ret));
    }
    
    // gather evicted variables by the presolve step
    for (std::pair<const std::string, int> & p : colmap) {
        tmp_results[p.first] = (long long) get_var_primalresult(lp, numrow+p.second);
    }
    
    /*
     * Finally, extract the values for all our variables as well as the value
     * of our objective function and insert them into our result list. The
     * postsolve method will then add these results into the schedule.
     */
    double * outrow = new double[numcol+1];
    get_variables(lp, outrow);
    
    // variables are be at the same column num after solving than it was when creating the problem
    for (int i=0 ; i < get_Ncolumns(lp) ; ++i) {
        char *tmp = get_col_name(lp, i+1); //column starts at 1 not 0
        if(tmp == NULL) continue;
        tmp_results[string(tmp)] = (long long) 	outrow[i];
    }

    Utils::INFO("Objective value: " + to_string(get_objective(lp)));
    result->makespan((uint128_t) round(get_objective(lp)));
    
    delete outrow;
    delete_lp(lp);
}

void LPSolve::add_constraint(ILPConstraint & cons, lprec & lp, std::map<const std::string, int> & colmap) {
    std::vector<int> indices;
    std::vector<double> values;

    /*
     * Construct a sparse row vector using two std::vectors which will be of
     * the same size.
     */
    std::map<const std::string, int256_t> flat = cons.subject->flatten();

    for (std::pair<const std::string, int256_t> & p : flat) {
        indices.push_back(colmap.at(p.first));
        values.push_back(p.second.convert_to<double>());
    }

    /*
     * Construct a constraint from our vectors, which is allowed because vector
     * memory is guaranteed to be consecutive.
     */
    add_constraintex(
        &lp, indices.size(), &values[0], &indices[0],
        constraint_type(cons.type), cons.value.convert_to<double>()
    );

    /*
     * If our constraint has a name, then we add a name to the row in the LP
     * model as well.
     */
    if (!cons.name.empty()) {
        char tmp_name[512];
        strncpy(tmp_name, cons.name.c_str(), 511);
        set_row_name(&lp, get_Nrows(&lp), tmp_name);
    }
}
#endif

/*!
 * \file ILPSolver.hpp
 * \copyright GNU Public License v3.
 * \date 2017
 * \author Benjamin Rouxel <benjamin.rouxel@uva.nl>, University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "ILPSolver.hpp"
#include "ScheduleFactory.hpp"

using namespace std;

IMPLEMENT_HELP_WITH_CODE(ILPSolver, SINGLE_ARG({
    std::string types = "";
    for(ILPStrategyRegistry::key_t t : ILPStrategyRegistry::keys())
        types += ((std::string)t)+", ";
    types = types.substr(0, types.size()-2);
        
    msg = "Integer Linear Programming Solver:\n"
            "clean ["+string(CONFIG_FALSE)+"-"+CONFIG_TRUE+"], default "+
            CONFIG_TRUE+": should the temporary files be cleaned\n"
            "optimal ["+string(CONFIG_FALSE)+"-"+CONFIG_TRUE+"], default "+
            CONFIG_TRUE+": should it search for an optimal solution, or just a solution\n"
            "type ["+types+"]: type of scheduling strategy\n\n"
            +Solver::help();
}),
   @see Solver::help
) 

string ILPSolver::more_help(const string &key) {
    if(ILPStrategyRegistry::IsRegistered(key))
        return ILPStrategyRegistry::Create(key, *tg, *conf, nullptr, false)->help();
    return "Unknown ILP scheduling policy strategy `"+key+"'\n";
}

void ILPSolver::forward_params(const std::map<std::string, std::string> &args) {
    Solver::forward_params(args);

    if(args.count("optimal"))
        optimal = (args.at("optimal") == CONFIG_TRUE);
    if(args.count("clean"))
        clean = (args.at("clean") == CONFIG_TRUE);
}

void ILPSolver::presolve(Schedule *sched) {
    if(type().empty())
        throw CompilerException("ilpsolver", "You need to precise what type of solver you want, (check doc?)");

    for (ComputeUnit *p : tg->processors())
        ScheduleFactory::add_core(sched, p);
    ScheduleFactory::build_elements(sched);

    generator = ILPStrategyRegistry::Create(type(), *tg, *conf, sched, optimal);
    generator->set_allow_migration(allow_migration);
    generator->gen();
}

void ILPSolver::postsolve(Schedule *result) {
    for (const std::pair<std::string, long long> &el : tmp_results) {
        if(el.first.find("tmp_") == string::npos)
            Utils::DEBUG(el.first + " : " + to_string(el.second));
    }
    
    generator->postsolve(tmp_results);

    result->compute_makespan();
    result->compute_energy();
    result->mapping_mode(ScheduleProperties::Mapping::MAP_PARTITIONED);
    result->priority_mode(ScheduleProperties::PriorityScheme_e::PRIO_FIXED);
    result->preemption_mode(ScheduleProperties::PreemptionModel::PRE_NON);

    Utils::INFO(""+*result);
    Utils::INFO("\t====> Final Schedule length = "+to_string(result->makespan()) + tg->time_unit());
    Utils::INFO("\t====> Final Energy Consumption = "+to_string(result->energy()) + tg->energy_unit());
}
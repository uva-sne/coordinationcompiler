/*!
 * \file Cplex.hpp
 * \copyright GNU Public License v3.
 * \date 2017
 * \author Benjamin Rouxel <benjamin.rouxel@uva.nl>, University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CPLEX_H
#define CPLEX_H

#include "Solvers/ILP/ILP.hpp"
#include "ILPSolver.hpp"

#ifdef CPLEX

/**
 * \brief Interface to the CPLEX solver
 * 
 * https://www.ibm.com/analytics/cplex-optimizer
 */
class Cplex : public ILPSolver {
    size_t cores = 0; //!< number of cores the solver is able to use
    
public:
    //! Constructor
    explicit Cplex();
    //! \copydoc CompilerPass::setParams
    void forward_params(const std::map<std::string, std::string> &args) override;
    //! \copydoc CompilerPass::help
    virtual std::string help() override;
    //! \copydoc CompilerPass::get_uniqid_rtti
    const std::string get_uniqid_rtti() const override;
protected:
    //! \copydoc Solver::solve
    void solve(Schedule *result) override;
};


REGISTER_SOLVER(Cplex, "cplex")
#endif //CPLEX

#endif

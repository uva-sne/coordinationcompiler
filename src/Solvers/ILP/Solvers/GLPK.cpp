/*!
 * \file GLPK.hpp
 * \copyright GNU Public License v3.
 * \date 2020 
 * \author Stephen Nicholas Swatman <s.n.swatman@uva.nl> University of Amsterdam
 * \author Benjamin Rouxel <benjamin.rouxel@uva.nl>, University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef ILP_SOLVER_GLPK

#include <glpk.h>

#include "GLPK.hpp"
#include "Solvers/ILP/ILP.hpp"
#include "Solvers/ILP/Renderers/LP.hpp"

GLPK::GLPK() : ILPSolver() {}

const std::string GLPK::get_uniqid_rtti() const {
    return "solver-ilpsolver-glpk";
}

IMPLEMENT_HELP_WITH_CODE(GLPK, SINGLE_ARG({msg += ILPSolver::help();}),
    GLPK Solve: www.gnu.org/software/glpk/\n\n
    @see ILPSolver::help
)

void GLPK::solve(Schedule *result) {
    ILPProblem & problem = generator->problem();
    
    /*
     * Dump the problem to disk in CPLEX LP format.
     */
    boost::filesystem::path filename = conf->files.output_folder / conf->files.coord_basename;
    filename += ".lp";
    ILP::Renderer::LP::render_to_file(filename, problem);

    ILPProblem::Objective obj;
    ILPVariable * fun;
    std::tie(obj, fun) = problem.objective();

    /*
     * Construct a mapping from variable names to column indices in our matrix,
     * and also count the total number of columns.
     */
    std::map<const std::string, int> colmap;
    int numcol = 0;

    for (std::pair<const std::string, ILPVariable> & p : problem.variables) {
        colmap[p.first] = ++numcol;
    }

    /*
     * Initialize the LP model and configure some of its parameters.
     */
    glp_prob * lp = glp_create_prob();
    glp_add_rows(lp, problem.constraints.size());
    glp_add_cols(lp, numcol);

    /*
     * Set the name and type for each of the columns in our LP problem.
     */
    for (std::pair<const std::string, ILPVariable> & p : problem.variables) {
        char tmp_name[512];
        strncpy(tmp_name, p.first.c_str(), 511);
        glp_set_col_name(lp, colmap[p.first], tmp_name);
        glp_set_col_bnds(lp, colmap[p.first], GLP_FR, 0, 0);

        if (p.second.type == ILPVariable::Type::GENERAL) {
            glp_set_col_kind(lp, colmap[p.first], GLP_IV);
        } else {
            glp_set_col_kind(lp, colmap[p.first], GLP_BV);
        }
    }

    /*
     * Add each of the constraints in our model, using our helper function.
     */
    int j = 0;
    for (ILPConstraint & c : problem.constraints) {
        add_constraint(c, *lp, colmap, ++j);
    }

    /*
     * Add our bounds to the model.
     */
    for (ILPBound & b : problem.bounds) {
        glp_set_col_bnds(
            lp, colmap[b.subject.name], GLP_DB,
            b.lower.convert_to<double>(), b.upper.convert_to<double>()
        );
    }

    /*
     * Set the function to optimize and whether to minimize or maximize it.\
     */
    if (obj == ILPProblem::Objective::MAXIMIZE) {
        glp_set_obj_dir(lp, GLP_MAX);
    } else {
        glp_set_obj_dir(lp, GLP_MIN);
    }

    glp_set_obj_coef(lp, colmap[fun->name], 1.0);
    
    /*
     * Solve our ILP problem using lp_solve, then handle the possible return
     * values.
     */
    glp_smcp param1;
    glp_init_smcp(&param1);
    param1.presolve = 1;
    
    glp_iocp param;
    glp_init_iocp(&param);
    if(timeout() > 0)
        param.tm_lim = timeout() / 1000000; //time limit is in milliseconds
    
    param.br_tech = GLP_BR_DTH;
    param.bt_tech = GLP_BT_BLB;
    param.pp_tech = GLP_PP_ALL;
    
    glp_simplex(lp, &param1);
    glp_intopt(lp, &param);
    int ret = glp_get_status(lp);

    if (ret == GLP_OPT) {
        Utils::INFO("Model is FEASIBLE, and result is OPTIMAL!");
        result->status(ScheduleProperties::sched_statue_e::SCHED_COMPLETE);
    } else if (ret == GLP_FEAS) {
        Utils::INFO("Model is FEASIBLE, but result is SUBOPTIMAL.");
        result->status(ScheduleProperties::sched_statue_e::SCHED_PARTIAL);
    } else {
        Utils::INFO("Model or result is INFEASIBLE!");
        result->status(ScheduleProperties::sched_statue_e::SCHED_UNSCHEDULABLE);
    }

    /*
     * Finally, extract the values for all our variables as well as the value
     * of our objective function and insert them into our result list. The
     * postsolve method will then add these results into the schedule.
     */
    for (std::pair<const std::string, int> & p : colmap)
        tmp_results[p.first] = glp_mip_col_val(lp, colmap[p.first]);

    Utils::INFO("Objective value: " + to_string(glp_mip_obj_val(lp)));
    result->makespan((uint128_t) round(glp_mip_obj_val(lp)));
    
    glp_write_sol(lp, "/tmp/plop.sol");

    glp_delete_prob(lp);
}

void GLPK::add_constraint(ILPConstraint & cons, glp_prob & lp, std::map<const std::string, int> & colmap, int j) const {
    std::vector<int> indices;
    std::vector<double> values;

    indices.push_back(0);
    values.push_back(0);

    /*
     * Construct a sparse row vector using two std::vectors which will be of
     * the same size.
     */
    std::map<const std::string, int256_t> flat = cons.subject->flatten();

    for (std::pair<const std::string, int256_t> & p : flat) {
        indices.push_back(colmap.at(p.first));
        values.push_back(p.second.convert_to<double>());
    }

    /*
     * Construct a constraint from our vectors, which is allowed because vector
     * memory is guaranteed to be consecutive.
     */
    glp_set_mat_row(&lp, j, indices.size() - 1, &indices[0], &values[0]);

    if (cons.type == ILPConstraint::Type::GEQ) {
        glp_set_row_bnds(&lp, j, GLP_LO, cons.value.convert_to<double>(), 0);
    } else if (cons.type == ILPConstraint::Type::LEQ) {
        glp_set_row_bnds(&lp, j, GLP_UP, 0, cons.value.convert_to<double>());
    } else {
        glp_set_row_bnds(&lp, j, GLP_FX, cons.value.convert_to<double>(), 0);
    }

    /*
     * If our constraint has a name, then we add a name to the row in the LP
     * model as well.
     */
    if (!cons.name.empty()) {
        char tmp_name[512];
        strncpy(tmp_name, cons.name.c_str(), 511);
        glp_set_row_name(&lp, j, tmp_name);
    }
}
#endif

/* 
 * Copyright (C) 2017 Benjamin Rouxel <benjamin.rouxel@inria.fr> 
 *                    Irisa/INRIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef FORM_IDEAL_H
#define FORM_IDEAL_H

#include "Solvers/ILP/ILP.hpp"
#include "BaseILPCommunicationAware.hpp"

/**
 * \brief Generate an ILP problem to schedule an application
 * 
 * Communication between tasks are assumed to be ideal, meaning that there is 
 * never an interference
 * 
 */
class ILPIdeal : public BaseILPCommunicationAware {
public:
    //! \copydoc ILPFormGen::ILPFormGen
    explicit ILPIdeal(const SystemModel &m, const config_t &c, Schedule *s, bool o);
    
    virtual std::string help() override;
    
    //! \copydoc ILPFormGen::gen
    virtual void gen() override;
    
protected:
    //! \copydoc ILPFormGen::communication_delay
    void communication_delay() override;
};

REGISTER_ILPSTRATEGY(ILPIdeal, "ideal");

#endif
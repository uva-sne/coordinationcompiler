/*
    Copyright (C) 2017  Benjamin Rouxel, Irisa/INRIA, benjamin.rouxel@inria.fr

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "CommContentionAware.hpp"

using namespace std;

IMPLEMENT_HELP(ILPContentionAware,
    Generate an ILP problem to schedule an application\n
    \n
    Communication between tasks are assumed to always imply the worst possible
    never an interference
)

ILPContentionAware::ILPContentionAware(const SystemModel &m, const config_t &c, Schedule *s, bool o) : BaseILPCommunicationAware(m, c, s, o) {}

void ILPContentionAware::gen() {
    if(conf.archi.interconnect.burst != "none") 
        throw CompilerException("ILPContentionAware", "Has never been ported to handle burst communication");
    
    SeqTauMax = (int64_t)tg.global_deadline();
    if(SeqTauMax == 0 || (int64_t)tg.sequential_length() < SeqTauMax)
        SeqTauMax = (int64_t)tg.sequential_length();

    timinginfos_t lastiter = 0;
    for(SchedJob *t : sched->schedjobs()) {
        if(t->min_rt_period() > lastiter)
            lastiter = t->min_rt_period();
    }
    SeqTauMax += lastiter;

    add_header_comments();
    objectiv_func();

    unicity();
    detect_samecore();
    decide_task_order();
    decide_task_ordering_percore();
    conflicts();
    if(conf.runtime.rtt == runtime_task_e::SINGLEPHASE)
        single_phase();
    else
       _3_phases();
    wcet();
    causality();
    communication_delay();

    if(conf.archi.interconnect.behavior == "non-blocking") {
        decide_communications_order();
        decide_communications_order_percore();
        conflicts_communications();
        causality_communications();
    }

    detect_communication();
    detect_overlapping();
    contention();

//    if(conf.archi.spm.assign_region)
//        map_spm_region_to_task();

    force_mapping();

    multiperiod();

    linkObj();

    add_bounds(SeqTauMax);
    add_communication_bounds(SeqTauMax);
    for(SchedJob *t : sched->schedjobs()) {
        _problem.add_bounds(_problem.var("conc_r_"+*t).var, 0, tg.processors().size()-1);
        _problem.add_bounds(_problem.var("conc_w_"+*t).var, 0, tg.processors().size()-1);
    }
    if(conf.archi.interconnect.mechanism != "sharedonly") {
        for (SchedJob *t : sched->schedjobs()) {
            if(_problem.variables.count("remdata_r_" + *t))
                _problem.add_bounds(_problem.var("remdata_r_" + *t).var, 0, tg.interconnect()->getActiveWindowTime()-1);
            if(_problem.variables.count("remdata_w_" + *t))
                _problem.add_bounds(_problem.var("remdata_w_" + *t).var, 0, tg.interconnect()->getActiveWindowTime()-1);
            if(_problem.variables.count("unused_r_" + *t))
                _problem.add_bounds(_problem.var("unused_r_" + *t).var, 0, tg.interconnect()->getActiveWindowTime()-1);
            if(_problem.variables.count("unused_w_" + *t))
                _problem.add_bounds(_problem.var("unused_w_" + *t).var, 0, tg.interconnect()->getActiveWindowTime()-1);
        }
    }
}
    
void ILPContentionAware::communication_delay() {
    if(conf.runtime.rtt == runtime_task_e::SINGLEPHASE) return;

    if(conf.archi.interconnect.mechanism == "sharedonly") {
        map<ILPVariable *, int256_t> mapping;
        for(SchedElt *ep : sched->schedpackets()) {
            SchedPacket *p = (SchedPacket*) ep;
            packet_comm_delay(p, mapping, p->data());
        }
    }
    else {
        for(SchedJob *t : sched->schedjobs()) {
            SchedPacket *rpac = t->packet_list(0);
            if(rpac != nullptr && rpac->data() > 0) {
                uint64_t sum = 0;
                map<ILPVariable *, int256_t> mapping;
                for(SchedElt *tmpel : rpac->previous()) {
                    SchedJob *el = tmpel->schedtask();
                    uint32_t data = t->task()->data_read(el->task());
                    mapping[&_problem.var("m_" + *el + "_" + *t).var] = data;
                    sum += data;
                }

                packet_comm_delay(rpac, mapping, sum);
            }
            else {
                _problem.add_constraint(_problem.var("delay_r_" + *t) == 0, "DelayR" + *t);
                _problem.add_constraint(_problem.var("DR_"+*t) == 0, "Data" + *t);
            }

            SchedPacket *wpac = t->packet_list(1);
            if(wpac != nullptr && wpac->data() > 0) {
                uint64_t sum = 0;
                map<ILPVariable *, int256_t> mapping;
                for(SchedElt *tmpel : wpac->successors()) {
                    SchedJob *el = tmpel->schedtask();
                    uint32_t data = t->task()->data_written(el->task());
                    mapping[&_problem.var("m_" + *el + "_" + *t).var] = data;
                    sum += data;
                }

                packet_comm_delay(wpac, mapping, sum);
            }
            else {
                _problem.add_constraint(_problem.var("delay_w_" + *t) == 0, "DelayW" + *t);
                _problem.add_constraint(_problem.var("DW_"+*t) == 0, "Data" + *t);
            }
        }
    }
}
    
void ILPContentionAware::packet_comm_delay(SchedPacket *p, const std::map<ILPVariable *, int256_t> &mapping, uint64_t pdata) {
    string Dvar = "D"+boost::to_upper_copy(p->dirlbl())+"_"+*p;
    if(pdata == 0) {
        _problem.add_constraint(
            _problem.var("delay_"+p->dirlbl()+"_" + *p) == 0,
            "DelayR" + *p
        );
        _problem.add_constraint(_problem.var(Dvar) == 0, "Data" + *p);
    }
    else if(p->schedtask()->task()->force_read_delay() > 0) {
        _problem.add_constraint(
            _problem.var("delay_"+p->dirlbl()+"_" + *p) == p->schedtask()->task()->force_read_delay(),
            "DelayR" + *p
        );
        _problem.add_constraint(_problem.var(Dvar) == 0, "Data" + *p);
    }
    else if(conf.archi.interconnect.mechanism == "sharedonly") {
        _problem.add_constraint(
            _problem.var("delay_"+p->dirlbl()+"_" + *p)
            - (conf.archi.interconnect.active_ress_time * (uint32_t)ceil(pdata/(float)conf.archi.interconnect.active_ress_time)) * _problem.var("conc_"+p->dirlbl()+"_"+*p->schedtask())
            == tg.interconnect()->getActiveWindowTime() * (uint32_t)floor(pdata/(float)conf.archi.interconnect.active_ress_time) 
                                + (pdata % conf.archi.interconnect.active_ress_time),
            "DelayR" + *p
        );
        _problem.add_constraint(_problem.var(Dvar) == pdata, "Data" + *p);
    }
    else {
        uint64_t data = pdata * conf.datatypes.at(p->datatype()).size_bits;
        data = ceil(data/conf.archi.interconnect.bit_per_timeunit); // probably + (bit_per_timeunit - (sum % bit_per_timeunit)) to add extra bit if data not aligned, e.g. send 5 bits when 8 bits are sent at a time, need to add 3 zeroes

        // Should be Dvar + mapping for shared stuff
        _problem.add_constraint(_problem.var(Dvar) + ILPExpressionSum(mapping) == data, "Data" + *p);
        _problem.add_constraint(
            _problem.var(Dvar) - tg.interconnect()->getActiveWindowTime() * _problem.var("trslot_"+p->dirlbl()+"_" + *p) - _problem.var("remdata_"+p->dirlbl()+"_" + *p) == 0,
            "TRSlot"+p->dirlbl() + *p
        );
        _problem.add_constraint(
            _problem.var(Dvar) - tg.interconnect()->getActiveWindowTime() * _problem.var("waitslot_"+p->dirlbl()+"_" + *p) + _problem.var("unused_"+p->dirlbl()+"_" + *p) == 0,
            "WSlot"+p->dirlbl() + *p
        );
        _problem.add_constraint(
            _problem.var("unused_"+p->dirlbl()+"_" + *p) - _problem.var("remdata_"+p->dirlbl()+"_" + *p) <= 1,
            "Round"+p->dirlbl() + *p
        );
        _problem.add_constraint(
            _problem.var("delay_"+p->dirlbl()+"_" + *p) 
            - (conf.archi.interconnect.active_ress_time * (uint32_t)ceil(data/(float)conf.archi.interconnect.active_ress_time)) * _problem.var("conc_"+p->dirlbl()+"_"+*p->schedtask())
            - tg.interconnect()->getActiveWindowTime() * _problem.var("trslot_"+p->dirlbl()+"_" + *p)
            - _problem.var("remdata_"+p->dirlbl()+"_" + *p) == 0,
            "Delay"+p->dirlbl() + *p
        );
    }
    
    _problem.add_constraint(
        _problem.var("delay_"+p->dirlbl()+"_" + *p) - _problem.var("delay_"+p->dirlbl()+"_" + *p->schedtask()) == 0  
    );
}
    
void ILPContentionAware::detect_communication() {
    for(SchedJob *t : sched->schedjobs()) {
        _problem.add_constraint(
            0 * _problem.var("NhasDR_"+*t) + 1 * _problem.var("hasDR_"+*t) - _problem.var("delay_r_"+*t) <= 0, 
            "hasDR1_"+*t
        );
        _problem.add_constraint(
            1 * _problem.var("NhasDR_"+*t) + SeqTauMax * _problem.var("hasDR_"+*t) - _problem.var("delay_r_"+*t) >= 1, 
            "hasDR2_"+*t
        );
        _problem.add_constraint(
            _problem.var("NhasDR_"+*t) + _problem.var("hasDR_"+*t) == 1
        );

        _problem.var("NhasDR_"+*t).var.binary();
        _problem.var("hasDR_"+*t).var.binary();
        
        _problem.add_constraint(
            0 * _problem.var("NhasDW_"+*t) + 1 * _problem.var("hasDW_"+*t) - _problem.var("delay_w_"+*t) <= 0, 
            "hasDW1_"+*t
        );
        _problem.add_constraint(
            1 * _problem.var("NhasDW_"+*t) + SeqTauMax * _problem.var("hasDW_"+*t) - _problem.var("delay_w_"+*t) >= 1, 
            "hasDW2_"+*t
        );
        _problem.add_constraint(
            _problem.var("NhasDW_"+*t) + _problem.var("hasDW_"+*t) == 1
        );
        

        _problem.var("NhasDW_"+*t).var.binary();
        _problem.var("hasDW_"+*t).var.binary();
    }
}
    
void ILPContentionAware::detect_overlapping() {
    auto jobs = sched->schedjobs();
    for (auto it = jobs.begin(), e = jobs.end(); it != e; ++it) {
        SchedJob *i = *it;
        for (auto jt = std::next(it); jt != e; ++jt) {
            SchedJob *j = *jt;

            _problem.var("ov_rr_"+*i+"_"+*j);
            _problem.var("ov_rr_"+*j+"_"+*i);
            _problem.var("ov_ww_"+*i+"_"+*j);
            _problem.var("ov_ww_"+*j+"_"+*i);
            _problem.var("ov_rw_"+*i+"_"+*j);
            _problem.var("ov_rw_"+*j+"_"+*i);
            _problem.var("ov_wr_"+*i+"_"+*j);
            _problem.var("ov_wr_"+*j+"_"+*i);

            if(!i->task()->is_parallel(j->task())) {
                _problem.add_constraint(_problem.var("ov_rr_"+*i+"_"+*j) == 0);
                _problem.add_constraint(_problem.var("ov_rr_"+*j+"_"+*i) == 0);
                _problem.add_constraint(_problem.var("ov_ww_"+*i+"_"+*j) == 0);
                _problem.add_constraint(_problem.var("ov_ww_"+*j+"_"+*i) == 0);
                _problem.add_constraint(_problem.var("ov_wr_"+*i+"_"+*j) == 0);
                _problem.add_constraint(_problem.var("ov_wr_"+*j+"_"+*i) == 0);
                _problem.add_constraint(_problem.var("ov_rw_"+*i+"_"+*j) == 0);
                _problem.add_constraint(_problem.var("ov_rw_"+*j+"_"+*i) == 0);
                continue;
            }

            // rho_r_i < rho_j (into tovrr2)
            _problem.add_constraint(
                -SeqTauMax * _problem.var("zrr2_"+*i+"_"+*j) + _problem.var("tovrr2_"+*i+"_"+*j) - _problem.var("rho_"+*j) + _problem.var("rho_r_"+*i) <= 0
            );
            _problem.add_constraint(
                -1 * _problem.var("zrr2_"+*i+"_"+*j) - SeqTauMax * _problem.var("tovrr2_"+*i+"_"+*j) + _problem.var("rho_"+*j) - _problem.var("rho_r_"+*i) <= -1
            ); 
            _problem.add_constraint(
                _problem.var("tovrr2_"+*i+"_"+*j) + _problem.var("zrr2_"+*i+"_"+*j) == 1
            );
            _problem.var("tovrr2_"+*i+"_"+j).var.binary();
            _problem.var("zrr2_"+*i+"_"+j).var.binary();

            //rho_r_j < rho_i (into tovrr3)
            _problem.add_constraint(
                -SeqTauMax * _problem.var("zrr3_"+*i+"_"+*j) + _problem.var("tovrr3_"+*i+"_"+*j) - _problem.var("rho_"+*i) + _problem.var("rho_r_"+*j) <= 0
            );
            _problem.add_constraint(
                -1 * _problem.var("zrr3_"+*i+"_"+*j) - SeqTauMax * _problem.var("tovrr3_"+*i+"_"+*j) + _problem.var("rho_"+*i) - _problem.var("rho_r_"+*j) <= -1
            ); 
            _problem.add_constraint(
                _problem.var("tovrr3_"+*i+"_"+*j) + _problem.var("zrr3_"+*i+"_"+*j) == 1
            );
            _problem.var("tovrr3_"+*i+"_"+j).var.binary();
            _problem.var("zrr3_"+*i+"_"+j).var.binary();

            // rho_r_i < rho_j AND rho_r_j < rho_i AND DR_i > 0 AND DR_j > 0
            _problem.add_constraint(
                _problem.var("ov_rr_"+*i+"_"+*j) - _problem.var("tovrr3_"+*i+"_"+*j) - _problem.var("tovrr2_"+*i+"_"+*j) - _problem.var("hasDR_"+*i) - _problem.var("hasDR_"+*j) >= -3
            );
            _problem.add_constraint(
                _problem.var("ov_rr_"+*i+"_"+*j) - _problem.var("tovrr3_"+*i+"_"+*j) <= 0
            );
            _problem.add_constraint(
                _problem.var("ov_rr_"+*i+"_"+*j) - _problem.var("tovrr2_"+*i+"_"+*j) <= 0
            );
            _problem.add_constraint(
                _problem.var("ov_rr_"+*i+"_"+*j) - _problem.var("hasDR_"+*i) <= 0
            );
            _problem.add_constraint(
                _problem.var("ov_rr_"+*i+"_"+*j) - _problem.var("hasDR_"+*j) <= 0
            );

            _problem.add_constraint(
                _problem.var("ov_rr_"+*i+"_"+*j) - _problem.var("ov_rr_"+*j+"_"+*i) == 0
            );

//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
            // rho_w_i < end_j
            _problem.add_constraint(
                -SeqTauMax * _problem.var("zww2_"+*i+"_"+*j) + _problem.var("tovww2_"+*i+"_"+*j) - _problem.var("end_"+*j) + _problem.var("rho_w_"+*i) <= 0
            );
            _problem.add_constraint(
                -1 * _problem.var("zww2_"+*i+"_"+*j) - SeqTauMax * _problem.var("tovww2_"+*i+"_"+*j) + _problem.var("end_"+*j) - _problem.var("rho_w_"+*i) <= -1
            ); 
            _problem.add_constraint(
                _problem.var("tovww2_"+*i+"_"+*j) + _problem.var("zww2_"+*i+"_"+*j) == 1
            );
            _problem.var("tovww2_"+*i+"_"+j).var.binary();
            _problem.var("zww2_"+*i+"_"+j).var.binary();

            //rho_w_j < end_i
            _problem.add_constraint(
                -SeqTauMax * _problem.var("zww3_"+*i+"_"+*j) + _problem.var("tovww3_"+*i+"_"+*j) - _problem.var("end_"+*i) + _problem.var("rho_w_"+*j) <= 0
            );
            _problem.add_constraint(
                -1 * _problem.var("zww3_"+*i+"_"+*j) - SeqTauMax * _problem.var("tovww3_"+*i+"_"+*j) + _problem.var("end_"+*i) - _problem.var("rho_w_"+*j) <= -1
            ); 
            _problem.add_constraint(
                _problem.var("tovww3_"+*i+"_"+*j) + _problem.var("zww3_"+*i+"_"+*j) == 1
            );
            _problem.var("tovww3_"+*i+"_"+j).var.binary();
            _problem.var("zww3_"+*i+"_"+j).var.binary();

            _problem.add_constraint(
                _problem.var("ov_ww_"+*i+"_"+*j) - _problem.var("tovww3_"+*i+"_"+*j) - _problem.var("tovww2_"+*i+"_"+*j) - _problem.var("hasDW_"+*i) - _problem.var("hasDW_"+*j) >= -3
            );
            _problem.add_constraint(
                _problem.var("ov_ww_"+*i+"_"+*j) - _problem.var("tovww3_"+*i+"_"+*j) <= 0
            );
            _problem.add_constraint(
                _problem.var("ov_ww_"+*i+"_"+*j) - _problem.var("tovww2_"+*i+"_"+*j) <= 0
            );
            _problem.add_constraint(
                _problem.var("ov_rr_"+*i+"_"+*j) - _problem.var("hasDW_"+*i) <= 0
            );
            _problem.add_constraint(
                _problem.var("ov_rr_"+*i+"_"+*j) - _problem.var("hasDW_"+*j) <= 0
            );

            _problem.add_constraint(
                _problem.var("ov_ww_"+*i+"_"+*j) - _problem.var("ov_ww_"+*j+"_"+*i) == 0
            );
        }
    }
    for(SchedJob *i : sched->schedjobs()) {
        for(SchedJob *j : sched->schedjobs()) {
            if(i == j || !i->task()->is_parallel(j->task())) continue;
//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
            // rho_r_i < end_j
            _problem.add_constraint(
                -SeqTauMax * _problem.var("zrw2_"+*i+"_"+*j) + _problem.var("tovrw2_"+*i+"_"+*j) - _problem.var("end_"+*j) + _problem.var("rho_r_"+*i) <= 0
            );
            _problem.add_constraint(
                -1 * _problem.var("zrw2_"+*i+"_"+*j) - SeqTauMax * _problem.var("tovrw2_"+*i+"_"+*j) + _problem.var("end_"+*j) - _problem.var("rho_r_"+*i) <= -1
            ); 
            _problem.add_constraint(
                _problem.var("tovrw2_"+*i+"_"+*j) + _problem.var("zrw2_"+*i+"_"+*j) == 1
            );
            _problem.var("tovrw2_"+*i+"_"+j).var.binary();
            _problem.var("zrw2_"+*i+"_"+j).var.binary();

            //rho_w_j < rho_i
            _problem.add_constraint(
                -SeqTauMax * _problem.var("zrw3_"+*i+"_"+*j) + _problem.var("tovrw3_"+*i+"_"+*j) - _problem.var("rho_"+*i) + _problem.var("rho_w_"+*j) <= 0
            );
            _problem.add_constraint(
                -1 * _problem.var("zrw3_"+*i+"_"+*j) - SeqTauMax * _problem.var("tovrw3_"+*i+"_"+*j) + _problem.var("rho_"+*i) - _problem.var("rho_w_"+*j) <= -1
            ); 
            _problem.add_constraint(
                _problem.var("tovrw3_"+*i+"_"+*j) + _problem.var("zrw3_"+*i+"_"+*j) == 1
            );
            _problem.var("tovrw3_"+*i+"_"+j).var.binary();
            _problem.var("zrw3_"+*i+"_"+j).var.binary();

            _problem.add_constraint(
                _problem.var("ov_rw_"+*i+"_"+*j) - _problem.var("tovrw3_"+*i+"_"+*j) - _problem.var("tovrw2_"+*i+"_"+*j) - _problem.var("hasDR_"+*i) - _problem.var("hasDW_"+*j) >= -3
            );
            _problem.add_constraint(
                _problem.var("ov_rw_"+*i+"_"+*j) - _problem.var("tovrw3_"+*i+"_"+*j) <= 0
            );
            _problem.add_constraint(
                _problem.var("ov_rw_"+*i+"_"+*j) - _problem.var("tovrw2_"+*i+"_"+*j) <= 0
            );
            _problem.add_constraint(
                _problem.var("ov_rr_"+*i+"_"+*j) - _problem.var("hasDR_"+*i) <= 0
            );
            _problem.add_constraint(
                _problem.var("ov_rr_"+*i+"_"+*j) - _problem.var("hasDW_"+*j) <= 0
            );

//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
            _problem.add_constraint(
                _problem.var("ov_wr_"+*i+"_"+*j) - _problem.var("ov_rw_"+*j+"_"+*i) == 0
            );
            _problem.add_constraint(
                _problem.var("ov_wr_"+*j+"_"+*i) - _problem.var("ov_rw_"+*i+"_"+*j) == 0
            );

        }
    }
}
    
void ILPContentionAware::contention() {
    for(SchedJob *t : sched->schedjobs()) {
        /*
        string tmp_r = "";
        string tmp_w = "";
        for(Task *j : tg.tasks()) {
            if(t == j || !t->is_parallel(j)) continue;
            tmp_r += " - ov_rr_"+*t+"_"+*j+" - "+"ov_rw_"+*t+"_"+*j;
            tmp_w += " - ov_ww_"+*t+"_"+*j+" - "+"ov_wr_"+*t+"_"+*j;
        }

        //EMSOFT
        // Problem: - concurrency can be higher than the number of cores
        //          - concurrency represent the concurrency between tasks, not interferences on the bus
        //      but was used as the number of interfering cores
        os+"conc_r_"+*t+*tmp_r+" = 0");
        os+"conc_w_"+*t+*tmp_w+" = 0");
        */

        //foreach i,j ; i != j
            //ov_r_i_j = ov_rr_i_j \/ ov_rw_i_j
        //foreach i,j,p ; i != j
            // tmpov_r_i_j_p = ov_r_i_j /\ p_j_p
        //foreach i,p ;
            //ov_r_i_p = foreach j; \/ tmpov_i_j_p
        //conc_r_i = sum foreach p ov_r_i_p

        for(SchedJob *j : sched->schedjobs()) {
            if(t == j || !t->task()->is_parallel(j->task())) continue;
            //ov_r_i_j = ov_rr_i_j \/ ov_rw_i_j
            _problem.add_constraint(_problem.var("ov_r_"+*t+"_"+*j) - _problem.var("ov_rr_"+*t+"_"+*j) - _problem.var("ov_rw_"+*t+"_"+*j) <= 0);
            _problem.add_constraint(_problem.var("ov_r_"+*t+"_"+*j) - _problem.var("ov_rr_"+*t+"_"+*j) >= 0);
            _problem.add_constraint(_problem.var("ov_r_"+*t+"_"+*j) - _problem.var("ov_rw_"+*t+"_"+*j) >= 0);

            //ov_w_i_j = ov_ww_i_j \/ ov_wr_i_j
            _problem.add_constraint(_problem.var("ov_w_"+*t+"_"+*j) - _problem.var("ov_ww_"+*t+"_"+*j) - _problem.var("ov_wr_"+*t+"_"+*j) <= 0);
            _problem.add_constraint(_problem.var("ov_w_"+*t+"_"+*j) - _problem.var("ov_ww_"+*t+"_"+*j) >= 0);
            _problem.add_constraint(_problem.var("ov_w_"+*t+"_"+*j) - _problem.var("ov_wr_"+*t+"_"+*j) >= 0);

            for(ComputeUnit *p : tg.processors()) {
                // tmpov_r_i_j_p = ov_r_i_j /\ p_j_p
                _problem.add_constraint(_problem.var("tmpov_r_"+*t+"_"+*j+"_"+*p) - _problem.var("ov_r_"+*t+"_"+*j) - _problem.var("p_"+*j+"_"+*p) >= -1);
                _problem.add_constraint(_problem.var("tmpov_r_"+*t+"_"+*j+"_"+*p) - _problem.var("ov_r_"+*t+"_"+*j) <= 0);
                _problem.add_constraint(_problem.var("tmpov_r_"+*t+"_"+*j+"_"+*p) - _problem.var("p_"+*j+"_"+*p) <= 0);
                _problem.var("tmpov_r_"+*t+"_"+*j+"_"+*p).var.binary();

                // tmpov_w_i_j_p = ov_w_i_j /\ p_j_p
                _problem.add_constraint(_problem.var("tmpov_w_"+*t+"_"+*j+"_"+*p) - _problem.var("ov_w_"+*t+"_"+*j) - _problem.var("p_"+*j+"_"+*p) >= -1);
                _problem.add_constraint(_problem.var("tmpov_w_"+*t+"_"+*j+"_"+*p) - _problem.var("ov_w_"+*t+"_"+*j) <= 0);
                _problem.add_constraint(_problem.var("tmpov_w_"+*t+"_"+*j+"_"+*p) - _problem.var("p_"+*j+"_"+*p) <= 0);
                _problem.var("tmpov_w_"+*t+"_"+*j+"_"+*p).var.binary();
            }
        }

        for(ComputeUnit *p : tg.processors()) {
            //ov_r_i_p = foreach j; \/ tmpov_r_i_j_p
            std::map<ILPVariable *, int256_t> tmp_r, tmp_w;
            for(SchedJob *j : sched->schedjobs()) {
                if(t == j || !t->task()->is_parallel(j->task())) continue;
                tmp_r[&_problem.var("tmpov_r_"+*t+"_"+*j+"_"+*p).var] = -1;
                _problem.add_constraint(_problem.var("ov_r_"+*t+"_"+*p) - _problem.var("tmpov_r_"+*t+"_"+*j+"_"+*p) >= 0);

                tmp_w[&_problem.var("tmpov_w_"+*t+"_"+*j+"_"+*p).var] = -1;
                _problem.add_constraint(_problem.var("ov_w_"+*t+"_"+*p) - _problem.var("tmpov_w_"+*t+"_"+*j+"_"+*p) >= 0);
            }
            _problem.add_constraint(_problem.var("ov_r_"+*t+"_"+*p) + ILPExpressionSum(tmp_r) <= 0);
            _problem.var("ov_r_"+*t+"_"+*p).var.binary();

            _problem.add_constraint(_problem.var("ov_w_"+*t+"_"+*p) + ILPExpressionSum(tmp_w) <= 0);
            _problem.var("ov_w_"+*t+"_"+*p).var.binary();
        }

        //conc_r_i = sum foreach p ov_r_i_p
        std::map<ILPVariable *, int256_t> tmp_r, tmp_w;
        for(ComputeUnit *p : tg.processors()) {
            tmp_r[&_problem.var("ov_r_"+*t+"_"+*p).var] = -1;
            tmp_w[&_problem.var("ov_w_"+*t+"_"+*p).var] = -1;
        }
        _problem.add_constraint(_problem.var("conc_r_"+*t) + ILPExpressionSum(tmp_r) == 0);
        _problem.add_constraint(_problem.var("conc_w_"+*t) + ILPExpressionSum(tmp_w) == 0);
    }
}

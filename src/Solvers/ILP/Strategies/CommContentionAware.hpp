/*!
 * \file worst_concurrency.hpp
 * \copyright GNU Public License v3.
 * \date 2017
 * \author Benjamin Rouxel <benjamin.rouxel@inria.fr>, INRIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef FORM_CONTENTIONAWARE_H
#define FORM_CONTENTIONAWARE_H

#include "Solvers/ILP/ILP.hpp"
#include "BaseILPCommunicationAware.hpp"

class ILPContentionAware : public BaseILPCommunicationAware {
public:
    explicit ILPContentionAware(const SystemModel &m, const config_t &c, Schedule *s, bool o);
    
    virtual std::string help() override;
    
    virtual void gen() override;
    
protected:
    void communication_delay() override;
    
    void packet_comm_delay(SchedPacket *p, const std::map<ILPVariable *, int256_t> &mapping, uint64_t pdata);
    
    void detect_communication();
    
    void detect_overlapping();
    
    void contention();
};

REGISTER_ILPSTRATEGY(ILPContentionAware, "contention_aware")
        
#endif
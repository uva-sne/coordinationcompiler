/*!
 * \file ideal.cpp
 * \copyright GNU Public License v3.
 * \date 2017
 * \author Benjamin Rouxel <benjamin.rouxel@uva.nl>, University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "ideal.hpp"

using namespace std;

IMPLEMENT_HELP(ILPIdeal,
    Generate an ILP problem to schedule an application\n
    \n
    Communication between tasks are assumed to be ideal, meaning that there is 
    never an interference
)

ILPIdeal::ILPIdeal(const SystemModel &m, const config_t &c, Schedule *s, bool o) : BaseILPCommunicationAware(m, c, s, o) {}

void ILPIdeal::gen() {
    if(conf.archi.interconnect.burst != "none")
        throw Todo("Cplex + ideal + burst mode edge/packet/flit doesn't make any sens");
    
    SeqTauMax = (uint64_t)tg.global_deadline();
    if(SeqTauMax == 0 || (uint64_t)tg.sequential_length() < SeqTauMax)
        SeqTauMax = (uint64_t)tg.sequential_length();
    
    timinginfos_t lastiter = 0;
    for(SchedJob *t : sched->schedjobs()) {
        if(t->min_rt_period() > lastiter)
            lastiter = t->min_rt_period();
    }
    SeqTauMax += lastiter;
    
    add_header_comments();
    objectiv_func();

    unicity();
    detect_samecore();
    decide_task_order();
    decide_task_ordering_percore();
    conflicts();
    if(conf.runtime.rtt == runtime_task_e::SINGLEPHASE)
        single_phase();
    else
       _3_phases();
    wcet();
    causality();
    communication_delay();
    
    force_mapping();

    linkObj();

    add_bounds(SeqTauMax);
    add_communication_bounds(SeqTauMax);
}

void ILPIdeal::communication_delay() {
    for (SchedPacket *p : sched->schedpackets()) {
        _problem.add_constraint(_problem.var("delay_"+p->dirlbl()+"_" + *p) == 0);
        _problem.add_constraint(_problem.var("D"+boost::to_upper_copy(p->dirlbl())+"_"+*p) == 0);
    }
}
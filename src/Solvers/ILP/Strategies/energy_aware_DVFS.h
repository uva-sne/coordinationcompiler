/*
 * Copyright (C) 2019 Julius Roeder <j.roeder@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ENERGYAWAREDVFS
#define ENERGYAWAREDVFS

#include "Solvers/ILP/ILP.hpp"
#include "BaseILPEnergyAware.hpp"
#include <stdint.h>

using namespace std;

class ILPEnergyAwareDVFS : public BaseILPEnergyAware {
public:
    explicit ILPEnergyAwareDVFS(const SystemModel &m, const config_t &c, Schedule *s, bool o);
    virtual std::string help() override;
    void gen() override;
    void objectiv_func() override;
    void linkObj() override;
    void multiversion_mapping();
    void wcet() override;
    void wcec();
    void task_voltage_cluster();
    void same_voltage_island();
    void same_time();
    void same_time_and_cluster();
    void enforce_same_frequency();
    void static_energy();
    void static_energy_temp();
    void multiversion_selection();
    void precedence_samecore();
    void causality() override;
    void postsolve(std::map<std::string, long long>);
    void task_version_frequency();
    void task_requires_gpu();
    void task_gpu_frequency();
    void task_pairs_require_gpu();
    void gpu_task_orders();
    void gpu_frequency_time();
    void static_energy_GPU();
    void cluster_frequencies();
    void cluster_frequency_times();
    void static_energy_CPU();
};

REGISTER_ILPSTRATEGY(ILPEnergyAwareDVFS, "energy_aware-DVFS")

#endif
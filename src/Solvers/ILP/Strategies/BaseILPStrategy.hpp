/*!
 * \file ILPFormGen.hpp
 * \copyright GNU Public License v3.
 * \date 2017
 * \author Benjamin Rouxel <benjamin.rouxel@uva.nl>, University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef BASEIILPSTRATEGY_HPP
#define BASEIILPSTRATEGY_HPP

#include <string>
#include <map>

#include "Solvers/ILP/ILP.hpp"

#include "registry.hpp"

#include "SystemModel.hpp"
#include "Schedule.hpp"
#include "config.hpp"

class BaseILPStrategy {
protected:
    const SystemModel &tg; //!< IR of the current application
    const config_t &conf; //!< the global configuration
    Schedule *sched; //!< resulting schedule

    int64_t SeqTauMax = -1; //!< Sequential length of the schedule
    
    bool optimal = true; //!< \copybrief ILPSolver::optimal
    bool allow_migration = true; //!< \copybrief Solver::allow_migration
    
    ILPProblem _problem; //!< The problem
    
    std::map<std::string, long long> tmp_results; //!< Store temporary results before post-processing
public:
    /*!
     * \brief Constructor
     * \param m \copybrief ILPFormGen::tg
     * \param c \copybrief ILPFormGen::conf
     * \param s \copybrief ILPFormGen::sched
     * \param o \copybrief ILPFormGen::optimal 
     */
    
    BaseILPStrategy(const SystemModel &m, const config_t &c, Schedule *s, bool o) : tg(m), conf(c), sched(s), optimal(o) {}
    
    /**
     * Start the generation of the problem
     */
    virtual void gen() = 0;
    
    virtual std::string help() = 0;
    
    /**
     * Do some post-processing on the result given by the solver
     * @param result map of variables and solver values
     */
    virtual void postsolve(const std::map<std::string, long long> &result);
    
    /**
     * Set if the solver should allow task migration
     * @param a true or false
     */
    virtual void set_allow_migration(bool a) { allow_migration = a; }
    
    //! Add header comments to the problem
    virtual void add_header_comments();
    
    ACCESSORS_R(BaseILPStrategy, ILPProblem&, problem)
    
protected:
    /**
     * Return a result from a variable name
     * @param lbl variable name
     * @throw CompilerException if the label is not found
     * @return value given by the solver
     */
    long long get_result(const std::string &lbl);
    /**
     * Post-process application with a AER runtime \see config_t::runtime_task_e
     */
    void postsolve_aer();
    /**
     * Post-process application with a PREM runtime \see config_t::runtime_task_e
     */
    void postsolve_prem();
    /**
     * Post-process application with a historical runtime \see config_t::runtime_task_e
     */
    void postsolve_singlephase();
};

/*!
 * \typedef ILPFormRegistry
 * \brief Registry containing all ILP problem generator
 */
using ILPStrategyRegistry = registry::Registry<BaseILPStrategy, std::string, const SystemModel &, const config_t &, Schedule *, bool>;

/*!
 * \brief Helper macro to register a new derived generator
 */
#define REGISTER_ILPSTRATEGY(ClassName, Identifier) \
  REGISTER_SUBCLASS(BaseILPStrategy, ClassName, std::string, Identifier, const SystemModel &, const config_t &, Schedule *, bool)


#endif /* BASEIILPSTRATEGY_HPP */


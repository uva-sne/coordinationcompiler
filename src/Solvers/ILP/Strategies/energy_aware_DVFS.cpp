/*
 * Copyright (C) 2019 Julius Roeder <j.roeder@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "energy_aware_DVFS.h"
#include "Utils/Log.hpp"

using namespace std;

ILPEnergyAwareDVFS::ILPEnergyAwareDVFS(const SystemModel &m, const config_t &c, Schedule *s, bool o) : BaseILPEnergyAware(m, c, s, o) {}

IMPLEMENT_HELP(ILPEnergyAwareDVFS,
        Julius will add some blah blah
)

void ILPEnergyAwareDVFS::gen() {
    // must be non-blocking
    SeqTauMax = tg.global_deadline();
    SeqTauMaxEnergy = tg.global_deadline();
    if(SeqTauMax == 0 || tg.sequential_length() < SeqTauMax)
        SeqTauMax = tg.sequential_length();

    if(tg.sequential_length_energy_based() < SeqTauMaxEnergy) //trying to calculate the time for a most energy optimal sequential execution.
        SeqTauMaxEnergy = tg.sequential_length_energy_based();

    SeqTauMax = SeqTauMaxEnergy * 1.2;

    uint128_t lastiter = 0;
    for(SchedElt *t : sched->schedjobs()) {
        if(t->min_rt_period() > lastiter)
            lastiter = t->min_rt_period();
    }
    SeqTauMax += lastiter;


    for (voltage_island *v: tg.voltage_islands()){
        if(v->type == "cpu"){
            if (v->max_frequency>max_cpu_frequency) max_cpu_frequency = v->max_frequency;
        }else{
            if (v->max_frequency>max_gpu_frequency) max_cpu_frequency = v->max_frequency;
        }
    }

    Utils::INFO("Sequential Schedule Lenght: "+to_string(tg.sequential_length()));
    Utils::INFO("Deadline Schedule Lenght: "+to_string(tg.global_deadline()));
    Utils::INFO("Sequential based on energy Schedule Lenght: "+to_string(SeqTauMax));

    add_header_comments();
    objectiv_func();
    linkObj();
    unicity();
    detect_samecore();
    precedence_samecore();
    single_phase();
    wcet();
    wcec();
    conflicts();
    causality();
    static_energy_temp();
    static_energy();
    task_voltage_cluster();
    same_voltage_island();
    same_time();
    same_time_and_cluster();
    enforce_same_frequency();
    cluster_frequencies();
    cluster_frequency_times();
    static_energy_CPU();

    multiversion_mapping();
    multiversion_selection();
    task_version_frequency();

    task_requires_gpu();
    task_gpu_frequency();
    task_pairs_require_gpu();
    gpu_task_orders();
    gpu_frequency_time();
    static_energy_GPU();


    add_bounds(SeqTauMax);

//        Multiperiod is currently causing bad unnamed constraints
//        multiperiod();
}

void ILPEnergyAwareDVFS::task_version_frequency(){
    //TODO: Synchronize task frequency and version frequency
    for(SchedElt *t: sched->schedjobs()){
        std::map<ILPVariable *, int256_t> values;

        for(Version* elv : t->schedtask()->task()->versions()) {
            Version *v = elv;
            values[&_problem.var("vc_" + *t + "_" + *v).var] = v->frequency();
        }

        _problem.add_constraint(
                _problem.var("frequency_" + *t) - ILPExpressionSum(values) == 0,
                "Frequency_" + *t
        );
    }

}

void ILPEnergyAwareDVFS::task_voltage_cluster(){
    //TODO: Determine if tasks are on same voltage island
    for (auto voltage: tg.voltage_islands()){
        if (voltage->type != "cpu") continue; //only want general purpose Compute units
        for (auto *t: sched->schedjobs()){
            std::map<ILPVariable *, int256_t> values;

            for (auto p: voltage->impacted_processors){
                values[&_problem.var("p_" + *t + "_" + *p).var] = 1;
            }
            _problem.add_constraint(
                    _problem.var("voltage_island_" + voltage->id +"_" + *t) - ILPExpressionSum(values) == 0,
                    "Voltage_island_"+ voltage->id +"_" + *t
            );
            _problem.var("voltage_island_" + voltage->id +"_" + *t).var.binary();
        }
    }
}

void ILPEnergyAwareDVFS::same_voltage_island(){
    //TODO: Determine if tasks run on the same voltage islands
    auto st = sched->schedjobs();
    for (auto it = st.begin(), et=st.end() ; it != et ; ++it) {
        for (auto jt = std::next(it) ; jt != et ; ++jt) {
            SchedJob *ti = *it, *tj = *jt;
    
            std::map<ILPVariable *, int256_t> values;
            for (auto voltage: tg.voltage_islands()) {
                //logical and
                std::string tmp_svi = "tmp_svi_" + *ti + "_" + *tj + "_" + voltage->id;
                values[&_problem.var(tmp_svi).var] = 1;
                _problem.var(tmp_svi).var.binary();

                _problem.add_constraint(
                        _problem.var("voltage_island_" + voltage->id +"_" + *ti) + _problem.var("voltage_island_" + voltage->id +"_" + *tj) - _problem.var(tmp_svi)  <= 1,
                        "same_voltage_island_1_" + *ti + "_" + *tj + "_" + voltage->id
                );

                _problem.add_constraint(
                        _problem.var("voltage_island_" + voltage->id +"_" + *ti)  - _problem.var(tmp_svi) >= 0,
                        "same_voltage_island_2_" + *ti + "_" + *tj  + voltage->id
                        );

                _problem.add_constraint(
                        _problem.var("voltage_island_" + voltage->id +"_" + *tj) - _problem.var(tmp_svi) >= 0,
                        "same_voltage_island_3_" + *ti + "_" + *tj  + voltage->id
                );
            }

            //summation over voltage islands
            _problem.add_constraint(
                    ILPExpressionSum(values) - _problem.var("same_voltage_island_" + *ti + "_" + *tj) == 0,
                    "same_voltage_island_4_" + *ti + "_" + *tj
            );

            // make sure both i-j and j-i are both set and are the same.
            //TODO: double check these aren't necessary
//            _problem.add_constraint(
//                    _problem.var("same_voltage_island_" + *ti + "_" + *tj) - _problem.var("same_voltage_island_" + *tj + "_" + *ti) == 0,
//                    "same_voltage_island_5_" + *ti + "_" + *tj
//            );

            _problem.var("same_voltage_island_" + *ti + "_" + *tj).var.binary();
//            _problem.var("same_voltage_island_" + *tj + "_" + *ti).var.binary();
        }
    }

}

void ILPEnergyAwareDVFS::same_time(){
    //TODO: Determine if tasks run at the same time

    auto st = sched->schedjobs();
    for (auto it = st.begin(), et=st.end() ; it != et ; ++it) {
        for (auto jt = std::next(it); jt != et; ++jt) {
            SchedJob *ti = *it, *tj = *jt;

            // if end time of task q (tj)  is larger than start time of task p (ti)
            _problem.add_constraint(
                    _problem.var("rho_" + *ti) - (_problem.var("rho_" +*tj) + _problem.var("wcet_" + *tj)) + SeqTauMax * _problem.var("temp_time_1_"+ *ti +"_" + *tj) >= 0,
                    "same_time_1_tmp1_" + *ti + "_" + *tj
                    );
            _problem.add_constraint(
                    _problem.var("rho_" + *ti) - (_problem.var("rho_" +*tj) + _problem.var("wcet_" + *tj)) + SeqTauMax * _problem.var("temp_time_1_"+ *ti +"_" + *tj) <= SeqTauMax - 1,
                    "same_time_1_tmp2_" + *ti + "_" + *tj
            );

            // if end time of task p (ti)  is larger than start time of task q (tj)
            _problem.add_constraint(
                    _problem.var("rho_" + *tj) - (_problem.var("rho_" +*ti) + _problem.var("wcet_" + *ti)) + SeqTauMax * _problem.var("temp_time_2_"+ *ti +"_" + *tj) >= 0,
                    "same_time_2_tmp1_" + *ti + "_" + *tj
            );
            _problem.add_constraint(
                    _problem.var("rho_" + *tj) - (_problem.var("rho_" +*ti) + _problem.var("wcet_" + *ti)) + SeqTauMax * _problem.var("temp_time_2_"+ *ti +"_" + *tj) <= SeqTauMax - 1,
                    "same_time_2_tmp2_" + *ti + "_" + *tj
            );

            // logical AND over the two temp times
            _problem.add_constraint(
                    _problem.var("temp_time_1_" + *ti +"_" + *tj) + _problem.var("temp_time_2_" + *ti +"_" + *tj) - _problem.var("same_time_"+ *ti +"_" + *tj)  <= 1,
                    "same_time_3_" + *ti + "_" + *tj
            );

            _problem.add_constraint(
                    _problem.var("temp_time_1_" + *ti +"_" + *tj)  - _problem.var("same_time_"+ *ti +"_" + *tj) >= 0,
                    "same_time_4_" + *ti + "_" + *tj
            );

            _problem.add_constraint(
                    _problem.var("temp_time_2_" + *ti +"_" + *tj) - _problem.var("same_time_"+ *ti +"_" + *tj) >= 0,
                    "same_time_5_" + *ti + "_" + *tj
            );

            _problem.var("same_time_"+ *ti +"_" + *tj).var.binary();
            _problem.var("temp_time_1_"+ *ti +"_" + *tj).var.binary();
            _problem.var("temp_time_2_"+ *ti +"_" + *tj).var.binary();
        }
    }
}

void ILPEnergyAwareDVFS::same_time_and_cluster(){
    //TODO: same time and cluster
    auto st = sched->schedjobs();
    for (auto it = st.begin(), et=st.end() ; it != et ; ++it) {
        for (auto jt = std::next(it); jt != et; ++jt) {
            SchedJob *ti = *it, *tj = *jt;

            // logical AND over the two temp times
            _problem.add_constraint(
                    _problem.var("same_time_" + *ti +"_" + *tj) + _problem.var("same_voltage_island_" + *ti +"_" + *tj) - _problem.var("same_time_and_voltage_island_"+ *ti +"_" + *tj)  <= 1,
                    "same_time_voltage_1_" + *ti + "_" + *tj
            );

            _problem.add_constraint(
                    _problem.var("same_time_" + *ti +"_" + *tj)  - _problem.var("same_time_and_voltage_island_"+ *ti +"_" + *tj) >= 0,
                    "same_time_voltage_2_" + *ti + "_" + *tj
            );

            _problem.add_constraint(
                    _problem.var("same_voltage_island_" + *ti +"_" + *tj) - _problem.var("same_time_and_voltage_island_"+ *ti +"_" + *tj) >= 0,
                    "same_time_voltage_3_" + *ti + "_" + *tj
            );

            _problem.var("same_time_and_voltage_island_"+ *ti +"_" + *tj).var.binary();

        }
    }
}

void ILPEnergyAwareDVFS::enforce_same_frequency() {
    //TODO: enforce same frequency if two tasks are on the same voltage island and at the same time
    auto st = sched->schedjobs();
    for (auto it = st.begin(), et = st.end(); it != et; ++it) {
        for (auto jt = std::next(it); jt != et; ++jt) {
            SchedJob *ti = *it, *tj = *jt;

            // calculate the difference
            _problem.add_constraint(
                    _problem.var("frequency_diff_" + *ti+"_"+*tj) - (_problem.var("frequency_" + *ti) - _problem.var("frequency_" + *tj)) == 0,
                    "frequency_diff_1_" + *ti + "_" + *tj
            );

            // do a multiply of same time & voltage with the frequency diff
            // https://www.leandro-coelho.com/linearization-product-variables/
            _problem.add_constraint(
                    (- max_cpu_frequency) * _problem.var("same_time_and_voltage_island_"+ *ti +"_" + *tj) - _problem.var("frequency_diff_mult_" + *ti+"_"+*tj) <= 0 ,
                    "frequency_diff_2_" + *ti + "_" + *tj
            );

            _problem.add_constraint(
                    (max_cpu_frequency) * _problem.var("same_time_and_voltage_island_"+ *ti +"_" + *tj) - _problem.var("frequency_diff_mult_" + *ti+"_"+*tj) >= 0 ,
                    "frequency_diff_3_" + *ti + "_" + *tj
            );

            _problem.add_constraint(
                    _problem.var("NOT_same_time_and_voltage_island_"+ *ti +"_" + *tj) + _problem.var("same_time_and_voltage_island_"+ *ti +"_" + *tj)  == 1,
                    "frequency_diff_4_" + *ti + "_" + *tj
                    );

            _problem.add_constraint(
                    _problem.var("frequency_diff_" + *ti+"_"+*tj) - max_cpu_frequency * _problem.var("NOT_same_time_and_voltage_island_"+ *ti +"_" + *tj) - _problem.var("frequency_diff_mult_" + *ti+"_"+*tj) <= 0 ,
                    "frequency_diff_5_" + *ti + "_" + *tj
            );

            _problem.add_constraint(
                    _problem.var("frequency_diff_" + *ti+"_"+*tj) - (-max_cpu_frequency) * (_problem.var("NOT_same_time_and_voltage_island_"+ *ti +"_" + *tj)) - _problem.var("frequency_diff_mult_" + *ti+"_"+*tj) >= 0 ,
                    "frequency_diff_6_" + *ti + "_" + *tj
            );


            _problem.add_constraint(
                    _problem.var("frequency_diff_" + *ti+"_"+*tj) + max_cpu_frequency * (_problem.var("NOT_same_time_and_voltage_island_"+ *ti +"_" + *tj)) - _problem.var("frequency_diff_mult_" + *ti+"_"+*tj) >= 0 ,
                    "frequency_diff_7_" + *ti + "_" + *tj
            );

            _problem.add_constraint(
                    _problem.var("frequency_diff_mult_" + *ti+"_"+*tj) == 0,
                    "frequency_diff_8_" + *ti + "_" + *tj
            );

            //todo: wrong max frequency
            _problem.add_bounds(_problem.var("frequency_diff_" + *ti+"_"+*tj).var, -max_cpu_frequency, max_cpu_frequency);
            _problem.add_bounds(_problem.var("frequency_diff_mult_" + *ti+"_"+*tj).var, -max_cpu_frequency, max_cpu_frequency);
            _problem.var("NOT_same_time_and_voltage_island_"+ *ti +"_" + *tj).var.binary();
        }
    }
}

void ILPEnergyAwareDVFS::cluster_frequencies(){
    for (timinginfos_t time = 0; time < SeqTauMax; time+=tg.energy_model_time_step()) {
        for (auto t: sched->schedjobs()){

            //start time of task t >= time ?
            _problem.add_constraint(
                    _problem.var("rho_" + *t) + SeqTauMax * _problem.var("time_larger_than_rho_" + *t + "_" + to_string(time)) >= 1 + time,
                    "Time_larger_than_rho_1_" + *t + "_" + to_string(time)
                    );

            _problem.add_constraint(
                    _problem.var("rho_" + *t) + SeqTauMax * _problem.var("time_larger_than_rho_" + *t + "_" + to_string(time)) <= SeqTauMax + time ,
                    "Time_larger_than_rho_2_" + *t + "_" + to_string(time)
            );

            //end time of task_t < time?
            _problem.add_constraint(
                    SeqTauMax * _problem.var("time_smaller_than_task_end_" + *t + "_" + to_string(time)) - (_problem.var("rho_" + *t) + _problem.var("wcet_"+*t)) >= -time,
                    "Time_smaller_than_task_end_1_" + *t + "_" + to_string(time)
            );

            _problem.add_constraint(
                    SeqTauMax * _problem.var("time_smaller_than_task_end_" + *t + "_" + to_string(time)) - (_problem.var("rho_" + *t) + _problem.var("wcet_"+*t))<= SeqTauMax - 1 -time,
                    "Time_smaller_than_task_end_2_" + *t + "_" + to_string(time)
            );

            // logical AND between time_larger_than_rho_ and time_smaller_than_task_end_; resulting in if the task is active or not
            _problem.add_constraint(
                    _problem.var("time_larger_than_rho_" + *t + "_" + to_string(time)) + _problem.var("time_smaller_than_task_end_" + *t + "_" + to_string(time)) - _problem.var("task_active_" + *t + "_" + to_string(time)) <= 1,
                    "Task_active_1_"+ *t + "_" + to_string(time)
            );

            _problem.add_constraint(
                    _problem.var("time_larger_than_rho_" + *t + "_" + to_string(time))  - _problem.var("task_active_" + *t + "_" + to_string(time)) >= 0,
                    "Task_active_2_"+ *t + "_" + to_string(time)
            );

            _problem.add_constraint(
                    _problem.var("time_smaller_than_task_end_" + *t + "_" + to_string(time))- _problem.var("task_active_" + *t + "_" + to_string(time)) >= 0,
                    "Task_active_3_"+ *t + "_" + to_string(time)
            );


            //variables
            _problem.var("time_larger_than_rho_" + *t + "_" + to_string(time)).var.binary();
            _problem.var("time_smaller_than_task_end_" + *t + "_" + to_string(time)).var.binary();
            _problem.var("task_active_" + *t + "_" + to_string(time)).var.binary();
        }

//      //determine number of tasks on cluster for each time step is active
        for (auto voltage: tg.voltage_islands()){
            if (voltage->type != "cpu") continue; //only want general purpose Compute units
            std::map<ILPVariable *, int256_t> values;

            //determine if a task is active and if its on the current cluster
            for (auto t: sched->schedjobs()){
                _problem.add_constraint(
                        _problem.var("voltage_island_" + voltage->id +"_" + *t) + _problem.var("task_active_" + *t + "_" + to_string(time)) - _problem.var("task_active_on_correct_voltage_" + *t + "_" + to_string(time) + "_" + voltage->id) <= 1,
                        "Task_active_on_correct_voltage_1_"+ *t + "_" + to_string(time) + "_" + voltage->id
                );

                _problem.add_constraint(
                        _problem.var("task_active_" + *t + "_" + to_string(time))  - _problem.var("task_active_on_correct_voltage_" + *t + "_" + to_string(time) + "_" + voltage->id) >= 0,
                        "Task_active_on_correct_voltage_2_"+ *t + "_" + to_string(time) + "_" + voltage->id
                );

                _problem.add_constraint(
                        _problem.var("voltage_island_" + voltage->id +"_" + *t)- _problem.var("task_active_on_correct_voltage_" + *t + "_" + to_string(time) + "_" + voltage->id) >= 0,
                        "Task_active_on_correct_voltage_3_"+ *t + "_" + to_string(time) + "_" + voltage->id
                );

                values[&_problem.var("task_active_on_correct_voltage_" + *t + "_" + to_string(time) + "_" + voltage->id).var] = 1;

                _problem.var("task_active_on_correct_voltage_" + *t + "_" + to_string(time) + "_" + voltage->id).var.binary();

                //linearization of task_active_on_cluster x frequency variable
                _problem.add_constraint(
                        _problem.var("cluster_frequency_" + *t + "_"+ voltage->id + "_" +to_string(time)) - max_cpu_frequency * _problem.var("task_active_on_correct_voltage_" + *t + "_" + to_string(time) + "_" + voltage->id) <= 0,
                        "Cluster_frequency_1_"+ *t + "_"+ voltage->id + "_" +to_string(time)
                );

                _problem.add_constraint(
                        _problem.var("cluster_frequency_" + *t + "_"+ voltage->id + "_" +to_string(time)) - _problem.var("frequency_" + *t) <= 0,
                        "Cluster_frequency_2_"+ *t + "_"+ voltage->id + "_" +to_string(time)
                );

                _problem.add_constraint(
                        _problem.var("cluster_frequency_" + *t + "_"+ voltage->id + "_" +to_string(time)) - _problem.var("frequency_" + *t) - max_cpu_frequency*_problem.var("task_active_on_correct_voltage_" + *t + "_" + to_string(time) + "_" + voltage->id) >= - max_cpu_frequency ,
                        "Cluster_frequency_3_"+ *t + "_"+ voltage->id + "_" +to_string(time)
                );

                _problem.add_bounds(_problem.var("cluster_frequency_"+ *t + "_" + voltage->id + "_" +to_string(time)).var, 0, max_cpu_frequency);

                //cast to boolean to find if cluster is active atm
                _problem.add_constraint(
                        _problem.var("cluster_active_temp_"+ *t + "_"+ voltage->id + "_" + to_string(time)) - _problem.var("cluster_frequency_"+ *t + "_" + voltage->id + "_" +to_string(time)) <= 0,
                        "Cluster_active_bool_cast_1_"+ *t + "_"+ voltage->id + "_" + to_string(time)
                );
                _problem.add_constraint(
                        max_cpu_frequency * _problem.var("cluster_active_temp_"+ *t + "_"+ voltage->id + "_" + to_string(time))  -  _problem.var("cluster_frequency_"+ *t + "_" + voltage->id + "_" +to_string(time)) >= 0,
                        "Cluster_active_bool_cast_2_"+ *t + "_"+ voltage->id + "_" + to_string(time)
                );

                _problem.var("cluster_active_temp_" + *t + "_"+ voltage->id + "_" + to_string(time)).var.binary();

                //concatenating and bounding the cluster frequency
                _problem.add_constraint(
                        _problem.var("cluster_frequency_temp_" + voltage->id + "_" +to_string(time)) - _problem.var("cluster_frequency_" + *t + "_"+ voltage->id + "_" +to_string(time))>= 0,
                        "Cluster_frequency_flattened_1_"+ *t + "_"+ voltage->id + "_" +to_string(time)
                );

                _problem.add_constraint(
                        _problem.var("cluster_frequency_temp_" + voltage->id + "_" +to_string(time)) - _problem.var("cluster_frequency_" + *t + "_"+ voltage->id + "_" +to_string(time)) + max_cpu_frequency * _problem.var("cluster_active_temp_"+ *t + "_"+ voltage->id + "_" + to_string(time)) <= max_cpu_frequency   ,
                        "Cluster_frequency_flattened_2_"+ *t + "_"+ voltage->id + "_" +to_string(time)
                );
            }

//            sum up number of active clusters
            _problem.add_constraint(
                    _problem.var("num_active_tasks_" + voltage->id + "_" + to_string(time)) - ILPExpressionSum(values) == 0,
                    "Num_active_tasks"  + voltage->id + "_" + to_string(time)
                    );

//            cast to boolean to find if cluster is active atm
            _problem.add_constraint(
                    _problem.var("cluster_active_"+ voltage->id + "_" + to_string(time)) - _problem.var("num_active_tasks_" + voltage->id + "_" + to_string(time)) <= 0,
                    "Cluster_active_bool_cast_1_"+ voltage->id + "_" + to_string(time)
                    );
            _problem.add_constraint(
                    voltage->impacted_processors.size() * _problem.var("cluster_active_"+ voltage->id + "_" + to_string(time))  -  _problem.var("num_active_tasks_" + voltage->id + "_" + to_string(time)) >= 0,
                    "Cluster_active_bool_cast_2_"+ voltage->id + "_" + to_string(time)
            );

            _problem.var("cluster_active_"+ voltage->id + "_" + to_string(time)).var.binary();
            _problem.add_bounds(_problem.var("num_active_tasks_" + voltage->id + "_" +to_string(time)).var, 0, tg.processors().size());

            //correcting cluster frequency for short running _problems
            _problem.add_constraint(
                    _problem.var("cluster_frequency_" + voltage->id + "_" +to_string(time)) -max_cpu_frequency * _problem.var("cluster_active_"+ voltage->id + "_" + to_string(time)) <= 0,
                    "Cluster_frequency_correction1_" + voltage->id + "_" +to_string(time)
            );

            _problem.add_constraint(
                    _problem.var("cluster_frequency_" + voltage->id + "_" +to_string(time)) -_problem.var("cluster_frequency_temp_" + voltage->id + "_" +to_string(time)) <= 0,
                    "Cluster_frequency_correction2_" + voltage->id + "_" +to_string(time)
            );

            _problem.add_constraint(
                    _problem.var("cluster_frequency_" + voltage->id + "_" +to_string(time)) -_problem.var("cluster_frequency_temp_" + voltage->id + "_" +to_string(time)) - max_cpu_frequency * _problem.var("cluster_active_"+ voltage->id + "_" + to_string(time)) >= - max_cpu_frequency,
                    "Cluster_frequency_correction3_" + voltage->id + "_" +to_string(time)
            );

            _problem.add_bounds(_problem.var("cluster_frequency_" + voltage->id + "_" +to_string(time)).var, 0, max_cpu_frequency);
            _problem.add_bounds(_problem.var("cluster_frequency_temp_" + voltage->id + "_" +to_string(time)).var, 0, max_cpu_frequency);
        }
    }
}

void ILPEnergyAwareDVFS::cluster_frequency_times(){
    for (auto voltage: tg.voltage_islands()){
        if (voltage->type != "cpu") continue; //only want general purpose Compute units
        for(auto frequency_watt: voltage->frequency_watt){
            auto frequency = frequency_watt.first;
            std::map<ILPVariable *, int256_t> values;
            for (timinginfos_t time = 0; time < SeqTauMax; time+=tg.energy_model_time_step()) {

                _problem.add_constraint(
                        _problem.var("cluster_frequency_diff_" + voltage->id +"_"+to_string(frequency)+"_"+to_string(time)) + _problem.var("cluster_frequency_" + voltage->id + "_" +to_string(time)) == frequency ,
                        "Frequency_diff_1_" +  voltage->id +"_"+to_string(frequency)+"_"+to_string(time)
                        );
                _problem.add_bounds(_problem.var("cluster_frequency_diff_" + voltage->id +"_"+to_string(frequency)+"_"+to_string(time)).var, -max_cpu_frequency,max_cpu_frequency);


                //cast diff to bool, which can be positive and negative
                _problem.add_constraint(
                        (-max_cpu_frequency * _problem.var("frequency_diff_bool_" +  voltage->id +"_"+to_string(frequency)+"_"+to_string(time)) - _problem.var("cluster_frequency_diff_" + voltage->id +"_"+to_string(frequency)+"_"+to_string(time))) <= 0 ,
                        "Frequency_time_boolean_cast_1_" +  voltage->id +"_"+to_string(frequency)+"_"+to_string(time)
                );

                _problem.add_constraint(
                        max_cpu_frequency * _problem.var("frequency_diff_bool_" +  voltage->id +"_"+to_string(frequency)+"_"+to_string(time)) - _problem.var("cluster_frequency_diff_" + voltage->id +"_"+to_string(frequency)+"_"+to_string(time)) >= 0 ,
                        "Frequency_time_boolean_cast_2_" +  voltage->id +"_"+to_string(frequency)+"_"+to_string(time)
                );

                _problem.add_constraint(
                        _problem.var("frequency_diff_bool_" +  voltage->id +"_"+to_string(frequency)+"_"+to_string(time)) - (max_cpu_frequency + 1) * _problem.var("frequency_diff_bool_temp_" +  voltage->id +"_"+to_string(frequency)+"_"+to_string(time)) - _problem.var("cluster_frequency_diff_" +  voltage->id +"_"+to_string(frequency)+"_"+to_string(time)) <= 0,
                        "Frequency_time_boolean_cast_3_" +  voltage->id +"_"+to_string(frequency)+"_"+to_string(time)
                );

                _problem.add_constraint(
                        _problem.var("frequency_diff_bool_temp_"+  voltage->id +"_"+to_string(frequency)+"_"+to_string(time)) + max_cpu_frequency * _problem.var("frequency_diff_bool_temp_" +  voltage->id +"_"+to_string(frequency)+"_"+to_string(time)) + _problem.var("frequency_diff_bool_" +  voltage->id +"_"+to_string(frequency)+"_"+to_string(time)) + _problem.var("cluster_frequency_diff_" + voltage->id +"_"+to_string(frequency)+"_"+to_string(time)) <= 1 + max_cpu_frequency,
                        "Frequency_time_boolean_cast_4_"+  voltage->id +"_"+to_string(frequency)+"_"+to_string(time)
                );

                //invert bool
                _problem.add_constraint(
                        _problem.var("frequency_diff_bool_" +  voltage->id +"_"+to_string(frequency)+"_"+to_string(time)) + _problem.var("frequency_same_bool_" + voltage->id +"_"+to_string(frequency)+"_"+to_string(time)) == 1,
                        "Frequency_diff_4_" +  voltage->id +"_"+to_string(frequency)+"_"+to_string(time)
                );

                _problem.var("frequency_diff_bool_"+  voltage->id +"_"+to_string(frequency)+"_"+to_string(time)).var.binary();
                _problem.var("frequency_diff_bool_temp_"+  voltage->id +"_"+to_string(frequency)+"_"+to_string(time)).var.binary();
                _problem.var("frequency_same_bool_"+  voltage->id +"_"+to_string(frequency)+"_"+to_string(time)).var.binary();



                values[&_problem.var("frequency_same_bool_"+  voltage->id +"_"+to_string(frequency)+"_"+to_string(time)).var] = 1;
            }

            _problem.add_constraint(
                    _problem.var("frequency_time_temp_" +  voltage->id +"_"+to_string(frequency)) - ILPExpressionSum(values) == 0,
                    "Frequency_time_temp_" +  voltage->id +"_"+to_string(frequency)
            );

            _problem.add_constraint(
                    _problem.var("frequency_time_" +  voltage->id +"_"+to_string(frequency)) - tg.energy_model_time_step() * _problem.var("frequency_time_temp_" +  voltage->id +"_"+to_string(frequency))== 0,
                    "Frequency_time_" +  voltage->id +"_"+to_string(frequency)
            );
            _problem.add_bounds(_problem.var("frequency_time_temp_" +  voltage->id +"_"+to_string(frequency)).var, 0, SeqTauMax);
            _problem.add_bounds(_problem.var("frequency_time_" +  voltage->id +"_"+to_string(frequency)).var, 0, SeqTauMax);

        }
    }
}

void ILPEnergyAwareDVFS::static_energy_CPU() {
    // gets the required watt for each frequency
    std::map<ILPVariable *, int256_t> values_static_cpu;
    for (auto voltage: tg.voltage_islands()) {
        if (voltage->type != "cpu") continue; //only want general purpose Compute units
        for (auto frequency_watt: voltage->frequency_watt) {
            auto frequency = frequency_watt.first;
            auto w = frequency_watt.second;
            values_static_cpu[&_problem.var("frequency_time_" + voltage->id + "_" + to_string(frequency)).var] = w;
        }
    }

    _problem.add_constraint(
            _problem.var("static_energy_cpu_temp") - ILPExpressionSum(values_static_cpu) == 0,
            "Static_energy_CPU_temp"
    );

    //corrects the watt*time for the different time and power unit combinations
    if (tg.time_unit() == "ms" && tg.power_unit()=="mW" && tg.energy_unit()=="mJ") {
        _problem.add_constraint(
                1000 * _problem.var("static_energy_cpu") -  _problem.var("static_energy_cpu_temp") <= 0,
                "static_energy_cpu_upper"
        );
        _problem.add_constraint(
                1000 * _problem.var("static_energy_cpu") - _problem.var("static_energy_cpu_temp") >= -999,
                "static_energy_cpu_lower"
        );
    }else if (tg.time_unit() == "s" && tg.power_unit()=="W" && tg.energy_unit()=="J"){
        _problem.add_constraint(
                _problem.var("static_energy_cpu") -_problem.var("static_energy_cpu_temp") == 0,
                "static_energy_cpu_lower_upper"
        );
    }else if (tg.time_unit() == "us" && tg.power_unit()=="uW" && tg.energy_unit()=="uJ"){
        _problem.add_constraint(
                1000000 * _problem.var("static_energy_cpu") - _problem.var("static_energy_cpu_temp") <= 0,
                "static_energy_cpu_upper"
        );
        _problem.add_constraint(
                1000000 * _problem.var("static_energy_cpu") - _problem.var("static_energy_cpu_temp") >= -999999,
                "static_energy_cpu_lower"
        );
    }else if (tg.time_unit() == "ds" && tg.power_unit()=="mW" && tg.energy_unit()=="mJ"){
        _problem.add_constraint(
                10 * _problem.var("static_energy_cpu") - _problem.var("static_energy_cpu_temp") <= 0,
                "static_energy_cpu_upper"
        );
        _problem.add_constraint(
                10 * _problem.var("static_energy_cpu") - _problem.var("static_energy_cpu_temp") >= -9,
                "static_energy_cpu_lower"
        );
    }else{
        Utils::WARN("ILPEnergyAwareDVFS::static_energy_cpu in energy_aware_ECRTS2020.cpp cannot compute energy properly as the time and power units dont match any of the predetermined cases");
    }

    // bounds the static cpu energy consumptions. assumes everything is run at max frequency
    uint64_t additional_watt = 0;
    for (auto vi: tg.voltage_islands()){
        uint64_t vi_max = 0;
        for (auto frequency_watt: vi->frequency_watt){
            if (frequency_watt.second > vi_max) vi_max = frequency_watt.second;
        }
        additional_watt += vi_max;
    }
    std::cout << SeqTauMax << " " << tg.base_watt() << " " << additional_watt << std::endl;

    _problem.add_bounds(_problem.var("static_energy_cpu_temp").var, 0, SeqTauMax * (tg.base_watt() + additional_watt)*10);
    _problem.add_bounds(_problem.var("static_energy_cpu").var, 0, SeqTauMax * (tg.base_watt() + additional_watt));
}

void ILPEnergyAwareDVFS::task_requires_gpu(){
    //Task requires GPU

    for(SchedElt *t: sched->schedjobs()){
        std::map<ILPVariable *, int256_t> values;

        for(Version* elv : t->schedtask()->task()->versions()) {
            Version *v = elv;
            values[&_problem.var("vc_" + *t + "_" + *v).var] = v->gpu_required();
        }

        _problem.add_constraint(
                _problem.var("gpu_required_" + *t) - ILPExpressionSum(values) == 0,
                "GPU_required_" + *t
        );

        _problem.var("gpu_required_" + *t).var.binary();
    }

}

void ILPEnergyAwareDVFS::task_gpu_frequency(){
    //Task GPU freqeuncy

    for(SchedElt *t: sched->schedjobs()){
        std::map<ILPVariable *, int256_t> values;

        for(Version* elv : t->schedtask()->task()->versions()) {
            Version *v = elv;
            values[&_problem.var("vc_" + *t + "_" + *v).var] = v->gpu_frequency();
        }

        _problem.add_constraint(
                _problem.var("gpu_frequency_" + *t) - ILPExpressionSum(values) == 0,
                "GPU_frequency_" + *t
        );
    }

}

void ILPEnergyAwareDVFS::task_pairs_require_gpu() {
    //checks if in a task pair both tasks require the gpu
    auto st = sched->schedjobs();
    for (auto it = st.begin(), et = st.end(); it != et; ++it) {
        for (auto jt = std::next(it); jt != et; ++jt) {
            SchedJob *ti = *it, *tj = *jt;
            // logical AND over the two required gpus
            _problem.add_constraint(
                    _problem.var("gpu_required_" + *ti ) + _problem.var("gpu_required_" + *tj) - _problem.var("task_pairs_require_gpu_"+ *ti +"_" + *tj)  <= 1,
                    "Task_pairs_require_gpu_1_" + *ti + "_" + *tj
            );

            _problem.add_constraint(
                    _problem.var("gpu_required_" + *ti )  - _problem.var("task_pairs_require_gpu_"+ *ti +"_" + *tj) >= 0,
                    "Task_pairs_require_gpu_2_" + *ti + "_" + *tj
            );

            _problem.add_constraint(
                    _problem.var("gpu_required_"  + *tj) - _problem.var("task_pairs_require_gpu_"+ *ti +"_" + *tj) >= 0,
                    "Task_pairs_require_gpu_3_" + *ti + "_" + *tj
            );

            _problem.var("task_pairs_require_gpu_"+ *ti +"_" + *tj).var.binary();
        }
    }
}

void ILPEnergyAwareDVFS::gpu_task_orders() {
    //calculate order of tasks that require the gpu; either ti comes first or tj comes first
    auto st = sched->schedjobs();
    for (auto it = st.begin(), et = st.end(); it != et; ++it) {
        for (auto jt = std::next(it); jt != et; ++jt) {
            SchedJob *ti = *it, *tj = *jt;
            // logical AND over the two required gpus
            _problem.add_constraint(
                    _problem.var("gpu_task_order_" + *ti + "_" + *tj) + _problem.var("gpu_task_order_" + *tj + "_" + *ti) - _problem.var("task_pairs_require_gpu_"+ *ti +"_" + *tj)  == 0,
                    "GPU_task_order_1_" + *ti + "_" + *tj
            );

            _problem.var("gpu_task_order_" + *ti + "_" + *tj).var.binary();
            _problem.var("gpu_task_order_" + *tj + "_" + *ti).var.binary();

        }
    }

    for (auto ti: st){
        for (auto tj: st){
            if (ti == tj) continue;

            _problem.add_constraint(
                    _problem.var("NOT_gpu_task_order_" + *ti + "_" + *tj) + _problem.var("gpu_task_order_" + *ti + "_" + *tj) == 1,
                    "NOT_GPU_task_order_1_" + *ti + "_" + *tj
            );

            _problem.add_constraint(
                    _problem.var("rho_" + *tj) + SeqTauMax * _problem.var("NOT_gpu_task_order_" + *ti + "_" + *tj)  - _problem.var("rho_" + *ti) - _problem.var("wcet_" + *ti) >= 0,
                    "GPU_task_order_2_" + *ti + "_" + *tj
                    );

            _problem.var("NOT_gpu_task_order_" + *ti + "_" + *tj).var.binary();
        }
    }
}

void ILPEnergyAwareDVFS::gpu_frequency_time() {
    //calcualte amount of time in each GPU frequency
    //("gpu_required_" + *ti) ^ ("gpu_frequency_" + *t == f)

    for (auto cop: tg.coprocessors()) {
        for (auto freq_watt: cop->target_voltage_island->frequency_watt) {
            std::map<ILPVariable *, int256_t> values;
            auto f = freq_watt.first;
            for (SchedJob *t: sched->schedjobs()) {
                //only introduce these constraints if there is a version that can be scheduled on the GPU at that frequency!
                bool right_frequency = false;
                for (auto v: t->task()->versions()) {
                    if (v->gpu_frequency() == f) right_frequency = true;
                }
                if (!right_frequency) continue;

                // calculate difference between frequency and task_frequency
                _problem.add_constraint(
                        _problem.var("gpu_frequency_diff_" + *t + "_" + to_string(f)) + _problem.var("gpu_frequency_" + *t) == f,
                        "GPU_task_frequency_" + *t + "_" + to_string(f)
                );


                //cast difference to boolean where its 1 if they are DIFFERENT
                _problem.add_constraint(
                        (-max_cpu_frequency * _problem.var("gpu_bool_diff_" + *t + "_" + to_string(f))) - _problem.var("gpu_frequency_diff_" + *t + "_" + to_string(f)) <= 0,
                        "GPU_task_frequency_boolean_1_" + *t + "_" + to_string(f)
                );

                _problem.add_constraint(
                        (max_cpu_frequency * _problem.var("gpu_bool_diff_" + *t + "_" + to_string(f))) -
                        _problem.var("gpu_frequency_diff_" + *t + "_" + to_string(
                                f)) >= 0,
                        "GPU_task_frequency_boolean_2_" + *t + "_" + to_string(f)
                );

                _problem.add_constraint(
                        _problem.var("gpu_bool_diff_" + *t + "_" + to_string(f)) - (max_cpu_frequency + 1) * _problem.var("gpu_bool_temp_" + *t + "_" + to_string(f)) - _problem.var("gpu_frequency_diff_" + *t + "_" + to_string(f)) <= 0,
                        "GPU_task_frequency_boolean_3_" + *t + "_" + to_string(f)
                );

                _problem.add_constraint(
                        _problem.var("gpu_bool_temp_" + *t + "_" + to_string(f)) +
                        max_cpu_frequency * _problem.var("gpu_bool_temp_" + *t + "_" + to_string(
                                f)) + _problem.var("gpu_bool_diff_" + *t + "_" + to_string(f)) +
                        _problem.var("gpu_frequency_diff_" + *t + "_" + to_string(
                                f)) <= 1 + max_cpu_frequency,
                        "GPU_task_frequency_boolean_4_" + *t + "_" + to_string(f)
                );


                //NOT
                _problem.add_constraint(
                        _problem.var("gpu_bool_diff_" + *t + "_" + to_string(f)) +
                        _problem.var("gpu_bool_same_" + *t + "_" + to_string(
                                f)) == 1,
                        "GPU_task_frequency_NOT_1_" + *t + "_" + to_string(f)
                );


                // logical AND between same frequency and gpu required
                _problem.add_constraint(
                        _problem.var("gpu_bool_same_" + *t + "_" + to_string(f)) + _problem.var("gpu_required_" + *t) -
                        _problem.var("gpu_required_and_same_frequency_" + *t + "_" + to_string(
                                f)) <= 1,
                        "Gpu_required_and_same_frequency_1_" + *t + "_" + to_string(f)
                );

                _problem.add_constraint(
                        _problem.var("gpu_required_" + *t) -
                        _problem.var("gpu_required_and_same_frequency_" + *t + "_" + to_string(
                                f)) >= 0,
                        "Gpu_required_and_same_frequency_2_" + *t + "_" + to_string(f)
                );

                _problem.add_constraint(
                        _problem.var("gpu_bool_same_" + *t + "_" + to_string(f)) -
                        _problem.var("gpu_required_and_same_frequency_" + *t + "_" + to_string(
                                f)) >= 0,
                        "Gpu_required_and_same_frequency_3_" + *t + "_" + to_string(f)
                );

                // multiplication
                _problem.add_constraint(
                        _problem.var("gpu_time_required_" + *t + "_" + to_string(f)) -
                        SeqTauMax * _problem.var("gpu_required_and_same_frequency_" + *t + "_" + to_string(
                                f)) <= 0,
                        "Gpu_time_required_1_" + *t + "_" + to_string(f)
                );

                _problem.add_constraint(
                        _problem.var("gpu_time_required_" + *t + "_" + to_string(f)) - _problem.var("wcet_" + *t) <= 0,
                        "Gpu_time_required_2_" + *t + "_" + to_string(f)
                );

                _problem.add_constraint(
                        SeqTauMax * _problem.var("gpu_required_and_same_frequency_" + *t + "_" + to_string(f)) +
                        _problem.var("wcet_" + *t) - _problem.var("gpu_time_required_" + *t + "_" + to_string(
                                f)) <= SeqTauMax,
                        "Gpu_time_required_3_" + *t + "_" + to_string(f)
                );

                // final summation
                values[&_problem.var("gpu_time_required_" + *t + "_" + to_string(f)).var] = 1;



                // all the fun new variables
                _problem.add_bounds(_problem.var("gpu_time_required_" + *t + "_" + to_string(f)).var, 0, SeqTauMax);
                _problem.var("gpu_bool_diff_" + *t + "_" + to_string(f)).var.binary();
                _problem.var("gpu_bool_same_" + *t + "_" + to_string(f)).var.binary();
                _problem.var("gpu_required_and_same_frequency_" + *t + "_" + to_string(f)).var.binary();

                _problem.add_bounds(_problem.var("gpu_frequency_diff_" + *t + "_" + to_string(f)).var, -max_gpu_frequency, max_gpu_frequency);
                _problem.var("gpu_bool_temp_" + *t + "_" + to_string(f)).var.binary();
            }
            //calculate gpu time spent in each frequency
            if (!values.empty())
                _problem.add_constraint(
                        _problem.var("gpu_time_in_" + to_string(f)) - ILPExpressionSum(values) == 0,
                        "GPU_time_in_" + to_string(f)
                );
            _problem.add_bounds(_problem.var("gpu_time_in_" + to_string(f)).var, 0, SeqTauMax);
        }
    }
}

void ILPEnergyAwareDVFS::objectiv_func() {
    if(optimal) {
        _problem.set_objective(_problem.var("globE").var, ILPProblem::Objective::MINIMIZE);
    } else {
        _problem.set_objective(_problem.var("unused").var, ILPProblem::Objective::MINIMIZE);
    }
    if(tg.global_available_energy() > 0) {
        _problem.add_constraint(_problem.var("globE") <= (long) tg.global_available_energy());
    }
}

void ILPEnergyAwareDVFS::linkObj() {
    BaseILPEnergyAware::linkObj(); //TODO: Is this correct? How did this change? vs:
    std::map<ILPVariable *, int256_t> values_dynamic_energy;

    for(SchedJob *t : sched->schedjobs()) {
        values_dynamic_energy[&_problem.var("wcec_" + *t).var] = -1;
    }

    _problem.add_constraint(_problem.var("globE") + ILPExpressionSum(values_dynamic_energy) - _problem.var("static_energy_gpu") - _problem.var("static_energy_cpu") - _problem.var("static_energy")  == 0);
}

void ILPEnergyAwareDVFS::static_energy_GPU() {
    std::map<ILPVariable *, int256_t> values_static_gpu;

    for (auto cop: tg.coprocessors()) {
        for (auto freq_watt: cop->target_voltage_island->frequency_watt) {
            auto f = freq_watt.first;
            auto w = freq_watt.second;
            values_static_gpu[&_problem.var("gpu_time_in_" + to_string(f)).var] = w;
        }
    }

    _problem.add_constraint(
            _problem.var("static_energy_gpu_temp") - ILPExpressionSum(values_static_gpu) == 0,
            "Static_energy_GPU_temp"
    );

    if (tg.time_unit() == "ms" && tg.power_unit()=="mW" && tg.energy_unit()=="mJ") {
        _problem.add_constraint(
                1000 * _problem.var("static_energy_gpu") -  _problem.var("static_energy_gpu_temp") <= 0,
                "static_energy_gpu_upper"
        );
        _problem.add_constraint(
                1000 * _problem.var("static_energy_gpu") - _problem.var("static_energy_gpu_temp") >= -999,
                "static_energy_gpu_lower"
        );
    }else if (tg.time_unit() == "s" && tg.power_unit()=="W" && tg.energy_unit()=="J"){
        _problem.add_constraint(
                _problem.var("static_energy_gpu") -_problem.var("static_energy_gpu_temp") == 0,
                "static_energy_gpu_lower_upper"
        );
    }else if (tg.time_unit() == "us" && tg.power_unit()=="uW" && tg.energy_unit()=="uJ"){
        _problem.add_constraint(
                1000000 * _problem.var("static_energy_gpu") - _problem.var("static_energy_gpu_temp") <= 0,
                "static_energy_gpu_upper"
        );
        _problem.add_constraint(
                1000000 * _problem.var("static_energy_gpu") - _problem.var("static_energy_gpu_temp") >= -999999,
                "static_energy_gpu_lower"
        );
    }else if (tg.time_unit() == "ds" && tg.power_unit()=="mW" && tg.energy_unit()=="mJ"){
        _problem.add_constraint(
                10 * _problem.var("static_energy_gpu") - _problem.var("static_energy_gpu_temp") <= 0,
                "static_energy_gpu_upper"
        );
        _problem.add_constraint(
                10 * _problem.var("static_energy_gpu") - _problem.var("static_energy_gpu_temp") >= -9,
                "static_energy_gpu_lower"
        );
    }else{
        Utils::WARN("ILPEnergyAwareDVFS::static_energy_GPU in energy_aware_DVFS.cpp cannot compute static GPU energy properly as the time and power units dont match any of the predetermined cases");
    }

    // bounds the additional wattage due to GPU
    uint64_t additional_watt = 0;
    for (auto vi: tg.coprocessors()){
        uint64_t vi_max = 0;
        for (auto frequency_watt: vi->target_voltage_island->frequency_watt){
            if (frequency_watt.second > vi_max) vi_max = frequency_watt.second;
        }
        additional_watt += vi_max;
    }

    _problem.add_bounds(_problem.var("static_energy_gpu").var, 0, SeqTauMax * (additional_watt));
    _problem.add_bounds(_problem.var("static_energy_gpu_temp").var, 0, SeqTauMax * (additional_watt) * 10);

}

void ILPEnergyAwareDVFS::multiversion_mapping() {
    auto st = sched->schedjobs();

    //the mapping of a task is equal to the mapping of one version
    for(ComputeUnit *p : tg.processors()) {
        for(SchedJob *t : st) {
            std::map<ILPVariable *, int256_t> values;

            for(Version* elv : t->task()->versions()) {
                Version *v = elv;
                values[&_problem.var("q_" + *v + "_" + *p).var] = 1;
            }

            _problem.add_constraint(_problem.var("p_" + *t + "_" + *p) - ILPExpressionSum(values) == 0);
        }
    }

    //ensure that the unicity of mapping of the chosen version
    for(SchedJob *t : st) {
        for(Version* elv : t->task()->versions()) {
            Version *v = elv;
            std::map<ILPVariable *, int256_t> values;

            for(ComputeUnit *p : tg.processors()) {
                values[&_problem.var("q_" + *v + "_" + *p).var] = -1;
                _problem.var("q_" + *v + "_" + *p).var.binary();
            }

            _problem.add_constraint(_problem.var("vc_" + *t + "_" + *v) + ILPExpressionSum(values) == 0);
            _problem.var("vc_" + *t + "_" + *v).var.binary();
        }
    }

    //ensure that versions will be placed on cores for which they are designed
    vector<ComputeUnit*> cores = tg.processors();
    sort(cores.begin(), cores.end());
    for(std::pair<std::string, Task*> t : tg.tasks()) {
        for(Version* elv : t.second->versions()) {
            set<ComputeUnit*> forced = elv->force_mapping_proc();
//            sort(forced.begin(), forced.end()); //TODO: set_difference requires a sorted range, does it still work?
            vector<ComputeUnit*> inter(cores.size() - forced.size()); //target for processors that are not allowed
            vector<ComputeUnit*>::iterator it_inter = set_difference(cores.begin(), cores.end(), forced.begin(), forced.end(), inter.begin()); //determines difference between allowed cores and all cores
            for(ComputeUnit *p : inter) {
                _problem.add_constraint(
                        _problem.var("q_" + *elv + "_" + *p) == 0,
                        "Forbid" + *elv + *p
                );
            }
        }
    }
}

void ILPEnergyAwareDVFS::wcet() {
    for(SchedJob *t : sched->schedjobs()) {
        std::map<ILPVariable *, int256_t> values;

        for(Version* elv : t->task()->versions()) {
            Version *v = elv;
            values[&_problem.var("vc_" + *t + "_" + *v).var] = v->C();
        }

        _problem.add_constraint(
                _problem.var("wcet_" + *t) - ILPExpressionSum(values) == 0,
                "Wcet_" + *t
        );
    }
}

void ILPEnergyAwareDVFS::wcec() {
    for(SchedJob *t : sched->schedjobs()) {
        std::map<ILPVariable *, int256_t> values;

        for(Version* elv : t->task()->versions()) {
            Version *v = elv;
            values[&_problem.var("vc_" + *t + "_" + *v).var] = v->C_E();
        }

        _problem.add_constraint(
                _problem.var("wcec_" + *t) - ILPExpressionSum(values) == 0,
                "Wcec_" + *t
        );
    }
}

void ILPEnergyAwareDVFS::static_energy() {
    //does division for the static energy with respect to makespan
    if (tg.time_unit() == "ms" && tg.power_unit()=="mW" && tg.energy_unit()=="mJ") {
        _problem.add_constraint(
                1000 * _problem.var("static_energy") -  _problem.var("static_energy_temp") <= 0,
                "static_energy_upper"
        );
        _problem.add_constraint(
                1000 * _problem.var("static_energy") - _problem.var("static_energy_temp") >= -999,
                "static_energy_lower"
        );
    }else if (tg.time_unit() == "s" && tg.power_unit()=="W" && tg.energy_unit()=="J"){
        _problem.add_constraint(
                _problem.var("static_energy") -_problem.var("static_energy_temp") == 0,
                "static_energy"
        );
    }else if (tg.time_unit() == "us" && tg.power_unit()=="uW" && tg.energy_unit()=="uJ"){
        _problem.add_constraint(
                1000000 * _problem.var("static_energy") - _problem.var("static_energy_temp") <= 0,
                "static_energy_upper"
        );
        _problem.add_constraint(
                1000000 * _problem.var("static_energy") - _problem.var("static_energy_temp") >= -999999,
                "static_energy_lower"
        );
    }else if (tg.time_unit() == "ds" && tg.power_unit()=="mW" && tg.energy_unit()=="mJ"){
        _problem.add_constraint(
                10 * _problem.var("static_energy") - _problem.var("static_energy_temp") <= 0,
                "static_energy_upper"
        );
        _problem.add_constraint(
                10 * _problem.var("static_energy") - _problem.var("static_energy_temp") >= -9,
                "static_energy_lower"
        );
    }else{
        Utils::WARN("ILPEnergyAwareDVFS::static_energy in energy_aware_ECRTS2020.cpp cannot compute energy properly as the time and power units dont match any of the predetermined cases");
    }

    _problem.add_bounds(_problem.var("static_energy").var, 0, SeqTauMax * (tg.base_watt()));
    _problem.add_bounds(_problem.var("static_energy_temp").var, 0, SeqTauMax * (tg.base_watt())*10);
}

void ILPEnergyAwareDVFS::static_energy_temp() {
    _problem.add_constraint(
            _problem.var("static_energy_temp") - tg.base_watt() * _problem.var("makespan")  == 0,
            "static_energy_temp"
    );
}

void ILPEnergyAwareDVFS::multiversion_selection() {
    for(SchedJob *t : sched->schedjobs()) {
        std::map<ILPVariable *, int256_t> values;

        if(t->task()->versions().size() == 0)
            throw CompilerException("ILP gen energy aware", "Each task must have at least one version, one is generally automatically added by the coordination parser, what happen dude?");

        for(Version* elv : t->task()->versions()) {
            Version *v = elv;
            values[&_problem.var("vc_" + *t + "_" + *v).var] = 1;
        }

        _problem.add_constraint(ILPExpressionSum(values) == 1);
    }
}

void ILPEnergyAwareDVFS::precedence_samecore() {
    // precedence same core
    auto st = sched->schedjobs();
    for (auto i = st.begin(), e = st.end(); i != e; ++i) {
        for (auto j = std::next(i); j != e; ++j) {
            // m_i_j = am_i_j + am_j_i
            _problem.add_constraint(
                    _problem.var("m_" + **i + "_" + **j) - _problem.var("am_" + **i + "_" + **j) - _problem.var("am_" + **j + "_" + **i) == 0,
                    "PR_" + **i + "_" + **j
            );

            _problem.var("am_" + **i + "_" + **j).var.binary();
            _problem.var("am_" + **j + "_" + **i).var.binary();
        }
    }
}

void ILPEnergyAwareDVFS::causality() {
    for(SchedJob *t : sched->schedjobs()) {
        for(SchedElt *el : t->previous()) {
            // \rho_j + WCET_j <= \rho_i
            _problem.add_constraint(
                    _problem.var("rho_" + *t) - _problem.var("rho_" + *el) - _problem.var("wcet_" + *el) >= 0,
                    "Cau_1_" + *t + "_" + *el
            );
        }
    }
}

void ILPEnergyAwareDVFS::postsolve(std::map<std::string, long long> tmp_results) {
    for(SchedJob *st : sched->schedjobs()) {
    //TODO: gpu frequency!
        string lbl = "wcec_" + *st;
        string freq = "frequency_" + *st;
        string gpu_freq = "gpu_frequency_" + *st;
        if(!tmp_results.count(lbl))
            throw CompilerException("ILP postsolve", lbl+" not found in result");
        st->wce(tmp_results.at(lbl));
        st->frequency(tmp_results.at(freq));
        st->gpu_frequency(tmp_results.at(gpu_freq));

        for(Version* elv : st->task()->versions()) {
            string lbl = "vc_"+st->ilp_label() +"_"+elv;

            if(tmp_results.count(lbl) && tmp_results.at(lbl) == 1) {
                if(!st->selected_version().empty())
                    throw CompilerException("ILP postsolve version", "Multiple versions selected by the ILP for task "+st->task()->id());
                st->selected_version(elv->id());
            }
        }
    }
    sched->energy(tmp_results.at("globE"));
}

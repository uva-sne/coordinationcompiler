/*!
 * \file ILPFormGen.cpp
 * \copyright GNU Public License v3.
 * \date 2017
 * \author Benjamin Rouxel <benjamin.rouxel@uva.nl>, University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "BaseILPStrategy.hpp"
#include "ScheduleFactory.hpp"

using namespace std;

void BaseILPStrategy::add_header_comments() {
    _problem.header_comments.push_back("Problem name: Scheduling task graph");
    _problem.header_comments.push_back("Correspondances:");

    for (SchedJob * j : sched->schedjobs()) {
        _problem.header_comments.push_back("  " + j->task()->id() + " -> " + j->ilp_label());
        for(Version *v : j->task()->versions()) {
            _problem.header_comments.push_back("    " + v->id() + " -> " + v->ilp_label());
            for(Phase *p : v->phases()) {
                _problem.header_comments.push_back("      " + p->id() + " -> " + p->ilp_label());
            }
        }
    }
}

long long BaseILPStrategy::get_result(const string &lbl) {
    if(!tmp_results.count(lbl))
        throw CompilerException("ILP postsolve", lbl+" not found in result");
    return tmp_results.at(lbl);
}

void BaseILPStrategy::postsolve(const map<string, long long> &result) {
    tmp_results.insert(result.begin(), result.end());
    
    if(conf.runtime.rtt == runtime_task_e::AER)
        postsolve_aer();
    else if(conf.runtime.rtt == runtime_task_e::PREM)
        postsolve_prem();
    else if(conf.runtime.rtt == runtime_task_e::SINGLEPHASE)
        postsolve_singlephase();
}

void BaseILPStrategy::postsolve_aer() {
    if (sched->status() == ScheduleProperties::sched_statue_e::SCHED_UNSCHEDULABLE) return;
    
    for (const std::pair<std::string, long long> el : tmp_results) {
        Utils::DEBUG(el.first + " : " + to_string(el.second));
    }

    for (Task *t : tg.tasks_it()) {
        SchedJob *st = ScheduleFactory::build_job(sched, t, 0);
        st->rt(get_result("rho_" + *t));
        st->wct(get_result("wcet_" + *t));

        for (ComputeUnit *p : tg.processors()) {
            if (get_result("p_" + *t + "_" + *p)) {
                sched->map_core(st, sched->schedcore(p));
                break;
            }
        }

        sched->schedule(st);

        // suspicious code: this only considers the first inport/outport
        string datatypeR = (t->predecessor_tasks().size() > 0) ? t->predecessor_tasks().begin()->second[0]->to()->token_type() : "void";
        string datatypeW = (t->successor_tasks().size() > 0) ? t->successor_tasks().begin()->second[0]->from()->token_type() : "void";

        if (conf.archi.interconnect.burst == "none") {
            SchedPacket *rpac = st->packet_list(0);
            if(rpac == nullptr) {
                rpac = ScheduleFactory::build_packet(sched, st, 0, "r", st, 1);
            }
            rpac->rt(get_result("rho_r_" + *t));
            rpac->set_transmission(1, get_result("DR_" + *t) / conf.datatypes.at(datatypeR).size_bits, datatypeR, get_result("delay_r_" + *t));
            sched->schedule(rpac);

            SchedPacket *wpac = st->packet_list(1);
            if(wpac == nullptr) {
                wpac = ScheduleFactory::build_packet(sched, st, 0, "w", st, 1);
            }
            wpac->rt(get_result("rho_w_" + *t));
            wpac->set_transmission(1, get_result("DW_" + *t) / conf.datatypes.at(datatypeW).size_bits, datatypeW, get_result("delay_w_" + *t));
            sched->schedule(wpac);
        }
        else {
            vector<string> regions;
            map<SchedElt*, string> packets;
            for (Task::dep_t el : t->predecessor_tasks()) {
                Task *i = el.first;

                for (Connection *conn  : el.second) {
                    Connector *inport = conn->to();
                    uint32_t D_ij = inport->tokens();
                    vector<uint32_t> slots = CoreMemInterConnect::burst(conf, D_ij, conf.datatypes.at(inport->token_type()).size_bits);
                    if (slots.size() == 0) continue;

                    for (uint32_t s = 0; s < slots.size(); ++s) {
                        regions.push_back("MR" + *i + *t + to_string(s));
                        SchedPacket *rpac = ScheduleFactory::build_packet(sched, st, 0, "r", st, s);
                        rpac->rt(get_result("rho_r_" + *t + "_" + *i + "_" + to_string(s)));
                        rpac->set_transmission(slots.size(), slots[s], inport->token_type(), get_result("delay_r_" + *t + "_" + *i + "_" + to_string(s)));
                        packets[rpac] = "_r_" + *t + "_" + *i + "_" + to_string(s);
                        sched->schedule(rpac);
                    }
                }
            }

            regions.push_back("MR"+*t);
            packets[st] = "_e_" + *t;

            for (Task::dep_t el : t->successor_tasks()) {
                Task *i = el.first;
                for (Connection *conn  : el.second) {
                    Connector *outport = conn->from();
                    uint32_t D_ij = outport->tokens();
                    vector<uint32_t> slots = CoreMemInterConnect::burst(conf, D_ij, conf.datatypes.at(outport->token_type()).size_bits);
                    if (slots.size() == 0) continue;

                    for (uint32_t s = 0; s < slots.size(); ++s) {
                        regions.push_back("MR" + *t + *i + to_string(s));
                        SchedPacket *wpac = ScheduleFactory::build_packet(sched, st, 0, "w", st, s);
                        wpac->rt(get_result("rho_w_" + *t + "_" + *i + "_" + to_string(s)));
                        wpac->set_transmission(slots.size(), slots[s], outport->token_type(), get_result("delay_w_" + *t + "_" + *i + "_" + to_string(s)));
                        packets[wpac] = "_w_" + *t + "_" + *i + "_" + to_string(s);
                        sched->schedule(wpac);
                    }
                }
            }
        }
    }
}

void BaseILPStrategy::postsolve_prem() {
    if (sched->status() == ScheduleProperties::sched_statue_e::SCHED_UNSCHEDULABLE) return;
    
    for (const std::pair<std::string, long long> el : tmp_results) {
        Utils::DEBUG(el.first + " : " + to_string(el.second));
    }

    for (Task *t : tg.tasks_it()) {
        SchedJob *st = ScheduleFactory::build_job(sched, t, 0);
        st->rt(get_result("rho_" + *t));
        st->wct(get_result("wcet_" + *t));

        for (ComputeUnit *p : tg.processors()) {
            if (get_result("p_" + *t + "_" + *p)) {
                sched->map_core(st, sched->schedcore(p));
                break;
            }
        }

        sched->schedule(st);

        // suspicious code: only considers first inport
        string datatypeR = (t->predecessor_tasks().size() > 0) ? t->predecessor_tasks().begin()->second[0]->to()->token_type() : "void";

        if (conf.archi.interconnect.burst == "none") {
            SchedPacket *rpac = ScheduleFactory::build_packet(sched, st, 0, "r", st, 1);
            rpac->rt(get_result("rho_r_" + *t));
            rpac->set_transmission(1, get_result("DR_" + *t) / conf.datatypes.at(datatypeR).size_bits, datatypeR, get_result("delay_r_" + *t));
            sched->schedule(rpac);
        }
        else {
            vector<string> regions;
            map<SchedElt*, string> packets;
            for (Task *j : tg.tasks_it()) {
                for (Task::dep_t el : j->predecessor_tasks()) {
                    Task *i = el.first;
                    for (Connection *conn : el.second) {
                        Connector *inport = conn->to();
                        uint32_t D_ij = inport->tokens();
                        vector<uint32_t> slots = CoreMemInterConnect::burst(conf, D_ij, conf.datatypes.at(inport->token_type()).size_bits);
                        if (slots.size() == 0) continue;

                        for (uint32_t s = 0; s < slots.size(); ++s) {
                            regions.push_back("MR" + *i + *j + to_string(s));
                            SchedPacket *rpac = ScheduleFactory::build_packet(sched, st, 0, "r", st, s);
                            rpac->rt(get_result("rho_r_" + *j + "_" + *i + "_" + to_string(s)));
                            rpac->set_transmission(slots.size(), slots[s], inport->token_type(), get_result("delay_r_" + *j + "_" + *i + "_" + to_string(s)));
                            packets[rpac] = "_r_" + *j + "_" + *i + "_" + to_string(s);
                            sched->schedule(rpac);
                        }
                    }
                }

                regions.push_back("MR"+*j);
                packets[st] = "_e_" + *j;
            }
        }
    }
}

void BaseILPStrategy::postsolve_singlephase() {
    if (sched->status() == ScheduleProperties::sched_statue_e::SCHED_UNSCHEDULABLE) return;
    
    for (const std::pair<std::string, long long> el : tmp_results)
        Utils::DEBUG(el.first + " : " + to_string(el.second));

    for(SchedJob *t : sched->schedjobs()) {
        t->rt(get_result("rho_" + *t));
        t->wct(get_result("wcet_" + *t));

        SchedCore *core = nullptr;
        for (ComputeUnit *p : tg.processors()) {
            if (get_result("p_" + *t + "_" + *p)) {
                core = sched->schedcore(p);
                sched->map_core(t, core);
                break;
            }
        }
        if(core == nullptr)
            throw CompilerException("ILP", "Can't find the mapping core");

        sched->schedule(t);
        
        if(conf.archi.spm.assign_region) {
            for(SchedSpmRegion *spm : core->memory_regions()) {
                if(tmp_results.count("spmp_e_"+spm->label()+"_"+*t) && get_result("spmp_e_"+spm->label()+"_"+*t)) {
                    sched->map_spm(t, spm, get_result("startresa_e_"+*t), get_result("finresa_e_"+*t));
                }
            }
        }
        
        for(SchedPacket *pac : t->packet_list()) {
            if(pac == nullptr) continue;
            
            pac->rt(get_result("rho_"+pac->dirlbl()+"_" + *pac));
            pac->wct(get_result("delay_"+pac->dirlbl()+"_" + *pac));
            sched->schedule(pac);
            if(conf.archi.spm.assign_region) {
                for(SchedSpmRegion *spm : core->memory_regions()) {
                    if(tmp_results.count("spmp_"+pac->dirlbl()+"_"+spm->label()+"_"+*pac) && get_result("spmp_"+pac->dirlbl()+"_"+spm->label()+"_"+*pac))
                        sched->map_spm(pac,spm, get_result("startresa_"+pac->dirlbl()+"_"+*pac), get_result("finresa_"+pac->dirlbl()+"_"+*pac));
                }
            }
        }
    }
}

/*!
 * \file energy_aware_JRWRTC2019.hpp
 * \copyright GNU Public License v3.
 * \date 2019 
 * \author Julius Roeder <j.roeder@uva.nl>, University of Amsterdam
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ENERGYAWAREJRWRTC2019
#define ENERGYAWAREJRWRTC2019

#include "Solvers/ILP/ILP.hpp"
#include "BaseILPEnergyAware.hpp"
#include <stdint.h>

/**
 * Generate a scheduling problem that tries to minimise the overall energy
 * consumption
 * 
 * Work published at the JRWRTC workshop in 2019
 */
class ILPEnergyAware : public BaseILPEnergyAware {
public:
    //! \copydoc ILPFormGen::ILPFormGen
    explicit ILPEnergyAware(const SystemModel &m, const config_t &c, Schedule *s, bool o);
    
    virtual std::string help() override;
    
    //! \copydoc ILPFormGen::gen
    void gen() override;
    //! Add constraint to map version of tasks
    void multiversion_mapping();
    //! \copydoc ILPFormGen::decide_task_ordering_percore
    void decide_task_ordering_percore();
};

REGISTER_ILPSTRATEGY(ILPEnergyAware, "energy_aware-JRWRTC2019")

#endif
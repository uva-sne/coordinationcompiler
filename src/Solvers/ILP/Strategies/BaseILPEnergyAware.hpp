/*
 * Copyright (C) 2021 brouxel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* 
 * File:   BaseEnergyAware.hpp
 * Author: brouxel
 *
 * Created on 28 janvier 2021, 18:29
 */

#ifndef BASEENERGYAWARE_HPP
#define BASEENERGYAWARE_HPP

#include "BaseILPStrategy.hpp"
#include <boost/static_assert.hpp>
#include <boost/type_traits.hpp>
#include <map>

class BaseILPEnergyAware : public BaseILPStrategy {
public:
    /*!
     * \brief Constructor
     * \param m \copybrief ILPFormGen::tg
     * \param c \copybrief ILPFormGen::conf
     * \param s \copybrief ILPFormGen::sched
     * \param o \copybrief ILPFormGen::optimal 
     */
    BaseILPEnergyAware(const SystemModel &m, const config_t &c, Schedule *s, bool o) : BaseILPStrategy(m,c,s,o) {}
protected:

    uint64_t SeqTauMaxEnergy = -1; //!< Sequential length of the schedule
    frequencyinfo_t max_cpu_frequency = 0; //!< Maximum frequency for CPUs
    frequencyinfo_t max_gpu_frequency = 0; //!< Maximum frequency for GPUs

    //! Add an objective function to the problem
    virtual void objectiv_func();
    //! Link ILP variables to the obejctive function
    virtual void linkObj();
    //! Add mapping unicity constraint
    virtual void unicity();
    //! Add constraint to detect if 2 tasks are mapped on the same core
    virtual void detect_samecore();
    
    //! Add constraint to compute or set task WCET
    virtual void wcet();
    
    //! Add constraint to compute or set task WCEC
    virtual void wcec();
    
    //! Add constraint to enforce task dependencies
    virtual void causality();

    virtual //! Add constraint to select the version
    void multiversion_selection();
    
    /*! \brief Add constraint to avoid conflicting task
     * Tasks are in conflict if they are mapped on the same core and executing
     * at the same time.
     */
    virtual void conflicts();
    //! Enforce historical runtime \see config_t::runtime_e
    virtual void single_phase();
    /**
     * Add boundaries for task
     * @param deadline
     */
    virtual void add_bounds(uint64_t deadline);
};

#endif /* BASEENERGYAWARE_HPP */


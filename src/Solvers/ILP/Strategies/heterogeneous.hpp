/* 
 * Copyright (C) 2021 Benjamin Rouxel <benjamin.rouxel@uva.nl> 
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HETEROGENEOUS
#define HETEROGENEOUS

#include "Solvers/ILP/ILP.hpp"
#include "BaseILPStrategy.hpp"
#include "Solvers/AdditionalAnalyses/CRPD.hpp"

/**
 * \brief Generate an ILP problem to schedule an application
 * 
 * Focus is on heterogeneous platform where tasks can have mulitple versions.
 * Each version is split in phases where phases have target compute units 
 * restrictions.
 * 
 */
class ILPSmartHeterogeneous : public BaseILPStrategy {
public:
    //! \copydoc ILPFormGen::ILPFormGen
    explicit ILPSmartHeterogeneous(const SystemModel &m, const config_t &c, Schedule *s, bool o);
    //! \copydoc ILPFormGen::gen
    virtual void gen() override;
    
    virtual std::string help() override;
    
protected:
    CRPD *crpd;
    
    void objectiv_func();
    
    void unicity_version_job();
    void unicity();
    void detect_samecore();
    void force_mapping();
    void causality_phase();
    void causality_job();
    
    void decide_task_order();
    void decide_task_ordering_percore();
    void causality();
    void wcet();
    void wcet_version();
    
    void postsolve(const std::map<std::string, long long> &result);
};

REGISTER_ILPSTRATEGY(ILPSmartHeterogeneous, "heterogeneous");

#endif
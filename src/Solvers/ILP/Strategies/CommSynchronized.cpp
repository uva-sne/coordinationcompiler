/*
    Copyright (C) 2017  Benjamin Rouxel, Irisa/INRIA, benjamin.rouxel@inria.fr

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "CommSynchronized.hpp"

using namespace std;
IMPLEMENT_HELP(ILPSynchronized,
    Generate an ILP problem to schedule an application\n
    \n
    Communication between tasks are assumed to always imply the worst possible
    never an interference
)

ILPSynchronized::ILPSynchronized(const SystemModel &m, const config_t &c, Schedule *s, bool o) : BaseILPCommunicationAware(m, c, s, o) {}

void ILPSynchronized::gen() {
    SeqTauMax = (uint64_t)tg.global_deadline();
    if(SeqTauMax == 0 || (uint64_t)tg.sequential_length() < SeqTauMax)
        SeqTauMax = (uint64_t)tg.sequential_length();
    
    timinginfos_t lastiter = 0;
    for(SchedJob *t : sched->schedjobs()) {
        if(t->min_rt_period() > lastiter)
            lastiter = t->min_rt_period();
    }
    SeqTauMax += lastiter;

    add_header_comments();
    objectiv_func();

    unicity();
    detect_samecore();
    decide_task_order();
    decide_task_ordering_percore();
    conflicts();
    if(conf.runtime.rtt == runtime_task_e::SINGLEPHASE)
        single_phase();
    else
       _3_phases();
    wcet();
    causality();
    communication_delay();

    decide_communications_order();
    if(conf.archi.interconnect.behavior == "non-blocking") {
        conflicts_communications();
        causality_communications();
    }
    ensure_mutex();
    
//    if(conf.archi.spm.assign_region)
//        map_spm_region_to_task();

    force_mapping();

    multiperiod();

    linkObj();

    add_bounds(SeqTauMax);
    add_communication_bounds(SeqTauMax);
    
    if(conf.archi.interconnect.mechanism != "sharedonly") {
        for (SchedJob *t : sched->schedjobs()) {
            if(_problem.variables.count("remdata_r_" + *t))
                _problem.add_bounds(_problem.var("remdata_r_" + *t).var, 0, tg.interconnect()->getActiveWindowTime()-1);
            if(_problem.variables.count("remdata_w_" + *t))
                _problem.add_bounds(_problem.var("remdata_w_" + *t).var, 0, tg.interconnect()->getActiveWindowTime()-1);
            if(_problem.variables.count("unused_r_" + *t))
                _problem.add_bounds(_problem.var("unused_r_" + *t).var, 0, tg.interconnect()->getActiveWindowTime()-1);
            if(_problem.variables.count("unused_w_" + *t))
                _problem.add_bounds(_problem.var("unused_w_" + *t).var, 0, tg.interconnect()->getActiveWindowTime()-1);
        }
    }
}

void ILPSynchronized::communication_delay() {
    if(conf.runtime.rtt == runtime_task_e::SINGLEPHASE) return;

    if(conf.archi.interconnect.mechanism == "sharedonly") {
        map<ILPVariable *, int256_t> mapping;
        for(SchedElt *ep : sched->schedpackets()) {
            SchedPacket *p = (SchedPacket*) ep;
            packet_comm_delay(p, mapping, p->data());
        }
    }
    else if(conf.archi.interconnect.burst == "none") {
        for(SchedJob *t : sched->schedjobs()) {
            SchedPacket *rpac = t->packet_list(0);
            if(rpac != nullptr && rpac->data() > 0) {
                uint64_t sum = 0;
                map<ILPVariable *, int256_t> mapping;
                for(SchedElt *tmpel : rpac->previous()) {
                    SchedJob *el = tmpel->schedtask();
                    uint32_t data = t->task()->data_read(el->task());
                    mapping[&_problem.var("m_" + *el + "_" + *t).var] = data;
                    sum += data;
                }

                packet_comm_delay(rpac, mapping, sum);
            }

            SchedPacket *wpac = t->packet_list(1);
            if(wpac != nullptr && wpac->data() > 0) {
                uint64_t sum = 0;
                map<ILPVariable *, int256_t> mapping;
                for(SchedElt *tmpel : wpac->successors()) {
                    SchedJob *el = tmpel->schedtask();
                    uint32_t data = t->task()->data_written(el->task());
                    mapping[&_problem.var("m_" + *el + "_" + *t).var] = data;
                    sum += data;
                }

                packet_comm_delay(wpac, mapping, sum);
            }
        }
    }
    else {
        for(SchedElt *ep : sched->schedpackets()) {
            SchedPacket *p = (SchedPacket*) ep;
            map<ILPVariable *, int256_t> mapping;
            mapping[&_problem.var("m_" + *(p->schedtask()) + "_" + *(p->tofromschedtask())).var] = p->data();
            packet_comm_delay(p, mapping, p->data());
        }
    }
}

void ILPSynchronized::packet_comm_delay(SchedPacket *p, const std::map<ILPVariable *, int256_t> &mapping, uint64_t pdata) {
    string Dvar = "D"+boost::to_upper_copy(p->dirlbl())+"_"+*p;
    if(pdata == 0) {
        _problem.add_constraint(
            _problem.var("delay_"+p->dirlbl()+"_" + *p) == 0,
            "DelayR" + *p
        );
        _problem.add_constraint(_problem.var(Dvar) == 0, "Data" + *p);
    }
    else if(p->schedtask()->task()->force_read_delay() > 0) {
        _problem.add_constraint(
            _problem.var("delay_"+p->dirlbl()+"_" + *p) == p->schedtask()->task()->force_read_delay(),
            "DelayR" + *p
        );
        _problem.add_constraint(_problem.var(Dvar) == 0, "Data" + *p);
    }
    else if(conf.archi.interconnect.mechanism == "sharedonly") {
            _problem.add_constraint(
            _problem.var("delay_"+p->dirlbl()+"_" + *p) == tg.interconnect()->comm_delay(0, pdata, p->datatype(), 0),
            "DelayR" + *p
        );
        _problem.add_constraint(_problem.var(Dvar) == pdata, "Data" + *p);
    }
    else {
        uint64_t data = pdata * conf.datatypes.at(p->datatype()).size_bits;
        data = ceil(data/conf.archi.interconnect.bit_per_timeunit); // probably + (bit_per_timeunit - (sum % bit_per_timeunit)) to add extra bit if data not aligned, e.g. send 5 bits when 8 bits are sent at a time, need to add 3 zeroes

        _problem.add_constraint(_problem.var(Dvar) + ILPExpressionSum(mapping) == data, "Data" + *p);
        _problem.add_constraint(
            _problem.var(Dvar) - tg.interconnect()->getActiveWindowTime() * _problem.var("trslot_"+p->dirlbl()+"_" + *p) - _problem.var("remdata_"+p->dirlbl()+"_" + *p) == 0,
            "TRSlot"+p->dirlbl() + *p
        );
        _problem.add_constraint(
            _problem.var(Dvar) - tg.interconnect()->getActiveWindowTime() * _problem.var("waitslot_"+p->dirlbl()+"_" + *p) + _problem.var("unused_"+p->dirlbl()+"_" + *p) == 0,
            "WSlot"+p->dirlbl() + *p
        );
        _problem.add_constraint(
            _problem.var("unused_"+p->dirlbl()+"_" + *p) - _problem.var("remdata_"+p->dirlbl()+"_" + *p) <= 1,
            "Round"+p->dirlbl() + *p
        );
        _problem.add_constraint(
            _problem.var("delay_"+p->dirlbl()+"_" + *p) -
            tg.interconnect()->getActiveWindowTime() * _problem.var("trslot_"+p->dirlbl()+"_" + *p) -
            _problem.var("remdata_"+p->dirlbl()+"_" + *p) == 0,
            "Delay"+p->dirlbl() + *p
        );
    }
}

void ILPSynchronized::ensure_mutex() {
    if(conf.runtime.rtt == runtime_task_e::SINGLEPHASE) return;
    
    for(SchedPacket *i : sched->schedpackets()) {
        for(SchedPacket *j : sched->schedpackets()) {
            if(i == j || !i->schedtask()->task()->is_parallel(j->schedtask()->task())) continue;

            _problem.add_constraint(
                _problem.var("rho_"+i->dirlbl()+"_"+*i) 
                + _problem.var("delay_"+i->dirlbl()+"_"+*i) 
                - _problem.var("rho_"+j->dirlbl()+"_"+*j)
                + SeqTauMax * _problem.var("a_"+i->dirlbl()+j->dirlbl()+"_"+*i+"_"+*j) <= SeqTauMax,
            "Mutex_"+*i+"_"+*j);
        }
    }
}

/*
 * Copyright (C) 2021 brouxel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "BaseILPEnergyAware.hpp"

using namespace std;

void BaseILPEnergyAware::unicity() {
    // Unicity
    for (SchedElt* i : sched->schedjobs()) {
        std::map<ILPVariable *, int256_t> values;

        for (ComputeUnit* j : tg.processors()) {
            values[&_problem.var("p_" + *i + "_" + *j).var] = 1;
            _problem.var("p_" + *i + "_" + *j).var.binary();
        }

        _problem.add_constraint(ILPExpressionSum(values) == 1, "Uni_" + *i);
    }
}

void BaseILPEnergyAware::detect_samecore() {
    // same core
    auto st = sched->schedjobs();
    for (auto it = st.begin(), et=st.end() ; it != et ; ++it) {
        for (auto jt = std::next(it) ; jt != et ; ++jt) {
            SchedJob *i = *it, *j = *jt;

            std::map<ILPVariable *, int256_t> values;

            for (ComputeUnit *k : tg.processors()) {
                std::string lbl_tmp_m = "tmp_m_" + *i + "_" + *j + "_" + *k;
                values[&_problem.var(lbl_tmp_m).var] = 1;
                _problem.var(lbl_tmp_m).var.binary();

                _problem.add_constraint(
                    _problem.var("p_" + *i + "_" + *k) + _problem.var("p_" + *j + "_" + *k) - _problem.var(lbl_tmp_m) <= 1,
                    "OSC_1_" + *i + "_" + *j + "_" + *k
                );

                _problem.add_constraint(
                    _problem.var("p_" + *i + "_" + *k) - _problem.var(lbl_tmp_m) >= 0,
                    "OSC_2_" + *i + "_" + *j + "_" + *k
                );

                _problem.add_constraint(
                    _problem.var("p_" + *j + "_" + *k) - _problem.var(lbl_tmp_m) >= 0,
                    "OSC_3_" + *i + "_" + *j + "_" + *k
                );
            }

            _problem.add_constraint(
                ILPExpressionSum(values) - _problem.var("m_" + *i + "_" + *j) == 0,
                "OSC_4_" + *i + "_" + *j
            );

            _problem.add_constraint(
                _problem.var("m_" + *i + "_" + *j) - _problem.var("m_" + *j + "_" + *i) == 0,
                "OSC_5_" + *i + "_" + *j
            );

            _problem.var("m_" + *i + "_" + *j).var.binary();
            _problem.var("m_" + *j + "_" + *i).var.binary();
        }
    }
}

void BaseILPEnergyAware::single_phase() {
    for(SchedElt *t : sched->schedjobs()) {
        _problem.add_constraint(
            _problem.var("rho_" + *t) + _problem.var("wcet_" + *t) - _problem.var("end_" + *t) == 0,
            "EndComp_" + *t
        );
    }
}

void BaseILPEnergyAware::conflicts() {
    if(conf.runtime.rtt == runtime_task_e::SINGLEPHASE) {
        for(SchedElt *i : sched->schedjobs()) {
            for(SchedElt *j : sched->schedjobs()) {
                if(i == j) continue;
                // rho_i + WCETi <= rho_j + M (1 - amij) : no overlapping of exec phases on the same core
                _problem.add_constraint(
                    _problem.var("rho_" + *i) + _problem.var("wcet_" + *i) - _problem.var("rho_" + *j) + SeqTauMax * _problem.var("am_" + *i + "_" + *j) <= SeqTauMax,
                    "Conf1_" + *i + "_" + *j
                );
            }
        }
    }
    else {
        if(conf.archi.interconnect.behavior == "blocking") {
            for (Task *i : tg.tasks_it()) {
                for (Task *j : tg.tasks_it()) {
                    if(i == j) continue;
                    // rho_r_i + delay_r_i + WCETi + delay_w_i <= rho_r_j + M (1 - amij) : no overlapping of tasks on the same core
                    _problem.add_constraint(
                        _problem.var("end_" + *i) - _problem.var("rho_r_" + *j) + SeqTauMax * _problem.var("am_" + *i + "_" + *j) <= SeqTauMax,
                        "Conf_" + *i + "_" + *j
                    );
                }
            }
        }
            else if(conf.archi.interconnect.behavior == "non-blocking") {
            for (Task *i : tg.tasks_it()) {
                for (Task *j : tg.tasks_it()) {
                    if(i == j) continue;
                    // rho_i + WCETi <= rho_j + M (1 - amij) : no overlapping of exec phases on the same core
                    _problem.add_constraint(
                        _problem.var("rho_" + *i) + _problem.var("wcet_" + *i) - _problem.var("rho_" + *j) + SeqTauMax * _problem.var("am_" + *i + "_" + *j) <= SeqTauMax,
                        "Conf1_" + *i + "_" + *j
                    );
                }
            }
        }
    }
}

void BaseILPEnergyAware::add_bounds(uint64_t deadline) {
    _problem.add_bounds(_problem.var("makespan").var, 0, deadline);
    for(SchedElt *t : sched->schedjobs()) {
        _problem.add_bounds(_problem.var("rho_" + *t).var, 0, deadline);
        _problem.add_bounds(_problem.var("end_" + *t).var, 0, deadline);
    }
}

void BaseILPEnergyAware::linkObj() {
    for (SchedElt *i : sched->schedjobs()) {
        _problem.add_constraint(
            _problem.var("end_" + *i) - _problem.var("makespan") <= 0,
            "OC_" + *i
        );
    }
    map<ILPVariable *, int256_t> values;

    for(SchedElt *t : sched->schedjobs()) {
        values[&_problem.var("wcec_" + *t).var] = -1;
    }

    _problem.add_constraint(_problem.var("globE") + ILPExpressionSum(values) - _problem.var("static_energy") == 0);
}

void BaseILPEnergyAware::objectiv_func() {
    if(optimal) {
        _problem.set_objective(_problem.var("globE").var, ILPProblem::Objective::MINIMIZE);
    } else {
        _problem.set_objective(_problem.var("unused").var, ILPProblem::Objective::MINIMIZE);
    }
    if(tg.global_available_energy() > 0) {
        //TODO if we change the type of energycons to a float we have to e.g. floor the available energy in the next line
        _problem.add_constraint(_problem.var("globE") <= (int)(tg.global_available_energy()));
    }
}

void BaseILPEnergyAware::wcet() {
    for(SchedElt *t : sched->schedjobs()) {
        map<ILPVariable *, int256_t> values;

        for(Version *v : t->schedtask()->task()->versions()) {
            values[&_problem.var("vc_" + *t + "_" + *v).var] = v->C();
        }

        _problem.add_constraint(
            _problem.var("wcet_" + *t) - ILPExpressionSum(values) == 0,
            "Wcet_" + *t
        );
    }
}

void BaseILPEnergyAware::wcec() {
    for(SchedElt *t : sched->schedjobs()) {
        map<ILPVariable *, int256_t> values;

        for(Version *v : t->schedtask()->task()->versions()) {
            values[&_problem.var("vc_" + *t + "_" + *v).var] = (int)v->C_E(); //on another branch, the energycons_t is from another type which shouldn't trigger an error, @todo find a better way than that
        }

        _problem.add_constraint(
            _problem.var("wcec_" + *t) - ILPExpressionSum(values) == 0,
            "Wcec_" + *t
        );
    }
}

void BaseILPEnergyAware::causality() {
    for(SchedElt *t : sched->schedjobs()) {
        for(SchedElt *el : t->previous()) {
            // \rho_j + WCET_j <= \rho_i
            _problem.add_constraint(
                _problem.var("rho_" + *t) - _problem.var("rho_" + *el) - _problem.var("wcet_" + *el) >= 0,
                "Cau_1_" + *t + "_" + *el
            );
        }
    }
}

void BaseILPEnergyAware::multiversion_selection() {
    for(SchedElt *t : sched->schedjobs()) {
        map<ILPVariable *, int256_t> values;

        if(t->schedtask()->task()->versions().size() == 0)
            throw CompilerException("ILP gen energy aware", "Each task must have at least one version, one is generally automatically added by the coordination parser, what happen dude?");

        for(Version *v : t->schedtask()->task()->versions()) {
            values[&_problem.var("vc_" + *t + "_" + *v).var] = 1;
        }

        _problem.add_constraint(ILPExpressionSum(values) == 1);
    }
}
/*!
 * \file worst_concurrency.hpp
 * \copyright GNU Public License v3.
 * \date 2017
 * \author Benjamin Rouxel <benjamin.rouxel@uva.nl>, University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef FORM_WORSTCONCURRENCY_H
#define FORM_WORSTCONCURRENCY_H

#include "Solvers/ILP/ILP.hpp"
#include "BaseILPCommunicationAware.hpp"

/**
 * \brief Generate an ILP problem to schedule an application
 * 
 * Communication between tasks are assumed to always imply the worst possible
 * interference
 */
class ILPWorstConcurrency : public BaseILPCommunicationAware {
public:
    //! \copydoc ILPFormGen::ILPFormGen
    explicit ILPWorstConcurrency(const SystemModel &m, const config_t &c, Schedule *s, bool o);
    
    virtual std::string help() override;
    
    //! \copydoc ILPFormGen::gen
    virtual void gen() override;
    
protected:
    //! \copydoc ILPFormGen::communication_delay
    void communication_delay() override;
    void packet_comm_delay(SchedPacket *p, const std::map<ILPVariable *, int256_t> &mapping, uint64_t pdata);
};

REGISTER_ILPSTRATEGY(ILPWorstConcurrency, "worst_concurrency")

#endif
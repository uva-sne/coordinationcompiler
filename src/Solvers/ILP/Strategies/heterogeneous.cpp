/*!
 * \file heterogeneous.cpp
 * \copyright GNU Public License v3.
 * \date 2021
 * \author Benjamin Rouxel <benjamin.rouxel@uva.nl>, University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "heterogeneous.hpp"
#include "ScheduleFactory.hpp"

using namespace std;

IMPLEMENT_HELP(ILPSmartHeterogeneous,
Julius will write something here
)

ILPSmartHeterogeneous::ILPSmartHeterogeneous(const SystemModel &m, const config_t &c, Schedule *s, bool o) : BaseILPStrategy(m, c, s, o) {}

void ILPSmartHeterogeneous::gen() {
    if(conf.runtime.rtt != runtime_task_e::SINGLEPHASE)
        throw Todo("Do not deal with AER/PREM task models");
    if(optimal)
        throw Todo("Can't do optimal scheduling with this setup, need to adapt the objectiv func");
    
//    SeqTauMax = tg.global_deadline();
    SeqTauMax = tg.sequential_length() * 2;
    if(SeqTauMax == 0)
        SeqTauMax = tg.hyperperiod();
    if(SeqTauMax == 0)
        SeqTauMax = tg.sequential_length();
    if(SeqTauMax == 0)
        SeqTauMax = (uint64_t)-1;

    ScheduleFactory::build_elements(sched);
    crpd = new CRPD();

    add_header_comments();

    objectiv_func();

    detect_samecore();

    unicity_version_job();
    unicity();
    force_mapping();

    causality_phase();
    causality_job();
    causality();

    decide_task_order();
    decide_task_ordering_percore();

    wcet();
    wcet_version();
}
void ILPSmartHeterogeneous::objectiv_func() {
    _problem.set_objective(_problem.var("unused").var, ILPProblem::Objective::MINIMIZE);
}

/*
This is just some documentation for us about how to linearise equation \ref{eq:ilp:preemptiondelay}.
Part "\rho_{i,p} < \rho_{j,q} < \rho_{i,r}":
    g = \rho_{i,p} < \rho_{j,q} then linearised as:
        -\BigM*g + 1*h \le \rho_{j,q} - \rho_{i,p}
        \rho_{j,q} - \rho_{i,p} \le 1*g + \BigM*h - 1
    
    m = \rho_{j,q} < \rho_{i,r} then linearised as:
        -\BigM*m + 1*n \le \rho_{i,r} - \rho_{j,q}
        \rho_{i,r} - \rho_{j,q} \le 1*m + \BigM*n - 1
    Note that here, h and n will not be further used but they are mandatory and point the opposite of respectively g and m
    
Part "\implies":
    x = d_{i,p,j,q} \wedge  d_{j,q,i,r} \wedge g \wedge m
    \omega_{r,c} = C_{r,c} + CRPD_{p,q,r} * x
 */
void ILPSmartHeterogeneous::wcet() {
    for (SchedJob *i : sched->schedjobs()) {
        const Task *s = i->task();
        for(Version *u : i->task()->versions()) {
            vector<Phase*> phases = u->phases();
            std::vector<Phase *> wcet_done;

            // add the wcet for the first phase --> wcet is known for sure
            Phase *p = phases[0];
            _problem.add_constraint(
                _problem.var("tempw_"+* i+u+p)
                == p->C(),
                "TempWCET"+*i+u+p
            );
            wcet_done.push_back(p);

            // adds the delay constraints in the case of multiple phases that are not directly behind each other!
            // adds any sort of migration or entanglement between phases that MIGHT be on the same core/ core type
            for(auto pi= phases.begin(), et=phases.end() ; next(pi) != et ; ++pi) {
                // only care about the phases AFTER pi
                for(auto pr= next(pi), et2=phases.end() ; pr != et2 ; ++pr) {
                    // can continue if the two phases are the same
                    if (pr == pi) continue;
                    Phase *p = *pi;
                    Phase *r = *pr;
                    timinginfos_t v_crpd = crpd->get_CRPD(r->C());
                    // can continue if they are NOT on the same architecture
                    if (next(pi) == pr){
                        vector<ComputeUnit *> shared_cores(
                                p->force_mapping_proc().size() + r->force_mapping_proc().size());
                        auto ci = set_intersection(p->force_mapping_proc().begin(), p->force_mapping_proc().end(),
                                                   r->force_mapping_proc().begin(), r->force_mapping_proc().end(),
                                                   shared_cores.begin());
                        shared_cores.resize(distance(shared_cores.begin(), ci));
                        if (shared_cores.size() == 0) {
                            continue;
                        }
                    }
                    // if we already added the WCET calculation
                    if (find(wcet_done.begin(), wcet_done.end(), r) != wcet_done.end()) continue;
                    wcet_done.push_back(r);


                    // Entanglement & Migration
                    // adds the next phases WCET if the two phases are on the same architecture: i.e. next phase is also on the LITTLE CPU.
                    // Used for task migration with fixed migration points to different CPUs on the same architecture or if something is squeezed in between the two phases.
                    std::map<ILPVariable *, int256_t > values;
                    for (SchedJob *j : sched->schedjobs()) {
                        const Task *t = j->task();
                        if (s == t) continue;

                        // if any other task/version/phases are between p & r then add the CRPD
                        for (Version *v : j->task()->versions()) {
                            for (Phase *q : v->phases()) {

                                // Is phase rho_p smaller than rho_q
                                _problem.add_constraint(
                                        _problem.var("rho_" + *j + v + q) - _problem.var("rho_" + *i + u + p)
                                        - SeqTauMax * _problem.binvar("g_" + *i + u + p + r + j + v + q)
                                        <= 0,
                                        "P_before_q1" + *i + u + p + r + j + v + q
                                );
                                _problem.add_constraint(
                                        _problem.var("rho_" + *i + u + p) - _problem.var("rho_" + *j + v + q) +
                                        SeqTauMax * _problem.binvar("g_" + *i + u + p + r + j + v + q)
                                        <= SeqTauMax - 1,
                                    "P_before_q2" + *i + u + p + r + j + v + q
                                );

                                // is phase rho_q smaller than phase rho_r
                                _problem.add_constraint(
                                        _problem.var("rho_" + *i + u + r) - _problem.var("rho_" + *j + v + q)
                                        - SeqTauMax * _problem.binvar("m_" + *i + u + p + r + j + v + q)
                                        <= 0,
                                        "Q_before_r1" + *i + u + p + r + j + v + q
                                );
                                _problem.add_constraint(
                                        _problem.var("rho_" + *j + v + q) -_problem.var("rho_" + *i + u + r)
                                        + SeqTauMax * _problem.binvar("m_" + *i + u + p + r + j + v + q)
                                        <= SeqTauMax - 1,
                                        "Q_before_r2" + *i + u + p + r + j + v + q
                                );

                                // Computes o which indicates if there is entanglement. d indicates if there was migration.
                                // Next 4 constraints force o to 1 if d,d,g and m are all 1!
                                _problem.add_constraint(
                                        _problem.binvar("d_" + *i + u + p + j + v + q) + //if p and q are on the same core
                                        _problem.binvar("d_" + *i + u + r + j + v + q)  // if r and q are on the same core
                                        + _problem.binvar("g_" + *i + u + p + r + j + v + q)
                                        + _problem.binvar("m_" + *i + u + p + r + j + v + q)
                                        - _problem.binvar("o_" + *i + u + p + r + j + v + q) <= 3,
                                        "Entangled1" + *i + u + p + r + j + v + q
                                );
                                _problem.add_constraint(
                                        _problem.binvar("o_" + *i + u + p + r + j + v + q) -
                                        _problem.binvar("d_" + *i + u + p + j + v + q) <= 0,
                                        "Entangled2" + *i + u + p + r + j + v + q
                                );
                                _problem.add_constraint(
                                        _problem.binvar("o_" + *i + u + p + r + j + v + q) -
                                        _problem.binvar("d_" + *i + u + r + j + v + q) <= 0,
                                        "Entangled3" + *i + u + p + r + j + v + q
                                );
                                _problem.add_constraint(
                                        _problem.binvar("o_" + *i + u + p + r + j + v + q)
                                        - _problem.binvar("g_" + *i + u + p + r + j + v + q)
                                        <= 0,
                                        "Entangled4" + *i + u + p + r + j + v + q
                                );
                                _problem.add_constraint(
                                        _problem.binvar("o_" + *i + u + p + r + j + v + q)
                                        - _problem.binvar("m_" + *i + u + p + r + j + v + q)
                                        <= 0,
                                        "Entangled5" + *i + u + p + r + j + v + q
                                );
                                values[&_problem.binvar("o_" + *i + u + p + r + j + v + q).var]=1;
                            }
                        }
                    }

                    // summing the entanglement variables
                    values[&_problem.var("temp_o_" + *i + u + p + r).var]= -1;
                    _problem.add_constraint(
                            ILPExpressionSum(values) == 0,
                            "Entanglement accumulator" + *i + u + p + r
                    );

                    // casting the entannglement sum (temp_o) to binary
                    _problem.add_constraint(
                            SeqTauMax * _problem.binvar("o_" + *i + u + p + r) - _problem.var("temp_o_" + *i + u + p + r)  >= 0,
                            "Cast accumulator1" + *i + u + p + r
                    );

                    _problem.add_constraint(
                            SeqTauMax * _problem.binvar("o_" + *i + u + p + r) - _problem.var("temp_o_" + *i + u + p + r)  <= SeqTauMax - 1,
                            "Cast accumulator2" + *i + u + p + r
                    );

                    //Adds either migration OR entanglement cost (i.e. if
                    _problem.add_constraint(
                            _problem.var("tempw_" + *i + u + r)
                            + v_crpd * _problem.binvar("d_" + *i + u + p + i + u + r) /* migration cost */
                            - v_crpd * _problem.binvar("o_" + *i + u + p + r) /* entanglement cost */
                            == r->C() + v_crpd,
                            "TempWCET" + *i + u + p + r
                    );
                    // only want the migration or entanglement between the closest tasks.
                    // if we get here then the first and second loop phases are the closest.
                    // hence we can go to the next outer loop phase
                    break;
                }
            }

            // Adds all wcets not added befor (i.e. not impacted by entanglement etc
            for(auto pi= phases.begin(), et=phases.end() ; pi != et ; ++pi) {
                Phase *p = *pi;
                if (find(wcet_done.begin(), wcet_done.end(), p) != wcet_done.end()) continue;

                //adds the next phases wcet IF the two phases are NOT on the same architecture: e.g. shifting execution from CPU to GPU
                    _problem.add_constraint(
                            _problem.var("tempw_" + *i + u + p)
                            == p->C(), // wcet of p to w_(i,u,r)
                            "TempWCET" + *i + u + p
                    );
                    wcet_done.push_back(p);
            }
        }
    }
}

void ILPSmartHeterogeneous::wcet_version(){
    for(SchedJob *i : sched->schedjobs()) {
        const Task *t = i->task();
        for (Version *u : i->task()->versions()) {
            for (Phase *p : u->phases()) {

                //binary multiplication to set wcet of a phase ONLY if the version is selected
                _problem.add_constraint(
                        _problem.var("w_"+* i+u+p) - SeqTauMax * _problem.binvar("a_"+* t+u+i)  <= 0,
                        "WCET_Version1" + * i+u+p);

                _problem.add_constraint(
                        _problem.var("w_"+* i+u+p) - _problem.var("tempw_"+* i+u+p) <=  0,
                        "WCET_Version2"  + * i+u+p);

                _problem.add_constraint(
                        _problem.var("w_"+* i+u+p)  >=  0,
                        "WCET_Version3"  + * i+u+p);

                _problem.add_constraint(
                        _problem.var("w_"+* i+u+p) - _problem.var("tempw_"+* i+u+p) - SeqTauMax * _problem.binvar("a_"+* t+u+i)   >=  - int256_t(SeqTauMax) ,
                        "WCET_Version4" + * i+u+p);
            }
        }
    }
}

void ILPSmartHeterogeneous::unicity_version_job() {
    // only one version of a task per job is executed
    for(SchedJob *i : sched->schedjobs()) {
        const Task *t = i->task();
        std::map<ILPVariable *, int256_t> values;
        for(Version *u : t->versions()) {
            values[&_problem.binvar("a_" +*t+u+i).var] = 1;
        }
        _problem.add_constraint(ILPExpressionSum(values) == 1, "UniVers_" + *i);
    }
}

void ILPSmartHeterogeneous::unicity() {
    // all phases of one version of a task are scheduled once
    for(SchedJob *i : sched->schedjobs()) {
        const Task *t = i->task();
        for(Version *u : t->versions()) {
            for(Phase *p : u->phases()) {
                std::map<ILPVariable *, int256_t> values;
                for(ComputeUnit *c : tg.processors()) {
                    values[&_problem.binvar("b_" +*i+u+p+c).var] = 1; // a phase needs to be mapped to exactly one CU
                }
                
                values[&_problem.binvar("a_"+*t+u+i).var] = -1;
                _problem.add_constraint(ILPExpressionSum(values) == 0, "Uni_" +*i+u+p);
            }
        }
    }
}

void ILPSmartHeterogeneous::detect_samecore() {
    // detect if two phases are on the same core.
    for(SchedJob *i : sched->schedjobs()) {
        for(SchedJob *j : sched->schedjobs()) {
            for(Version *u : i->task()->versions()) {
                for(Version *v : j->task()->versions()) {
                    for(Phase *p : u->phases()) {
                        for(Phase *q : v->phases()) {
                            if(p == q) continue;
                            
                            std::map<ILPVariable *, int256_t> values;
                            
                            for (ComputeUnit *c : tg.processors()) {
                                string suffix_i = ""+* i+u+p;
                                string suffix_j = ""+* j+v+q;

                                values[&_problem.binvar("tmp_d_"+suffix_i+suffix_j+c).var] = 1;

                                _problem.add_constraint(
                                    _problem.binvar("b_"+suffix_i+c) + _problem.binvar("b_"+suffix_j+c) - _problem.binvar("tmp_d_"+suffix_i+suffix_j+c) <= 1,
                                    "OSC_1_" + suffix_i+suffix_j+c
                                );

                                _problem.add_constraint(
                                    _problem.binvar("b_" +suffix_i+c) - _problem.binvar("tmp_d_"+suffix_i+suffix_j+c) >= 0,
                                    "OSC_2_" + suffix_i+suffix_j+c
                                );

                                _problem.add_constraint(
                                    _problem.binvar("b_" +suffix_j+c) - _problem.binvar("tmp_d_"+suffix_i+suffix_j+c) >= 0,
                                    "OSC_3_" +suffix_i+suffix_j+c
                                );
                            }
                            _problem.add_constraint(
                                ILPExpressionSum(values) - _problem.binvar("d_"+* i+u+p + j+v+q) == 0,
                                "OSC_4_" + *i+u+p + j+v+q
                            );

                            _problem.add_constraint(
                                _problem.binvar("d_"+*i+u+p + j+v+q) - _problem.binvar("d_"+*j+v+q + i+u+p) == 0,
                                "OSC_5_" + *i+u+p + j+v+q
                            );
                        }
                    }
                }
            }
        }
    }
}

void ILPSmartHeterogeneous::force_mapping() {
    // need a sorted set of the compute units for the set_difference
    vector<ComputeUnit *> all_compute_units(tg.processors().begin(),  tg.processors().end());
    sort(all_compute_units.begin(), all_compute_units.end());

//    prevents phases from being mapped on to a certain processor
    for (Task *t : tg.tasks_it()) {
       for(Version *u : t->versions()) {
            for(Phase *p : u->phases()) {
                if(p->force_mapping_proc().empty())
                    continue;

                // getting all compute units that are allowed and sorting them.
                vector<ComputeUnit *> force_procs(p->force_mapping_proc().begin(),  p->force_mapping_proc().end());
                sort(force_procs.begin(),  force_procs.end());
                vector<ComputeUnit*> forbidden(tg.processors().size() + force_procs.size());
                // set difference between allowed compute units and ALL compute units.
                // TODO: Make sure that the sorting is not deleted; i.e. unit test it more.
                vector<ComputeUnit*>::iterator it = set_difference(all_compute_units.begin(), all_compute_units.end(),
                                                            force_procs.begin(), force_procs.end(),
                                                            forbidden.begin());
                forbidden.resize(distance(forbidden.begin(), it));
                for(ComputeUnit *c : forbidden) {
                    for(SchedJob *i : sched->schedjobs(t)) {
                        _problem.add_constraint(_problem.binvar("b_"+*i+u+p+c) == 0, "FobidMap_" +*i+u+p+c);
                    }
                }
            }
        }
   } 
    
}

void ILPSmartHeterogeneous::causality_phase() {
    // makes sure that phases are in the correct order; i.e. phase p of version/job has to be scheduled and finished before phase q
    for(SchedJob *i : sched->schedjobs()) {
        for(Version *u : i->task()->versions()) {
            vector<Phase*> phases = u->phases();
            for(auto pi= phases.begin(), et=phases.end() ; next(pi) != et ; ++pi) {
                Phase *p = *pi;
                Phase *q = *next(pi);
                
                _problem.add_constraint(
                    _problem.var("rho_"+*i+u+p) + _problem.var("w_"+*i+u+p) - _problem.var("rho_"+*i+u+q) == 0,
                    "CausaPhase"+*i+u+p+q
                );
            }
        }
    }
}

void ILPSmartHeterogeneous::causality_job() {
    // the start of a phase has to be larger than the minimum release time period and the end of a phase has to be smaller than the deadline of the current job
    // Only selected version/phase! Otherwise ALL phases of all versions have to be done beforehand
    for(SchedJob *i : sched->schedjobs()) {
        const Task *s = i->task();
        for(Version *u : i->task()->versions()) {
            for(Phase *p : u->phases()) {
                _problem.add_constraint(
                        _problem.var("rho_" + *i + u + p) >= i->min_rt_period(),
                        "Period" + *i + u + p
                );

                //if each task has a deadline
                // TODO Fix Period stuff
                if (i->min_rt_deadline() > 0) {
                    _problem.add_constraint(
                            _problem.var("rho_" + *i + u + p) + _problem.var("w_" + *i + u + p) - (SeqTauMax * _problem.var("inv_a_" +*s+u+i)) <= tg.hyperperiod() , // BIG O for makeing sure only the selected ones
                            "Deadline" + *i + u + p
                    );
                }else{
                    // if the DAG has a deadline
//                    if (i->successors().empty()) {
                        _problem.add_constraint(
                                _problem.var("rho_" + *i + u + p) + _problem.var("w_" + *i + u + p) - (SeqTauMax * _problem.var("inv_a_" +*s+u+i)) <= tg.hyperperiod(),
                                "Deadline" + *i + u + p
                        );
//                    }
                }
            }
        }
    }
}

void ILPSmartHeterogeneous::causality() {
    // all phases of a task must be done before all phases of the successor
    // this constraints data dependency
    // Connects the timing calculations to the version and target core selection
    for(SchedJob *i : sched->schedjobs()) {
        const Task *s = i->task();
        for(Version *u : i->task()->versions()) {
            for(Phase *p : u->phases()) {

                // need the inverse of a_ the nig O in Causa only should be activated if sui is NOT selected
                _problem.add_constraint(
                         _problem.var("inv_a_" + *s+u+i) + _problem.var("a_" + *s+u+i) == 1,
                        "Inv_a"+ *s+u+i
                        );
                
                for(SchedElt *j : i->successors()) {
                    for(Version *v : j->schedtask()->task()->versions()) {
                        for(Phase *q : v->phases()) {
                            _problem.add_constraint(
                                    // is s u i correct??
                                _problem.var("rho_"+*i+u+p) + _problem.var("w_"+*i+u+p) - _problem.var("rho_"+*j+v+q) - SeqTauMax * _problem.var("inv_a_"+*s+u+i) <= 0,
                                "Causa"+*i+u+p+j+v+q
                            );
                        }
                    }
                }
            }
        }
    }
}

void ILPSmartHeterogeneous::decide_task_order() {
    // order of two job/task/phases
    // only order i,j or ordering j,i is possible
    auto st = sched->schedjobs();
    for (auto it = st.begin(), et=st.end() ; it != et ; ++it) {
        SchedJob *i = *it;
        for(Version *u : i->task()->versions()) {
            for(Phase *p : u->phases()) {

                for(auto jt = std::next(it) ; jt != et ; ++jt) {
                    SchedJob *j = *jt;
                
                    for(Version *v : j->task()->versions()) {
                        for(Phase *q : v->phases()) {
                            if(p == q) continue;
                            
                            _problem.add_constraint(
                                _problem.binvar("e_"+* i+u+p + j+v+q) + _problem.binvar("e_"+* j+v+q + i+u+p) == 1,
                                "DTO"+*i+u+p + j+v+q
                            );
                        }
                    }
                }
            }
        }
    }
    // constrain ordering depending on if they are on the same core or not
    for (SchedJob *i : sched->schedjobs()) {
        for(Version *u : i->task()->versions()) {
            for(Phase *p : u->phases()) {
                
                for(SchedJob *j : sched->schedjobs()) {
                    for(Version *v : j->task()->versions()) {
                        for(Phase *q : v->phases()) {
                            if(p == q) continue;
                            
                            _problem.add_constraint(
                                _problem.binvar("e_"+* i+u+p + j+v+q) + _problem.binvar("d_"+* i+u+p + j+v+q) - _problem.binvar("f_"+* i+u+p + j+v+q) <= 1,
                                "PROSM_1_" +* i+u+p + j+v+q
                            );

                            _problem.add_constraint(
                                _problem.binvar("e_"+* i+u+p + j+v+q) - _problem.binvar("f_"+* i+u+p + j+v+q) >= 0,
                                "PROSM_2_" +* i+u+p + j+v+q
                            );

                            _problem.add_constraint(
                                _problem.binvar("d_"+* i+u+p + j+v+q) - _problem.binvar("f_"+* i+u+p + j+v+q) >= 0,
                                "PROSM_3_" +* i+u+p + j+v+q
                            );
                        }
                    }
                }
            }
        }
    }
            
}

void ILPSmartHeterogeneous::decide_task_ordering_percore() {
    // the end of one task has to be before the start of another task if they are on the same core
    for (SchedJob *i : sched->schedjobs()) {
        for(Version *u : i->task()->versions()) {
            for(Phase *p : u->phases()) {
        
                for(SchedJob *j : sched->schedjobs()) {
                    if(i == j) continue;
                    
                    for(Version *v : j->task()->versions()) {
                        for(Phase *q : v->phases()) {
                            
                            _problem.add_constraint(
                                _problem.var("rho_"+*i+u+p) + _problem.var("w_"+*i+u+p) - _problem.var("rho_"+*j+v+q) + SeqTauMax * _problem.binvar("f_"+* i+u+p + j+v+q) <= SeqTauMax,
                                "DTOPC"+*i+u+p + j+v+q
                            );
                        }
                    }
                }
            }
        }
    }
}

void ILPSmartHeterogeneous::postsolve(const map<string, long long> &result) {
    if (sched->status() == ScheduleProperties::sched_statue_e::SCHED_UNSCHEDULABLE) return;
    tmp_results.insert(result.begin(), result.end());
    
    for(SchedJob *i : sched->schedjobs()) {
        const Task *t = i->task();
        for(Version *u : i->task()->versions()) {
            long long vselect = get_result("a_"+* t+u+i);
            if(vselect == 0)
                continue;
            
            if(!i->selected_version().empty())
                throw CompilerException("ILP postsolve version", "Multiple versions selected by the ILP for task "+t->id());
            i->selected_version(u->id());
            
            SchedJob *cur = i;
            for(Phase *p : u->phases()) {
                cur->rt(get_result("rho_" +* i+u+p));
                cur->wct(get_result("w_" +* i+u+p));

                for (ComputeUnit *c : tg.processors()) {
                     if (get_result("b_"+* i+u+p+c)) {
                        sched->map_core(cur, sched->schedcore(c));
                        break;
                    }
                }
                sched->schedule(cur);
                cur = ScheduleFactory::build_job(sched, t, cur->min_rt_period());
            }
            //sched->rem_job(cur);
        }
    }
    sched->compute_makespan();
    Utils::INFO("\t====> Final Schedule length = "+to_string(sched->makespan()));
}
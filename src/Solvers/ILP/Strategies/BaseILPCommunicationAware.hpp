/*!
 * \file ILPFormGen.hpp
 * \copyright GNU Public License v3.
 * \date 2017
 * \author Benjamin Rouxel <benjamin.rouxel@uva.nl>, University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef BASEILPCOMMUNICATIONAWARE_HPP
#define BASEILPCOMMUNICATIONAWARE_HPP

#include "BaseILPStrategy.hpp"

class BaseILPCommunicationAware : public BaseILPStrategy {
public:
    /*!
     * \brief Constructor
     * \param m \copybrief ILPFormGen::tg
     * \param c \copybrief ILPFormGen::conf
     * \param s \copybrief ILPFormGen::sched
     * \param o \copybrief ILPFormGen::optimal 
     */
    BaseILPCommunicationAware(const SystemModel &m, const config_t &c, Schedule *s, bool o) : BaseILPStrategy(m,c,s,o) {}

protected:

    //! Add an objective function to the problem
    virtual void objectiv_func();
    //! Link ILP variables to the obejctive function
    virtual void linkObj();
    //! Add mapping unicity constraint
    virtual void unicity();
    //! Add constraint to detect if 2 tasks are mapped on the same core
    virtual void detect_samecore();
    
    //! Add constraint ordering task
    virtual void decide_task_order();
    //! Add constraint determining the ordering of communications
    virtual void decide_communications_order();
    //! Add constraint ordering task mapped on the same core
    virtual void decide_task_ordering_percore();
    //! Add constraint ordering communication mapped on the same core
    virtual void decide_communications_order_percore();
    
    /*! \brief Add constraint to avoid conflicting task
     * Tasks are in conflict if they are mapped on the same core and executing
     * at the same time.
     */
    virtual void conflicts();
    /*! \brief Add constraint to avoid conflicting communication
     * Communicaitons are in conflict if they are scheduled at the same time.
     */
    virtual void conflicts_communications();
    //! Enforce AER runtime \see config_t::runtime_e
    virtual void _3_phases();
    //! Enforce historical runtime \see config_t::runtime_e
    virtual void single_phase();
    //! Add constraint to enforce task dependencies
    virtual void causality();
    //! Add constraint to enforce communication dependencies
    virtual void causality_communications();
    
    //! Add constaint for periods
    virtual void multiperiod();
    
    //! Add SPM mapping cosntraints
    virtual void map_spm_region_to_task();
    
    //! Add constraint to compute or set task WCET
    virtual void wcet();
    //! Add constraint to compute or set communication cost
    virtual void communication_delay() = 0;
    
    /**
     * Add boundaries for task
     * @param deadline global deadline
     * @param bounds other boundaries
     */
    virtual void add_bounds(uint64_t deadline, const std::string & bounds);
    /**
     * Add boundaries for task
     * @param deadline
     */
    virtual void add_bounds(uint64_t deadline);
    /**
     * Add boundaries for communication
     * @param deadline global deadline
     * @param bounds other boundaries
     */
    virtual void add_communication_bounds(uint64_t deadline, const std::string & bounds);
    /**
     * Add boundaries for communication
     * @param deadline global deadline
     */
    virtual void add_communication_bounds(uint64_t deadline);

    //! Some task can only execute on specific processing units
    virtual void force_mapping();
};

#endif /* BASEILPCOMMUNICATIONAWARE_HPP */


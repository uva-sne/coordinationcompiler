/*!
 * \file energy_aware_JRWRTC2019.cpp
 * \copyright GNU Public License v3.
 * \date 2019 
 * \author Julius Roeder <j.roeder@uva.nl>, University of Amsterdam
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "energy_aware_JRWRTC2019.hpp"

using namespace std;

IMPLEMENT_HELP(ILPEnergyAware,
    Generate a scheduling problem that tries to minimise the overall energy consumption\n
    Work published at the JRWRTC workshop in 2019
)

ILPEnergyAware::ILPEnergyAware(const SystemModel &m, const config_t &c, Schedule *s, bool o) : BaseILPEnergyAware(m, c, s, o) {}

void ILPEnergyAware::gen() {
    // must be non-blocking
    SeqTauMax = (uint64_t)tg.global_deadline();
    if(SeqTauMax == 0 || (uint64_t)tg.sequential_length() < SeqTauMax)
        SeqTauMax = (uint64_t)tg.sequential_length();
    
    uint64_t lastiter = 0;
    for(SchedElt *t : sched->schedjobs()) {
        if(t->min_rt_period() > lastiter)
            lastiter = t->min_rt_period();
    }
    SeqTauMax += lastiter;

    add_header_comments();
    objectiv_func();
    linkObj();
    unicity();
    detect_samecore();
    decide_task_ordering_percore();
    single_phase();
    wcet();
    wcec();
    conflicts();
    causality();
    
    multiversion_mapping();
    multiversion_selection();
    
    add_bounds(SeqTauMax);
}



void ILPEnergyAware::multiversion_mapping() {
    auto st = sched->schedjobs();

    //the mapping of a task is equal to the mapping of one version
    for(ComputeUnit *p : tg.processors()) {
        for(SchedJob *t : st) {
            map<ILPVariable *, int256_t> values;

            for(Version *v : t->task()->versions()) {
                values[&_problem.var("q_" + *v + "_" + *p).var] = 1;
            }

            _problem.add_constraint(_problem.var("p_" + *t + "_" + *p) - ILPExpressionSum(values) == 0);
        }
    }
    
    //ensure that the unicity of mapping of the chosen version
    for(SchedJob *t : st) {
        for(Version *v : t->task()->versions()) {
            map<ILPVariable *, int256_t> values;

            for(ComputeUnit *p : tg.processors()) {
                values[&_problem.var("q_" + *v + "_" + *p).var] = 1;
                _problem.var("q_" + *v + "_" + *p).var.binary();
            }

            _problem.add_constraint(_problem.var("vc_" + *t + "_" + *v) - ILPExpressionSum(values) == 0);
            _problem.var("vc_" + *t + "_" + *v).var.binary();
        }
    }
    
    //ensure that versions will be placed on cores for which they are designed
    vector<ComputeUnit*> cores = tg.processors();
    sort(cores.begin(), cores.end());
    for(Task *t : tg.tasks_it()) {
        for(Version *v : t->versions()) {
            vector<ComputeUnit*> forced(v->force_mapping_proc().begin(),  v->force_mapping_proc().end());
            sort(forced.begin(), forced.end());
            vector<ComputeUnit*> inter(cores.size());
            vector<ComputeUnit*>::iterator it_inter = set_difference(cores.begin(), cores.end(), forced.begin(), forced.end(), inter.begin());
            inter.resize(distance(inter.begin(), it_inter));
            for(ComputeUnit *p : inter) {
                _problem.add_constraint(_problem.var("q_" + *v + "_" + *p) == 0, "Forbid" + *v + *p);
            }
        }
    }
}


void ILPEnergyAware::decide_task_ordering_percore() {
    // precedence same core
    auto st = sched->schedjobs();
    for (auto i = st.begin(), e = st.end(); i != e; ++i) {
        for (auto j = std::next(i); j != e; ++j) {
            // m_i_j = am_i_j + am_j_i
            _problem.add_constraint(
                _problem.var("m_" + **i + "_" + **j) - _problem.var("am_" + **i + "_" + **j) - _problem.var("am_" + **j + "_" + **i) == 0,
                "PR_" + **i + "_" + **j
            );
        }
    }
}

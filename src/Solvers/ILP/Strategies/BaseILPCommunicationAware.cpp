/*!
 * \file ILPFormGen.cpp
 * \copyright GNU Public License v3.
 * \date 2017
 * \author Benjamin Rouxel <benjamin.rouxel@uva.nl>, University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "BaseILPCommunicationAware.hpp"

using namespace std;

void BaseILPCommunicationAware::objectiv_func() {
    ILPExpressionVar objvar = _problem.var(optimal ? "makespan" : "unused");

    _problem.set_objective(objvar.var, ILPProblem::Objective::MINIMIZE);
}

void BaseILPCommunicationAware::linkObj() {
    for (SchedElt *i : sched->schedjobs()) {
        _problem.add_constraint(
            _problem.var("end_" + *i) - _problem.var("makespan") <= 0,
            "OC_" + *i
        );
    }
}

void BaseILPCommunicationAware::unicity() {
    // Unicity
    for (SchedElt* i : sched->schedjobs()) {
        std::map<ILPVariable *, int256_t> values;

        for (ComputeUnit* j : tg.processors()) {
            values[&_problem.var("p_" + *i + "_" + *j).var] = 1;
            _problem.var("p_" + *i + "_" + *j).var.binary();
        }

        _problem.add_constraint(ILPExpressionSum(values) == 1, "Uni_" + *i);
    }
}

void BaseILPCommunicationAware::detect_samecore() {
    // same core
    auto st = sched->schedjobs();
    for (auto it = st.begin(), et=st.end() ; it != et ; ++it) {
        for (auto jt = std::next(it) ; jt != et ; ++jt) {
            SchedJob *i = *it, *j = *jt;

            std::map<ILPVariable *, int256_t> values;

            for (ComputeUnit *k : tg.processors()) {
                std::string lbl_tmp_m = "tmp_m_" + *i + "_" + *j + "_" + *k;
                values[&_problem.var(lbl_tmp_m).var] = 1;
                _problem.var(lbl_tmp_m).var.binary();

                _problem.add_constraint(
                    _problem.var("p_" + *i + "_" + *k) + _problem.var("p_" + *j + "_" + *k) - _problem.var(lbl_tmp_m) <= 1,
                    "OSC_1_" + *i + "_" + *j + "_" + *k
                );

                _problem.add_constraint(
                    _problem.var("p_" + *i + "_" + *k) - _problem.var(lbl_tmp_m) >= 0,
                    "OSC_2_" + *i + "_" + *j + "_" + *k
                );

                _problem.add_constraint(
                    _problem.var("p_" + *j + "_" + *k) - _problem.var(lbl_tmp_m) >= 0,
                    "OSC_3_" + *i + "_" + *j + "_" + *k
                );
            }

            _problem.add_constraint(
                ILPExpressionSum(values) - _problem.var("m_" + *i + "_" + *j) == 0,
                "OSC_4_" + *i + "_" + *j
            );

            _problem.add_constraint(
                _problem.var("m_" + *i + "_" + *j) - _problem.var("m_" + *j + "_" + *i) == 0,
                "OSC_5_" + *i + "_" + *j
            );

            _problem.var("m_" + *i + "_" + *j).var.binary();
            _problem.var("m_" + *j + "_" + *i).var.binary();
        }
    }
}

void BaseILPCommunicationAware::decide_task_order() {
    // precedence
    auto st = sched->schedjobs();
    for (auto i = st.begin(), e = st.end(); i != e; ++i) {
        for (auto j = std::next(i); j != e; ++j) {
            _problem.add_constraint(
                _problem.var("a_" + **i + "_" + **j) + _problem.var("a_" + **j + "_" + **i) == 1,
                "PR_" + **i + "_" + **j
            );

            _problem.var("a_" + **i + "_" + **j).var.binary();
            _problem.var("a_" + **j + "_" + **i).var.binary();
        }
    }
}

void BaseILPCommunicationAware::decide_communications_order() {
    auto st = sched->schedpackets();
    for (auto i = st.begin(), e = st.end(); i != e; ++i) {
        for (auto j = std::next(i); j != e; ++j) {
            
            std::string di = (*i)->dirlbl();
            std::string dj = (*j)->dirlbl();
            _problem.add_constraint(
                _problem.var("a_"+di+dj+"_" + **i + "_" + **j) + _problem.var("a_"+dj+di+"_" + **j + "_" + **i) == 1,
                "COrder_"+di+dj+"_" + **i + "_" + **j
            );

            _problem.var("a_"+di+dj+"_" + **i + "_" + **j).var.binary();
            _problem.var("a_"+dj+di+"_" + **j + "_" + **i).var.binary();
        }
    }
}

void BaseILPCommunicationAware::decide_task_ordering_percore() {
    // precedence same core
    for (SchedElt *i : sched->schedjobs()) {
        for (SchedElt *j : sched->schedjobs()) {
            if (i == j) continue;

            _problem.add_constraint(
                _problem.var("a_" + *i + "_" + *j) + _problem.var("m_" + *i + "_" + *j) - _problem.var("am_" + *i + "_" + *j) <= 1,
                "PROSM_1_" + *i + "_" + *j
            );

            _problem.add_constraint(
                _problem.var("a_" + *i + "_" + *j) - _problem.var("am_" + *i + "_" + *j) >= 0,
                "PROSM_2_" + *i + "_" + *j
            );

            _problem.add_constraint(
                _problem.var("m_" + *i + "_" + *j) - _problem.var("am_" + *i + "_" + *j) >= 0,
                "PROSM_3_" + *i + "_" + *j
            );

            _problem.var("am_" + *i + "_" + *j).var.binary();
            
        }
    }
}
void BaseILPCommunicationAware::decide_communications_order_percore() {
    for (SchedPacket *i : sched->schedpackets()) {
        for (SchedPacket *j : sched->schedpackets()) {
            if (i == j) continue;
            std::string di = i->dirlbl();
            std::string dj = j->dirlbl();
            
            _problem.add_constraint(
                _problem.var("a_"+di+dj+"_" + *i + "_" + *j) + _problem.var("m_" + *(i->schedtask()) + "_" + *(j->schedtask())) - _problem.var("am_"+di+dj+"_" + *i + "_" + *j) <= 1,
                "CPROSM_13_" + *i + "_" + *j
            );
            _problem.add_constraint(
                _problem.var("a_"+di+dj+"_" + *i + "_" + *j) - _problem.var("am_"+di+dj+"_" + *i + "_" + *j) >= 0,
                "CPROSM_14_" + *i + "_" + *j
            );
            _problem.add_constraint(
                _problem.var("m_" + *(i->schedtask()) + "_" + *(j->schedtask())) - _problem.var("am_"+di+dj+"_" + *i + "_" + *j) >= 0,
                "CPROSM_15_" + *i + "_" + *j
            );

            _problem.var("am_"+di+dj+"_" + *i + "_" + *j).var.binary();
        }
    }
}

void BaseILPCommunicationAware::conflicts() {
    if(conf.runtime.rtt == runtime_task_e::SINGLEPHASE) {
        for(SchedJob *i : sched->schedjobs()) {
            for(SchedJob *j : sched->schedjobs()) {
                if(i == j) continue;
                // rho_i + WCETi <= rho_j + M (1 - amij) : no overlapping of exec phases on the same core
                _problem.add_constraint(
                    _problem.var("rho_" + *i) + _problem.var("wcet_" + *i) - _problem.var("rho_" + *j) + SeqTauMax * _problem.var("am_" + *i + "_" + *j) <= SeqTauMax,
                    "Conf1_" + *i + "_" + *j
                );
            }
        }
    }
    else {
        if(conf.archi.interconnect.behavior == "blocking") {
            for (SchedJob *i : sched->schedjobs()) {
                for (SchedJob *j : sched->schedjobs()) {
                    if(i == j) continue;
                    
                    // rho_r_i + delay_r_i + WCETi + delay_w_i <= rho_r_j + M (1 - amij) : no overlapping of tasks on the same core
                    _problem.add_constraint(
                        _problem.var("end_" + *i) - _problem.var("rho_r_" + *j) + SeqTauMax * _problem.var("am_" + *i + "_" + *j) <= SeqTauMax,
                        "Conf_" + *i + "_" + *j
                    );
                }
            }
        }
        else if(conf.archi.interconnect.behavior == "non-blocking") {
            for (SchedElt *i : sched->schedjobs()) {
                for (SchedElt *j : sched->schedjobs()) {
                    if(i == j) continue;
                    // rho_i + WCETi <= rho_j + M (1 - amij) : no overlapping of exec phases on the same core
                    _problem.add_constraint(
                        _problem.var("rho_" + *i) + _problem.var("wcet_" + *i) - _problem.var("rho_" + *j) + SeqTauMax * _problem.var("am_" + *i + "_" + *j) <= SeqTauMax,
                        "Conf1_" + *i + "_" + *j
                    );
                }
            }
        }
    }
}

void BaseILPCommunicationAware::conflicts_communications() {
    if(conf.runtime.rtt == runtime_task_e::SINGLEPHASE) return; // no communication
    if(conf.archi.interconnect.behavior == "blocking") return; // conflicts are managed at the wider range configts()
    
    for (SchedPacket *i : sched->schedpackets()) {
        for (SchedPacket *j : sched->schedpackets()) {
            if (i == j) continue;
            std::string di = i->dirlbl();
            std::string dj = j->dirlbl();
            // rho^r_i + delay^r_i <= rho^r_j + M (1 - am_rr_ij) : no overlapping of read phase on the same core
            _problem.add_constraint(
                _problem.var("rho_"+di+"_" + *i) + _problem.var("delay_"+di+"_" + *i) - _problem.var("rho_"+dj+"_" + *j) + SeqTauMax * _problem.var("am_"+di+dj+"_" + *i + "_" + *j) <= SeqTauMax,
                "ConfCom_" + *i + "_" + *j
            );
        }
    }
}

void BaseILPCommunicationAware::_3_phases() {
    ILPConstraint::Type cons_type = (conf.archi.interconnect.behavior == "blocking") 
                                        ? ILPConstraint::Type::EQ 
                                        : ILPConstraint::Type::GEQ;
    
    for (SchedJob *t : sched->schedjobs()) {
        vector<SchedPacket*> pr = t->packet_read();
        vector<SchedPacket*> pw = t->packet_write();

        if(pr.size() > 0) {
            size_t i = 1;
            for( ; i < pr.size() ; ++i) {
                _problem.add_constraint(
                    ILPConstraint(_problem.var("rho_r_"+*pr[i-1]) + _problem.var("delay_r_"+*pr[i-1]) - _problem.var("rho_r_"+*pr[i]), cons_type, 0)
                );
                _problem.add_constraint(_problem.var("a_rr_" + *pr[i-1] + "_" + *pr[i]) == 1);
            }
            _problem.add_constraint(
                 ILPConstraint(_problem.var("rho_" + *t) - _problem.var("rho_r_" + *pr[i-1]) - _problem.var("delay_r_" + *pr[i-1]), cons_type, 0, "Pr_" + *t)
            );
            _problem.add_constraint(
                _problem.var("rho_r_"+*pr[0]) - _problem.var("rho_r_"+*t) == 0
            );
        }
        else {
            _problem.add_constraint(
                _problem.var("rho_r_"+*t) - _problem.var("rho_" + *t) == 0
            );
        }

        if(pw.size() > 0) {
            _problem.add_constraint(
                ILPConstraint(_problem.var("rho_w_" + *pw[0]) - _problem.var("rho_" + *t) - _problem.var("wcet_" + *t), cons_type, 0, "Ps_" + *t)
            );

            size_t i=1;
            for( ; i < pw.size()-1 ; ++i) {
                _problem.add_constraint(
                    ILPConstraint(_problem.var("rho_w_"+*pw[i-1]) + _problem.var("delay_w_"+*pw[i-1]) - _problem.var("rho_w_"+*pw[i]), cons_type, 0)
                );
                _problem.add_constraint(_problem.var("a_ww_" + *pw[i-1] + "_" + *pw[i]) == 1);
            }

            _problem.add_constraint(
                ILPConstraint(_problem.var("end_" + *t) - _problem.var("rho_w_" + *pw[i-1]) - _problem.var("delay_w_" + *pw[i-1]), cons_type, 0, "Pe_" + *t)
            );
        }
        else {
            _problem.add_constraint(
                ILPConstraint(_problem.var("end_" + *t) - _problem.var("rho_" + *t) - _problem.var("wcet_" + *t), cons_type, 0, "Ps_" + *t)
            );
        }
        
        for(SchedPacket *r : pr) {
            for(SchedPacket *w : pw) {
                _problem.add_constraint(_problem.var("a_rw_" + *r + "_" + *w) == 1);
            }
        }
    }
}

void BaseILPCommunicationAware::single_phase() {
    for(SchedElt *t : sched->schedjobs()) {
        _problem.add_constraint(
            _problem.var("rho_" + *t) + _problem.var("wcet_" + *t) - _problem.var("end_" + *t) == 0,
            "EndComp_" + *t
        );
        _problem.add_constraint(
            _problem.var("rho_r_"+*t) - _problem.var("rho_" + *t) == 0
        );
    }
}

void BaseILPCommunicationAware::wcet() {
    for(SchedElt *t : sched->schedjobs()) {
        _problem.add_constraint(
            _problem.var("wcet_" + *t) == t->wct(),
            "Wcet_" + *t
        );
    }
}

void BaseILPCommunicationAware::causality() {
    if(conf.runtime.rtt == runtime_task_e::SINGLEPHASE) {
        for(SchedElt *t : sched->schedjobs()) {
            for(SchedElt *el : t->previous()) {
                // \rho_j + WCET_j <= \rho_i
                _problem.add_constraint(
                    _problem.var("rho_" + *t) - _problem.var("rho_" + *el) - _problem.var("wcet_" + *el) >= 0,
                    "Cau_1_" + *t + "_" + *el
                );
                _problem.add_constraint(
                    _problem.var("a_" + *el + "_" + *t) == 1,
                    "Cau_2_" + *el + "_" + *t
                );
            }
        }
    }
    else {
        if(conf.archi.interconnect.behavior == "blocking") {
            for(SchedJob *t : sched->schedjobs()) {
                for(SchedPacket *pac : t->packet_list()) {
                    if(pac == nullptr || pac->dirlbl() != "r") continue;
                    for(SchedElt *prev : pac->previous()) {
                        if(prev->schedtask() == t) continue; //chain of packets in burst mode
                        
                        _problem.add_constraint(
                            _problem.var("rho_r_" + *pac) - _problem.var("end_" + *prev->schedtask()) >= 0,
                            "Cau_1_" + *t + "_" + *prev->schedtask()
                        );
                        _problem.add_constraint(
                            _problem.var("a_" + *prev->schedtask() + "_" + *t) == 1,
                            "Cau_2_" + *prev->schedtask() + "_" + *t
                        );
                    }
                }
            }
        }
        else {
            for(SchedJob *t : sched->schedjobs()) {
                for(SchedPacket *pac : t->packet_list()) {
                    if(pac == nullptr || pac->dirlbl() != "r") continue;
                    for(SchedElt *prev : pac->previous()) {
                        if(prev->schedtask() == t) continue; //chain of packets in burst mode
                        
                        // \rho_j + WCET_j <= \rho_i
                        _problem.add_constraint(
                            _problem.var("rho_" + *t) - _problem.var("rho_" + *prev->schedtask()) - _problem.var("wcet_" + *prev->schedtask()) >= 0,
                            "Cau_1_" + *t + "_" + *prev->schedtask()
                        );
                        _problem.add_constraint(
                            _problem.var("a_" + *prev->schedtask() + "_" + *t) == 1,
                            "Cau_2_" + *prev->schedtask() + "_" + *t
                        );
                    }
                }
            }
        }
    }
}

void BaseILPCommunicationAware::causality_communications() {
    for(SchedPacket *p : sched->schedpackets()) {
        for(SchedElt *prev : p->previous()) {
            if(!Utils::isa<SchedPacket*>(prev)) continue;
            
            // \rho_j + WCET_j <= \rho_i
            string di = p->dirlbl();
            string dj = ((SchedPacket*)prev)->dirlbl();
            _problem.add_constraint(
                _problem.var("rho_"+dj+"_" +*prev) + _problem.var("delay_"+dj+"_"+*prev) - _problem.var("rho_"+di+"_"+p) <= 0, 
            "Cau_3_" + *prev + "_" + *p);

            // end_j <= \rho^r_i + M m_ij
            //os << "Cau_7_" << prev << "_" << t << ": end_" << prev << " - rho_r_" << t << " - " << SeqTauMax << " m_" << t << "_" << prev << " <= 0" << endl;
        }
    }
}

void BaseILPCommunicationAware::add_bounds(uint64_t deadline, const std::string & bounds) {
    add_bounds(deadline);
    throw Todo("Stephen forgot to handle bounds");
}
void BaseILPCommunicationAware::add_bounds(uint64_t deadline) {
    _problem.add_bounds(_problem.var("makespan").var, 0, deadline);
//    for(SchedElt *t : sched->schedjobs()) {
//        _problem.add_bounds(_problem.var("rho_" + *t).var, 0, deadline);
//        _problem.add_bounds(_problem.var("end_" + *t).var, 0, deadline);
//    }
}

void BaseILPCommunicationAware::add_communication_bounds(uint64_t deadline, const std::string &bounds) {
    add_communication_bounds(deadline);
}
void BaseILPCommunicationAware::add_communication_bounds(uint64_t deadline) {
    for(SchedJob *t : sched->schedjobs()) {
        for(SchedPacket *p : t->packet_list()) {
            if(p == nullptr) continue;
            _problem.add_bounds(_problem.var("rho_"+p->dirlbl()+"_" + *p).var, 0, deadline);
        }
    }
}

void BaseILPCommunicationAware::force_mapping() {
/*    for(Task *t : sched->schedtasks()) {
        if(!t->force_mapping_proc.empty()) {
            Utils::WARN("ILPFormGen::force_mapping -- Work in progress");
            os << "ForceMap_" << t << " : p_" << t << "_" << t->force_mapping_proc << " = 1" << endl;

            for(Task *t2 : sched->schedtasks()) {
                if(t == t2 || t2->force_mapping_proc.empty()) continue;
                os << "ForceMap_m_" << t << "_" << t2 << " : m_" << t << "_" << t2 << " = " << ((t->force_mapping_proc == t2->force_mapping_proc)) << endl;
        }
    }
    }
    os << endl;
 */
}

void BaseILPCommunicationAware::multiperiod() {
    for(SchedElt *t : sched->schedjobs()) {
        _problem.add_constraint(_problem.var("rho_" + *t) >= t->min_rt_period(), "MultiT_" + *t);
        if(t->min_rt_deadline() > 0)
            _problem.add_constraint(_problem.var("end_" + *t) <= t->min_rt_deadline(), "MultiD_" + *t);
    }

    if(!allow_migration) {
        std::map<const Task*, std::vector<SchedElt*>> corres;
        for(SchedElt *st : sched->schedjobs())
            corres[st->schedtask()->task()].push_back(st);

        for(std::pair<const Task*, std::vector<SchedElt*>> el : corres) {
            for(std::vector<SchedElt*>::iterator i = el.second.begin(), e = el.second.end(); i != e; ++i) {
                for (std::vector<SchedElt*>::iterator j = i + 1; j != e; ++j) {
                    _problem.add_constraint(_problem.var("m_" + **i + "_" + **j) == 1);
                }
            }
        }
    }
}

void BaseILPCommunicationAware::map_spm_region_to_task() {
    auto st = sched->schedjobs();
    auto sp = sched->schedpackets();
    auto elts = sched->elements();
    vector<string> memory_regions;
    size_t cnt = 0;
    for(size_t i=0 , e=tg.tasks().size() ; i < e ; ++i) {
        // one for each phase
        memory_regions.push_back("MR"+to_string(cnt++));
        memory_regions.push_back("MR"+to_string(cnt++));
        memory_regions.push_back("MR"+to_string(cnt++));
    }
    for(SchedCore *c : sched->schedcores()) {
        for(string r : memory_regions)
            c->add_spmregion(r, 0);
    }
    
    // Unicity, a task is mapped on only one region
    for(SchedPacket *t : sp) {
        std::map<ILPVariable *, int256_t> values;

        for(string r : memory_regions) {
            values[&_problem.var("spmp_"+t->dirlbl()+"_"+r+"_"+*t).var] = 1;
            _problem.var("spmp_"+t->dirlbl()+"_"+r+"_"+*t).var.binary();
        }

        _problem.add_constraint(ILPExpressionSum(values) == 1, "RSPM_UP_"+*t);
    }
    
    // Determine the size of the region that is the biggest amount of stored data in it
    cnt = 0;
    for(ComputeUnit *c : tg.processors()) {
        for(string r : memory_regions) {
            for(SchedEltPtr &t : sched->elements()) {
                // srspm_{p,r}  >= data ( spmp_{r,t} /\ p_{t,p})
                string tmpvar = "tmp"+to_string(cnt)+"_"+*c+"_"+r+"_"+*t;
                string spmpvar = "spmp_"+t.get()->dirlbl()+"_"+r+"_"+*t;
                string pvar = "p_"+*t->schedtask()+"_"+*c;
                _problem.add_constraint(_problem.var(tmpvar) - _problem.var(spmpvar) <= 0);
                _problem.add_constraint(_problem.var(tmpvar) - _problem.var(pvar) <= 0);
                _problem.add_constraint(_problem.var(tmpvar) - _problem.var(spmpvar) - _problem.var(pvar) >= -1);
                _problem.var(tmpvar).var.binary();
                _problem.add_constraint(_problem.var("srspm_"+*c+"_"+r) - t.get()->mem_data() * _problem.var(tmpvar) >= 0, 
                    "RSPM_S_R_"+*c+"_"+r+"_"+*t.get()); 
                cnt++;
            }
        }
    }

    // The total size of regions must not exceed the size of the spm
    for(ComputeUnit *c : tg.processors()) {
        std::map<ILPVariable *, int256_t> values;
        for(string r : memory_regions) {
            values[&_problem.var("srspm_"+*c+"_"+r).var] = 1;
        }
        _problem.add_constraint(ILPExpressionSum(values) <= c->spm_size, "RSPM_TS_"+*c);
    }
    
    //if we don't force communication through external memory, edge where src and dst are mapped on the same core must have the same region
    if(conf.archi.interconnect.mechanism == "shared") {
        for(SchedPacket *a : sp) {
            for(SchedElt *b : a->successors()) {
                if(a->schedtask() == b->schedtask()) continue;
                if(Utils::isa<SchedJob*>(b)) continue;
                
                for(string r : memory_regions) {
                    _problem.add_constraint(
                        _problem.var("spmp_"+a->dirlbl()+"_"+r+"_"+*a) - 
                        _problem.var("spmp_"+b->dirlbl()+"_"+r+"_"+*b) - 
                        SeqTauMax * _problem.var("m_"+*a->schedtask()+"_"+*b->schedtask()) 
                        >= (-1 * SeqTauMax),
                        "RSPM_SR1_"+*a+"_"+*b+"_"+r
                    );
                    _problem.add_constraint(
                        _problem.var("spmp_"+a->dirlbl()+"_"+r+"_"+*a) - 
                        _problem.var("spmp_"+b->dirlbl()+"_"+r+"_"+*b) - 
                        SeqTauMax * _problem.var("m_"+*a->schedtask()+"_"+*b->schedtask()) 
                        <= SeqTauMax,
                        "RSPM_SR2_"+*a+"_"+*b+"_"+r
                    );
                }
            }
        }
    }
    
    // Determine si 2 phases are assigned to the same region, similar to the "m_{i,j}" for processor assignment
    for (auto it = elts.begin(), et=elts.end() ; it != et ; ++it) {
        SchedElt *i = (*it).get();
        for (auto jt = it ; jt != et ; ++jt) {
            SchedElt *j = (*jt).get();
            if(i == j) continue;
            
            string ijlbl = i->dirlbl()+j->dirlbl();
            string jilbl = j->dirlbl()+i->dirlbl();
            std::map<ILPVariable *, int256_t> values;
            for (string r : memory_regions) {
                string lbl_tmp_m = "spm"+ijlbl+"tmp_m_" + *i + "_" + *j + "_" + r;
                _problem.var(lbl_tmp_m).var.binary();
                _problem.add_constraint(
                    _problem.var("spmp_"+i->dirlbl()+"_"+r+"_"+*i) + _problem.var("spmp_"+j->dirlbl()+"_"+r+"_"+*j) - _problem.var(lbl_tmp_m) <= 1,
                    "spmOSC_1"+ijlbl+"_"+*i+"_"+*j+"_"+r
                );
                _problem.add_constraint(
                    _problem.var("spmp_"+i->dirlbl()+"_"+r+"_"+*i) - _problem.var(lbl_tmp_m) >= 0,
                    "spmOSC_2"+ijlbl+"_"+*i+"_"+*j+"_"+r
                );
                _problem.add_constraint(
                    _problem.var("spmp_"+j->dirlbl()+"_"+r+"_"+*j) - _problem.var(lbl_tmp_m) >= 0,
                    "spmOSC_3"+ijlbl+"_"+*i+"_"+*j+"_"+r
                );
                
                values[&_problem.var(lbl_tmp_m).var] = 1;
            }
            
            string spmmijvar = "spmm_"+ijlbl+"_"+*i+"_"+*j;
            string spmmjivar = "spmm_"+jilbl+"_"+*j+"_"+*i;
            if(i->schedtask() == j->schedtask()) {
                _problem.add_constraint(ILPExpressionSum(values) - _problem.var(spmmijvar) == 0, "spmOSC_4"+ijlbl+"_"+*i+"_"+*j);
            }
            else {
                string spmmtmp = "spmmtmp2_"+ijlbl+"_"+*i+"_"+*j;
                _problem.var(spmmtmp).var.binary();
                _problem.add_constraint(ILPExpressionSum(values) - _problem.var(spmmtmp) == 0, "spmOSC_4"+ijlbl+"_"+*i+"_"+*j);
                _problem.add_constraint(_problem.var("m_"+*i+"_"+*j) + _problem.var(spmmtmp)  - _problem.var(spmmijvar) <= 1, "spmOSC_5"+ijlbl+"_"+*i+"_"+*j);
                _problem.add_constraint(_problem.var("m_"+*i+"_"+*j) - _problem.var(spmmijvar) >= 0, "spmOSC_6"+ijlbl+"_"+*i+"_"+*j);
                _problem.add_constraint(_problem.var(spmmtmp) - _problem.var(spmmijvar) >= 0, "spmOSC_7"+ijlbl+"_"+*i+"_"+*j);
            }
            
            // opti constraints
            _problem.add_constraint(_problem.var(spmmjivar) - _problem.var(spmmijvar) == 0, "spmOSC_8"+ijlbl+"_"+*i+"_"+*j);
            _problem.var(spmmijvar).var.binary();
            _problem.var(spmmjivar).var.binary();
        }
    }

    // identical above precedence but add precedency between comm and exec
    for(SchedJob *i : st) {
        for (SchedPacket *j : sp) {
            _problem.add_constraint(_problem.var("a_"+i->dirlbl()+j->dirlbl()+"_"+*i+"_"+*j) + _problem.var("a_"+j->dirlbl()+i->dirlbl()+"_"+*j+"_"+*i) == 1, "spmPR_nb5_"+*i+"_"+*j);

            _problem.var("a_"+i->dirlbl()+j->dirlbl()+"_"+*i+"_"+*j).var.binary();
            _problem.var("a_"+j->dirlbl()+i->dirlbl()+"_"+*j+"_"+*i).var.binary();
        }
    }
    // identical above causality but add causality between comm and exec
    for(SchedJob *t : st) {
        for(SchedPacket *p : t->schedtask()->packet_read()) {
            _problem.var("a_re_"+*p+"_"+*t).var.binary();
            _problem.add_constraint(_problem.var("a_re_"+*p+"_"+*t) == 1);
        }
        for(SchedPacket *p : t->schedtask()->packet_write()) {
            _problem.var("a_ew_"+*t+"_"+*p).var.binary();
            _problem.add_constraint(_problem.var("a_ew_"+*t+"_"+*p) == 1);
        }
    }
    
    // identical above precedence same core
    // Determine spmam_XY_i,j similar to am_XY_i,j
    //    spmam_XY_i,j = a_XY_i,j && spmm_XY_i,j 
    for(SchedEltPtr &ip : elts) {
        SchedElt *i = ip.get();
        for(SchedEltPtr &jp : elts) {
            SchedElt *j = jp.get();
            if(i == j) continue;
            
            string ijlbla = i->dirlbl()+j->dirlbl()+"_";
            if(Utils::isa<SchedJob*>(i) && Utils::isa<SchedJob*>(j))
                ijlbla = "";
            string ijlblspm = i->dirlbl()+j->dirlbl();
            
            _problem.add_constraint(_problem.var("a_"+ijlbla+*i+"_"+*j) + _problem.var("spmm_"+ijlblspm+"_"+*i+"_"+*j) - _problem.var("spmam_"+ijlblspm+"_"+*i+"_"+*j) <= 1);
            _problem.add_constraint(_problem.var("a_"+ijlbla+*i+"_"+*j) - _problem.var("spmam_"+ijlblspm+"_"+*i+"_"+*j) >= 0);
            _problem.add_constraint(_problem.var("spmm_"+ijlblspm+"_"+*i+"_"+*j) - _problem.var("spmam_"+ijlblspm+"_"+*i+"_"+*j) >= 0);
            _problem.var("spmam_"+ijlblspm+"_"+*i+"_"+*j).var.binary();
        }
    }
    
    for(SchedJob *a : st) {
        string stvar = "startresa_e_"+*a;
        string envar = "finresa_e_"+*a;
        
        if(!conf.archi.spm.prefetch_code && conf.archi.spm.store_code) { // code resident in mem
            _problem.add_constraint(_problem.var(stvar) == 0);
            _problem.add_constraint(_problem.var(envar) == SeqTauMax);
        }
        else if(conf.archi.spm.prefetch_code && conf.archi.spm.store_code) { // prefetch code
            _problem.add_constraint(_problem.var(stvar) - _problem.var("rho_r_"+*a) == 0);
            _problem.add_constraint(_problem.var(envar) - _problem.var("rho_"+*a) - _problem.var("wcet_"+*a) == 0);
        }
        else { // only local data
            _problem.add_constraint(_problem.var(stvar) - _problem.var("rho_"+*a) == 0);
            _problem.add_constraint(_problem.var(envar) - _problem.var("rho_"+*a) - _problem.var("wcet_"+*a) == 0);
        }
    }
    
    for(SchedPacket *a : sp) {
        string rstvar = "startresa_r_"+*a;
        string renvar = "finresa_r_"+*a;
        string wstvar = "startresa_w_"+*a;
        string wenvar = "finresa_w_"+*a;
        
        _problem.add_constraint(_problem.var(rstvar) - _problem.var("rho_r_"+*a) == 0);
        _problem.add_constraint(_problem.var(renvar) - _problem.var("rho_w_"+*a) == 0);
        _problem.add_constraint(_problem.var(wstvar) - _problem.var("rho_"+*a) == 0);
        _problem.add_constraint(_problem.var(wenvar) - _problem.var("end_"+*a) == 0);
    }
    
    //if we don't force communication through external memory, finresa of write phase is the max(startresa of successors mapped on same core)
    /*if(conf.interconnect.mechanism != "sharedonly") {
        for(Task *a : tg.tasks()) {
            // if some successors are on the same core, then the finresa is the start resa of the successor that starts the later
            for(Task::dep_t el : a->successors) {
                os << "finresa_w_" << a << " - startresa_r_" << el.first << " - " << SeqTauMax << "m_" << a << "_" << el.first << " >= -" << SeqTauMax << endl;
            }
        }
    }*/
    
    for(SchedEltPtr &i : sched->elements()) {
        for(SchedEltPtr &j : sched->elements()) {
            if(i == j) continue;
            _problem.add_constraint(
                _problem.var("finresa_"+i.get()->dirlbl()+"_"+*i) -
                _problem.var("finresa_"+j.get()->dirlbl()+"_"+*j) +
                SeqTauMax * _problem.var("spmam_"+i.get()->dirlbl()+j.get()->dirlbl()+"_"+*i+"_"+*j)
                <= SeqTauMax
            );
        }
    }
}
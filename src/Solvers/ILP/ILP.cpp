/*!
 * \file ILP.cpp
 * \copyright GNU Public License v3.
 * \date 2020 
 * \author Benjamin Rouxel <benjamin.rouxel@uva.nl>, University of Amsterdam
 * \author Stephen Nicholas Swatman <s.n.swatman@uva.nl> University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <boost/multiprecision/cpp_int.hpp>

#include "ILP.hpp"
#include "Utils/Log.hpp"

using boost::multiprecision::int256_t;
using namespace std;

ILPVariable::ILPVariable(
    std::string name,
    ILPVariable::Type type
) :
    name(name),
    type(type)
{}

void ILPVariable::binary() {
    type = Type::BINARY;
}

ILPBound::ILPBound(
    ILPVariable & subject,
    int256_t lower,
    int256_t upper
) :
    subject(subject),
    lower(lower),
    upper(upper)
{}

ILPConstraint::ILPConstraint(
    ILPExpression && expr,
    ILPConstraint::Type type,
    int256_t value,
    std::string name
) :
    name(name),
    subject(expr.clone()),
    type(type),
    value(value)
{}

void ILPConstraint::set_name(std::string new_name) {
    name = new_name;
}

ILPConstraint operator>=(ILPExpression && expr, int256_t value) {
    return ILPConstraint(std::move(expr), ILPConstraint::Type::GEQ, value);
}

ILPConstraint operator>(ILPExpression && expr, int256_t value) {
    return ILPConstraint(std::move(expr), ILPConstraint::Type::GE, value);
}


ILPConstraint operator==(ILPExpression && expr, int256_t value) {
    return ILPConstraint(std::move(expr), ILPConstraint::Type::EQ, value);
}

ILPConstraint operator<=(ILPExpression && expr, int256_t value) {
    return ILPConstraint(std::move(expr), ILPConstraint::Type::LEQ, value);
}


ILPExpressionVar::ILPExpressionVar(
    ILPVariable & var
) :
    var(var)
{}

std::unique_ptr<ILPExpression> ILPExpressionVar::clone() const {
    return std::unique_ptr<ILPExpression>(new ILPExpressionVar(*this));
}

std::map<const std::string, int256_t> ILPExpressionVar::flatten() const {
    return {{var.name, 1}};
}

ILPExpressionBinop::ILPExpressionBinop(
    Type op,
    ILPExpression && lhs,
    ILPExpression && rhs
) :
    lhs(lhs.clone()),
    rhs(rhs.clone()),
    type(op)
{}

ILPExpressionBinop::ILPExpressionBinop(const ILPExpressionBinop & o) :
    lhs(o.lhs->clone()),
    rhs(o.rhs->clone()),
    type(o.type)
{}

std::unique_ptr<ILPExpression> ILPExpressionBinop::clone() const {
    return std::unique_ptr<ILPExpression>(new ILPExpressionBinop(*this));
}

std::map<const std::string, int256_t> ILPExpressionBinop::flatten() const {
    std::map<const std::string, int256_t> lhsv = lhs->flatten();
    std::map<const std::string, int256_t> rhsv = rhs->flatten();

    for (std::pair<const std::string, int256_t> & p : rhsv) {
        int256_t modifier = p.second * (type == ILPExpressionBinop::Type::ADD ? 1 : -1);

        if (lhsv.count(p.first) == 0) {
            lhsv[p.first] = modifier;
        } else {
            lhsv[p.first] += modifier;
        }
    }

    return lhsv;
}

ILPExpressionScale::ILPExpressionScale(
    int256_t scalar,
    ILPExpression && subject
) :
    scalar(scalar),
    subject(subject.clone())
{}

ILPExpressionScale::ILPExpressionScale(const ILPExpressionScale & o) :
    scalar(o.scalar),
    subject(o.subject->clone())
{}

std::unique_ptr<ILPExpression> ILPExpressionScale::clone() const {
    return std::unique_ptr<ILPExpression>(new ILPExpressionScale(*this));
}

std::map<const std::string, int256_t> ILPExpressionScale::flatten() const {
    std::map<const std::string, int256_t> result = subject->flatten();

    for (std::pair<const std::string, int256_t> & p : result) {
        p.second *= scalar;
    }

    return result;
}

ILPExpressionBinop operator+(ILPExpression && lhs, ILPExpression && rhs) {
    return ILPExpressionBinop(ILPExpressionBinop::Type::ADD, std::move(lhs), std::move(rhs));
}

ILPExpressionBinop operator-(ILPExpression && lhs, ILPExpression && rhs) {
    return ILPExpressionBinop(ILPExpressionBinop::Type::SUB, std::move(lhs), std::move(rhs));
}

ILPExpressionSum::ILPExpressionSum(std::map<ILPVariable *, int256_t> m):
    values(m)
{}

ILPExpressionSum::ILPExpressionSum(const ILPExpressionSum & o):
    values(o.values)
{}

std::unique_ptr<ILPExpression> ILPExpressionSum::clone() const {
    return std::unique_ptr<ILPExpression>(new ILPExpressionSum(*this));
}

std::map<const std::string, int256_t> ILPExpressionSum::flatten() const {
    std::map<const std::string, int256_t> result;

    for (const std::pair<const ILPVariable *, int256_t> p : values) {
        result[p.first->name] = p.second;
    }

    return result;
}

ILPExpressionScale operator*(int256_t scalar, ILPExpression && subject) {
    return ILPExpressionScale(scalar, std::move(subject));
}

ILPExpressionVar ILPProblem::var(
    std::string name
) {
    if (variables.count(name) == 0) {
        variables.emplace(name, ILPVariable(name, ILPVariable::Type::GENERAL));
    }

    return ILPExpressionVar(variables.at(name));
}
ILPExpressionVar ILPProblem::binvar(
    std::string name
) {
    if (variables.count(name) == 0) {
        variables.emplace(name, ILPVariable(name, ILPVariable::Type::BINARY));
    }

    return ILPExpressionVar(variables.at(name));
}

void ILPProblem::set_objective(
    ILPVariable & var,
    Objective goal
) {
    objective_var = &var;
    objective_goal = goal;
}

void ILPProblem::add_constraint(
    ILPConstraint && cons,
    std::string name
) {
    if (!name.empty()) {
        cons.set_name(name);
    }

    constraints.emplace_back(std::move(cons));
}

void ILPProblem::add_bounds(
    ILPVariable & var,
    int256_t lower,
    int256_t upper
) {
    bounds.emplace_back(var, lower, upper);
}

std::pair<ILPProblem::Objective, ILPVariable *> ILPProblem::objective() const {
    return {objective_goal, objective_var};
}

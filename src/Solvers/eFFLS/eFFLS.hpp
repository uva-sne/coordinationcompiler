/*
 * Copyright (C) 2022 Lukas Miedema <l.miedema@uva.nl>
 *                    University of Amsterdam
 * Copyright (C) 2022 Rick Knegt <rick.knegt@student.uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CECILE_EFFLS_HPP
#define CECILE_EFFLS_HPP

#include <Solvers/Solver.hpp>
#include <Solvers/Criteria.hpp>

//! Convenience data structure to track the current "best"
struct eFflsPlacementCandidate {
    SchedCore *core = nullptr; // The best core
    Version *version = nullptr; // The best version
    timinginfos_t responseTime; // Release time + WCET of the SchedElt on that core (lower = better)
    timinginfos_t delay; // The delay (time after the last SchedElt on that core finishes), (lower = better)
    long scheduleIndex; // The index in that cores schedule vector
    energycons_t energy_e1 = 0; // The energy, * 10 for extra precision (divide by 10 to get the actual value), i.e. * 10^-1 mJ when base_unit = "mJ"

    float getCost(float factor);
    void computeEnergy_e1(const SystemModel *tg, timinginfos_t rolling_makespan, energycons_t ucf);
};

/**
 * Implements a restricted version of FLS with good runtime performance.
 * Algorithmic complexity of sorting (LLF) is O(n log n) (n=task count)
 * Algorithmic complexity of scheduling a single task is O(n * v) (n=task count, v=max versions for one task)[1]
 * Total complexity is O(n (log n + n*v)) = O(n^2 * v)
 *
 * [1] Can probably be optimized with a more efficient data structure for tracking "gaps" in the schedule
 *
 * - Supports heterogeneous multi-version scheduling
 * - Supports criteria-aware scheduling
 */
class EnergyFFLS : public Solver, public CriteriaAware {
private:
    //! Evaluate the placement of a core to a task. This function has no (lasting) side-effects.
    eFflsPlacementCandidate evaluatePlacement(
            std::map<SchedCore *, std::vector<SchedElt *>> *schedule, SchedCore *core,
            SchedElt *toSchedule, Version *version, timinginfos_t notBefore, timinginfos_t rolling_makespan);

    energycons_t _unit_conversion_factor; //!< 1 energy unit = 1 time unit * 1 power unit / this factor

    void solveHelper(Schedule *result, float factor, std::vector<SchedElt*> const &Qready);

public:
    explicit EnergyFFLS() : Solver() {};

    //! \copydoc CompilerPass::get_uniqid_rtti
    const std::string get_uniqid_rtti() const override { return "solver-effls"; }

    //! \copydoc CompilerPass::help
    virtual std::string help() override;

    //! \copydoc Solver::solve
    void solve(Schedule *result) override;

    /**
     * Solve with a criteria.
     * @param result
     * @param criteria
     */
    void solve(Schedule *result, OptimizationTarget::Criteria* criteria) override;
};

REGISTER_SOLVER(EnergyFFLS, "effls")

#endif //CECILE_EFFLS_HPP

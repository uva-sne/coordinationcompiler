/*
 * Copyright (C) 2022 Lukas Miedema <l.miedema@uva.nl>
 *                    University of Amsterdam
 * Copyright (C) 2022 Rick Knegt <rick.knegt@student.uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <SystemModel.hpp>
#include <Schedule.hpp>
#include <ScheduleFactory.hpp>
#include "eFFLS.hpp"
#include <Solvers/FastFLS/FLSSorting.hpp>
#include <Solvers/ForwardListScheduling/FLSFragmentedQreadyBuilder.hpp>

using E = LabeledCompilerException<C_STR("effls")>;

IMPLEMENT_HELP(EnergyFFLS,
        Energy Efficient Fast Forward List Scheduler/Solver: \n
        Optimizes for makespan, and supports heterogeneous architectures + multi-version scheduling\n
        HTMLINDENT- type [@see Solver]: type of scheduling strategy\n)

void EnergyFFLS::solve(Schedule *result) {
    if (this->_criteria == nullptr) {
        auto defaultCriteria = OptimizationTarget::Energy(this->tg->global_deadline());
        solve(result, &defaultCriteria);
    } else
        solve(result, this->_criteria.get());
}

void EnergyFFLS::solve(Schedule *result, OptimizationTarget::Criteria* criteria) {
    // Check assumptions
    for (ComputeUnit *p : this->tg->processors()) {
        if (p->target_voltage_island == nullptr)
            throw E("no voltage island referenced for processing unit " + p->id);
    }
    this->_unit_conversion_factor = Utils::get_energy_units_per_power_time_unit(tg->time_unit(), tg->power_unit(), tg->energy_unit());

    for (Task *task : this->tg->tasks_it()) {
        for (Connector *inport : task->inputs())
            if (inport->tokens() != 1 && inport->type() == Connector::REGULAR)
                throw E("cannot schedule SDF graph directly");
        for (Connector *outport : task->outputs())
            if (outport->tokens() != 1 && outport->type() == Connector::REGULAR)
                throw E("cannot schedule SDF graph directly");
    }

    // Initialize
    if (result->status() == ScheduleProperties::SCHED_COMPLETE)
        result->clear();

    for (ComputeUnit *p: this->tg->processors())
        ScheduleFactory::add_core(result, p);

    ScheduleFactory::build_elements(result);
    std::vector<SchedElt*> elements = result->elements_unowned();

    // Sort the elements
    std::vector<SchedElt*> Qready = FLSSorting::sort_llf(std::move(elements));
    assert(Qready.size() == result->elements_lut().size());


    float factor = 0.5;
    float delta_factor = 0.25;
    float best_factor = 0.0;

    if (!tg->has_global_deadline()) {
        Utils::WARN("Application has no global deadline, scheduling with maximum energy-efficiency");
        solveHelper(result, 1.0, Qready);
        return;
    }

    while (delta_factor > 0.000001) {
        Utils::DEBUG("\t---- factor: " + std::to_string(factor));
        solveHelper(result, factor, Qready);

        assert (result->status() == ScheduleProperties::SCHED_COMPLETE);
        if (criteria->is_acceptable(result))
            best_factor = factor;

        if (criteria->can_use_less_energy(result)) {
            factor += delta_factor;
        } else {
            factor -= delta_factor;
        }
        delta_factor /= 2;
    }

    if (best_factor != factor)
        solveHelper(result, best_factor, Qready);
}

void EnergyFFLS::solveHelper(Schedule *result, float factor, std::vector<SchedElt*> const &Qready) {
    // Reset all timing assignment as this call may be made many times
    result->clear_assigned();

    timinginfos_t rolling_makespan = 0; // do not recompute makespan at every step

    // Start mapping
    /* This version of FLS works as follows
     * - The list of unscheduled elements is always scheduled as they appear, elements are never skipped and processed later
     * - Elements may be delayed (pushed forward in time) to ensure their predecessors have finished
     * - Already scheduled elements are never moved (greedy)
     * - The best core is selected as follows
     *  1. Find the cores with the earliest release time for the task
     *  2. From those cores, find the cores with the smallest amount of delay between the return of the last task and the release time
     *  3. Finally, if there are still multiple cores, prefer cores earlier in the list
     */
    std::map<SchedCore *, std::vector<SchedElt *>> schedule; // each vector is kept sorted by RT
    for (SchedElt* toSchedule : Qready) {

        eFflsPlacementCandidate best = {
                .core = nullptr,
                .version = nullptr,
                .responseTime = std::numeric_limits<timinginfos_t>::max(),
                .delay = 0,
                .scheduleIndex = 0,
                .energy_e1 = 0
        };

        timinginfos_t notBefore = 0;
        for (SchedElt* predecessor : toSchedule->previous()) {
            notBefore = std::max(predecessor->rt() + predecessor->wct(), notBefore);
        }

        std::vector<Version *> versions = toSchedule->task()->versions(); // this returns a _copy_ of all versions -_-

        for (SchedCore* core : result->schedcores()) {
            for (Version *version : versions) {
                if (version->canRunOn(core->core()) && !version->gpu_required()) {
                    eFflsPlacementCandidate candidate = evaluatePlacement(&schedule, core, toSchedule,
                                                                          version, notBefore, rolling_makespan);
                    candidate.computeEnergy_e1(tg, rolling_makespan, this->_unit_conversion_factor);

                    if (!best.version ||candidate.getCost(factor) < best.getCost(factor))
                        best = candidate;
                }
            }
        }

        // Best core has been found -> now do actual scheduling
        toSchedule->rt(best.responseTime - best.version->C());
        toSchedule->wct(best.version->C());
        toSchedule->wce(best.version->C_E());
        toSchedule->schedtask()->selected_version(best.version->id());
        toSchedule->schedtask()->frequency(best.version->frequency());

        result->map_core(toSchedule, best.core);
        result->schedule(toSchedule);

        // Update scheduling data structures
        rolling_makespan = std::max(rolling_makespan, best.responseTime);
        schedule[best.core].insert(schedule[best.core].begin() + best.scheduleIndex, toSchedule);
    }

    // This scheduler will never build a partial schedule
    // Even if makespan > deadline, the "COMPLETE" status is expected, otherwise e.g. the SVGViewer will not work
    result->status(ScheduleProperties::SCHED_COMPLETE);
    result->makespan(rolling_makespan);

    result->compute_energy();
    Utils::DEBUG("\t====> Energy consumption = " + to_string(result->energy()) + tg->energy_unit());
    Utils::DEBUG("\t====> Makespan = " + to_string(result->makespan()) + tg->time_unit());

}

eFflsPlacementCandidate EnergyFFLS::evaluatePlacement(
        std::map<SchedCore *, std::vector<SchedElt *>> *schedule,
        SchedCore *core, SchedElt *toSchedule, Version *version,
        timinginfos_t notBefore, timinginfos_t rolling_makespan) {

    // Find the first element that finishes after notBefore (RT+WCET >= notBefore)
    std::vector<SchedElt *>& coreSchedule = schedule->operator[](core);
    long taskAfter; // may be == coreSchedule.size() -> there is no such task
    for (taskAfter = 0; taskAfter < (long) coreSchedule.size(); taskAfter++) {
        if (coreSchedule[taskAfter]->rt() + coreSchedule[taskAfter]->wct() >= notBefore)
            break;
    }

    // Try to find a gap
    for (; taskAfter < (long) coreSchedule.size(); taskAfter++) {
        // We know that this task (ta) finishes _after_ notBefore (nb)
        // 1. As such, the situation may be this (- = idle)
        // ---- [nb] --------------- [ ta.start .... ta.end ]
        //        ^ this is our gap ^
        // 2. Or, alternatively, a task could be in that gap
        //
        // [ta-1.start ..... [nb] .. ta-1.end] ------------ [ ta.start ... ta.end ]
        //                                   ^  our gap     ^
        // 3. Final case, ta.start itself is _before_ notBefore
        //
        // [ta.start .... [nb] ....... ta.end ]
        //       ~ there is no gap here ~

        // As such, compute the start of the gap by taking the max, dealing with case 1 and 2
        timinginfos_t previousEnd = taskAfter == 0 ? 0 : (coreSchedule[taskAfter-1]->rt() + coreSchedule[taskAfter-1]->wct());
        timinginfos_t gapStart = std::max(notBefore, previousEnd);
        timinginfos_t gapEnd = coreSchedule[taskAfter]->rt();
        if (gapEnd <= gapStart) {
            // There is no gap (case 3)
            continue;
        }
        timinginfos_t gapSize = gapEnd - gapStart;
        if (gapSize < version->C()) {
            // Doesn't fit
            continue;
        }
        // Got it
        timinginfos_t delay = previousEnd - gapStart;
        return {
            .core = core,
            .version = version,
            .responseTime = gapStart + version->C(),
            .delay = delay,
            .scheduleIndex = taskAfter,
            .energy_e1 = 0

        };
    }

    // No gap found -> place element at the open end of the schedule
    timinginfos_t rt;
    timinginfos_t delay;
    if (taskAfter == 0) { // no tasks in the schedule for this core
        rt = notBefore;
        delay = notBefore;
    } else {
        SchedElt* lastTask = coreSchedule[taskAfter - 1];
        rt = std::max(notBefore, (lastTask->rt() + lastTask->wct()));
        delay = (lastTask->rt() + lastTask->wct()) - notBefore;
    }

    return {
        .core = core,
        .version = version,
        .responseTime = rt + version->C(),
        .delay = delay,
        .scheduleIndex = taskAfter,
        .energy_e1 = 0
    };
}

float eFflsPlacementCandidate::getCost(float factor) {
    return energy_e1 * factor + responseTime * (1 - factor);
}

/*
 * ! Doesn't have support for voltage islands
 */
void eFflsPlacementCandidate::computeEnergy_e1(const SystemModel *tg, timinginfos_t rolling_makespan, energycons_t ucf) {
    energy_e1 = 0;

    /* Add energy of the task itself. */
    energy_e1 += version->C_E() * 10; // * 10 because we want "e1"

    /* Add base watt energy if there  */
    if (responseTime > rolling_makespan)
        energy_e1 += (responseTime - rolling_makespan) * tg->base_watt() * 10 / ucf; // time in ds * power in mW makes makes 10^-2 J, but that is what we want

    /* Find the power of the given frequency of the specific version.
     * Afterwards add the energy usage of the frequency of the given version
     * (Power * release time, * 10 as we want _e1). */
    for (const auto &freq_watt: core->core()->target_voltage_island->frequency_watt){
        if(freq_watt.first == version->frequency()){
            energy_e1 += version->C() * freq_watt.second * 10 / ucf;
            break;
        }
    }
}

bool OptimizationTarget::Energy::can_use_less_energy(Schedule *result) const {
    return result->makespan() <= _global_deadline;
}

bool OptimizationTarget::Energy::is_acceptable(Schedule *result) const {
    return result->makespan() <= _global_deadline; // same thing, when energy can be decreased we have an otherwise good schedule
}

bool OptimizationTarget::Deadline::can_use_less_energy(Schedule *result) const {
    if (result->makespan() > _global_deadline)
        return false; // speed up to meet deadline, more important than the budget
    if (result->energy() > _budget)
        return true; // must stay in the budget
    return false; // budget not spent yet
}

bool OptimizationTarget::Deadline::is_acceptable(Schedule *result) const {
    return result->makespan() <= _global_deadline && result->energy() <= _budget; // energy budget must also be met
}

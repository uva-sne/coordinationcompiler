/*!
 * \file Solver.hpp
 * \copyright GNU Public License v3.
 * \date 2017
 * \author Benjamin Rouxel <benjamin.rouxel@inria.fr> Irisa/INRIA
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SOLVER_H
#define SOLVER_H

#include <string>
#include <map>
#include <chrono>

#include "config.hpp"
#include "registry.hpp"
#include "Schedule.hpp"
#include "CompilerPass.hpp"
#include "Utils/Log.hpp"
#include "Utils.hpp"

/*!
 * \brief Base class for solvers
 * 
 * Solve a scheduling problem either by building the schedule for off-line 
 * schedulers, or performing a schedulability analysis.
 */
class
Solver : public CompilerPass {
protected:
    //! IR of the current application
    const SystemModel *tg;
    //! the global configuration
    const config_t *conf;
    //TODO: Make a timeout check in solver class so that it is the same across all solvers
    //! Start time for solve time computation
    std::chrono::high_resolution_clock::time_point start_time;
    //! Should the problem be solved
    bool _solve = true;
    //! Timeout of the scheduler in seconds
    timinginfos_t _timeout = 0;
    //! Type of the scheduler, either the ILP form generator, or the specific heuristic
    std::string _type;
    //! Does the scheduler should allow migration
    bool allow_migration = false;
    //! The optimisation target
    SolverProperties::SolverTargets _target;

    /**
     * \brief Schedule the application
     * @param result contains the resulting schedule
     */
    virtual void solve(Schedule *result) = 0;
    /**
     * Perform a pre-solve step
     * @param sched
     */
    virtual void presolve(Schedule *sched) {}
    /**
     * Perform a post-solve step
     * @param sched
     */
    virtual void postsolve(Schedule *sched) {}
    
public:
    /**
     * Constructor
     */
    explicit Solver() {};
    virtual void forward_params(const std::map<std::string, std::string> &args) override ;
    
    virtual std::string help() override;

    /*!
     * \copydoc CompilerPass::run
     * \remark Execute the 3 steps: pre-solve, solve, post-solve. The solving time then includes all three steps
     */
    void run(SystemModel *m, config_t *c, Schedule *s) override;

    // Bring in the overloads from CompilerPass
    using CompilerPass::run;

    ACCESSORS_RW(Solver, std::string, type);
    ACCESSORS_RW(Solver, timinginfos_t, timeout);
    ACCESSORS_RW(Solver, SolverProperties::SolverTargets, target);
};

/*!
 * \typedef SolverRegistry
 * \brief Registry containing all solvers
 */
using SolverRegistry = registry::Registry<Solver, std::string>;

/*!
 * \brief Helper macro to register a new derived solver
 */
#define REGISTER_SOLVER(ClassName, Identifier) \
  REGISTER_SUBCLASS(Solver, ClassName, std::string, Identifier)

#endif /* SOLVER_H */


/*
 * Copyright (C) 2021 brouxel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* 
 * File:   ResetSchedule.hpp
 * Author: brouxel
 *
 * Created on 1 juillet 2021, 10:57
 */

#ifndef RESETSCHEDULE_HPP
#define RESETSCHEDULE_HPP

#include "Solver.hpp"

class ResetSchedule : public Solver {
public:
    ResetSchedule() : Solver() {}
    
    const std::string get_uniqid_rtti() const override;
    virtual std::string help() override;
    
protected:
    virtual void solve(Schedule *sched) override;
    virtual void forward_params(const std::map<std::string, std::string> &) override;
};

REGISTER_SOLVER(ResetSchedule, "reset")

#endif /* RESETSCHEDULE_HPP */


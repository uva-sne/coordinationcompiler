#include "LIST_EA.h"
#include <unordered_set>
#include <boost/random/uniform_int_distribution.hpp>
#include <boost/random/variate_generator.hpp>
#include <boost/random/uniform_real.hpp>


/*****************************************************************************
 * LIST_EA_Population
*****************************************************************************/
void LIST_EA_Population::set_population_size(uint32_t population_size, uint32_t offspring_size) {
    this->population_size = population_size;
    this->offspring_size = offspring_size;
}

void LIST_EA_Population::set_num_tasks(uint32_t n) {
    num_tasks = n;
}


void LIST_EA_Population::init_schedules(Schedule *copy) {

    Utils::INFO("Population size: " + to_string(population_size));
    Utils::INFO("Number of tasks: " + to_string(num_tasks));

    for (uint32_t i = 0; i < population_size + offspring_size; i++) {
        Schedule sched(*copy);
        schedules.emplace_back(sched);
    }

    Utils::INFO("Init schedule elements size:" + to_string(copy->elements_lut().size()));
}

void LIST_EA_Population::init_data(const SystemModel& systemmodel) {
    //TODO Single phase only!
    //TODO Just work with map instead of vector
    const std::map<std::string, Task*>& temp_tasks = systemmodel.tasks();
    std::vector<Task*> tasks;
    for(const auto & temp_task : temp_tasks) {
        tasks.push_back( temp_task.second );
    }

    chromosomes.resize(population_size + offspring_size);
    fitness.resize(population_size + offspring_size);
    max_version_id.resize(num_tasks);
    allele_version_map.resize(num_tasks);

    // Get all available processors
    for (ComputeUnit* cu: systemmodel.processors()) {
        compute_units.push_back(cu);
    }

    // Get total number of versions for each task and setup allele to version mapping
    int task_id=0;
    for (Task* task: tasks) {

        gene_t gene = 0;
        for (Version* mapping: task->versions()) { //mapping used to be
            if (!gpu_enabled && mapping->gpu_required()) continue;
            // Remember what value maps to what version
            assert(mapping != nullptr);
            allele_version_map[task_id][gene++] = mapping;
        }
        max_version_id[task_id] = gene;
        task_id++;
    }

    // Randomly initialise chromosomes
    for (chromosome_t& chromosome: chromosomes) {
        chromosome.resize(num_tasks);
        for (uint task_id = 0; task_id < chromosome.size(); task_id++) {
            chromosome[task_id] = random_int(0, max_version_id[task_id] - 1); //TODO random mapping
        }
    }

    // We do this so we can retrieve the correct gene for each SchedElt
    // Probably unnecessary but this guarantees correct behaviour if SchedElts
    // are added to Schedules in a different order than Tasks are added
    // to Systemmodel.
    std::map<Task*, int> task_id_map;
    for (size_t task_id = 0; task_id < tasks.size(); task_id++) {
        task_id_map[tasks.at(task_id)] = task_id;
    }
    for (Schedule& sched: schedules) {
        for (SchedEltPtr &elt: sched.elements()) {
            if (Utils::isa<SchedJob*>(elt.get())) {
                Task* t = const_cast<Task *>(elt.get()->task());
                schedelt_task_id_map[elt.get()] = task_id_map[t];
            }
        }
    }

    schedule_all();
}


float LIST_EA_Population::random_float(float min, float max) {
    boost::uniform_real<float> u(min, max);
    boost::variate_generator<boost::mt19937&, boost::uniform_real<float>> gen(rng, u);
    return gen();
}

int LIST_EA_Population::random_int(int min, int max) {
    boost::random::uniform_int_distribution<> u(min, max);
    boost::variate_generator<boost::mt19937&, boost::random::uniform_int_distribution<int> > gen(rng, u);
    return gen();
}

int LIST_EA_Population::random_normal_int(float m, float s) {
    boost::random::normal_distribution<float> u(m, s);
    boost::variate_generator<boost::mt19937&, boost::random::normal_distribution<float>> gen(rng, u);
    return (int) std::round(gen());
}

int LIST_EA_Population::tournament(uint32_t k) {

    if (k >= population_size) {
        k /= 2;
    }

    std::vector<int> indices;

    while (indices.size() < k) {
        indices.push_back(random_int(0, population_size - 1));
    }

    int winner = indices[0];
    for (int index: indices) {
        if (fitness[index] > fitness[winner]) {
            winner = index;
        }
    }

    return winner;
}

void LIST_EA_Population::recombine() {

    evaluate_fitness(0, population_size);
    const int max_crossover_point = chromosomes[0].size();
    for (uint32_t child_index = population_size; child_index < population_size + offspring_size; child_index++) {
        
        int crossover_point = random_int(0, max_crossover_point - 2);
        std::pair<int, int> parents = {tournament(4), tournament(4)}; // TODO
        std::copy(chromosomes[parents.first].begin(),
                  chromosomes[parents.first].begin() + crossover_point,
                  chromosomes[child_index].begin());
        std::copy(chromosomes[parents.second].begin() + crossover_point,
                  chromosomes[parents.second].end(),
                  chromosomes[child_index].begin() + crossover_point);

    }

}

void LIST_EA_Population::mutate(float p_mutate) {

    for (chromosome_t& chromosome: chromosomes) {
        for (uint32_t i = 0; i < chromosome.size(); i++) {
            if (random_float() < p_mutate) {
                chromosome[i] = (chromosome[i] + random_int(0, max_version_id[i])) % max_version_id[i];
                assert(chromosome[i] < max_version_id[i]);
                // chromosome[i] += random_normal_int(0, max_version_id[i]) % max_version_id[i];
            }
        }
    }

}

// Return true if s contains all elements in v , false otherwise
bool contains_all(const std::set<SchedElt*>& s, const std::vector<SchedElt*>& v) {
    for (auto* item: v) {
        if (s.find(item) == s.end()) return false;
    }
    return true;
}

// Add all elements in v to q
void add_all(const std::vector<SchedElt*>& v, std::queue<SchedElt*>& q) {
    for (auto* e: v) {
        q.push(e);
    }
}


bool overlap(SchedElt* a, SchedElt* b) {
    return (a->rt() + a->wct()) > b->rt() && a->rt() < (b->rt() + b->wct());
}

void LIST_EA_Population::schedule_efls(Schedule& sched, const chromosome_t& chromosome) {
    std::set<SchedElt*> done;
    std::priority_queue<SchedComponent,
                    std::vector<SchedComponent>,
                    std::greater<SchedComponent>> ready_queue;
    std::queue<SchedElt*> consider_queue;
    std::map<SchedElt*, timinginfos_t> done_end_time;
    
    std::map<ComputeUnit*, timinginfos_t> cu_available_time;
    for (ComputeUnit* cu: compute_units) {
        cu_available_time[cu] = 0;
    }

    for (SchedEltPtr &elt: sched.elements()) {
        if (elt->previous().size() == 0) {
            int tid = schedelt_task_id_map[elt.get()];
            gene_t all = chromosome[tid];
            Version* cv = allele_version_map[tid][all];
            ready_queue.push({elt.get(), 0, cv->C()});
        }
    }

    while (!ready_queue.empty()) {

        // Get next element to be scheduled
        SchedComponent current = ready_queue.top();
        SchedElt* current_elt = current.element;
        current_elt->rt(current.release_time);
        ready_queue.pop();
        if (done.find(current_elt) != done.end()) {
            continue;
        }

        int task_id = schedelt_task_id_map[current.element];
        gene_t allele = chromosome[task_id];
        Version* current_version = allele_version_map[task_id][allele];

        bool change = true;
        sched.schedule(current_elt);
        current_elt->wct(current_version->C());
        current_elt->wce(current_version->C_E());
        current_elt->schedtask()->selected_version(current_version->id());
        current_elt->schedtask()->frequency(current_version->frequency());
        current_elt->schedtask()->gpu_required(false); // TODO
        current_elt->schedtask()->gpu_frequency(0); // TODO


        std::priority_queue<std::pair<ComputeUnit*, timinginfos_t>,
                            std::vector<std::pair<ComputeUnit*, timinginfos_t>>,
                            std::less<std::pair<ComputeUnit*, timinginfos_t>>> sched_prioq;
        for (ComputeUnit* cu: current_version->force_mapping_proc()) {
            if (cu_available_time.find(cu) == cu_available_time.end()) {
                continue;
            }
            sched_prioq.push({cu, cu_available_time[cu]});
        }

        if (sched_prioq.empty()) {
            sched.status(ScheduleProperties::sched_statue_e::SCHED_UNSCHEDULABLE);
            return;
        }

        ComputeUnit* selected_cu = nullptr;
        while (change) {
            change = false;
            auto cu_available = sched_prioq.top();
            sched_prioq.pop();
            ComputeUnit* cu = cu_available.first;

            
            for (SchedElt* t: done) {
                if (sched.get_mapping(t) == sched.schedcore(cu)) {
                    if (overlap(current_elt, t)) {
                        current_elt->rt(t->rt() + t->wct());
                        change = true;
                    }
                }
            }

            for (SchedElt* t: done) {
                if (sched.get_mapping(t)->core()->voltage_island_id == cu->voltage_island_id) {
                    if (overlap(current_elt, t)) {
                        if (current_elt->schedtask()->frequency() != t->schedtask()->frequency()) {
                            current_elt->rt(t->rt() + t->wct());
                            change = true;
                        }
                    }
                }
            }
            sched_prioq.push({cu, current_elt->rt()});
            SchedJob* ct = sched.schedjob(current_elt->id());
            ct->selected_version(current_version->id());
            sched.map_core(ct, sched.schedcore(cu));
            ct->rt(current_elt->rt());
            selected_cu = cu;
        }

        // Utils::INFO("Scheduled version: " + current_elt->schedtask()->selected_version());
        // Utils::INFO("RT: " + to_string(current_elt->rt()));

    
        // Add scheduled element to the done queue
        done.insert(current_elt);
        done_end_time[current_elt] = current_elt->rt() + current_elt->wct();
        cu_available_time[selected_cu] = done_end_time[current_elt];

        // Add all successors to consideration queue
        // add_all(current->successors(), consider_queue);
        for (SchedElt* elt: current.element->successors()) {
            consider_queue.push(elt);
        }

        timinginfos_t release_time = 0;
        size_t max_new_elements = consider_queue.size();


        //---------- See which element can be scheduled next
        // Check consideration queue and add elements to ready queue
        // if all predecessors have been scheduled
        for (size_t i = 0; i < max_new_elements; i++) {
            bool all_in = true;
            timinginfos_t rt = 0;
            SchedElt* next = consider_queue.front();
            consider_queue.pop();

            for (SchedElt* elt: next->previous()) {
                if (done.find(elt) == done.end()) {
                    all_in = false;
                    break;
                }
                rt = done_end_time[elt] > rt ? done_end_time[elt] : rt;
            }
            if (all_in) {
                int tid = schedelt_task_id_map[next];
                gene_t all = chromosome[tid];
                Version* cv = allele_version_map[tid][all];
                ready_queue.push({next, rt, cv->C()});
            } else {
                consider_queue.push(next);
            }
        }

    }
    // sched.compute_energy();
    // sched.compute_makespan();
    // Utils::INFO(">>>> MAKESPAN: " + to_string(sched.makespan()));
    // Utils::INFO(">>>> ENERGY: " + to_string(sched.energy()));

    sched.status(ScheduleProperties::sched_statue_e::SCHED_COMPLETE);
}

void LIST_EA_Population::schedule_all() {
    for (size_t i = 0; i < population_size + offspring_size; i++) {
        schedule_efls(schedules[i], chromosomes[i]);
    }
}

void LIST_EA_Population::evaluate_fitness() {
    evaluate_fitness(0, schedules.size());
}

void LIST_EA_Population::evaluate_fitness(uint32_t lower, uint32_t higher) {

    fitness_queue = std::priority_queue<std::pair<float, int>>();

    for (uint32_t i = lower; i < higher; i++) {
        set_fitness(i);
        fitness_queue.push({get_fitness(i), i});
    }
}

void LIST_EA_Population::set_fitness(uint32_t individual) {
    
    if (schedules[individual].status() != ScheduleProperties::sched_statue_e::SCHED_COMPLETE) {
        fitness[individual] = FLT_MIN;
        return;
    }

    schedules[individual].compute_energy();
    energycons_t energy = schedules[individual].energy();
    fitness[individual] = -1.0f * static_cast<float>(energy);
}

float LIST_EA_Population::get_fitness(uint32_t individual) const {
    return fitness[individual];
}

void LIST_EA_Population::swap_individuals(int i, int j) {
    using std::swap;
    swap(chromosomes[i], chromosomes[j]);
    swap(schedules[i], schedules[j]);
    swap(fitness[i], fitness[j]);

}

// TODO test proper algorithm performance
void LIST_EA_Population::select() {

    evaluate_fitness();

    std::vector<int> new_order;
    while (!fitness_queue.empty()) {
        new_order.push_back(fitness_queue.top().second);
        fitness_queue.pop();
    }

    for (uint32_t i = 0; i < population_size; i++) {

        int j = new_order[i];
        
        while (j < (int) i) {
            j = new_order[j];
        }
        swap_individuals(i, j);
        
    }
}



std::string LIST_EA_Population::chromosome_to_string(int index) {

    std::string message = "";
    std::string version_message = "";
    for (gene_t gene: chromosomes[index]) {
        message += to_string(gene) + " ";
    }

    for (uint i = 0; i < chromosomes[index].size(); i++) {
        gene_t gene = chromosomes[index][i];
        Version* v = allele_version_map[i][gene];
        version_message += v->id() + " ";
    }

    return message + version_message;

}

void LIST_EA_Population::evolve(float p_mutate) {
    recombine();
    mutate(p_mutate);
    schedule_all();
    select();
}

/*****************************************************************************
 * LIST_EA
*****************************************************************************/

LIST_EA::LIST_EA(const SystemModel &m, const config_t &c) : EAStrategy(m,c) {}


void LIST_EA::init_population(Schedule* copy) {

    uint32_t num_tasks = tg.tasks().size();

    population.set_population_size(population_size, offspring_size);
    population.set_num_tasks(num_tasks);

    population.init_schedules(copy);
    population.init_data(tg);


}

void LIST_EA::evolve_population() {
    std::cout << "Current best: " << get_best_schedule().energy() << std::endl;
    population.evolve(p_mutate);
}

Schedule LIST_EA::get_best_schedule() {
    population.set_best_individual();
    population.set_worst_individual();
    return *(population.get_best_individual());
}

void LIST_EA::reset_learning(float input) {

}
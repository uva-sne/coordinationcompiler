/*
 * Copyright (C) 2020 Julius Roeder <j.roeder@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* Title: Energy-efficient Static Task Scheduling on VFI based NoC-HMPSoCs for Intelligent Edge Devices in Cyber-Physical Systems
 * Authors: UMAIR ULLAH TARIQ
 * Year: 2019
 */

#include "Utils/Log.hpp"
#include "ARSH_FATI.h"


void ARSH_FATI_Population::set_population_size(int pop_size, int n_tasks) {
    population_size = pop_size;
    num_tasks = n_tasks;

    Utils::INFO("Population size: " + to_string(pop_size));
    individual_tasks.resize(population_size);
    frequency.resize(population_size);
    fitness.resize(population_size);
    energy.resize(population_size);
}

void ARSH_FATI_Population::set_global_deadline() {
    assert(tg != nullptr);
    global_deadline = tg->sequential_length();
}

std::vector<SchedElt*> ARSH_FATI_Population::reverse_graph(int individual){
    // build reversed topological order
    std::vector<SchedElt*> task_graph_reversed;
    for (auto& test: schedules[individual].elements()){
        if(Utils::isa<SchedJob*>(test.get())){
            task_graph_reversed.push_back(test.get());
        }
    }
    std::reverse(task_graph_reversed.begin(), task_graph_reversed.end());
    if (!Utils::isReversedTopologicalOrder(task_graph_reversed)) Utils::WARN("Reversing the topology didn't work");

    return task_graph_reversed;
}

int256_t ARSH_FATI_Population::calculate_EG(energycons_t original_energy, timinginfos_t original_makespan, energycons_t energy_prime, timinginfos_t makespan_prime){
    if (original_energy > energy_prime || original_makespan >= makespan_prime){
        return (gamma * (original_energy - energy_prime));
    } else {
        return ((original_energy - energy_prime)/(makespan_prime - original_makespan));
    }
}

void ARSH_FATI_Population::energy_gradient_descent(int individual){
    schedules[individual].compute_makespan();
    schedules[individual].compute_energy();


    if (schedules[individual].makespan() <= global_deadline){
        // find the "extensible islands"; i.e. islands where reducing the voltage doesn't lead to too long makespan
        
        for (auto &island: frequency[individual]){
            int256_t Gamma = -1;
            int256_t EG_best = INT64_MIN;
            energycons_t energy_prime_prime = 0;
            timinginfos_t makespan_prime_prime = 0;

            Schedule copy(schedules[individual]);
            energycons_t original_energy = schedules[individual].energy();
            timinginfos_t original_makespan = schedules[individual].makespan();

            for (auto &freq: island.first->frequency_watt){
                uint128_t temp_freq = island.second;
                island.second = freq.first;

                construct_schedule(individual);
                schedules[individual].compute_makespan();
                schedules[individual].compute_energy();
                energycons_t energy_prime = schedules[individual].energy();
                timinginfos_t makespan_prime = schedules[individual].makespan();

                island.second = temp_freq;

                if (makespan_prime <= global_deadline){
                    int256_t EG = calculate_EG(original_energy, original_makespan, energy_prime, makespan_prime);
                    if (EG_best < EG){
                        EG_best = EG;
                        Gamma = freq.first;
                        energy_prime_prime = energy_prime;
                        makespan_prime_prime = makespan_prime;
                    }
                }
                // restore original
                schedules[individual] = copy;
                schedules[individual].makespan(original_makespan);
                schedules[individual].energy(original_energy);
            }
            if (Gamma != 1){
                // update island frequency
                island.second = (uint128_t) Gamma;
                schedules[individual].makespan(makespan_prime_prime);
                schedules[individual].energy(energy_prime_prime);
            }


        }
    }
}

void ARSH_FATI_Population::energy_gradient_descent_population(){
//    construct schedule from mapping and voltage frequency
    for (size_t ind = 0; ind < population_size; ind++){
        energy_gradient_descent(ind);
    }
}

void ARSH_FATI_Population::set_wcet(int individual, std::vector<SchedElt*> task_graph_reversed){
    // compute wcet across all DVFS excluding GPU versions
    // traverse graph in reversed
    for(auto &task: task_graph_reversed) {

        // calculate wcet for the task
        timinginfos_t wcet_task = 0;
        energycons_t wcee_task = 0;
        uint64_t frequency_task = 0;
        std::string version_task = "";
        uint128_t task_freq = 0;

        for (auto &version: task->schedtask()->task()->versions()) {
            // dont take into account gpu tasks
            if (version->gpu_required()) continue;
            // if version and mapping on same core
            bool found = false;

            //find the frequency at which this version "must" execute according to the frequency set by the scheduler
            ComputeUnit *proc = *version->force_mapping_proc().begin();
            for (auto island: frequency[individual]){
                if(proc->voltage_island_id == island.first->id){
                    task_freq = island.second;
                }
            }
            if(version->frequency() != task_freq) continue; //if the

            string proc_mapping = "P" + to_string(individual_tasks[individual][task].mapping);
            for (auto proc_id: version->force_mapping_proc()) { //if this version can run on the selected core
                if (proc_id->id == proc_mapping) found = true;
            }

            if (!found) continue;

            // if version wcet is larger
            if (version->C() > wcet_task) {
                wcet_task = version->C();
            }

            wcet_task = version->C();
            wcee_task = version->C_E();
            version_task = version->id();
            frequency_task = task_freq; // need to only copy at the end otherwise task_freq gets set wrong.
        }

        individual_tasks[individual][task].task_wcet = wcet_task;
        individual_tasks[individual][task].task_wcee = wcee_task;
        individual_tasks[individual][task].selected_version = version_task;
        individual_tasks[individual][task].frequency = frequency_task;
        individual_tasks[individual][task].t = task;
        individual_tasks[individual][task].absolute_wcet = wcet_task;
    }
}

void ARSH_FATI_Population::set_d_prime(int individual, std::vector<SchedElt*> task_graph_reversed){
    // compute the ECD, d_prime
    // traverse graph in reversed
    for(auto &task: task_graph_reversed) {
            //if its a sink node set d' to the global deadline
        if (task->successors().size() == 0){
            individual_tasks[individual][task].d_prime = tg->sequential_length();
        }else{ // else set d' to the minimum start time of its successors
            int256_t temp_d_prime = UINT64_MAX;
            for(auto &pred: task->successors()){
                uint128_t temp_min_time = individual_tasks[individual][pred].d_prime - individual_tasks[individual][pred].absolute_wcet;
                temp_d_prime = (temp_min_time < temp_d_prime) ? temp_min_time : temp_d_prime;
            }
            if (temp_d_prime < 0) Utils::WARN("temp-prime is negative");
            individual_tasks[individual][task].d_prime = (uint128_t)temp_d_prime;
        }
    }
}

SchedElt*  ARSH_FATI_Population::get_lowest_d_prime(std::vector<SchedElt*> ready_q, int individual){
    // take the node with the smallest d_prime; smaller index is tie breaker
    uint128_t temp_lowest_d_prime = UINT64_MAX;
    SchedElt* to_be_scheduled;
    for(auto &t1: ready_q){
        if(individual_tasks[individual][t1].d_prime < temp_lowest_d_prime ){
            temp_lowest_d_prime = individual_tasks[individual][t1].d_prime;
            to_be_scheduled = t1;
        }
    }
    return to_be_scheduled;
}

void ARSH_FATI_Population::schedule_task(SchedElt* to_be_scheduled, int individual){
    SchedElt *copy_elt = schedules[individual].schedjob(to_be_scheduled->id());
    schedules[individual].schedule(copy_elt);

    // Schedule task according to R1, R2 and R3
    // set release time of scheduled task
    to_be_scheduled->rt(individual_tasks[individual][to_be_scheduled].release_time);
    to_be_scheduled->wct(individual_tasks[individual][to_be_scheduled].task_wcet);
    to_be_scheduled->wce(individual_tasks[individual][to_be_scheduled].task_wcee);
    to_be_scheduled->schedtask()->selected_version(individual_tasks[individual][to_be_scheduled].selected_version);
    to_be_scheduled->schedtask()->frequency(individual_tasks[individual][to_be_scheduled].frequency);

    to_be_scheduled->schedtask()->gpu_required(false);
    to_be_scheduled->schedtask()->gpu_frequency(0);
    uint128_t end_time = to_be_scheduled->rt() + to_be_scheduled->wct();

    //update successor release time
    for (auto suc: to_be_scheduled->successors()) {
        if (end_time > individual_tasks[individual][suc].release_time)  individual_tasks[individual][suc].release_time = end_time;
    }

    //if two tasks mapped on the same core make sure that the later task starts after (or something like that)
    for (auto &ind: individual_tasks[individual]){
        if (ind.first == to_be_scheduled) continue; //make sure they aren't the same
        if (ind.second.mapping == individual_tasks[individual][to_be_scheduled].mapping){ //on same processor
            if (end_time > ind.second.release_time) ind.second.release_time = end_time; //increase release time if necessary
        }
    }

    SchedJob *ct = schedules[individual].schedjob(to_be_scheduled->id());
    ct->selected_version(individual_tasks[individual][to_be_scheduled].selected_version);

    ComputeUnit * target = NULL;
    for (auto &proc: tg->processors()){
        std::string target_string = "P" + to_string(individual_tasks[individual][to_be_scheduled].mapping);
        if (proc->id == target_string){
            target = proc;
        }
    }

    schedules[individual].map_core(ct, schedules[individual].schedcore(target));
    ct->rt(individual_tasks[individual][to_be_scheduled].release_time);

}

void ARSH_FATI_Population::construct_schedule(int individual){
    std::vector<SchedElt*> task_graph_reversed = reverse_graph(individual);
    set_wcet(individual, task_graph_reversed);
    set_d_prime(individual, task_graph_reversed);

    std::vector<SchedElt*> ready_q;
    std::vector<SchedElt*> done_q;
    std::vector<SchedElt*>::iterator it;

    schedules[individual].status(ScheduleProperties::sched_statue_e::SCHED_NOTDONE);

    // insert source node into ready queue (R)
    for(SchedJob *task: schedules[individual].schedjobs()){
        if (task->previous().size() == 0) ready_q.push_back(task);
    }

    //reset release time
    for(auto &task: individual_tasks[individual]){
        task.second.release_time = 0;
    }

    // while nodes in R
    while (!ready_q.empty()){
        SchedElt* to_be_scheduled = get_lowest_d_prime(ready_q, individual);
        schedule_task(to_be_scheduled, individual);

        // remove from ready_q
        it = find(ready_q.begin(), ready_q.end(), to_be_scheduled);
        if(it != ready_q.end()){
            ready_q.erase(it);
        }
        //added to done_q
        done_q.push_back(to_be_scheduled);

        // add additional tasks to ready_q
        for(auto &task: schedules[individual].elements()){
            if(!Utils::isa<SchedJob*>(task.get())) continue;
            //if already scheduled
            it = find(done_q.begin(), done_q.end(), task.get());
            if(it != done_q.end())continue;
            // if predecessors scheduled;
            bool ready_to_be_scheduled = true;
            for (auto pred: task->previous()){
                it = find(done_q.begin(), done_q.end(), pred);
                if (it == done_q.end()) ready_to_be_scheduled=false;
            }
            // make sure not to add tasks twice
            it = find(ready_q.begin(), ready_q.end(), task.get());
            if(it != ready_q.end()){
                ready_to_be_scheduled=false;
            }
            if (ready_to_be_scheduled) ready_q.push_back(task.get());
        }
    }
    schedules[individual].status(ScheduleProperties::sched_statue_e::SCHED_COMPLETE);
}

void ARSH_FATI_Population::construct_schedules_population(){
    // construct schedule from mapping and voltage frequency
    for (size_t ind=0; ind < population_size; ind++){
        construct_schedule(ind);
    }
}

void ARSH_FATI_Population::calc_population_fitness(){
    // fitness
    for (size_t ind=0; ind < population_size; ind++){
            fitness[ind] = individual_fitness(ind);
    }
}

long double ARSH_FATI_Population::individual_fitness(int individual){
    schedules[individual].compute_makespan();
    schedules[individual].compute_energy();
    //if m < D then 1 / energy
    if (schedules[individual].makespan() <= global_deadline) {
            return (1 / (long double)(schedules[individual].energy()/1000));
    } else{ //- inf
        return (LDBL_MIN);
    }
}

Schedule ARSH_FATI_Population::get_schedule(int individual){
    return schedules[individual];
}

void ARSH_FATI_Population::set_tg(const SystemModel *m) {
    tg = m;
}

void ARSH_FATI_Population::set_schedules(Schedule* copy){
    for (size_t ind=0; ind < population_size; ind++) {
        Schedule base(*copy);
        schedules.emplace_back(base);
    }
}

void ARSH_FATI_Population::set_tasks(){
    for (size_t i = 0; i < population_size; i++){
        for (auto &sched: schedules[i].elements()){
            if (!Utils::isa<SchedJob*>(sched.get())) continue;
            individual_task new_individual;
            individual_tasks[i][sched.get()] = new_individual;
        }
    }

}

ARSH_FATI_Population::ARSH_FATI_Population() {

}

ARSH_FATI_Population::~ARSH_FATI_Population() {
    individual_tasks.clear();
    frequency.clear();
}

//---------------------------------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------------------
ARSH_FATI::ARSH_FATI(const SystemModel &m, const config_t &c) : EAStrategy(m,c) {}

int ARSH_FATI::random_mapping(){
    boost::random::uniform_int_distribution<> dist(1,num_procs);
    return dist(int_gen);
}

void ARSH_FATI::init_population(Schedule* copy) {

    population.set_population_size(population_size, num_tasks);
    population.set_tg(&tg);
    population.set_schedules(copy);
    population.set_tasks();
    population.set_global_deadline();
    Utils::INFO("Deadline sequential energy: " + to_string(tg.sequential_length_energy_based()));
    Utils::INFO("Deadline sequential: " + to_string(tg.sequential_length()));

    // for each individual in the population (mu) make a random mapping for each task (V)
    for (int individual=0; individual < population_size; individual++){
        for (auto &i_task: population.schedules[individual].elements()){
            if (!Utils::isa<SchedJob*>(i_task.get())) continue;
            population.individual_tasks[individual][i_task.get()].mapping = random_mapping();
        }
    }

    // for each individual in the population (mu) for each voltage island set the maximum DVFS
    for (auto &individual: population.frequency){
        for (size_t i=0; i < tg.voltage_islands().size(); i++){
            individual.insert(std::pair<voltage_island*, int>(tg.voltage_islands()[i], tg.voltage_islands()[i]->max_frequency));
        }
    }

    // evaluate fitness for all individuals in population
    population.construct_schedules_population();
    population.energy_gradient_descent_population();
    population.calc_population_fitness();
}

float ARSH_FATI::random_float(float min, float max) {
    boost::uniform_real<float> u(min, max);
    boost::variate_generator<boost::mt19937&, boost::uniform_real<float> > gen(int_gen, u);
    return gen();
}

int ARSH_FATI::random_int(int min, int max) {
    boost::uniform_int<int> u(min, max);
    boost::variate_generator<boost::mt19937&, boost::uniform_int<int> > gen(int_gen, u);
    return gen();
}

int ARSH_FATI::new_mapping_alpha(int best_solution){

    float r1 = random_float(0,1);
    float r2 = random_float(0,1);

    if (r1 < alpha) {
        return best_solution;
    } else {
        return ceil(r2 * num_procs);
    }
}

int ARSH_FATI::new_mapping_uniform(){

    float r = random_float(0, 1);
    return ceil(r * num_procs);
}

int ARSH_FATI::new_mapping_binselect(std::vector<int>& best_mapping){

    int r = random_int(0, best_mapping.size() - 1);
    return best_mapping[r];

}

void ARSH_FATI::evolve_population() {

    // find best and worst solution
    population.set_best_individual();
    population.set_worst_individual();
    Schedule * best = population.get_best_individual();


    // NOTE needed for binselect remapping  
    // std::vector<int> best_mapping;
    // best_mapping.reserve(best->mapping().size());
    // for (auto task_map: best->mapping()) {
    //     best_mapping.push_back(task_map.second->core()->sysID + 1);
    // }

    // NOTE needed for alpha remapping
    std::map<std::string, int> best_mapping;
    for (auto task_map: best->mapping()) {
        if (!Utils::isa<SchedJob*>(task_map.first)) {
                continue;
            }
        best_mapping[((SchedJob*)task_map.first)->task()->ilp_label()] = task_map.second->core()->sysID + 1;
    }


    // NOTE keep DR for now to verify that it stays constant
    std::cout << "arsh fati population evolve " << best->energy() << " " << DR << std::endl;

    // for all individuals in population
    for (int ind=0; ind < population_size; ind++) {
        //for all tasks in a graph
        for (auto &task: population.individual_tasks[ind]) {
            if (!Utils::isa<SchedJob*>(task.first)) {
                continue;
            }
            // Uniform mapping
            // task.second.mapping = new_mapping_uniform();

            // Alpha mapping
            task.second.mapping = new_mapping_alpha(best_mapping[((SchedJob*)task.first)->task()->ilp_label()]);
        
            // Binselect mapping
            // task.second.mapping = new_mapping_binselect(best_mapping);
        }
        // update the schedule
        population.construct_schedule(ind);
        population.energy_gradient_descent(ind);

        // evaluate fitness
        long double fitness = population.individual_fitness(ind);
        population.fitness[ind] = fitness;
        population.energy[ind] = population.schedules[ind].energy();
    }

}

Schedule ARSH_FATI::get_best_schedule() {
    population.set_best_individual();
    population.set_worst_individual();
    Schedule individual = *population.get_best_individual();
    return individual;
}
/*
 * Copyright (C) 2020 Julius Roeder <j.roeder@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef METHANE_EVOLUTIONARYALGORITHM_H
#define METHANE_EVOLUTIONARYALGORITHM_H

#include <cstdlib>
#include <vector>
#include <string>
#include <map>
#include <algorithm>
#include <array>
#include <cmath>
#include <cassert>
#include <chrono>
#include <tuple>
#include <functional>

#include <boost/algorithm/string.hpp>
#include <boost/multiprecision/cpp_int.hpp>
#include <boost/math/tools/precision.hpp>

#include "config.hpp"
#include "Utils.hpp"
#include "SystemModel.hpp"
#include "Schedule.hpp"
#include "Solvers/Solver.hpp"
#include "registry.hpp"

class Population {
protected:
    // maximum population size
    Schedule* best_individual = nullptr;
    Schedule* worst_individual = nullptr;

public:
    uint32_t population_size = 0;
    uint32_t num_tasks = 0;

    // actual population of schedules
    std::vector<Schedule> schedules;

    explicit Population();
    virtual ~Population();
    Population(const Population&) = delete;

    virtual void set_population_size(int pop_size);
    virtual void set_best_individual() ;
    virtual void set_worst_individual();
    virtual Schedule *get_best_individual() {return best_individual;}
    virtual Schedule *get_worst_individual() {return worst_individual;}
};

class EAStrategy{
protected:
    const SystemModel &tg;
    const config_t &conf;

public:
    explicit EAStrategy(const SystemModel &m, const config_t &c) : tg(m), conf(c) {};
    virtual ~EAStrategy() = default;
    EAStrategy(const EAStrategy&) = delete;

    virtual void init_population(Schedule *copy) = 0;
    virtual void evolve_population() = 0;
    virtual Schedule get_best_schedule() = 0;
    virtual void reset_learning(float input) = 0;
};


using EAStrategyRegistry = registry::Registry<EAStrategy, std::string, const SystemModel &, const config_t &>;

#define REGISTER_EASTRATEGY(ClassName, Identifier) \
  REGISTER_SUBCLASS(EAStrategy, ClassName, std::string, Identifier, const SystemModel &, const config_t &)


class EAScheduling : public Solver {
    EAStrategy *strategy;
    std::string _type;

public:
    explicit EAScheduling() : Solver() {};
    const std::string get_uniqid_rtti() const override { return "solver-EA"; }
    virtual std::string help() override;

    virtual void solve(Schedule *result) override;
    virtual void do_run(Schedule *sched);

    void presolve(Schedule *sched) override;
    void postsolve(Schedule *sched) override;
};

REGISTER_SOLVER(EAScheduling, "EA")

#endif //METHANE_EVOLUTIONARYALGORITHM_H

/*
 * Copyright (C) 2020 Julius Roeder <j.roeder@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* Title: Energy-efficient Static Task Scheduling on VFI based NoC-HMPSoCs for Intelligent Edge Devices in Cyber-Physical Systems
 * Authors: UMAIR ULLAH TARIQ
 * Year: 2019
 */

#ifndef METHANE_ARSH_FATI_H
#define METHANE_ARSH_FATI_H

#include "EvolutionaryAlgorithm.h"
#include <boost/random/mersenne_twister.hpp>
#include <boost/random/uniform_int_distribution.hpp>
#include <boost/random/uniform_real.hpp>
#include <boost/random/uniform_int.hpp>
#include <boost/random/variate_generator.hpp>
#include <boost/multiprecision/gmp.hpp>

#include "Utils/Graph.hpp"

using namespace std;

class ARSH_FATI_Population: public Population {
protected:
    Schedule *base_schedule=NULL;
    const SystemModel *tg;
    uint128_t gamma = 9999;

    struct individual_task{
        SchedElt *t = NULL;
        int mapping = 0;
        uint128_t d_prime = 0;
        timinginfos_t absolute_wcet = 0;
        timinginfos_t task_wcet = 0;
        energycons_t task_wcee = 0;
        timinginfos_t release_time = 0;
        uint128_t frequency = 0;
        std::string selected_version = "";
    };

public:
    std::vector<std::map<SchedElt*, individual_task>> individual_tasks;
    std::vector<std::map<voltage_island*, uint128_t>> frequency;
    std::vector<long double> fitness;
    std::vector<energycons_t> energy;
    uint128_t global_deadline = 0;

    ARSH_FATI_Population();
    ~ARSH_FATI_Population();
    ARSH_FATI_Population(const ARSH_FATI_Population&) = delete;

    void set_population_size(int pop_size, int n_tasks);
    void set_global_deadline();
    void set_schedules(Schedule* copy);
    void construct_schedules_population();
    void calc_population_fitness();
    long double individual_fitness(int individual);
    void construct_schedule(int individual);
    Schedule get_schedule(int individual);
    std::vector<SchedElt*> reverse_graph(int individual);
    void set_d_prime(int individual, std::vector<SchedElt*> task_graph_reversed);
    void set_wcet(int individual, std::vector<SchedElt*> task_graph_reversed);
    void set_tasks();
    void set_tg (const SystemModel *tg);
    SchedElt* get_lowest_d_prime(std::vector<SchedElt*> ready_q, int individual);
    void schedule_task(SchedElt* to_be_scheduled, int individual);

    void energy_gradient_descent_population();
    void energy_gradient_descent(int individual);
    int256_t calculate_EG(energycons_t original_energy, timinginfos_t original_makespan, energycons_t energy_prime, timinginfos_t makespan_prime);
};

class ARSH_FATI : public EAStrategy{
protected:
    float DR = 0.3; //from paper
    float lambda = 0.9;
    float alpha = 0.8; // Probability of selecting mapping in the best solution 
    int population_size = 500;
    ARSH_FATI_Population population;
    int num_procs = tg.processors().size();
    int num_tasks = tg.tasks().size();

    // random ints between 0 and num procs
    boost::mt19937 int_gen;

public:

    explicit ARSH_FATI(const SystemModel &m, const config_t &c);

    virtual void init_population(Schedule* copy) override ;
    virtual void evolve_population() override ;
    virtual Schedule get_best_schedule() override ;
    virtual void reset_learning(float input) override {DR = input;}
    int new_mapping_uniform();
    int new_mapping_alpha(int best_solution);
    int new_mapping_binselect(std::vector<int>& best_mapping);
    int random_mapping();
    float random_float(float min, float max);
    int random_int(int min, int max);
};

REGISTER_EASTRATEGY(ARSH_FATI, "ARSH_FATI");

#endif //METHANE_ARSH_FATI_H

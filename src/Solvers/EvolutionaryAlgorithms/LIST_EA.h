#ifndef METHANE_LIST_EA_H
#define METHANE_LIST_EA_H

#include "EvolutionaryAlgorithm.h"
#include "Utils/Graph.hpp"
#include <boost/random/mersenne_twister.hpp>
#include <boost/random/normal_distribution.hpp>
#include <queue>

using gene_t = uint32_t;
using chromosome_t = std::vector<gene_t>;
using allele_version_map_t = std::map<gene_t, Version*>;



struct SchedComponent {
    SchedElt* element;
    timinginfos_t release_time;
    timinginfos_t worst_case_time;

    SchedComponent(SchedElt* elt, timinginfos_t rt, timinginfos_t wct) : element(elt),
    release_time(rt), worst_case_time(wct) {};
    friend bool operator<(const SchedComponent& lhs, const SchedComponent& rhs) {
        return lhs.worst_case_time < rhs.worst_case_time;
    };
    friend bool operator>(const SchedComponent& lhs, const SchedComponent& rhs) {
        return lhs.worst_case_time > rhs.worst_case_time;
    };
    friend bool operator==(const SchedComponent& lhs, const SchedComponent& rhs) {
        return lhs.worst_case_time == rhs.worst_case_time;
    };

};

class LIST_EA_Population: public Population {

private:
    uint32_t offspring_size;

    std::vector<ComputeUnit*> compute_units;

    // For each gene, store maximum allele value
    std::vector<gene_t> max_version_id;

    // All individuals
    std::vector<chromosome_t> chromosomes;

    // For each gene, map allele to a task version
    std::vector<allele_version_map_t> allele_version_map;

    // Map the SchedElts to task id's (i.e. SchedElt -> gene mapping)
    std::map<SchedElt*, int> schedelt_task_id_map;

    // Store fitness for each individual
    std::vector<float> fitness;

    std::priority_queue<std::pair<float, int>> fitness_queue;
    boost::mt19937 rng;

    float random_float(float low=0.0f, float hig=1.0f);
    int random_int(int low, int high);
    int random_normal_int(float m, float s);

    int tournament(uint32_t k);
    void recombine();
    void mutate(float p_mutate);
    void select();


    void schedule_efls(Schedule& sched, const chromosome_t& chromosome);
    void schedule(Schedule& sched, const chromosome_t& chromosome);
    void schedule_all();


    void evaluate_fitness();
    void evaluate_fitness(uint32_t lower, uint32_t higher);
    void set_fitness(uint32_t individual);
    float get_fitness(uint32_t individual) const;
    void swap_individuals(int i, int j);

public:

    bool gpu_enabled = false;

    LIST_EA_Population() = default;
    ~LIST_EA_Population() = default;
    LIST_EA_Population(const LIST_EA_Population&) = delete;

    void set_population_size(uint32_t population_size, uint32_t offspring_size);
    void set_num_tasks(uint32_t n);
    void init_data();
    void init_schedules(Schedule *copy);
    void init_data(const SystemModel& systemmodel);


    void evolve(float p_mutate);

    std::string chromosome_to_string(int index=0);


protected:

};

class LIST_EA: public EAStrategy {

public:
    explicit LIST_EA(const SystemModel &m, const config_t &c);

    virtual void init_population(Schedule* copy) override;
    virtual void evolve_population() override;
    virtual Schedule get_best_schedule() override;
    virtual void reset_learning(float input) override;

protected:
    LIST_EA_Population population;

private:
    uint32_t population_size = 500;
    uint32_t offspring_size = 1.5 * population_size;
    float p_mutate = 0.1;
};

REGISTER_EASTRATEGY(LIST_EA, "LIST_EA");

#endif //METHANE_LIST_EA_H
/*
 * Copyright (C) 2020 Julius Roeder <j.roeder@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "Utils/Log.hpp"
#include "EvolutionaryAlgorithm.h"
#include "ScheduleFactory.hpp"

using namespace std;

Population::Population() {}

Population::~Population() {
    delete best_individual;
    delete worst_individual;
}

void Population::set_population_size(int pop_size) {
    population_size = pop_size;
}

void Population::set_best_individual() {
    energycons_t lowest_energy = boost::math::tools::max_value<energycons_t>();

    if (best_individual != NULL){
        best_individual->compute_energy();
        lowest_energy = best_individual->energy();
    }

    for (int i=0; i < schedules.size(); i++){
        schedules[i].compute_makespan();
        schedules[i].compute_energy();
        if (schedules[i].energy() < lowest_energy){
            if (best_individual == NULL){
                best_individual = new Schedule(schedules[i]);
            }else{
                *best_individual = schedules[i];
            }
            lowest_energy = schedules[i].energy();
        };
    }
    if (best_individual->energy() <= 0) Utils::WARN("No best individual yet");
}

void Population::set_worst_individual() {
    energycons_t highest_energy = 0;

    if (worst_individual!=NULL){
        worst_individual->compute_energy();
        highest_energy = worst_individual->energy();
    }

    for (size_t i=0; i < schedules.size(); i++){
        schedules[i].compute_makespan();
        schedules[i].compute_energy();
        if (schedules[i].energy() >= highest_energy){
            if (worst_individual == NULL){
                worst_individual = new Schedule(schedules[i]);
            }else {
                *worst_individual = schedules[i];
            }
            highest_energy = schedules[i].energy();
        };
    }
    if (worst_individual->energy() <= 0) Utils::WARN("No worst individual yet");
}

string EAScheduling::help(){
    std::string types = "";
    for(EAStrategyRegistry::key_t t : EAStrategyRegistry::keys())
        types += ((std::string)t)+", ";
    types = types.substr(0, types.size()-2);

    return "Evolutionary Algorithm Scheduler/Solver:\n"
           "type ["+types+"]: type of scheduling strategy\n\n"+
           Solver::help();
}

void EAScheduling::presolve(Schedule *sched) {
    if(type().empty())
        throw CompilerException("EA", "You need to precise what type of solver you want, (check doc?)");

    for (ComputeUnit *p : tg->processors())
        ScheduleFactory::add_core(sched, p);

    strategy = EAStrategyRegistry::Create(type(), *tg, *conf);
}

void EAScheduling::solve(Schedule *sched) {
    ScheduleFactory::build_elements(sched);

    string error = "";

    Schedule schedEAStrategy(*sched);
    do_run(&schedEAStrategy);
    schedEAStrategy.compute_makespan();
    schedEAStrategy.compute_energy();

    *sched = schedEAStrategy;

    if(sched->status() == ScheduleProperties::sched_statue_e::SCHED_UNSCHEDULABLE)
        throw Unschedulable(error);
    if(!error.empty())
        Utils::WARN(error);

    sched->compute_makespan();
    sched->compute_energy();
    Utils::INFO(""+*sched);
    Utils::INFO("\t====> Final Schedule length = "+to_string(sched->makespan()) + tg->time_unit());
    Utils::INFO("\t====> Final Energy Consumption = " + to_string(sched->energy()) + tg->energy_unit());
}

void EAScheduling::postsolve(Schedule *sched) {
    delete strategy;
}

void EAScheduling::do_run(Schedule *sched) {
    chrono::duration<double> running_time = chrono::high_resolution_clock::now() - start_time; //in seconds
    if(timeout() <= (timinginfos_t)running_time.count()) {
        sched->status(ScheduleProperties::sched_statue_e::SCHED_PARTIAL); // Time-out already reached
        return;
    }

    // stopping conditions: no more improvements, too many generations or time out
    energycons_t last_energy = boost::math::tools::max_value<energycons_t>();

    float improvement = MAXFLOAT;
    float minimum_improvement = 0.01; //1%
    int generation_count = 0;
    int MAX_GENERATION = 500;
    int improvement_counter = 0;
    int improvement_counter_total = 0;


    // make copy of schedule
    Schedule copy(*sched);
    // init population for this EA strategy
    strategy->init_population(&copy);

    while (generation_count < MAX_GENERATION && improvement_counter < 40){

        //evolve population according to the current strategy
        strategy->evolve_population();

        // copy best schedule over to sched
        copy = strategy->get_best_schedule();
        copy.compute_makespan();
        copy.compute_energy();

        energycons_t  current_energy = copy.energy();
        if (last_energy > current_energy){
            improvement = (float)(last_energy - current_energy);
            last_energy = current_energy;
            improvement_counter = 0;
        }else{
            improvement = 0;
        }

        if(improvement == 0){
            improvement_counter++;
//            improvement_counter_total++;
        }

//        if (improvement_counter == 10){
//            strategy->reset_learning(0.3);
//        }

        // Check memory usage
        if((Utils::get_ram_usage()/Utils::get_total_ram())*100 > 90 || Utils::get_swap_usage() > 0)
            throw CompilerException("scheduler", "Host out-of-memory -- aborting");
        // Check running time
        chrono::duration<double, std::nano> running_time = chrono::high_resolution_clock::now() - start_time;
        if(timeout() <=  (timinginfos_t)running_time.count()) {
            sched->status(ScheduleProperties::sched_statue_e::SCHED_PARTIAL);
            Utils::WARN("Time Out!");
            break;
        }
        generation_count++;


    }
    *sched = copy;

}


/*!
 * \file HER.cpp
 * \copyright GNU Public License v3.
 * \date 2021
 * \author Julius Roeder <j.roeder@uva.nl>, University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include "HER.hpp"
#include "Utils/Graph.hpp"
#include "ScheduleFactory.hpp"
#include <boost/range/adaptor/reversed.hpp>
#include <boost/accumulators/accumulators.hpp>
#include <boost/accumulators/statistics.hpp>

using namespace std;

IMPLEMENT_HELP_WITH_CODE(HER, {
    std::string types = "";
    for(HERStrategyRegistry::key_t t : HERStrategyRegistry::keys())
        types += ((std::string)t)+", ";
    types = types.substr(0, types.size()-2);

    msg = string("Forward List Scheduler using Heterogeneous Energy Ranking/Solver:\n") +
          "type ["+types+"]: type of scheduling strategy\n\n"+
          Solver::help();
}, HER Scheduler/Solver: \n
   HTMLINDENT- type [@see Solver]: type of scheduling strategy\n)

void HER::presolve(Schedule *sched) {
    if(type().empty())
        throw CompilerException("HER", "You need to precise what type of solver you want, (check doc?)");

    for (ComputeUnit *p : tg->processors())
        ScheduleFactory::add_core(sched, p);
    ScheduleFactory::build_elements(sched);

    strategy = HERStrategyRegistry::Create(type(), *tg, *conf);
}

void HER::build_rank(const SystemModel *tg, const config_t *conf, Schedule * schedule, vector<SchedElt*> *schedorder){
    map<SchedElt*, energycons_t> elt_rank;
    vector<pair<SchedElt*, energycons_t> > sorted_elt_rank;

    std::vector<std::vector<Task*>> DAGS;
    Utils::extractGraphs(tg, &DAGS);

    for (auto & dag: DAGS) {

        for (Task * task: boost::adaptors::reverse(dag)) {
            //TODO: might need fixing for multiple jobs
            for (auto elt: schedule->schedjobs(task)) {

                energycons_t rank = 0;
                energycons_t version_average_C = 0;
                energycons_t max_succ_rank = 0;

                // needed for variance
                boost::accumulators::accumulator_set<energycons_t, boost::accumulators::stats<boost::accumulators::tag::variance> > acc;


                for (Version *vers: elt->task()->versions()) {
                    energycons_t version_C_E = 0;

                    // static base energy
                    version_C_E += vers->C() * tg->base_watt() / 10; //devided by 10 for deca

                    // dynamic energy of task
                    version_C_E += vers->C_E();

//                     calculates energy of CPU - needed for full energy
//                                for (auto island: tg->voltage_islands()){
                    //                if (vers.second->force_mapping_proc()[0]->voltage_island_id == island->id){
                    //                    for (pair<uint128_t, uint128_t> freq_watt: island->frequency_watt){
                    //                        if (freq_watt.first == vers.second->frequency()){
                    //                            version_C_E += (freq_watt.second * vers.second->C() /10);
                    //                        }
                    //                    }
                    //                }
                    //            }
                    //
                    //            // calculates energy of GPU
                    //            if (vers.second->gpu_required()){
                    //                for (auto gpu: tg->coprocessors()){
                    //                    for (pair<uint128_t, uint128_t> freq_watt: gpu->frequency_watt){
                    //                        if (freq_watt.first == vers.second->gpu_frequency()){
                    //                            version_C_E += (freq_watt.second * vers.second->C() /10);
                    //                        }
                    //                    }
                    //                }
                    //            }

                    acc(version_C_E);

                    // for average or sum
//                    version_average_C += version_C_E;

                    // for Min
                    if (version_C_E < version_average_C || version_average_C == 0)
                        version_average_C = version_C_E;
                }

                //uncomment to get variance
//        energycons_t variance = boost::multiprecision::sqrt(boost::accumulators::variance(acc));
                energycons_t variance = boost::accumulators::variance(acc);
//                version_average_C += variance; // highest task with highest variance scheduled FIRST - maybe try std

                // uncomment for average
//        version_average_C /= elt->task()->versions().size();


                for (SchedElt *succ: elt->successors()) {
                    // maximum rank of successor
                    if (elt_rank.at(succ) > max_succ_rank)
                        max_succ_rank = elt_rank.at(succ);

                    // sum of succ
//            max_succ_rank += elt_rank.at(succ);
                }

                rank = version_average_C + max_succ_rank;
                elt_rank[elt] = rank;
            }
        }
    }

    sorted_elt_rank.reserve(elt_rank.size());
    for (pair<SchedElt*, energycons_t> p: elt_rank){
        sorted_elt_rank.push_back(p);
    }

    sort(sorted_elt_rank.begin(), sorted_elt_rank.end(),[](pair<SchedElt*, energycons_t> &left, pair<SchedElt*, energycons_t> &right){return left.second > right.second;} );

    for (pair<SchedElt*, energycons_t> p: sorted_elt_rank){
        schedorder->push_back(p.first);
    }

}

void HER::solve(Schedule *sched) {
    string error = "";

    Schedule HER_sched(*sched);
    try {
        vector<SchedElt*> schedorder;
        HER::build_rank(tg, conf, sched, &schedorder);
        do_run(&HER_sched, schedorder);
        HER_sched.compute_makespan();
        HER_sched.compute_energy();
    }
    catch(Unschedulable &e){
        error += "HER with delay: "+e.str()+"\n";
    }

    Utils::INFO("\t====> Schedule length HER (" + ScheduleProperties::to_string(HER_sched.status()) + ") = " +
                to_string(HER_sched.makespan()));

    sched->makespan((unsigned)-1);
    sched->energy((unsigned)-1);

    //if FLS scheduler is energy, take the best schedule with respect to energy.
    Utils::INFO(HER::type());

    if(HER_sched.status() == ScheduleProperties::sched_statue_e::SCHED_COMPLETE)
        *sched = HER_sched;

    clear_memento();

    if(sched->status() == ScheduleProperties::sched_statue_e::SCHED_UNSCHEDULABLE)
        throw Unschedulable(error);
    if(!error.empty())
        Utils::WARN(error);

    sched->stats("HER energy", to_string(HER_sched.status() == ScheduleProperties::sched_statue_e::SCHED_COMPLETE ? HER_sched.energy() : -1));
    sched->stats("HER makespan", to_string(HER_sched.status() == ScheduleProperties::sched_statue_e::SCHED_COMPLETE ? HER_sched.makespan() : -1));

    sched->compute_makespan();
    sched->compute_energy();

    Utils::INFO(""+*sched);
    Utils::INFO("\t====> Final Schedule length = "+to_string(sched->makespan()));
    Utils::INFO("\t====> Final Energy Consumption = " + to_string(sched->energy()));

}

void HER::do_run(Schedule *sched, vector<SchedElt*> &schedorder) {
    chrono::duration<double> running_time = chrono::high_resolution_clock::now() - start_time;
    if(timeout() <=  (timinginfos_t)running_time.count()) {
        sched->status(ScheduleProperties::sched_statue_e::SCHED_PARTIAL); // Time-out already reached
        return;
    }
    uint64_t hyperperiod = tg->hyperperiod();

    clear_memento();
    Utils::DEBUG("/***************** Start Mapping ********************/");
    while(!schedorder.empty()) {
        // get next element
        SchedElt *t = *(schedorder.begin());

        // store recovering point
        memento[t].schedule = new Schedule(*sched);
        memento[t].schedorder = schedorder;
        memento[t].forbid_proc = strategy->forbidden_proc_mapping;

        // consume the element to schedule
        schedorder.erase(schedorder.begin());

        Utils::memory_usage("-> Sched "+t->toString());
        Schedule copy(*sched);
        SchedElt *copy_elt = copy.schedjob(t->id());

        try {
            // do the mapping/scheduling
            copy.schedule(copy_elt);
            copy_elt->accept(strategy, &copy);

            copy_elt = copy.schedjob(t->id());

            copy.compute_makespan();
//          hyperperiod set to max_int if tasks dont have periodicity
//            if(copy.makespan() > hyperperiod)
//                throw Unschedulable("Schedule length ("+to_string(copy.makespan())+") is above hyperperiod "+to_string(hyperperiod));
//          ignore error if min_rt_deadline is 0 TODO: Should min_rt_deadline be fixed elsewhere?
            if(copy_elt->min_rt_deadline() > 0 && copy_elt->rt()+copy_elt->wct() > copy_elt->min_rt_deadline())
                throw Unschedulable("Element "+copy_elt->toString()+" missed its deadline -- "+to_string(copy_elt->rt()+copy_elt->wct())+" > "+to_string(copy_elt->min_rt_deadline()));

            if(!allow_migration) {
                SchedJob *concerned_task = copy_elt->schedtask();
                SchedCore *mapping = copy.get_mapping(concerned_task);
                if(mapping != nullptr) {
                    for(SchedCore *c : sched->schedcores()) { //warning, this is core from sched that we want to put in the forbidden list, not from copy
                        if(c->core() != mapping->core()) {
                            Utils::DEBUG("No migration, add "+*c+" to forbid list for "+concerned_task->toString());
                            strategy->forbidden_proc_mapping[concerned_task->task()].insert(c);
                        }
                    }
                }
            }

            *sched = copy;

            // Check memory usage
//            if((Utils::get_ram_usage()/Utils::get_total_ram())*100 > 90 || Utils::get_swap_usage() > 0)
//                throw CompilerException("scheduler", "Host out-of-memory -- abording");
            // Check running time
            chrono::duration<double> running_time = chrono::high_resolution_clock::now() - start_time;
            if(timeout() > 0 && timeout() <=  (timinginfos_t)running_time.count()) {
                sched->status(ScheduleProperties::sched_statue_e::SCHED_PARTIAL);
                Utils::WARN("Time Out!");
                break;
            }
            Utils::DEBUG(""+*sched);
        }
        catch(Unschedulable &e) {
            Utils::DEBUG(e.what());
            // as for now, this exception is raised only when there is no more space on SPM
            // mapping to SPM happens only when the whole task is scheduled (packets+exec)

            // Grab the task we were scheduling (well, the exec phase as it is the one that is mapped on a core)
            SchedJob *concerned_task = t->schedtask();

            // Return to a stable known state
            *sched = *(memento[concerned_task].schedule);
            delete memento[concerned_task].schedule;
            memento[concerned_task].schedule = nullptr;
            schedorder = memento[concerned_task].schedorder;
            strategy->forbidden_proc_mapping = memento[concerned_task].forbid_proc;

            // Add the last tested core to the forbidden list to not test it again
            SchedCore *concerned_core = sched->schedcore(copy.get_mapping(copy_elt->schedtask())->core());
            strategy->forbidden_proc_mapping[concerned_task->task()].insert(concerned_core);
            Utils::DEBUG("=====================> add "+*concerned_core+" to forbid list for "+concerned_task->toString());

            // Check if we tried to map the task on every core, if so raise exception
            if(strategy->forbidden_proc_mapping[concerned_task->task()].size() == sched->schedcores().size()) {
                sched->status(ScheduleProperties::sched_statue_e::SCHED_UNSCHEDULABLE);
                // we tried every processor, and on all of them we end up in an unschedulable state, so be it
                throw Unschedulable("Can't find a valid schedule on any core for elt: "+t->toString());
            }
        }
    }

    Utils::memory_usage("***************** Done *******************");

    if(sched->status() == ScheduleProperties::sched_statue_e::SCHED_UNSCHEDULABLE)
        return;

    sched->compute_makespan();
    sched->compute_energy();
}

void HER::clear_memento() {
    for(pair<SchedElt*, memento_sched_state> el : memento) {
        if(el.second.schedule != nullptr)
            delete el.second.schedule;
    }
    memento.clear();
    strategy->forbidden_proc_mapping.clear();
}


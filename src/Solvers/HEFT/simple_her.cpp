/*!
 * \file simple_her.cpp
 * \copyright GNU Public License v3.
 * \date 2017
 * \author Julius Roeder <j.roeder@uva.nl>, University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include "simple_her.hpp"


simple_HER::simple_HER(const SystemModel &m, const config_t &c) : HERStrategy(m, c) {}


uint64_t simple_HER::find_best_start_read(const Schedule &sched, SchedPacket *t) {
    uint64_t bestStart = t->min_rt_period();
    for(SchedElt * e : t->previous()) {
        if(e->rt() + e->wct() > bestStart)
            bestStart = e->rt() + e->wct();
    }
    return bestStart;
}

uint64_t simple_HER::find_best_start_exec(const Schedule &sched, SchedJob *t) {
    bool change = true;

    uint64_t bestStart = t->get_end_last_read();
    if (bestStart < t->min_rt_period()) {
        bestStart = t->min_rt_period();
    }
    // get predecessor end
    for (SchedElt *e : t->previous()) {
        if (e->rt() + e->wct() > bestStart)
            bestStart = e->rt() + e->wct();
    }

    // end time of this version
    uint64_t endtime = bestStart + t->wct();

    // while any changes were made to the start time
    while (change) {
        change = false;
        for (SchedElt *elt : sched.scheduled_elements()) {
            if (elt == t) continue;
            uint64_t eRtE = elt->rt();
            uint64_t eEndTime = eRtE + elt->wct();

            if (t->gpu_required()) {
                if (eRtE < endtime && bestStart < eEndTime) {
                    if (elt->schedtask()->gpu_required()) {
                        bestStart = eEndTime;
                        endtime = bestStart + t->wct();
                        change = true;
                    } else {
                        if (sched.get_mapping(elt) == sched.get_mapping(t)) {
                            bestStart = eEndTime;
                            endtime = bestStart + t->wct();
                            change = true;
                        }
                    }
                }
            } else {
                if (sched.get_mapping(elt) != sched.get_mapping(t)) continue;
                if (eRtE < endtime && bestStart < eEndTime) {
                    bestStart = eEndTime;
                    endtime = bestStart + t->wct();
                    change = true;
                }
            }
        }

        //checks if the current task is on the same cluster and checks if the frequency is the same. otherwise move it forwards.
        for (SchedElt *elt : sched.scheduled_elements()) {
            if (elt == t) continue;
            if (sched.get_mapping(elt)->core()->voltage_island_id != sched.get_mapping(t)->core()->voltage_island_id) continue;

            uint64_t eRtE = elt->rt();
            uint64_t eEndTime = eRtE + elt->wct();
            if (eRtE < endtime && bestStart < eEndTime) {
                if (elt->schedtask()->frequency() != t->frequency()) {
                    bestStart = eEndTime;
                    endtime = bestStart + t->wct();
                    change = true;
                }
            }
        }
    }

    return bestStart;
}

void simple_HER::visitSchedEltPacketRead(Schedule *sched, SchedPacket *p) {
    p->rt(0);
    p->wct(0);
}
void simple_HER::visitSchedEltPacketWrite(Schedule *sched, SchedPacket *pac) {
    pac->rt(0);
    pac->rt(pac->schedtask()->rt() + pac->schedtask()->wct());
}

void simple_HER::visitSchedEltTask(Schedule *sched, SchedJob *t) {
// TODO: For now this assumes that any multi-phase program uses the GPU and ONE CPU core!
    energycons_t bestEnergy = -1;
    Schedule bestSchedule(*sched);
    bestSchedule.status(ScheduleProperties::sched_statue_e::SCHED_PARTIAL);

    for(SchedCore *candidate : sched->schedcores()) {
        if(find_if(forbidden_proc_mapping[t->task()].begin(), forbidden_proc_mapping[t->task()].end(), [candidate](SchedCore *a) {return a->core() == candidate->core();}) != forbidden_proc_mapping[t->task()].end()) {
            Utils::DEBUG("\tcandidate "+*candidate+" unusable due to previous unschedulability issue");
            continue;
        }
        // loop overall versions of a task
        for(Version * t_version: t->task()->versions()){
            Version *v = t_version;

            // if version proc and current proc dont match -> continue
            if(!v->force_mapping_proc().empty() && find(v->force_mapping_proc().begin(), v->force_mapping_proc().end(), candidate->core()) == v->force_mapping_proc().end())
                continue;

            t->wct(t_version->C());
            t->wce(t_version->C_E());
            t->selected_version(t_version->id());
            t->frequency(t_version->frequency());
            t->gpu_frequency(t_version->gpu_frequency());
            t->gpu_required(t_version->gpu_required());

            Schedule schedule(*sched);
            SchedJob *ct = schedule.schedjob(t->id()); 
            ct->selected_version(t_version->id());
            schedule.map_core(ct, schedule.schedcore(candidate->core()));

            for(SchedPacket *p : ct->packet_read()) {
                p->rt(find_best_start_read(schedule, p));
            }

            ct->rt(find_best_start_exec(schedule, ct));

            Utils::DEBUG("\t\t synchronized: "+*ct);

            schedule.compute_makespan();
            schedule.compute_energy();

            if(bestEnergy > schedule.energy()) {
                Utils::DEBUG("\t\tbest candidate makespan -- "+to_string(schedule.energy())+" ("+to_string(bestEnergy)+")");
                bestSchedule = schedule;
                bestSchedule.status(ScheduleProperties::sched_statue_e::SCHED_COMPLETE);
                bestEnergy = schedule.energy();
            }
            else
                Utils::DEBUG("\t\t\t\tbad candidate makespan -- "+to_string(schedule.energy())+" ("+to_string(bestEnergy)+")");
        }
    }

    *sched = bestSchedule;
}

/*!
 * \file simple_heft.cpp
 * \copyright GNU Public License v3.
 * \date 2017
 * \author Julius Roeder <j.roeder@uva.nl>, University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include "simple_heft.hpp"
#include "ScheduleFactory.hpp"


simple_HEFT::simple_HEFT(const SystemModel &m, const config_t &c) : HEFTStrategy(m, c) {}


uint64_t simple_HEFT::find_best_start_read(const Schedule &sched, SchedPacket *t) {
    uint64_t bestStart = t->min_rt_period();
    for(SchedElt * e : t->previous()) {
        if(e->rt() + e->wct() > bestStart)
            bestStart = e->rt() + e->wct();
    }
    return bestStart;
}

uint64_t simple_HEFT::find_best_start_exec(const Schedule &sched, SchedJob *t, SchedCore* c) {
    bool change = true;

    uint64_t bestStart = t->get_end_last_read();
    if (bestStart < t->min_rt_period()) {
        bestStart = t->min_rt_period();
    }
    // get predecessor end
    // TODO check if multi phase grabs the right predecessor end
    for (SchedElt *e : t->previous()) {
        if (e->rt() + e->wct() > bestStart)
            bestStart = e->rt() + e->wct();
    }

    // end time of this version
    uint64_t endtime = bestStart + t->wct();

    // while any changes were made to the start time
    while (change) {
        change = false;
        for (SchedElt *elt : sched.scheduled_elements()) {
            if (elt == t) continue;
            uint64_t eRtE = elt->rt();
            uint64_t eEndTime = eRtE + elt->wct();

            if (t->gpu_required()) {
                if (eRtE < endtime && bestStart < eEndTime) {
                    if (elt->schedtask()->gpu_required()) {
                        bestStart = eEndTime;
                        endtime = bestStart + t->wct();
                        change = true;
                    } else {
                        if (sched.get_mapping(elt) == sched.get_mapping(t)) {
                            bestStart = eEndTime;
                            endtime = bestStart + t->wct();
                            change = true;
                        }
                    }
                }
            } else {
                if (sched.get_mapping(elt) != sched.get_mapping(t)) continue;
                if (eRtE < endtime && bestStart < eEndTime) {
                    bestStart = eEndTime;
                    endtime = bestStart + t->wct();
                    change = true;
                }
            }
        }

        //checks if the current task is on the same cluster and checks if the frequency is the same. otherwise move it forwards.
        for (SchedElt *elt : sched.scheduled_elements()) {
            if (elt == t) continue;
            if (sched.get_mapping(elt)->core()->voltage_island_id != sched.get_mapping(t)->core()->voltage_island_id) continue;

            uint64_t eRtE = elt->rt();
            uint64_t eEndTime = eRtE + elt->wct();
            if (eRtE < endtime && bestStart < eEndTime) {
                if (elt->schedtask()->frequency() != t->frequency()) {
                    bestStart = eEndTime;
                    endtime = bestStart + t->wct();
                    change = true;
                }
            }
        }
        for (SchedElt *elt : sched.scheduled_elements()) {
            if (elt == t) continue;
            for (Phase *p: t->task()->versions(t->selected_version())->phases()){
                // if the type is the same as the current candidate we continue
                if ((*p->force_mapping_proc().begin())->type == c->core()->type) continue;
                // if the candidate core and the previously scheduled element are the same we continue
                if (sched.get_mapping(elt)->core()->id == c->core()->id) continue;
                // if the previously scheduled type and the phase type are the not the same we continue
                if (sched.get_mapping(elt)->core()->type != c->core()->type) continue;


                uint64_t eRtE = elt->rt();
                uint64_t eEndTime = eRtE + elt->wct();
                if (eRtE < endtime && bestStart < eEndTime) {
                    bestStart = eEndTime;
                    endtime = bestStart + t->wct();
                    change = true;
                }
            }
        }
    }

    return bestStart;
}

void simple_HEFT::visitSchedEltPacketRead(Schedule *sched, SchedPacket *p) {
    p->rt(0);
    p->wct(0);
}
void simple_HEFT::visitSchedEltPacketWrite(Schedule *sched, SchedPacket *pac) {
    pac->rt(0);
    pac->rt(pac->schedtask()->rt() + pac->schedtask()->wct());
}

void simple_HEFT::visitSchedEltTask(Schedule *sched, SchedJob *t) {
// TODO: For now this assumes that any multi-phase program uses the GPU and ONE CPU core!
    uint128_t bestScheduleTime = -1;
    Schedule bestSchedule(*sched);
    bestSchedule.status(ScheduleProperties::sched_statue_e::SCHED_PARTIAL);

    for (SchedCore *candidate: sched->schedcores()) {
        if (find_if(forbidden_proc_mapping[t->task()].begin(), forbidden_proc_mapping[t->task()].end(),
                    [candidate](SchedCore *a) { return a->core() == candidate->core(); }) !=
            forbidden_proc_mapping[t->task()].end()) {
            Utils::DEBUG("\tcandidate " + *candidate + " unusable due to previous unschedulability issue");
            continue;
        }
        // loop overall versions of a task
        for (Version *t_version: t->task()->versions()) {
            // if version proc and current proc dont match -> continue
            if(!t_version->force_mapping_proc().empty() && find(t_version->force_mapping_proc().begin(), t_version->force_mapping_proc().end(), candidate->core()) == t_version->force_mapping_proc().end())
                continue;

            t->wct(t_version->C());
            t->wce(t_version->C_E());
            t->selected_version(t_version->id());
            t->frequency(t_version->frequency());
            t->gpu_frequency(t_version->gpu_frequency());
            t->gpu_required(t_version->gpu_required());

            Schedule schedule(*sched);
            SchedJob *ct = schedule.schedjob(t->id());
            ct->selected_version(t_version->id());
            schedule.map_core(ct, schedule.schedcore(candidate->core()));

            for (SchedPacket *p: ct->packet_read()) {
                p->rt(find_best_start_read(schedule, p));
            }

            uint64_t rt = find_best_start_exec(schedule, ct, candidate);
            ct->rt(rt);
//            schedule.map_core(ct, schedule.schedcore(candidate->core()));
//            schedule.schedule(ct);


            for (Phase *p: t_version->phases()) {
                // if the processor required for the new phase does not equal the processor of the first phase we add new jobs and reserve the right cores.
                if ((*p->force_mapping_proc().begin())->type ==
                    (*t_version->force_mapping_proc().begin())->type)
                    continue;
                SchedJob *new_ct = ScheduleFactory::build_job(&schedule, ct->task(), rt);
                new_ct->wct(t_version->C());
                new_ct->selected_version(t_version->id());
                new_ct->min_rt_period(ct->min_rt_period() + 1);
                schedule.map_core(new_ct, schedule.schedcore((*p->force_mapping_proc().begin())));
                schedule.schedule(new_ct);
            }

            Utils::DEBUG("\t\t synchronized: " + *ct);

            schedule.compute_makespan();
            schedule.compute_energy();

            if (bestScheduleTime > (rt + ct->wct())) {
                Utils::DEBUG("\t\tbest candidate makespan -- " + to_string(rt + ct->wct()) + " (" + to_string(bestScheduleTime) +
                             ")");

                // update task wct and wce for current versions
//                SchedJob *bestScheduleJob = bestSchedule.schedjob(t->id()); ;
//                bestScheduleJob->rt(rt);
//                bestScheduleJob->wct(t_version->C());
//                bestScheduleJob->wce(t_version->C_E());
//
//                t->rt(rt);
//                t->wct(t_version->C());
//                t->wce(t_version->C_E());
//                t->selected_version(t_version->id());

                bestSchedule = schedule;
                bestSchedule.status(ScheduleProperties::sched_statue_e::SCHED_COMPLETE);
                bestScheduleTime = ct->rt() + ct->wct();
            } else {
                Utils::DEBUG("\t\t\t\tbad candidate makespan -- " + to_string(schedule.makespan()) + " (" +
                             to_string(bestScheduleTime) + ")");
            }
        }
    }
    *sched = bestSchedule;
}


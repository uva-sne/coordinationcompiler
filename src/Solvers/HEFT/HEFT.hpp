/*!
 * \file HEFT.hpp
 * \copyright GNU Public License v3.
 * \date 2017
 * \author Julius Roeder <j.roeder@uva.nl>, University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef METHANE_HEFT_HPP
#define METHANE_HEFT_HPP

#include <cstdlib>
#include <vector>
#include <string>
#include <map>
#include <algorithm>
#include <array>
#include <cmath>
#include <cassert>
#include <chrono>
#include <tuple>
#include <functional>
//#include <stdint.h>

#include <boost/algorithm/string.hpp>

#include "config.hpp"
#include "Utils.hpp"
#include "SystemModel.hpp"
#include "Schedule.hpp"
#include "Solvers/Solver.hpp"
#include "registry.hpp"
#include "Utils/Graph.hpp"

/**
 * Base Strategy class to perform the schedule
 */
class HEFTStrategy : public SchedEltVisitor {
protected:
    const SystemModel &tg; //!< IR of the current application
    const config_t &conf; //!< the global configuration

public:
    //! Use for the memento pattern to store already tried mapping, \see ForwardListScheduling::memento
    std::map<const Task*, std::set<SchedCore*>> forbidden_proc_mapping;

    /**
     * Constructor
     * @param m \copybrief FLSStrategy::tg
     * @param c \copybrief FLSStrategy::conf
     */
    explicit HEFTStrategy(const SystemModel &m, const config_t &c) : tg(m), conf(c) {};
};

/*!
 * \typedef HEFTStrategyRegistry
 * \brief Registry containing all FLS strategy
 */
using HEFTStrategyRegistry = registry::Registry<HEFTStrategy, std::string, const SystemModel &, const config_t &>;

/*!
 * \brief Helper macro to register a new derived FLS strategy
 */
#define REGISTER_HEFTSTRATEGY(ClassName, Identifier) \
  REGISTER_SUBCLASS(HEFTStrategy, ClassName, std::string, Identifier, const SystemModel &, const config_t &)

class HEFT : public Solver{
    //! Structure to implement a memento pattern, think of a Ctrl+Z
    typedef struct {
        Schedule *schedule;
        std::vector<SchedElt*> schedorder;
        std::map<const Task*, std::set<SchedCore*>> forbid_proc;
    } memento_sched_state;
    std::map<SchedElt*, memento_sched_state> memento; //!< hold memorised state

    HEFTStrategy *strategy; //!< the FLS strategy to apply

    //! Reset the memento state
    void clear_memento();

public:
    //! Constructor
    explicit HEFT() : Solver() {};

    //! \copydoc CompilerPass::get_uniqid_rtti
    const std::string get_uniqid_rtti() const override { return "solver-heft"; }
    //! \copydoc CompilerPass::help
    virtual std::string help() override;

    //! \copydoc Solver::solve
    virtual void solve(Schedule *result) override;

    /**
     * We try to schedule the application with different input task ordering,
     * this function will schedule the app for a given order
     *
     * @param sched resulting schedule
     * @param schedorder selected Task order
     */
    virtual void do_run(Schedule *sched, std::vector<SchedElt*> &schedorder);

    /*! \copydoc Solver::presolve
     *
     * It only instanciates the strategy here.
     */
    void presolve(Schedule *sched) override;

    /**
     * Builds the heft rank with 0 communication cost.
     *
     * @param
     * @param
     * @param
     * @param
     */
    void build_rank(const SystemModel *tg, const config_t *conf, Schedule * schedule, std::vector<SchedElt*> *schedorder);
};

REGISTER_SOLVER(HEFT, "HEFT")

#endif //METHANE_HEFT_HPP

/*!
 * \file Solver.cpp
 * \copyright GNU Public License v3.
 * \date 2017
 * \author Benjamin Rouxel <benjamin.rouxel@inria.fr> Irisa/INRIA
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "Solver.hpp"

using namespace std;

IMPLEMENT_HELP(Solver, 
    General Solver parameters:\n
    * solve [CONFIG_FALSE-CONFIG_TRUE], default CONFIG_TRUE: Solver should 
    be called or not\n
    * timeout [int], default 0: solving time timeout, 0 to disable the timeout\n
    * allow_migration (experimental) [CONFIG_FALSE-CONFIG_TRUE], default 
        CONFIG_TRUE: task/job ? migration is allowed or not\n
    * type: the type of scheduling policy to use
)
        
void Solver::run(SystemModel *m, config_t *c, Schedule *result) {
    tg = m;
    conf = c;
    if(!_solve)
        return;

    start_time = std::chrono::high_resolution_clock::now();
    try {
        this->presolve(result);
        this->solve(result);
        this->postsolve(result);
        std::chrono::high_resolution_clock::time_point end = std::chrono::high_resolution_clock::now();
        std::chrono::duration<double> diff = end-start_time;
//        Utils::INFO("==> Schedule time: " + to_string(diff.count()) + " seconds");
        result->solvetime(diff.count());
    }
    catch(Unschedulable &e) {
        std::chrono::high_resolution_clock::time_point end = std::chrono::high_resolution_clock::now();
        std::chrono::duration<double> diff = end-start_time;
//        Utils::INFO("==> Schedule time: " + to_string(diff.count()) + " seconds");
        result->solvetime(diff.count());
        throw e;
    }
}
        
void Solver::forward_params(const std::map<std::string, std::string> &args) {
    if(args.count("solve"))
        _solve = (args.at("solve") == "true");
    if(args.count("timeout"))
        timeout(Utils::stringtime_to_ns(args.at("timeout")));
    if(args.count("type"))
        type(args.at("type"));
    if(args.count("migration"))
        allow_migration = args.at("migration") == CONFIG_TRUE;
    if(args.count("target"))
        _target = SolverProperties::from_string(args.at("target"));
}
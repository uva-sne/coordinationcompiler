/*
 * Copyright (C) 2022 Lukas Miedema <l.miedema@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <SystemModel.hpp>
#include <Schedule.hpp>
#include "FFLS.hpp"
#include "FLSSorting.hpp"
#include "ScheduleFactory.hpp"
#include <Solvers/ForwardListScheduling/FLSFragmentedQreadyBuilder.hpp>

using E = LabeledCompilerException<C_STR("ffls")>;

IMPLEMENT_HELP(FastForwardListScheduling,
        Fast Forward List Scheduler/Solver: \n
        Optimizes for makespan, and supports heterogeneous architectures + multi-version scheduling\n
        HTMLINDENT- type [@see Solver]: type of scheduling strategy\n)

void FastForwardListScheduling::solve(Schedule *result) {
    if (result->status() == ScheduleProperties::sched_statue_e::SCHED_COMPLETE)
        result->clear();

    for(ComputeUnit *p: this->tg->processors())
        ScheduleFactory::add_core(result, p);

    // Check assumptions
    for (Task *task : this->tg->tasks_it()) {
        for (Connector *inport : task->inputs())
            if (inport->tokens() != 1)
                throw E("cannot schedule SDF graph directly");
        for (Connector *outport : task->outputs())
            if (outport->tokens() != 1)
                throw E("cannot schedule SDF graph directly");
    }

    ScheduleFactory::build_elements(result);
    std::vector<SchedElt *> elements = result->elements_unowned();

    // Sort the elements
    std::vector<SchedElt*> Qready = FLSSorting::sort_llf(elements);
    assert(Qready.size() == result->elements_lut().size());

    timinginfos_t rolling_makespan = 0; // do not recompute makespan at every step

    // Start mapping
    /* This version of FLS works as follows
     * - The list of unscheduled elements is always scheduled as they appear, elements are never skipped and processed later
     * - Elements may be delayed (pushed forward in time) to ensure their predecessors have finished
     * - Already scheduled elements are never moved (greedy)
     * - The best core is selected as follows
     *  1. Find the cores with the earliest release time for the task
     *  2. From those cores, find the cores with the smallest amount of delay between the return of the last task and the release time
     *  3. Finally, if there are still multiple cores, prefer cores earlier in the list
     */
    std::map<SchedCore *, std::vector<SchedElt *>> schedule; // each vector is kept sorted by RT
    for (SchedElt* toSchedule : Qready) {

        FlsPlacementCandidate best = {
                .core = nullptr,
                .version = nullptr,
                .responseTime = std::numeric_limits<timinginfos_t>::max(),
                .delay = 0,
                .scheduleIndex = 0
        };

        timinginfos_t notBefore = 0;
        for (SchedElt* predecessor : toSchedule->previous()) {
            notBefore = std::max(predecessor->rt() + predecessor->wct(), notBefore);
        }

        std::vector<Version *> versions = toSchedule->task()->versions(); // this returns a _copy_ of all versions -_-
        // might as well make use of that copy -> sort the versions by WCET ascending
        // then we can stop the search as soon as we find any match
        std::sort(versions.begin(), versions.end(), [](Version* lhs, Version* rhs) {
            return lhs->C() < rhs->C();
        });

        for (SchedCore* core : result->schedcores()) {
            for (Version *version : versions) {
                // Check compatibility
                if (version->canRunOn(core->core()) && !version->gpu_required()) {
                    FlsPlacementCandidate candidate = evaluatePlacement(&schedule, core, toSchedule, version, notBefore);
                    if (candidate.responseTime < best.responseTime ||
                            (candidate.responseTime == best.responseTime && candidate.delay < best.delay)) {
                        best = candidate;
                    }

                    // No use in continuing to consider versions for this task as they're all worse
                    // As such, we either consider 0 versions for this core (all are incompatible), or just 1
                    break;
                }
            }
        }

        // Best core has been found -> now do actual scheduling
        toSchedule->rt(best.responseTime - best.version->C());
        toSchedule->wct(best.version->C());
        toSchedule->schedtask()->selected_version(best.version->id());
        toSchedule->wce(best.version->C_E());
        toSchedule->schedtask()->frequency(best.version->frequency());

        result->map_core(toSchedule, best.core);
        result->schedule(toSchedule);

        // Update scheduling data structures
        rolling_makespan = std::max(rolling_makespan, best.responseTime);
        schedule[best.core].insert(schedule[best.core].begin() + best.scheduleIndex, toSchedule); // TODO: replace this with a treemap for O(log n) insertion?
    }

    // This scheduler will never build a partial schedule
    // Even if makespan > deadline, the "COMPLETE" status is expected, otherwise e.g. the SVGViewer will not work
    result->status(ScheduleProperties::sched_statue_e::SCHED_COMPLETE);
    result->mapping_mode(ScheduleProperties::Mapping::MAP_PARTITIONED);
    result->priority_mode(ScheduleProperties::PriorityScheme_e::PRIO_FIXED);
    result->preemption_mode(ScheduleProperties::PreemptionModel::PRE_NON);
    result->makespan(rolling_makespan);
    result->compute_energy();
}

FlsPlacementCandidate FastForwardListScheduling::evaluatePlacement(
        std::map<SchedCore *, std::vector<SchedElt *>> *schedule,
        SchedCore *core, SchedElt *toSchedule, Version *version, timinginfos_t notBefore) {

    // TODO: add a "notAfter" parameter communicating the current best time, and don't search after as an some extra optimization
    // TODO -- Dealing with multi-phase tasks --

    // Find the first element that finishes after notBefore (RT+WCET >= notBefore)
    std::vector<SchedElt *>& coreSchedule = schedule->operator[](core);
    long taskAfter; // may be == coreSchedule.size() -> there is no such task
    for (taskAfter = 0; taskAfter < (long) coreSchedule.size(); taskAfter++) {
        if (coreSchedule[taskAfter]->rt() + coreSchedule[taskAfter]->wct() >= notBefore)
            break;
    }

    // Try to find a gap
    for (; taskAfter < (long) coreSchedule.size(); taskAfter++) {
        // We know that this task (ta) finishes _after_ notBefore (nb)
        // 1. As such, the situation may be this (- = idle)
        // ---- [nb] --------------- [ ta.start .... ta.end ]
        //        ^ this is our gap ^
        // 2. Or, alternatively, a task could be in that gap
        //
        // [ta-1.start ..... [nb] .. ta-1.end] ------------ [ ta.start ... ta.end ]
        //                                   ^  our gap     ^
        // 3. Final case, ta.start itself is _before_ notBefore
        //
        // [ta.start .... [nb] ....... ta.end ]
        //       ~ there is no gap here ~

        // As such, compute the start of the gap by taking the max, dealing with case 1 and 2
        timinginfos_t previousEnd = taskAfter == 0 ? 0 : (coreSchedule[taskAfter-1]->rt() + coreSchedule[taskAfter-1]->wct());
        timinginfos_t gapStart = std::max(notBefore, previousEnd);
        timinginfos_t gapEnd = coreSchedule[taskAfter]->rt();
        if (gapEnd <= gapStart) {
            // There is no gap (case 3)
            continue;
        }
        timinginfos_t gapSize = gapEnd - gapStart;
        if (gapSize < version->C()) {
            // Doesn't fit
            continue;
        }
        // Got it
        timinginfos_t delay = previousEnd - gapStart;
        return {
            .core = core,
            .version = version,
            .responseTime = gapStart + version->C(),
            .delay = delay,
            .scheduleIndex = taskAfter
        };
    }

    // No gap found -> place element at the open end of the schedule
    timinginfos_t rt;
    timinginfos_t delay;
    if (taskAfter == 0) { // no tasks in the schedule for this core
        rt = notBefore;
        delay = notBefore;
    } else {
        SchedElt* lastTask = coreSchedule[taskAfter - 1];
        rt = std::max(notBefore, (lastTask->rt() + lastTask->wct()));
        delay = notBefore - (lastTask->rt() + lastTask->wct());
    }

    return {
        .core = core,
        .version = version,
        .responseTime = rt + version->C(),
        .delay = delay,
        .scheduleIndex = taskAfter
    };
}

/*
 * Copyright (C) 2022 Lukas Miedema <l.miedema@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "FLSSorting.hpp"

bool operator> (const llf_queue_t &lhs, const llf_queue_t &rhs) {
    if (lhs.dist == rhs.dist) {
        assert(lhs.element != rhs.element);
        assert(lhs.element->id() != rhs.element->id());
        return lhs.element->id() > rhs.element->id();
    }
    return lhs.dist > rhs.dist;
}

std::vector<SchedElt*> FLSSorting::sort_llf(std::vector<SchedElt *> elements) {
    // LLF: for each task, compute the _longest_ path to the furthest terminal task it is connected to
    //
    // LLF is a bit weird because it visits the DAG in reverse order (terminal tasks first / post-order tree walk)
    // While it is relatively trivial to produce a list in "regular order" it in effect turns FLS into
    // "Backward List Sorting". In this sorting, these steps happen explicitly (including the reversing).
    // Note that this is exactly what the FLSFragQreadyBuilder::build_QreadyLaxity does by first recursively
    // visiting the DAG, and only then building the list. As such, it does not have an explicit "reverse" step.
    //
    // The basis of this algorithm is Dijkstras, applied in reverse. Elements are only added to the queue when all
    // other paths have been visited, ensuring that the longest path is taken (shorter paths will be visited first).
    // Dijkstras is not what FLSFragQreadyBuilder::build_QreadyLaxity uses, as such this sorting probably outperforms it

    auto visited = std::set<SchedElt*>();
    auto Qready = std::priority_queue<llf_queue_t, std::vector<llf_queue_t>, std::greater<>>();
    auto Qdone = std::vector<SchedElt*>(); // in reverse

    // Locate the end
    for (SchedElt *element : elements) {
        if (element->successors().empty()) {
            Qready.emplace(element, element->wct());
        }
    }

    if (Qready.empty())
        throw CompilerException("ffls", "Task graph contains cycles");

    while (!Qready.empty()) {
        llf_queue_t top = Qready.top();
        Qready.pop();

        Qdone.push_back(top.element);
        visited.insert(top.element);

        for (SchedElt* predecessor : top.element->previous()) {
            // Only add it if all its successors are already visited (=we are visiting the last path now)
            bool allVisited = true;
            for (SchedElt *successor: predecessor->successors()) {
                if (visited.find(successor) == visited.end()) {
                    allVisited = false;
                    break;
                }
            }

            // Add predecessor to the queue
            // We are the last path to this predecessor -> our dist is the highest from its successors
            if (allVisited) {
                Qready.emplace(predecessor, top.dist + predecessor->wct());
            }
        }
    }

    // Qdone was constructed in reverse
    std::reverse(Qdone.begin(),  Qdone.end());
    return Qdone;
}
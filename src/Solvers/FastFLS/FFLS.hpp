/*
 * Copyright (C) 2022 Lukas Miedema <l.miedema@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CECILE_FFLS_HPP
#define CECILE_FFLS_HPP

#include <Solvers/Solver.hpp>

//! Convenience data structure to track the current "best"
struct FlsPlacementCandidate {
    SchedCore *core; // The best core
    Version *version; // The best version
    timinginfos_t responseTime; // Release time + WCET of the SchedElt on that core (lower = better)
    timinginfos_t delay; // The delay (time after the last SchedElt on that core finishes), (lower = better)
    long scheduleIndex; // The index in that cores schedule vector
};

/**
 * Implements a restricted version of FLS with good runtime performance.
 * Algorithmic complexity of sorting (LLF) is O(n log n) (n=task count)
 * Algorithmic complexity of scheduling a single task is O(n * v) (n=task count, v=max versions for one task)[1]
 * Total complexity is O(n (log n + n*v)) = O(n^2 * v)
 *
 * [1] Can probably be optimized with a more efficient data structure for tracking "gaps" in the schedule
 *
 * Supports heterogeneous multi-version scheduling
 */
class FastForwardListScheduling : public Solver {
public:
    explicit FastForwardListScheduling() : Solver() {};

    //! \copydoc CompilerPass::get_uniqid_rtti
    const std::string get_uniqid_rtti() const override { return "solver-ffls"; }

    //! \copydoc CompilerPass::help
    virtual std::string help() override;

    //! \copydoc Solver::solve
    void solve(Schedule *result) override;

private:
    //! Evaluate the placement of a core to a task. This function has no (lasting) side-effects.
    FlsPlacementCandidate evaluatePlacement(
            std::map<SchedCore *, std::vector<SchedElt *>> *schedule, SchedCore *core,
            SchedElt *toSchedule, Version *version, timinginfos_t notBefore);
};

REGISTER_SOLVER(FastForwardListScheduling, "ffls")

#endif //CECILE_FFLS_HPP

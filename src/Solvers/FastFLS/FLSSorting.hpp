/*
 * Copyright (C) 2022 Lukas Miedema <l.miedema@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CECILE_FLSSORTING_HPP
#define CECILE_FLSSORTING_HPP

#include <Schedule.hpp>

/**
 * Bunch of static list sorting functions.
 */
class FLSSorting {
public:
    static std::vector<SchedElt *> sort_llf(std::vector<SchedElt *> elements);
};

struct llf_queue_t {
    SchedElt *element;
    timinginfos_t dist; // distance from the _start_ of this element to the end of the furthest terminal task
    llf_queue_t(SchedElt *element, timinginfos_t dist) : element(element), dist(dist) {}
};

bool operator>(const llf_queue_t &a, const llf_queue_t &b);

#endif //CECILE_FLSSORTING_HPP

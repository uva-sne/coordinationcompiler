/*!
 * \file ForwardListScheduling.hpp
 * \copyright GNU Public License v3.
 * \date 2021
 * \author Julius Roeder (j.roeder@uva.nl), University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *
 * You should have received a copy of the GNU General Public License
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "Utils/Log.hpp"
#include "FLSMultiPhase.hpp"
#include "ScheduleFactory.hpp"

IMPLEMENT_HELP(FLSMultiPhase,
Julius will write something here
)

FLSMultiPhase::FLSMultiPhase(const SystemModel &m, const config_t &c) : FLSStrategy(m, c) {
    crpd = new CRPD();
}

uint32_t FLSMultiPhase::compute_concurrency(Schedule *, SchedPacket *p) {
    p->concurrency(0);
    return 0;
}

uint64_t FLSMultiPhase::find_best_start_read(const Schedule &sched, SchedPacket *t) {
    uint64_t bestStart = t->min_rt_period();
    for(SchedElt * e : t->previous()) {
        if(e->rt() + e->wct() > bestStart)
            bestStart = e->rt() + e->wct();
    }
    return bestStart;
}

uint64_t FLSMultiPhase::find_best_start_exec(const Schedule &sched, SchedJob *t, SchedCore* c, std::vector<SchedElt*> *previous_cts, timinginfos_t wcet) {
    bool change = true;
    bool penalty_not_added = true;
    std::pair<uint64_t, uint64_t> return_pair;

//    uint64_t bestStart = t->get_end_last_read();
    uint64_t bestStart = 0;
    if (bestStart < t->min_rt_period()) {
        bestStart = t->min_rt_period();
    }
    // get predecessor end
    // TODO check if multi phase grabs the right predecessor end
    for (SchedElt *e : t->previous()) {
        if (e->rt() + e->wct() > bestStart)
            bestStart = e->rt() + e->wct();
        // checks for the other phases of the predecessor tasks
        for (SchedElt * schedel : sched.scheduled_elements()){
            if (schedel->schedtask()->task() == e->schedtask()->task()){
                bestStart =  std::max(bestStart, schedel->rt() + schedel->wct());
            }
        }
    }

    // previous phase execution time and rt
    uint64_t previous_phase_rt = 0;
    uint64_t previous_phase_wct = 0;
    if (!previous_cts->empty()) {
            previous_phase_rt = previous_cts->back()->rt();
            previous_phase_wct = previous_cts->back()->wct();
    }

    // max of previous phase or bestStart
    bestStart = std::max(bestStart, previous_phase_rt + previous_phase_wct);

    // end time of this phase
    uint64_t endtime = bestStart + wcet;

    // while any changes were made to the start time
    while (change) {
        change = false;
        // check if there is any overlap on the current core
        for (SchedElt *elt : sched.scheduled_elements()) {
            if (elt == t) continue;
            if (sched.get_mapping(elt)->core()->id != c->core()->id) continue;

            uint64_t eRtE = elt->rt();
            uint64_t eEndTime = eRtE + elt->wct();
            if (eRtE < endtime && bestStart < eEndTime) {
                bestStart = eEndTime;
                endtime = bestStart + wcet;
                change = true;
            }
        }
    }

    return bestStart;
}

void FLSMultiPhase::visitSchedEltPacketRead(Schedule *sched, SchedPacket *p) {
    p->rt(0);
    p->wct(0);
}
void FLSMultiPhase::visitSchedEltPacketWrite(Schedule *sched, SchedPacket *pac) {
    pac->rt(0);
    pac->rt(pac->schedtask()->rt() + pac->schedtask()->wct());
}

void FLSMultiPhase::visitSchedEltTask(Schedule *sched, SchedJob *t) {
    uint128_t bestScheduleTime = -1;
    sched->status(ScheduleProperties::sched_statue_e::SCHED_PARTIAL);

    Schedule best_Schedule(*sched);
    SchedJob* best_schedule_task = best_Schedule.schedjob(t->id());

    // for all versions
    for(Version *t_version: t->task()->versions()) {
        Schedule version_schedule(*sched);
        SchedJob* version_t = version_schedule.schedjob(t->id());

        std::vector<SchedElt*> previous_cts;

        bool version_scheduled = false;

        // for all phases
        for (Phase *phase: t_version->phases()) {
            // need to set the elements to the phase times. just setting *t isnt enough because the schedules arent constructed from *sched.
            if (phase == t_version->phases().front()) {
                version_t->wct(phase->C());
                version_t->selected_version(t_version->id());
            }
            Schedule best_core_schedule(version_schedule);
            SchedJob* best_core_task = best_core_schedule.schedjob(version_t->id());
            best_core_task->rt(1000000);

            best_core_schedule.makespan(-1);
            SchedJob* ct;

            // need to make sure that a phase actually is scheduled
            bool phase_scheduled = false;

            // all cores
            for (SchedCore *candidate : sched->schedcores()) {
                if (find_if(forbidden_proc_mapping[t->task()].begin(), forbidden_proc_mapping[t->task()].end(),
                            [candidate](SchedCore *a) { return a->core() == candidate->core(); }) !=
                    forbidden_proc_mapping[t->task()].end()) {
                    Utils::DEBUG("\tcandidate " + *candidate + " unusable due to previous unschedulability issue");
                    continue;
                }
                // loop overall versions of a task
                // if phase proc and current proc dont match -> continue
                if ((*phase->force_mapping_proc().begin())->type != candidate->core()->type) {
                    continue;
                }
                // update task wct and wce for current versions
                Utils::DEBUG("\tcandidate " + *candidate);

                // makes another internal_schedule copy and adds the task & version to this internal_schedule
                Schedule internal_schedule(version_schedule);
                if (phase == t_version->phases().front()){
                    // for the first phase we use the original job
                    version_t->wct(phase->C());
//                    version_t->rt(phase->rt());
                    ct = internal_schedule.schedjob(version_t->id());
                } else {
                    // if its a new phase we build a new job
                    ct =  ScheduleFactory::build_job(&internal_schedule, previous_cts.back()->schedtask()->task(), phase->rt());
                    // add original phase to
                    for (SchedElt* succ: previous_cts.front()->successors()){
                        if (std::find_if(previous_cts.begin(), previous_cts.end(), [succ](SchedElt * p_ct){return succ->equals(p_ct);}) == previous_cts.end()){
                            ct->add_successor(internal_schedule.schedjob(succ->id()));
                        }
                    }
                    // add phase as successor to previous phase
                    previous_cts.back()->add_successor(ct);
                    internal_schedule.schedjob(previous_cts.back()->id())->add_successor(ct);
                }

                timinginfos_t new_wcet = phase->C();

                // find best start read phase.
                for (SchedPacket *p : ct->packet_read()) {
                    p->rt(find_best_start_read(internal_schedule, p));
                }

                // find best start for execution of this phase
                uint64_t cur_rt = find_best_start_exec(internal_schedule, ct, candidate, &previous_cts, new_wcet);

                // Migration & Entanglement due tasks ALREADY scheduled
                // Check if there is migration or Entanglement in comparison to all previous phases that could be scheduled on the same CPU type
                // this adds entanglement to the current phase BEFORE determining the final RT
                // if no --> add CRPD
                bool applied_crpd=false;
                for (SchedElt *pp: previous_cts) {
                    if (pp->schedtask()->phase() == phase) break; // dont need to check the phases AFTER this phase
                    // Migration:
                    // TODO: Test
                    // Have to find the scheduled elt of the previous phases
                    SchedCore * pp_core = version_schedule.get_mapping(pp);
                    // if the previous phase is of the same CPU type, is it on the same CPU ID?
                    if (pp_core->core()->type == candidate->core()->type && pp_core->core()->id != candidate->core()->id){
                        // if no --> is there already an inference cost on the current phase?
                        if (!applied_crpd){
                            new_wcet = (phase->C() + crpd->get_CRPD(phase->C()));
                            applied_crpd=true;
                            // interference delay added already dont need to check more
                            break;
                        }
                    }

                    // Entanglement due tasks ALREADY scheduled
                    // Tested and works
                    // only need to check for existing entanglement if previous phase and current phase are on the same core
                    if (pp_core->core()->id == candidate->core()->id) {
                        // Given the current release time check if there is entanglement of the task/phase that is currently being scheduled
                        for (SchedElt *interference_task: internal_schedule.scheduled_elements()) {
                            if (interference_task->type() != SchedElt::TASK) continue;
                            // if the schedule element is on the same core as current phase
                            if (internal_schedule.get_mapping(interference_task)->core()->id != candidate->core()->id) continue;
                            // if the scheduled element is different than the previous phase (tested by release time)
                            if (interference_task->equals(pp)) continue;
                            // if we get here check if the interference is between the previous phase and the current phase
                            if (pp->rt() + pp->wct() <= interference_task->rt() && interference_task->rt() + interference_task->wct() <= cur_rt ){
                                if (!applied_crpd) {
                                    new_wcet = (phase->C() + crpd->get_CRPD(phase->C()));
                                    applied_crpd=true;
                                    break; // interference delay added already dont need to check more
                                }else{
                                    // interference delay added already dont need to check more
                                    break;
                                }
                            }
                        }
                    }
                }

                //redo incase wcet changed
                cur_rt = find_best_start_exec(internal_schedule, ct, candidate, &previous_cts, new_wcet);

                ct->wct(new_wcet);
                ct->rt(cur_rt);
                ct->selected_version(t_version->id());
                ct->phase(phase);
                ct->interference_applied(applied_crpd);

                // Entanglement due to task that is currently BEING scheduled
                // check if scheduling of current phases CAUSES entanglement of OTHER tasks that have already been scheduled
                bool entangled = false;
                SchedElt * entangled_Elt;
                for (SchedElt *interference_task1: internal_schedule.scheduled_elements()) {
                    // if the interference_task belongs to the same task as the currently scheduled task we can ignore it
                    if (interference_task1->schedtask()->task() == ct->task()) continue;
                    // if the interference_task and the currently considered candidate are not on the same core we can ignore them
                    if (internal_schedule.get_mapping(interference_task1)->core() != candidate->core()) continue;
                    for (SchedElt *interference_task2: internal_schedule.scheduled_elements()) {
                        // dont care if the two SchedElts are the same
                        if (interference_task1 == interference_task2) continue;
                        if (ct == interference_task2) continue;
                        // dont care if the second task isnt on the same core
                        if (internal_schedule.get_mapping(interference_task2)->core() != candidate->core()) continue;
                        // dont care of the two phases dont belong to the same task
                        if (interference_task1->schedtask()->task() != interference_task2->schedtask()->task()) continue;
                        // if the current task is being scheduled BETWEEN the two previous tasks.
                        if (interference_task1->rt() + interference_task1->wct() <= cur_rt && cur_rt + phase->C() <= interference_task2->rt()){
                            // only if the crpd hasnt been applied yet!
                            if (!interference_task2->interference_applied() && !applied_crpd){
                                entangled = true;
                                interference_task2->interference_applied(true);
                                entangled_Elt = interference_task2;
                            }
                        }
                    }
                }

                // if this caused entanglement  --> find all impacted schedElt
                std::vector<SchedElt*> impacted_schedElts;
                std::vector<SchedElt*> impacted_schedElts_temp;
                if (entangled){
                    impacted_schedElts.push_back(entangled_Elt);
                    timinginfos_t additional_entang_cost = crpd->get_CRPD(entangled_Elt->wct());

                    // find all impacted schedElts
                    bool change = true;
                    while(change){
                        change = false;
                        // find impacted successors
                        for (SchedElt* impacted: impacted_schedElts){
                            for (SchedElt* impacted_suc: impacted->successors()){
                                // if the successor has been scheduled
                                if (std::find(internal_schedule.scheduled_elements().begin(), internal_schedule.scheduled_elements().end(), impacted_suc) != internal_schedule.scheduled_elements().end()){
                                    // make sure its not in the non-temporary vector
                                    if (std::find(impacted_schedElts.begin(), impacted_schedElts.end(), impacted_suc) != impacted_schedElts.end()) continue;
                                        // if it hasnt been added to the temp vector of impacted elements yet
                                    if (std::find(impacted_schedElts_temp.begin(), impacted_schedElts_temp.end(), impacted_suc) == impacted_schedElts_temp.end()){
                                        // TODO: make sure ALL phases of the successor are added to the impacted list
                                        impacted_schedElts_temp.push_back(impacted_suc);
                                        change = true;
                                    }
                                }
                            }
                        }
                        impacted_schedElts.insert(impacted_schedElts.end(), impacted_schedElts_temp.begin(), impacted_schedElts_temp.end());
                        impacted_schedElts_temp.clear();
                        // find scheduled elements on the same core after impacted the scheduled elts
                        for (SchedElt* impacted: impacted_schedElts){
                            for (SchedElt* after_impacted: internal_schedule.scheduled_elements()){
                                // dont need to check if they are the same
                                if (after_impacted->equals(impacted)) continue;
                                // if on same core as one of the other impacted
                                if(internal_schedule.get_mapping(impacted)->core()->id == internal_schedule.get_mapping(after_impacted)->core()->id){
                                    // only care about tasks that are AFTER the current task
                                    if(impacted->rt()+impacted->wct() <= after_impacted->rt()){
                                        // make sure its not in the non-temporary vector
                                        if (std::find(impacted_schedElts.begin(), impacted_schedElts.end(), after_impacted) != impacted_schedElts.end()) continue;
                                        // if it hasnt been added to the vector of impacted elements yet
                                        if (std::find(impacted_schedElts_temp.begin(), impacted_schedElts_temp.end(), after_impacted) == impacted_schedElts_temp.end()){
                                            // TODO: make sure ALL phases of the task after the current one are added to the impacted list
                                            impacted_schedElts_temp.push_back(after_impacted);
                                            change = true;
                                        }
                                    }
                                }
                            }
                        }
                        impacted_schedElts.insert(impacted_schedElts.end(), impacted_schedElts_temp.begin(), impacted_schedElts_temp.end());
                        impacted_schedElts_temp.clear();
                    }

                    // move all impacted schedElts
                    for (SchedElt* elt: impacted_schedElts){
                        if (elt != entangled_Elt){
                            internal_schedule.add_interference_impacted(elt, additional_entang_cost, "rt");
                        }else{
                            // this is the first element that was delayed
                            internal_schedule.add_interference_impacted(elt, additional_entang_cost, "wct");
                            elt->interference_applied(true);
                        }
                    }
                }

                internal_schedule.map_core(ct, internal_schedule.schedcore(candidate->core()));
                internal_schedule.schedule(ct);
                phase_scheduled = true;

                Utils::DEBUG("\t\t synchronized: " + *ct);

                internal_schedule.compute_makespan();
                if ((internal_schedule.makespan() < best_core_schedule.makespan()) || (internal_schedule.makespan() >! best_core_schedule.makespan() && cur_rt < best_core_task->rt())) {
//                if (cur_rt + new_wcet < best_core_task->rt() + best_core_task->wct()) {
//                    need to make sure that the current element gets updated in all schedule copies
                    if (phase == t_version->phases().front()) {
                        version_t->rt(cur_rt);
                        version_t->wct(new_wcet);
                        version_t->selected_version(t_version->id());
                        version_t->phase(phase);
                        best_core_task->rt(cur_rt);
                        best_core_task->wct(new_wcet);
                        best_core_task->selected_version(t_version->id());
                        best_core_task->phase(phase);
                    }
                    // safe the best core rt for later.
                    best_core_schedule = internal_schedule;
                    best_core_schedule.compute_makespan();

                    best_core_task = best_core_schedule.schedjob(version_t->id());
                    // make sure that the phases of this version are updated if the found internal_schedule is better
                }
            }

            // if a phase was not scheduled - dont try to schedule more phases
            if (!phase_scheduled)
                break;

            version_schedule =  best_core_schedule;
            version_t = version_schedule.schedjob(t->id());

            if (phase == t_version->phases().front()) {
                previous_cts.push_back(version_t);
            }else{
                previous_cts.push_back(ct);
            }
            version_scheduled = true;
        }
        // here all phases of a version are scheduled to the best CU
        // recompute makespan and
        // todo check if putting a condition on the starting time helps
        version_schedule.compute_makespan();
        if (bestScheduleTime > version_schedule.makespan() && version_scheduled) {
//        if (bestScheduleTime > (version_t->rt() + version_t->wct()) && version_scheduled) {

                Utils::DEBUG("\t\tbest candidate makespan -- " + to_string(version_schedule.makespan()) + " (" +
                         to_string(bestScheduleTime) + ")");

            // best schedule
            t->rt(version_t->rt());
            t->wct(version_t->wct());
            t->selected_version(version_t->selected_version());
            best_schedule_task->rt(version_t->rt());
            best_schedule_task->wct(version_t->wct());
            best_schedule_task->selected_version(version_t->selected_version());

//            for (SchedElt * succ: previous_cts){
//                if (!t->equals(succ)){
//                    t->add_successor(succ);
//                    best_schedule_task->add_successor(succ);
//                }
//            }

            if (previous_cts.size()>1){
                t->add_successor(previous_cts[1]);
                best_schedule_task->add_successor(previous_cts[1]);
            }

            bestScheduleTime = version_schedule.makespan();
//            bestScheduleTime = version_t->rt() + version_t->wct();
            best_Schedule = version_schedule;
            best_schedule_task = best_Schedule.schedjob(t->id())->schedtask();
        } else {
            Utils::DEBUG("\t\t\t\tbad candidate makespan -- " + to_string(version_schedule.makespan()) + " (" +
                         to_string(bestScheduleTime) + ")");
        }
    }
    *sched = best_Schedule;
    sched->status(ScheduleProperties::sched_statue_e::SCHED_COMPLETE);
}

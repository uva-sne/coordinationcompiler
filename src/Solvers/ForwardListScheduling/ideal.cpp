/*!
 * \file ideal.cpp
 * \copyright GNU Public License v3.
 * \date 2017
 * \author Benjamin Rouxel <benjamin.rouxel@uva.nl>, University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "ideal.hpp"
#include "Utils/Log.hpp"

using namespace std;

IMPLEMENT_HELP(FLSIdeal,
    Generate the schedule of an application\n
    \n
    Communication between tasks are assumed to be ideal, meaning that there is 
    never an interference
)

FLSIdeal::FLSIdeal(const SystemModel &m, const config_t &c) : FLSStrategy(m, c) {}

uint32_t FLSIdeal::compute_concurrency(Schedule *, SchedPacket *p) {
    p->concurrency(0);
    return 0;
}

uint64_t FLSIdeal::find_best_start_read(const Schedule &sched, SchedPacket *t) {
    uint64_t bestStart = t->min_rt_period();
    for(SchedElt * e : t->previous()) {
        if(e->rt() + e->wct() > bestStart)
            bestStart = e->rt() + e->wct();
    }
    return bestStart;
}

uint64_t FLSIdeal::find_best_start_exec(const Schedule &sched, SchedJob *t) {
    uint64_t bestStart = t->get_end_last_read();
    if(bestStart < t->min_rt_period())
        bestStart = t->min_rt_period();
    for(SchedElt * e : t->previous()) {
        if(e->rt() + e->wct() > bestStart)
            bestStart = e->rt() + e->wct();
    }
    
    uint64_t endtime = bestStart + t->wct();
    for(SchedElt *elt : sched.scheduled_elements()) {
        if(elt == t) continue;
        if(sched.get_mapping(elt) != sched.get_mapping(t)) continue;
        
        uint64_t eRtE = elt->rt();
        uint64_t eEndTime = eRtE + elt->wct();
        if(eRtE < endtime && bestStart < eEndTime) {
            bestStart = eEndTime;
            endtime = bestStart + t->wct();
        }
    }

    return bestStart;
}

void FLSIdeal::visitSchedEltPacketRead(Schedule *sched, SchedPacket *p) {
    p->rt(0);
    p->wct(0);
}
void FLSIdeal::visitSchedEltPacketWrite(Schedule *sched, SchedPacket *pac) {
    pac->rt(pac->schedtask()->rt() + pac->schedtask()->wct());
    pac->wct(0);
}

void FLSIdeal::visitSchedEltTask(Schedule *sched, SchedJob *t) {
    uint128_t bestScheduleTime = -1;
    Schedule bestSchedule(*sched);
    bestSchedule.status(ScheduleProperties::sched_statue_e::SCHED_PARTIAL);
    
    for(SchedCore *candidate : sched->schedcores()) {
        if(find_if(forbidden_proc_mapping[t->task()].begin(), forbidden_proc_mapping[t->task()].end(), [candidate](SchedCore *a) {return a->core() == candidate->core();}) != forbidden_proc_mapping[t->task()].end()) {
            Utils::DEBUG("\tcandidate "+*candidate+" unusable due to previous unschedulability issue");
            continue;
        }
        // loop overall versions of a task
        for(Version *t_version: t->task()->versions()){
            // if version proc and current proc dont match -> continue
            if(!t_version->force_mapping_proc().empty() && (*t_version->force_mapping_proc().begin())->type != candidate->core()->type)
                continue;
            
            // update task wct and wce for current versions
            t->wct(t_version->C());
            t->wce(t_version->C_E());
            t->schedtask()->selected_version(t_version->id());

            Utils::DEBUG("\tcandidate "+*candidate);

            Schedule schedule(*sched);
            SchedJob *ct = schedule.schedjob(t->id()); 
            ct->selected_version(t_version->id());
            schedule.map_core(ct, schedule.schedcore(candidate->core()));
            schedule.schedule(ct);

            for(SchedPacket *p : ct->packet_read()) {
                p->rt(find_best_start_read(schedule, p));
            }

            ct->rt(find_best_start_exec(schedule, ct));

            schedule.compute_makespan();
            schedule.compute_energy();

            if(bestScheduleTime > schedule.makespan()) {
                Utils::DEBUG("\t\tbest candidate makespan -- "+to_string(schedule.makespan())+" ("+to_string(bestScheduleTime)+")");
                bestSchedule = schedule;
                bestSchedule.status(ScheduleProperties::sched_statue_e::SCHED_COMPLETE);
                bestScheduleTime = schedule.makespan();
            }
            else
                Utils::DEBUG("\t\t\t\tbad candidate makespan -- "+to_string(schedule.makespan())+" ("+to_string(bestScheduleTime)+")");
        }
    }

    *sched = bestSchedule;
}

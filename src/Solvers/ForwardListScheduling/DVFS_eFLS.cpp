/* 
 * Copyright (C) 2019 Julius Roeder <j.roeder@uva.nl> 
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "Utils/Log.hpp"
#include "DVFS_eFLS.h"
//TODO: implement completely

using namespace std;

FLSEnergy_DVFS::FLSEnergy_DVFS(const SystemModel &m, const config_t &c) : FLSStrategy(m, c) {}

IMPLEMENT_HELP(FLSEnergy_DVFS,
        Julius will add some blah blah
)

uint32_t FLSEnergy_DVFS::compute_concurrency(Schedule *, SchedPacket *p) {
     p->concurrency(0);
    return p->concurrency();
}

uint64_t FLSEnergy_DVFS::find_best_start_read(const Schedule &sched, SchedPacket *t) {
    uint64_t bestStart = t->min_rt_period();
    for(SchedElt * e : t->previous()) {
        if(e->rt() + e->wct() > bestStart)
            bestStart = e->rt() + e->wct();
    }
    return bestStart;
}

uint64_t FLSEnergy_DVFS::find_best_start_exec(const Schedule &sched, SchedJob *t) {
    uint64_t bestStart = t->get_end_last_read();

    if(bestStart < t->min_rt_period())
        bestStart = t->min_rt_period();

    for(SchedElt * e : t->previous()) {
        if(e->rt() + e->wct() > bestStart)
            bestStart = e->rt() + e->wct();
    }

    uint64_t endtime = bestStart + t->wct();

    //checks if the current task overlaps with any previous task and moves it forwards.
    bool change = true;
    while (change) {
        change = false;
        for (SchedElt *elt : sched.scheduled_elements()) {
            if (elt == t) continue;
            uint64_t eRtE = elt->rt();
            uint64_t eEndTime = eRtE + elt->wct();

            if (t->gpu_required()) {
                if (eRtE < endtime && bestStart < eEndTime) {
                    if (elt->schedtask()->gpu_required()) {
                        bestStart = eEndTime;
                        endtime = bestStart + t->wct();
                        change = true;
                    } else {
                        if (sched.get_mapping(elt) == sched.get_mapping(t)) {
                            bestStart = eEndTime;
                            endtime = bestStart + t->wct();
                            change = true;
                        }
                    }
                }
            } else {
                if (sched.get_mapping(elt) != sched.get_mapping(t)) continue;
                if (eRtE < endtime && bestStart < eEndTime) {
                    bestStart = eEndTime;
                    endtime = bestStart + t->wct();
                    change = true;
                }
            }
        }

        //checks if the current task is on the same cluster and checks if the frequency is the same. otherwise move it forwards.
        for (SchedElt *elt : sched.scheduled_elements()) {
            if (elt == t) continue;
            if (sched.get_mapping(elt)->core()->voltage_island_id != sched.get_mapping(t)->core()->voltage_island_id) continue;

            uint64_t eRtE = elt->rt();
            uint64_t eEndTime = eRtE + elt->wct();
            if (eRtE < endtime && bestStart < eEndTime) {
                if (elt->schedtask()->frequency() != t->frequency()) {
                    bestStart = eEndTime;
                    endtime = bestStart + t->wct();
                    change = true;
                }
            }
        }
    }

    return bestStart;
}

void FLSEnergy_DVFS::visitSchedEltPacketRead(Schedule *sched, SchedPacket *p) {
    p->rt(0);
    p->wct(0);
}
void FLSEnergy_DVFS::visitSchedEltPacketWrite(Schedule *sched, SchedPacket *pac) {
    pac->wct(0);
    pac->rt(pac->schedtask()->rt() + pac->schedtask()->wct());
}

void FLSEnergy_DVFS::visitSchedEltTask(Schedule *sched, SchedJob *t) {
    energycons_t bestScheduleEnergy = -1;
    Schedule bestSchedule(*sched);
    bestSchedule.status(ScheduleProperties::sched_statue_e::SCHED_PARTIAL);

    for(SchedCore *candidate : sched->schedcores()) {
        if(find_if(forbidden_proc_mapping[t->task()].begin(), forbidden_proc_mapping[t->task()].end(), [candidate](SchedCore *a) {return a->core() == candidate->core();}) != forbidden_proc_mapping[t->task()].end()) {
            Utils::DEBUG("\tcandidate "+*candidate+" unusable due to previous unschedulability issue");
            continue;
        }
        for(Version* t_version: t->task()->versions()){
            //Todo: better processor type checking.
            if(!t_version->force_mapping_proc().empty() && find(t_version->force_mapping_proc().begin(), t_version->force_mapping_proc().end(), candidate->core()) == t_version->force_mapping_proc().end())
                continue;

            if (t_version->gpu_required() && tg.coprocessors().size() != 1) {
                if (tg.coprocessors().empty())
                    continue; // skip this version
                throw CompilerException("DVFS_eFLS", "Multiple coprocessors ambigious for DVFS_eFLS");
            }

            t->wct(t_version->C());
            t->wce(t_version->C_E());
            t->selected_version(t_version->id());
            t->frequency(t_version->frequency());
            t->gpu_frequency(t_version->gpu_frequency());
            t->gpu_required(t_version->gpu_required());

            Utils::DEBUG("\tcandidate "+*candidate);

            Schedule schedule(*sched);
            SchedJob *ct = schedule.schedjob(t->id());
            ct->selected_version(t_version->id());
            schedule.map_core(ct, schedule.schedcore(candidate->core()));

            for(SchedPacket *p : ct->packet_read()) {
                p->rt(find_best_start_read(schedule, p));
            }

            ct->rt(find_best_start_exec(schedule, ct));

            Utils::DEBUG("\t\t synchronized: "+*ct);

            schedule.compute_makespan();
            schedule.compute_energy();

            //using energy instead of makespan
            if(bestScheduleEnergy > schedule.energy()) {
                Utils::DEBUG("\t\tbest candidate energy -- "+to_string(schedule.energy())+" ("+to_string(bestScheduleEnergy)+")");
                bestSchedule = schedule;
                bestSchedule.status(ScheduleProperties::sched_statue_e::SCHED_COMPLETE);
                bestScheduleEnergy = schedule.energy();
            }
            else
                Utils::DEBUG("\t\t\t\tbad candidate energy -- "+to_string(schedule.energy())+" ("+to_string(bestScheduleEnergy)+")");
        }
    }

    *sched = bestSchedule;
}

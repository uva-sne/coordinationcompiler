/*!
 * \file worst_concurrency.cpp
 * \copyright GNU Public License v3.
 * \date 2017
 * \author Benjamin Rouxel <benjamin.rouxel@uva.nl>, University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "worst_concurrency.hpp"
#include "Utils/Log.hpp"

using namespace std;

IMPLEMENT_HELP(FLSWorstConcurrency,
    Generate the schedule of an application\n
    \n
    Communication between tasks are assumed to always imply the worst possible
    never an interference
)

FLSWorstConcurrency::FLSWorstConcurrency(const SystemModel &m, const config_t &c) : FLSStrategy(m, c) {}

uint32_t FLSWorstConcurrency::compute_concurrency(Schedule *, SchedPacket *p) {
    p->concurrency(tg.processors().size()-1);
    return p->concurrency();
}

uint64_t FLSWorstConcurrency::find_best_start_read(const Schedule &sched, SchedPacket *t) {
    uint64_t bestStart = t->min_rt_period();
    for(SchedElt * e : t->previous()) {
        if(e->rt() + e->wct() > bestStart)
            bestStart = e->rt() + e->wct();
    }
    return adjust_start_time(sched, t, bestStart);
}

uint64_t FLSWorstConcurrency::find_best_start_write(const Schedule &sched, SchedPacket *t) {
    return adjust_start_time(sched, t, t->schedtask()->rt() + t->schedtask()->wct());
}

uint64_t FLSWorstConcurrency::find_best_start_exec(const Schedule &sched, SchedJob *t) {
    uint64_t bestStart = t->get_end_last_read();
    if(bestStart < t->min_rt_period())
        bestStart = t->min_rt_period();
    return adjust_start_time(sched, t, bestStart);
}

uint64_t FLSWorstConcurrency::adjust_start_time(const Schedule &sched, SchedElt *t, uint64_t bestStartTime) {
    uint64_t endtime = bestStartTime + t->wct();
    for(SchedElt *elt : sched.scheduled_elements()) {
        if(elt == t) continue;
        if(conf.archi.interconnect.behavior == "non-blocking" && elt->type() != t->type()) continue;
        if(sched.get_mapping(t) != sched.get_mapping(elt)) continue;
        
        uint64_t eRtE = elt->rt();
        uint64_t eEndTime = eRtE + elt->wct();
        if(eRtE < endtime && bestStartTime < eEndTime) {
            bestStartTime = eEndTime;
            endtime = bestStartTime + t->wct();
        }
    }
    return bestStartTime;
}

void FLSWorstConcurrency::optimize_communication(Schedule *sched, SchedJob *t) {
    for(SchedPacket *pac : t->packet_read()) {
        for(SchedElt* prev : pac->previous()) {
            if(pac->schedtask()->task() == prev->schedtask()->task()) continue;
            if(sched->get_mapping(t) != sched->get_mapping(prev)) continue;
            if(!Utils::isa<SchedPacket*>(prev)) continue;

            SchedPacket *prevpac = (SchedPacket*)prev;
            uint32_t data = 0;
            for (Connection *conn : pac->schedtask()->task()->predecessor_tasks().find(prevpac->schedtask()->task())->second)
                data += conn->to()->tokens();
            if(conf.archi.interconnect.burst == "none") {
                pac->data(pac->data() - data);
                prevpac->data(prevpac->data() - data);
            }
            else {
                pac->data(0); 
                prevpac->data(0);
            }
            pac->wct(tg.interconnect()->comm_delay(compute_concurrency(sched, pac), pac->data(), pac->datatype(), 0));
            prevpac->wct(tg.interconnect()->comm_delay(compute_concurrency(sched, prevpac), prevpac->data(), prevpac->datatype(), 0));
        }
    }
}

void FLSWorstConcurrency::adjust_schedule(Schedule *sched) {
    bool has_changed = true;
    while(has_changed) {
        has_changed = false;
        for(SchedElt *elt : sched->scheduled_elements()) {
            uint64_t oldstart = elt->rt();
            if(Utils::isa<SchedJob*>(elt)) {
                SchedJob *t = (SchedJob*)elt;
                elt->rt(find_best_start_exec(*sched, t));
            }
            else {
                SchedPacket *p = (SchedPacket*)elt;
                elt->rt((p->dirlbl() == "r") ? find_best_start_read(*sched, p) : find_best_start_write(*sched, p));
            }
            has_changed = has_changed || oldstart != elt->rt();
        }
    }
}

/*! \note because Packet must not overlap when originating from the same core
 *   but can overlap with from different core, then read packet must be scheduled when the task is mapped/scheduled
 */
void FLSWorstConcurrency::visitSchedEltPacketRead(Schedule *sched, SchedPacket *p) {
    p->wct(tg.interconnect()->comm_delay(compute_concurrency(sched, p), p->data(), p->datatype(), 0));
}
void FLSWorstConcurrency::visitSchedEltPacketWrite(Schedule *sched, SchedPacket *pac) {
    pac->wct(tg.interconnect()->comm_delay(compute_concurrency(sched, pac), pac->data(), pac->datatype(), 0));
    pac->rt(find_best_start_write(*sched, pac));
    adjust_schedule(sched);
}

void FLSWorstConcurrency::visitSchedEltTask(Schedule *sched, SchedJob *t) {
    uint128_t bestScheduleTime = -1;
    Schedule bestSchedule(*sched);
    bestSchedule.status(ScheduleProperties::sched_statue_e::SCHED_PARTIAL);

    for(SchedCore *candidate : sched->schedcores()) {
        if(find_if(forbidden_proc_mapping[t->task()].begin(), forbidden_proc_mapping[t->task()].end(), [candidate](SchedCore *a) {return a->core() == candidate->core();}) != forbidden_proc_mapping[t->task()].end()) {
            Utils::DEBUG("\tcandidate "+*candidate+" unusable due to previous unschedulability issue");
            continue;
        }
        
        Utils::DEBUG("\tcandidate "+*candidate);

        Schedule schedule(*sched);
        SchedJob *ct = schedule.schedjob(t->id()); 
        schedule.map_core(ct, schedule.schedcore(candidate->core()));
        
        if(conf.archi.interconnect.mechanism == "shared")
            optimize_communication(&schedule, ct);
        
        for(SchedPacket *p : ct->packet_read()) {
            p->rt(find_best_start_read(schedule, p));
        }
        
        ct->rt(find_best_start_exec(schedule, ct));

        adjust_schedule(&schedule);
        
        schedule.compute_makespan();
        if(bestScheduleTime > schedule.makespan()) {
            Utils::DEBUG("\t\tbest candidate makespan -- "+to_string(schedule.makespan())+" ("+to_string(bestScheduleTime)+")");
            bestSchedule = schedule;
            bestSchedule.status(ScheduleProperties::sched_statue_e::SCHED_COMPLETE);
            bestScheduleTime = schedule.makespan();
        }
        else 
            Utils::DEBUG("\t\t\t\tbad candidate makespan -- "+to_string(schedule.makespan())+" ("+to_string(bestScheduleTime)+")");
    }

    *sched = bestSchedule;
}

/*!
 * \file FLSFragmentedQreadyBuilder.hpp
 * \copyright GNU Public License v3.
 * \date 2017
 * \author Benjamin Rouxel <benjamin.rouxel@inria.fr>, INRIA/Irisa
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SPLITFLSQREADYBUILDER_H
#define SPLITFLSQREADYBUILDER_H

#include <vector>

#include "config.hpp"
#include "Schedule.hpp"
#include "Utils/Log.hpp"

class FLSStrategy;
/**
 * Create a sort list of all elements to be scheduled
 * 
 * Order depends on the algorithm selected to walk-through the graph and tie 
 * breaking.
 */
class FLSFragQreadyBuilder {
public:
    //! Convenient type for the graph walk-through function
    typedef std::function<void (std::vector<SchedElt*>*, std::vector<SchedElt*>&)> func_graph_walk_through_t;
protected:
    /**
     * Singleton
     * @param m \copybrief FLSFragQreadyBuilder::tg
     * @param c \copybrief FLSFragQreadyBuilder::conf
     * @param s \copybrief FLSFragQreadyBuilder::strategy
     * @return FLSFragQreadyBuilder
     */
    static FLSFragQreadyBuilder* getInstance(const SystemModel &m, const config_t &c, FLSStrategy *s);
 
    const SystemModel &tg; //!< IR of the current application
    const config_t &conf; //!< the global configuration

    FLSStrategy *strategy; //!< the FLS strategy to apply
    
    std::map<SchedElt*, size_t> laxity; //!< laxity of scheduled elements
    std::map<SchedElt*, energycons_t> EnergyLaxity;
    std::map<SchedElt*, std::pair<energycons_t,  energycons_t>> best_Frequency_DynEnergy;
    
    /**
     * Sort elements 
     * 
     * First sort by releat time
     * Then communication before execution
     * Then by WCET/communication cost
     * 
     * @param a
     * @param b
     * @return a < b
     */
    static bool sortFun(SchedElt *a, SchedElt *b);
    
    /**
     * Build the list
     * @param elements all elements that will be scheduled
     * @param schedorder resulting order of the aforementioned elemens
     * @param fun_walk_through how to walk-through the graphs
     */
    virtual void buildList(Schedule *sched, std::vector<SchedElt*> *schedorder, FLSFragQreadyBuilder::func_graph_walk_through_t fun_walk_through);
    
    /**
     * Helper function for a DFS algorithm
     * @param Qdone elements already visited
     * @param current element
     * @param Qready elements to visit next according to dependencies
     */
    virtual void get_next_readyDFS(std::vector<SchedElt*> &Qdone, SchedElt *current, std::vector<SchedElt*> *Qready);
    /**
     * Perform a DFS walk-through the graph
     * @param Qready initial element to start
     * @param unused_compat_fun_walk_through
     */
    virtual void build_QreadyDFS(std::vector<SchedElt*> *Qready, std::vector<SchedElt*> &unused_compat_fun_walk_through);
    
    /**
     * Helper function for a BFS algorithm
     * @param Qdone elements already visited
     * @param current element
     * @param Qready elements to visit next according to dependencies
     */
    virtual void get_next_readyBFS(std::vector<SchedElt*> &Qdone, SchedElt *current, std::vector<SchedElt*> *Qready);
    /**
     * Perform a BFS walk-through the graph
     * @param Qready next elements to visit
     * @param Qdone elements already visited
     */
    virtual void build_QreadyBFS(std::vector<SchedElt*> *Qready, std::vector<SchedElt*> &Qdone);
    
    /**
     * Helper function for a DFS algorithm
     * @param Qdone elements already visited
     * @param current element
     * @param Qready elements to visit next according to dependencies
     */
    virtual void get_next_readyDFSwithreaddelay(std::vector<SchedElt*> &Qdone, std::vector<SchedElt*> &current, std::vector<SchedElt*> *Qready);
    /**
     * \brief Perform a DFS walk-through the graph
     * \see get_next_readyDFSwithreaddelay
     * @param Qready
     * @param unused_compat_fun_walk_through
     */
    virtual void build_QreadyDFSwithreaddelay(std::vector<SchedElt*> *Qready, std::vector<SchedElt*> &unused_compat_fun_walk_through);
    
    /**
     * Order scheduled element according to their laxity (time left to the deadline)
     * @param Qready
     * @param Qdone
     */
    virtual void build_QreadyLaxity(std::vector<SchedElt*> *Qready, std::vector<SchedElt*> &Qdone);
    /**
     * Order scheduled element according to their energy laxity (energy left to the deadline)
     * @param Qready
     * @param Qdone
     */
    virtual void build_QreadyEnergyLaxity(std::vector<SchedElt*> *Qready, std::vector<SchedElt*> &Qdone);

public:
   /**
     * Helper function to call a DFS sorting
     * @param m \copybrief FLSFragQreadyBuilder::tg
     * @param c \copybrief FLSFragQreadyBuilder::conf
     * @param s \copybrief FLSFragQreadyBuilder::conf
     * @param elements to sort
     * @param schedorder to get the final elements order
     */
    static void buildListDFS(const SystemModel &m, const config_t &c, FLSStrategy *s, Schedule *sched, std::vector<SchedElt*> *schedorder);
   /**
     * \brief Helper function to call a DFS with read delay sorting
     * \see build_QreadyDFSwithreaddelay
     * @param m \copybrief FLSFragQreadyBuilder::tg
     * @param c \copybrief FLSFragQreadyBuilder::conf
     * @param s \copybrief FLSFragQreadyBuilder::conf
     * @param elements to sort
     * @param schedorder to get the final elements order
     */
    static void buildListDFSWithRDelay(const SystemModel &m, const config_t &c, FLSStrategy *s, Schedule *sched, std::vector<SchedElt*> *schedorder);
   /**
     * Helper function to call a BFS sorting
     * @param m \copybrief FLSFragQreadyBuilder::tg
     * @param c \copybrief FLSFragQreadyBuilder::conf
     * @param s \copybrief FLSFragQreadyBuilder::strategy
     * @param elements to sort
     * @param schedorder to get the final elements order
     */
    static void buildListBFS(const SystemModel &m, const config_t &c, FLSStrategy *s, Schedule *sched, std::vector<SchedElt*> *schedorder);
   /**
     * Helper function to call a Laxity sorting
     * @param m \copybrief FLSFragQreadyBuilder::tg
     * @param c \copybrief FLSFragQreadyBuilder::conf
     * @param s \copybrief FLSFragQreadyBuilder::strategy
     * @param elements to sort
     * @param schedorder to get the final elements order
     */
    static void buildListLaxity(const SystemModel &m, const config_t &c, FLSStrategy *s, Schedule *sched, std::vector<SchedElt*> *schedorder);
    static void buildListEnergyLaxity(const SystemModel &m, const config_t &c, FLSStrategy *s, Schedule *sched, std::vector<SchedElt*> *schedorder);

    //! Destructor
    virtual ~FLSFragQreadyBuilder() = default;
    
private:
    static FLSFragQreadyBuilder* _instance; //!< singleton
    /**
     * Constructor
     * @param m \copybrief FLSFragQreadyBuilder::tg
     * @param c \copybrief FLSFragQreadyBuilder::conf
     * @param s \copybrief FLSFragQreadyBuilder::strategy
     */
    explicit FLSFragQreadyBuilder(const SystemModel &m, const config_t &c, FLSStrategy *s) : tg(m), conf(c), strategy(s) {};
};

#endif /* SPLITFLSQREADYBUILDER_H */


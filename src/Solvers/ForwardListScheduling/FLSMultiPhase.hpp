/*!
 * \file ForwardListScheduling.hpp
 * \copyright GNU Public License v3.
 * \date 2021
 * \author Julius Roeder (j.roeder@uva.nl), University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef FLSMULTIPHASE_HPP
#define FLSMULTIPHASE_HPP

#include <Solvers/AdditionalAnalyses/CRPD.hpp>
#include "ForwardListScheduling.hpp"

/**
 * \brief Generate the schedule of an application
 *
 * Communication between tasks are assumed to be ideal, meaning that there is
 * never an interference
 *
 */
class FLSMultiPhase : public FLSStrategy {
public:
    //! \copydoc FLSStrategy::FLSStrategy
    explicit FLSMultiPhase(const SystemModel &m, const config_t &c);

    virtual uint32_t compute_concurrency(Schedule *sched, SchedPacket *t) override;
    /**
     * Find the best start time for a read packet
     * @param sched
     * @param t
     * @return time
     */
    uint64_t find_best_start_read(const Schedule &sched, SchedPacket *t);
    /**
     * Find the best start time for an exec phase
     * @param sched
     * @param t
     * @return time
     */
    uint64_t find_best_start_exec(const Schedule &sched, SchedJob *t, SchedCore* c, std::vector<SchedElt*> *previous_ct, timinginfos_t wcet);

    //! \copydoc SchedEltVisitor::visitSchedEltPacketRead
    void visitSchedEltPacketRead(Schedule *sched, SchedPacket *p);
    //! \copydoc SchedEltVisitor::visitSchedEltPacketWrite
    void visitSchedEltPacketWrite(Schedule *sched, SchedPacket *p);
    //! \copydoc SchedEltVisitor::visitSchedEltTask
    void visitSchedEltTask(Schedule *sched, SchedJob *t);
    
    virtual std::string help() override;

protected:
    CRPD *crpd;
};

REGISTER_FLSSTRATEGY(FLSMultiPhase, "multiphase")

#endif //METHANE_FLSMULTIPHASE_HPP

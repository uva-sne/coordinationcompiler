/* 
 * Copyright (C) 2020 Julius Roeder <j.roeder@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef eFLS_DVFS
#define eFLS_DVFS

#include "ForwardListScheduling.hpp"


using namespace std;
class FLSEnergy_DVFS : public FLSStrategy {
public:
    explicit FLSEnergy_DVFS(const SystemModel &m, const config_t &c);
    virtual uint32_t compute_concurrency(Schedule *, SchedPacket *p);
    uint64_t find_best_start_read(const Schedule &sched, SchedPacket *t);
    uint64_t find_best_start_exec(const Schedule &sched, SchedJob *t);
    void visitSchedEltPacketRead(Schedule *sched, SchedPacket *p);
    void visitSchedEltPacketWrite(Schedule *sched, SchedPacket *pac);
    void visitSchedEltTask(Schedule *sched, SchedJob *t);
    virtual std::string help() override;
};

REGISTER_FLSSTRATEGY(FLSEnergy_DVFS, "DVFS_eFLS")

#endif
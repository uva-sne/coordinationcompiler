/*!
 * \file FLSFragmentedQreadyBuilder.cpp
 * \copyright GNU Public License v3.
 * \date 2017
 * \author Benjamin Rouxel <benjamin.rouxel@inria.fr>, INRIA/Irisa
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "FLSFragmentedQreadyBuilder.hpp"

using namespace std;

FLSFragQreadyBuilder* FLSFragQreadyBuilder::_instance = nullptr;
FLSFragQreadyBuilder* FLSFragQreadyBuilder::getInstance(const SystemModel &m, const config_t &c, FLSStrategy *s) {
    if (_instance == nullptr) {
        _instance = new FLSFragQreadyBuilder(m, c, s);
    } else if (&m != &_instance->tg || &c != &_instance->conf || s != _instance->strategy) {
        delete _instance;
        _instance = new FLSFragQreadyBuilder(m, c, s);
    }
    return _instance;
}

bool FLSFragQreadyBuilder::sortFun(SchedElt *a, SchedElt *b) {
    if(a->rt() != b->rt())
        return a->rt() < b->rt();
    
    if(a->type() == SchedElt::Type::PACKET && b->type() == SchedElt::Type::TASK)
        return true;
    if(a->type() == SchedElt::Type::TASK && b->type() == SchedElt::Type::PACKET)
        return false;
    
    if(a->wct() == b->wct()) 
        return a->id() < b->id();
        
    return a->wct() > b->wct();
}

void FLSFragQreadyBuilder::buildListDFS(const SystemModel &m, const config_t &c, FLSStrategy *s, Schedule *sched, vector<SchedElt*> *schedorder) {
    getInstance(m, c, s)->buildList(sched, schedorder, [&m,&c,&s](vector<SchedElt*> *Qready, vector<SchedElt*> &Qdone) {
        FLSFragQreadyBuilder::getInstance(m, c, s)->build_QreadyDFS(Qready, Qdone);
    });
}

void FLSFragQreadyBuilder::buildListDFSWithRDelay(const SystemModel &m, const config_t &c, FLSStrategy *s, Schedule *sched, vector<SchedElt*> *schedorder) {
    getInstance(m, c, s)->buildList(sched, schedorder, [&m,&c,&s](vector<SchedElt*> *Qready, vector<SchedElt*> &Qdone) {
        FLSFragQreadyBuilder::getInstance(m, c, s)->build_QreadyDFSwithreaddelay(Qready, Qdone);
    });
}

void FLSFragQreadyBuilder::buildListBFS(const SystemModel &m, const config_t &c, FLSStrategy *s, Schedule *sched, vector<SchedElt*> *schedorder) {
    getInstance(m, c, s)->buildList(sched, schedorder, [&m,&c,&s](vector<SchedElt*> *Qready, vector<SchedElt*> &Qdone) {
        FLSFragQreadyBuilder::getInstance(m, c, s)->build_QreadyBFS(Qready, Qdone);
    });
}

void FLSFragQreadyBuilder::buildListLaxity(const SystemModel &m, const config_t &c, FLSStrategy *s, Schedule *sched, vector<SchedElt*> *schedorder) {
    getInstance(m, c, s)->buildList(sched, schedorder, [&m,&c,&s](vector<SchedElt*> *Qready, vector<SchedElt*> &Qdone) {
        FLSFragQreadyBuilder::getInstance(m, c, s)->build_QreadyLaxity(Qready, Qdone);
    });
}

void FLSFragQreadyBuilder::buildListEnergyLaxity(const SystemModel &m, const config_t &c, FLSStrategy *s, Schedule *sched, vector<SchedElt*> *schedorder) {
    getInstance(m, c, s)->buildList(sched, schedorder, [&m,&c,&s](vector<SchedElt*> *Qready, vector<SchedElt*> &Qdone) {
        FLSFragQreadyBuilder::getInstance(m, c, s)->build_QreadyEnergyLaxity(Qready, Qdone);
    });
}

 void FLSFragQreadyBuilder::buildList(Schedule *sched, vector<SchedElt*> *schedorder, FLSFragQreadyBuilder::func_graph_walk_through_t fun_walk_through) {
    vector<vector<SchedElt*>> DAGS;
    
    for(SchedEltPtr &e : sched->elements()) {
        if(e.get()->previous().size() == 0) {
            vector<SchedElt*> dag;
            vector<SchedElt*> QtmpDone;
            dag.push_back(e.get());
            fun_walk_through(&dag, QtmpDone);
            DAGS.push_back(dag);
        }
    }

    sort(DAGS.begin(), DAGS.end(), [](const vector<SchedElt*> &a, const vector<SchedElt*> &b) {
        uint64_t seqLengthA = 0, seqLengthB = 0;
        uint64_t miniterA = -1; //account for periodic task iteration with an initial rt set
        uint64_t miniterB = -1;
        for(SchedElt *ela : a) {
            seqLengthA += ela->wct();
            if(miniterA > ela->min_rt_period())
                miniterA = ela->min_rt_period();
        }
        for(SchedElt *elb : b) {
            seqLengthB += elb->wct();
            if(miniterB > elb->min_rt_period())
                miniterB = elb->min_rt_period();
        }
        if(miniterA < miniterB)
            return true;
        if(miniterB < miniterA)
            return false;
        
        return seqLengthA > seqLengthB;
    });
    
    for(vector<SchedElt*> dag : DAGS) {
        for(SchedElt* elt : dag) {
            Utils::DEBUG(elt->toString());
            schedorder->push_back(elt);
        }
    }
    Utils::DEBUG("Laxity:");
    for(auto el : laxity) {
        Utils::DEBUG(el.first->toString()+" - "+to_string(el.second));
    }
    laxity.clear();
    //assert(false);
}
 
void FLSFragQreadyBuilder::get_next_readyBFS(vector<SchedElt*> &Qdone, SchedElt *current, vector<SchedElt*> *Qready) {
    vector<SchedElt*> Qtmp;
    
    for(SchedElt *s : current->successors()) {
        if(find(Qdone.begin(), Qdone.end(), s) != Qdone.end())
            continue;
        bool ok = true;
        for(SchedElt* el : s->previous()) {
            if(find(Qdone.begin(), Qdone.end(), el) == Qdone.end()) {
                ok = false;
                break;
            }
        }
        if(ok)
            Qtmp.push_back(s);
    }
    sort(Qtmp.begin(), Qtmp.end(), FLSFragQreadyBuilder::sortFun);
    Qready->insert(Qready->end(), Qtmp.begin(), Qtmp.end());
}

void FLSFragQreadyBuilder::build_QreadyBFS(vector<SchedElt*> *Qready, vector<SchedElt*> &Qdone) {
    vector<SchedElt*> nextQready;
    for(SchedElt *root : *Qready) {
        Qdone.push_back(root);
        get_next_readyBFS(Qdone, root, &nextQready);
    }
    if(nextQready.empty()) {
        return;
    }

    build_QreadyBFS(&nextQready, Qdone);
    Qready->insert(Qready->end(), nextQready.begin(), nextQready.end());
}

void FLSFragQreadyBuilder::get_next_readyDFS(vector<SchedElt*> &Qdone, SchedElt *current, vector<SchedElt*> *Qready) {
    if(find(Qdone.begin(), Qdone.end(), current) != Qdone.end())
        return;
    
    bool ok = true;
    for(SchedElt* el : current->previous()) {
        if(find(Qdone.begin(), Qdone.end(), el) == Qdone.end()) {
            ok = false;
            break;
        }
    }
    if(ok) {
        Qready->push_back(current);
        Qdone.push_back(current);

        vector<SchedElt*> succs(current->successors());
        sort(succs.begin(), succs.end(), FLSFragQreadyBuilder::sortFun);
        for(SchedElt *s : succs) {
            get_next_readyDFS(Qdone, s, Qready);
        }
    }
}

void FLSFragQreadyBuilder::build_QreadyDFS(vector<SchedElt*> *Qready, vector<SchedElt*> &) {
    vector<SchedElt*> nextQready;
    vector<SchedElt*> Qdone;

    for(SchedElt *root : *Qready) {
        Qdone.push_back(root);

        vector<SchedElt*> succs(root->successors());
        sort(succs.begin(), succs.end(), FLSFragQreadyBuilder::sortFun);
        for(SchedElt *s : succs) {
            get_next_readyDFS(Qdone, s, &nextQready);
        }
    }

    Qready->insert(Qready->end(), nextQready.begin(), nextQready.end());
}

void FLSFragQreadyBuilder::get_next_readyDFSwithreaddelay(vector<SchedElt*> &Qdone, vector<SchedElt*> &curlist, vector<SchedElt*> *Qready) {
    while(!curlist.empty()) {
        SchedElt *current = *(curlist.begin());
        curlist.erase(curlist.begin());
        
        if(find(Qdone.begin(), Qdone.end(), current) != Qdone.end()) {
            continue;
        }
    
        bool ok = true;
        for(SchedElt* el : current->previous()) {
            if(find(Qdone.begin(), Qdone.end(), el) == Qdone.end()) {
                ok = false;
                break;
            }
        }
    
        if(!ok) {
            curlist.push_back(current);
            continue;
        }
    
        /*! 
         * \note for read packet, look if all other first read packet of edges from the same task are elligible to schedule
         *  this is to delay reads for joiner in the scheduling order list
         */
        if(current->type() == SchedElt::Type::PACKET && ((SchedPacket*)current)->dirlbl() == "r") {
            ok = true;

            for(SchedElt* sib : current->siblings()) {
                for(SchedElt* el : sib->previous()) {
                    if(find(Qdone.begin(), Qdone.end(), el) == Qdone.end()) {
                        ok = false;
                        break;
                    }
                }
            }

            if(!ok){
                curlist.push_back(current);
                continue;
            }
            //! \note if ok, all siblings have been delayed, so they must be added now
            for(SchedElt* sib : current->siblings()) {
                Qready->push_back(sib);
                Qdone.push_back(sib);

                vector<SchedElt*> succs(sib->successors());
                sort(succs.begin(), succs.end(), FLSFragQreadyBuilder::sortFun);
                curlist.insert(curlist.begin(), succs.begin(), succs.end());
            }
        }
    
        Qready->push_back(current);
        Qdone.push_back(current);

        vector<SchedElt*> succs(current->successors());
        sort(succs.begin(), succs.end(), FLSFragQreadyBuilder::sortFun);
        curlist.insert(curlist.begin(), succs.begin(), succs.end());
    }
}

void FLSFragQreadyBuilder::build_QreadyDFSwithreaddelay(vector<SchedElt*> *Qready, vector<SchedElt*> &unused_compat_fun_walk_through) {
    if(conf.archi.interconnect.behavior == "blocking") {
        // because there is no distinction between communication phases and exec one, a simple DFS is enough
        build_QreadyDFS(Qready, unused_compat_fun_walk_through);
    }
    
    vector<SchedElt*> Qdone;
    vector<SchedElt*> roots(*Qready);
    Qready->clear();
    get_next_readyDFSwithreaddelay(Qdone, roots, Qready);
}

void FLSFragQreadyBuilder::build_QreadyLaxity(vector<SchedElt*> *Qready, vector<SchedElt*> &Qdone) {
    vector<SchedElt*> nextQready;
    for(SchedElt *root : *Qready) {
        Qdone.push_back(root);
        get_next_readyBFS(Qdone, root, &nextQready);
    }
    if(nextQready.empty()) {
        return;
    }

    build_QreadyLaxity(&nextQready, Qdone);

    for(SchedElt *elt : nextQready) {
        laxity[elt] = 0;
        for(SchedElt *suc : elt->successors()) {
            laxity[elt] = max(laxity[elt], laxity[suc]+suc->wct()); // ? This is probably broken -> suc->wct is already included
        }
        laxity[elt] += elt->wct();
    }
    sort(nextQready.begin(), nextQready.end(), [this](SchedElt* a, SchedElt *b) {
        if(laxity.at(a) == laxity.at(b))
            return a->id() < b->id();
        return laxity.at(a) > laxity.at(b);
    });

    Qready->insert(Qready->end(), nextQready.begin(), nextQready.end());
}

void FLSFragQreadyBuilder::build_QreadyEnergyLaxity(vector<SchedElt*> *Qready, vector<SchedElt*> &Qdone) {
    vector<SchedElt*> nextQready;
    for(SchedElt *root : *Qready) {
        Qdone.push_back(root);
        get_next_readyBFS(Qdone, root, &nextQready);
    }
    if(nextQready.empty()) {
        return;
    }

    build_QreadyEnergyLaxity(&nextQready, Qdone);

    for(SchedElt *elt : nextQready) {
        EnergyLaxity[elt] = 0;
        for(SchedElt *suc : elt->successors()) {
            EnergyLaxity[elt] = max(EnergyLaxity[elt], EnergyLaxity[suc]+suc->schedtask()->task()->C_E());
        }
        EnergyLaxity[elt] += elt->schedtask()->task()->C_E();
    }
    sort(nextQready.begin(), nextQready.end(), [this](SchedElt* a, SchedElt *b) {
        return EnergyLaxity.at(a) > EnergyLaxity.at(b);
    });

    Qready->insert(Qready->end(), nextQready.begin(), nextQready.end());
}
/*!
 * \file ForwardListScheduling.hpp
 * \copyright GNU Public License v3.
 * \date 2021
 * \author Julius Roeder (j.roeder@uva.nl), University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *
 * You should have received a copy of the GNU General Public License
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "Utils/Log.hpp"
#include "FLSSinglePhase.hpp"
#include "ScheduleFactory.hpp"

IMPLEMENT_HELP(FLSSinglePhase,
Julius will write something here
)

FLSSinglePhase::FLSSinglePhase(const SystemModel &m, const config_t &c) : FLSStrategy(m, c) {
    crpd = new CRPD();
}

uint32_t FLSSinglePhase::compute_concurrency(Schedule *, SchedPacket *p) {
    p->concurrency(0);
    return 0;
}

uint64_t FLSSinglePhase::find_best_start_read(const Schedule &sched, SchedPacket *t) {
    uint64_t bestStart = t->min_rt_period();
    for(SchedElt * e : t->previous()) {
        if(e->rt() + e->wct() > bestStart)
            bestStart = e->rt() + e->wct();
    }
    return bestStart;
}

uint64_t FLSSinglePhase::find_best_start_exec(const Schedule &sched, SchedJob *t, SchedCore* c) {
    bool change = true;

//    uint64_t bestStart = t->get_end_last_read();
    uint64_t bestStart = 0;
    if (bestStart < t->min_rt_period()) {
        bestStart = t->min_rt_period();
    }
    // get predecessor end
    // TODO check if multi phase grabs the right predecessor end
    for (SchedElt *e : t->previous()) {
        if (e->rt() + e->wct() > bestStart)
            bestStart = e->rt() + e->wct();
    }

    // end time of this phase
    uint64_t endtime = bestStart + t->wct();

    // while any changes were made to the start time
    while (change) {
        change = false;
        // check if there is any overlap on the current core
        for (SchedElt *elt : sched.scheduled_elements()) {
            if (elt == t) continue;
            if (sched.get_mapping(elt)->core()->id != c->core()->id) continue;

            uint64_t eRtE = elt->rt();
            uint64_t eEndTime = eRtE + elt->wct();
            if (eRtE < endtime && bestStart < eEndTime) {
                bestStart = eEndTime;
                endtime = bestStart + t->wct();
                change = true;
            }
        }
        for (SchedElt *elt : sched.scheduled_elements()) {
            if (elt == t) continue;
            for (Phase *p: t->task()->versions(t->selected_version())->phases()){
                // if the type is the same as the current candidate we continue
                if ((*p->force_mapping_proc().begin())->type == c->core()->type) continue;
                // if the candidate core and the previously scheduled element are the same we continue
                if (sched.get_mapping(elt)->core()->id == c->core()->id) continue;
                // if the previously scheduled type and the phase type are the not the same we continue
                if (sched.get_mapping(elt)->core()->type != c->core()->type) continue;


                uint64_t eRtE = elt->rt();
                uint64_t eEndTime = eRtE + elt->wct();
                if (eRtE < endtime && bestStart < eEndTime) {
                    bestStart = eEndTime;
                    endtime = bestStart + t->wct();
                    change = true;
                }
            }
        }
    }

    return bestStart;
}

void FLSSinglePhase::visitSchedEltPacketRead(Schedule *sched, SchedPacket *p) {
    p->rt(0);
    p->wct(0);
}
void FLSSinglePhase::visitSchedEltPacketWrite(Schedule *sched, SchedPacket *pac) {
    pac->rt(0);
    pac->rt(pac->schedtask()->rt() + pac->schedtask()->wct());
}

void FLSSinglePhase::visitSchedEltTask(Schedule *sched, SchedJob *t) {
// TODO: For now this assumes that any multi-phase program uses the GPU and ONE CPU core!
    uint128_t bestScheduleTime = -1;
    Schedule bestSchedule(*sched);
    bestSchedule.status(ScheduleProperties::sched_statue_e::SCHED_PARTIAL);

    for(SchedCore *candidate : sched->schedcores()) {
        if(find_if(forbidden_proc_mapping[t->task()].begin(), forbidden_proc_mapping[t->task()].end(), [candidate](SchedCore *a) {return a->core() == candidate->core();}) != forbidden_proc_mapping[t->task()].end()) {
            Utils::DEBUG("\tcandidate "+*candidate+" unusable due to previous unschedulability issue");
            continue;
        }
        // loop overall versions of a task
        for(Version *t_version: t->task()->versions()){

            if (t_version->phases().size() > 1){
                timinginfos_t WCET = 0;
                energycons_t WCEE = 0;
                for(Phase *p: t_version->phases()){
                    WCET += p->C();
                    WCEE += p->C_E();
                }
                t_version->C(WCET);
                t_version->C_E(WCEE);
            }else{
                t_version->C(t_version->phases()[0]->C());
                t_version->C_E(t_version->phases()[0]->C_E());
            }

            // if version proc and current proc dont match -> continue
            if((*t_version->force_mapping_proc().begin())->type != candidate->core()->type){
                continue;
            }

            Utils::DEBUG("\tcandidate "+*candidate);

            Schedule schedule(*sched);
            SchedJob *ct = schedule.schedjob(t->id());
            ct->selected_version(t_version->id());
            ct->wct(t_version->C());

            for(SchedPacket *p : ct->packet_read()) {
                p->rt(find_best_start_read(schedule, p));
            }
            uint64_t rt = find_best_start_exec(schedule, ct, candidate);
            ct->rt(rt);
            schedule.map_core(ct, schedule.schedcore(candidate->core()));
            schedule.schedule(ct);

            for(Phase *p: t_version->phases()){
                // if the processor required for the new phase does not equal the processor of the first phase we add new jobs and reserve the right cores.
                if ((*p->force_mapping_proc().begin())->type == (*t_version->force_mapping_proc().begin())->type ) continue;
                SchedJob *new_ct = ScheduleFactory::build_job(&schedule, ct->task(), rt);
                new_ct->wct(t_version->C());
                new_ct->selected_version(t_version->id());
                new_ct->min_rt_period(ct->min_rt_period()+1);
                schedule.map_core(new_ct, schedule.schedcore((*p->force_mapping_proc().begin())));
                schedule.schedule(new_ct);
            }

            Utils::DEBUG("\t\t synchronized: "+*ct);

            schedule.compute_makespan();
            schedule.compute_energy();

            if(bestScheduleTime > schedule.makespan()) {
                Utils::DEBUG("\t\tbest candidate makespan -- "+to_string(schedule.makespan())+" ("+to_string(bestScheduleTime)+")");

                // update task wct and wce for current versions
                SchedJob * bestScheduleJob = bestSchedule.schedjob(t->id());
                bestScheduleJob->rt(rt);
                bestScheduleJob->wct(t_version->C());
                bestScheduleJob->wce(t_version->C_E());

                t->rt(rt);
                t->wct(t_version->C());
                t->wce(t_version->C_E());
                t->selected_version(t_version->id());

                bestSchedule = schedule;
                bestSchedule.status(ScheduleProperties::sched_statue_e::SCHED_COMPLETE);
                bestScheduleTime = schedule.makespan();
            }
            else
                Utils::DEBUG("\t\t\t\tbad candidate makespan -- "+to_string(schedule.makespan())+" ("+to_string(bestScheduleTime)+")");
        }
    }

    *sched = bestSchedule;
}

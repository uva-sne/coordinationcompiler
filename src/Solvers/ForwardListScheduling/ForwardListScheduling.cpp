/*!
 * \file ForwardListScheduling.hpp
 * \copyright GNU Public License v3.
 * \date 2017
 * \author Benjamin Rouxel <benjamin.rouxel@uva.nl>, University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "Utils/Log.hpp"
#include "ForwardListScheduling.hpp"
#include "ScheduleFactory.hpp"
#include <boost/range/adaptor/reversed.hpp>
//!\todo: find good way to switch between makespan and energy optimisation - currrently in Solver.h as energy_minimisation
using namespace std;

IMPLEMENT_HELP_WITH_CODE(ForwardListScheduling, {
    std::string types = "";
    for(FLSStrategyRegistry::key_t t : FLSStrategyRegistry::keys())
        types += ((std::string)t)+", ";
    types = types.substr(0, types.size()-2);

    msg = string("Forward List Scheduler/Solver:\n") +
            "type ["+types+"]: type of scheduling strategy\n\n"+
            Solver::help();
}, Forward List Scheduler/Solver: \n
   HTMLINDENT- type [@see Solver]: type of scheduling strategy\n)

string ForwardListScheduling::more_help(const std::string &key) {
    if(FLSStrategyRegistry::IsRegistered(key))
        return FLSStrategyRegistry::Create(key, *tg, *conf)->help();
    return "Unknown FLS scheduling policy strategy `"+key+"'\n";
}

void ForwardListScheduling::presolve(Schedule *sched) {
    if(type().empty())
        throw CompilerException("fls", "You need to precise what type of solver you want, (check doc?)");

    for (ComputeUnit *p : tg->processors())
        ScheduleFactory::add_core(sched, p);
    ScheduleFactory::build_elements(sched);

    strategy = FLSStrategyRegistry::Create(type(), *tg, *conf);
}

void ForwardListScheduling::build_rank(const SystemModel *tg, const config_t *conf, Schedule * schedule, vector<SchedElt*> *schedorder){
    map<SchedElt*, timinginfos_t> elt_rank;
    vector<pair<SchedElt*, timinginfos_t> > sorted_elt_rank;

    for (SchedEltPtr &e: boost::adaptors::reverse(schedule->elements())){

        if (e->type() == SchedElt::PACKET) continue;

        SchedJob * elt = (SchedJob*) e.get();

        timinginfos_t rank = 0;
        timinginfos_t version_average_C = 0;
        timinginfos_t max_succ_rank = 0;

        for (Version *vers: elt->task()->versions()){
            for (Phase * ph: vers->phases()){
                version_average_C += ph->C();
            }
        }
        version_average_C /= elt->task()->versions().size();

        for (SchedElt *succ :elt->successors()){
            max_succ_rank = max(elt_rank.at(succ), max_succ_rank);
        }

        rank = version_average_C + max_succ_rank;
        elt_rank[elt] = rank;
    }

    sorted_elt_rank.reserve(elt_rank.size());
    for (pair<SchedElt*, timinginfos_t> p: elt_rank){
        sorted_elt_rank.push_back(p);
    }

    sort(sorted_elt_rank.begin(), sorted_elt_rank.end(),[](pair<SchedElt*, timinginfos_t> &left, pair<SchedElt*, timinginfos_t> &right){return left.second > right.second;} );

    for (pair<SchedElt*, timinginfos_t> p: sorted_elt_rank){
        schedorder->push_back(p.first);
    }

}

void ForwardListScheduling::solve(Schedule *sched) {
    vector<Schedule*> schedules;
    Schedule schedDFSdelay(*sched), schedDFS(*sched), schedBFS(*sched), schedLaxity(*sched), schedEnergyLaxity(*sched);
    
    string error = "";
    // BFS is better on test_complex and audiobeam
    // burst: edge is better for audiobeam
    // burst: packet is better for test_complex
    // BFS has a better gain on StreamIT
    // DFSdelay, with STG (heur vs ILP), has a wider standard deviation with a smaller min degradation and a bigger max degradation. But the avg degradation is smaller than BFS
    try {
        vector<SchedElt*> schedorder;
        FLSFragQreadyBuilder::buildListDFSWithRDelay(*tg,*conf, strategy, sched, &schedorder);
        do_run(&schedDFSdelay, schedorder);
        schedDFSdelay.compute_makespan();
        schedDFSdelay.compute_energy();
        Utils::DEBUG("\t====> Schedule length DFSDelay (" + ScheduleProperties::to_string(schedDFSdelay.status()) + "): makespan=" + to_string(schedDFSdelay.makespan())+", energy="+to_string(schedDFSdelay.energy()));    
        sched->stats("DFSDelay makespan", to_string(schedDFSdelay.makespan()));
        sched->stats("DFSDelay energy", to_string(schedDFSdelay.energy()));
        schedules.push_back(&schedDFSdelay);
    }
    catch(Unschedulable &e){
        error += "DFS with delay: "+e.str()+"\n";
        Utils::DEBUG("\t====> Schedule length DFSDelay: Unschedulable ");    
        sched->stats("DFSDelay makespan", "0");
        sched->stats("DFSDelay energy", "0");
    }


    try {
        vector<SchedElt*> schedorder;
        FLSFragQreadyBuilder::buildListDFS(*tg,*conf, strategy, sched, &schedorder);
        do_run(&schedDFS, schedorder);
        schedDFS.compute_makespan();
        schedDFS.compute_energy();
        Utils::DEBUG("\t====> Schedule length DFS (" + ScheduleProperties::to_string(schedDFS.status()) + "): makespan=" + to_string(schedDFS.makespan())+", energy="+to_string(schedDFS.energy()));
        sched->stats("DFS makespan", to_string(schedDFS.makespan()));
        sched->stats("DFS energy", to_string(schedDFS.energy()));
        schedules.push_back(&schedDFS);
    }
    catch(Unschedulable &e){
        error += "DFS: "+e.str()+"\n";
        Utils::DEBUG("\t====> Schedule length DFS: Unschedulable ");    
        sched->stats("DFS makespan", "0");
        sched->stats("DFS energy", "0");
    }

    try {
        vector<SchedElt*> schedorder;
        FLSFragQreadyBuilder::buildListBFS(*tg,*conf, strategy, sched, &schedorder);
        do_run(&schedBFS, schedorder);
        schedBFS.compute_makespan();
        schedBFS.compute_energy();
        Utils::DEBUG("\t====> Schedule length BFS (" + ScheduleProperties::to_string(schedBFS.status()) + "): makespan=" + to_string(schedBFS.makespan())+", energy="+to_string(schedBFS.energy()));
        sched->stats("BFS makespan", to_string(schedBFS.makespan()));
        sched->stats("DFS energy", to_string(schedBFS.energy()));
        schedules.push_back(&schedBFS);
    }
    catch(Unschedulable &e){
        error += "BFS: "+e.str()+"\n";
        Utils::DEBUG("\t====> Schedule length BFS: Unschedulable ");    
        sched->stats("BFS makespan", "0");
        sched->stats("BFS energy", "0");
    }

    try {
        vector<SchedElt*> schedorder;
        FLSFragQreadyBuilder::buildListLaxity(*tg,*conf, strategy, sched, &schedorder);
        do_run(&schedLaxity, schedorder);
        schedLaxity.compute_makespan();
        schedLaxity.compute_energy();
        Utils::DEBUG("\t====> Schedule length Laxity (" + ScheduleProperties::to_string(schedLaxity.status()) + "): makespan=" + to_string(schedLaxity.makespan())+", energy="+to_string(schedLaxity.energy()));
        sched->stats("Laxity makespan", to_string(schedLaxity.makespan()));
        sched->stats("Laxity energy", to_string(schedLaxity.energy()));
        schedules.push_back(&schedLaxity);
    }
    catch(Unschedulable &e){
        error += "Laxity: "+e.str()+"\n";
        Utils::DEBUG("\t====> Schedule length Laxity: Unschedulable ");    
        sched->stats("Laxity makespan", "0");
        sched->stats("Laxity energy", "0");
    }

    //TODO: Cannot do an energy laxity sorting if there is no energy info.
//    try {
//        vector<SchedElt*> schedorder;
//        FLSFragQreadyBuilder::buildListEnergyLaxity(*tg,*conf, strategy, sched->elements(), &schedorder);
//        do_run(&schedEnergyLaxity, schedorder);
//        schedEnergyLaxity.compute_makespan();
//        schedEnergyLaxity.compute_energy();
//        Utils::INFO("\t====> Schedule energy Laxity (" + ScheduleProperties::to_string(schedEnergyLaxity.status()) + "): makespan=" + to_string(schedEnergyLaxity.makespan())+", energy="+to_string(schedEnergyLaxity.energy()));
//        sched->stats("EnergyLaxity energy", to_string(schedEnergyLaxity.energy()));
//        sched->stats("EnergyLaxity makespan", to_string(schedEnergyLaxity.makespan()));
//        schedules.push_back(&schedEnergyLaxity);
//    }
//    catch(Unschedulable &e){
//        error += "Energy Laxity: "+e.str()+"\n";
//    }


    sched->status(ScheduleProperties::sched_statue_e::SCHED_UNSCHEDULABLE);
    sched->makespan((uint128_t)-1);
    sched->energy((uint128_t)-1);
    
    if(schedules.size() > 0) {
        *sched = *get_best(schedules);
        sched->preemption_mode(ScheduleProperties::PreemptionModel::PRE_NON);
        sched->mapping_mode(ScheduleProperties::Mapping::MAP_PARTITIONED);
        sched->priority_mode(ScheduleProperties::PriorityScheme_e::PRIO_FIXED);
    }

    clear_memento();
    
    Utils::DEBUG(""+*sched);
    Utils::INFO("\t====> Final Schedule length = "+to_string(sched->makespan()) + tg->time_unit());
    Utils::INFO("\t====> Final Energy Consumption = " + to_string(sched->energy()) + tg->energy_unit());

    if(sched->status() == ScheduleProperties::sched_statue_e::SCHED_UNSCHEDULABLE)
        throw Unschedulable(error);
    if(!error.empty())
        Utils::WARN(error);
}

Schedule *ForwardListScheduling::get_best(const vector<Schedule*> &s) {
    auto func = (target() == SolverProperties::SolverTargets::ENERGY)
        ? [](Schedule *a, Schedule *b) { return a->energy() <= b->energy();}
        : [](Schedule *a, Schedule *b) { return a->makespan() <= b->makespan();};
        
    Schedule *best = s[0];
    for(size_t i=1 ; i < s.size() ; ++i) {
        if(func(s[i], best))
            best = s[i];
    }
    return best;
}

void ForwardListScheduling::do_run(Schedule *sched, vector<SchedElt*> &schedorder) {
    chrono::duration<double> running_time = chrono::high_resolution_clock::now() - start_time;
    if(timeout() > 0 && timeout() <= (timinginfos_t)running_time.count()) {
        sched->status(ScheduleProperties::sched_statue_e::SCHED_PARTIAL); // Time-out already reached
        return;
    }

    clear_memento();
    Utils::DEBUG("/***************** Start Mapping ********************/");
    while(!schedorder.empty()) {
        // get next element
        SchedElt *t = *(schedorder.begin());
        assert(sched->elements_lut().count(t->id()) == 1); // schedorder must only contain elements in the Schedule

        // store recovering point
        memento[t].schedule = new Schedule(*sched);
        memento[t].schedorder = schedorder;
        memento[t].forbid_proc = strategy->forbidden_proc_mapping;

        // consume the element to schedule
        schedorder.erase(schedorder.begin());

        Utils::memory_usage("-> Sched "+t->toString());
        Schedule copy(*sched);
        SchedElt *copy_elt = copy.elements_lut().at(t->id()).get();

        try {
            // do the mapping/scheduling
            copy.schedule(copy_elt);
            copy_elt->accept(strategy, &copy);
            // Need to get a new ref as copy is changed in the FLS scheduler!
            // Thus, the old "copy" is deleted and "copy_elt" is rightfully deleted as well during the deconstruction of copy!
            // This is an actual improvement and fixes memory leaks.
            copy_elt = copy.elements_lut().at(t->id()).get();
            
            // memory allocation
            if(conf->archi.spm.assign_region) {
                if(Utils::isa<SchedJob*>(copy_elt)) {
                    strategy->map_to_spm_region(&copy, copy_elt);
                    for(SchedPacket *p : copy_elt->schedtask()->packet_read()) {
                        strategy->map_to_spm_region(&copy, p);
                    }
                }
                else if(((SchedPacket*)copy_elt)->dirlbl() == "w")
                    strategy->map_to_spm_region(&copy, copy_elt);
            }

            copy.compute_makespan();

            if(copy_elt->min_rt_deadline() > 0 && copy_elt->rt()+copy_elt->wct() > copy_elt->min_rt_deadline())
                throw Unschedulable("Element "+copy_elt->toString()+" missed its deadline -- "+to_string(copy_elt->rt()+copy_elt->wct())+" > "+to_string(copy_elt->min_rt_deadline()));


            if(!allow_migration) {
                SchedJob *concerned_task = copy_elt->schedtask();
                SchedCore *mapping = copy.get_mapping(concerned_task);
                if(mapping != nullptr) {
                    for(SchedCore *c : sched->schedcores()) { //warning, this is core from sched that we want to put in the forbidden list, not from copy
                        if(c->core() != mapping->core()) {
                            Utils::DEBUG("No migration, add "+*c+" to forbid list for "+t->toString());
                            strategy->forbidden_proc_mapping[concerned_task->task()].insert(c);
                        }
                    }
                }
            }
            
            // adds any sort of delays to packages in the schedorder if the delays werent propagated
            if (!sched->delay_impacted_elements().empty()){
                for (SchedElt * in_schedorder: schedorder){
                    for (pair<SchedEltPtr, SchedElt*>& old_new: sched->delay_impacted_elements()){
                        if(in_schedorder->schedtask()->equals(old_new.first.get())){
                            in_schedorder->schedtask()->rt(old_new.second->rt());
                            in_schedorder->schedtask()->wct(old_new.second->wct());
                        }
                    }
                }
            }

            // Check memory usage
            if((Utils::get_ram_usage()/Utils::get_total_ram())*100 > 90 || Utils::get_swap_usage() > 0)
                throw CompilerException("scheduler", "Host out-of-memory -- abording");
            // Check running time
            chrono::duration<double> running_time = chrono::high_resolution_clock::now() - start_time;
            if(timeout() > 0 && timeout() <=  running_time.count()) {
                sched->status(ScheduleProperties::sched_statue_e::SCHED_PARTIAL);
                Utils::WARN("Time Out!");
                break;
            }

            // empty forbidden so that next job can still be scheduled
            strategy->forbidden_proc_mapping.clear();

            *sched = copy; // this will deallocate 't'
            // Check memory usage
            Utils::DEBUG(""+*sched);
        }
        catch(Unschedulable &e) {
            Utils::DEBUG(e.what());
            // as for now, this exception is raised only when there is no more space on SPM
            // mapping to SPM happens only when the whole task is scheduled (packets+exec)

            // Grab the task we were scheduling (well, the exec phase as it is the one that is mapped on a core)
            SchedJob *concerned_task = t->schedtask();

            // Return to a stable known state
            *sched = *(memento[concerned_task].schedule);
            delete memento[concerned_task].schedule;
            memento[concerned_task].schedule = nullptr;
            schedorder = memento[concerned_task].schedorder;
            strategy->forbidden_proc_mapping = memento[concerned_task].forbid_proc;

            // Add the last tested core to the forbidden list to not test it again
            SchedCore *concerned_core = sched->schedcore(copy.get_mapping(copy_elt)->core());
            strategy->forbidden_proc_mapping[concerned_task->task()].insert(concerned_core);
            Utils::DEBUG("=====================> add "+*concerned_core+" to forbid list for "+concerned_task->toString());

            size_t possible_core_count = concerned_task->task()->force_mapping_proc().size();
            if(possible_core_count == 0)
                possible_core_count = sched->schedcores().size();

            // Check if we tried to map the task on every core, if so raise exception
            if(strategy->forbidden_proc_mapping[concerned_task->task()].size() == possible_core_count) {
                clear_memento();
                sched->status(ScheduleProperties::sched_statue_e::SCHED_UNSCHEDULABLE);
                // we tried every processor, and on all of them we end up in an unschedulable state, so be it
                throw Unschedulable("Can't find a valid schedule on any core for elt: "+t->toString());
            }
        }
    }

    clear_memento();

    Utils::memory_usage("***************** Done *******************");

    if(sched->status() == ScheduleProperties::sched_statue_e::SCHED_UNSCHEDULABLE)
        return;

    sched->compute_makespan();
    sched->compute_energy();
}

void ForwardListScheduling::clear_memento() {
    for(pair<SchedElt*, memento_sched_state> el : memento) {
        if(el.second.schedule != nullptr)
            delete el.second.schedule;
    }
    memento.clear();
    strategy->forbidden_proc_mapping.clear();
}

void FLSStrategy::map_to_spm_region(Schedule* sched, SchedElt* current) {
    if(!conf.archi.spm.assign_region) return;
    if(current->mem_data() == 0) return;

    //Maybe TODO: if "Allocation XY overlap" arise, then maybe the phase Y should go in an other region,

    uint64_t startresa = 0; // resident exec region !conf->archi.prefetch_code && conf->archi.spm.store_code
    uint64_t endresa = -1;
    string suffix = "";
    if(Utils::isa<SchedJob*>(current)) {
        suffix = "E";
        if(!conf.archi.spm.prefetch_code && conf.archi.spm.store_code) { // code resident in mem
            startresa = 0;
            endresa = -1;
        }
        else if(conf.archi.spm.prefetch_code && conf.archi.spm.store_code) { // prefetch code
            startresa = ((SchedJob*)current)->get_release_first_read();
            endresa = current->rt() + current->wct();
        }
        else { // only local data
            startresa = current->rt();
            endresa = current->rt() + current->wct();
        }
    }
    else {
        SchedPacket *p = (SchedPacket*)current;
        if(p->dirlbl() == "r") {
            suffix = "R";
            startresa = p->rt();
            endresa = p->schedtask()->rt() + p->schedtask()->wct();

            if(conf.archi.interconnect.mechanism == "shared") {
                SchedJob *prev = p->tofromschedtask();
                if(p->schedtask() != prev) { // are eq when no burst
                    if(sched->get_mapping(p->schedtask()) == sched->get_mapping(prev)) {
                        auto it = find_if(p->previous().begin(), p->previous().end(), [prev](SchedElt *c) {
                            return c->schedtask() == prev && Utils::isa<SchedPacket*>(c) && ((SchedPacket*) c)->dirlbl() == "w";
                        });
                        assert(it != p->previous().end());
                        SchedPacket *prevpac = (SchedPacket*)(*it);
                        SchedSpmRegion *sr = sched->get_spm_mapping(prevpac);
                        assert(sr != nullptr);
                        if(sr->is_booked(prevpac->rt()+prevpac->wct(), endresa)) {
                            throw UnschedulableSPMSpace(current->schedtask()->task(), "Optimisation failed "+sched->get_mapping(current)->core()->id+
                                +" "+sr->label()+" for element "+current->toString()+" and "+prevpac->toString()
                            );
                        }
                        sched->map_spm(p, sr, prevpac->rt()+prevpac->wct(), endresa);
                        Utils::DEBUG("Mapping opti "+current->toString()+" on SPM "+sr->label()+", core "+sched->get_mapping(current)->core()->id+", size="+to_string(sr->size())+", start="+to_string(startresa)+", end="+to_string(endresa));
                        return;
                    }
                }
            }
        }
        else {
            suffix = "W";
            startresa = p->schedtask()->rt();
            endresa = p->rt()+p->wct();
        }
    }
    SchedCore *core = sched->get_mapping(current);
    SchedSpmRegion *sr = core->get_available_spmregion(startresa, endresa);
    if(sr != nullptr &&
      (sr->size() >= current->mem_data() || core->available_spm_size() >= current->mem_data()-sr->size())) {
        sched->map_spm(current, sr, startresa, endresa);
        Utils::DEBUG("Reuse reg - Mapping "+current->toString()+" on SPM "+sr->label()+", core "+core->core()->id+", size="+to_string(sr->size())+", start="+to_string(startresa)+", end="+to_string(endresa));
        return;
    }

    if(core->available_spm_size() >= current->mem_data()) {
        SchedSpmRegion *sr = core->add_spmregion("MR"+suffix+current->id(), current->mem_data());
        sched->map_spm(current, sr, startresa, endresa);
        Utils::DEBUG("New reg - Mapping "+current->toString()+" on new SPM "+sr->label()+", core "+core->core()->id+", size="+to_string(sr->size())+", start="+to_string(startresa)+", end="+to_string(endresa));
        return;
    }

    throw UnschedulableSPMSpace(current->schedtask()->task(), "No spm space on proc "+sched->get_mapping(current)->core()->id+
        "("+to_string(core->available_spm_size())+")"+" for element "+current->toString()
    );
}

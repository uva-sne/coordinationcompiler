/*!
 * \file energy.hpp
 * \copyright GNU Public License v3.
 * \date 2017 
 * \author Benjamin Rouxel <benjamin.rouxel@inria.fr>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef IDEAL_H
#define IDEAL_H

#include "ForwardListScheduling.hpp"

/**
 * \brief Generate the schedule of an application
 * 
 * Communication between tasks are assumed to be ideal, meaning that there is 
 * never an interference
 * 
 */
class FLSIdeal : public FLSStrategy {
public:
    //! \copydoc FLSStrategy::FLSStrategy
    explicit FLSIdeal(const SystemModel &m, const config_t &c);
    
    virtual std::string help() override;
    
    virtual uint32_t compute_concurrency(Schedule *sched, SchedPacket *t) override;
    /**
     * Find the best start time for a read packet
     * @param sched
     * @param t
     * @return time
     */
    uint64_t find_best_start_read(const Schedule &sched, SchedPacket *t);
    /**
     * Find the best start time for an exec phase
     * @param sched
     * @param t
     * @return time
     */
    uint64_t find_best_start_exec(const Schedule &sched, SchedJob *t);
    
    //! \copydoc SchedEltVisitor::visitSchedEltPacketRead
    void visitSchedEltPacketRead(Schedule *sched, SchedPacket *p);
    //! \copydoc SchedEltVisitor::visitSchedEltPacketWrite
    void visitSchedEltPacketWrite(Schedule *sched, SchedPacket *p);
    //! \copydoc SchedEltVisitor::visitSchedEltTask
    void visitSchedEltTask(Schedule *sched, SchedJob *t);
};

REGISTER_FLSSTRATEGY(FLSIdeal, "ideal")

#endif
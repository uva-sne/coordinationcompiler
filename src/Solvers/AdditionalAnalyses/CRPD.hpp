/*
 * Copyright (C) 2021 brouxel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CRPD_HPP
#define CRPD_HPP

#include "Utils.hpp"
#include "SystemModel.hpp"
#include "Schedule.hpp"
#include "config.hpp"

class CRPD {
public:
    CRPD();
    CRPD(const CRPD& orig);
    virtual ~CRPD();
    
    timinginfos_t get_CRPD(timinginfos_t t);
private:

};

#endif /* CRPD_HPP */


/*!
 * \file RegistryEntries.hpp
 * \copyright GNU Public License v3.
 * \date 2020
 * \author Stephen Nicholas Swatman <s.n.swatman@uva.nl> University of Amsterdam
 * \author Benjamin Rouxel <benjamin.rouxel@uva.nl>, University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "ResetSchedule.hpp"

#include "ILP/Solvers/Cplex.hpp"
#include "ILP/Solvers/LPSolve.hpp"
#include "ILP/Solvers/GLPK.hpp"

#include "ILP/Strategies/ideal.hpp"
#include "ILP/Strategies/worst_concurrency.hpp"
#include "ILP/Strategies/heterogeneous.hpp"
#include "ILP/Strategies/CommContentionAware.hpp"
#include "ILP/Strategies/CommSynchronized.hpp"
#include "ILP/Strategies/energy_aware_DVFS.h"

#include "ForwardListScheduling/DVFS_eFLS.h"
#include "ForwardListScheduling/DVFS_FLS.h"
#include "ForwardListScheduling/ideal.hpp"
#include "ForwardListScheduling/worst_concurrency.hpp"
#include "ForwardListScheduling/contention_aware.hpp"
#include "ForwardListScheduling/synchronized.hpp"
#include "ForwardListScheduling/FLSMultiPhase.hpp"
#include "ForwardListScheduling/FLSSinglePhase.hpp"


#include "FastFLS/FFLS.hpp"
#include "eFFLS/eFFLS.hpp"

#include "SchedulabilityAnalysis/MicaelaVerucchiSchedAnalyses.hpp"
#include "SchedulabilityAnalysis/AutoSelectSchedAnalysis.hpp"

#include "AdditionalAnalyses/CRPD.hpp"

#include "EvolutionaryAlgorithms/ARSH_FATI.h"
#include "EvolutionaryAlgorithms/LIST_EA.h"

#include "HEFT/HEFT.hpp"
#include "HEFT/HER.hpp"
#include "HEFT/simple_heft.hpp"
#include "HEFT/simple_heft_energy.hpp"
#include "HEFT/simple_her.hpp"

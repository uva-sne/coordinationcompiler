# Copyright (C) 2020  Stephen Nicholas Swatman <s.n.swatman@uva.nl> 
#                     Univeristy of Amsterdam
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Use pkg-config to get hints about paths
pkg_check_modules(LIBXMLPP_PKGCONF REQUIRED libxml++-2.6)

# Finally the library itself
find_library(LIBXMLPP_LIBRARY
  NAMES xml++ xml++-2.6
  PATHS ${LIBXMLPP_PKGCONF_LIBRARY_DIRS}
)

FIND_PACKAGE_HANDLE_STANDARD_ARGS(
  libxml++ 
  DEFAULT_MSG 
  LIBXMLPP_PKGCONF_INCLUDE_DIRS
  LIBXMLPP_LIBRARY 
)

if(libxml++_FOUND)
  add_library(LibXML::libxmlpp STATIC IMPORTED)
  set_target_properties(
    LibXML::libxmlpp
    PROPERTIES 
    IMPORTED_LOCATION "${LIBXMLPP_LIBRARY}"
    INTERFACE_INCLUDE_DIRECTORIES "${LIBXMLPP_PKGCONF_INCLUDE_DIRS}"
    INTERFACE_LINK_LIBRARIES "${LIBXMLPP_PKGCONF_LIBRARIES}"
  )
endif()

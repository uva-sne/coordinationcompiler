# Copyright (C) 2020  Stephen Nicholas Swatman <s.n.swatman@uva.nl>
#                     Univeristy of Amsterdam
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


set(
  LIBLPSOLVE_INCLUDE_TRIAL_PATH
  /usr/include
  /usr/local/include
  /opt/include
  /opt/local/include
)

find_path(
  LIBLPSOLVE_INCLUDE_PATH
  lpsolve/lp_lib.h
  ${LPSOLVE_INCLUDE_TRIAL_PATH}
)

find_library(
  LIBLPSOLVE_LIBRARY
  NAMES lpsolve55
)

find_package_handle_standard_args(
  lpsolve
  DEFAULT_MSG
  LIBLPSOLVE_INCLUDE_PATH
  LIBLPSOLVE_LIBRARY
)

if(lpsolve_FOUND)
  add_library(lpsolve STATIC IMPORTED)
  set_target_properties(
    lpsolve
    PROPERTIES
    IMPORTED_LOCATION "${LIBLPSOLVE_LIBRARY}"
    INTERFACE_INCLUDE_DIRECTORIES "${LIBLPSOLVE_INCLUDE_PATH}"
    INTERFACE_LINK_LIBRARIES "${LIBLPSOLVE_LIBRARY}"
  )
endif()

# Copyright (C) 2020  Stephen Nicholas Swatman <s.n.swatman@uva.nl> 
#                     Benjamin Rouxel <benjamin.rouxel@uva.nl>
#                     Univeristy of Amsterdam
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Include dir
find_path(LIBANTLR4_RUNTIME_INCLUDE_DIR
  NAMES antlr4-runtime/antlr4-runtime.h
)

find_program(ANTLR_PROG antlr4)
if (NOT ANTLR_PROG)
    message(FATAL_ERROR "Antlr4 not found!")
endif()
execute_process(COMMAND bash -c "antlr4 | grep Version | cut -d' ' -f6" |  OUTPUT_VARIABLE ANTLR_VERSION)
if(${ANTLR_VERSION} STREQUAL "")
    message(FATAL_ERROR "Can't find antlr4 version")
endif()

# Finally the library itself
find_library(LIBANTLR4_RUNTIME_LIBRARY
  NAMES
  libantlr4-runtime.a
  antlr4-runtime
  LIBANTLR4_RUNTIME
)

if(LIBANTLR4_RUNTIME_LIBRARY)
    if(${CMAKE_VERSION} VERSION_LESS "3.17.0")
        set(PACKAGE_NAME LIBANTLR4_RUNTIME)
    else()
        set(PACKAGE_NAME antlr4-runtime)
    endif()
    message("-- find_library found ${PACKAGE_NAME}")
    FIND_PACKAGE_HANDLE_STANDARD_ARGS(
      ${PACKAGE_NAME}
      FOUND_VAR ${PACKAGE_NAME}_FOUND
      REQUIRED_VARS LIBANTLR4_RUNTIME_LIBRARY LIBANTLR4_RUNTIME_INCLUDE_DIR
    )
endif()

add_library(ANTLR4::cpp_runtime STATIC IMPORTED)
set_target_properties(
  ANTLR4::cpp_runtime 
  PROPERTIES 
  IMPORTED_LOCATION ${LIBANTLR4_RUNTIME_LIBRARY}
  INTERFACE_INCLUDE_DIRECTORIES ${LIBANTLR4_RUNTIME_INCLUDE_DIR}/antlr4-runtime
)

unset(PACKAGE_NAME)

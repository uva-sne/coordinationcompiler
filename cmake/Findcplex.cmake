# Copyright (C) 2020  Stephen Nicholas Swatman <s.n.swatman@uva.nl> 
#                     Univeristy of Amsterdam
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

include(FindPackageHandleStandardArgs)

find_path(CPLEX_ROOT_DIR
  NAMES 
  cplex/include/ilcplex/ilocplex.h 
  cplex/include/ilcplex/cplex.h
  concert/include/ilconcert/ilotupleset.h
  concert/include/ilconcert/ilosys.h
  PATHS 
  ${CECILE_CPLEX_ROOT} 
  /opt/ibm/ILOG/CPLEX_Studio1271
)

file(GLOB CPLEX_LIBCONCERT_PATHS "${CPLEX_ROOT_DIR}/concert/lib/*/static_pic/")
file(GLOB CPLEX_LIBCPOPTIMIZER_PATHS "${CPLEX_ROOT_DIR}/cpoptimizer/lib/*/static_pic/")
file(GLOB CPLEX_LIBCPLEX_PATHS "${CPLEX_ROOT_DIR}/cplex/lib/*/static_pic/")

find_library(CPLEX_LIBCONCERT NAMES concert PATHS ${CPLEX_LIBCONCERT_PATHS})
find_library(CPLEX_LIBCPOPTIMIZER NAMES cp PATHS ${CPLEX_LIBCPOPTIMIZER_PATHS})
find_library(CPLEX_LIBCPLEX NAMES cplex PATHS ${CPLEX_LIBCPLEX_PATHS})
find_library(CPLEX_LIBILOCPLEX NAMES ilocplex PATHS ${CPLEX_LIBCPLEX_PATHS})

FIND_PACKAGE_HANDLE_STANDARD_ARGS(
  cplex
  "Could NOT find CPLEX -- please check or provide -DCECILE_CPLEX_ROOT=/path/to/cplex/install" 
  CPLEX_ROOT_DIR
  CPLEX_LIBCONCERT
  CPLEX_LIBCPOPTIMIZER
  CPLEX_LIBCPLEX
  CPLEX_LIBILOCPLEX
)

if(cplex_FOUND)
  add_library(CPLEX::cplex STATIC IMPORTED)
  set_target_properties(
    CPLEX::cplex 
    PROPERTIES 
    IMPORTED_LOCATION "${CPLEX_LIBCPLEX}"
    INTERFACE_INCLUDE_DIRECTORIES "${CPLEX_ROOT_DIR}/cplex/include/"
    INTERFACE_LINK_LIBRARIES dl
  )
  
  add_library(CPLEX::concert STATIC IMPORTED)
  set_target_properties(
    CPLEX::concert 
    PROPERTIES 
    IMPORTED_LOCATION "${CPLEX_LIBCONCERT}"
    INTERFACE_INCLUDE_DIRECTORIES "${CPLEX_ROOT_DIR}/concert/include/"
    INTERFACE_LINK_LIBRARIES CPLEX::cplex
  )
  
  add_library(CPLEX::cpoptimizer STATIC IMPORTED)
  set_target_properties(
    CPLEX::cpoptimizer 
    PROPERTIES 
    IMPORTED_LOCATION ${CPLEX_LIBCPOPTIMIZER}
    INTERFACE_LINK_LIBRARIES CPLEX::cplex
  )
  
  add_library(CPLEX::ilocplex STATIC IMPORTED)
  set_target_properties(
    CPLEX::ilocplex 
    PROPERTIES 
    IMPORTED_LOCATION ${CPLEX_LIBILOCPLEX}
    INTERFACE_LINK_LIBRARIES CPLEX::cplex
  )
endif()

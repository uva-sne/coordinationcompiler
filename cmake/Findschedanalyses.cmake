# Copyright (C) 2020  Benjamin Rouxel <b.rouxel@uva.nl>
#                     Univeristy of Amsterdam
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

#######################
# This allows to connect Micaela Verucchi's work on Schedulability Analyses
#######################

file(GLOB_RECURSE CECILE_MSA_SOURCES ${CMAKE_SOURCE_DIR}/3rdparty/SchedAnalyses/src/*.cpp)

add_library(micaela_sched_analyses ${CECILE_MSA_SOURCES})

set_property(TARGET micaela_sched_analyses PROPERTY CXX_STANDARD 17)

# Remove YAML dependency
target_compile_definitions(micaela_sched_analyses PUBLIC NO_YAML)

# Remove TBB dependency
target_compile_definitions(micaela_sched_analyses PUBLIC CONFIG_COLLECT_SCHEDULE_GRAPH)

target_link_libraries(micaela_sched_analyses)

target_include_directories(micaela_sched_analyses PUBLIC ${CMAKE_SOURCE_DIR}/3rdparty/SchedAnalyses/include/)
target_include_directories(micaela_sched_analyses PUBLIC ${CMAKE_SOURCE_DIR}/3rdparty/SchedAnalyses/np-schedulability-analysis/include/)

message("-- Found sched analyses submodule")
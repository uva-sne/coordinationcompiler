# Copyright (C) 2020  Benjamin Rouxel <benjamin.rouxel@uva.nl>
#                     University of Amsterdam
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

set(CECILE_GEN_SOURCES )

function(generate_grammar GramName)

if(CECILE_DSL_PROJECT)
  get_filename_component(
    COORD_DSL_GRAMFILE 
    ${CECILE_DSL_PROJECT}/org.uva.sne.pcs.teamplay.coordination.parent/org.uva.sne.pcs.teamplay.coordination/src-gen/${GramName}.g4
    ABSOLUTE
  )
  if(EXISTS ${COORD_DSL_GRAMFILE})
    message("-- Found grammar file (${GramName}), rebuild antlr files target")
    add_custom_command(OUTPUT ${CMAKE_SOURCE_DIR}/src-gen/${GramName}.g4
      COMMAND cp ${COORD_DSL_GRAMFILE} .
      DEPENDS ${COORD_DSL_GRAMFILE}
      WORKING_DIRECTORY ${CMAKE_SOURCE_DIR}/src-gen
    )
  else()
    message("-- Reusing grammar file from this repository")
  endif()
else()
    message("-- You can specify -DCECILE_DSL_PROJECT=/path/to/root/DSL/ if you wish to rebuild parsers for ${GramName} language")
endif()
   
  set(${GramName}_GEN_SOURCES
    	${CMAKE_SOURCE_DIR}/src-gen/${GramName}.interp
	${CMAKE_SOURCE_DIR}/src-gen/${GramName}.tokens
	${CMAKE_SOURCE_DIR}/src-gen/${GramName}BaseVisitor.cpp
	${CMAKE_SOURCE_DIR}/src-gen/${GramName}BaseVisitor.h
	${CMAKE_SOURCE_DIR}/src-gen/${GramName}Lexer.cpp
	${CMAKE_SOURCE_DIR}/src-gen/${GramName}Lexer.h
	${CMAKE_SOURCE_DIR}/src-gen/${GramName}Lexer.interp
	${CMAKE_SOURCE_DIR}/src-gen/${GramName}Lexer.tokens
	${CMAKE_SOURCE_DIR}/src-gen/${GramName}Parser.cpp
	${CMAKE_SOURCE_DIR}/src-gen/${GramName}Parser.h
	${CMAKE_SOURCE_DIR}/src-gen/${GramName}Visitor.cpp
	${CMAKE_SOURCE_DIR}/src-gen/${GramName}Visitor.h
  )


  add_custom_command(OUTPUT ${${GramName}_GEN_SOURCES}
      COMMAND antlr4  -Dlanguage=Cpp -no-listener -visitor ${GramName}.g4
      DEPENDS ${CMAKE_SOURCE_DIR}/src-gen/${GramName}.g4
      WORKING_DIRECTORY ${CMAKE_SOURCE_DIR}/src-gen
  )
  add_custom_target(grammar_${GramName} ALL
    DEPENDS ${CMAKE_SOURCE_DIR}/src-gen/${GramName}.g4
    WORKING_DIRECTORY ${CMAKE_SOURCE_DIR}/src-gen
  )

  set(CECILE_GEN_SOURCES ${CECILE_GEN_SOURCES} ${${GramName}_GEN_SOURCES} PARENT_SCOPE)
endfunction()

generate_grammar(TeamPlay)
generate_grammar(CoordinationLanguage)

add_library(cecile_antlr ${CECILE_GEN_SOURCES})

add_dependencies(cecile_antlr grammar_TeamPlay)
add_dependencies(cecile_antlr grammar_CoordinationLanguage)

target_include_directories(cecile_antlr PUBLIC ${CMAKE_SOURCE_DIR}/src-gen/)
target_link_libraries(cecile_antlr PUBLIC ANTLR4::cpp_runtime)

message("-- Found grammar package")
# CECILE: a Compiler for the tEamplay CoordInation LanguagE

_cecile_ is the TeamPlay coordination compiler. It generates source code for
real-time software designed to run efficiently on possibly heterogeneous system
architectures. It does this by taking small components, examining their
non-functional properties, and the way they interact with each other and then
coordinating those components in as efficient a manner as possible.

## Fetch and preparing your project the repository

```bash
$ git clone https://bitbucket.org/uva-sne/coordinationcompiler.git
$ cd coordinationcompiler
$ git submodule update --init --recursive
$ git -C 3rdparty/SchedAnalyses apply ../SchedAnalyses.patch
```

## Dependencies

To build _cecile_, you will require the following software to be installed on
your machine (don't forget the -dev if you are on a debian-based OS):

* `libxml++-2.6`
* `boost` (specifically the `system`, `program_options`, and `filesystem` components)
* `yaml-cpp`
* `CMake` (version 3.16 or higher)
* `Git`
* `antlr4` and `antlr4-cpp-runtime` (the parser-generator and runtime)
* (optional) `Eigen3`
* (optional) `cplex`
* (optional) `lpsolve`
* (optional) `glpk`

The first few dependencies will most likely be installable through a package
manager of your choice. Complete build instructions for Ubuntu 20.04 can be found [here](#markdown-header-complete-build-for-ubuntu-2004-lts).

The following software is required to enable _optional_ components of the
compiler. If any of these packages are causing trouble during the compilation,
they can be disabled using build flags, see the "Building optional components"
section of this document for more information.

* `cplex`
* `lp_solve` (the dev package, most likely `liblpsolve55-dev`)
* `glpk` (the dev package, most likely `libglpk-dev`)
* `Eigen3` (the dev package, most likely `libeigen3-dev`)

Of these, `lp_solve`, `glpk` and `Eigen3` are most likely available in your package
repositories. `cplex`, being proprietary IBM technology, is most certainly not.
Note that these optional dependencies are independent of each other, so you can
compile _cecile_ with support for GLPK even if you do not have CPLEX
installed.

### CPLEX

CPLEX is proprietary IBM software that can, for reasons that are perhaps
obvious, not be found in open source software repositories. You will need to
source a copy of the CPLEX software yourself if you wish, but please note that
_cecile_ can be compiled without CPLEX and run fine. CMake should 
automatically detect the lack of a CPLEX installation and ignore any modules
that require CPLEX.

The CPLEX install utility will, as of the current version, install into the
directory `/opt/ibm/ILOG/CPLEX_Studio1271` by default. The build system should
be able to recognise this. If you install CPLEX elsewhere, such as into your
user directory, you will need to specify this to CMake as described futher down
these instructions.

### Loading dependencies at build time

In most cases, CMake will detect the presence of the dependencies automatically
through the `cmake/*.cmake` files. However, if the libraries are installed in
non-standard locations (particularly `cplex` or `antlr4-cpp-runtime`), you may
need to manually specify the location of the libraries. For 
`antlr4-cpp-runtime`, one should specify the following CMake flag:

```
-DCMAKE_PREFIX_PATH=${ANTLR4_CPP_RUNTIME_INSTALL_DIR}/usr/local/
```

If CPLEX is present on the machine but not found automatically (i.e. it is not
installed in the default location), the following flag is to be used:

```
-DCECILE_CPLEX_ROOT=${CPLEX_INSTALL_DIR}
```

Here, `${CPLEX_INSTALL_DIR}` should be the top level directory of your CPLEX
installation, which should contain the directories `concert`, `cplex`, 
`cpoptimizer`, and several more.

Feel free to modify the version of the libraries according to your system. The
build system should be robust against variations in dependency versioning.

## Compilation

To generate a build system for  _cecile_, follow the following steps in your 
shell from the root directory of the compiler source code:

```bash
$ mkdir build
$ cd build
$ cmake .. --DCMAKE_BUILD_TYPE=Release
```

If any dependencies are missing, add the CMake flags from the previous section
to help CMake find then.
If you wish to activate debug information, perform the following CMake command
instead (in addition to the other commands):

```bash
$ cmake .. -DCMAKE_BUILD_TYPE=Debug
```

Then, to build _cecile_, simply execute:

```bash
$ make
```

By default, the compiled binary is placed into the `dist` directory.

### A note on CPLEX

The CPLEX software package provides only static versions of its libraries, so
they must be statically compiled into the _cecile_ binary. This increases the
file size significantly, but there is little we can do about this.

### Building optional components

There are three optional components to _cecile_ that can be enabled and
disabled individually. Those components are IBM CPLEX ILP solver support,
GNU Linear Programming Kit (GLPK) support, and lp_solve support. The default
behaviour of the build system is to attempt to find these packages and, if they
are found, to compile the relevant components.

Compilation of optional components can be further controlled by providing flag
arguments to the CMake build system. The relevant flags are:

* `CECILE_ENABLE_ILP_CPLEX`
* `CECILE_ENABLE_ILP_LPSOLVE`
* `CECILE_ENABLE_ILP_GLPK`

By default, these flags are set to enabled (`ON` in CMake terminology), and
they can be disabled using the `-DCECILE_ENABLE_ILP_CPLEX=OFF` (or similar for
the other components) command line argument.

## Compiling the Documentation

To build the documentation based on doxygen, sphinx and breathe use the additional cmake argument ` -DBUILD_DOCUMENTATION=True`. 

### Dependencies

To build the documentation you require additional packages. 

* `doxygen` via your package manager
* `sphinx` via your package manager, pip or conda 
* `breathe` via pip

Alternatively use the `python_requirements.yml` in `/doc/sphinx/` to create a conda environment.  


## Complete build for Ubuntu 20.04 LTS

The following is a guide to doing a complete build and install of _cecile_ on
Ubuntu 20.04 LTS (Focal Fossa). This has been verified to work on a completely
fresh install of the operating system. First, install the necessary
dependencies that are available from the Ubuntu software repositories:

```
# apt install --no-install-recommends -y \
    ca-certificates \
    build-essential \
    git \
    cmake \
    libxml++2.6-dev \
    libboost-all-dev \
    uuid-dev \
    libantlr-dev \
    liblpsolve55-dev \
    libglpk-dev \
    libyaml-cpp-dev \
    libyaml-cpp0.6 \
    libeigen3-dev \
    antlr4 \
    libantlr4-runtime-dev
```

Build the _cecile_ software (from the project root, i.e. `foo/bar/coordinationcompiler`).

``` 
$ mkdir build
$ cd build
$ cmake .. -DCMAKE_BUILD_TYPE=Release
$ make -j$(nproc)
$ make install
```

The _cecile_ binary should now be installed into your PATH, and should be
ready to use. Of course, it will be compiled without support for the CPLEX ILP
solver, which is proprietary and needs to be installed separately as installed
above.

### Troubleshooting

In theory, there should be no problem finding the CPLEX runtime, because we link them statically at compile time. However, computers
can be fickle, and if for some reason you do run into such issues, you may try
to run _cecile_ with the following modified environment variables: 

```bash
$ export CPLEX_HOME=/your/cplex/install/path
$ export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:${CPLEX_HOME}/opl/lib/x86-64_linux/static_pic
$ export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:$(pwd)/3rdparty/run/lib/
$ ./dist/cecile
```

## Testing

_cecile_ comes with a test suite that can be used to verify that the tool is
behaving as expected. The test suite is contained in a binary called 
`cecile_test`. To enable the compilation of `cecile_test` add the `-DCECILE_BUILD_TESTS=On` flag. 
To minimize compilation time overhead, both are built from a common core. Invoke 
`cecile_test` simply:

```bash
$ ./dist/cecile_test
```

### Test coverage

We support test coverage analysis using `gcov`. Note that this functionality is
rather ham-fisted and might not work on operating systems other than Linux. It
certainly doesn't work with compilers other than GCC. Note that you need to
have both `gcov` and `lcov` installed for this to work. A coverage analysis
target is generated if the build type is set to `Coverage`:

```bash
$ cmake -DCMAKE_BUILD_TYPE=Coverage .
```

Then, execute the make target called `cecile_coverage`:

```bash
$ make cecile_coverage
```

If all is well, this will produce a browsable coverage report at 
`cecile_coverage/index.html`.

## Contact

If you have any questions, problems, suggestions, complaints, or compiments 
regarding _cecile_, please feel free to contact us at:

* Benjamin Rouxel <benjamin@lexuor.net>
* Lukas Miedema <l.miedema@uva.nl>
* Julius Roeder <j.roeder@uva.nl>
* Clemens Grelck <c.grelck@uva.nl>
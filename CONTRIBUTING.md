# Contributing

### Maintainers
* Benjamin Rouxel <benjamin@lexuor.net>
* Lukas Miedema <l.miedema@uva.nl>
* Julius Roeder <j.roeder@uva.nl>

### Civil guidelines
Want to contribute? Great! But, please recognize;
- Not everything is perfect or will be perfect, Cecile is academia-ware
- Try to avoid stepping on anyones toes. If you are unhappy about something, reach out about it and try to communicate constructively.
- Be receptive to the feedback of others and try to empathise with the other persons

### Programming guidelines
- Prevent collosal change sets, instead aim for the "minimum mergeable unit"
- Offer your code as a pull request and summarize what the change does. Give others a reasonable chance to review it, but also realize they may not have time (its okay to merge then if you have merge permissions).

- Making a structural change to the "spine" of Cecile (like `Schedule.hpp/cpp`, `SystemModel.hpp/cpp`, `Solver.hpp` or `main.cpp`)?
   1. Possibly a big, controversial change? Contact the (other) maintainers before you start
   2. Implement your spine changes with minimal effect on other code
   3. Make an effort to ensure your code doesn't break already-existing code
- Unit test your code
- Add documentation 
   + In the code: corner cases comment, doxygen, help() 
   + The user-manual (if appropriate: new CompilerPass, new Option, ...), please commit the updated PDF 
#include "ToyExample.h"

void mult(int in, double* d){
    printf("input number: %d from task: mult\n", in);

    int a=2;
    int b=6;

    for (size_t i = 0; i < NUM_ITER; i++) {
        a *= b;
    }

    printf("Results from mult are: %d \n", a);
    *d= 4;
}

#include "ToyExample.h"

void sub(int in, double* c){
    printf("input number: %d from task: sub\n", in);

    int a=0;
    int b=10;

    for (size_t i = 0; i < NUM_ITER; i++) {
        a -= b;
    }

    printf("Results from sub are: %d \n", a);
    *c = 3;
}

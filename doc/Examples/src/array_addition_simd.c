#include "ToyExample.h"
#include <omp.h>

void vector_addition_simd(float in, float* f){
    printf("input number: %f from task: vector_addition_simd\n", in);

      float * a, * b, * c;
    a = (float *) malloc(sizeof(float)*NUM_ITER);
    b = (float *) malloc(sizeof(float)*NUM_ITER);
    c = (float *) malloc(sizeof(float)*NUM_ITER);

    #pragma omp simd
    for (size_t i = 0; i < NUM_ITER; i++) {
        a[i] = i;
        b[i] = i*2;
    }

    #pragma omp simd
    for (size_t i = 0; i < NUM_ITER; i++) {
        c[i] = a[i] + b[i];
        *f+=c[i];
    }

    printf("Results from vector_addition_simd are: %f \n", *f);
    // *f= 6.0D;
}

#include "ToyExample.h"

void vector_addition(float in, float* f){
    printf("input number: %f from task: vector_addition\n", in);

    float * a, * b, * c;
    a = (float *) malloc(sizeof(float) * NUM_ITER);
    b = (float *) malloc(sizeof(float) * NUM_ITER);
    c = (float *) malloc(sizeof(float) * NUM_ITER);

    for (int i = 0; i < NUM_ITER; i++) {
        a[i] = i;
        b[i] = i*2;
    }

    for (int i = 0; i < NUM_ITER; i++) {
        c[i] = a[i] + b[i];
        *f+=c[i];
    }

    printf("Results from vector_addition are: %f \n", *f);
    // *f= 6.0D;
}

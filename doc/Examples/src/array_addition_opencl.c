#include "ToyExample.h"
#include <CL/cl.h>

const char * code =
"__kernel void add_array(__global int * a, __global int * b, __global int * c) \n"
  "{\n"
    "size_t id = get_global_id(0);\n"
    "a[id]=id;\n"
    "b[id]=id*2;\n"
    "c[id]=a[id]+b[id];\n"
  "}\n";

void vector_addition_opencl(float in, float* f){
    printf("input number: %f from task: vector_addition_simd\n", in);
    cl_context context;
    cl_context_properties properties[3];
    cl_kernel kernel;
    cl_command_queue commandqueue;
    cl_program program;
    cl_int err;
    cl_uint num_of_platforms;
    cl_platform_id platform_id;
    cl_device_id device_id;
    cl_uint num_of_devices;
    cl_mem buffer1, buffer2, outputbuffer;

      float * a, * b, * c;
    a = (float *) malloc(sizeof(float)*NUM_ITER);
    b = (float *) malloc(sizeof(float)*NUM_ITER);
    c = (float *) malloc(sizeof(float)*NUM_ITER);

    #pragma omp simd
    for (size_t i = 0; i < NUM_ITER; i++) {
        a[i] = i;
        b[i] = i*2;
    }

    #pragma omp simd
    for (size_t i = 0; i < NUM_ITER; i++) {
        c[i] = a[i] + b[i];
        *f+=c[i];
    }

    printf("Results from vector_addition_simd are: %f \n", *f);
    // *f= 6.0D;
}

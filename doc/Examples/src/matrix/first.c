#include "ToyExampleMatrix.h"

void first(matrix_int_t* a) {
    printf("First Task \n");
    for(int i=NUM_ROWS_COLS-1 ; i >= 0 ; --i) {
        for(int j=NUM_ROWS_COLS-1 ; j >= 0 ; --j) {
            a->cell[i][j] = 1;
        }
    }
}

void first_v1c(matrix_int_t* a) {
    printf("First Task GPU \n");
    int num_rows = NUM_ROWS_COLS;
    int num_cols = NUM_ROWS_COLS;
    cl_int err;
    
    cl_program program = get_program();
    cl_command_queue queue = get_queue();
    cl_context context = get_context();
    
    cl_mem buffer = clCreateBuffer(context, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR , sizeof(matrix_int_t), (void*)a, NULL);
    
    cl_kernel kernel = clCreateKernel(program, "first_gpu", NULL);
    clSetKernelArg(kernel, 0, sizeof(cl_mem), (void*)&buffer);
    
    const size_t global[2] = { num_rows, num_cols };
    cl_event event = NULL;
    err = clEnqueueNDRangeKernel(queue, kernel, 2, NULL, global, NULL, 0, NULL, &event);
    assert(err == CL_SUCCESS);
    clWaitForEvents(1, &event);

    clEnqueueReadBuffer(queue, buffer, CL_TRUE, 0, sizeof(matrix_int_t), (void*)a, 0, NULL, NULL);
   
    clReleaseMemObject(buffer);
}
#include "ToyExampleMatrix.h"

void mult_matrix(matrix_int_t a, matrix_int_t b, matrix_int_t* out){
    printf("mult_matrix\n");
    
    for (int i = 0 ; i < NUM_ROWS_COLS ; i++) {
        for (int j = 0 ; j < NUM_ROWS_COLS ; j++) {
            long acc = 0;
            for (int k = 0 ; k < NUM_ROWS_COLS ; k++) {
                acc += a.cell[k][i] * b.cell[j][k];
            }
            out->cell[i][j] = acc;
        }
    }
}

void mult_matrix_v1c(matrix_int_t a, matrix_int_t b, matrix_int_t* out) {
    printf("mult_matrix_v1c\n");
    int num_rows = NUM_ROWS_COLS;
    int num_cols = NUM_ROWS_COLS;
    
    cl_program program = get_program();
    cl_command_queue queue = get_queue();
    cl_context context = get_context();
    
    cl_mem buffera = clCreateBuffer(context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, sizeof(matrix_int_t), (void*)&a, NULL);
    cl_mem bufferb = clCreateBuffer(context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, sizeof(matrix_int_t), (void*)&b, NULL);
    cl_mem bufferout = clCreateBuffer(context, CL_MEM_WRITE_ONLY | CL_MEM_COPY_HOST_PTR, sizeof(matrix_int_t), (void*)out, NULL);
    
    cl_kernel kernel = clCreateKernel(program, "mult_matrix_gpu", NULL);
    clSetKernelArg(kernel, 0, sizeof(int), (void*)&num_rows);
    clSetKernelArg(kernel, 1, sizeof(cl_mem), (void*)&buffera);
    clSetKernelArg(kernel, 2, sizeof(cl_mem), (void*)&bufferb);
    clSetKernelArg(kernel, 3, sizeof(cl_mem), (void*)&bufferout);
    
    const size_t global[2] = { num_rows, num_cols };
    cl_event event = NULL;
    clEnqueueNDRangeKernel(queue, kernel, 2, NULL, global, NULL, 0, NULL, &event);
    clWaitForEvents(1, &event);
    
    clEnqueueReadBuffer(queue, bufferout, CL_TRUE, 0, sizeof(matrix_int_t), (void*)out, 0, NULL, NULL);
    
    clReleaseMemObject(buffera);
    clReleaseMemObject(bufferb);
    clReleaseMemObject(bufferout);
}

#include "ToyExampleMatrix.h"

void sum_all(matrix_int_t f, matrix_int_t g, matrix_int_t h){
    printf("sum_all\n");
    
    matrix_int_t out;
    for(int i=NUM_ROWS_COLS-1 ; i >= 0 ; --i) {
        for(int j=NUM_ROWS_COLS-1 ; j >= 0 ; --j) {
            out.cell[i][j] = f.cell[i][j] + g.cell[i][j] + h.cell[i][j];
        }
    }
    
    for(int i=0 ; i < NUM_ROWS_COLS ; i++) {
        for(int j=0 ; j < NUM_ROWS_COLS ; j++) {
            printf("%d ", out.cell[i][j]);
        }
        printf("\n");
    }
}

void sum_all_v1c(matrix_int_t f, matrix_int_t g, matrix_int_t h) {
    printf("sum_all_v1c \n");
    int num_rows = NUM_ROWS_COLS;
    int num_cols = NUM_ROWS_COLS;
    
    cl_program program = get_program();
    cl_command_queue queue = get_queue();
    cl_context context = get_context();
    
    matrix_int_t out;
    
    cl_mem bufferf = clCreateBuffer(context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, sizeof(matrix_int_t), (void*)&f, NULL);
    cl_mem bufferg = clCreateBuffer(context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, sizeof(matrix_int_t), (void*)&g, NULL);
    cl_mem bufferh = clCreateBuffer(context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, sizeof(matrix_int_t), (void*)&h, NULL);
    cl_mem bufferout = clCreateBuffer(context, CL_MEM_WRITE_ONLY | CL_MEM_COPY_HOST_PTR, sizeof(matrix_int_t), (void*)&out, NULL);
    
    cl_kernel kernel = clCreateKernel(program, "sum_all_gpu", NULL);
    clSetKernelArg(kernel, 0, sizeof(cl_mem), (void*)&bufferf);
    clSetKernelArg(kernel, 1, sizeof(cl_mem), (void*)&bufferg);
    clSetKernelArg(kernel, 2, sizeof(cl_mem), (void*)&bufferh);
    clSetKernelArg(kernel, 3, sizeof(cl_mem), (void*)&bufferout);
    
    const size_t global[2] = { num_rows, num_cols };
    cl_event event = NULL;
    clEnqueueNDRangeKernel(queue, kernel, 2, NULL, global, NULL, 0, NULL, &event);
    clWaitForEvents(1, &event);
    
    clEnqueueReadBuffer(queue, bufferout, CL_TRUE, 0, sizeof(matrix_int_t), (void*)&out, 0, NULL, NULL);
    clReleaseMemObject(bufferout);
    
    for(int i=0 ; i < NUM_ROWS_COLS ; i++) {
        for(int j=0 ; j < NUM_ROWS_COLS ; j++) {
            printf("%d ", out.cell[i][j]);
        }
        printf("\n");
    }
}
#ifndef TOY_EXAMPLE_MATRIX_H
#define TOY_EXAMPLE_MATRIX_H

#ifndef NUM_ITER
#define NUM_ITER 1000000000
#endif

#ifndef NUM_ROWS_COLS
#define NUM_ROWS_COLS 10
#endif

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#include <CL/cl.h>

typedef struct _IntMatrix matrix_int_t;

struct _IntMatrix {
    int cell[NUM_ROWS_COLS][NUM_ROWS_COLS];
    
    _IntMatrix& operator= (_IntMatrix &b);
    _IntMatrix& operator= (const _IntMatrix &b);
    
    _IntMatrix();
    _IntMatrix(const _IntMatrix&);
    
    void display();
};

bool operator==(const matrix_int_t &a, const matrix_int_t &b);
bool operator!=(const matrix_int_t &a, const matrix_int_t &b);

void first(matrix_int_t* a);
void add(matrix_int_t in, matrix_int_t* b);
void sub(matrix_int_t in, matrix_int_t* c);
void mult(matrix_int_t in, matrix_int_t* d);
void division(matrix_int_t in, matrix_int_t* e);
void square_matrix(matrix_int_t in, matrix_int_t* f);
void transpose_matrix(matrix_int_t in, matrix_int_t* g);
void mult_matrix(matrix_int_t b, matrix_int_t c, matrix_int_t* h);
void sum_all(matrix_int_t f, matrix_int_t g, matrix_int_t h);

void first_v1c(matrix_int_t* a);
void add_v1c(matrix_int_t in, matrix_int_t* b);
void sub_v1c(matrix_int_t in, matrix_int_t* c);
void mult_v1c(matrix_int_t in, matrix_int_t* d);
void division_v1c(matrix_int_t in, matrix_int_t* e);
void square_matrix_v1c(matrix_int_t in, matrix_int_t* f);
void transpose_matrix_v1c(matrix_int_t in, matrix_int_t* g);
void mult_matrix_v1c(matrix_int_t b, matrix_int_t c, matrix_int_t* h);
void sum_all_v1c(matrix_int_t f, matrix_int_t g, matrix_int_t h);

void init_gpu();
cl_program get_program();
cl_command_queue get_queue();
cl_context get_context();

#endif //TOY_EXAMPLE_MATRIX_H
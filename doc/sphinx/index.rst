.. 
   TeamPlay documentation master file, created by
   sphinx-quickstart on Mon Jun 29 16:42:12 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Cecile
=======

*Cecile* is the name of the TeamPlay coordination compiler. It generates source code for
real-time software designed to run efficiently on possibly heterogeneous system
architectures. It does this by taking small components, examining their
non-functional properties, and the way they interact with each other and then
coordinating those components in as efficient a manner as possible.


* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`


.. toctree::
   :maxdepth: 2
   :caption: Contents:

   cecile_install
   cecile_quickstart
   cecile_capabilities
   docs/cpp/cpp_docs
   extending_cecile
   intermediate_representation
    


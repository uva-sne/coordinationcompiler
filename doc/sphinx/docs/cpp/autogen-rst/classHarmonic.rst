Harmonic
============
.. 
    This file has been automatically generated. Please do not modify it or
    your changes might have been lost.

Inheritance Diagram
^^^^^^^^^^^^^^^^^^^

.. image:: /../../../figures/classHarmonic__inherit__graph.png
    :alt: Inheritance Diagram

Collaboration Diagram
^^^^^^^^^^^^^^^^^^^^^

.. image:: /../../../figures/classHarmonic__coll__graph.png
    :alt: Inheritance Diagram


.. doxygenclass:: Harmonic


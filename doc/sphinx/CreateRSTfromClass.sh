#!/usr/bin/bash

#sourcedir=$1
#dstdir=$2
sourcedir=$(dirname $0)/../../src/
dstdir=$(dirname $0)/docs/cpp/autogen-rst
incfile=$(dirname $0)/docs/cpp/cpp_docs.rst
types=(class 
struct 
enum
"enum class")

if [ -z "$sourcedir" ] ; then
    echo "Need to provide the source dir as first argument"
    exit -1
fi
if [ -z "$dstdir" ] ; then
    echo "Need to provide the destination dir as second argument"
    exit -2
fi

if [ ! -e "$sourcedir" -o ! -d "$sourcedir" ] ; then
    echo "Source dir doesn't exist, or is not a directory"
    exit -3
fi
if [ ! -e "$dstdir" -o ! -d "$sourcedir" ] ; then
    mkdir -p "$dstdir"
fi

rm -rf $dstdir/*

BKIFS=$IFS
IFS=$'\n'

for type in ${types[@]} ; do
    for l in $(grep -r -E "$type.* {" "$sourcedir") ; do
        file=$(echo $l | cut -d':' -f1)
        tmp=$(echo $l | cut -d':' -f2 | sed -e 's/^[[:space:]]*//')
        class=""
        if [[ $tmp == "$type"* ]] ; then
            class=$(echo "$tmp" | sed -e "s/^$type[[:space:]]*//" | cut -d' ' -f1)
        elif [[ $tmp == "typedef"* ]] ; then
            echo "found typedef need further parsing $tmp $file"
        fi
        if [ ! -z "$class" ] ; then
            echo "$class
============
.. 
    This file has been automatically generated. Please do not modify it or
    your changes might have been lost.

Inheritance Diagram
^^^^^^^^^^^^^^^^^^^

.. image:: /../../../figures/"$type$class"__inherit__graph.png
    :alt: Inheritance Diagram

Collaboration Diagram
^^^^^^^^^^^^^^^^^^^^^

.. image:: /../../../figures/"$type$class"__coll__graph.png
    :alt: Inheritance Diagram


.. doxygen"$type":: "$class"
" > $dstdir/$type$class.rst
        fi
    done
done

IFS=$BKIFS

#This changes the access time of this file, allowing to rebuild the documentation
touch $incfile
.. _intermediate_presentation:

Compiler passes
===============

Data exchange between compiler passes
-------------------------------------

In :ref:`capabilities of Cecile<cecile_capabilities>`, the option to chain compiler passes together is discussed. Each compiler pass is executed in the order defined in the
configuration, operating on the a shared data structure called the *Intermediate Representation* (IR). The procedural nature of compiler passes allows constructing complex setups
by composing compiler passes. For example, an SDF homogenization pass can be combined with an energy-aware scheduler, as well as the Yasmin code generator. Each compiler pass
operates on the IR and can mutate it. Specifically, the IR is composed of the three main elements:

* ``SystemModel`` -- the task graph itself, annotated with non-functional properties
* ``config_t`` -- the configuration, such as the hardware platform, bus behavior, as well as the sequence of compiler passes
* ``Schedule`` or ``StrategyStateMachine`` (SSM) -- the output of the scheduling / strategy generation procedure (depends on the compiler pass which is provided)

Understanding the StrategyStateMachine
--------------------------------------
The nature of each element in the IR is singleton: there is one ``SystemModel``, one ``config_t`` and one ``Schedule``. However, for some workflows, it is necessary to look at a population of schedules. For this,
the concept of a *Strategy* is introduced, together with the *Strategy State Machine*, which keeps track of the relation between strategies.

A strategy is a piece of extra information (a “parameter” of sorts) that affects how a schedule is made, of which many can exist. For example, a strategy may say “task Foo and task Bar run under triple modular redundancy", which adds a voter ``SchedElt`` as well as replica ``SchedElt`` elements to the ``Schedule#_elements_lut`` on which a scheduler can then operate. Many hundreds of such strategies can independently exist, each modifying the way a schedule is made by affecting the schedule elements prior to scheduling. However, only one strategy can be active at a time, so the effects do not stack. Rather, they facilitate a way to explore different schedules all derived from the same task set.

Strategies are scheduled on-demand when a compiler pass wants to look at the schedule (there is still just one ``Schedule`` in the IR). The thinking here is that you probably use a fast scheduler (e.g. FFLS), and that you are more limited by operations on the mass of strategies than pure scheduling time. In my work, the schedules are generated once to test schedulability (by construction), after which no operation on the set of strategies needs the schedule. Only during code generation are the schedules needed again, so each is generated only (at most) twice.

Implementing strategies
-----------------------
The ``Strategy`` class itself is abstract (in ``StateMachine/Strategy.hpp``), and many implementations can exist. Currently, there is one implementation for transient fault tolerance (like the described "run this under TMR"), but the intention is to also use this for TeamPlay modes. Each different projection of modes becomes a strategy. The FaultToleranceStrategy is only a boolean vector indicating which tasks run under fault tolerance and which do not.

Strategy state machine
----------------------

The strategies are all kept in a data structure called the ``StrategyStateMachine`` (SSM), which maintains the set of all created strategies (really just a ``std::vector<std::unique_ptr<Strategy>>``). The strategy vector must be homogeneous, you cannot mix different types of strategies. Strategies are managed by a ``StrategyManager``, which is also abstract. As strategies need to be scheduled on-demand for evaluation, the ``StrategyManager`` has a pointer to the scheduler, and other things necessary for scheduling. For example, the ``FaultToleranceStrategyManager`` determines what to do with tasks under fault-tolerance (e.g. use TMR, but it could also use 5MR or something like that). It effectively decides how to treat the data in a ``FaultToleranceStrategy`` and apply it to the schedule. The ``FaultToleranceStrategy`` itself can then just be a simple ``std::vector<bool>``.

It is called a “state machine” as it can also encode the relationship between strategies (how to transition between them).

Working *without* strategies
----------------------------
The SSM is always created as part of the IR, just like the ``Schedule`` and ``SystemModel``, even when no SSM compiler pass is used. The SSM holds the ``Schedule`` (just a single one), and regular non-SSM compiler passes operate on this Schedule. In ``CompilerPass``, two run overloads exist:

 * ``CompilerPass#run(SystemModel *tg, config_t *conf, Schedule *s)``
 * ``CompilerPass#run(SystemModel *tg, config_t *conf, StrategyStateMachine *ssm)``

The new function has a default implementation which tries to convert the SSM to a single schedule when it is *trivial*. By default, the state of the SSM is trivial, which allows direct access to its wrapped schedule. When a compiler pass has been used that has installed a ``StrategyManager``, this access is no longer possible and a ``CompilerException`` is thrown.

Creating a (functional) state machine
-------------------------------------
A ``<strategy-generator id=... />`` compiler pass starts the SSM workflow. They are scoped, i.e. ``<strategy-generator id=ft::exhaustive...`` As a non-trivial SSM always needs a ``StrategyManager``, this is where that is set. In the case of fault tolerance, you set the scheduler and the fault-tolerance technique on the ``strategy-generator`` in the configuration xml. This is then used to construct the ``FaultToleranceStrategyManager``.

Creating/read/update/delete strategies
++++++++++++++++++++++++++++++++++++++

Compiler passes can then work on the set of strategies. Schedules can be made on-demand via the ``StrategyManager``, as it holds a ref to the scheduler. Operations that create, delete, filter, analyze or do whatever with strategies can now follow.

Why a state machine
+++++++++++++++++++

The Strategy State Machine is called so because it can encode the relation between strategies. For transient faults, a strategy runs a particular set of tasks under fault-tolerance. At runtime, for each strategy the next best successor must be known to switch to minimize the fault rate. This can be determined ahead-of-time, making the application effectively step through the “state machine” of strategies at runtime. Each new period, a new strategy is selected and its associated schedule is used.

Results
+++++++

For fault tolerance, there is information about which tasks failed and which did not at runtime. This is presented in the fault tolerance strategy state machine as a result – simply a bool array of which tasks failed and which did not. At runtime, the application switches from strategy to result to strategy ad infinitum. This is captured by a ``Result`` class, for which the ``FaultToleranceResult`` is a bool array of which tasks failed and which succeeded.

Results are not first-class citizens of the SSM, and rather maintained by the ``FaultToleranceStrategyManager`` as they may be generated on-demand each time.

StrategyScope
+++++

It is possible to use regular compiler passes (such as the dot exporter) on a strategy-derived schedule using the ``<strategy id=...>`` tag. With the strategies key, a list of strategy ids can be specified. Each of the strategies will be applied to the schedule, after which compiler passes in the ``<strategy>...</strategy>`` tag are run. When multilpe strategies are given, the compiler passes are executed once for every strategy.

Scoping and lifecycle
---------------------

Let us consider the possible effects of some sequence of compiler passes on the IR, with and without a strategy state machine

No strategy state machine
+++++++

**1. Before the first pass**

Cecile always starts by reading a configuration file (``.xml``), coordination language file (``.tey`` or ``.coord``), and optionally a non-functional properties file (``.nfp``). These inputs are read to populate the ``SystemModel`` and ``config_t``.

A ``Schedule`` and ``StrategyStateMachine`` are always allocated, but they remain empty. The ``Schedule`` has no ``ScheduleElts`` -- the building blocks of a schedule. The ``StrategyStateMachine`` is in the *trivial* state. The IR is in state "no schedule + trivial SSM".

**2. Non-mutating passes**

Not all compiler passes modify the structure of the IR. Analysis and exporters do not change what is in the IR. Likewise, some passes may directly modify the task graph. The IR remains in "no schedule + trivial SSM"

**3. Partial scheduling**

Some compiler passes set *part* of the schedule -- they do not provide a full schedule, but rather create a restriction such as partitioning the task graph. This is done by creating the population of ``SchedElt`` (schedule elements) in the schedule. A schedule element is the primitive that is actually scheduled, and it is derived from the task graph. Communication is modeled through packets (``SchedPacket``) and tasks are mapped to zero or more jobs (``SchedJob``), depending on the characteristics of the task graph.

A partitioning pass may construct these elements, and then assign a processor to each element without setting a release time. The IR is in state "partial schedule + trivial SSM"

**4. Full scheduling**

A scheduler creates the schedule elements (if necessary) and assigns release times. The IR is now in state "schedule + trivial SSM".

**5. Exporters**

Finally, the schedule can be exported. Code can be generated. This does not mutate the structure of the IR.

Using a strategy state machine
++++++
Step 1 and 2 are identical. Then;

**3. Creating the strategy state machine**

The strategy state machine is initialized with a ``<strategy-generator id="...`` compiler pass.
To initialize the strategy state machine, a *strategy manager* must be assigned. This manager decides what the state machine is for. For example, this could be a manager for fault-tolerance. The manager also controls the lifecycle (including instantiation) of the strategies. A pool of strategies can now be constructed (not necessary, but typical). The IR is in state "no schedule + fault-tolerance SSM (unlinked)".

**4. Operations on the set of strategies**

Compiler passes can create, edit and delete individual strategies. The schedule in the IR can be populated to evaluate a single strategy, but it must be cleared before the compiler pass finishes.

**5. Linking**

Some SSM use-cases require determining relations between strategies, a process called *linking*. This allocates ``Result`` objects, which are managed by the strategy manager (*not* the strategy state machine). The presence of these moves the SSM to the state "no schedule + fault-tolerance SSM (linked)".

**6. Scopes**

Using the ``<strategy id="some-id"> <some-compiler-pass /> </strategy>``, regular compiler passes can be run. These compiler passes are served, in this example, an IR of state "schedule + fault-tolerance SSM (linked)". The schedule is constructed based on the provided strategy id, and cleared afterwards. The state stays "no schedule + fault-tolerance SSM (linked)".

**7. Exporting**

Exporters can export the SSM.
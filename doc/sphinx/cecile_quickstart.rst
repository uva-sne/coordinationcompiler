.. _cecile_quickstart:

Using Cecile
=============

After following the :ref:`installation guide<cecile_install>` you will be able to generate a schedule for a target application. 
For scheduling you need at 3 files:

- Configuration file
- Coordination file
- Non-Functional-Properties file



Config File
-----------

.. literalinclude:: ../Examples/example_config.xml
   :caption: Configuration File Example
   :language: c++
   :linenos:

Coord File
----------

.. literalinclude:: ../Examples/example.coord
   :caption: Configuration File Example
   :language: c++
   :linenos:


NFP File
--------

.. literalinclude:: ../Examples/example.nfp
   :caption: Configuration File Example
   :language: c++
   :linenos:



.. _cecile_install:

Installation
============

Dependencies
------------

To build *Cecile*, you will require the following software to be installed on
your machine:

* `libxml++-2.6` (specifically the dev package)
* `boost` (specifically the `system`, `program_options`, and `filesystem` components)
* `antlr4`
* `antlr4-cpp-runtime`

The first three dependencies will most likely be installable through a package
manager of your choice. On some distributions of Linux, `antlr4-cpp-runtime` is
also available through the package manager, but this is notably not the case
for Ubuntu.

The following software is required to enable _optional_ components of the
compiler. If any of these packages are causing trouble during the compilation,
they can be disabled using build flags, see the "Building optional components"
section of this document for more information.

* `cplex`
* `lp_solve` (the dev package, most likely `liblpsolve55-dev`)
* `glpk` (the dev package, most likely `libglpk-dev`)

Of these, `lp_solve` and `glpk` are most likely available in your package
repositories. `cplex`, being proprietary IBM technology, is most certainly not.
Note that these optional dependencies are independent of each other, so you can
compile _cecile_ with support for GLPK even if you do not have CPLEX
installed.

CPLEX
-----

CPLEX is proprietary IBM software that can, for reasons that are perhaps
obvious, not be found in open source software repositories. You will need to
source a copy of the CPLEX software yourself if you wish, but please note that
_cecile_ can be compiled without CPLEX and run fine. CMake should 
automatically detect the lack of a CPLEX installation and ignore any modules
that require CPLEX.

The CPLEX install utility will, as of the current version, install into the
directory `/opt/ibm/ILOG/CPLEX_Studio1271` by default. The build system should
be able to recognise this. If you install CPLEX elsewhere, such as into your
user directory, you will need to specify this to CMake as described futher down
these instructions.

ANTLR4 C++ runtime
------------------

If `antlr4-cpp-runtime` is not provided by your package manager, you will need
to install it from source yourself. The source code is available from
https://github.com/antlr/antlr4. The following is a quick example of how it can
be built and installed:

.. code-block:: console
    
    $ git clone --depth 1 --branch=4.8 https://github.com/antlr/antlr4.git
    $ cd antlr4/runtime/Cpp
    $ cmake .
    $ make install


This will install the ANTLR4 C++ runtime into the default location, requiring
administrative permissions. To install it elsewhere, in a location where you
have write permissions, use the following:

.. code-block:: console
    
    $ make install DESTDIR=${ANTLR4_CPP_RUNTIME_INSTALL_DIR}


Regenerating parser code
------------------------

If you're working on the language grammar, you may wish to (re-)generate the
parser files in `src-gen/` as a part of the build process to ensure that
_cecile_ will be compiled with all your changes. The _cecile_ build system
provides the `CECILE_DSL_PROJECT` definition which, when set, will attempt to
rebuild the parser from whichever directory it is defined as.

In general, `CECILE_DSL_PROJECT` should contain the
`org.uva.sne.pcs.teamplay.coordination.parent/` directory, which in turn should
have the file `CoordinationLanguage.g4` somewhere deeper in the hierarchy. If
you're unsure which directory to specify, use `CoordinationTools/DSL/` relative
to the top level of the `TeamPlay_Tools` repository.

Note that the ANTLR4 package must be installed in order for the build system to
be able to generate the parser.

Loading dependencies at build time
----------------------------------

In most cases, CMake will detect the presence of the dependencies automatically
through the `cmake/*.cmake` files. However, if the libraries are installed in
non-standard locations (particularly `cplex` or `antlr4-cpp-runtime`), you may
need to manually specify the location of the libraries. For 
`antlr4-cpp-runtime`, one should specify the following CMake flag:

```
-DCMAKE_PREFIX_PATH=${ANTLR4_CPP_RUNTIME_INSTALL_DIR}/usr/local/
```

If CPLEX is present on the machine but not found automatically (i.e. it is not
installed in the default location), the following flag is to be used:

```
-DCPLEX_ROOT=${CPLEX_INSTALL_DIR}
```

Here, `${CPLEX_INSTALL_DIR}` should be the top level directory of your CPLEX
installation, which should contain the directories `concert`, `cplex`, 
`cpoptimizer`, and several more.

Feel free to modify the version of the libraries according to your system. The
build system should be robust against variations in dependency versioning.

Compilation
-----------

To generate a build system for  _cecile_, follow the following steps in your 
shell from the root directory of the compiler source code:

.. code-block:: console

    $ mkdir build
    $ cd build
    $ cmake .. --DCMAKE_BUILD_TYPE=Release


If any dependencies are missing, add the CMake flags from the previous section
to help CMake find then.
If you wish to activate debug information, perform the following CMake command
instead (in addition to the other commands):

.. code-block:: console

    $ cmake .. -DCMAKE_BUILD_TYPE=Debug


Then, to build _cecile_, simply execute:

.. code-block:: console

    $ make

By default, the compiled binary is placed into the `dist` directory.

A note on CPLEX and `antlr4-cpp-runtime`
----------------------------------------

The CPLEX software package provides only static versions of its libraries, so
they must be statically compiled into the _cecile_ binary. This increases the
file size significantly, but there is little we can do about this.

The `antlr4-cpp-runtime` library provides both dynamic and static objects, but
we choose to compile it statically into our binary. The reason for this is the
limited availability of the library through package managers. If someone
builds cecile for multiple people to use, they would also have to distribute
the `antlr4-cpp-runtime` shared objects, which is an additional hassle. This
may change in the future.

Building optional components
--------------------------------

There are three optional components to _cecile_ that can be enabled and
disabled individually. Those components are IBM CPLEX ILP solver support,
GNU Linear Programming Kit (GLPK) support, and lp_solve support. The default
behaviour of the build system is to attempt to find these packages and, if they
are found, to compile the relevant components.

Compilation of optional components can be further controlled by providing flag
arguments to the CMake build system. The relevant flags are:

* `CECILE_ENABLE_ILP_CPLEX`
* `CECILE_ENABLE_ILP_LPSOLVE`
* `CECILE_ENABLE_ILP_GLPK`

By default, these flags are set to enabled (`ON` in CMake terminology), and
they can be disabled using the `-DCECILE_ENABLE_ILP_CPLEX=OFF` (or similar for
the other components) command line argument.

Complete build for Ubuntu 16.04 LTS
--------------------------------------

The following is a guide to doing a complete build and install of _cecile_ on
Ubuntu 16.04 LTS (Xenial Xerus). This has been verified to work on a completely
fresh install of the operating system. First, install the necessary
dependencies that are available from the Ubuntu software repositories:

.. code-block:: console
    
    $ apt install cmake libxml++2.6-dev libboost-all-dev uuid-dev libantlr-dev liblpsolve55-dev libglpk-dev


If you wish to regenerate the parser, you will also need to install the
following:

.. code-block:: console

    $ apt install antlr4

Then, install the ANTLR4 C++ runtime from source, as it's not available in the
repositories:

.. code-block:: console

    $ cd /tmp/
    $ git clone https://github.com/antlr/antlr4.git
    $ cd antlr4/runtime/Cpp/
    $ cmake -DCMAKE_BUILD_TYPE=Release .
    $ make
    $ make install

Finally, we can build and install the _cecile_ software. We'll assume you have
SSH keys installed to clone the repository. If you don't, you'll need to get
the source code onto your new Ubuntu install in a different way:

.. code-block:: console

    $ cd /tmp/
    $ git clone git@gitlab.inria.fr:TeamPlay/TeamPlay_Development/TeamPlay_Tools.git
    $ cd TeamPlay_Tools/CoordinationTools/compiler/
    $ cmake -DCMAKE_BUILD_TYPE=Release .
    $ make
    $ make install


The _cecile_ binary should now be installed into your PATH, and should be
ready to use. Of course, it will be compiled without support for the CPLEX ILP
solver, which is proprietary and needs to be installed seperately as installed
above.

Installation on the TUHH server
-------------------------------

Installing the latest version of _cecile_ on the TUHH server is trivial, as
CPLEX is already installed and we have installed any missing dependencies in
the `/opt/local/teamplay/COORDINATION/` prefix. Compiling and installing
_cecile_ is therefore as simple as:

.. code-block:: console

    $ cmake -DCMAKE_BUILD_TYPE=Release -DCPLEX_ROOT=/opt/local/cplex/12.8/ -DCMAKE_INSTALL_PREFIX=/opt/local/teamplay/COORDINATION/
    $ make -j 8 install

Note that this will overwrite the existing installation of _cecile_ which may
be in use by other people, so take care not to install broken versions of the
code.

Usage
-----

In order to display a helpful message explaining the arguments for _cecile_,
run the following:

.. code-block:: console

    $ ./dist/cecile


The most common usage of _cecile_ is to provide a configuration file, a
coordination file, and a non-functional properties file. If you have all of
these at hand, you can invoke _cecile_ as follows:

.. code-block:: console

    $ ./dist/cecile --config config.xml --coord use-case.coord --nfp use-case.nfp 

Troubleshooting
---------------

In theory, there should be no problem finding CPLEX and the ANTLR4 C++ runtime
at runtime, because we link them statically at compile time. However, computers
can be fickle, and if for some reason you do run into such issues, you may try
to run _cecile_ with the following modified environment variables: 

.. code-block:: console

    $ export CPLEX_HOME=/your/cplex/install/path
    $ export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:${CPLEX_HOME}/opl/lib/x86-64_linux/static_pic
    $ export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:$(pwd)/3rdparty/run/lib/
    $ ./dist/cecile


Testing
---------

_cecile_ comes with a test suite that can be used to verify that the tool is
behaving as expected. The test suite is contained in a binary called 
`cecile_test` that is compiled alongside `cecile` by default. To minimize
compilation time overhead, both are built from a common core. Invoke 
`cecile_test` simply:

.. code-block:: console

    $ ./dist/cecile_test


Test coverage
-----------------

We support test coverage analysis using `gcov`. Note that this functionality is
rather ham-fisted and might not work on operating systems other than Linux. It
certainly doesn't work with compilers other than GCC. Note that you need to
have both `gcov` and `lcov` installed for this to work. A coverage analysis
target is generated if the build type is set to `Coverage`:

.. code-block:: console

    $ cmake -DCMAKE_BUILD_TYPE=Coverage .


Then, execute the make target called `cecile_coverage`:

.. code-block:: console

    $ make cecile_coverage


If all is well, this will produce a browsable coverage report at 
`cecile_coverage/index.html`.

Contact
--------

If you have any questions, problems, suggestions, complaints, or compiments 
regarding _cecile_, please feel free to contact us at:

* Benjamin Rouxel <benjamin.rouxel@uva.nl>
* Stephen Nicholas Swatman <s.n.swatman@uva.nl>
* Julius Roeder <j.roeder@uva.nl>
* Clemens Grelck <c.grelck@uva.nl>




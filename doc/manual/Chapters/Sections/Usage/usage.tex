\section{Usage}

The general goal of this tool chain is to generate C-code that manages the run-time behaviour of an input set of components described by a high-level coordination language.

\subsection{Tool workflow}

Figure~\ref{fig:coordination-workflow} summarises the workflow to build a coordinated application. The four main inputs of the workflow are: 
\begin{enumerate}
	\item \textit{coordination file} (Section \ref{ssec:use:use:coord}): describes the application structure (components and their dependencies) as well as non-functional properties;
	\item \textit{config file} (Section \ref{ssec:use:use:conf}): provides practical configurations, e.g.~target hardware descriptions and security-level mission specifications, compiler passes to apply, etc;
	\item (optional)\textit{non-functional properties file(s)} (Section \ref{ssec:use:use:nfp}): timing and energy information provided by various external analysis tools, e.g. AbsInt aiT, Heptane, Absting Energy Analyser;
	\item \textit{object files}: contain compiled C-code for all components and their versions.
\end{enumerate} 


\begin{figure}[ht]
	\includegraphics[width=.9\textwidth]{Figures/Coord_workflow}
	\caption{\label{fig:coordination-workflow}Coordination compiler tool chain}
\end{figure}


\paragraph{Syntactic \& semantic analyses}

The first element of our tool chain, the \textit{Syntactic \& semantic analyses}, parses a \textit{coordination file} and a \textit{configuration file} to generate a first Intermediate Representation (IR) corresponding to the system model. This IR includes the set of components as well as the configuration for the target platform, the set of analyses and transformations selected and ordered within the \textit{configuration file}. Relevant analyses and transformations for this work shall be presented throughout this document.

\paragraph{Scheduling policy generation}

The second element of our tool chain, the \textit{Scheduling policy generator}, creates, with the help of the ETS information found in the NFP file, a second IR corresponding to the mapping and scheduling decided at this step. The \textit{configuration file} guides the scheduling policy generator to select the desired scheduling policy.
This either builds a schedule table for off-line scheduling or performs a schedulability analysis proving whether the component set is schedulable on the given platform or not.

\paragraph{Generate \& Export}

The third block of our tool chain, the \textit{Generator}, generates the C-code for the targeted hardware and Operating System (OS) specified in the \textit{configuration file}. Generated schedules are also passed to this code generation step, e.g.~dispatching schedule tables if off-line schedule is selected.
In addition, several exporters exist to output reports of different elements, i.e. the graph of the application in Dot format or the schedule information in a XML file.


\subsection{Invoking}

To get some help you can call CECILE with:
\begin{bashscript}{}
$ cecile --help
Allowed options:
-h [ --help ]         displays this help message.
--help-pass arg       displays a help message for a particular pass, invoke 
                      with "category:passid"
-s [ --show-pass ]    shows the list of all available compiler passes.
-c [ --config ] arg   sets the configuration file.
--coord arg           sets the TeamPlay coordination (.coord or .tey) file.
--nfp arg             sets the Non-Functional Properties file (.nfp).
-o [ --outdir ] arg   sets the global output directory for produced files.
--property arg        key:value property to replace in each input files. Can 
                      be specified multiple times.
-v [ --verbose ]      displays debugging messages in addition to other 
                      severity levels
--version             displays the version
\end{bashscript}

The `--show-pass' flag lists all compiler passes available, then to look for the particular help message of a pass `--help-pass' with an argument of the form `category:pass id', e.g. `--help-pass analyse-cycle'.

To help running CECILE on a large set of input files without to actually store all the files, it is possible to parametrise the input files, and then specify arguments with the `--property key:value' flag. CECILE will in turn look for the `key' in each input files, and replace it with the value.

Then, to invoke CECILE, at least the two input files \textit{TeamPlay coordination} (Section \ref{ssec:use:use:coord}) and \textit{configuration} (Section \ref{ssec:use:use:conf}) must be provided. A third optional file, the \textit{Non-Functional Properties} can be specified to provide external information about the timing, energy, security of each component, (Section \ref{ssec:use:use:nfp}). 

\begin{bashscript}{}
$ cecile --coord use-case.tey --config use-case.xml
\end{bashscript}

\paragraph{Running tests}

CECILE comes with a test suite that can be used to verify that the tool is behaving as expected. The test suite is contained in a binary called `cecile\_test' that is compiled alongside methane by default. Test target is generated with the proper CMake flag:

\begin{bashscript}{}
$ cmake -DCECILE_BUILD_TESTS=On ..
$ make cecile_test
$ ./dist/cecile_test
\end{bashscript}

\paragraph{Running the code coverage}

We support test coverage analysis using `gcov'. Note that this functionality is rather ham-fisted and might not work on operating systems other than Linux. It certainly doesn't work with compilers other than GCC. Note that you need to have both `gcov' and `lcov' installed for this to work. A coverage analysis target is generated if the build type is set to Coverage:

\begin{bashscript}{}
$ cmake -DCMAKE_BUILD_TYPE=Coverage ...
$ make cecile_coverage
$ ./dist/cecile_coverage
\end{bashscript}

If all is well, this will produce a browsable coverage report at cecile\_coverage/index.html.

\subsection{\label{ssec:use:use:coord}The TeamPlay language}

\input{Chapters/Sections/Theory/TeamPlayLanguage/language}

\subsection{\label{ssec:use:use:conf}The configuration file}

The configuration file is a XML file that describes the context of the target system.
\begin{xml}
<?xml version="1.0" ?>
<cecile>
    <architecture>
    	<processors>
    	</processors>
    </architecture>
    <datatypes>
    </datatypes>
    <!-- pass list -->
</cecile>
\end{xml}

First, some facts about the architecture are described with the list of computing units. Each computing unit is described in a `<processor />' XML tag with the following attribute:
\begin{itemize}
	\item `id' corresponds to a unique arbitrary ID given by the user,
	\item `type' corresponds to the precise type of core,
	\item `class' corresponds to the class of computing unit, accepted classes are: cpu/gpu/fpga,
	\item `system-id' is the system id that can be used when assigning code to a particular computing unit.
\end{itemize}
A complete example is given for a Odroid-XU4 platform.
\begin{xml}
<processors>
  <processor id="p1" type="arm7little" class="cpu" system-id="0" />
  <processor id="p2" type="arm7little" class="cpu" system-id="1" />
  <processor id="p3" type="arm7little" class="cpu" system-id="2" />
  <processor id="p4" type="arm7little" class="cpu" system-id="3" />
  <processor id="p5" type="arm7big" class="cpu" system-id="4" />
  <processor id="p6" type="arm7big" class="cpu" system-id="5" />
  <processor id="p7" type="arm7big" class="cpu" system-id="6" />
  <processor id="p8" type="arm7big" class="cpu" system-id="7" />
  <processor id="p9" type="mali" class="gpu" />
</processors>
\end{xml}

Second, some facts about data types can be described in order to generate a proper C-code. This is obviously optional if you don't want C code generation. This can be done by adding a `<datatype />' XML tag with the following attribute:
\begin{itemize}
	\item `id'  corresponds to the ID used in the TeamPlay language,
	\item `size' corresponds to the size in bytes,
	\item `ctype' the actual representation of the data type in C-C++ language,
	\item `initval' a value to initialise a newly declared variable with this type.
\end{itemize}
Any type declaration is accepted in the `ctype', e.g. pointers, arrays, class names, ... . However, if you intend to use a complex type involving a C++ template (e.g. std::vector<int>), you need to replace '<' by its XML entities `\&lt;', and '>' by `\&gt;'. This is a XML restriction.
\begin{xml}
<datatypes>
  <datatype id="frame" ctype="jpegFrame*" initval="NULL" />
  <datatype id="exif" ctype="ExifData*" initval="NULL" />
  <datatype id="integer" ctype="int" initval="-1" />
  <datatype id="vector_int" ctype="std::vector&lt;int&gt;*" initval="new vector&lt;int&gt;" />
</datatypes>
\end{xml}

Finally, the list of compiler passes that can be invoked are described in Section \ref{ssec:use:pass}. They will be executed in the reading order they are found in the XML configuration file.
For example the following first exports a dot representation of the input application, then performs a cycle check analyse, then schedules the application using the `energy'-aware scheduler, before generating the code for the YASMIN runtime.
\begin{xml}
	<exporter id="dot" />
	<analyse id="cycle" />
	<schedule id="energy" />
	<generator id="yasmin" />
	...
\end{xml}

\subsection{\label{ssec:use:use:nfp}The non-functional property file}

\input{Chapters/Sections/Theory/NFP}

%!TEX root=../../../../usermanual.tex
\subsection{Example use case reconnaissance drone}
\label{sec:use:theo:coord:drone}

We illustrate our coordination approach by means of a use case that we develop jointly with our project partners University of Southern Denmark and Sky-Watch A/S \cite{Seewald2019}.
Fixed-wing drones can stay several hours in the air, making them ideal equipment for surveillance and reconnaissance missions.
In addition to the flight control system keeping the drone up in the air, our drone is equipped with a camera and a payload computing system.
Since fixed-wing drones are highly energy-efficient, computing on the payload system does have a noticeable impact on overall energy consumption and, thus, on mission length.
We illustrate our coordination approach in Fig.~\ref{fig:usecase}; the corresponding coordination code is shown in Fig.~\ref{fig:usecase-code}.
We re-use the original application building blocks developed and used by Sky-Watch A/S.

\begin{figure*}[ht]
	\centering
	\includegraphics[width=0.9\textwidth]{Chapters/Sections/Theory/TeamPlayLanguage/figures/workflow_darknet_version2}
	\caption{Reconnaissance drone use case coordination model}
	\label{fig:usecase}
\end{figure*}

The drone's camera system takes pictures in predefined intervals.
Our \textit{ImageCapture} component represents this interface to the physical world.
Global period and deadline specifications correspond to the capture frequency of the camera.
The non-standard data types declared in the \texttt{datatypes} section of the coordination program are adopted from the original application code. 
We use the C types in string form for code generation and require that corresponding C type definitions are made available to the backend C compiler via header files.

Images are broadcast to three subsequent components.
The \textit{VideoEncryption} component encrypts the images of the video stream and forwards the encrypted images to follow-up component \textit{SaveVideo} that stores the video in persistent memory for post-mission analysis and archival.
Video encryption comes with three different security levels. 
For simplicity we just call them \textit{Encryption1}, \textit{Encryption2} and \textit{Encryption3}.
Different encryption levels could be used, for instance, for different mission environments, from friendly to hostile.


The drone also performs on-board analyses of the images taken.
These are represented by our components \textit{ObjectDetector} and \textit{GroundSpeed}.
Object detection can choose between three algorithms with different accuracy, time and energy properties:
Darknet\footnote{\url{https://pjreddie.com/darknet/}},
Tiny Darknet\footnote{\url{https://pjreddie.com/darknet/tiny-darknet/}},
OpenCV. %\footnote{\url{https://opencv.org/}}.
The ground speed estimator works by comparing two subsequent images from the video stream. 
This is the only stateful component in our model.
The results of object detection and ground speed estimation are synchronised and fed into the follow-up component \textit{Decision} that combines all information and decides whether or not to notify the base station about a potentially relevant object detected.

Transmission of the message is modelled by the sink component \textit{SendMessage}, where the action returns to the physical world.
To implement dynamic adaptation to dynamically changing mission phases, as sketched out in Section \ref{sec:use:theo:coord:coord:version}, we would need multiple versions of this component with different security levels as well.
However, we leave dynamic adaptation to future work for now.

\input{Chapters/Sections/Theory/TeamPlayLanguage/drone_example_dsl}

As Fig.~\ref{fig:usecase-code} demonstrates, our coordination language allows users to specify non-trivial cyber-physical applications in a concise and comprehensible way.
The entire wiring of components only takes a few lines of code.
Our approach facilitates playing with implementation variations and, thus, enables system engineers to explore the consequences of design choices on non-functional properties at an early stage. 
Note that all ports in our example have a token multiplicity of one, and we consistently make use of default ports where components only feature a single input port or a single output port.

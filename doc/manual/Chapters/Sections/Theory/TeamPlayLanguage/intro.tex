%!TEX root=../../../../usermanual.tex
\subsection{\label{sec:use:theo:coord:intro}Introduction}

Cyber-physical systems (CPS) deeply intertwine software with physical components, such as sensors and actuators that impact the physical world.
Broadly speaking the software controls the actuators of a physical system based on input from the sensors and specified policies.
Our world is full of cyber-physical systems, ranging from washing machines to airplanes. 
Designing secure, safe and correct cyber-physical systems requires a tremendous amount of verification, validation and certification. 

A common characteristic of cyber-physical systems is that non-functional properties of the software, such as time, energy and security, are as important for correct behaviour as purely functional correctness. 
Actuators must react on sensor input within a certain time limit, or the reaction might in the worst case become useless.
In addition to general environmental concerns, energy consumption of computing devices becomes crucial in battery-powered cyber-physical systems.
Security concerns are paramount in many cyber-physical systems due to their potentially harmful impact on the real world.
However, more security typically requires more computing effort. 
More computing effort takes more time and consumes more energy. 
Thus, time, energy and security are connected in the triangle of non-functional properties. 

The multi-core revolution has meanwhile also reached the domain of cyber-physical systems. 
A typical example is the ARM big.LITTLE CPU architecture that features four energy-efficient Cortex A7 cores and four energy-hungry, but computationally more powerful, Cortex A15 cores. 
Many platforms complement this heterogeneous CPU architecture with an on-chip GPU.
Such architectures create previously unknown degrees of freedom regarding the internal organisation of an application: what to compute where and when. This induces a global optimisation problem, for instance minimising energy consumption, under budget constraints, for instance in terms of time and security.

We propose the domain-specific functional coordination language TeamPlay and the associated tool chain that consider the aforementioned non-functional properties as first-class citizens in the application design and development process.
Our tool chain compiles coordination code to a final executable linked with separately compiled component implementations.
We combine a range of analysis and scheduling techniques for the user to choose from like in a tool box.
The generated code either implements a static (offline) schedule or a dynamic (online) schedule.
With static/offline scheduling all placements and activation times are pre-computed;
with dynamic/online scheduling certain decisions are postponed until runtime.

Both options are driven by application-specific global objectives. 
The most common objective is to minimise energy consumption while meeting both time and security constraints. 
A variation of the theme would be to maximise security while meeting time and energy constraints.
Less popular, but possible in principle, would be the third combination: minimising time under energy and security constraints.

Both offline and online scheduling share the concept of making conscious and application-specific decisions as to what compute where and when. 
Our work distinguishes itself from, say, operating system level scheduling by the clear insight into both the inner workings of an application and into the available computing resources.

The specific contribution of this paper lies in the design of the energy-, time- and security-aware coordination language and the overall approach.
Due to space limitations we can only sketch out the various elements of our tool chain and must refer the interested reader to future publications to some degree.


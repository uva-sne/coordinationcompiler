%!TEX root=../../../../usermanual.tex

To facilitate the the description of applications structure, we created a Domain Specific Language (DSL) called TeamPlay (extension \textbf{.tey}). A .tey file embeds the description of all components (a.k.a \emph{actors}, \emph{tasks}, \emph{nodes}) and their non-functional properties such as timing, energy constraints.

TeamPlay not only supports independent periodic tasks, which is the most common for real-time systems, but we also support graph-based task models where channels (a.k.a \emph{edges}, \emph{dependencies}) connect components. A channel defines a data exchange between a source and a sink through a FIFO channel. Such a data item, called \emph{token}, can have different types, from primitive types to more elaborate structures.
We require the described graph to be acyclic to form a Directed Acyclic Graph (DAG).

The complete current grammar is displayed in listing \ref{lst:use:use:lang}.

\subsubsection{Root element}

A minimal .tey file doing nothing is in listing \ref{lst:use:use:lang:mini}. It shows the keyword `app' to start the application MyExample, and `components' that will list the components.
\begin{tey}{}{lst:use:use:lang:mini}
app MyExample {
	components {
	}
}
\end{tey}
Some information can be given at the root level, as in listing \ref{lst:use:use:lang:root}. The period and deadline will be the default one for the whole following components, or for a graph it will be the period/deadline of the graph. The energy, at this level, corresponds to the available energy budget available for a period. Note that all these fields can be given in any order, and if they are present multiple times, only the last read one will override any other values.
\begin{tey}{}{lst:use:use:lang:root}
app MyExample {
	period=50Hz; /*default period*/
	deadline=40Hz; /*default deadline/
	energy=100kWh; /*available energy budget*/
}
\end{tey}

\subsubsection{Components \& Versions}

Then a list of independent components can be added as in listing \ref{lst:use:use:lang:comps}. Components ID (e.g. compA), must be unique across the .tey file, this ID is defined by you.
\begin{tey}{}{lst:use:use:lang:comps}
components {
	compA {
	}
	compB {
	}
	compC {
	}
}
\end{tey}

Depending on the type of information required to deploy the application, a component can be completely described as in listing \ref{lst:use:use:lang:comp}. Note that all of these information are optional and depends on your application and the analyses you want to apply, e.g. for a periodic task set you need to precise the period. Note that all these fields can be given in any order, and if they are present multiple times, only the last read one will override any other values.
\begin{tey}{}{lst:use:use:lang:comp}
compA {
	deadline=10ms; /*relative deadline at which the component should be completed*/
	period=10ms; /*activation period*/
	WCET=5ms; /*worst-case execution time*/
	WCEC=3kJ; /*worst-case energy consumption*/
	offset=10us; /*starting offset of the 1st period after time 0*/
	cname="concreteCfunction_compA"; /*concrete C function to call*/
	csignature=""; /*required extra parameters to call the function*/EXPERIMENTAL
	security=0; /*a security level*/
	tainted; /*a tainted component deals with sensitive data*/
}
\end{tey}
If no value is given for a particular field, then either we will try to find a hierarchically higher default value, or an exception will be raised by CECILE if a compiler pass needs a specific information that is not available. For the specific case of `cname', if no value is given, the default C function to call is set to the name of the component. 

Each component can embed of a set of task versions, as in listing \ref{lst:use:use:lang:versions}. The different versions of a task are functionally equivalent (i.e.they implement the same input/output relation), but differ in theirnon-functional properties. Different versions can be the result of:
\begin{enumerate}
	\item targeting different functional units (i.e. big core, LITTLE core or
	GPU); 
	\item using varying compilation flags to, for example, optimise
	code for energy consumption, binary size, speed or architecture
	features \cite{pallister2015identifying};
	\item different algorithms or implementation variants.

\end{enumerate}
All tasks in the task graph must beexecuted. However, only one version of each task is executed. Hence,the scheduler chooses the version to achieve the best trade-offbetween some criteria which depends on the selected scheduling strategy. Versions ID (e.g. v1), must be unique within a component but similar name can be reused between components (e.g. a compB can also have a version named v1), this ID is defined by you.
\begin{tey}{}{lst:use:use:lang:versions}
compA {
	...
	versions {
   		v1 {
   			WCET=1ms; /*worst-case execution time*/
   			WCEC=1J; /*worst-case energy consumption*/
   			arch="cpu"; /*mapping restriction*/
   			security=1; /*a security level*/
   			cname="concreteCfunction_compA_v1"; /*concrete C function to call*/
   			csignature=""; /*required extra parameters to call the function*/EXPERIMENTAL
   		}
	   	v2 {
		   	WCET=500us;
		   	WCEC=4J;
		   	arch="gpu";
		   	security=5;
		   	cname="concreteCfunction_compA_v2";
		   	csignature="";
	   }
	}
}
\end{tey}
Some information are redundant between versions and components, this allows us to override them at the version level, while keeping the possibility to have default values from the parent. Also, not all (even none) components may have versions in your application, duplicating the possibility to fill in these information at different levels allow us to be more versatile and support more task models, and keeping the backward compatibility with our own future evolutions. Similarly as before, not all the information at the version level are mandatory, and order do not matter.

If no value is given for a particular field, then either we will try to find a hierarchically higher default value, or an exception will be raised by CECILE if a compiler pass needs a specific information that is not available. For the specific case of `cname', if no value is given, the default C function to call is set to `componentID\_versionID'.

We can model two types of state for components: \begin{enumerate*}
	\item stateless;
	\item stateful.
\end{enumerate*}
Stateless components do not keep any information, or state, from their previous execution(s), while stateful ones do. This discrimination is necessary for the scheduling part as 2 instances of a stateless components can be executed in parallel, 2 instances of a stateful one can't as the 1st instance produces data useful for the 2nd one. While nothing special is required for stateless components, listing \ref{sec:use:use:lang:state} shows how to add a state. In this example 2 ports for the state are added, port `p1' is of type int and will store a single int. Port `p2' is a complex data type, and there will be an array of 10 of them stored across compA instances. The datatype is a ID which aliases of the concrete type which in turn can be defined in the configuration file, see Section \ref{sec:use:use:config}. More explanation on state can be found in Section \ref{sec:thy:coord}.
\begin{tey}{}{sec:use:use:lang:state}
compA {
	state {
		int p1;
		myComplexData p2[10];
	}
	...
}
\end{tey}


\subsubsection{(Multi-version) Components} 


A port specification includes a unique name, the token multiplicity and a data type identifier. 
Token multiplicities are optional and default to one.
They allow components to consume a fixed number of tokens on an input port at once, to produce a fixed number of tokens on an output port at once or to keep multiple tokens of the same type as internal (pseudo) state.
The firing rule for components is amended accordingly and requires (at least) the right number of tokens on each input port.
Typing ports is useful to perform static type checking and to guarantee that tokens produced by one component are expected by a subsequent component connected by an edge.
To start with we require type equality, but we intend to introduce some form of subtyping at a later stage,

\subsubsection{Dependencies}

Dependencies (or \emph{edges}, or \emph{channels}) represent the flow of tokens in the graph between components. To connect components, their description must include ports which represent their interface, as in listing \ref{sec:use:use:lang:dep:ports}.
The \emph{inports} specify the data items (or tokens) consumed by a component while the \emph{outports} specify the data items (or tokens) that a component (potentially) produces.
\begin{tey}{}{sec:use:use:lang:dep:ports}
compA {
	outports {
		int o1[10];
		myComplexData a;
	}
}
compB {
	inports {
		int i1[2];
	}
}
compC {
	inports {
		myComplexData a;
	}
}
\end{tey}
In this example 2 ports for each component are added, ports `o1' and `i1' will transmit/receive 10 tokens of type int. Port `a' from both component is a complex data type. The datatype is a ID which aliases of the concrete type which in turn can be defined in the configuration file, see Section \ref{sec:use:use:config}. More explanation on component interface can be found in Section \ref{sec:thy:coord}.

Then to connect components using their ports, the root level must include a `channels' part which must be after the component one as in listing \ref{sec:use:use:lang:dep}. Note that in the example the channel connecting compA to compB only displays the name of the port for compA as compB as a single port, it is not mandatory to specify it.

As in this example, the production and consumption of tokens can differ along an edge to form a specific task model (Synchronous Dataflow Graph -- SDF, see Section \ref{sec:thy:tm} for details). However extra compiler passes will be needed to homogenise the the production/consumption rate in order to generate a schedule.

\begin{tey}{}{sec:use:use:lang:dep}
app MyExample {
	components {...}
	channels { 
		compA.o1 -> compB;
		compA.a -> compC;	
	}
}
\end{tey}

We support a number of constructions to connect output ports to input ports.
In the following we illustrate each such construction with both a graphical sketch and the corresponding textual representation. 

\begin{figure}[ht]
\setlength{\columnseprule}{0.4pt}
\centering
\begin{multicols}{3}

\begin{minipage}{0.3\textwidth}
	\centering
	\newsavebox{\boxlstsimple}
	\begin{lrbox}{\boxlstsimple}
		\begin{simplelst}
A.x -> B.y;
		\end{simplelst}
	\end{lrbox}
	\subfloat[Simple edge\label{fig:use:use:coord:simple-edge}]{
		\hspace*{-22px}
		\begin{tikzpicture}
			\node at (0.2,1) {\includegraphics[width=0.6\columnwidth]{Chapters/Sections/Theory/TeamPlayLanguage/figures/edge_simple.pdf}};
			\node at (-0.3,0) {\usebox{\boxlstsimple}};
		\end{tikzpicture}
	}
\end{minipage}

\begin{minipage}{0.3\textwidth}
	\centering
	\newsavebox{\boxlstmult}
	\begin{lrbox}{\boxlstmult}
		\begin{simplelst}
A.x -> B.z;
A.y -> C.q;
		\end{simplelst}
	\end{lrbox}
	\subfloat[Multiple edges\label{fig:use:use:coord:multisink-edge}]{
	\hspace*{-22px}
		\begin{tikzpicture}
			\node at (0.2,1.7) {\includegraphics[width=0.6\columnwidth]{Chapters/Sections/Theory/TeamPlayLanguage/figures/edge_mult.pdf}};
			\node at (-0.2,0) {\usebox{\boxlstmult}};
		\end{tikzpicture}
	}
\end{minipage}

	\begin{minipage}{0.3\textwidth}
		\centering
	\newsavebox{\boxlstdup}
	\begin{lrbox}{\boxlstdup}
		\begin{simplelst}  
A.x -> B.y & C.z;
		\end{simplelst}
	\end{lrbox}
	\subfloat[Broadcast edge\label{fig:use:use:coord:dup-edge}]{
		\hspace*{-10px}
		\begin{tikzpicture}
			\node at (0,1.7) {\includegraphics[width=0.6\columnwidth]{Chapters/Sections/Theory/TeamPlayLanguage/figures/edge_dup.pdf}};
			\node at (0,0) {\usebox{\boxlstdup}};
		\end{tikzpicture}
	}
	\end{minipage}

\end{multicols}
\caption{Various edge construction examples}
\label{fig:use:use:coord:multifig1}
\end{figure}

Fig.~\ref{fig:use:use:coord:simple-edge} presents a simple edge between the output port~\texttt{x} of component~$A$ and the input port~\texttt{y} of component~\texttt{B}.
In our example the output port has a multiplicity of one token while the input port has a multiplicity of two tokens.
We show token multiplicities in Fig.~\ref{fig:use:use:coord:simple-edge} for illustration only.
In the coordination program token multiplicities are part of the port specification (Listing \ref{lst:use:use:lang}, line~\ref{lst:use:use:lang:port}), not the edge specification (line~\ref{lst:use:use:lang:edge:simple}).
Coming back to the example of Fig.~\ref{fig:use:use:coord:simple-edge}, component~\texttt{A} produces one output token per activation, but component~\texttt{B} only becomes activated once (at least) two tokens are available on its input port.
Thus, component~\texttt{A} must fire twice before component~\texttt{B} becomes activated.

Fig.~\ref{fig:use:use:coord:multisink-edge} shows an extension of the previous dependency construction where component $A$ produces a total of four tokens: one on port~\texttt{x} and three on port~\texttt{y}.
Component~\texttt{B} expects two tokens on input port~\texttt{z} while sink component~\texttt{C} expects a total of six tokens on input port~\texttt{q}.
These examples can be extended to fairly complex dependency graphs.

Fig.~\ref{fig:use:use:coord:dup-edge} shows a so-called \emph{broadcast edge} between a source component~$A$ producing one token and two sink components~$B$ and~$C$ consuming two tokens and one token, respectively (corresponding to Listing \ref{lst:use:use:lang},  line~\ref{lst:use:use:lang:edge:dup}). 
This form of component dependency duplicates the token produced on the output port of the source component and sends it to the corresponding input ports of all sink components.
Token multiplicities work in the very same way as before: any tokens produced by a source component go to each sink component, but sink components only become activated as soon as the necessary number of tokens accumulate on their input ports.
A broadcast edge does not copy the data associated with a token, only the token itself.  
Hence, components~$B$ and~$C$ in the above example will operate on the same data and, thus, are restricted to read access. 

Components with a single input port or a single output port are very common. In these cases port names in edge specifications can be omitted, as they are not needed for disambiguation.

\begin{figure}[hbt]
\setlength{\columnseprule}{0.4pt}
\centering
\begin{multicols}{3}
	\begin{minipage}{0.3\textwidth}
	\centering
\newsavebox{\boxlstdataor}
\begin{lrbox}{\boxlstdataor}
\begin{simplelst}
    A.x -> B.z
  | A.y -> C.q; EXPERIMENTAL
\end{simplelst}
\end{lrbox}
\subfloat[Data-dependent\label{fig:use:use:coord:dataor-edge}]{
\hspace*{-22px}
\begin{tikzpicture}
\node at (0,1.5) {\includegraphics[width=0.7\columnwidth]{Chapters/Sections/Theory/TeamPlayLanguage/figures/edge_or_data.pdf}};
\node at (-0.3,0) {\usebox{\boxlstdataor}};
\end{tikzpicture}
}
\end{minipage}

	\begin{minipage}{0.3\textwidth}
	\centering
\newsavebox{\boxlstschedor}
\begin{lrbox}{\boxlstschedor}
\begin{simplelst}
  A.x -> B.y | C.z; EXPERIMENTAL
\end{simplelst}
\end{lrbox}
\subfloat[Scheduler-dependent\label{fig:use:use:coord:schedor-edge}]{
\hspace*{-12px}
\begin{tikzpicture}
\node at (0,1.5) {\includegraphics[width=0.7\columnwidth]{Chapters/Sections/Theory/TeamPlayLanguage/figures/edge_or_sched.pdf}};
\node at (0,0) {\usebox{\boxlstschedor}};
\end{tikzpicture}
}
\end{minipage}

	\begin{minipage}{0.3\textwidth}
	\centering
\newsavebox{\boxlstenvor}
\begin{lrbox}{\boxlstenvor}
\begin{simplelst}
  A.x -> B.y
   where "Cexpr"
     | C.z
   where "Cexpr"; EXPERIMENTAL
\end{simplelst}
\end{lrbox}
\subfloat[Environment-dependent\label{fig:use:use:coord:envor-edge}]{
\hspace*{-22px}
\begin{tikzpicture}
\node at (0,1.9) {\includegraphics[width=0.7\columnwidth]{Chapters/Sections/Theory/TeamPlayLanguage/figures/edge_or_env.pdf}};
\node at (0,0) {\usebox{\boxlstenvor}};
\end{tikzpicture}
}
\end{minipage}

\end{multicols}
\caption{Data-, scheduler- and environment-dependent edges}
\label{fig:use:use:coord:multifig2}
\end{figure}

Fig.~\ref{fig:use:use:coord:dataor-edge} illustrates a data-driven conditional dependency (corresponding to Listing \ref{lst:use:use:lang}, line \ref{lst:use:use:lang:edge:dataor}). 
In this case, component $B$ and component $C$ are dependent on component~$A$, but only one is allowed to actually execute depending on which output port component~$A$ makes use of. 
If at the end of the execution of $A$ a token is present on port~$x$ then component~$B$ is fired; if a token is present on port~$y$ then component~$C$ is fired. 
If no tokens are present on either port at the end of the execution of~$A$ then neither~$B$ nor~$C$ are fired. 
This enables a powerful mechanism that can be used in control programs where the presence of a stimulus enables part of the application. 
For example, in a face recognition system an initial component in a processing pipeline could detect if there are any person on an image.
If so, the image is forwarded to the subsequent face recognition sub-algorithms; otherwise, it is discarded.

Fig.~\ref{fig:use:use:coord:schedor-edge} allows conditional dependencies driven by the scheduler (corresponding to Listing \ref{lst:use:use:lang}, line~\ref{lst:use:use:lang:edge:schedor}. 
Similar to the previous case, component $B$ and component $C$ depend on component~$A$, but only one is allowed to actually execute depending on a decision by the scheduler. 
For example, if the time budget requested by component~$B$ is lower than that requested by component~$C$, the scheduler can choose to fire component~$B$ instead of~$C$. 
Such a decision could be motivated by the need to avoid a deadline miss at the expense of some loss of accuracy.   

Fig.~\ref{fig:use:use:coord:envor-edge} allows conditional dependencies driven by the user (corresponding to Listing \ref{lst:use:use:lang}, line~\ref{lst:use:use:lang:edge:envor}). 
In this case components~$B$ and~$C$ again depend on component~$A$, but this time the dependency is guarded by a condition. 
If the condition evaluates to true then the token is sent to the corresponding route.
There is no particular evaluation order for conditions, and tokens are simultaneously sent to all sink components whose guards evaluate to true.
Like in the case of the broadcast edge all fired components receive the very same input data. 
If no guard returns \textit{true}, the token is discarded.

The guards come in the form of strings as inline C code.
The code generator will literally place this code into condition positions in the generated code.
The user is responsible for the syntactic and semantic correctness of these C code snippets.
This is not ideal with respect to static validation of coordination code, but similar to, for instance, the if-clause in OpenMP.
On the positive side, this feature ensures maximum flexibility in application design without the need for a fully-fledged C compiler frontend, which would be far beyond our means. 

For example, the \textit{Cexpr} could contain a  call to a function \texttt{get\_battery} that enquires about the battery charge status. 
The coordination program may choose to fire all subsequent components as long as the battery is well charged, but only some as the battery power drains.
Or, it may fire different components altogether, changing the system behaviour under different battery conditions.

\subsubsection{Templates component}

In order to have avoid multiple definitions of the same component, we have to decouple the component definition and their instantiation. To do this, we introduce an extra field on the top-level application definition as in listing \ref{lst:use:use:lang:templates}. 
\begin{tey}{}{lst:use:use:lang:templates}
app MyExample {
	templates {}
	components {}
	channels {}
}
\end{tey}

Components defined under this keyword are not automatically instantiated but serve as an abstract definition. These definitions have to be instantiated explicitly under the {components} keyword, as in listing \ref{lst:use:use:lang:templates:ex}. Here, we have a template {Sensors} including 2 components. Then, two {Sensors}: {SensorFront} and {SensorRear} ; are instantiated. Both are coupled to an extended {Decision} component.

\begin{tey}{}{lst:use:use:lang:templates:ex}
templates { 
	Sensors {
		outports {
			num distance;
			frame frameData;
		}
		components {
			ImageCapture {
				outports { frame outFrame; }
			}
			DistanceSensor {
				outports { num dist;  }
			}
		}
		channels {
			ImageCapture.outFrame -> out.frameData;
			DistanceSensor.dist -> out.distance;
		}
	}
}
components { 
	// Instantiations go here, components defined here are auto-instantiated
	Sensors SensorFront;
	Sensors SensorRear;
	
	Decision {
		inports {
			frame frameFront; num distanceFront;
			frame frameRear; num distanceRear;  
		}
	}
}
channels {
	SensorFront.distance -> Decision.distanceFront;
	SensorFront.frameData -> Decision.frameFront;
	SensorRear.distance -> Decision.distanceRear;
	SensorRear.frameData -> Decision.frameRear;
}
\end{tey}

The split between components and templates makes explicit to the programmer what is already instantiated and what is not. Components defined in the components keyword, are automatically instantiated while the templates have to be instantiated explicitly in components. This also reduces additional code overhead for smaller programs, as they can be automatically instantiated by placing their definitions directly into components.

\subsection{Parameters}
To satisfy the need for variations between instances of components, we introduce the passing of parameters to templates.

Parameters can be added in the template definition, as in listing \ref{lst:use:use:lang:params:ex}.  Arguments in instantiation level will be replaced by the value given during instantiation. If no parameters are desired, the brackets can be left out.

\begin{tey}{}{lst:use:use:lang:params:ex}
templates { 
	Sensors(deadline, callable1, callable2) {
		outports {
			num distance;
			frame frameData;
		}
		deadline=$deadline;
		components {
			ImageCapture {
				outports { frame outFrame; }
				cname=$callable1;
			}
			DistanceSensor {
				outports { num dist;  }
				cname=$callable2;
			}
		}
		...
	}
}
components { 
	// Instantiations go here, components defined here are auto-instantiated
	Sensors SensorFront(10ms, "cfuncNameFrontCap", "cfuncNameDist");
	Sensors SensorRear(5ms, "cfuncNameRearCap", "cfuncNameDist");
}
\end{tey}

\subsubsection{Basic sub-networks}
We introduce the creation of sub-network components. This helps us to enable users to reuse sub-networks of components in different parts of a program. 

\begin{figure}[ht]
	\centering
	\includegraphics[scale=0.8]{Figures/subgraph_example.pdf}
	\caption{Example of sub-networks. The edges originating from {ImageCapture} and {DistanceSensor} are presented towards the outside of the graph. From the outside of the {Sensors} sub-network, two edges go to the {Decisision} component. The {Decision} calculates a voltage goes into the sub-network. From the edge of the {Actuators} sub-network, a duplicate edge copies the same voltage token to the {LeftActuator} and {RightActuator}.}
	\label{fig:subgraph-example}
\end{figure}

Basic sub-networks are defined within the {components} section and can contain one or more components. Figure~\ref{fig:subgraph-example} shows a schematic example of a sub-network, which is detailed in code in listing \ref{lst:use:use:lang:subcomp}. The {Sensors} sub-network is defined with two components: {ImageCapture} and {DistanceSensor}. In these components, it is again possible to define inline settings. These sub-networks are treated similarly to components as they have the same outports as normal components and they are automatically instantiated. Everything that is defined under the components keyword is instantiated under the name it has been given. The major difference between the interface of a normal component and a sub-network is that in sub-networks, the programmer has to specify when a channel should be presented towards the outside of the sub-network. To connect the inner components to the container one, the \emph{out} keyword can be used instead of the component name. Similarly The \emph{in} keyword can be used to allow a channel from the inport of the sub-network to connect to a component inside the sub-network. 

The advantage of specifying the edges inside the sub-network is that they are self-contained units. Thus they can easily be copied in other parts of the program or even other programs entirely, provided the used types also exist there. 

\begin{tey}{}{lst:use:use:lang:subcomp}
components {
	Sensors {
		outports {
			num distance;
			frame frameData;
		}
		components {
			ImageCapture {
				outports { frame outFrame; }
			}
			DistanceSensor {
				outports { num dist;  }
			}
		}
		channels {
			ImageCapture.outFrame -> out.frameData;
			DistanceSensor.dist -> out.distance;
		}
	}
	Actuators {
		inports { num voltage; }
		LeftActuator {
			inports { num voltage; }
		}
		RightActuator {
			inports { num voltage; }
		}
		channels {
			// 'in' refers to the Actuators subgraph
			in.voltage -> LeftActuator.voltage & RightActuator.voltage
		}
		
	}
	Decision {
		inports { frame frameData; num dist; }
		outports { num voltage; }
	}
}
channels {
	Sensors.dist -> Decision.dist
	Sensors.frameData -> Decision.frameData
	Decision.voltage -> Actuators.voltage
}
\end{tey}

\subsubsection{Fault-tolerance}

Some components or groups of components may be more important than others, depending on the hardware target, domain, application, and other factors. We opt for a user-directed approach where the user can specify which of the predefined options to apply in different parts of the application. This is due to major semantic challenges in having a compiler or scheduler analyse the criticality of a component in the application as a whole. Furthermore, the way fault-tolerance is implemented and achieved needs to be transparent to the programmer in order to make sure they they fit the application requirements.

For more information on Fault-tolerance, checkout Section \ref{sec:thy:fault-tolerance}.

\paragraph{Checkpoint/restart}
This applies only to stateless components, i.e., the state of the program is saved in the FIFO buffers that exist on the edges between the components. Thus, we can create checkpoints at component granularity by copying the buffers between the components. These components are theoretically more lightweight than system level checkpoints, thus removing the need for multi-level checkpointing. This can be done without having to coordinate and synchronise the checkpointing of hardware components to find a stable checkpoint, i.e., a domino effect cannot occur.

The benefits of using checkpoint/restart are (potentially) three-fold: implementation is straightforward, coordination between hardware components is not needed, and it only requires extra memory and copy time and no redundant active components. The last reason is specific to the destination hardware and application.

Listing \ref{lst:use:use:lang:ft:checkpoint} shows how checkpoint/restart can be specified on a component. The option specified is the default option, meaning that if left empty, the default value will be taken. Currently, our specification of checkpoint/restart has no supported options.

\begin{tey}{}{lst:use:use:lang:ft:checkpoint}
compA {
	inports { ... }
	outports { ... }
	checkpoint {}
}
\end{tey}

\paragraph{Primary-backup}
In primary-backup, the state of the primary and standby component is synchronised. The degree of this synchronisation depends on which flavour, cold, warm or hot is implemented. This allows the application to quickly switch outputs when a fault is detected. In TeamPlay, the firing of a component is discrete, i.e., every execution produces output tokens only once. This, together with the fact that state is made explicit in the buffers of the edges, means that it is not necessary to run the primary and standby components at the same time, i.e., they do not have to be synchronised. We can simply provide copies of the input tokens to the hardware units and take the first unit who delivers an output as the primary component. If it fails, one of the standby components will deliver output instead. This makes this method predictable as one does not have to account for switching from the primary to the replica component or synchronisation mechanisms.

Listing \ref{lst:use:use:lang:ft:prima} shows the way primary-backup can be specified, the options specified are again, the default options. In primary-backup the {replicas} option can be specified as an integer signifying the number of replicas to run for this component.

\begin{tey}{}{lst:use:use:lang:ft:prima}
compA {
	inports { ... }
	outports { ... }
	standby {
		replicas=2;
	}
}
\end{tey}

\paragraph{N-Modular redundancy}

N-Modular redundancy (NMR) runs $n$ identical instances followed by a voting process to reduce the risk of deviating components caused by faults~\cite{lyons1962use, tanenbaum2007distributed, gamer2013fault}. NMR provides a straightforward mechanism that allows us to deal with transient faults without employing low level (hardware) error detection techniques. Furthermore, NMR is a time-predictable method, i.e., it is suitable for use in real-time systems~\cite{poledna2007fault}.Listing \ref{lst:use:use:lang:ft:nmr} shows the defaults of the N-modular redundancy. We support the following options:

\begin{itemize}
	\item \emph{replicas} (line 6), integer signifying the number of replicas. Default is \emph{3}, meaning a TMR setup.
	\item \emph{votingReplicas} (line 7), integer signifying whether and how much the voting processes need to be replicated.
	\item \emph{waitingTime} (line 8), how long processes should wait before initiating the voting process. Given as a percentage of the average execution time of the finished components, the percentage can be higher than \emph{100\%}.
	\item \emph{waitingStart} (line 9), defines the starting point of waiting. When \emph{waitingStart} is \emph{majority}, processes start waiting based on the execution time when a majority of processes are done. In the case of \emph{single}, the waiting will start when a single process is ready. 
	\item \emph{waitingJoin} (line 10), boolean defining whether processes that are finished later should be added in the waitingTime calculation. Can apply on both a \emph{waitingStart} value of \emph{majority} and \emph{single}.
\end{itemize}

\begin{tey}{}{lst:use:use:lang:ft:nmr}
compA {
	inports { ... }
	outports { ... }
	nModular {
		replicas=3;
		votingReplicas=2;
		waitingTime=30%;
		waitingStart=majority;
		waitingJoin=true;
	}
}
\end{tey}

\subsection{N-version programming}
N-version programming (Section~\ref{background:nvp}) runs $n$ different processes conforming to the same specification~\cite{peng2010building}. The coordination language already has the concept of versions. We aim to let the scheduler decide which implementation to use regarding mission state and global objectives set forth. This results in higher efficiency since a specific objective is targeted, instead of general operation. For example, it is possible to have multiple versions of a component with different security levels. The problem could be that a more hardened component could use more energy, thus if the mission state is less critical it may be worthwhile to use the less hardened version. Furthermore, these versions can be architecture-specific, meaning that a certain version can only be run on a component featuring both a CPU and GPU while another version may need a specific CPU-architecture.

The main disadvantage of using NVP is that a programmer would need to provide multiple implementations of the same functionality. However, by leveraging this existing version mechanic, the programmer can kill two birds with one stone by using the same versions for the non-functional properties specification and NVP. Using NVP may also provide fault-tolerance for software faults. A situation could occur where one version of the software solves a particular edge-case and another version does not.

The options we support in NVP are similar to NMR adding an option to specify \emph{versions}, which defines which versions should be used and how many of each of these versions should be run. This is displayed in listing \ref{lst:use:use:lang:ft:nvp}. This removes the need for the \emph{replicas} option.

\begin{tey}{}{lst:use:use:lang:ft:nvp}
compA {
	inports { ... }
	outports { ... }
	versions {
		v1 {
			security=4;
		} 
		v2 {
			security=6;
		} 
		v3 {
			security=9;
		} 
	}
	nVersion {
		versions {
			v1[2];
			v2[1]; 
		}
	} 
}
\end{tey}

\paragraph{Profiles}

In order to provide intuitive structures while keeping duplication low, we pursue the idea of using profiles. These profiles are defined globally and can be added to components to prevent the user from having to specify all options again to components which require similar fault-tolerance treatment. Listing \ref{lst:use:use:lang:prof} shows an example of adding profiles. The global profile, \emph{TMR} adds triple-modular-redundancy and  is defined in the \emph{profiles} keyword at the root level, and applied to the \emph{compA} component. All profiles found in a component are applied in reading order, thus overwriting any duplicate options. In the example, the replicas from TMR is overwritten by the replicas from DMR, while still keeping the prof2 active and the votingReplicas from TMR. In the same spirit, it is possible to add more information, or overwrite some by giving more fault-tolerance information at the component level.

\begin{tey}{}{lst:use:use:lang:prof}
app MyExample {
	profiles {
		TMR {
			nModular {
				replicas=3;
				votingReplicas=1;
			}
		}
		DMR {
			nModular {
				replicas=2;
			}
		}
		prof2 {
			checkpoint {}
		}
	}
	components {
		compA {
			profile "TMR";
			profile "prof2";
			profile "DMR";
			nVersion { ... }
		}
	}
}
\end{tey}

\paragraph{Controlling cascading options}
In general-purpose languages, there are keywords that can be used to communicate certain intentions towards other programmers. For example to indicate that a function needs to implemented, or that that a variable cannot be overwritten. To this end, we introduce two keywords to allow more expressiveness and control over options: \emph{remove} and \emph{vital}. 

\emph{remove} can be placed before a (fault-tolerance) option to signify that a method should be removed if it is specified. \emph{remove} can be used when the programmer wants to cascade certain options using multiple profiles but not all. For now, we only allow removes on the first layer keywords of the fault-tolerance options and normal settings. When cascading multiple profiles the remove is handled per singular cascade. For example, given three profiles, the first of which adds \emph{nModular} with \emph{replicas 5}, the second profile removes \emph{nModular} while the third one adds it again without specified options, i.e., the default settings will be used. In handling the cascade of the first and second profile, \emph{nModular} will be deleted. When the result of the first cascading operation is cascaded with the third and final profile, \emph{replicas} will be equal to 3.

The \emph{vital} keyword signifies an option that is not allowed to be changed by cascading or removal. Listing \ref{lst:use:use:lang:ft:vitrm} shows \emph{remove} and \emph{vital}. If we were to have a component that has these profiles in the same order they are defined, i.e., \emph{profiles TMR, TMRRedundantVoting, RemoveNModular} the implementation produces an error that the option with the vital keyword cannot be overwritten, nor can it be removed by the \emph{remove} keyword.

\begin{tey}{}{lst:use:use:lang:ft:vitrm}
profiles {
	TMR {
		nModular {
			vital votingReplicas=1; // will not be overwritten by cascading profiles
		}
	}
	TMRRedundantVoting {
		nModular {    
			votingReplicas=3;
		}
	}
	removeNModular {
		remove nModular;
	}
}
\end{tey}

\subsubsection{The complete current language}

Listing \ref{lst:use:use:lang} presents the grammar of our coordination language written in pseudo-Xtext style.

\input{Chapters/Sections/Theory/TeamPlayLanguage/CompleteLanguage.tex}

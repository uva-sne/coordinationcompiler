%!TEX root=../../../../usermanual.tex
\subsection{Coordination model}
\label{sec:use:theo:coord:coordination}

The term \emph{coordination} goes back to the seminal work of Gelernter and Carriero \cite{GeleCarr92} and their coordination language Linda.
Coordination languages can be classified as either \emph{endogenous} or \emph{exogenous} \cite{Arbab06}.
Endogenous approaches provide coordination primitives within application code.
The original work on Linda falls into this category.
Exogenous approaches fully separate the concerns of coordination programming and application programming

We pursue an exogenous approach and foster the separation of concerns between intrinsic component behaviour and extrinsic component interaction.
The notion of a component is the bridging point between low-level functionality implementation and high-level application design.

\paragraph{Components}
\label{sec:use:theo:coord:coord:comp}

We illustrate our component model in Fig.~\ref{fig:component}.
Following the keyword \texttt{component} we have a unique component name that serves the dual purpose of identifying a certain application functionality and of locating the corresponding implementation in the object code. 


A component interacts with the outside world via component-specific numbers of typed and named input ports and output ports.
As the Kleene star in Fig.~\ref{fig:component} suggests, a component may have zero input ports or zero output ports.
A component without input ports is called a \emph{source component}; 
a component without output ports is called a \emph{sink component}.
Source components and sink components form the quintessential interfaces between the physical world and the cyber-world representing sensors and actuators in the broadest sense.
We adopt the firing rule of Petri-nets, i.e.~a component is activated as soon as data (tokens) are available on each of its input ports.

\begin{wrapfigure}{r}{.5\textwidth}
	\centering
	\includegraphics[width=.5\textwidth]{Chapters/Sections/Theory/TeamPlayLanguage/figures/component.pdf}
	\caption{Illustration of component model}
	\label{fig:component}
	\vspace*{-3mm}
\end{wrapfigure}

Technically, a component implementation is a function adhering to the C calling and linking conventions \cite{ritchie1988c}. Name and signature of this function can be derived from the component specification in a defined way.
This function may call other functions using the regular C calling convention.
The execution of a component (function), including execution of all subsidiary regular functions, must be free of side-effects. In other words, input tokens must map to output tokens in a purely functional way.
Exceptions are source and sink components that are supposed to control sensors and actuators, respectively.

\paragraph{Stateful components}
\label{sec:use:theo:coord:coord:state}

Our components are conceptually stateless.
However, some sort of state is very common in cyber-physical systems. 
We model such state in a functionally transparent way as illustrated in Fig.~\ref{fig:component}, namely by so-called state ports that are short-circuited from output to input.
In analogy to input ports and output ports, a component may well have no state ports.
We call such a component a (practically) \emph{stateless} component.

Our approach to state is not dissimilar from main-stream purely functional languages, such as Haskell or Clean.
They are by no means free of state either, for the simple reason that many real-world problems and phenomena are stateful. 
However, purely functional languages apply suitable techniques to make any state fully explicit, be it monads \cite{Wadler92} in Haskell or uniqueness types \cite{AchtPlas95} in Clean.
Making state explicit is key to properly deal with state and state changes in a declarative way.
In contrast, the quintessential problem of impure functional and even more so imperative languages is that state is potentially scattered all over the place. 
And even where this is not the case in practice, proving this property is hardly possible.

\paragraph{ETS-aware components}
\label{sec:use:theo:coord:coord:ets}

We are particularly interested in the non-functional properties of code execution.
Hence, any component not only comes with functional contracts, as sketched out before, but additionally with non-functional contracts concerning energy, time and security (and potentially more in the future).

These three non-functional properties are inherently different in nature.
Execution time and energy consumption can be measured, depend on a concrete execution machinery and vary between different hardware scenarios.
In contrast, security, more precisely algorithmic security, depends on the concrete implementation of a component, for example using different levels of encryption, etc.
However, different security levels almost inevitably incur different computational demands and, thus, are likely to expose different runtime behaviour in terms of time and energy consumption as well.

Knowledge about non-functional properties of components is at the heart of our approach.
It is this information that drives our scheduling and mapping decisions to solve the given optimisation problem (e.g.~minimising energy consumption) under constraints (e.g.~execution deadlines and minimum security requirements).

\paragraph{Multi-version components}
\label{sec:use:theo:coord:coord:version}

As illustrated in Fig.~\ref{fig:component_var}, a component may have multiple versions with identical functional behaviour, but with different implementations and, thus, different energy, time and (possibly) security contracts.
Multi-version components add another degree of freedom to the scheduling and mapping problem that we address: selecting the best fitting variant of a component under given optimisation objectives and constraints.



\begin{wrapfigure}{l}{.5\textwidth}
    \centering
%    \vspace*{-4mm}
    \includegraphics[width=.5\textwidth]{Chapters/Sections/Theory/TeamPlayLanguage/figures/component_var.pdf}
    \caption{Multi-version component with individual energy, time and security contracts}
    \label{fig:component_var}
    \vspace*{-2mm}
\end{wrapfigure}

Take as an example our reconnaissance drone use case, that we will explore in more detail in Section~\ref{sec:use:theo:coord:drone}.
A drone could adapt its security protocol for communication with the base station in accordance with changing mission state: low security while taking off or landing, medium security while navigating to/from  mission area, high security during mission.
Continuous adaptation of security levels results in less computing and, thus, in energy savings that could be exploited for longer flight times.

Our solution is to provide different versions of the same component (similar to \cite{rusu2005multi}) and to select the best version regarding mission state and objectives based on the scheduling strategy.
For the time being, we only support off-line version selection, but scenarios with online version control, as sketched out above, are on our agenda.

\paragraph{Component interplay}
\label{sec:use:theo:coord:coord:interplay}


Components are connected via FIFO channels to exchange data, as illustrated in Fig.~\ref{fig:component_net}.
Depending on application requirements, components may start computing at statically determined time slots (when all input data is guaranteed to be present) or may be activated dynamically by the presence of all required input data.
Components may produce output data on all or on selected output ports.

\begin{figure*}[hbt]
    \centering
    \includegraphics[width=.97\textwidth]{Chapters/Sections/Theory/TeamPlayLanguage/figures/component_net.pdf}
    \caption{Illustration of data-driven component interplay via FIFO channels}
    \label{fig:component_net}
\end{figure*}

%!TEX root=../../../usermanual.tex

\section{Task model}

A task model, a.k.a workload model, describes the properties of a real-time application. 
At a coarse grain, tasks compose the \textit{application}. A \textit{task} corresponds to a piece of code. At a finer grain, tasks infer task instances, known as \textit{jobs}. Each job corresponds to an instance of a task that is effectively scheduled and executed on the platform. A task can generate multiple jobs depending on its timing properties and the studied time window. Jobs are ordered and job $i$ must complete before job $i+1$.
%A \textit{job} might be an extracted part from the code of its corresponding task, or be a full copy. 
Depending on the level of abstraction and expressiveness of further task models, properties defined for tasks and/or jobs include : 
\begin{itemize}
	\item WCET: an upper bound  of any possible execution time, 
	\item period: the frequency at which the task/job is ready for execution, 
	\item deadline: the time at which the execution must be complete, 
	\item predecessors/successors: execution order constraints.
\end{itemize}

%Each job from a same task can share the same properties, or have their own specificities usually represented as a vector where the firing sequence determines the vector order. When all properties of a job are identical and the task model allows it, it is a common practice to remain at the higher level of abstraction, the task level.

Differences between task models essentially come from the amount of exhibited information (expressiveness), the amount of applications it can represent (flexibility), and the applied generalisation (abstraction level).
This section does not attempt to be exhaustive as the multiplicity of task models is tremendous, but it covers the wide range of possibilities. For a list of other task models, the reader is advised to look into \cite{stigge2015graph}.

%\paragraph{Asynchronous/Synchronous task-set}
%When all tasks of a task-set have the same first release time, e.g. $0$, the task-set is \textit{synchronous}. While it is \textit{asynchronous} when first release times are not equal. 

%\paragraph{Periodic/Sporadic/Aperiodic task}
The seminal work from Liu and Layland \cite{liu1973scheduling} introduced the periodic task model. It represents applications where jobs arrival times appear at a known and strict frequency after the first one (defined at designed time). Then, each job must terminate before a deadline relative to its arrival time. The lack of flexibility in term of restrictive periodicity was latter relaxed by Mok \cite{mok1983fundamental} with sporadic tasks. This allows releasing tasks at later time point as long as at least a minimum time interval as elapsed between two firings (pseudo-period). Both cases are said periodical task models as they generate an infinite sequence of job instances, released periodically. In addition  each job must end before the next one is released. 
%Mok \cite{mok1983fundamental} also presented a transformation from sporadic to periodic task model.

%\paragraph{Constraint/Implicit/Arbitrary Deadline}
Three other sub-categories classify the aforementioned task models depending on the deadline property for the entire tasks set. If for all tasks the deadline is equal to the period \cite{liu1973scheduling}, the tasks set is \textit{implicit}. If all deadlines are inferior or equal to the period then the category is \textit{constrained} deadlines, while \textit{arbitrary} deadline is otherwise used \cite{mok1983fundamental}.


%\paragraph{Multi-rate task}
Above-mentioned task models lack of flexibility. As an example, a MPEG decoder sequentially receives, decodes and displays video frames. In such codec, a frame is periodically bigger than others, and thus requires more computational time. Applying the same task/function to all frames results in an over-provisioning of the system as most frame computation need less processor time. The Multi-Frame model \cite{mok1996multiframe,} and its Generalisation (GMF) \cite{baruah1999generalized} allow to configure different properties, such as WCET estimates, per jobs originating from an identical task.

\begin{wrapfigure}{r}{.5\textwidth}
	\centering
	\includegraphics[scale=0.3]{Chapters/Sections/Usage/figures/DAG.pdf}
	\caption{Example of an application represented by a DAG}
	\label{chap1:fig:dag}
\end{wrapfigure}

Another class of embedded real-time systems concerns signal processing applications. They mainly focus on images, sounds or any digital signals, e.g. a wireless router, surveillance camera, \ldots. This type of application processes an uninterrupted flow of information. The expressiveness of seminal periodic task model does not capture this flow of information which is transmitted from task to task. In \cite{ackerman1982data}, Ackerman expressed the \textit{data-flow program graph} task model, known as Directed Acyclic Graph (DAG) when no cycle are present. This representation increases the expressiveness of the task-model by exhibiting dependencies between tasks, thus exposing the parallelism of the application. In data-flow graphs, nodes represent computations (tasks) and edges represent communications between tasks. An edge is present when a task is causally dependent on another one, meaning the source of the edge needs to complete prior to run the target. The edge corresponds to a First In First Out (FIFO) channel where the source produces a certain amount of tokens, and the sink consumes all of them. An example is presented by Figure \ref{chap1:fig:dag} where labels on edges represent the number of tokens. 

Similarly to periodic task models, a data-flow graph instance is called an \textit{iteration} and a job is a task instance inside an \textit{iteration}. Then, the DAG may iteratively executes until the end of time (or platform is shutdown). Hence, jobs execution order follows aforementioned constraint, job $i$ finished before job $i+1$. But, the iteration $j+1$ can start before the completion of iteration $j$ as long as jobs dependencies are satisfied. This allows to exploit job parallelism, i.e. pipelining \cite{tendulkar2014many}.

In data-flow graphs, timing properties (e.g. period, deadline) can be attached to graph itself and not anymore stated for each task and job. All tasks must therefore complete their execution between the release time and the deadline of the whole graph. A multi-task application is then a multi-DAG application as in \cite{perret2017pred}.

\begin{figure}[ht]
	\setlength{\columnseprule}{0.4pt}
	\centering
	\begin{multicols}{3}
		\begin{minipage}{0.3\textwidth}
	\centering
	\subfloat[Example of a SDF\label{chap1:fig:sdf}]{
		\hspace*{-0.3cm}
		\includegraphics[scale=0.3]{Chapters/Sections/Usage/figures/SDF}
		
	}
		\end{minipage}

	\begin{minipage}{0.3\textwidth}
	\subfloat[HSDF version of Figure \ref{chap1:fig:sdf}\label{chap1:fig:hsdf}]{
		\includegraphics[scale=0.3]{Chapters/Sections/Usage/figures/HSDF}
	}
	\end{minipage}
	
	\begin{minipage}{0.3\textwidth}
	\vspace*{0.5cm}
	\subfloat[One possible PEG from the SDF in \ref{chap1:fig:sdf}\label{chap1:fig:peg}]{
		\centering
		\includegraphics[scale=0.3]{Chapters/Sections/Usage/figures/PEG}
	}
	\end{minipage}
	\end{multicols}

	\caption{\label{chap1:fig:tm}An SDF example and its transformation to HSDF and one possible PEG}
\end{figure}

%\paragraph{SDF/partialSDF/HSDF/CSDF graph}
In general DAGs, edge sinks consume all tokens produced by the corresponding source in one job execution. To overcome this limitation, Synchronous Data-Flow (SDF) graphs \cite{lee1987static} allow different production/consumption rates between two actors of an edge. An additional constraint on SDF forces the amount of transiting tokens to be known at compile time which allows static analysis on the graph, see Figure \ref{chap1:fig:sdf}.
% To reuse knowledge on classic periodical task model, Bamakhrama et al. \cite{bamakhrama2013hard} defined a method to transform SDF into sporadic tasks.
% Despite the loss of information it is possible to transform a SDF into more common task models. 

Due to different rates of production and consumption of tokens on an edge, SDF graphs need an expansion pass prior to be scheduled. The larger expansion builds an Homogeneous Data-Flow graphs (HSDF) \cite{lee1987static}, where all production/consumption rates are equal to $1$ (there is as much tokens produced than consumed). Despite of the exponential complexity when expanding SDFs, HSDF representations are required \cite{de2013back} to determine, \textit{a priori}, the amount of node repetitions and all number of transmitted tokens. Figure \ref{chap1:fig:hsdf} presents the example HSDF obtained after expanding the SDF from Figure \ref{chap1:fig:sdf}. 


%Prior to analyse the timing behaviour of a SDF graph, the graph is expanded to its homogeneous version \cite{de2013back}, called Homogeneous Data-Flow graphs \cite{lee1987static}. 
%A HSDF is a specialisation of its corresponding SDF where the consumption/production rate is equal between the source and the sink of an edge, similar to aforementioned DAG. To build this representation, nodes must be duplicated as presented by Figure \ref{chap1:fig:hsdf} that specializes the SDF in Figure \ref{chap1:fig:sdf}. 
%Despite of the exponential complexity when building such representation, a HSDF is analysable in polynomial time -- e.g. computing the throughput\footnote{The throughput of a graph corresponds to the amount of tokens generated by sink nodes in a time window.} and a static schedule \cite{dasdan2004experimental}. 

%Considering a specialisation of SDF, Homogeneous Data-Flow graphs \cite{lee1987static} guarantee that the number of tokens along each edges of the graph is equal to $1$. In Figure \ref{chap1:fig:hsdf}, some edges have 2 tokens, to have the full HSDF it is simple as duplicated the edge which is not done here for clarity. Lee et al. \cite{lee1987static} also demonstrate that for any SDF there is always a corresponding HSDF with an exponential increase of vertices and edges

Due to the inherent complexity of building a HSDF, middle size graph representations have been proposed by Zacki \cite{zaki2013scalable}. Partial Expansion Graph (PEG) exposes more parallelism than SDF, with potentially less tasks than HSDF. Therefore, using strength from both initial representations.

\begin{wrapfigure}{r}{.5\textwidth}
	\centering
	\includegraphics[scale=0.3]{Chapters/Sections/Usage/figures/forkjoin}
	\caption{Expanded example of a fork-Join graph}
	\label{chap1:fig:forkjoin}
\end{wrapfigure}

%\paragraph{Fork-Join graph}
In streaming applications, fork-join graphs \cite{thies2002streamit} correspond to an adaptation of SDF graphs. Identically to SDF, they can include different production/consumption rates. They also need to be expanded for further analyses. In contrast to SDF, all actors, except specific fork and join nodes, can have one and only one predecessor and successor. 
%This shape of graph can ease WCET and WCRT analyses due to constraints on successors and predecessors. 
Figure \ref{chap1:fig:forkjoin} exhibits an expanded version of a sample fork-join graph.
%But to create complex software, adding fork/join nodes can lead to a huge amount of tasks and thus increasing the complexity of the solved problem.

\begin{wrapfigure}{r}{.5\textwidth}
	\centering
	\includegraphics[scale=0.3]{Chapters/Sections/Usage/figures/CSDF}
	\caption{Example of a CSDF}
	\label{chap1:fig:csdf}
\end{wrapfigure}

Applying the multi-rate idea from GMF to SDF, Bilsen et al. \cite{bilsen1996cycle} introduced the Cyclo-Static Data-Flow graph (CSDF). CSDF graphs allow to have different production/consumption rates within a period to jobs of the same task. The example from Figure \ref{chap1:fig:csdf} present a CSDF where, for example, actor $C$ has a firing rate of $(2,1)$ on its output edge. Then actor $C$ alternatively produces $2$ tokens then $1$ token, then $2$ \ldots . 

%\paragraph{Directed Acyclic Graph}

%\paragraph{multi-DAG}

Synchronous Data-Flow applications can be represented with different languages, such as Esterel \cite{berry1984esterel}, StreamIT \cite{thies2002streamit} or Prelude \cite{pagetti2011multi}. They all have their specificities, Prelude targets multi-periodic synchronous system, while StreamIT generates fork-join graphs targeting streaming applications. All help building parallel applications represented by graphs. %Programming languages will not be discussed anymore as they are out of the scope of this dissertation.

These DAGs do not necessarily need to be built from scratch, which would require an important engineering effort. 
%In one hand, Derrien et al. \cite{derrien2017wcet} presented an automatic extraction of parallelism from a high level description of applications in model based design workflows.
It is possible to extract tasks from legacy sequential code as in \cite{floc2013gecos, cordes2012multi, cordes2013automatic}.

The literature abounds of other graph-based task model, e.g. Hierarchical Task Graph \cite{girkar1994hierarchical}, Dynamic Data Flow graphs \cite{bhattacharyya2013dynamic}. Only the most common were presented here and not all of them are suitable for critical applications.
An attractive task model for real-time system lies in its expressiveness (no ambiguities, expose parallelism) without too much flexibility (concise) and too high abstraction (remain implementable), therefore allowing proof of timing behaviour.
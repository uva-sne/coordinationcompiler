\section{Fault-tolerance techniques}
\label{sec:thy:fault-tolerance}
In order to provide fault-tolerance in our coordination language, we first go into how faults and errors are defined and which types of faults exist. Based on these definitions, we move on to categorise and explain widely used fault-tolerance methods to provide a global overview of what is available, to gain a better understanding of what can be applied to the TeamPlay coordination model.

\subsection{Fault definition and models}
Before we go into fault-tolerance schemes we establish definitions of faults and several fault models. In this work, we use the definitions from the widely used definitions proposed by \cite{lapri2004taxonomy}. A service is a sequence of states. In the event of a failure at least one of these states deviates from the correct state. The deviation from these state(s) is called an error while the hypothesised cause of such an error is called a fault.

In this section, we provide definitions for several types of faults used throughout this work:~\cite{tam1992generic, charron2007harmful, hadzilacos1994modular, omana2003model, carvalho2018dynamic, castro1999practical, correia2004tolerate, correia2013bft, hamid2005formal, tanenbaum2007distributed, lapri2004taxonomy}. 

\begin{description}
	\item[Crash fault] Component crashes fully; no further messages are sent.
	\item[Transient fault] Fault in the logic gates of the processor such that the result of the computation is erroneous. 
	\item[Software / Commission fault] Generic type of fault that deals with the software or hardware not meeting their specifications.
	\item[Security fault] Faults that occur because of a compromised system. Messages may or may not be sent. Messages may be falsified.
	\item[Omission fault] Message delay can be infinitely long so that the message will not be delivered or responded to. Comes in two variants: send and receive-omission faults, which involve faults in the sending and receiving of messages respectively.
	\item[Temporal / Timing fault] System provides correct result but outside the timing scope of the associated (real-time) system.
	\item[Response fault] Incorrect value within the correct time frame. Two types of response faults exist, value faults in which the system returns the wrong value and state transitions failures in which a component responds unexpectedly.
	\item[Byzantine fault] An arbitrary fault, can be anything from the above list. The weakest and most general form of fault. Dealing with this type is troublesome as it is often difficult to detect them. In literature, another type of Byzantine fault is defined in which messages cannot be falsified.
\end{description}

\subsection{Recovery methods}

Fault-tolerance aims at failure avoidance, i.e., that the service that is running keeps working correctly. Two generic types of fault-tolerance exist: error-detection, which identifies the presence of an error, and (system) recovery, which transforms the systems state into one without errors and faults~\cite{lapri2004taxonomy}.

Figure~\ref{fig:fault-tolerance-overview} presents an overview which highlights the two sub-classes of recovery methods, error handling and fault handling. Error handling aims to mask and remove errors from the system while fault handling aims to prevent faults from happening in the future. In this work we mainly focus on error handling techniques. This will also be the focus in this section in which we highlight different fault-tolerance methods which are connected to the classes that can be found in the aforementioned figure. This overview closely resembles the structure and order of the sections in this chapter.

\begin{figure}[ht]
	\centering
	\includegraphics[width=\textwidth]{Figures/FTMETHODS (1).pdf}
	\caption{Overview of fault-tolerance methods starting with generic classes (indicated with blue) collected by \cite{lapri2004taxonomy}. Our contribution to this figure is the collection of several concrete methods (indicated with purple) that can be attributed to these generic classes. 
		The methods coloured green are chosen for implementation and further detailed in Chapter~\ref{ch:language} and~\ref{ch:implementation}. }
	\label{fig:fault-tolerance-overview}
\end{figure}

\subsection{Rollback recovery}
\label{background:rollback}

Rollback recovery lets the system return to a stable backup state when a fault has occurred~\cite{tanenbaum2007distributed, sultana2019toward}. Most commonly, checkpoint/restart is used. Checkpoints containing snapshots of the system are saved in a persistent form of storage to enable rollback. In independent checkpointing, every process saves its previous states. This may lead to high memory usage. A known issue with checkpoint/restart is that it can cause a domino effect. In the domino effect, each process triggers the checkpointing process which results in an inconsistent system state. This inconsistent state triggers the checkpointing mechanism again. This process repeats itself until the initial state of the system is restored. Checkpoint/restart requires error detection mechanisms which initiate the rollback process. The advantage of checkpoint/restart is that the overhead when faults do not occur is minimal.

Another version of checkpoint/restart, called coordinated checkpointing~\cite{tanenbaum2007distributed}, synchronises the individual checkpoints of the system to prevent the domino effect, at the cost of higher overhead. This way of check-pointing does lower the memory overhead compared to independent checkpointing since each process only needs a single stable checkpoint.

Multi-level checkpointing uses two levels, a lightweight and faster checkpointing system for common failures and a parallel system checkpoint for rarer, more severe failures. In shrinking recovery, faulty resources are removed from the resource pool, while in non-shrinking recovery, faulty resources are replaced by back-up resources. In message logging, message logs (also called determinants) are saved together with checkpoints to enable the messages to replay from the checkpoint to allow for faster recovery.

\subsection{Compensation}
The compensation class makes use of redundancy mechanisms to mask the error and prevent it from affecting the service~\cite{lapri2004taxonomy}. In computing, three classes of redundancy can be defined: information redundancy, time redundancy and physical redundancy~\cite{tanenbaum2007distributed, lyons1962use}. 

\label{background:information-redundancy}

Information redundancy makes use of extra, redundant data to recover from data failures. Data failures can occur in processes like data transmission and storage. Examples of information redundancy are parity bits and Hamming codes~\cite{tanenbaum2007distributed, peng2010building}. In time redundancy, actions are repeated when errors occur, e.g., in transactions. This requires that the actions are actually repeatable, actions that interact with system state are in practice difficult to redo. Process re-execution is the simplest form of time redundancy. In physical redundancy, extra equipment or processes is employed to serve as a backup, this can be done in several ways. 

\subsubsection{N-Modular redundancy}
\label{background:nmr}
A classic example of physical redundancy is \acrfull{nmr}. In this strategy, $n$ independent identical processes are executed with identical input~\cite{tanenbaum2007distributed}. These $n$ processes are followed by voting processes, which vote which answer they will be outputting. This method primarily focuses on masking transient faults. Depending on the fault-model for the application, it can be possible that the voter processes fail. In order to decrease the chance of this happening it is possible to increase the number of voters~\cite{lapri2004taxonomy}.

If a minority of the computational processes  have faults, a majority vote will still result in the correct answer. \acrfull{tmr}~\cite{lyons1962use, tanenbaum2007distributed, gamer2013fault} is a special case of \acrshort{nmr} in which $n$ is minimally chosen such that the computation does not have to be repeated (when a single fault is present). Since we can't know which process is likely to be faulty in case $n = 2$. However, this double-modular redundancy method has uses as an error detection method since it can be used to detect transient errors. Figure~\ref{fig:NMR} depicts \acrlong{nmr}. In this figure, we see a pipeline consisting of two stages of components followed by voting processes. 

\begin{figure}[ht]
	\centering
	\includegraphics[scale=0.6]{Figures/NMR.pdf}
	\caption{Depiction of \acrfull{nmr}.}
	\label{fig:NMR}
\end{figure}

\subsubsection{Standby methods}
\label{background:standby}
In standby or primary-backup methods, standby components can take over the active computing component in case of failure. This process is illustrated in Figure~\ref{fig:standby}. Initially the output of the primary-process is used, and if a fault is detected, the output of the standby component is used instead. A distinction can be made between cold, warm and hot standby which differ in the amount of synchronisation the back-up components have to the active components~\cite{gamer2013fault}, the distinction between these types can be defined as follows:

\begin{description}
	\item[Cold] The redundant component(s) are only initialised and then turned off.
	\item[Warm] The state of the active component is mirrored to the redundant component(s) at specified points.
	\item[Hot] The redundant component(s) are actively synchronised with the main component so that the active component can be taken over immediately.
\end{description}

\begin{figure}[ht]
	\centering
	\includegraphics[scale=0.8]{Figures/Hot_Standby.pdf}
	\caption{Depiction of recovery of a failing active component using the hot standby method. When a fault is detected, the output of the active component is routed to that of the standby component, so that the service can continue.}
	\label{fig:standby}
\end{figure}

%This method has an advantage in terms of computing resources over methods like NMR.  

In case of crash failures, components with long startup times benefit from this type of synchronisation because the working component can be taken over faster when using primary-backup \cite{gamer2013fault}. Generally, the number of required computing resources for primary-backup are lower compared to methods like NMR. Especially using cold standby can save a lot of energy resources, which is important in (often) battery powered systems like those in the cyber-physical class. The main disadvantage of this method is that a fault needs to be detected before the redundant component can take over. Furthermore, primary-backup cannot detect value faults (i.e., there is no voting) and it relies on an error detection method to detect faults, so that the active component can be taken over. Thus, this method is primarily used in systems which require fast switch times while keeping a state close to the state of the original, crashed process~\cite{gamer2013fault}. 

\subsubsection{N-version programming}
\label{background:nvp}

In \acrfull{nvp} multiple functional equivalent implementations of the same specification are created~\cite{peng2010building}. In runtime, they can be run in the same way as \acrlong{nmr}. The advantage over \acrshort{nmr} is that bugs (i.e., software-faults) present in a single implementation are caught the same way transient faults are caught. The disadvantage is that the development overhead is high since each version should be developed by different people.

\subsection{Rollforward recovery}
\label{background:rollforward}
Rollforward recovery creates a new state from an older state (often a checkpoint) without errors~\cite{lapri2004taxonomy, tanenbaum2007distributed}. Rollforward checkpointing helps in limiting the amount of rollback so that the aforementioned domino effect can be prevented~\cite{gupta2002design}. The problem with this class of methods is that it has to be known in advance which errors may occur~\cite{tanenbaum2007distributed}.

\cite{xu1996roll} present a rollforward mechanism for use in real-time (distributed) embedded systems~\cite{xu1996roll}. The mechanism uses a 2-modular redundancy approach for error detection. Furthermore the method requires periodic checkpoints to be created. When a fault is discovered a third process is created on a spare processor which will run the computation starting from the previous checkpoint. When it is finished the three results are compared and the faulty processor's state will be restored to the current checkpoint (the checkpoint after the one used for recovery). This checkpoint system saves on computing resources compared to a traditional \acrshort{tmr} setup as the spare processor only has to be invoked when a fault occurs.

Rollforward and rollback recovery are not mutually exclusive~\cite{lapri2004taxonomy}. Rollback may be followed by rollforward if the error persists in the rollback mechanism.

\subsection{Real-time systems}
According to \cite{poledna2007fault}, active replication strategies like triple-modular redundancy are best suited for (hard) real-time systems since the execution time when faults occur is similar to when they do not occur. The problem with these methods is that they always use more computing resources which is not always possible for battery-powered systems. Mechanisms like rollforward recovery also have variations that are more suitable for real-time systems~\cite{xu1996roll}, these often make use of extra hardware in order to create a more predictable mechanism.

In primary-backup, as noted in Section~\ref{background:standby}, faults need to be detected before control can be handed over to the standby component. Therefore it is sometimes not feasible in real-time systems (depending on the application and error checking mechanism) to use primary-backup as it is sometimes not deterministic when faults are detected. 
In timing-critical real-time systems, one could use the \acrfull{wcet} to determine whether a component has crashed, i.e. a timing- or crash-fault has occurred~\cite{claesson1998xbw}. This method cannot account for value faults unless it has application-specific constraints to monitor the output values of the system.

\subsection{Error detection }
\label{background:error-detection}
There are two types of error detection, concurrent error detection which occurs during the service, and preemptive error detection which occurs before the service is started. The latter usually focuses on hardware or software diagnostics and is outside the scope of our research. Like in recovery methods, the type of detection method employed depends on the types of faults that are expected.

As mentioned in Section~\ref{background:nmr} and~\ref{background:rollforward}, \acrshort{nmr} (specifically, two-modular redundancy or dual-process execution), can be used to detect errors like transient faults with $n = 2$~\cite{claesson1998xbw, xu1996roll, tanenbaum2007distributed}. This way of working can be expanded to detect whether it is likely that a fault is transient or permanent on a hardware component~\cite{claesson1998xbw}. This can be done by performing an additional execution with a golden reference check, i.e., execute the process with reference data, which can be compared with reference output data of that component~\cite{claesson1998xbw, kopetz2006real, lapri2004taxonomy}.

\acrshort{nmr} methods only work if the fault does not affect all executions in the same manner, i.e., faults need to be independent~\cite{lapri2004taxonomy}. For example, software faults are not independent hence they can only be detected by employing mechanisms like \acrshort{nvp}. 

Message validity can be checked by using information redundancy, e.g., sending an error detection code such as a checksum, cyclic redundancy check (CRC), or hamming code, to check whether a message is corrupted~\cite{tanenbaum2007distributed, kopetz2006real}.

In distributed systems, there are three generic methods to check whether a process (or component) has crashed. The first method involves actively sending messages between processes (also called heartbeat or pinging) to make sure that they are still responding \cite{tanenbaum2007distributed}. The second method is to passively wait for messages to come in \cite{tanenbaum2007distributed}. This method can only be used when regular communication between processes is guaranteed, hence the first method is more often used. In the third method, the node periodically executes a challenge-response protocol \cite{kopetz2006real}. An error detector provides an input pattern to the node, it then expects the node to respond in a defined manner which involves as many functional units of the node as possible. If the result deviates from what the error detector expects, it is flagged as an error of the node. The benefit of this method is that it can potentially find errors in the value domain of the node, contrary to the previously mentioned distributed node checking methods which are primarily about crash faults. The disadvantage of this method is that it consumes additional resources on both the node and error detector compared to methods like heartbeat.


\subsection{Fault handling}
Error handling focuses on eliminating errors from the system state while fault handling prevents faults from being activated again~\cite{lapri2004taxonomy}. Error handling followed by fault handling forms system recovery. Fault handling has four sub-classes as listed in Figure~\ref{fig:fault-tolerance-overview}. Diagnosis focuses on identifying the cause of the error. Isolation excludes faulty resources from the active resource pool. Reconfiguration switches in spares and potentially re-assigns tasks among non-faulty components. Finally, reinitialization is the process of updating and checking the reconfigured system.

% \todoi{example here: https://ieeexplore.ieee.org/abstract/document/761103 ???}

% \todoi{maybe reread parts of the admorph docu?}

% \todoi{At least some examples:
% reconfig: https://ieeexplore.ieee.org/stamp/stamp.jsp?arnumber=936217}

% \todoi{Anything else here? Maybe in relation with admorph? If not better to move it upward to the general explanation and not have a separate section on it. }
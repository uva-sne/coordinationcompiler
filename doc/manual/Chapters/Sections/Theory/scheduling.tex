%!TEX root=../../../usermanual.tex
\section{Real-Time Scheduling}

Real-time scheduling determines the execution order of tasks on a processor. Then, targeting multi-core architectures, mapping algorithms determine on which core a task will run. Most of the times, scheduling on single-core architecture is NP-hard, e.g. with fixed-priority and sporadic tasks \cite{ekberg2017fixed}. Mapping+scheduling on multi-core architectures is therefore classified as a NP-hard problem \cite{coffman1996approximation}, because the mapping step reduces to the known bin-packing problem. For the sake of simplicity, when not explicitly stated in the following, a multi-core scheduling algorithm refers to both a mapping and a scheduling algorithm.

The literature on multi-core real-time scheduling is tremendously vast. Davis and Burns \cite{davis2011survey} sort scheduling algorithms in three main categories: \begin{enumerate*}
	\item partitioned, 
	\item global, and 
	\item hybrid.
\end{enumerate*}
Following the classification of single-core schedulers, further review focuses on multi-core architectures where the main content focuses on partitioned (including static/off-line) scheduling, while other scheduling policies are summarised. More details on scheduling strategy and schedulability analysis are available in this very same survey \cite{davis2011survey}.

\subsection{Classification of single-core schedulers}

The two first major categories of schedulers represent the construction moment of the selection order: \textit{off-line} or \textit{on-line.}
The former, statically pre-computes off-line an execution sequence for each core. Then at runtime, a dispatcher executes the generated schedule. The latter executes \textit{on-line}, and determines the next task to execute according to some criteria, the most common being assigned priorities and current execution state. 

Priorities are assigned to tasks dynamically or statically (also known as fixed). Rate Monotonic (RM) \cite{liu1973scheduling} scheduling policy statically affects, at design time, higher priorities to tasks with shorter periods. In contrast, Earliest Deadline First (EDF) \cite{liu1973scheduling} determines priorities according to the current execution state of tasks and set higher priorities to unfinished tasks closer to their deadlines. Multiple other examples exist for fixed or dynamic priorities scheduling policies targeting single-core architectures, e.g. Deadline Monotonic (DM) \cite{leung1982complexity} or Least Laxity First (LLF) \cite{dertouzos1989multiprocessor}.

On-line schedulers select the task to execute at run time upon task arrivals or terminations (except few particular cases, e.g. Least Laxity First \cite{dertouzos1989multiprocessor} scheduling policy where decisions are taken at each time instant). Due to the nature of WCET that is likely to not happen, on-line schedulers are generally more flexible than off-line ones. But their implementation is more costly. Another solution is to combine off-line and on-line schedulers \cite{combaz2005qos} to benefit from the flexibility and low implementation cost.

%Recently, Skalistis \cite{skalistis2017efficient} proposed a mix of on-line/off-line scheduling strategies to benefit from both advantages and alleviate the under-utilisation induced by off-line scheduler.

Because on-line schedulers construct the schedule at run-time, a schedulability analysis is mandatory, also known as a feasibility test. It statically proves that no task misses a deadline. In contradiction, off-line schedulers validate at schedule time that no task misses a deadline by building the schedule in advance \cite{che2010scheduling, che2011scheduling}.

Pre-emptive scheduling policies, e.g. \cite{liu1973scheduling}, allow a task to be paused/resumed by an other task. Pre-emption depends on the priority of the current executing task. A higher priority task pre-empts a lower priority one. Pre-emptive schedules require a Real-Time Operating System (RTOS) to handle context switches when a task pre-empts another one. Between these two extreme cases, limited pre-emptive scheduling policy offers an alternative which limits the number of context switches but remain flexible \cite{buttazzo2013limited}.

%On-line schedulers act as the job level while off-line schedulers can schedule a task-set both at the task level or job level. With independent tasks, switching from task to job level is straightforward. 
An infinite schedule does not need to be built for periodic task models. Instead, the analysis computes the minimum time a schedule needs to enter in a repetitive pattern. This interval, known as the \textit{hyper-period}, corresponds to the Least Common Multiple (LCM) of all periods in the task set \cite{goossens2016periodicity}. This principle is extended by Puffitsch et al. \cite{puffitsch2015off} for dependent task set where all dependent tasks have different periods. 
% But if tasks are dependent and period differs, then  proposed to extend the precedence constraints. 
%They signal to the scheduler that job instances of an identical task are related and ordered.

%\paragraph{Toward multi-core scheduling}
%
%The dominating properties in uniprocessor scheduling describes priorities (fixed/dynamic) and preemption (with/without/limited). 
%
%A Rate Monotonic (RM) \cite{liu1973scheduling} scheduler will chose the task with the highest priority among the ready ones. RM uses fixed priority statically assigned and is designed for the periodic task model. RM is proven to be optimal for fixed priority with implicit deadlines ($period = deadline$), non-preemptive \cite{park2007non}, preemptive \cite{liu1973scheduling}. Its schedulability analysis states that the load of the processor must not exceed $1$. 
%
%Early Deadline First (EDF) \cite{liu1973scheduling} chooses the task that has the closest deadline among the ready ones. EDF uses dynamic priority depending on the execution state. EDF is optimal among preemptive scheduling algorithms for periodic independent tasks with constrained deadlines ($deadline \le period$) with a higher load than RM.

\subsection{Multi-core partitioned scheduling}

Partitioned approaches group tasks into partitions. Each partition is assigned to a core and scheduling algorithms designed on uniprocessor can then be applied (e.g. RM \cite{liu1973scheduling}, LLF \cite{dertouzos1989multiprocessor}). Baruah et al \cite{baruah2008partitioned} find the best partition set for sporadic tasks under constrained deadlines. Then EDF or RM from \cite{liu1973scheduling} will schedule each partition on a core.

Off-line schedule problems are solved with different types of algorithm depending on the wished level of optimality. Integer Linear Programming (ILP) \cite{puffitsch2015off,gorcitz2015scalability} and Constraint Programming (CP) \cite{gorcitz2015scalability, perret2017pred} solvers formulate the problem as a series of constraints, then solving the constraint system results in an optimal solution according to an objective function. 
%Model checking \cite{ersfolk2011scheduling} and 
Satisfiability Modulo Theory (SMT) methods \cite{tendulkar2014many,gorcitz2015scalability} also result in optimal schedules but logic constraints are applied instead of algebraic ones. Mapping a task set onto a multi-core is NP-hard \cite{coffman1996approximation}, therefore such techniques do not well scale with large problems. It is a common practice to provide a heuristic aside from a method generating optimal results, for example in \cite{becker2016contention} both an ILP and a heuristic solving the same problem are given. Such heuristic targets the same goal but with a small possible over-approximation. Hence, the goal of the heuristic algorithm is to generate a close to optimal result within a much smaller solving time. In between, meta-heuristics aim at limiting the over-approximation to get closer to the optimal solution with a control on the solving time ; e.g. Particle Swarm Optimisation (PSO) \cite{zaki2013scalable}, genetic algorithms \cite{pop2006performance,weichslgartner2014daarm}.
%All contribution chapters of this dissertation provide both an ILP and a heuristic targeting the same problems. All corresponding experiment sections empirically evaluate the over-approximation of the heuristic.

Different objectives drive scheduling algorithms. Among all the different possibilities, the literature offers to : 
\begin{itemize}
	\item minimise the overall schedule makespan in \cite{yi2009ilp, perret2017pred},
	\item find a valid schedule (not specifically the shortest)  \cite{becker2016contention},
	\item optimise the energy consumption \cite{calinescu2018energy},
	\item maximise throughput \cite{choi2009stream},
	\item minimise total communication cost \cite{tendulkar2014many}.
\end{itemize}

In \cite{kudlur2008orchestrating,tendulkar2014many} a multi-step process schedules communication and tasks from SDF. They first augment the graph by adding nodes to model DMA transfers. Then a SMT solver outputs a schedule before allocating buffers for data transfer. In \cite{tendulkar2014strictly}, Tendulkar et al. add jobs pipelining to there SMT formulation from \cite{tendulkar2014many} to increase the throughput of the schedule while it was already present in Kudlur et al. work from \cite{kudlur2008orchestrating}.

%\cite{weichslgartner2014daarm} present a genetic algorithm to map a task graph on a many-core architecture with a worst-case contention at the link/routeur level.

%Static preemptive schedules are built in \cite{carle2014static} targeting many-cores with NoC. The schedule is  also compute pre-emption point.

Following works manage to capture the essence of the targeted hardware platform to generate more specialised schedules.

Alhammad and Pellizzoni \cite{alhammad2014time} propose a heuristic to map and schedule a fork/join graph onto a multi-core architecture in a contention-free manner. They split the graph in sequential or parallel segments, and then schedule each segment. They consider only code and local data access in contention estimations, leaving global shared variables in the main external memory, where a worst concurrency scenario is assumed when accessing them. 
%Moreover, we deal with any DAG not just fork/join graphs, and write back modified data to memory only when required.

%Rihani et al. \cite{rihani2015wcet} target periodical independent tasks and many-core platforms including a shared bus arbitrated with a TDMA policy. They provide a SMT formulation that models the specificities of the TDMA arbiter. Then, bus requests from each core overlap with their corresponding core allocated TDM slots. The major drawback of TDMA arbiters is therefore minimised with a global diminution of the waiting time when requesting access to the shared bus.

Puffitsch et al. \cite{puffitsch2015off} statically schedule periodical dependent tasks on many-core platforms. They account for the memory loading such as SPM, and places preloading time in case of caches in order to achieve a timing isolation process. They also allow to parametrise the objective function between the number of used cores, and a threshold of contention along the bus.
%voir aussi contention et inter-core com. 2 possible objectives, number of used cores, amount of contention

Becker et al. \cite{becker2016contention} propose an ILP formulation and a heuristic to schedule periodic sporadic independent tasks on one cluster of a Kalray MPPA processor. They systematically create a contention-free schedule.

Nguyen et al. \cite{nguyen2017cache} statically schedule DAG on one cluster of a Kalray MPPA architecture. The objective is to build a cache-conscious schedule in which tasks sharing information are mapped on the same core, and in an order that will maximise cache lines reuse.

Skalistis and Simalatsar \cite{skalistis2017near} build a partitioned off-line schedule from DAGs, and minimise the overall schedule makespan. Because an off-line schedule uses the WCET of tasks, which is likely to not happen, an on-line scheduler can fire a job earlier as long as off-line scheduling decisions are fulfilled. 

%In addition to mapping software scheduling, \cite{belaid2011static, belaid2013complete} map hardware tasks on a FPGA in three steps. They first cluster tasks with identical resource requirements, then they validate the schedulability of the clusters before mapping tasks onto a minimum space.

\subsection{Multi-core global scheduling}

Global methods schedule tasks dynamically and globally on all cores, at a job level. Jobs from the same task can migrate between cores. They are often adaptations of uniprocessor scheduling strategies ; EDF becomes Global-EDF \cite{dhall1978real}, RM becomes Global-RM \cite{andersson2001static}.

Because global scheduling may imply a lot of context switches and migration, Anderson et al \cite{anderson2008mathsf} improve the initial work from Dhall et al. \cite{dhall1978real} on Global-EDF to limit the number of migrations.

When dealing with sporadic tasks, new tasks appear with a minimum interval during the execution of the real-time system. With an online global scheduler, to decide whether to accept the task or not, the system must recheck the schedulability in an on-line fashion. Therefore, the admission control algorithm requires an efficient schedulability analysis to limit the decision overhead. Two methods are proposed by Zhou et al. \cite{zhou2018execution}. They identified factors affecting the efficiency of RTA methods -- i.e. unnecessary recalculation of WCRTs under Global-EDF. They also improved the computational method. However checking the schedulability of a system under Global-EDF and Global-RM with sporadic tasks remain pseudo-polynomial.

%For dependent periodic task set, Forget et al. \cite{forget2010scheduling} apply transformation on deadlines and activation times. This enables global scheduling algorithm to respect dependencies without synchronisation mechanism.

In \cite{marinakis2017efficient}, an on-line global scheduler decides which tasks to run in parallel. Decisions imply to know the memory demand, and the scheduler determine the amount of interferences it allows. If the scheduler detects more interferences than expected and above a threshold, then tasks are re-prioritised and pre-empted to drop down the contention.

\subsection{Multi-core hybrid scheduling}

Because partitioned approaches might under-utilise the hardware, and global scheduling techniques induce possible high overhead with migration costs, then hybrid scheduling methods try their best to improve these two factors of performance.

First, semi-partitioned approaches limit the fragmentation of spare capacity. They generally build an initial partition set which is assigned to a core but this assignation does not remain immutable, as following defined.

Cannella et al. \cite{cannella2014system} build a semi-partitioned heuristic scheduler : First-Fit Decreasing Semi-Partitioned (FFD-SP). As opposed to regular partition algorithms, they allow a whole task to temporarily migrate into another partition. 

Burns et al \cite{burns2012partitioned} give a method to compute partitions which will then be executed with EDF. The specificity is the possibility for a task to migrate when pre-empted. A task can then start on a core, and ends on another one, but returns to the initial core at the next firing. 

Second, cluster approaches group processors with related properties (e.g. shared caches). Then  a global EDF is applied on the group of cluster \cite{shin2008hierarchical}.

Third, federated scheduling \cite{li2014analysis} group tasks depending on their processor utilisation. The idea is to identify heavy tasks from light ones, where heavy tasks load more the processor than light ones. 

In \cite{jiang2017semi}, heavy tasks are considered on their own. Light tasks are federated and a federation is considered as sequential sporadic and scheduled at once.

\subsection{\label{chap1:ssec:interf}Shared resource management on single-core and multi-core architectures}

Shared resources represent anything that tasks are required to share with other tasks. On the hardware side, the processor core is the first obvious shared resource. But other devices also can be shared, e.g. bus, memory hierarchy, I/O devices, \ldots . On the software side, shared variables are the primal shared resources. Because a shared resource usually only allows one access at a time, ordering access requests require an arbitration policy or a sharing protocol. Because a request might be delayed due to interferences with other ones, real-time systems require to compute the worst-case waiting time. This waiting time is commonly named \textit{blocking time} and is added to issuing task's WCET to form the  Worst-Case Response Time (WCRT) of the tasks \cite{joseph1986finding}. The general term \textit{blocking time} can be specialised depending on the context, when a task pre-empts (blocks) an other one, it is known as the \textit{pre-emption delay}; when a task is delayed du to interferences on the communication medium, the blocking time is known as the \textit{contention delay}.

Negrean et al. \cite{negrean2009response} provide a method to compute the blocking time induced by concurrent tasks (pre-emption delay) in order to determine their response time. But some studies might choose to reduce \cite{hardy2009using}, or even avoid \cite{suhendra2008exploring} interferences on shared resources leading to a decrease, or the removal, of the blocking time.

Aforementioned schedulers order accesses to the processor core. From Section \ref{chap1:sec:icc}, among other arbiters, RR and TDMA arbiters order accesses to the shared bus. A contention analysis is then defined to determine the worst case delay for a task to gain access to the resource (see \cite{fernandez2014contention} for a survey). Some shared resources may directly implement timing isolation mechanism between cores, such as TDMA buses, making contention analysis straightforward. Further Chapters \ref{chap:contaw} and \ref{chap:hiding}, in their respective related work sections, detail bus sharing mechanisms and blocking time computations. Therefore, following policies from this section summarise key concepts in other hardware-oriented (e.g. cache, SPM) and general resource sharing (e.g. software objects).

On uniprocessor with priority-based scheduling, when a higher priority task pre-empts another, the cache replacement policy can evict lines useful to lower priority tasks. Then the resumed task can suffer from a blocking time to refill its evicted cache lines. In this case, the blocking time corresponds to the Cache Related Pre-emption Delay (CRPD) \cite{altmeyer2010cache,altmeyer2011cache}. In multi-core architectures, the CRPD for shared caches results in an important pessimism. Therefore, with cache partitioning, Suhendra et al. \cite{suhendra2008exploring} fixes the CRPD to zero at the cost of reduced performance with a smaller cache area per task.
SPM-based architectures can suffer from the same issue when a higher priority task evict useful data/code from the SPM for a lower pre-empted task. Whitman et al. \cite{whitham2012investigation} port the CRPD to SPM with the Scratchpad Pre-emption Delay (SRPD).

Other works on shared caches tend to minimise the interference. In \cite{ding2013shared}, the mapping of tasks minimise interference on L2 cache. In \cite{nguyen2017cache} the scheduler decides pairs of task mapped on the same core contiguously scheduled that will maximise cache reuse. Hardy et al. \cite{hardy2009using} reduce interference on shared L2 cache by identifying at compile time, what basic blocks are uniquely used. Then these blocks are not stored in the shared cache avoiding cache pollution.

Dealing with shared software objects is not new, and there now exists several techniques adapted from the single-core systems. Most of them are based on priority inheritance. In particular Jarrett et al. \cite{jarrett2015contention} apply priority inheritance to multi-cores and propose a resource management protocol which bounds the access latency to a shared resource.
For single-core architecture Priority Ceiling Protocol (PCP) \cite{sha1990priority} and Shared Resource Protocol (SRP) \cite{baker1991stack} are widely accepted as the most efficient technique to deal with software resource accessed in mutual exclusion. It is known to avoid priority inversion and limit the blocking time. The PCP policy defines a ceiling attached to each semaphore as the maximum priority among all tasks that can possibly lock the semaphore. The SRP is similar to the PCP, but it has the additional property that a task is never blocked once it starts executing. Both have analogous methods targeting multi-core architectures, MPCP \cite{rajkumar2012synchronization} and MSRP \cite{gai2001minimizing}. MPCP extends  the concept of blocking time to include also a remote blocking (when a job has to wait for the execution of a task of any priority assigned to another processor). MSRP separates local and global resources. For local resources, a classic SRP is used, while for a global resource, if the resource is already locked by some other task on another processor, then the task performs a busy wait (also called spin lock). A comparison of both MPCP and MSRP \cite{gai2003comparison} showed that no method outperforms the other. MSRP is easier to implement with a lower overhead while MPCP has a higher schedulability bound.
/*
 * Copyright (C) 2020 Benjamin Rouxel <benjamin.rouxel@uva.nl>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef UNITTEST_UTILS_H
#define UNITTEST_UTILS_H

#include <algorithm>
#include <string>
#include <InputParser/NfpParser.hpp>
#include <InputParser/ResXmlSax.hpp>
#include <StateMachine/Strategy.hpp>
#include <catch.hpp>

#define STR1(x) #x
#define STR(x) STR1(x)

void setup_trivial(config_t & conf, SystemModel & tg);
static CommandLineProperties _def_prop;
void setup_config(config_t & conf, SystemModel & tg, const std::string& fn, CommandLineProperties &prop = _def_prop);
void setup_tey(config_t & conf, SystemModel & tg, const std::string& fn);
void setup_nfp(config_t & conf, SystemModel & tg, const std::string& fn);
ResXMLSaxParser * setup_xmlchecker(config_t & conf, SystemModel & tg, Schedule *s, const std::string& fn);

/**
 * Read NFP without modifying any system model. The resulting parser can be used multiple times.
 * @param fn path
 * @return NfpFileParser owned by the caller
 */
NfpFileParser *read_nfp(const std::string& fn);

template<typename T>
bool contains(const std::vector<T> & v, T e) {
    return std::find(v.begin(), v.end(), e) != v.end();
}

template<typename StrategyType>
void validate_ssm(SystemModel &tg, config_t &conf, StrategyStateMachine &ssm) {
    /*
     * - all strategies are of the FT strategies
     * - all strategies are schedulable
     */
    std::vector<SchedEltPtr> elements = ssm.get_schedule_elements_exclusive();
    size_t originalSize = elements.size();
    REQUIRE_FALSE(elements.empty());

    for (StrategyPtr &strategy: ssm.strategies()) {
        REQUIRE(dynamic_cast<StrategyType *>(strategy.get()) != nullptr);

        strategy->schedule(tg, conf, *ssm.strategy_manager(), elements, *ssm.schedule());
        CHECK(ssm.schedule()->elements_lut().size() >= originalSize);
        CHECK(ssm.schedule()->makespan() <= tg.global_deadline());
        CHECK(ssm.schedule()->status() == ScheduleProperties::SCHED_COMPLETE);

        ssm.schedule()->elements_lut().clear();
    }

    REQUIRE(elements.size() == originalSize);
    ssm.set_schedule_elements(std::move(elements));
}


#endif /* UNITTEST_UTILS_H */


/*
 * Copyright (C) 2020 Benjamin Rouxel <benjamin.rouxel@uva.nl>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "config.hpp"
#include "CompilerPass.hpp"
#include "InputParser/InputParser.hpp"
#include "SystemModel.hpp"
#include "Transform/Transform.hpp"

#include "test_utils.hpp"

void setup_trivial(config_t & conf, SystemModel & tg) {
    setup_config(conf, tg, "trivial.xml");
}


void setup_config(config_t& conf, SystemModel& tg, const std::string& fn, CommandLineProperties &prop) {
    init_config(&conf);
    ParserRegistry::Create(".xml", &tg, &conf, &prop)->parse(STR(TEST_RESOURCE_DIR) "/resources/config/" + fn);
    conf.files.config_path = STR(TEST_RESOURCE_DIR) "/resources/config/" + fn;
    conf.files.output_folder = STR(TEST_RESOURCE_DIR) "/resources/tmp";

    if (conf.passes.find_pass_before("transform-propagate_properties") == nullptr)
        conf.passes->insert(conf.passes->begin(), std::unique_ptr<CompilerPass>(TransformRegistry::Create("propagate_properties")));
}

void setup_tey(config_t & conf, SystemModel & tg, const std::string& fn) {
    CommandLineProperties prop;
    try {
        conf.files.coord_path = STR(TEST_RESOURCE_DIR) "/resources/coord/" + fn;
        conf.files.coord_basename = conf.files.coord_path.leaf().replace_extension("");
        ParserRegistry::Create(".tey", &tg, &conf, &prop)->parse(STR(TEST_RESOURCE_DIR) "/resources/coord/" + fn);
    }
    catch(std::invalid_argument &e) {
        Utils::ERROR("Error parsing coord file /resources/coord/" + fn);
        Utils::ERROR(e.what());
    }
}

ResXMLSaxParser * setup_xmlchecker(config_t & conf, SystemModel & tg, Schedule *s, const std::string& fn) {
    CommandLineProperties prop;
    InputParser *rp = ParserRegistry::Create("res-xml", &tg, &conf, &prop);
    dynamic_cast<ResXMLSaxParser *>(rp)->schedule(s);
    rp->parse(STR(TEST_RESOURCE_DIR) "/resources/resxml/" + fn);
    return dynamic_cast<ResXMLSaxParser *>(rp);
}

NfpFileParser *read_nfp(const std::string& fn) {
    std::ifstream file_stream(STR(TEST_RESOURCE_DIR) "/resources/nfp/" + fn);
    auto *parser = new NfpFileParser(file_stream);
    file_stream.close();
    return parser;
}

void setup_nfp(config_t & conf, SystemModel & tg, const std::string& fn) {
    CommandLineProperties prop;
    ParserRegistry::Create(".nfp", &tg, &conf, &prop)->parse(STR(TEST_RESOURCE_DIR) "/resources/nfp/" + fn);
}

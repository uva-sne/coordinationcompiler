/*
 * Copyright (C) 2020 Stephen Nicholas Swatman <s.n.swatman@uva.nl>
 *                    Univeristy of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <boost/multiprecision/cpp_int.hpp>

#include "catch.hpp"

#include "Solvers/ILP/ILP.hpp"

using boost::multiprecision::int256_t;

namespace {
    std::unique_ptr<ILPExpression> expr(ILPExpression && v) {
        return v.clone();
    }
}

TEST_CASE("ILP variables", "[solver][ilp]") {
    ILPProblem p;

    ILPExpressionVar v1 = p.var("hello");
    CHECK( v1.var.name == "hello" );
    CHECK( v1.var.type == ILPVariable::Type::GENERAL );

    p.var("hello").var.binary();

    CHECK( v1.var.type == ILPVariable::Type::BINARY );

    ILPExpressionVar v2 = p.var("bye");
    ILPExpressionVar v3 = p.var("dup");
    ILPExpressionVar v4 = p.var("dup");

    CHECK( v3.var.name == v4.var.name );
    CHECK( v3.var.name != v2.var.name );

    CHECK( v3.var.type == ILPVariable::Type::GENERAL );
    CHECK( v4.var.type == ILPVariable::Type::GENERAL );

    p.var("dup").var.binary();

    CHECK( v3.var.type == ILPVariable::Type::BINARY );
    CHECK( v4.var.type == ILPVariable::Type::BINARY );

    CHECK( p.variables.size() == 3 );
}

TEST_CASE("ILP expressions", "[solver][ilp]") {
    ILPProblem p;
    std::unique_ptr<ILPExpression> v;

    SECTION("Single value expression") {
        v = expr(p.var("v1"));

        std::map<const std::string, int256_t> c = v->flatten();

        CHECK( p.variables.size() == 1 );
        CHECK( c.size() == 1 );
        REQUIRE( c.count("v1") > 0 );
        CHECK( c.at("v1") == 1 );
    };

    SECTION("Scale expression") {
        v = expr(7 * p.var("v1"));

        std::map<const std::string, int256_t> c = v->flatten();

        CHECK( p.variables.size() == 1 );
        CHECK( c.size() == 1 );
        REQUIRE( c.count("v1") > 0 );
        CHECK( c.at("v1") == 7 );
    };

    SECTION("Binop expression addition") {
        v = expr(p.var("v1") + p.var("v2"));

        std::map<const std::string, int256_t> c = v->flatten();

        CHECK( p.variables.size() == 2 );
        CHECK( c.size() == 2 );
        REQUIRE( c.count("v1") > 0 );
        REQUIRE( c.count("v2") > 0 );
        CHECK( c.at("v1") == 1);
        CHECK( c.at("v2") == 1);
    };

    SECTION("Binop expression subtraction") {
        v = expr(p.var("v1") - p.var("v2"));

        std::map<const std::string, int256_t> c = v->flatten();

        CHECK( p.variables.size() == 2 );
        CHECK( c.size() == 2 );
        REQUIRE( c.count("v1") > 0 );
        REQUIRE( c.count("v2") > 0 );
        CHECK( c.at("v1") == 1);
        CHECK( c.at("v2") == -1);
    };

    SECTION("Binop expression addition with duplicates") {
        v = expr(p.var("v1") + p.var("v1"));

        std::map<const std::string, int256_t> c = v->flatten();

        CHECK( p.variables.size() == 1 );
        CHECK( c.size() == 1 );
        REQUIRE( c.count("v1") > 0 );
        CHECK( c.at("v1") == 2);
    };

    SECTION("Binop expression addition with duplicates") {
        v = expr(8 * p.var("v1") - p.var("v1"));

        std::map<const std::string, int256_t> c = v->flatten();

        CHECK( p.variables.size() == 1 );
        CHECK( c.size() == 1 );
        REQUIRE( c.count("v1") > 0 );
        CHECK( c.at("v1") == 7);
    };

    SECTION("Flat value map expression") {
        std::map<ILPVariable *, int256_t> vals;
        vals[&p.var("v1").var] = 5;
        vals[&p.var("v2").var] = 2;

        v = expr(ILPExpressionSum(vals));

        std::map<const std::string, int256_t> c = v->flatten();

        CHECK( p.variables.size() == 2 );
        CHECK( c.size() == 2 );
        REQUIRE( c.count("v1") > 0 );
        REQUIRE( c.count("v2") > 0 );
        CHECK( c.at("v1") == 5);
        CHECK( c.at("v2") == 2);
    };

    SECTION("Binary expression complex subtraction") {
        std::map<ILPVariable *, int256_t> vals;
        vals[&p.var("v3").var] = 5;
        vals[&p.var("v4").var] = 2;

        v = expr(p.var("v1") - 3 * (6 * p.var("v2") + 2 * ILPExpressionSum(vals)));

        std::map<const std::string, int256_t> c = v->flatten();

        CHECK( p.variables.size() == 4 );
        CHECK( c.size() == 4 );
        REQUIRE( c.count("v1") > 0 );
        REQUIRE( c.count("v2") > 0 );
        REQUIRE( c.count("v3") > 0 );
        REQUIRE( c.count("v4") > 0 );
        CHECK( c.at("v1") == 1);
        CHECK( c.at("v2") == -18);
        CHECK( c.at("v3") == -30);
        CHECK( c.at("v4") == -12);
    };

    SECTION("Binary expression chained subtraction") {
        v = expr(p.var("v1") - p.var("v2") - p.var("v3") - p.var("v4"));

        std::map<const std::string, int256_t> c = v->flatten();

        CHECK( p.variables.size() == 4 );
        CHECK( c.size() == 4 );
        REQUIRE( c.count("v1") > 0 );
        REQUIRE( c.count("v2") > 0 );
        REQUIRE( c.count("v3") > 0 );
        REQUIRE( c.count("v4") > 0 );
        CHECK( c.at("v1") == 1);
        CHECK( c.at("v2") == -1);
        CHECK( c.at("v3") == -1);
        CHECK( c.at("v4") == -1);
    };

    SECTION("Binary expression chained subtraction with brackets") {
        v = expr(p.var("v1") - p.var("v2") - (p.var("v3") - p.var("v4")));

        std::map<const std::string, int256_t> c = v->flatten();

        CHECK( p.variables.size() == 4 );
        CHECK( c.size() == 4 );
        REQUIRE( c.count("v1") > 0 );
        REQUIRE( c.count("v2") > 0 );
        REQUIRE( c.count("v3") > 0 );
        REQUIRE( c.count("v4") > 0 );
        CHECK( c.at("v1") == 1);
        CHECK( c.at("v2") == -1);
        CHECK( c.at("v3") == -1);
        CHECK( c.at("v4") == 1);
    };

    SECTION("Combined 1") {
        v = expr(2 * (7 * p.var("v1") - ((-2) * p.var("v2"))) + 3 * (p.var("v2") - p.var("v3")));

        std::map<const std::string, int256_t> c = v->flatten();

        CHECK( p.variables.size() == 3 );
        CHECK( c.size() == 3 );
        REQUIRE( c.count("v1") > 0 );
        REQUIRE( c.count("v2") > 0 );
        REQUIRE( c.count("v3") > 0 );
        CHECK( c.at("v1") == 14);
        CHECK( c.at("v2") == 7);
        CHECK( c.at("v3") == -3);
    };
}

TEST_CASE("ILP constraints", "[solver][ilp]") {
    ILPProblem p;

    SECTION("Simple GEQ") {
        ILPConstraint c(p.var("v1") >= 5);

        CHECK( c.value == 5 );
        CHECK( c.type == ILPConstraint::Type::GEQ );
    };

    SECTION("Simple LEQ") {
        ILPConstraint c(p.var("v1") <= 5);

        CHECK( c.value == 5 );
        CHECK( c.type == ILPConstraint::Type::LEQ );
    };

    SECTION("Simple EQ") {
        ILPConstraint c(p.var("v1") == 5);

        CHECK( c.value == 5 );
        CHECK( c.type == ILPConstraint::Type::EQ );
    };

    SECTION("Add unnamed constraint") {
        p.add_constraint(p.var("v1") == 5);

        REQUIRE( p.constraints.size() == 1 );
        CHECK( p.constraints[0].name.empty() );
    };

    SECTION("Add named constraint") {
        p.add_constraint(p.var("v1") == 5, "my_constraint");

        REQUIRE( p.constraints.size() == 1 );
        CHECK( p.constraints[0].name == "my_constraint" );
    };

    SECTION("Add multiple constraints") {
        p.add_constraint(p.var("v1") == 5, "my_constraint1");
        p.add_constraint(p.var("v2") == 4, "my_constraint2");

        CHECK( p.constraints.size() == 2 );
        CHECK( p.variables.size() == 2 );
    };
}

TEST_CASE("ILP bounds", "[solver][ilp]") {
    ILPProblem p;

    SECTION("Single bound") {
        p.add_bounds(p.var("v1").var, 5, 18);

        REQUIRE( p.bounds.size() == 1 );
        CHECK( p.bounds[0].lower == 5 );
        CHECK( p.bounds[0].upper == 18 );
    };

    SECTION("Multiple bound") {
        p.add_bounds(p.var("v1").var, 5, 18);
        p.add_bounds(p.var("v1").var, -7, 21);

        REQUIRE( p.bounds.size() == 2 );
        CHECK( p.bounds[0].lower == 5 );
        CHECK( p.bounds[0].upper == 18 );
        CHECK( p.bounds[1].lower == -7 );
        CHECK( p.bounds[1].upper == 21 );
    };
}

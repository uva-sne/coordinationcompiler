/*
 * Copyright (C) 2020 Stephen Nicholas Swatman <s.n.swatman@uva.nl> 
 *                    Univeristy of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <algorithm>

#include "SystemModel.hpp"
#include "Utils/Graph.hpp"

#include "test_utils.hpp"
#include "catch.hpp"
#include "Utils/Log.hpp"
#include "Transform/TransitivClosure.hpp"

TEST_CASE("Extract DAGs", "[utils][graph]") {
    config_t conf;
    SystemModel tg;

    setup_trivial(conf, tg);
    
    TransitiveClosure trc;

    SECTION("Single DAG") {
        setup_tey(conf, tg, "dag1.tey");
        trc.run(&tg, &conf,nullptr);

        std::vector<std::vector<Task *>> out;

        REQUIRE_NOTHROW( Utils::extractGraphs(&tg, &out) );

        REQUIRE( out.size() == 1 );

        std::vector<Task *> & sg = out[0];

        CHECK( sg.size() == 3 );
        CHECK( std::find(sg.begin(), sg.end(), tg.task("p1")) != sg.end() );
        CHECK( std::find(sg.begin(), sg.end(), tg.task("p2")) != sg.end() );
        CHECK( std::find(sg.begin(), sg.end(), tg.task("p3")) != sg.end() );
    }

    SECTION("Multiple DAGs") {
        setup_tey(conf, tg, "dag4.tey");
        trc.run(&tg, &conf,nullptr);

        std::vector<std::vector<Task *>> out;

        REQUIRE_NOTHROW( Utils::extractGraphs(&tg, &out) );

        REQUIRE( out.size() == 2 );

        std::vector<Task *> & sg1 = out[0];
        std::vector<Task *> & sg2 = out[1];

        if (sg1.size() != 3) {
            std::swap(sg1, sg2);
        }

        CHECK( sg1.size() == 3 );
        CHECK( sg2.size() == 2 );

        CHECK( std::find(sg1.begin(), sg1.end(), tg.task("p1")) != sg1.end() );
        CHECK( std::find(sg1.begin(), sg1.end(), tg.task("p2")) != sg1.end() );
        CHECK( std::find(sg1.begin(), sg1.end(), tg.task("p3")) != sg1.end() );
        CHECK( std::find(sg2.begin(), sg2.end(), tg.task("q1")) != sg2.end() );
        CHECK( std::find(sg2.begin(), sg2.end(), tg.task("q2")) != sg2.end() );
    }

    SECTION("Mixed DAGs and cyclic") {
        setup_tey(conf, tg, "dag5.tey");
        trc.run(&tg, &conf,nullptr);

        std::vector<std::vector<Task *>> out;

        REQUIRE_NOTHROW( Utils::extractGraphs(&tg, &out) );

        REQUIRE( out.size() == 3 );

        std::vector<Task *> & sg1 = out[0];
        std::vector<Task *> & sg2 = out[1];

        if (sg1.size() != 3) {
            std::swap(sg1, sg2);
        }

        CHECK( sg1.size() == 3 );
        CHECK( sg2.size() == 2 );

        CHECK( std::find(sg1.begin(), sg1.end(), tg.task("p1")) != sg1.end() );
        CHECK( std::find(sg1.begin(), sg1.end(), tg.task("p2")) != sg1.end() );
        CHECK( std::find(sg1.begin(), sg1.end(), tg.task("p3")) != sg1.end() );
        CHECK( std::find(sg2.begin(), sg2.end(), tg.task("q1")) != sg2.end() );
        CHECK( std::find(sg2.begin(), sg2.end(), tg.task("q2")) != sg2.end() );
    }

    SECTION("Real use case") {
        setup_tey(conf, tg, "dag7.tey");
        trc.run(&tg, &conf,nullptr);

        std::vector<std::vector<Task *>> out;

        REQUIRE_NOTHROW( Utils::extractGraphs(&tg, &out) );

        REQUIRE( out.size() == 1 );
    }

    SECTION("Breaks BFS 1") {
        setup_tey(conf, tg, "breakbfs1.tey");
        trc.run(&tg, &conf,nullptr);

        std::vector<std::vector<Task *>> out;

        REQUIRE_NOTHROW( Utils::extractGraphs(&tg, &out) );
        CHECK( out.size() == 1 );
    }

    SECTION("Breaks BFS 2") {
        setup_tey(conf, tg, "breakbfs2.tey");
        trc.run(&tg, &conf,nullptr);

        std::vector<std::vector<Task *>> out;

        REQUIRE_NOTHROW( Utils::extractGraphs(&tg, &out) );
        CHECK( out.size() == 1 );
    }

    SECTION("Breaks BFS 3") {
        setup_tey(conf, tg, "breakbfs3.tey");
        trc.run(&tg, &conf,nullptr);

        std::vector<std::vector<Task *>> out;

        REQUIRE_NOTHROW( Utils::extractGraphs(&tg, &out) );
        CHECK( out.size() == 2 );
    }
}

TEST_CASE("Check if topological ordering", "[utils][graph]") {
    config_t conf;
    SystemModel tg;

    setup_trivial(conf, tg);
    
    TransitiveClosure trc;

    SECTION("Case 1") {
        setup_tey(conf, tg, "topological1.tey");
        trc.run(&tg, &conf,nullptr);

        Task * t1 = tg.task("p1");
        Task * t2 = tg.task("p2");
        Task * t3 = tg.task("p3");
        Task * t4 = tg.task("p4");

        REQUIRE( t1 != nullptr );
        REQUIRE( t2 != nullptr );
        REQUIRE( t3 != nullptr );
        REQUIRE( t4 != nullptr );

        std::vector<Task *> case1 = {t1, t2, t3, t4};
        std::vector<Task *> case2 = {t1, t3, t2, t4};
        std::vector<Task *> case3 = {t4, t1, t2, t3};
        std::vector<Task *> case4 = {t1, t2, t4, t3};
        std::vector<Task *> case5 = {t1, t4, t3, t2};

        CHECK( Utils::isTopologicalOrder(case1) );
        CHECK( Utils::isTopologicalOrder(case2) );
        CHECK( !Utils::isTopologicalOrder(case3) );
        CHECK( !Utils::isTopologicalOrder(case4) );
        CHECK( !Utils::isTopologicalOrder(case5) );
    }
}

TEST_CASE("Extracted DAGs must be in topological order", "[utils][graph]") {
    config_t conf;
    SystemModel tg;

    setup_trivial(conf, tg);
    
    TransitiveClosure trc;

    SECTION("Case 1") {
        setup_tey(conf, tg, "dag1.tey");
        trc.run(&tg, &conf,nullptr);

        std::vector<std::vector<Task *>> out;

        REQUIRE_NOTHROW( Utils::extractGraphs(&tg, &out) );
        REQUIRE( out.size() == 1 );

        CHECK( Utils::isTopologicalOrder(out[0]) );
    }

    SECTION("Case 2") {
        setup_tey(conf, tg, "dag4.tey");
        trc.run(&tg, &conf,nullptr);

        std::vector<std::vector<Task *>> out;

        REQUIRE_NOTHROW( Utils::extractGraphs(&tg, &out) );

        REQUIRE( out.size() == 2 );

        CHECK( Utils::isTopologicalOrder(out[0]) );
        CHECK( Utils::isTopologicalOrder(out[1]) );
    }

    SECTION("Case 3") {
        setup_tey(conf, tg, "dag5.tey");
        trc.run(&tg, &conf,nullptr);

        std::vector<std::vector<Task *>> out;

        REQUIRE_NOTHROW( Utils::extractGraphs(&tg, &out) );
        REQUIRE( out.size() == 3 );

        CHECK( Utils::isTopologicalOrder(out[0]) );
        CHECK( Utils::isTopologicalOrder(out[1]) );
        CHECK( Utils::isTopologicalOrder(out[2]) );
    }

    SECTION("Case 4") {
        setup_tey(conf, tg, "dag7.tey");
        trc.run(&tg, &conf,nullptr);

        std::vector<std::vector<Task *>> out;

        REQUIRE_NOTHROW( Utils::extractGraphs(&tg, &out) );
        REQUIRE( out.size() == 1 );

        CHECK( Utils::isTopologicalOrder(out[0]) );
    }

    SECTION("Breaks BFS 1") {
        setup_tey(conf, tg, "breakbfs1.tey");
        trc.run(&tg, &conf,nullptr);

        std::vector<std::vector<Task *>> out;

        REQUIRE_NOTHROW( Utils::extractGraphs(&tg, &out) );
        REQUIRE( out.size() == 1 );

        CHECK( Utils::isTopologicalOrder(out[0]) );
    }

    SECTION("Breaks BFS 2") {
        setup_tey(conf, tg, "breakbfs2.tey");
        trc.run(&tg, &conf,nullptr);

        std::vector<std::vector<Task *>> out;

        REQUIRE_NOTHROW( Utils::extractGraphs(&tg, &out) );
        REQUIRE( out.size() == 1 );

        CHECK( Utils::isTopologicalOrder(out[0]) );
    }

    SECTION("Breaks BFS 3") {
        setup_tey(conf, tg, "breakbfs3.tey");
        trc.run(&tg, &conf,nullptr);

        std::vector<std::vector<Task *>> out;

        REQUIRE_NOTHROW( Utils::extractGraphs(&tg, &out) );
        REQUIRE( out.size() == 2 );

        CHECK( Utils::isTopologicalOrder(out[0]) );
        CHECK( Utils::isTopologicalOrder(out[1]) );
    }
}
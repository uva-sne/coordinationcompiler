/* 
 * Copyright (C) 2021 Benjamin Rouxel <benjamin.rouxel@uva.nl> 
 *                    Lukas Miedina <l.miedina@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "config.hpp"
#include "Utils.hpp"
#include "SystemModel.hpp"
#include "Schedule.hpp"
#include "Exporter/FaultToleranceUppaalExporter.hpp"

#include "test_utils.hpp"

#include "catch.hpp"
#include <string>
#include <Exporter/UppaalTrait/Func3Trait.hpp>
#include <Exporter/UppaalTrait/GedfSchedulerTrait.hpp>

using namespace std;

class Wrapper : public FaultToleranceUppaalExporter {
public:
    Wrapper() : FaultToleranceUppaalExporter() {}
    void init(SystemModel *m, config_t *c, Schedule *s) { conf = c; sched = s; tg = m; }
};

static constexpr char expected[] =
"// Processors\n"
"P0 = Processor(0);\n"
"\n"
"// Tasks and edges\n"
"T0 = Task(0, 18, 999727);\n"
"T1 = Task(1, 0, 999727);\n"
"T2 = Task(2, 272, 1000000);\n"
"T3 = Task(3, 0, 1000000);\n"
"T4 = Task(4, 0, 1000000);\n"
"E0 = Edge(0, 1);\n"
"E1 = Edge(1, 2);\n"
"E2 = Edge(3, 4);\n"
"\n"
"// System\n"
"system Scheduler,\n"
"    P0, \n"
"    T0, T1, T2, T3, T4, E0, E1, E2;\n";

TEST_CASE("UPPAAL fault-tolerance exporter", "[exporter][uppaal]") {
    config_t conf;
    SystemModel tg;

    setup_config(conf, tg, "trivial-3p.xml");
    setup_tey(conf, tg, "dag4.tey");
    setup_nfp(conf, tg, "test.nfp");

    tg.global_deadline(S_TO_NS(0));

    for (auto &pass : conf.passes.passes()) {
        pass->run(&tg, &conf, static_cast<Schedule *>(nullptr));
    }

    Wrapper wrap;
    wrap.init(&tg, &conf, nullptr);

    UppaalExportConfiguration config(&tg, &conf);
    REQUIRE_THROWS(config.test_complete());

    // Apply some traits
    std::vector<std::unique_ptr<UppaalTrait>> traits;

    SECTION("FUNC3+GEDF", "[func3][gedf]") {
        traits.push_back(std::make_unique<Func3Trait>());
        traits.push_back(std::make_unique<GedfSchedulerTrait>());

        for (auto &trait: traits)
            trait->apply(config);

        REQUIRE_NOTHROW(config.test_complete());
        REQUIRE(expected == FaultToleranceUppaalExporter::get_system(config));
    }
}
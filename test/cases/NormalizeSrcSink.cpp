/*
 * Copyright (C) 2020 Stephen Nicholas Swatman
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include "config.hpp"
#include "Utils.hpp"
#include "CompilerPass.hpp"
#include "InputParser/InputParser.hpp"
#include "SystemModel.hpp"
#include "Schedule.hpp"
#include "Transform/Transform.hpp"
#include "Utils/Graph.hpp"

#include "test_utils.hpp"

#include <csignal>
#include <stdexcept>
#include "catch.hpp"
#include "Transform/TransitivClosure.hpp"
#include "Transform/NormalizeSrcSink.hpp"
#include <iostream>
#include <string>
#include <boost/algorithm/cxx11/any_of.hpp>

using namespace std;

TEST_CASE("Normalize sources and sinks", "[transform][normalize]") {
    TransitiveClosure trc;
    NormalizeSrcSink nss;

    SECTION("Single source, single sink") {
        config_t conf;
        SystemModel tg;
        setup_trivial(conf, tg);
        setup_tey(conf, tg, "normalize_1_1.tey");
        trc.run(&tg, &conf, nullptr);

        CHECK( tg.tasks().size() == 3 );
        REQUIRE( tg.task("s1") != nullptr );
        REQUIRE( tg.task("d1") != nullptr );
        REQUIRE( tg.task("c") != nullptr );

        REQUIRE_NOTHROW( nss.run(&tg, &conf, nullptr) );

        Task * s1 = tg.task("s1");

        CHECK( tg.tasks().size() == 3 );
        REQUIRE( s1 != nullptr );
        REQUIRE( tg.task("c") != nullptr );
        REQUIRE( tg.task("d1") != nullptr );

        CHECK( s1->predecessor_tasks().size() == 0 );
    }
    
    SECTION("Multiple sources, single sink") {
        config_t conf;
        SystemModel tg;
        setup_trivial(conf, tg);
        setup_tey(conf, tg, "normalize_n_1.tey");
        
        vector<vector<Task*>> dags;
        Utils::extractGraphs(&tg, &dags);
        for(vector<Task*> dag : dags){
            cout << "----------------------------" << endl;
            for(Task* t : dag)
                cout << "   " << t->id() << endl;
            cout << endl;
        }
        
        trc.run(&tg, &conf, nullptr);

        CHECK( tg.tasks().size() == 5 );
        REQUIRE( tg.task("s1") != nullptr );
        REQUIRE( tg.task("s2") != nullptr );
        REQUIRE( tg.task("s3") != nullptr );
        REQUIRE( tg.task("c") != nullptr );
        REQUIRE( tg.task("d1") != nullptr );

        REQUIRE_NOTHROW( nss.run(&tg, &conf, nullptr) );

        Task * s1 = tg.task("s1");
        Task * s2 = tg.task("s2");
        Task * s3 = tg.task("s3");
        Task * ce = tg.task("c");
        Task * d1 = tg.task("d1");

        CHECK( tg.tasks().size() == 6 );
        REQUIRE( s1 != nullptr );
        REQUIRE( s2 != nullptr );
        REQUIRE( s3 != nullptr );
        REQUIRE( ce != nullptr );
        REQUIRE( d1 != nullptr );

        CHECK( s1->predecessor_tasks().size() == 1 );
        CHECK( s2->predecessor_tasks().size() == 1 );
        cout << "-------------------------------------" << s3->predecessor_tasks().size() << endl;
        CHECK( s3->predecessor_tasks().size() == 1 );
        CHECK( ce->predecessor_tasks().size() == 3 );
        CHECK( d1->successor_tasks().size() == 0 );

        REQUIRE( s1->predecessor_tasks().size() == 1 );

        Task * ns = Utils::mapkeys(s1->predecessor_tasks())[0];

        CHECK( ns->predecessor_tasks().size() == 0 );

        CHECK( Utils::mapkeys(s2->predecessor_tasks())[0] == ns );
        CHECK( Utils::mapkeys(s3->predecessor_tasks())[0] == ns );

        std::vector<Task *> ns_succ = Utils::mapkeys(ns->successor_tasks());

        CHECK( ns_succ.size() == 3 );
        CHECK( std::find(ns_succ.begin(), ns_succ.end(), s1) != ns_succ.end());
        CHECK( std::find(ns_succ.begin(), ns_succ.end(), s2) != ns_succ.end());
        CHECK( std::find(ns_succ.begin(), ns_succ.end(), s3) != ns_succ.end());
    }

    SECTION("Single source, multiple sinks") {
        config_t conf;
        SystemModel tg;
        setup_trivial(conf, tg);
        setup_tey(conf, tg, "normalize_1_n.tey");
        trc.run(&tg, &conf, nullptr);

        CHECK( tg.tasks().size() == 5 );
        REQUIRE( tg.task("s1") != nullptr );
        REQUIRE( tg.task("c") != nullptr );
        REQUIRE( tg.task("d1") != nullptr );
        REQUIRE( tg.task("d2") != nullptr );
        REQUIRE( tg.task("d3") != nullptr );

        REQUIRE_NOTHROW( nss.run(&tg, &conf, nullptr) );

        Task * s1 = tg.task("s1");
        Task * ce = tg.task("c");
        Task * d1 = tg.task("d1");
        Task * d2 = tg.task("d2");
        Task * d3 = tg.task("d3");

        CHECK( tg.tasks().size() == 6 );
        REQUIRE( s1 != nullptr );
        REQUIRE( ce != nullptr );
        REQUIRE( d1 != nullptr );
        REQUIRE( d2 != nullptr );
        REQUIRE( d3 != nullptr );

        CHECK( s1->predecessor_tasks().size() == 0 );
        CHECK( ce->successor_tasks().size() == 3 );
        CHECK( d1->successor_tasks().size() == 1 );
        CHECK( d2->successor_tasks().size() == 1 );
        CHECK( d3->successor_tasks().size() == 1 );

        Task * ns = Utils::mapkeys(d1->successor_tasks())[0];

        CHECK( ns->successor_tasks().size() == 0 );

        CHECK( Utils::mapkeys(d2->successor_tasks())[0] == ns );
        CHECK( Utils::mapkeys(d3->successor_tasks())[0] == ns );

        std::vector<Task *> ns_pred = Utils::mapkeys(ns->predecessor_tasks());

        CHECK( ns_pred.size() == 3 );
        CHECK( std::find(ns_pred.begin(), ns_pred.end(), d1) != ns_pred.end());
        CHECK( std::find(ns_pred.begin(), ns_pred.end(), d2) != ns_pred.end());
        CHECK( std::find(ns_pred.begin(), ns_pred.end(), d3) != ns_pred.end());
    }
}

/*
 * Copyright (C) 2020 Stephen Nicholas Swatman <s.n.swatman@uva.nl> 
 *                    Univeristy of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <algorithm>

#include "SystemModel.hpp"
#include "Utils/Graph.hpp"
#include "Transform/QVectorFlattenSplitConnectors.hpp"
#include "Analyse/CycleCheck.hpp"
#include "Analyse/QVector.hpp"

#include "test_utils.hpp"
#include "catch.hpp"
#include "Transform/NormalizeSrcSink.hpp"

using namespace std;

TEST_CASE("QVectorFlattenSplitConnectors", "[transform][qvector][qvflatten][qvflattensplit]") {
    config_t conf;
    SystemModel tg, check_tg;

    setup_trivial(conf, tg);
    setup_trivial(conf, check_tg);
    auto *qv = conf.passes.add_pass(new QVector);
    auto *qvf = conf.passes.add_pass(new QVectorFlattenSplitConnectors);
    
    SECTION("Missing QVector") {
        setup_tey(conf, tg, "qvectorflatten1.tey");
        setup_tey(conf, check_tg, "qvectorflatten1.tey");

        
        qvf->run(&tg, &conf);
        
        REQUIRE( tg.tasks().size() == check_tg.tasks().size() );
        
        for(Task *t : check_tg.tasks_it()) {
            for(size_t i = 0 ; i < t->repeat() ; ++i) {
                Task *nt = tg.task(t->id()+"_r"+to_string(i));
                REQUIRE(nt != nullptr);

                if(t->id() != "print")
                    CHECK(t->inputs().size() == nt->inputs().size());

                if(t->id() != "mul" && t->id() != "print")
                    CHECK(t->outputs().size() == nt->outputs().size());

                if(t->id() != "mul" && t->id() != "print") {
                    CHECK(t->successor_tasks().size() == nt->successor_tasks().size());
                    vector<Task*> sucs = Utils::mapkeys(nt->successor_tasks());
                    for(Task::dep_t els : t->successor_tasks()) {
                        vector<Task*>::iterator it2 = find_if(sucs.begin(), sucs.end(), [els](Task *a) { return els.first->id()+"_r0" == a->id();});
                        REQUIRE(it2 != sucs.end());
                    }
                }

                if(t->id() != "print") {
                    CHECK(t->predecessor_tasks().size() == nt->predecessor_tasks().size());
                    vector<Task*> prevs = Utils::mapkeys(nt->predecessor_tasks());
                    for(Task::dep_t elp : t->predecessor_tasks()) {
                        vector<Task*>::iterator it2 = find_if(prevs.begin(), prevs.end(), [elp](Task *a) { return elp.first->id()+"_r0" == a->id();});
                        REQUIRE(it2 != prevs.end());
                    }
                }
            }
        }
    }
    
    SECTION("Flattened") {
        setup_tey(conf, tg, "qvectorflatten1.tey");
        setup_tey(conf, check_tg, "qvectorflatten1.tey");
        
        qv->run(&tg, &conf);
        qvf->run(&tg, &conf);
        
        REQUIRE(tg.tasks().size() == 9);
        
        for(Task *base : check_tg.tasks_it()) {
            for(size_t i = 0 ; i < base->repeat() ; ++i) {
                Task *rep = tg.task(base->id()+"_r"+to_string(i));
                REQUIRE(rep != nullptr);
                
                CHECK(rep->inputs().size() == rep->predecessor_tasks().size());
                CHECK(rep->outputs().size() == rep->successor_tasks().size());

                size_t supposed_to_rec = 0;
                for(Task::dep_t ep : base->predecessor_tasks()) {
                    for (Connection *conn : ep.second)
                        supposed_to_rec += conn->to()->tokens();
                }
                size_t currently_sent = 0;
                for(Task::dep_t ep : rep->predecessor_tasks()) {
                    for (Connection *conn : ep.second)
                        currently_sent += conn->to()->tokens();
                }
                CHECK(supposed_to_rec == currently_sent);
            }
        }
    }
}


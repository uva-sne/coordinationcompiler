/*
 * Copyright (C) 2020 Benjamin Rouxel <benjamin.rouxel@uva.nl> 
 *                    Univeristy of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include "config.hpp"
#include "SystemModel.hpp"
#include "Schedule.hpp"
#include "Utils/Log.hpp"
#include "CompilerPass.hpp"

#include "test_utils.hpp"
#include "catch.hpp"
#include "Simulator/Simulator.hpp"
#include "Simulator/Defense/Harmonic.hpp"

using namespace std;

class HarmonicWrapper : public Harmonic {
public:
    typedef uint64_t timinginfos_t;
    typedef std::map<timinginfos_t, std::map<timinginfos_t, char>> hasse_diagram_t;
    typedef std::vector<timinginfos_t> period_set_t;
    
    period_set_t get_periods_to_protect() { return Harmonic::get_periods_to_protect(); }
    std::vector<timinginfos_t> get_offsets_victims() { return Harmonic::get_offsets_victims(); }
    period_set_t get_cm_periods(const period_set_t &initP) { return Harmonic::get_cm_periods(initP);}
    void add_counter_measure(const period_set_t &periods) { Harmonic::add_counter_measure(periods); }
    
    std::vector<timinginfos_t> get_cm_wcet() { return cm_wcet; }
    
    void set_algo(Harmonic::cm_period_gen_algo algo) { cm_T_algo = algo; }
};

using namespace std;

TEST_CASE("DefenseHarmonic", "[simulator][scheduleak][defense]") {
    config_t conf;
    SystemModel tg;
    Schedule result(&tg, &conf);
    
    setup_config(conf, tg, "attack_scheduleak_defense_harmonic.xml");
    
//    SECTION("Fail") {
//        setup_tey(conf, tg, "scheduleak.tey");
//        
//        Simulator *schedleak = (Simulator*) conf.passes.back();
//        map<string, string> args;
//        args["victims"] = "victim, error";
//        schedleak->event_handler()->forward_params(args);
//        
//        for(CompilerPassPtr &p : conf.passes) {
//            REQUIRE_THROWS( p->run(&tg, &conf, &result));
//        }
//    }
//    
//    SECTION("Defense1") {
//        setup_tey(conf, tg, "scheduleak.tey");
//        
//        Simulator *schedleak = (Simulator*) conf.passes.back();
//        map<string, string> args;
//        args["victims"] = "victim";
//        schedleak->event_handler()->forward_params(args);
//        
//        for(CompilerPassPtr &p : conf.passes) {
//            REQUIRE_NOTHROW( p->run(&tg, &conf, &result));
//        }
//        CHECK(tg.task("HarmonicCM_8") != nullptr);
//    }
//    
//    SECTION("Defense Tainted") {
//        setup_tey(conf, tg, "tainted_harmonic.tey");
//        
//        for(CompilerPassPtr &p : conf.passes) {
//            REQUIRE_NOTHROW( p->run(&tg, &conf, &result));
//        }
//        
//        CHECK(tg.task("HarmonicCM_12") != nullptr);
//        CHECK(tg.task("HarmonicCM_20") != nullptr);
//        CHECK(tg.task("HarmonicCM_30") != nullptr);
//    }
//    
//    SECTION("Defense Tainted Reduced") {
//        setup_tey(conf, tg, "tainted_harmonic2.tey");
//        
//        for(CompilerPassPtr &p : conf.passes) {
//            REQUIRE_NOTHROW( p->run(&tg, &conf, &result));
//        }
//        
//        CHECK(tg.task("HarmonicCM_10") != nullptr);
//        CHECK(tg.task("HarmonicCM_75") != nullptr);
//        for(Task *t : tg.tasks_it()) {
//            if(t->id != "HarmonicCM_10" && t->id != "HarmonicCM_75")
//                CHECK(t->id.find("HarmonicCM_") == string::npos);
//        }
//    }
    SECTION("Test Hasse algorithms") {
//        HarmonicWrapper hw;
//        
//        HarmonicWrapper::period_set_t a {10, 20, 30, 10};
//        HarmonicWrapper::period_set_t b {20, 30, 50, 75};
//        HarmonicWrapper::period_set_t c {4, 6, 14, 21, 22, 33, 35, 49, 55, 121};
//        vector<HarmonicWrapper::period_set_t> tests {
//            a,b,c
//        };
//
//        for(HarmonicWrapper::period_set_t periods_to_protect : tests) {
//            sort(periods_to_protect.begin(), periods_to_protect.end());
//
//            hw.set_algo(Harmonic::cm_period_gen_algo::EXHAUSTIV);
//            HarmonicWrapper::period_set_t cm_periods = hw.get_cm_periods(periods_to_protect);
//
//            cout << "Final CM periods (exhaustiv): " << endl;
//            float util = 0;
//            for(HarmonicWrapper::timinginfos_t p : cm_periods) {
//                cout << p << " ";
//                util += hw.get_cm_wcet() / (float)p;
//            }
//            cout << " --- Utilisation: " << util << endl;
//
//            hw.set_algo(Harmonic::cm_period_gen_algo::GREEDY);
//            cm_periods = hw.get_cm_periods(periods_to_protect);
//
//            cout << "Final CM periods (greedy): " << endl;
//            util = 0;
//            for(HarmonicWrapper::timinginfos_t p : cm_periods) {
//                cout << p << " ";
//                util += hw.get_cm_wcet() / (float)p;
//            }
//            cout << " --- Utilisation: " << util << endl;
//
//            hw.set_algo(Harmonic::cm_period_gen_algo::TRIVIAL);
//            cm_periods = hw.get_cm_periods(periods_to_protect);
//
//            cout << "Final CM periods (trivial): " << endl;
//            util = 0;
//            for(HarmonicWrapper::timinginfos_t p : cm_periods) {
//                cout << p << " ";
//                util += hw.get_cm_wcet() / (float)p;
//            }
//            cout << " --- Utilisation: " << util << endl;
//        }
    }
}

/*
 * Copyright (C) 2020 Benjamin Rouxel <benjamin.rouxel@uva.nl> 
 *                    Univeristy of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include "config.hpp"
#include "SystemModel.hpp"
#include "Schedule.hpp"
#include "Simulator/OnlineSchedSim/SingleCore.hpp"
#include "Utils/Log.hpp"

#include "test_utils.hpp"
#include "catch.hpp"
#include <string>

using namespace std;

TEST_CASE("Online Sched Simulator Single Core", "[simulator][onlinesched][singlecore]") {
    config_t conf;
    SystemModel tg;
    Schedule result(&tg, &conf);

    setup_config(conf, tg, "onlinesim_1.xml");

    SECTION("Valid 1") {
        setup_tey(conf, tg, "onlinesim.tey");
        for(CompilerPassPtr &p : conf.passes.passes()) {
            Utils::DEBUG(string("--> calling ")+Utils::demangle(typeid(*p.get()).name()));
            REQUIRE_NOTHROW( p->run(&tg, &conf, &result));
        }
    }
}

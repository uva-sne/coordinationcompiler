/*
 * Copyright (C) 2023 Lukas Miedema <l.miedema@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <map>
#include <string>
#include <StateMachine/FaultTolerance/FaultRateSpec.hpp>
#include <StateMachine/StrategyStateMachine.hpp>
#include <config.hpp>
#include <SystemModel.hpp>
#include <Schedule.hpp>
#include <StateMachine/FaultTolerance/FaultToleranceStrategy.hpp>
#include <StateMachine/Generator/FaultTolerance/EnergyFtGenerator.hpp>
#include <test_utils.hpp>
#include <StateMachine/Transform/FaultTolerance/EnergyBoostTransform.hpp>
#include "catch.hpp"

namespace EnergyFtSsmTest {
    std::map<std::string, std::string> make_parameters() {
        std::map<std::string, std::string> stratGenParams;
        stratGenParams["ft-protected"] = "3MR";
        stratGenParams["ft-unprotected"] = "none";
        stratGenParams["fault-rate"] = "1:1000";
        stratGenParams["solver"] = "effls";
        return stratGenParams;
    }
}

TEST_CASE("SSM+EFT DvfsFaultRateSpec", "[ssm][eft][faultratespec]") {
    // Fault rate is voltage dependent
    // Simple frequency sweep across an order of magnitude
    DvfsFaultRateSpec spec("1:1000", "s", 100000, 200000);
    const double epsilon = 0.0000001;
    REQUIRE(std::fabs(spec.to_fraction(100000) - 0.01) < epsilon);
    REQUIRE(std::fabs(spec.to_fraction(110000) - 0.00794328) < epsilon);
    REQUIRE(std::fabs(spec.to_fraction(120000) - 0.00630957) < epsilon);
    REQUIRE(std::fabs(spec.to_fraction(130000) - 0.00501187) < epsilon);
    REQUIRE(std::fabs(spec.to_fraction(140000) - 0.00398107) < epsilon);
    REQUIRE(std::fabs(spec.to_fraction(150000) - 0.00316228) < epsilon);
    REQUIRE(std::fabs(spec.to_fraction(160000) - 0.00251189) < epsilon);
    REQUIRE(std::fabs(spec.to_fraction(170000) - 0.00199526) < epsilon);
    REQUIRE(std::fabs(spec.to_fraction(180000) - 0.00158489) < epsilon);
    REQUIRE(std::fabs(spec.to_fraction(190000) - 0.00125893) < epsilon);
    REQUIRE(std::fabs(spec.to_fraction(200000) - 0.001) < epsilon);
}

TEST_CASE("SSM+EFT energy ", "[ssm][eft][energy]") {
    config_t conf;
    SystemModel tg;
    StrategyStateMachine ssm(std::make_unique<Schedule>(&tg, &conf));
    setup_config(conf, tg, "e4H4E.xml");

    std::map<std::string, std::string> stratGenParams = EnergyFtSsmTest::make_parameters();

    SECTION("independent") {
        setup_tey(conf, tg, "10t-independent.tey"); // independent tasks generate more strategies
        setup_nfp(conf, tg, "10t-dvfs.nfp");

        SECTION("extreme slack") {
            // energy ft cannot prune as aggressively, so this should lead to more strategies than the non-energy ft ssm test
            tg.global_deadline(1000);

            EnergyFtGenerator strategyGenerator;
            strategyGenerator.set_params(stratGenParams);
            REQUIRE_NOTHROW(strategyGenerator.run(&tg, &conf, &ssm));
            CHECK(1024 == ssm.strategies().size());
            validate_ssm<EnergyFaultToleranceStrategy>(tg, conf, ssm);

            auto ftManager = dynamic_cast<FaultToleranceStrategyManager *>(ssm.strategy_manager());
            auto energyBefore = ssm.schedule()->energy();
            auto ftRateBefore = ftManager->compute_dtmc_rate(&ssm);

            CHECK(energyBefore > 0);
            CHECK(ftRateBefore.energy > 0);

            // Increase reliability by boosting the frequency
            EnergyBoostTransform boost;
        }

        SECTION("constrained schedule") {
            // should lead to few strategies
            tg.global_deadline(70);

            EnergyFtGenerator strategyGenerator;
            strategyGenerator.set_params(stratGenParams);
            REQUIRE_NOTHROW(strategyGenerator.run(&tg, &conf, &ssm));
            CHECK(18 == ssm.strategies().size());
            validate_ssm<EnergyFaultToleranceStrategy>(tg, conf, ssm);
        }

        SECTION("unschedulable") {
            // nothing is schedulable
            tg.global_deadline(50);

            EnergyFtGenerator strategyGenerator;
            strategyGenerator.set_params(stratGenParams);
            REQUIRE_NOTHROW(strategyGenerator.run(&tg, &conf, &ssm));
            CHECK(ssm.strategies().empty());
        }
    }
}
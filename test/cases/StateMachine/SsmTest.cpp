/*
 * Copyright (C) 2022 Lukas Miedema <l.miedema@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <config.hpp>
#include <SystemModel.hpp>
#include <StateMachine/StrategyStateMachine.hpp>
#include <Solvers/FastFLS/FFLS.hpp>
#include <Analyse/ScheduleValidationAnalyzer.hpp>
#include <StateMachine/Generator/FaultTolerance/ExhaustiveFtGenerator.hpp>
#include <StateMachine/Transform/FaultTolerance/PruneRedundantFtTransform.hpp>
#include <StateMachine/Generator/FaultTolerance/EnergyFtGenerator.hpp>
#include <StateMachine/Linker/FaultTolerance/EnergyAnnealingFtLinker.hpp>
#include <StateMachine/Transform/FaultTolerance/EnergyBoostTransform.hpp>
#include <StateMachine/Exporter/StrategySummaryViewer.hpp>
#include <StateMachine/Exporter/FaultTolerance/CsvRowWriter.hpp>
#include <Solvers/eFFLS/eFFLS.hpp>
#include <Transform/PropagateProperties.hpp>
#include <StateMachine/Group/StrategyScope.hpp>
#include <Exporter/SummaryViewer.hpp>

#include <test_utils.hpp>
#include "catch.hpp"

/**
 * Dummy strategy manager
 */
class FooManager : public StrategyManager {
public:
    FooManager(Solver *solver) : StrategyManager(solver) {}

    std::string description() const override {
        return "foo";
    }

    std::vector<Result *> results(const Strategy *strategy) const override {
        throw "results not implemented";
    }

    std::vector<Result *> build_results(StrategyStateMachine &ssm, const std::vector<SchedElt *> &elements) override {
        throw "build_results not implemented";
    }
};

class FooStrategy : public Strategy {
public:
    bool is_trivial() override {
        return false;
    }

    schedulability_result_t is_schedulable(SystemModel &tg, config_t &config, StrategyManager &manager, std::vector<SchedEltPtr> &baseElements, Schedule &emptySchedule) const override {
        return { .is_schedulable = false, .makespan = 0 };
    }

    std::string get_role_for_elt(uint64_t i, const StrategyManager &manager) override {
        return std::string();
    }

    std::map<std::string, SchedEltPtr> export_to_schedule(std::vector<SchedEltPtr> &elements, StrategyManager &manager) const override {
        return std::map<std::string, SchedEltPtr>();
    }
};

TEST_CASE("Strategy specification in config", "[ssm][config]") {
    config_t conf;
    SystemModel tg;
    StrategyStateMachine ssm(std::make_unique<Schedule>(&tg, &conf));

    SECTION("energy ssm") {
        setup_config(conf, tg, "ssm_ft.xml");
        setup_tey(conf, tg, "energy_DAG_reduced.tey");
        setup_nfp(conf, tg, "energy_nfp.nfp");

        REQUIRE(conf.passes.passes().size() == 7);

        auto *propagateProperties = conf.passes->at(0).get();
        auto *energyFullStateSpace = conf.passes->at(1).get();
        auto *energyAnnealing = conf.passes->at(2).get();
        auto *energyBoostTransform = conf.passes->at(3).get();
        auto *strategySummary = conf.passes->at(4).get();
        auto *csv = conf.passes->at(5).get();
        auto *scope = conf.passes->at(6).get();

        REQUIRE(dynamic_cast<PropagateProperties *>(propagateProperties) != nullptr);
        REQUIRE(dynamic_cast<EnergyFtGenerator *>(energyFullStateSpace) != nullptr);
        Solver *solver = dynamic_cast<EnergyFtGenerator *>(energyFullStateSpace)->build_manager()->solver();
        REQUIRE(dynamic_cast<EnergyFFLS *>(solver) != nullptr);
        REQUIRE(dynamic_cast<EnergyAnnealingFtLinker *>(energyAnnealing) != nullptr);
        REQUIRE(dynamic_cast<EnergyBoostTransform *>(energyBoostTransform) != nullptr);
        REQUIRE(dynamic_cast<StrategySummaryViewer *>(strategySummary) != nullptr);
        REQUIRE(dynamic_cast<CsvRowWriter *>(csv) != nullptr);
        auto *strategyScope = dynamic_cast<StrategyScope *>(scope);
        REQUIRE(strategyScope != nullptr);

        REQUIRE(strategyScope->dispatcher().passes().size() == 1);
        auto *summary = strategyScope->dispatcher()->at(0).get();
        REQUIRE(dynamic_cast<SummaryViewer *>(summary) != nullptr);

        REQUIRE_NOTHROW(conf.passes.dispatch(&tg, &conf, &ssm));
    }
}

TEST_CASE("Strategy in IR", "[ssm]") {
    config_t conf;
    SystemModel tg;
    StrategyStateMachine ssm(std::make_unique<Schedule>(&tg, &conf));

    setup_trivial(conf, tg);
    setup_tey(conf, tg, "many.tey");

    std::map<std::string, std::string> stratGenParams;
    stratGenParams["ft-protected"] = "3MR";
    stratGenParams["ft-unprotected"] = "none";
    stratGenParams["fault-rate"] = "1:1000";
    stratGenParams["solver"] = "ffls";

    SECTION("IR without SSM") {
        // No SSM (trivial SSM) must work as a schedule in every way
        REQUIRE(ssm.is_trivial());
        REQUIRE_NOTHROW(FastForwardListScheduling().run(&tg, &conf, &ssm));
        REQUIRE(ssm.schedule()->is_built());

        // SSM ops fail now
        ExhaustiveFtGenerator strategyGenerator;
        strategyGenerator.set_params(stratGenParams);
        REQUIRE_THROWS(strategyGenerator.run(&tg, &conf, &ssm));
    }

    SECTION("IR with SSM") {
        REQUIRE(ssm.is_trivial()); // start without an SSM

        // Convert to SSM<FaultTolerance>
        ExhaustiveFtGenerator strategyGenerator;
        strategyGenerator.set_params(stratGenParams);
        REQUIRE_NOTHROW(strategyGenerator.run(&tg, &conf, &ssm));

        REQUIRE(ssm.schedule()->is_built());
        REQUIRE(!ssm.is_trivial());
        REQUIRE(typeid(*ssm.strategy_manager()) == typeid(FaultToleranceStrategyManager));
        REQUIRE(typeid(*ssm.strategy_manager()->solver()) == typeid(FastForwardListScheduling));

        // SSM<FaultTolerance> ops work now
        REQUIRE_NOTHROW(PruneRedundantFtTransform().run(&tg, &conf, &ssm));

        // Regular ops should fail now
        REQUIRE_THROWS(FastForwardListScheduling().run(&tg, &conf, &ssm));
    }

    SECTION("IR with incompatible SSM") {
        REQUIRE(ssm.is_trivial());

        // Assign the FooManager as SSM
        FastForwardListScheduling ffls;
        ssm.strategy_manager(std::make_unique<FooManager>(&ffls));

        // Note: EmptyStrategy + FooManager is _allowed_, it is up to the SM to determine the interpretation of the strategies, which may even be heterogeneous
        REQUIRE(!ssm.is_trivial());

        // Run an SSM op for a different SM fails now
        REQUIRE_THROWS(PruneRedundantFtTransform().run(&tg, &conf, &ssm));
    }

    SECTION("Scope") {
        REQUIRE(ssm.is_trivial());

        ExhaustiveFtGenerator strategyGenerator;
        strategyGenerator.set_params(stratGenParams);
        REQUIRE_NOTHROW(strategyGenerator.run(&tg, &conf, &ssm));

        // For this there must be at least one strategy, this strategy protects all (as there is no deadline in tg)
        REQUIRE(ssm.strategies().size() > 0);

        Strategy *scopeStrategy = ssm.strategies()[0].get();
        REQUIRE(typeid(*scopeStrategy) == typeid(FaultToleranceStrategy));
        const std::vector<bool> &pe = dynamic_cast<FaultToleranceStrategy *>(scopeStrategy)->protected_elements();

        // Assumption of this test: all elements are under FT
        REQUIRE(std::all_of(pe.begin(), pe.end(), [](bool is_protected) { return is_protected; }));
        REQUIRE(ssm.schedule()->status() == ScheduleProperties::SCHED_NOTDONE);
        REQUIRE(ssm.schedule()->elements_lut().size() == tg.tasks().size());

        // Enter scope
        ssm.enter_scope(scopeStrategy, &tg, &conf);
        REQUIRE(ssm.schedule()->status() == ScheduleProperties::SCHED_COMPLETE);
        REQUIRE(ssm.schedule()->elements_lut().size() == tg.tasks().size() * 4); // all tasks under 3MR + voters

        // Regular pass should work
        REQUIRE_NOTHROW(ScheduleValidationAnalyzer().run(&tg, &conf, ssm));

        // Even a schedule pass should work (will destroy TMR-generated SchedElt copies)
        REQUIRE_NOTHROW(FastForwardListScheduling().run(&tg, &conf, ssm));
        REQUIRE(ssm.schedule()->elements_lut().size() == tg.tasks().size()); // no TMR copies

        // Exit scope
        ssm.exit_scope();
        REQUIRE(ssm.schedule()->status() == ScheduleProperties::SCHED_NOTDONE); // no assignment, elements are there

        // Entering the scope again would restore the TMR copies
        ssm.enter_scope(scopeStrategy, &tg, &conf);
        REQUIRE(ssm.schedule()->elements_lut().size() == tg.tasks().size() * 4); // all tasks under 3MR + voters
        REQUIRE(ssm.schedule()->status() == ScheduleProperties::SCHED_COMPLETE);
        ssm.exit_scope();
        REQUIRE(ssm.schedule()->status() == ScheduleProperties::SCHED_NOTDONE); // no assignment, elements are there
    }
}
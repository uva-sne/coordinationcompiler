/*
 * Copyright (C) 2022 Lukas Miedema <l.miedema@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <config.hpp>
#include <SystemModel.hpp>
#include <StateMachine/StrategyStateMachine.hpp>
#include <Solvers/FastFLS/FFLS.hpp>
#include <StateMachine/Generator/FaultTolerance/ExhaustiveFtGenerator.hpp>
#include <StateMachine/Transform/FaultTolerance/PruneRedundantFtTransform.hpp>
#include <StateMachine/FaultTolerance/NMR.hpp>
#include <StateMachine/Generator/FaultTolerance/TrivialFtGenerator.hpp>
#include <test_utils.hpp>
#include "catch.hpp"

namespace FaultToleranceSsmTest {
    std::map<std::string, SchedEltPtr> apply_conversion(std::map<std::string, SchedEltPtr> &original, FaultToleranceScheme *ft1, FaultToleranceScheme *ft2 = nullptr) {
        auto ft1State = ft1->new_state();
        auto ft2State = ft2 == nullptr ? nullptr : ft2->new_state();

        auto globalState = mapping_state_t();
        std::map<std::string, SchedEltPtr> ftLut;

        size_t index = 0;
        for (auto &element: original) {
            auto &activeFt = (index % 2 == 0 || ft2 == nullptr) ? ft1 : ft2;
            auto &activeState = (index % 2 == 0 || ft2 == nullptr) ? ft1State : ft2State;
            activeFt->apply(element.second.get(), &ftLut, activeState.get(), &globalState);
            index++;
        }
        return ftLut;
    }

    std::map<std::string, std::string> make_parameters() {
        std::map<std::string, std::string> stratGenParams;
        stratGenParams["ft-protected"] = "3MR";
        stratGenParams["ft-unprotected"] = "none";
        stratGenParams["fault-rate"] = "1:1000";
        stratGenParams["solver"] = "ffls";
        return stratGenParams;
    }
}

TEST_CASE("SSM+FT NMR", "[ssm][ft][scheme]") {
    config_t conf;
    SystemModel tg;
    StrategyStateMachine ssm(std::make_unique<Schedule>(&tg, &conf));
    setup_trivial(conf, tg);
    setup_tey(conf, tg, "many.tey");
    FastForwardListScheduling().run(&tg, &conf, &ssm);

    auto &lut = ssm.schedule()->elements_lut();

    SECTION("only nullft") {
        std::unique_ptr<FaultToleranceScheme> ft = std::make_unique<NullFaultTolerance>();
        REQUIRE_FALSE(ft->new_state().get() == ft->new_state().get());
        REQUIRE_FALSE(ft->has_detect_capability());

        auto updatedLut = FaultToleranceSsmTest::apply_conversion(lut, ft.get());
        REQUIRE(lut.size() == updatedLut.size());

        // They should be equal, but share no elements
        for (auto &entry : lut) {
            std::string replicaId = entry.first;
            replicaId.replace(replicaId.find("_t"), 2, "_1_t");

            REQUIRE(updatedLut.count(replicaId) == 1);
            auto &original = entry.second;
            auto &update = updatedLut[replicaId];

            REQUIRE_FALSE(original.get() == update.get());

            // Check connections
            REQUIRE(original->previous().size() == update->previous().size());
            REQUIRE(original->successors().size() == update->successors().size());

            for (SchedElt *e1 : original->successors())
                REQUIRE(std::any_of(update->successors().begin(), update->successors().end(), [e1](SchedElt *e2) { return e1->id() == e2->id(); }));
            for (SchedElt *e1 : original->previous())
                REQUIRE(std::any_of(update->previous().begin(), update->successors().end(), [e1](SchedElt *e2) { return e1->id() == e2->id(); }));
        }
    }

    SECTION("only 3MR") {
        std::unique_ptr<FaultToleranceScheme> tmr = std::make_unique<NMR_3>();
        auto updatedLut = FaultToleranceSsmTest::apply_conversion(lut, tmr.get());
        REQUIRE(updatedLut.size() == lut.size() * 4);
        REQUIRE(tmr->has_detect_capability());

        // Should be at least one voter task in the lot
        REQUIRE(std::any_of(updatedLut.begin(), updatedLut.end(), [](auto &entry) { return entry.second->task()->id() == "3mr-voter-task"; }));

        // All voter tasks should be connected to 3 replicas
        for (auto &entry : updatedLut) {
            auto &elem = entry.second;
            if (elem->task()->id() == "3mr-voter-task") {
                auto &predecessors = elem->previous();
                REQUIRE(predecessors.size() == 3); // 3 replicas

                // All predecessors are replicas, so _their_ predecessors are equal
                const std::vector<SchedElt *> &zeroth = predecessors[0]->previous();
                for (size_t i = 1; i < predecessors.size(); i++) {
                    const std::vector<SchedElt *> &nth = predecessors[i]->previous();
                    // zeroth must match nth
                    REQUIRE(zeroth.size() == nth.size());
                    for (SchedElt *expected : zeroth) {
                        REQUIRE(std::any_of(nth.begin(), nth.end(), [expected](auto *got) { return expected == got; })); // note: ptr comparison
                    }
                }
            }
        }
    }

    SECTION("3MR/2MR") {
        std::unique_ptr<FaultToleranceScheme> tmr = std::make_unique<NMR_3>();
        std::unique_ptr<FaultToleranceScheme> dmr = std::make_unique<NMR_2>();

        auto updatedLut = FaultToleranceSsmTest::apply_conversion(lut, tmr.get(), dmr.get());
        REQUIRE(updatedLut.size() < lut.size() * 4); // 3 replicas + voter
        REQUIRE(updatedLut.size() > lut.size() * 3);

        // Should contain lut.size() number of voters as each task has a voter
        size_t tmrVoters = std::count_if(updatedLut.begin(),  updatedLut.end(), [](auto &e) { return e.second->task()->id() == "3mr-voter-task"; });
        size_t dmrVoters = std::count_if(updatedLut.begin(),  updatedLut.end(), [](auto &e) { return e.second->task()->id() == "2mr-voter-task"; });
        REQUIRE(dmrVoters == tmrVoters);
        REQUIRE(tmrVoters + dmrVoters == lut.size());
    }
}

TEST_CASE("SSM+FT exhaustive", "[ssm][ft][exhaustive]") {
    config_t conf;
    SystemModel tg;
    StrategyStateMachine ssm(std::make_unique<Schedule>(&tg, &conf));
    setup_config(conf, tg, "4H4E.xml");

    std::map<std::string, std::string> stratGenParams = FaultToleranceSsmTest::make_parameters();

    SECTION("independent") {
        setup_tey(conf, tg, "10t-independent.tey"); // independent tasks generate more strategies
        setup_nfp(conf, tg, "10t.nfp");

        SECTION("some slack") {
            // should lead to a lot of strategies
            tg.global_deadline(100);

            ExhaustiveFtGenerator strategyGenerator;
            strategyGenerator.set_params(stratGenParams);
            REQUIRE_NOTHROW(strategyGenerator.run(&tg, &conf, &ssm));
            CHECK(58 == ssm.strategies().size());
            validate_ssm<FaultToleranceStrategy>(tg, conf, ssm);
        }

        SECTION("extreme slack") {
            // should lead to just one strategy that protects all
            tg.global_deadline(1000);

            ExhaustiveFtGenerator strategyGenerator;
            strategyGenerator.set_params(stratGenParams);
            REQUIRE_NOTHROW(strategyGenerator.run(&tg, &conf, &ssm));
            CHECK(1 == ssm.strategies().size());
            validate_ssm<FaultToleranceStrategy>(tg, conf, ssm);
        }

        SECTION("constrained schedule") {
            // should lead to few strategies
            tg.global_deadline(70);

            ExhaustiveFtGenerator strategyGenerator;
            strategyGenerator.set_params(stratGenParams);
            REQUIRE_NOTHROW(strategyGenerator.run(&tg, &conf, &ssm));
            CHECK(4 == ssm.strategies().size());
            validate_ssm<FaultToleranceStrategy>(tg, conf, ssm);
        }

        SECTION("unschedulable") {
            // nothing is schedulable
            tg.global_deadline(50);

            ExhaustiveFtGenerator strategyGenerator;
            strategyGenerator.set_params(stratGenParams);
            REQUIRE_NOTHROW(strategyGenerator.run(&tg, &conf, &ssm));
            CHECK(ssm.strategies().empty());
        }
    }

    SECTION("precedence relation") {
        setup_tey(conf, tg, "10t-chain.tey"); // all in a chain limits the strategies considerably
        setup_nfp(conf, tg, "10t.nfp");

        SECTION("lots of slack") {
            tg.global_deadline(1000);

            ExhaustiveFtGenerator strategyGenerator;
            strategyGenerator.set_params(stratGenParams);
            REQUIRE_NOTHROW(strategyGenerator.run(&tg, &conf, &ssm));
            CHECK(1 == ssm.strategies().size());
            validate_ssm<FaultToleranceStrategy>(tg, conf, ssm);

            auto *strategy = dynamic_cast<FaultToleranceStrategy * >(ssm.strategies()[0].get());
            auto &protectedElements = strategy->protected_elements();
            REQUIRE(std::all_of(protectedElements.begin(), protectedElements.end(), [](bool element) { return element; }));
        }

        SECTION("constrained") {
            tg.global_deadline(330); // = ft makespan
            stratGenParams["ft-protected"] = "5MR";

            // With 4H4E, 3MR can just use the extra big cores to do replication in space
            // 5MR forces the use of at least one little core, which means not everything can be protected

            ExhaustiveFtGenerator strategyGenerator;
            strategyGenerator.set_params(stratGenParams);
            REQUIRE_NOTHROW(strategyGenerator.run(&tg, &conf, &ssm));
            CHECK(1 == ssm.strategies().size());
            validate_ssm<FaultToleranceStrategy>(tg, conf, ssm);

            auto *strategy = dynamic_cast<FaultToleranceStrategy * >(ssm.strategies()[0].get());
            auto &protectedElements = strategy->protected_elements();

            auto elements = ssm.schedule()->elements_unowned();
            for (size_t i = 0; i < elements.size(); i++) {
                Version *hi = elements[i]->task()->version("hi");
                Version *lo = elements[i]->task()->version("lo");
                if (protectedElements[i]) {
                    REQUIRE(hi != nullptr);
                    REQUIRE(lo != nullptr);
                    CHECK(hi->C() == lo->C());
                } else {
                    CHECK((lo == nullptr || hi->C() < lo->C()));
                }
            }
        }
    }
}

TEST_CASE("SSM+FT trivial", "[ssm][ft][trivial]") {
    config_t conf;
    SystemModel tg;
    StrategyStateMachine ssm(std::make_unique<Schedule>(&tg, &conf));
    setup_config(conf, tg, "4H4E.xml");

    std::map<std::string, std::string> stratGenParams = FaultToleranceSsmTest::make_parameters();

    setup_tey(conf, tg, "10t-independent.tey"); // independent tasks generate more strategies
    setup_nfp(conf, tg, "10t.nfp");

    // should lead to a lot of strategies, but the trivial generator makes just one protecting nothing
    tg.global_deadline(100);

    TrivialFtGenerator strategyGenerator;
    strategyGenerator.set_params(stratGenParams);
    REQUIRE_NOTHROW(strategyGenerator.run(&tg, &conf, &ssm));
    CHECK(1 == ssm.strategies().size());
    validate_ssm<FaultToleranceStrategy>(tg, conf, ssm);

    auto *strategy = dynamic_cast<FaultToleranceStrategy * >(ssm.strategies()[0].get());
    auto &protectedElements = strategy->protected_elements();
    REQUIRE(std::none_of(protectedElements.begin(), protectedElements.end(), [](bool e) { return e; }));

}


TEST_CASE("SSM+FT prune redundant", "[ssm][ft][prune-redundant]") {
    config_t conf;
    SystemModel tg;
    StrategyStateMachine ssm(std::make_unique<Schedule>(&tg, &conf));
    setup_config(conf, tg, "4H4E.xml");

    setup_tey(conf, tg, "10t-chain.tey"); // all in a chain limits the usable strategies considerably
    setup_nfp(conf, tg, "10t.nfp");

    // Create a SSM
    auto scheduler = FastForwardListScheduling();
    auto ftUnprotect = NullFaultTolerance();
    auto ftProtect = NMR_3();
    auto faultRate = FaultRateSpec("1:1000", "s");
    ftProtect.fault_rate(&faultRate);
    ftUnprotect.fault_rate(&faultRate);

    ssm.initialize_ssm(std::make_unique<FaultToleranceStrategyManager>(&ftProtect, &ftUnprotect, &scheduler));
    std::vector<StrategyPtr> &strategies = ssm.strategies();

    auto protectNone = std::make_unique<FaultToleranceStrategy>(0,
            std::vector<bool>({ false, false, false, false, false, false, false, false, false, false }));
    auto protectLast = std::make_unique<FaultToleranceStrategy>(1,
            std::vector<bool>({ false, false, false, false, false, false, false, false, false, true }));
    auto protectFirst = std::make_unique<FaultToleranceStrategy>(2,
            std::vector<bool>({ true, false, false, false, false, false, false, false, false, false }));

    auto protectSome = std::make_unique<FaultToleranceStrategy>(3,
            std::vector<bool>({ false, false, false, true, false, true, true, false, false, false }));
    auto protectOthers = std::make_unique<FaultToleranceStrategy>(4,
            std::vector<bool>({ false, false, false, true, true, false, false, false, false, false }));

    strategies.push_back(std::move(protectNone));
    strategies.push_back(std::move(protectLast));
    strategies.push_back(std::move(protectFirst));
    strategies.push_back(std::move(protectSome));
    strategies.push_back(std::move(protectOthers));
    REQUIRE_FALSE(ssm.is_trivial());

    REQUIRE(strategies.size() == 5);

    REQUIRE_NOTHROW(PruneRedundantFtTransform().run(&tg, &conf, &ssm));

    // Expected result:
    // protectNone is < all
    // protectLast is < protectFirst, because they're in a chain (due to the precedence relation)
    // protectFirst, protectSome and protectOthers don't have an order, so they're all kept
    REQUIRE(strategies.size() == 3);

    std::vector<uint64_t> ids;
    std::transform(strategies.begin(),  strategies.end(), std::back_inserter(ids), [](StrategyPtr &s) { return s->id(); });
    REQUIRE(std::vector<uint64_t>{ 2, 3, 4 } == ids);
}
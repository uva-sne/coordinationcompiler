/*
 * Copyright (C) 2020 Stephen Nicholas Swatman <s.n.swatman@uva.nl> 
 *                    Univeristy of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "SystemModel.hpp"
#include "Schedule.hpp"
#include "InputParser/InputParser.hpp"
#include "Transform/Transform.hpp"
#include "Transform/TransitivClosure.hpp"

#include "test_utils.hpp"

#include "catch.hpp"

#define HAS_SUCCESSOR(a, b) contains(a->successors_transitiv(), b)
#define HAS_PREDECESSOR(a, b) contains(a->previous_transitiv(), b)

TEST_CASE("Transitive closure", "[transform][transitive_closure]") {
    config_t conf;
    SystemModel tg;
    Schedule result(&tg, &conf);

    setup_trivial(conf, tg);

    SECTION("Simple") {
        setup_tey(conf, tg, "transitiveclosure.tey");

        Task * n1 = tg.task("p1");
        Task * n2 = tg.task("p2");
        Task * n3 = tg.task("p3");
        Task * n4 = tg.task("p4");
        Task * n5 = tg.task("p5");
        Task * n6 = tg.task("p6");
        Task * n7 = tg.task("p7");
        Task * n8 = tg.task("p8");
        
        REQUIRE( n1 != nullptr );
        REQUIRE( n2 != nullptr );
        REQUIRE( n3 != nullptr );
        REQUIRE( n4 != nullptr );
        REQUIRE( n5 != nullptr );
        REQUIRE( n6 != nullptr );
        REQUIRE( n7 != nullptr );
        REQUIRE( n8 != nullptr );

        REQUIRE_NOTHROW( registry::Registry<Transform, std::string>::Create("transitive_closure")->run(&tg, &conf, &result) );

        CHECK( n7->previous_transitiv().size() == 0 );
        CHECK( n1->previous_transitiv().size() == 0 );
        CHECK( n6->previous_transitiv().size() == 0 );
        CHECK( n5->successors_transitiv().size() == 0 );
        CHECK( n8->successors_transitiv().size() == 0 );

        CHECK( HAS_SUCCESSOR(n7, n2) );
        CHECK( HAS_PREDECESSOR(n2, n7) );

        CHECK( HAS_SUCCESSOR(n7, n3) );
        CHECK( HAS_PREDECESSOR(n3, n7) );

        CHECK( !HAS_SUCCESSOR(n7, n1) );
        CHECK( !HAS_PREDECESSOR(n1, n7) );

        CHECK( !HAS_SUCCESSOR(n7, n6) );
        CHECK( !HAS_PREDECESSOR(n6, n7) );

        CHECK( HAS_SUCCESSOR(n7, n4) );
        CHECK( HAS_PREDECESSOR(n4, n7) );

        CHECK( HAS_SUCCESSOR(n7, n5) );
        CHECK( HAS_PREDECESSOR(n5, n7) );

        CHECK( HAS_SUCCESSOR(n2, n3) );
        CHECK( HAS_PREDECESSOR(n3, n2) );

        CHECK( !HAS_SUCCESSOR(n2, n7) );
        CHECK( !HAS_PREDECESSOR(n7, n2) );

        CHECK( !HAS_SUCCESSOR(n2, n1) );
        CHECK( !HAS_PREDECESSOR(n1, n2) );

        CHECK( HAS_SUCCESSOR(n2, n5) );
        CHECK( HAS_PREDECESSOR(n5, n2) );

        CHECK( HAS_SUCCESSOR(n6, n5) );
        CHECK( HAS_PREDECESSOR(n5, n6) );

        CHECK( !HAS_SUCCESSOR(n6, n3) );
        CHECK( !HAS_PREDECESSOR(n3, n3) );

        CHECK( !HAS_SUCCESSOR(n6, n7) );
        CHECK( !HAS_PREDECESSOR(n7, n6) );

        CHECK( HAS_SUCCESSOR(n1, n3) );
        CHECK( HAS_PREDECESSOR(n3, n1) );

        CHECK( !HAS_SUCCESSOR(n1, n2) );
        CHECK( !HAS_PREDECESSOR(n2, n1) );

        CHECK( !HAS_SUCCESSOR(n1, n7) );
        CHECK( !HAS_PREDECESSOR(n7, n1) );

        CHECK( !HAS_SUCCESSOR(n1, n6) );
        CHECK( !HAS_PREDECESSOR(n6, n1) );

        CHECK( HAS_SUCCESSOR(n1, n4) );
        CHECK( HAS_PREDECESSOR(n4, n1) );

        CHECK( HAS_SUCCESSOR(n1, n5) );
        CHECK( HAS_PREDECESSOR(n5, n1) );

        CHECK( HAS_SUCCESSOR(n3, n5) );
        CHECK( HAS_PREDECESSOR(n5, n3) );

        CHECK( HAS_SUCCESSOR(n3, n4) );
        CHECK( HAS_PREDECESSOR(n4, n3) );

        CHECK( !HAS_SUCCESSOR(n3, n6) );
        CHECK( !HAS_PREDECESSOR(n6, n3) );

        CHECK( !HAS_SUCCESSOR(n3, n1) );
        CHECK( !HAS_PREDECESSOR(n1, n3) );

        CHECK( !HAS_SUCCESSOR(n3, n7) );
        CHECK( !HAS_PREDECESSOR(n7, n3) );

        CHECK( HAS_SUCCESSOR(n4, n8) );
        CHECK( HAS_PREDECESSOR(n8, n4) );

        CHECK( !HAS_SUCCESSOR(n5, n8) );
        CHECK( !HAS_PREDECESSOR(n8, n5) );

        CHECK( !HAS_SUCCESSOR(n8, n5) );
        CHECK( !HAS_PREDECESSOR(n5, n8) );

        CHECK( HAS_SUCCESSOR(n7, n8) );
        CHECK( HAS_PREDECESSOR(n8, n7) );
    }
}

/*
 * Copyright (C) 2020 Lukas Miedema <l.miedema@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <Schedule.hpp>
#include "SystemModel.hpp"
#include "Analyse/ScheduleValidationAnalyzer.hpp"

#include "test_utils.hpp"
#include "catch.hpp"
#include "Exporter/DotViewer.hpp"
#include "ScheduleFactory.hpp"

#define SECONDS(time)\
    time ## LLU * 1000 * 1000 * 1000

TEST_CASE("ScheduleValidator", "[analysis][analyse-validator]") {
    config_t conf;
    SystemModel tg;
    std::map<std::string, std::string> parameters;
    ScheduleValidationAnalyzer validator;

    parameters.emplace("throwerror", CONFIG_TRUE);
    validator.forward_params(parameters);
    setup_config(conf, tg, "trivial.xml");
    setup_tey(conf, tg, "and.tey");
    setup_nfp(conf, tg, "and.nfp");
    
    SECTION("No schedule") {
        REQUIRE_THROWS(validator.run(&tg, &conf, nullptr));
    }
    
    // Create a schedule by hand
    Schedule s(&tg, &conf);
    s.status(ScheduleProperties::sched_statue_e::SCHED_COMPLETE);
    s.makespan(SECONDS(31));
    for (ComputeUnit *p: tg.processors())
        ScheduleFactory::add_core(&s, p);
    REQUIRE(s.schedcores().size() == 2);

    SchedCore *proc1 = *std::find_if(s.schedcores().begin(), s.schedcores().end(),
            [](SchedCore*sc) { return sc->core()->id == "P1"; });
    SchedCore *proc2 = *std::find_if(s.schedcores().begin(), s.schedcores().end(),
            [](SchedCore *sc) { return sc->core()->id == "P2"; });

    ScheduleFactory::build_elements(&s);

    SECTION("Invalid schedule: no task<->core assignment") {
        REQUIRE_THROWS(validator.run(&tg, &conf, &s));
    }
    
    Task *tp1, *tp2, *tp3;
    tp1 = tg.task("p1");
    tp2 = tg.task("p2");
    tp3 = tg.task("p3");

    SchedJob *p1, *p2, *p3;
    p1 = s.schedjob("p1_0_t");
    p2 = s.schedjob("p2_0_t");
    p3 = s.schedjob("p3_0_t");
    REQUIRE(p1 != nullptr);
    REQUIRE(p2 != nullptr);
    REQUIRE(p3 != nullptr);

    p1->rt(SECONDS(0));
    p2->rt(SECONDS(12));
    p3->rt(SECONDS(27));
    
    p1->wct(tp1->versions()[0]->C());
    p2->wct(tp2->versions()[0]->C());
    p3->wct(tp3->versions()[0]->C());

    p1->selected_version("main");
    p2->selected_version("main");
    p3->selected_version("main");

    s.map_core(p1, proc2);
    s.map_core(p2, proc2);
    s.map_core(p3, proc2);
    
    s.schedule(p1);
    s.schedule(p2);
    s.schedule(p3);

    SECTION("Valid schedule") {
        REQUIRE_NOTHROW(validator.run(&tg, &conf, &s));
    }

    SECTION("Invalid schedule: makespan overrun") {
        s.makespan(1);
        REQUIRE_THROWS(validator.run(&tg, &conf, &s));
    }

    SECTION("Invalid schedule: task starts before dependency ends") {
        p2->rt(SECONDS(5));
        REQUIRE_THROWS(validator.run(&tg, &conf, &s));
    }

    SECTION("Invalid schedule: overallocated core") {
        p3->rt(SECONDS(14));
        REQUIRE_THROWS(validator.run(&tg, &conf, &s));
    }

    SECTION("Invalid schedule: illegal processor assignment") {
        s.map_core(p3, proc1); // incompatible
        REQUIRE_THROWS(validator.run(&tg, &conf, &s));
    }

    SECTION("Invalid schedule: incorrect WCET assignment") {
        p3->wct(SECONDS(1));
        REQUIRE_THROWS(validator.run(&tg, &conf, &s));
    }

    SECTION("Invalid schedule: no version assignment") {
        p1->selected_version("");
        REQUIRE_THROWS(validator.run(&tg, &conf, &s));
    }
}
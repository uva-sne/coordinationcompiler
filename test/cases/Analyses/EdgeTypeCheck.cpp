/*
 * Copyright (C) 2020 Stephen Nicholas Swatman <s.n.swatman@uva.nl> 
 *                    Univeristy of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include "SystemModel.hpp"
#include "Analyse/EdgeTypeCheck.hpp"

#include "test_utils.hpp"
#include "catch.hpp"

TEST_CASE("Edge type check", "[analysis][type_check]") {
    config_t conf;
    SystemModel tg;

    setup_trivial(conf, tg);

    EdgeTypeCheck phase;

    SECTION("Valid 1") {
        setup_tey(conf, tg, "edgetype_good1.tey");
        REQUIRE_NOTHROW( phase.run(&tg, &conf, nullptr) );
    }

    SECTION("Valid 2") {
        setup_tey(conf, tg, "edgetype_good2.tey");
        REQUIRE_NOTHROW( phase.run(&tg, &conf, nullptr) );
    }

    SECTION("Valid 3") {
        setup_tey(conf, tg, "edgetype_good3.tey");
        REQUIRE_NOTHROW( phase.run(&tg, &conf, nullptr) );
    }

    SECTION("Valid 4") {
        setup_tey(conf, tg, "edgetype_good4.tey");
        REQUIRE_NOTHROW( phase.run(&tg, &conf, nullptr) );
    }

    SECTION("Valid 5") {
        setup_tey(conf, tg, "edgetype_good5.tey");
        REQUIRE_NOTHROW( phase.run(&tg, &conf, nullptr) );
    }

    SECTION("Invalid 1") {
        setup_tey(conf, tg, "edgetype_bad1.tey");
        REQUIRE_THROWS( phase.run(&tg, &conf, nullptr) );
    }

    SECTION("Invalid 2") {
        setup_tey(conf, tg, "edgetype_bad2.tey");
        REQUIRE_THROWS( phase.run(&tg, &conf, nullptr) );
    }

    SECTION("Invalid 3") {
        setup_tey(conf, tg, "edgetype_bad3.tey");
        REQUIRE_THROWS( phase.run(&tg, &conf, nullptr) );
    }

    SECTION("Invalid 4") {
        setup_tey(conf, tg, "edgetype_bad4.tey");
        REQUIRE_THROWS( phase.run(&tg, &conf, nullptr) );
    }

    SECTION("Invalid 5") {
        setup_tey(conf, tg, "edgetype_bad5.tey");
        REQUIRE_THROWS( phase.run(&tg, &conf, nullptr) );
    }
}

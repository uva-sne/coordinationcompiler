/*
 * Copyright (C) 2020 Stephen Nicholas Swatman <s.n.swatman@uva.nl> 
 *                    Univeristy of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include "config.hpp"
#include "Utils.hpp"
#include "CompilerPass.hpp"
#include "InputParser/InputParser.hpp"
#include "SystemModel.hpp"
#include "Schedule.hpp"
#include "Analyse/Analyse.hpp"
#include "Analyse/QVector.hpp"

#include "test_utils.hpp"

#include <csignal>
#include <stdexcept>
#include "catch.hpp"
#include <iostream>
#include <string>
#include <boost/algorithm/cxx11/any_of.hpp>


TEST_CASE("QVector determination", "[transform][qvector]") {
    config_t conf;
    SystemModel tg;
    Schedule result(&tg, &conf);

    QVector qv;
    setup_trivial(conf, tg);

    SECTION("Simple") {
        setup_tey(conf, tg, "qvector1.tey");

        Task * n3 = tg.task("node_3");
        Task * n4 = tg.task("node_4");
        Task * n5 = tg.task("node_5");

        REQUIRE( n3 != nullptr );
        REQUIRE( n4 != nullptr );
        REQUIRE( n5 != nullptr );

        CHECK( n3->repeat() == 0 );
        CHECK( n4->repeat() == 0 );
        CHECK( n5->repeat() == 0 );

        REQUIRE_NOTHROW( qv.run(&tg, &conf, &result) );

        CHECK( n3->repeat() == 1 );
        CHECK( n4->repeat() == 1 );
        CHECK( n5->repeat() == 1 );
    }

    SECTION("Complex 1") {
        setup_tey(conf, tg, "qvector2.tey");

        Task * n3 = tg.task("node_3");
        Task * n4 = tg.task("node_4");
        Task * n5 = tg.task("node_5");

        REQUIRE( n3 != nullptr );
        REQUIRE( n4 != nullptr );
        REQUIRE( n5 != nullptr );

        CHECK( n3->repeat() == 0 );
        CHECK( n4->repeat() == 0 );
        CHECK( n5->repeat() == 0 );

        REQUIRE_NOTHROW( qv.run(&tg, &conf, &result) );

        CHECK( n3->repeat() == 2 );
        CHECK( n4->repeat() == 1 );
        CHECK( n5->repeat() == 2 );
    }

    SECTION("Complex 2") {
        setup_tey(conf, tg, "qvector3.tey");

        Task * n4 = tg.task("node_4");
        Task * n5 = tg.task("node_5");

        REQUIRE( n4 != nullptr );
        REQUIRE( n5 != nullptr );

        CHECK( n4->repeat() == 0 );
        CHECK( n5->repeat() == 0 );

        REQUIRE_NOTHROW( qv.run(&tg, &conf, &result) );

        CHECK( n4->repeat() == 1 );
        CHECK( n5->repeat() == 2 );
    }

    SECTION("Complex 3") {
        setup_tey(conf, tg, "qvector4.tey");

        Task * n4 = tg.task("node_4");
        Task * n5 = tg.task("node_5");

        REQUIRE( n4 != nullptr );
        REQUIRE( n5 != nullptr );

        CHECK( n4->repeat() == 0 );
        CHECK( n5->repeat() == 0 );

        REQUIRE_NOTHROW( qv.run(&tg, &conf, &result) );

        CHECK( n4->repeat() == 2 );
        CHECK( n5->repeat() == 1 );
    }

    SECTION("Complex 4") {
        setup_tey(conf, tg, "qvector5.tey");

        Task * n4 = tg.task("node_4");
        Task * n5 = tg.task("node_5");

        REQUIRE( n4 != nullptr );
        REQUIRE( n5 != nullptr );

        CHECK( n4->repeat() == 0 );
        CHECK( n5->repeat() == 0 );

        REQUIRE_NOTHROW( qv.run(&tg, &conf, &result) );

        CHECK( n4->repeat() == 7 );
        CHECK( n5->repeat() == 31 );
    }

    SECTION("Complex 5") {
        setup_tey(conf, tg, "qvector6.tey");

        Task * n4 = tg.task("node_4");
        Task * n5 = tg.task("node_5");

        REQUIRE( n4 != nullptr );
        REQUIRE( n5 != nullptr );

        CHECK( n4->repeat() == 0 );
        CHECK( n5->repeat() == 0 );

        REQUIRE_NOTHROW( qv.run(&tg, &conf, &result) );

        CHECK( n4->repeat() == 2 );
        CHECK( n5->repeat() == 9 );
    }

    SECTION("Complex 6") {
        setup_tey(conf, tg, "qvector7.tey");

        Task * n4 = tg.task("node_4");
        Task * n5 = tg.task("node_5");

        REQUIRE( n4 != nullptr );
        REQUIRE( n5 != nullptr );

        CHECK( n4->repeat() == 0 );
        CHECK( n5->repeat() == 0 );

        REQUIRE_NOTHROW( qv.run(&tg, &conf, &result) );

        CHECK( n4->repeat() == 5 );
        CHECK( n5->repeat() == 2 );
    }
    
    SECTION("Complex 7") {
        setup_tey(conf, tg, "qvectorflatten1.tey");

        Task * g1 = tg.task("gen_1");
        Task * g2 = tg.task("gen_2");
        Task * g3 = tg.task("gen_3");
        Task * add = tg.task("add");
        Task * mul = tg.task("mul");
        Task * p = tg.task("print");

        REQUIRE( g1 != nullptr );
        REQUIRE( g2 != nullptr );
        REQUIRE( g3 != nullptr );
        REQUIRE( add != nullptr );
        REQUIRE( mul != nullptr );
        REQUIRE( p != nullptr );

        REQUIRE_NOTHROW( qv.run(&tg, &conf, &result) );

        CHECK( g1->repeat() == 2 );
        CHECK( g2->repeat() == 2 );
        CHECK( g3->repeat() == 1 );
        CHECK( add->repeat() == 2 );
        CHECK( mul->repeat() == 1 );
        CHECK( p->repeat() == 1 );
    }

    SECTION("Invalid 1") {
        setup_tey(conf, tg, "qvector_invalid1.tey");

        REQUIRE_THROWS( qv.run(&tg, &conf, &result) );
    }
}

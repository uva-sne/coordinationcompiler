/*
 * Copyright (C) 2020 Stephen Nicholas Swatman <s.n.swatman@uva.nl> 
 *                    Univeristy of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include "SystemModel.hpp"
#include "Analyse/CheckTiming.hpp"
#include "Transform/PropagateProperties.hpp"

#include "test_utils.hpp"
#include "catch.hpp"

TEST_CASE("Timing check", "[analysis][check_timing]") {
    config_t conf;
    SystemModel tg;

    setup_trivial(conf, tg);

    auto *pp = conf.passes.add_pass(new PropagateProperties);
    auto *phase = conf.passes.add_pass(new CheckTiming);

    SECTION("Valid 1") {
        setup_tey(conf, tg, "checktiming_good1.tey");
        pp->run(&tg, &conf, (Schedule*) nullptr);
        REQUIRE_NOTHROW( phase->run(&tg, &conf, (Schedule*) nullptr) );
    }
}

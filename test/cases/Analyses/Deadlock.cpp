/*
 * Copyright (C) 2020 Stephen Nicholas Swatman <s.n.swatman@uva.nl> 
 *                    Univeristy of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include "SystemModel.hpp"
#include "Analyse/Deadlock.hpp"
#include "Analyse/QVector.hpp"
#include "Transform/QVectorFlatten.hpp"

#include "test_utils.hpp"
#include "catch.hpp"
#include "Analyse/CycleCheck.hpp"

TEST_CASE("Deadlock check", "[analysis][deadlock]") {
    config_t conf;
    SystemModel tg;

    setup_trivial(conf, tg);

    auto *qv = conf.passes.add_pass(new QVector);
    auto *qvf = conf.passes.add_pass(new QVectorFlatten);
    auto *phase = conf.passes.add_pass(new DeadlockCheck);
    
    SECTION("Valid 1") {
        setup_tey(conf, tg, "deadlock_good1.tey");
        qv->run(&tg, &conf);
        qvf->run(&tg, &conf);
        REQUIRE_NOTHROW( phase->run(&tg, &conf) );
    }

    SECTION("Valid 2") {
        setup_tey(conf, tg, "deadlock_good2.tey");
        qv->run(&tg, &conf);
        qvf->run(&tg, &conf);
        REQUIRE_NOTHROW( phase->run(&tg, &conf) );
    }

    SECTION("Valid 3") {
        setup_tey(conf, tg, "deadlock_good3.tey");
        qv->run(&tg, &conf);
        qvf->run(&tg, &conf);
        REQUIRE_NOTHROW( phase->run(&tg, &conf) );
    }

    SECTION("Valid 4") {
        setup_tey(conf, tg, "deadlock_good4.tey");
        qv->run(&tg, &conf);
        qvf->run(&tg, &conf);
        REQUIRE_NOTHROW( phase->run(&tg, &conf) );
    }

    SECTION("Valid 5") {
        setup_tey(conf, tg, "deadlock_good5.tey");
        qv->run(&tg, &conf);
        qvf->run(&tg, &conf);
        REQUIRE_NOTHROW( phase->run(&tg, &conf) );
    }

    SECTION("Invalid 1") {
        setup_tey(conf, tg, "deadlock_bad1.tey");
        REQUIRE_THROWS( phase->run(&tg, &conf) );
    }

    SECTION("Invalid 2") {
        setup_tey(conf, tg, "deadlock_bad2.tey");
        REQUIRE_THROWS( phase->run(&tg, &conf) );
    }

    SECTION("Invalid 3") {
        setup_tey(conf, tg, "deadlock_bad3.tey");
        REQUIRE_THROWS( phase->run(&tg, &conf) );
    }

    SECTION("Invalid 4") {
        setup_tey(conf, tg, "deadlock_bad4.tey");
        REQUIRE_THROWS( phase->run(&tg, &conf) );
    }

    SECTION("Invalid 5") {
        setup_tey(conf, tg, "deadlock_bad5.tey");
        REQUIRE_THROWS( phase->run(&tg, &conf) );
    }
    
    SECTION("Invalid 6") {
        setup_tey(conf, tg, "deadlock_bad6.tey");
        REQUIRE_THROWS( phase->run(&tg, &conf) );
    }
}

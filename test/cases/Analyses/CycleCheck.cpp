/*
 * Copyright (C) 2020 Stephen Nicholas Swatman <s.n.swatman@uva.nl> 
 *                    Univeristy of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include "SystemModel.hpp"
#include "Transform/Transform.hpp"
#include "Analyse/CycleCheck.hpp"

#include "test_utils.hpp"
#include "catch.hpp"

TEST_CASE("Cycle check", "[analysis][cycle_check]") {
    config_t conf;
    SystemModel tg;

    setup_trivial(conf, tg);

    CycleCheck phase;
    phase.forward_params(std::map<std::string, std::string>({{"throwerror", "true"}}));

    SECTION("No cycle") {
        setup_tey(conf, tg, "cycle_nocycle.tey");

        REQUIRE_NOTHROW( phase.run(&tg, &conf, nullptr) );
    }

    SECTION("Direct cycle") {
        setup_tey(conf, tg, "cycle1.tey");

        REQUIRE_THROWS( phase.run(&tg, &conf, nullptr) );
    }

    SECTION("Indirect cycle") {
        setup_tey(conf, tg, "cycle2.tey");

        REQUIRE_THROWS( phase.run(&tg, &conf, nullptr) );
    }
}

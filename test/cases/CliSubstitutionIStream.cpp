/*
 * Copyright (C) 2020 Lukas Miedema <l.miedema@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <boost/iostreams/filtering_stream.hpp>
#include "InputParser/CliSubstitutionIStream.hpp"
#include "config.hpp"
#include "SystemModel.hpp"

#include "catch.hpp"

namespace io = boost::iostreams;

std::string substitute(const std::string &templ, CommandLineProperties *properties) {
    std::istringstream input(templ);
    io::filtering_istream filtered_stream;
    filtered_stream.push(CliSubstitutionIStream(properties));
    filtered_stream.push(input);
    return std::string {std::istreambuf_iterator<char>(filtered_stream), std::istreambuf_iterator<char>() };
}

TEST_CASE("Command substitution") {
    config_t conf;
    SystemModel tg;
    CommandLineProperties properties;
    properties.insert({"prop1", "replaced!"});
    properties.insert({"prop2", "property2"});

    SECTION("No variable") {
        std::string in = "abcdefg";
        std::string out = substitute(in, &properties);
        REQUIRE(in == out);
    }

    
    SECTION("Middle") {
        std::string in = "abc${prop1}defg";
        std::string out = substitute(in, &properties);
        REQUIRE("abcreplaced!defg" == out);
    }
    
    SECTION("Start") {
        std::string in = "${prop1}defg";
        std::string out = substitute(in, &properties);
        REQUIRE("replaced!defg" == out);
    }
    
    SECTION("End") {
        std::string in = "abc${prop1}";
        std::string out = substitute(in, &properties);
        REQUIRE("abcreplaced!" == out);
    }
    
    SECTION("Only") {
        std::string in = "${prop1}";
        std::string out = substitute(in, &properties);
        REQUIRE("replaced!" == out);
    }

    SECTION("Multiple") {
        std::string in = "Property 1 has been ${prop1}, just like ${prop2}";
        std::string out = substitute(in, &properties);
        REQUIRE("Property 1 has been replaced!, just like property2" == out);
    }

    SECTION("Escape property") {
        std::string in = "\\${prop1}";
        std::string out = substitute(in, &properties);
        REQUIRE("${prop1}" == out);
    }

    SECTION("Escape stray") {
        std::string in = "\\$5 please";
        std::string out = substitute(in, &properties);
        REQUIRE("$5 please" == out);
    }

    SECTION("Missing variable") {
        std::string in = "${missing} and then some";
        REQUIRE_THROWS(substitute(in, &properties));
    }
}
/*
 * Copyright (C) 2020 Stephen Nicholas Swatman <s.n.swatman@uva.nl>, Univeristy of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include "config.hpp"
#include "Utils.hpp"
#include "CompilerPass.hpp"
#include "InputParser/InputParser.hpp"
#include "SystemModel.hpp"
#include "Schedule.hpp"

#include "test_utils.hpp"

#include <csignal>
#include "catch.hpp"
#include <string>

using Catch::Matchers::Contains;


TEST_CASE("NFP parser all columns", "[nfp][input][parser]") {
    config_t conf;
    SystemModel tg;

    setup_trivial(conf, tg);
    setup_tey(conf, tg, "many.tey");
    setup_nfp(conf, tg, "test.nfp");

    Task *t1 = tg.task("p1");
    Task *t2 = tg.task("p2");
    Task *t3 = tg.task("p3");
    Task *t4 = tg.task("p4");

    REQUIRE(t1 != nullptr);
    REQUIRE(t2 != nullptr);
    REQUIRE(t3 != nullptr);
    REQUIRE(t4 != nullptr);

    Version *v10 = t1->version("");
    Version *v20 = t2->version("");
    Version *v31 = t3->version("w");
    Version *v32 = t3->version("v");
    Version *v40 = t4->version("");

    REQUIRE(v10 != nullptr);
    REQUIRE(v20 != nullptr);
    REQUIRE(v31 != nullptr);
    REQUIRE(v32 != nullptr);
    REQUIRE(v40 != nullptr);


    CHECK(v10->C() == 12 * 1000000000l);
    CHECK(v10->C_E() == 15);
    CHECK(v10->cname() == "h");
    CHECK(v10->csignature() == "q");
    CHECK(v10->cfile() == "c.c");
    CHECK(v10->cbinary() == "g.so");
}

TEST_CASE("NFP parser reduced columns", "[nfp][input][parser][reduced_columns]") {
    config_t conf;
    SystemModel tg;

    setup_trivial(conf, tg);
    setup_tey(conf, tg, "many.tey");
    setup_nfp(conf, tg, "reduced_test.nfp");

    Task *t1 = tg.task("p1");
    Task *t2 = tg.task("p2");
    Task *t3 = tg.task("p3");
    Task *t4 = tg.task("p4");

    REQUIRE(t1 != nullptr);
    REQUIRE(t2 != nullptr);
    REQUIRE(t3 != nullptr);
    REQUIRE(t4 != nullptr);

    Version *v10 = t1->version("");
    Version *v20 = t2->version("");
    Version *v31 = t3->version("w");
    Version *v32 = t3->version("v");
    Version *v40 = t4->version("");

    REQUIRE(v10 != nullptr);
    REQUIRE(v20 != nullptr);
    REQUIRE(v31 != nullptr);
    REQUIRE(v32 != nullptr);
    REQUIRE(v40 != nullptr);


    CHECK(v10->C() == 12 * 1000000000l);
    CHECK(v10->C_E() == 0);
    CHECK(v10->cname() == "h");
    CHECK(v10->csignature().empty());
    CHECK(v10->cfile().empty());
    CHECK(v10->cbinary().empty());
}

TEST_CASE("NFP parser invalid", "[nfp][input][parser][invalid]") {
    config_t conf;
    SystemModel tg;
    setup_trivial(conf, tg);
    setup_tey(conf, tg, "many.tey");

    SECTION("duplicate column") {
        REQUIRE_THROWS_WITH(setup_nfp(conf, tg, "duplicate_column.nfp"),
                Contains("Duplicate column definition 'Component Name'"));
    }

    SECTION("unknown column") {
        REQUIRE_NOTHROW(setup_nfp(conf, tg, "unknown_column.nfp"));
    }

    SECTION("duplicate arch definition") {
        REQUIRE_THROWS_WITH(setup_nfp(conf, tg, "duplicate_arch.nfp"),
                Contains("Specifying multiple architectures for the same task version p3/w is not allowed"));
    }
}

TEST_CASE("Energy-aware NFP", "[nfp][input][parser][energy]"){
    config_t conf;
    SystemModel tg;
    setup_config(conf, tg, "eFLS.xml");
    setup_tey(conf, tg, "energy_DAG.tey");
    setup_nfp(conf, tg, "energy_nfp.nfp");

    Task *t1 = tg.task("t0_0");
    Task *t2 = tg.task("t0_41");

    Version *v10 = t1->version("t0_0_5_1900000");
    Version *v11 = t1->version("t0_0_1_1400000");
    Version *v20 = t2->version("t0_41_1_1400000_600000000");

    // make sure additional energy columns were parsed
    REQUIRE(v10->gpu_required() == FALSE);
    REQUIRE(v10->frequency() == 1900);
    REQUIRE(v10->C() == 1);
    REQUIRE(v10->phases().size() == 1);
    REQUIRE(v10->C_E() == 1);

    REQUIRE(v11->frequency() == 1400);

    REQUIRE(v20->gpu_required());
    REQUIRE(v20->gpu_frequency() == 600);
    REQUIRE(v20->C() == 519);
    REQUIRE(v20->C_E() == 15335);

    // make sure processors are done correctly
    for (auto CU: tg.processors()){
        if (CU->type == "armv7big") REQUIRE(v10->canRunOn(CU));
        else REQUIRE_FALSE(v10->canRunOn(CU));

        if (CU->type == "armv7little") REQUIRE(v11->canRunOn(CU));
        else REQUIRE_FALSE(v11->canRunOn(CU));

        if (CU->type == "armv7little") REQUIRE(v20->canRunOn(CU));
        else REQUIRE_FALSE(v20->canRunOn(CU));
    }
}
/* 
 * Copyright (C) 2020 Stephen Nicholas Swatman <s.n.swatman@uva.nl> 
 *                    Univeristy of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 


#include "Utils.hpp"
#include "Utils/Linalg.hpp"

#include <boost/numeric/ublas/matrix.hpp>

#include <csignal>
#include <stdexcept>
#include "catch.hpp"

TEST_CASE("Size string to integer produces correct values", "[utils]") {
    REQUIRE( Utils::stringmem_to_bit("0") == 0 );
    REQUIRE( Utils::stringmem_to_bit("0kb") == 0 );
    REQUIRE( Utils::stringmem_to_bit("0mb") == 0 );
    REQUIRE( Utils::stringmem_to_bit("0gb") == 0 );
    REQUIRE( Utils::stringmem_to_bit("1kb") == 1024 );
    REQUIRE( Utils::stringmem_to_bit("5kb") == 5120 );
    REQUIRE( Utils::stringmem_to_bit("1mb") == 1048576 );
    REQUIRE( Utils::stringmem_to_bit("1mB") == 8388608 );
    REQUIRE( Utils::stringmem_to_bit("255mb") == 267386880 );
    REQUIRE( Utils::stringmem_to_bit("255mB") == 2139095040 );
    REQUIRE( Utils::stringmem_to_bit("8181mb") == 8578400256 );
    REQUIRE( Utils::stringmem_to_bit("1gb") == 1073741824 );
    REQUIRE( Utils::stringmem_to_bit("123gb") == 132070244352 );
}

TEST_CASE("128-bit integer converts to string", "[utils]") {
    uint128_t q1 = 184467440737095516;
    uint128_t q2 = 184467440737095516;
    uint128_t q3 = 123;
    uint128_t q4 = 0;
    
    q1 *= 1000000000;
    q2 *= 1000000001;
    
    REQUIRE( to_string(q1) == "184467440737095516000000000" );
    REQUIRE( to_string(q2) == "184467440921562956737095516" );
    REQUIRE( to_string(q3) == "123" );
    REQUIRE( to_string(q4) == "0" );
}

TEST_CASE("String to processor class works", "[utils]") {
    REQUIRE( ArchitectureProperties::from_string("cpu") == ArchitectureProperties::ComputeUnitType::CUT_CPU );
    REQUIRE( ArchitectureProperties::from_string("gpu") == ArchitectureProperties::ComputeUnitType::CUT_GPU );
    REQUIRE( ArchitectureProperties::from_string("fpga") == ArchitectureProperties::ComputeUnitType::CUT_FPGA );

    // TPUs are evil! >:)
    REQUIRE( ArchitectureProperties::from_string("tpu") == ArchitectureProperties::ComputeUnitType::CUT_UNDEFINED );
    REQUIRE( ArchitectureProperties::from_string("hello world") == ArchitectureProperties::ComputeUnitType::CUT_UNDEFINED);
}

TEST_CASE("Time string to nanoseconds works", "[utils]") {
    REQUIRE( Utils::stringtime_to_ns("1Hz") == Utils::stringtime_to_ns("1 Hz") );
    
    REQUIRE( Utils::stringtime_to_ns("1Hz") == 1000000000 );
    REQUIRE( Utils::stringtime_to_ns("20Hz") == 50000000 );
    REQUIRE( Utils::stringtime_to_ns("1000000000Hz") == 1 );
    
    REQUIRE( Utils::stringtime_to_ns("1 cycles") == 1 );
    REQUIRE( Utils::stringtime_to_ns("2 cycles") == 2 );
    REQUIRE( Utils::stringtime_to_ns("100 cycles") == 100 );
    
    // Not a surpsising result.
    REQUIRE( Utils::stringtime_to_ns("1 ns") == 1 );
    REQUIRE( Utils::stringtime_to_ns("2 ns") == 2 );
    REQUIRE( Utils::stringtime_to_ns("100 ns") == 100 );
    
    REQUIRE( Utils::stringtime_to_ns("1 us") == 1000 );
    REQUIRE( Utils::stringtime_to_ns("2 us") == 2000 );
    REQUIRE( Utils::stringtime_to_ns("100 us") == 100000 );
    
    REQUIRE( Utils::stringtime_to_ns("1 ms") == 1000000 );
    REQUIRE( Utils::stringtime_to_ns("2 ms") == 2000000 );
    REQUIRE( Utils::stringtime_to_ns("100 ms") == 100000000 );
    
    REQUIRE( Utils::stringtime_to_ns("1 s") == 1000000000 );
    REQUIRE( Utils::stringtime_to_ns("2 s") == 2000000000 );
    REQUIRE( Utils::stringtime_to_ns("100 s") == 100000000000 );
    
    REQUIRE( Utils::stringtime_to_ns("17.5 h") == 63000000000000 );
    
    REQUIRE( Utils::stringtime_to_ns("60 m") == Utils::stringtime_to_ns("1 h") );
    REQUIRE( Utils::stringtime_to_ns("360 m") == Utils::stringtime_to_ns("6 h") );
}

TEST_CASE("Energy string to joules works", "[utils]") {
    //TODO fail
//    REQUIRE( Utils::stringenergy_to_joule("1J") == Approx(1.0) );
//    REQUIRE( Utils::stringenergy_to_joule("3.14J") == Approx(3.14) );
//    REQUIRE( Utils::stringenergy_to_joule("512J") == Approx(512.0) );
//    REQUIRE( Utils::stringenergy_to_joule("0J") == Approx(0.0) );
//
//    REQUIRE( Utils::stringenergy_to_joule("1Wh") == Approx(3600.0) );
//    REQUIRE( Utils::stringenergy_to_joule("3.14Wh") == Approx(11304.0) );
//    REQUIRE( Utils::stringenergy_to_joule("512Wh") == Approx(1843200.0) );
//    REQUIRE( Utils::stringenergy_to_joule("0Wh") == Approx(0.0) );
//
//    REQUIRE( Utils::stringenergy_to_joule("1kWh") == Approx(3600000.0) );
//    REQUIRE( Utils::stringenergy_to_joule("3.14kWh") == Approx(11304000.0) );
//    REQUIRE( Utils::stringenergy_to_joule("512kWh") == Approx(1843200000.0) );
//    REQUIRE( Utils::stringenergy_to_joule("0kWh") == Approx(0.0) );
}

TEST_CASE("Energy string to milli joules works", "[utils]") {
//    REQUIRE( Utils::stringenergy_to_mjoule("1J") == Approx(1000.0) );
//    REQUIRE( Utils::stringenergy_to_mjoule("3.14J") == Approx(3140.0) );
//    REQUIRE( Utils::stringenergy_to_mjoule("512J") == Approx(512000.0) );
//    REQUIRE( Utils::stringenergy_to_mjoule("0J") == Approx(0.0) );
//
//    REQUIRE( Utils::stringenergy_to_mjoule("1Wh") == Approx(3600000.0) );
//    REQUIRE( Utils::stringenergy_to_mjoule("3.14Wh") == Approx(11304000.0) );
//    REQUIRE( Utils::stringenergy_to_mjoule("512Wh") == Approx(1843200000.0) );
//    REQUIRE( Utils::stringenergy_to_mjoule("0Wh") == Approx(0.0) );
//
//    REQUIRE( Utils::stringenergy_to_mjoule("1kWh") == Approx(3600000000.0) );
//    REQUIRE( Utils::stringenergy_to_mjoule("3.14kWh") == Approx(11304000000.0) );
//    REQUIRE( Utils::stringenergy_to_mjoule("512kWh") == Approx(1843200000000.0) );
//    REQUIRE( Utils::stringenergy_to_mjoule("0kWh") == Approx(0.0) );
}

TEST_CASE("Joules to energy string works", "[utils]") {
//    REQUIRE( std::stof(Utils::njoule_to_stringenergy(static_cast<energycons_t>(0.0), "J")) == Approx(0.0) );
//    REQUIRE( std::stof(Utils::njoule_to_stringenergy(static_cast<energycons_t>(3.14), "J")) == Approx(3.14) );
//
//    REQUIRE( std::stof(Utils::njoule_to_stringenergy(static_cast<energycons_t>(0.0), "Wh")) == Approx(0.0) );
//    REQUIRE( std::stof(Utils::njoule_to_stringenergy(static_cast<energycons_t>(3140.0), "Wh")) == Approx(0.87222) );
//
//    REQUIRE( std::stof(Utils::njoule_to_stringenergy(static_cast<energycons_t>(0.0), "kWh")) == Approx(0.0) );
//    REQUIRE( std::stof(Utils::njoule_to_stringenergy(static_cast<energycons_t>(3140000.0), "kWh")) == Approx(0.87222) );
}

TEST_CASE("Nanoseconds to string works", "[utils]") {
//    REQUIRE( std::stof(Utils::nstime_to_string(1, "Hz")) == Approx(1000000000) );
//    REQUIRE( std::stof(Utils::nstime_to_string(10, "Hz")) == Approx(100000000) );
//    REQUIRE( std::stof(Utils::nstime_to_string(100000, "Hz")) == Approx(10000) );
//
//    REQUIRE( std::stof(Utils::nstime_to_string(1, "cycles")) == Approx(1.0) );
//    REQUIRE( std::stof(Utils::nstime_to_string(12345, "cycles")) == Approx(12345.0) );
//
//    REQUIRE( std::stof(Utils::nstime_to_string(1, "ns")) == Approx(1.0) );
//    REQUIRE( std::stof(Utils::nstime_to_string(12345, "ns")) == Approx(12345.0) );
//
//    REQUIRE( std::stof(Utils::nstime_to_string(1, "us")) == Approx(0.001) );
//    REQUIRE( std::stof(Utils::nstime_to_string(1000, "us")) == Approx(1.0) );
//    REQUIRE( std::stof(Utils::nstime_to_string(12345, "us")) == Approx(12.345) );
//
//    REQUIRE( std::stof(Utils::nstime_to_string(1, "ms")) == Approx(0.0) );
//    REQUIRE( std::stof(Utils::nstime_to_string(1000000, "ms")) == Approx(1.0) );
//    REQUIRE( std::stof(Utils::nstime_to_string(12345, "ms")) == Approx(0.012) );
//
//    REQUIRE( std::stof(Utils::nstime_to_string(60000000000, "m")) == Approx(1.0) );
//    REQUIRE( std::stof(Utils::nstime_to_string(480000000000, "m")) == Approx(8.0) );
//
//    REQUIRE( std::stof(Utils::nstime_to_string(3600000000000, "h")) == Approx(1.0) );
//    REQUIRE( std::stof(Utils::nstime_to_string(28800000000000, "h")) == Approx(8.0) );
}

TEST_CASE("Matrix rank calculation", "[utils]") {
    boost::numeric::ublas::matrix<int> test_1(3, 3);
    test_1(0, 0) = 4;
    test_1(0, 1) = -1;
    test_1(0, 2) = 0;
    test_1(1, 0) = 2;
    test_1(1, 1) = 1;
    test_1(1, 2) = 5;
    test_1(2, 0) = -2;
    test_1(2, 1) = -2;
    test_1(2, 2) = 0;
    CHECK( Utils::matrix_rank(test_1) == 3 );

    boost::numeric::ublas::matrix<int> test_2(3, 3);
    test_2(0, 0) = 4;
    test_2(0, 1) = -1;
    test_2(0, 2) = 1;
    test_2(1, 0) = 2;
    test_2(1, 1) = -3;
    test_2(1, 2) = 1;
    test_2(2, 0) = -2;
    test_2(2, 1) = -2;
    test_2(2, 2) = 0;
    CHECK( Utils::matrix_rank(test_2) == 2 );

    boost::numeric::ublas::matrix<int> test_3(3, 3);
    test_3(0, 0) = -2;
    test_3(0, 1) = -2;
    test_3(0, 2) = 0;
    test_3(1, 0) = 4;
    test_3(1, 1) = -1;
    test_3(1, 2) = 1;
    test_3(2, 0) = 2;
    test_3(2, 1) = -3;
    test_3(2, 2) = 1;
    CHECK( Utils::matrix_rank(test_3) == 2 );

    boost::numeric::ublas::matrix<int> test_4(3, 3);
    test_4(0, 0) = 1;
    test_4(0, 1) = 3;
    test_4(0, 2) = 6;
    test_4(1, 0) = 1;
    test_4(1, 1) = 1;
    test_4(1, 2) = 1;
    test_4(2, 0) = 1;
    test_4(2, 1) = 3;
    test_4(2, 2) = 3;
    CHECK( Utils::matrix_rank(test_4) == 3 );

    boost::numeric::ublas::matrix<int> test_5(12, 11);
    test_5(0, 0) = 0;
    test_5(0, 1) = -3;
    test_5(0, 2) = 0;
    test_5(0, 3) = 0;
    test_5(0, 4) = 0;
    test_5(0, 5) = 0;
    test_5(0, 6) = 0;
    test_5(0, 7) = 0;
    test_5(0, 8) = -2;
    test_5(0, 9) = 0;
    test_5(0, 10) = 0;
    test_5(1, 0) = -3;
    test_5(1, 1) = 0;
    test_5(1, 2) = 0;
    test_5(1, 3) = 0;
    test_5(1, 4) = 0;
    test_5(1, 5) = 0;
    test_5(1, 6) = 0;
    test_5(1, 7) = 0;
    test_5(1, 8) = 0;
    test_5(1, 9) = 0;
    test_5(1, 10) = 0;
    test_5(2, 0) = 4;
    test_5(2, 1) = 0;
    test_5(2, 2) = -4;
    test_5(2, 3) = -4;
    test_5(2, 4) = 0;
    test_5(2, 5) = 0;
    test_5(2, 6) = -2;
    test_5(2, 7) = 0;
    test_5(2, 8) = 0;
    test_5(2, 9) = 0;
    test_5(2, 10) = 0;
    test_5(3, 0) = 0;
    test_5(3, 1) = 1;
    test_5(3, 2) = 2;
    test_5(3, 3) = 0;
    test_5(3, 4) = 0;
    test_5(3, 5) = 0;
    test_5(3, 6) = 0;
    test_5(3, 7) = 0;
    test_5(3, 8) = 0;
    test_5(3, 9) = 0;
    test_5(3, 10) = -2;
    test_5(4, 0) = 0;
    test_5(4, 1) = 0;
    test_5(4, 2) = 0;
    test_5(4, 3) = 0;
    test_5(4, 4) = -1;
    test_5(4, 5) = 0;
    test_5(4, 6) = 0;
    test_5(4, 7) = 0;
    test_5(4, 8) = 0;
    test_5(4, 9) = 0;
    test_5(4, 10) = 0;
    test_5(5, 0) = 0;
    test_5(5, 1) = 0;
    test_5(5, 2) = 0;
    test_5(5, 3) = 1;
    test_5(5, 4) = 1;
    test_5(5, 5) = -3;
    test_5(5, 6) = 0;
    test_5(5, 7) = 0;
    test_5(5, 8) = 0;
    test_5(5, 9) = 0;
    test_5(5, 10) = 0;
    test_5(6, 0) = 0;
    test_5(6, 1) = 0;
    test_5(6, 2) = 0;
    test_5(6, 3) = 0;
    test_5(6, 4) = 0;
    test_5(6, 5) = 1;
    test_5(6, 6) = 0;
    test_5(6, 7) = 0;
    test_5(6, 8) = 0;
    test_5(6, 9) = 0;
    test_5(6, 10) = 0;
    test_5(7, 0) = 0;
    test_5(7, 1) = 0;
    test_5(7, 2) = 0;
    test_5(7, 3) = 0;
    test_5(7, 4) = 0;
    test_5(7, 5) = 0;
    test_5(7, 6) = 0;
    test_5(7, 7) = -4;
    test_5(7, 8) = 0;
    test_5(7, 9) = 0;
    test_5(7, 10) = 0;
    test_5(8, 0) = 0;
    test_5(8, 1) = 0;
    test_5(8, 2) = 0;
    test_5(8, 3) = 0;
    test_5(8, 4) = 0;
    test_5(8, 5) = 0;
    test_5(8, 6) = 4;
    test_5(8, 7) = 2;
    test_5(8, 8) = 0;
    test_5(8, 9) = 0;
    test_5(8, 10) = 0;
    test_5(9, 0) = 0;
    test_5(9, 1) = 0;
    test_5(9, 2) = 0;
    test_5(9, 3) = 0;
    test_5(9, 4) = 0;
    test_5(9, 5) = 0;
    test_5(9, 6) = 0;
    test_5(9, 7) = 0;
    test_5(9, 8) = 1;
    test_5(9, 9) = 3;
    test_5(9, 10) = 0;
    test_5(10, 0) = 0;
    test_5(10, 1) = 0;
    test_5(10, 2) = 0;
    test_5(10, 3) = 0;
    test_5(10, 4) = 0;
    test_5(10, 5) = 0;
    test_5(10, 6) = 0;
    test_5(10, 7) = 0;
    test_5(10, 8) = 0;
    test_5(10, 9) = -3;
    test_5(10, 10) = 3;
    test_5(11, 0) = -3;
    test_5(11, 1) = 0;
    test_5(11, 2) = 0;
    test_5(11, 3) = 0;
    test_5(11, 4) = 0;
    test_5(11, 5) = 0;
    test_5(11, 6) = 0;
    test_5(11, 7) = -2;
    test_5(11, 8) = 0;
    test_5(11, 9) = 0;
    test_5(11, 10) = 0;
    CHECK( Utils::matrix_rank(test_5) == 10 );

    boost::numeric::ublas::matrix<int> test_6(7, 3);
    test_6(0, 0) = 1;
    test_6(0, 1) = -6;
    test_6(0, 2) = -6;
    test_6(1, 0) = -6;
    test_6(1, 1) = 1;
    test_6(1, 2) = -1;
    test_6(2, 0) = -1;
    test_6(2, 1) = -5;
    test_6(2, 2) = -7;
    test_6(3, 0) = 8;
    test_6(3, 1) = 2;
    test_6(3, 2) = 8;
    test_6(4, 0) = -4;
    test_6(4, 1) = 6;
    test_6(4, 2) = 3;
    test_6(5, 0) = -3;
    test_6(5, 1) = 2;
    test_6(5, 2) = -4;
    test_6(6, 0) = -10;
    test_6(6, 1) = -4;
    test_6(6, 2) = 9;
    CHECK( Utils::matrix_rank(test_6) == 3 );

    boost::numeric::ublas::matrix<int> test_7(4, 3);
    test_7(0, 0) = 0;
    test_7(0, 1) = 0;
    test_7(0, 2) = 0;
    test_7(1, 0) = 0;
    test_7(1, 1) = 0;
    test_7(1, 2) = 1;
    test_7(2, 0) = 1;
    test_7(2, 1) = 1;
    test_7(2, 2) = -1;
    test_7(3, 0) = 1;
    test_7(3, 1) = 1;
    test_7(3, 2) = 0;
    CHECK( Utils::matrix_rank(test_7) == 2 );
}
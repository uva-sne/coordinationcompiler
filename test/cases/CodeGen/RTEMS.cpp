/* 
 * Copyright (C) 2020 Benjamin Rouxel <benjamin.rouxel@uva.nl> 
 *                    Univeristy of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "config.hpp"
#include "SystemModel.hpp"
#include "Utils.hpp"
#include "Schedule.hpp"
#include "CompilerPass.hpp"

#include "test_utils.hpp"
#include "catch.hpp"

TEST_CASE("RTEMS", "[generator][codegen][rtems]") {
    config_t conf;
    SystemModel tg;
    Schedule result(&tg, &conf);
    setup_config(conf, tg, "codegen_rtems.xml");

    SECTION("Ok") {
        conf.files.coord_basename = "simple";
        setup_tey(conf, tg, "simple.tey");

        for(CompilerPassPtr &p : conf.passes.passes()) {
            REQUIRE_NOTHROW( p->run(&tg, &conf, &result) );
        }
    }
}
/* 
 * Copyright (C) 2020 Stephen Nicholas Swatman <s.n.swatman@uva.nl> 
 *                    Univeristy of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 


#include "config.hpp"
#include "SystemModel.hpp"
#include "Utils.hpp"
#include "CompilerPass.hpp"
#include "InputParser/InputParser.hpp"
#include "Solvers/Solver.hpp"
#include "Schedule.hpp"
#include "Generator/Generator.hpp"
#include "Transform/Transform.hpp"
#include "Analyse/Analyse.hpp"

#include "test_utils.hpp"

#include <csignal>
#include <stdexcept>
#include "catch.hpp"
#include <iostream>
#include <string>
#include <boost/algorithm/cxx11/any_of.hpp>
#include <Exporter/Exporter.hpp>

TEST_CASE("XML bare config parser", "[xml][input][parser][config]") {
    config_t conf;
    SystemModel tg;
    setup_config(conf, tg, "test_dot_generation.xml");

    REQUIRE( conf.passes->size() == 1+1 ); //+1 as PropagateProperties is automatically added
}

TEST_CASE("XML config system model parser architecture", "[xml][input][parser][config][system_model]") {
    config_t conf;
    SystemModel tg;
    init_config(&conf);

    SECTION("FM radio config") {
        setup_config(conf, tg, "fmradio.xml");
        
        REQUIRE( tg.processors().size() == 6 );

        REQUIRE( boost::algorithm::any_of(
            tg.processors(), 
            [](ComputeUnit* p) { 
                return (p->id == "P1" && p->type == "armv7" && p->sysID == 0);
            }) 
        );
        REQUIRE( boost::algorithm::any_of(
            tg.processors(), 
            [](ComputeUnit* p) { 
                return (p->id == "P2" && p->type == "armv7" && p->sysID == 1);
            }) 
        );
        REQUIRE( boost::algorithm::any_of(
            tg.processors(), 
            [](ComputeUnit* p) { 
                return (p->id == "P3" && p->type == "armv7" && p->sysID == 2);
            }) 
        );
        REQUIRE( boost::algorithm::any_of(
            tg.processors(), 
            [](ComputeUnit* p) { 
                return (p->id == "P4" && p->type == "armv7" && p->sysID == 3);
            }) 
        );
        REQUIRE( boost::algorithm::any_of(
            tg.processors(), 
            [](ComputeUnit* p) { 
                return (p->id == "P5" && p->type == "armv7" && p->sysID == 4);
            }) 
        );
        REQUIRE( boost::algorithm::any_of(
            tg.processors(), 
            [](ComputeUnit* p) { 
                return (p->id == "P6" && p->type == "armv7" && p->sysID == 5);
            }) 
        );
    };        

    SECTION("Toy example config") {
        setup_config(conf, tg, "toyexample.xml");
        
        REQUIRE( tg.processors().size() == 8 );

        REQUIRE( boost::algorithm::any_of(
            tg.processors(), 
            [](ComputeUnit* p) { 
                return (p->id == "P1" && p->type == "armv7little" && p->sysID == 0);
            }) 
        );
        REQUIRE( boost::algorithm::any_of(
            tg.processors(), 
            [](ComputeUnit* p) { 
                return (p->id == "P2" && p->type == "armv7little" && p->sysID == 1);
            }) 
        );
        REQUIRE( boost::algorithm::any_of(
            tg.processors(), 
            [](ComputeUnit* p) { 
                return (p->id == "P3" && p->type == "armv7little" && p->sysID == 2);
            }) 
        );
        REQUIRE( boost::algorithm::any_of(
            tg.processors(), 
            [](ComputeUnit* p) { 
                return (p->id == "P4" && p->type == "armv7little" && p->sysID == 3);
            }) 
        );
        REQUIRE( boost::algorithm::any_of(
            tg.processors(), 
            [](ComputeUnit* p) { 
                return (p->id == "P5" && p->type == "armv7big" && p->sysID == 4);
            }) 
        );
        REQUIRE( boost::algorithm::any_of(
            tg.processors(), 
            [](ComputeUnit* p) { 
                return (p->id == "P6" && p->type == "armv7big" && p->sysID == 5);
            }) 
        );
        REQUIRE( boost::algorithm::any_of(
            tg.processors(), 
            [](ComputeUnit* p) { 
                return (p->id == "P7" && p->type == "armv7big" && p->sysID == 6);
            }) 
        );
        REQUIRE( boost::algorithm::any_of(
            tg.processors(), 
            [](ComputeUnit* p) { 
                return (p->id == "P8" && p->type == "armv7big" && p->sysID == 7);
            }) 
        );
    };
}

TEST_CASE("XML config parser configuring passes", "[xml][input][parser][config]") {
    config_t conf;
    SystemModel tg;

    SECTION("FM radio config") {
        setup_config(conf, tg, "fmradio.xml");

        REQUIRE( conf.passes->size() == 5+1 ); //+1 as PropagateProperties is automatically added

        REQUIRE( dynamic_cast<Exporter *>(conf.passes->at(1).get()) != nullptr );
        CHECK( conf.passes->at(1)->get_uniqid_rtti() == "generator-dot" );

        REQUIRE( dynamic_cast<Analyse *>(conf.passes->at(2).get()) != nullptr );
        CHECK( conf.passes->at(2)->get_uniqid_rtti() == "analyse-repetition_vector" );

        REQUIRE( dynamic_cast<Transform *>(conf.passes->at(3).get()) != nullptr );
        CHECK( conf.passes->at(3)->get_uniqid_rtti() == "transform-homogenize" );

        REQUIRE( dynamic_cast<Generator *>(conf.passes->at(4).get()) != nullptr );
        CHECK( conf.passes->at(4)->get_uniqid_rtti() == "generator-linux-nobuff-mgnt" );

        REQUIRE( dynamic_cast<Generator *>(conf.passes->at(5).get()) != nullptr );
        CHECK( conf.passes->at(5)->get_uniqid_rtti() == "generator-profiler" );
    };        

    SECTION("Toy example config") {
        setup_config(conf, tg, "toyexample.xml");

        REQUIRE( conf.passes->size() == 2+1 ); //+1 as PropagateProperties is automatically added

        REQUIRE( dynamic_cast<Exporter *>(conf.passes->at(1).get()) != nullptr );
        CHECK( conf.passes->at(1)->get_uniqid_rtti() == "generator-dot" );

        REQUIRE( dynamic_cast<Exporter *>(conf.passes->at(2).get()) != nullptr );
        CHECK( conf.passes->at(2)->get_uniqid_rtti() == "generator-svg" );

    };
}

TEST_CASE("Energy-aware information parser", "[xml][input][parser][config]") {
    config_t conf;
    SystemModel tg;
    setup_config(conf, tg, "eFLS.xml");
    setup_tey(conf, tg, "energy_DAG.tey");
    setup_nfp(conf, tg, "energy_nfp.nfp");

    // make sure the target units are parsed properly
    REQUIRE(tg.energy_unit() == "mJ");
    REQUIRE(tg.time_unit() == "ds");
    REQUIRE(tg.frequency_unit() == "mHz");
    REQUIRE(tg.power_unit() == "mW");

    // ensure base watt is correct
    REQUIRE(tg.base_watt() == 1843);

    // make sure that the GPU is parsed properly
    REQUIRE(tg.coprocessors().size() == 1);
    REQUIRE(tg.coprocessors()[0]->voltage_island_id == "GPU");
    REQUIRE(tg.coprocessors()[0]->proc_class == ArchitectureProperties::CUT_SEPARATE_COPROCESSOR);

    // ensure that voltage islands are there
    REQUIRE(tg.voltage_islands().size() == 3);
    REQUIRE(tg.coprocessors()[0]->target_voltage_island->frequency_watt.size() == 7 );
}
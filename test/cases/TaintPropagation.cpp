/*
 * Copyright (C) 2020 Benjamin Rouxel <benjamin.rouxel@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "SystemModel.hpp"
#include "Schedule.hpp"
#include "InputParser/InputParser.hpp"
#include "Transform/Transform.hpp"
#include "Transform/TransitivClosure.hpp"

#include "test_utils.hpp"

#include "catch.hpp"

#define HAS_SUCCESSOR(a, b) contains(a->successors_transitiv, b)
#define HAS_PREDECESSOR(a, b) contains(a->previous_transitiv, b)

TEST_CASE("Taint Propagation", "[transform][tainting]") {
    config_t conf;

    SECTION("Full") {
        SystemModel tg;
        setup_trivial(conf, tg);
        setup_tey(conf, tg, "tainted1.tey");
        
        
        for(Task *t : tg.tasks_it()) {
            if(t->id() == "first")
                CHECK(t->tainted() == 1);
            else
                CHECK(t->tainted() == 0);
        }
        
        REQUIRE_NOTHROW( TransformRegistry::Create("tainting")->run(&tg, &conf, nullptr) );
        
        CHECK(tg.task("first")->tainted() == 1);
        CHECK(tg.task("add")->tainted() == 2);
        CHECK(tg.task("sub")->tainted() == 2);
        CHECK(tg.task("mult")->tainted() == 2);
        CHECK(tg.task("division")->tainted() == 2);
        CHECK(tg.task("add_double")->tainted() == 3);
        CHECK(tg.task("sub_double")->tainted() == 3);
        CHECK(tg.task("mult_double")->tainted() == 3);
        CHECK(tg.task("division_double")->tainted() == 3);
        CHECK(tg.task("sum_all")->tainted() == 4);
    }
    
    SECTION("Path") {
        SystemModel tg;
        setup_trivial(conf, tg);
        setup_tey(conf, tg, "tainted2.tey");
        
        
        for(Task *t : tg.tasks_it()) {
            if(t->id() == "division")
                CHECK(t->tainted() == 1);
            else
                CHECK(t->tainted() == 0);
        }
        
        REQUIRE_NOTHROW( TransformRegistry::Create("tainting")->run(&tg, &conf, nullptr) );
        
        CHECK(tg.task("first")->tainted() == 0);
        CHECK(tg.task("add")->tainted() == 0);
        CHECK(tg.task("sub")->tainted() == 0);
        CHECK(tg.task("mult")->tainted() == 0);
        CHECK(tg.task("division")->tainted() == 1);
        CHECK(tg.task("add_double")->tainted() == 0);
        CHECK(tg.task("sub_double")->tainted() == 2);
        CHECK(tg.task("mult_double")->tainted() == 0);
        CHECK(tg.task("division_double")->tainted() == 0);
        CHECK(tg.task("sum_all")->tainted() == 3);
    }
    
    SECTION("Path+Connector") {
        SystemModel tg;
        setup_trivial(conf, tg);
        setup_tey(conf, tg, "tainted3.tey");
        
        
        for(Task *t : tg.tasks_it()) {
            if(t->id() == "division")
                CHECK(t->tainted() == 1);
            else
                CHECK(t->tainted() == 0);
        }
        
        REQUIRE_NOTHROW( TransformRegistry::Create("tainting")->run(&tg, &conf, nullptr) );
        
        CHECK(tg.task("first")->tainted() == 0);
        CHECK(tg.task("add")->tainted() == 0);
        CHECK(tg.task("sub")->tainted() == 0);
        CHECK(tg.task("mult")->tainted() == 0);
        CHECK(tg.task("division")->tainted() == 1);
        CHECK(tg.task("add_double")->tainted() == 1);
        CHECK(tg.task("sub_double")->tainted() == 2);
        CHECK(tg.task("mult_double")->tainted() == 0);
        CHECK(tg.task("division_double")->tainted() == 0);
        CHECK(tg.task("sum_all")->tainted() == 1);
    }
}


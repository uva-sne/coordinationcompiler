/*
 * Copyright (C) 2021 brouxel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <boost/iostreams/filtering_stream.hpp>
#include "InputParser/CliSubstitutionIStream.hpp"
#include "config.hpp"
#include "SystemModel.hpp"

#include "test_utils.hpp"
#include "catch.hpp"
#include "Analyse/ScheduleValidationAnalyzer.hpp"

using namespace std;


#ifdef CPLEX
static bool has_cplex = true;
#else
static bool has_cplex = false;
#endif

TEST_CASE("Heterogeneous", "[ilp][formgen]") {
//    Utils::setLevel(Utils::Level::DEBUG);
    config_t conf;
    SystemModel tg;
    Schedule sched(&tg, &conf);

    CommandLineProperties prop;
    prop.insert({"solver", (has_cplex) ? "cplex" : "lp_solve"});

    setup_config(conf, tg, "trivial_heterogeneous.xml", prop);
    setup_tey(conf, tg, "3indepTasks.tey");
    
    for(CompilerPassPtr &p : conf.passes.passes())
        REQUIRE_NOTHROW(p->run(&tg, &conf, &sched));

    ScheduleValidationAnalyzer anal;
    REQUIRE_NOTHROW(anal.run(&tg, &conf, &sched));
//    ILPSmartHeterogeneous ilp(tg, conf, &sched, false);
//    ilp.gen();
//    cout << ILP::Renderer::Cplex::render(ilp.problem);
}
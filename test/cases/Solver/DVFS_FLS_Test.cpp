/*
 * Copyright (C) 2022 Julius Roeder <j.roeder@uva.nl>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "config.hpp"
#include "Utils.hpp"
#include "InputParser/InputParser.hpp"
#include "Solvers/Solver.hpp"
#include "SystemModel.hpp"
#include "Schedule.hpp"
#include "ScheduleValidationAnalyzer.hpp"
#include "test_utils.hpp"
#include "ScheduleFactory.hpp"
#include <catch.hpp>

#include <csignal>
#include <string>
#include <Solvers/ForwardListScheduling/ForwardListScheduling.hpp>
#include <Solvers/HEFT/HEFT.hpp>
#include <Solvers/HEFT/HER.hpp>
#include <Solvers/eFFLS/eFFLS.hpp>
#include <Exporter/SVGViewer.hpp>
#include <Solvers/FastFLS/FFLS.hpp>

using namespace std;

TEST_CASE("Energy calculation", "[solver][fls][energy-aware]") {
    config_t conf;
    SystemModel tg;
    setup_config(conf, tg, "eFLS.xml");
    setup_tey(conf, tg, "energy_DAG.tey");
    setup_nfp(conf, tg, "energy_nfp.nfp");
    Schedule sched(&tg, &conf);
//    sched.build_elements();

    SchedJob* elt0 = ScheduleFactory::build_job(&sched, tg.task("t0_0"), 0);
    SchedJob* elt10 = ScheduleFactory::build_job(&sched, tg.task("t0_10"), 0);
    SchedJob* elt41 = ScheduleFactory::build_job(&sched, tg.task("t0_41"), 0);

    SchedCore * little_core = ScheduleFactory::add_core(&sched, tg.processors()[0]);
    SchedCore * big_core = ScheduleFactory::add_core(&sched, tg.processors()[4]);

    Version *v10 = elt0->task()->version("t0_0_5_1900000");
    Version *v10_1 = elt10->task()->version("t0_10_1_1400000");
    Version *v20 = elt41->task()->version("t0_41_1_1400000_600000000");

    SECTION("Schedule task and version on to LITTLE CPU and check energy.") {
        elt0->wce(v10->C_E());
        elt0->wct(v10->C());
        elt0->frequency(v10->frequency());
        elt0->gpu_frequency(v10->gpu_frequency());
        elt0->gpu_required(v10->gpu_frequency());
        elt0->rt(0);
        sched.schedule(elt0);
        sched.map_core(elt0, big_core);
        sched.compute_makespan();
        sched.compute_energy();

        REQUIRE(sched.energy() == 230); //manually calculated; 1 from task dynamic; 184 from base watt; 45 from additional due to frequency
    }

    SECTION("Schedule two task to two different voltage islands and check energy.") {
        elt0->wce(v10->C_E());
        elt0->wct(v10->C());
        elt0->frequency(v10->frequency());
        elt0->gpu_frequency(v10->gpu_frequency());
        elt0->gpu_required(v10->gpu_required());
        elt0->rt(0);
        sched.schedule(elt0);
        sched.map_core(elt0, big_core);

        elt10->wce(v10_1->C_E());
        elt10->wct(v10_1->C());
        elt10->frequency(v10_1->frequency());
        elt10->gpu_frequency(v10_1->gpu_frequency());
        elt10->gpu_required(v10_1->gpu_required());
        elt10->rt(0);

        sched.schedule(elt10);
        sched.map_core(elt10, little_core);
        sched.compute_makespan();
        sched.compute_energy();

        REQUIRE(sched.makespan() == 200);
        REQUIRE(sched.energy() == 43795); //manually calculated
    }

    SECTION("Schedule two task to two different voltage islands and check energy.") {
        elt41->wce(v20->C_E());
        elt41->wct(v20->C());
        elt41->frequency(v20->frequency());
        elt41->gpu_frequency(v20->gpu_frequency());
        elt41->gpu_required(v20->gpu_required());
        elt41->rt(0);
        sched.schedule(elt41);
        sched.map_core(elt41, little_core);

        sched.compute_makespan();
        sched.compute_energy();

        REQUIRE(sched.makespan() == 519);
        REQUIRE(sched.energy() == 118719); //manually calculated
    }

}

TEST_CASE("DVFS Aware Scheduler Test", "[solver][fls][energy-aware]") {
    config_t conf;
    SystemModel tg;
    Schedule sched(&tg, &conf);
    ScheduleValidationAnalyzer validator;
    setup_config(conf, tg, "eFLS.xml");
    setup_tey(conf, tg, "energy_DAG.tey");
    setup_nfp(conf, tg, "energy_nfp.nfp");

    //TODO: if we ever manage to get everything deterministic (i.e. no more pointers as keys etc) then each of the energy() calls should always lead to the same result

    SECTION("DVFS eFLS Scheduler") { //TODO: change when scheduling is deterministic
        ForwardListScheduling fls;
        fls.type("DVFS_eFLS");
        fls.timeout(21600);

        for (CompilerPassPtr &p: conf.passes.passes())
            REQUIRE_NOTHROW(p->run(&tg, &conf, &sched));

        fls.run(&tg, &conf, &sched);

        REQUIRE_NOTHROW(validator.run(&tg, &conf, &sched));

        REQUIRE(sched.energy() <= 730000); // exact value: 727903; should not be more than 10% worse
        REQUIRE(sched.makespan() <= 1600); // exact value: 1569; should not be more than 10% worse
    }

    SECTION("DVFS FLS Scheduler"){ //TODO: change when scheduling is deterministic
        ForwardListScheduling fls;
        fls.type("DVFS_FLS");
        fls.timeout(21600);

        for (CompilerPassPtr &p: conf.passes.passes())
            REQUIRE_NOTHROW(p->run(&tg, &conf, &sched));

        fls.run(&tg, &conf, &sched);

        REQUIRE_NOTHROW(validator.run(&tg, &conf, &sched));

        REQUIRE(sched.energy() <= 960000); // exact value: 952982; should not be more than 10% worse
        REQUIRE(sched.makespan() <= 1300); // exact value: 1271; should not be more than 10% worse
    }

    SECTION("DVFS HEFT Scheduler"){ //TODO: change when scheduling is deterministic
        HEFT heft;
        heft.type("simple_HEFT");
        heft.timeout(21600);

        for (CompilerPassPtr &p: conf.passes.passes())
            REQUIRE_NOTHROW(p->run(&tg, &conf, &sched));

        heft.run(&tg, &conf, &sched);

        REQUIRE_NOTHROW(validator.run(&tg, &conf, &sched));

        REQUIRE(sched.energy() <= 1100000); // exact value: 1053802; should not be more than 10% worse
        REQUIRE(sched.makespan() <= 1250); // exact value: 1215; should not be more than 10% worse
    }

    SECTION("DVFS simple HEFT Scheduler"){ //TODO: change when scheduling is deterministic
        HEFT heft;
        heft.type("simple_HEFT_energy");
        heft.timeout(21600);

        for (CompilerPassPtr &p: conf.passes.passes())
            REQUIRE_NOTHROW(p->run(&tg, &conf, &sched));

        heft.run(&tg, &conf, &sched);

        REQUIRE_NOTHROW(validator.run(&tg, &conf, &sched));

        REQUIRE(sched.energy() <= 735000); // exact value: 731943; should not be more than 10% worse
        REQUIRE(sched.makespan() <= 1500); // exact value: 1493; should not be more than 10% worse
    }

    SECTION("DVFS eHER Scheduler"){  //TODO: change when scheduling is deterministic
        HER her;
        her.type("simple_HER");
        her.timeout(21600);

        for (CompilerPassPtr &p: conf.passes.passes())
            REQUIRE_NOTHROW(p->run(&tg, &conf, &sched));

        her.run(&tg, &conf, &sched);

        REQUIRE_NOTHROW(validator.run(&tg, &conf, &sched));

        REQUIRE(sched.energy() <= 735000); // exact value: 729804; should not be more than 10% worse
        REQUIRE(sched.makespan() <= 1500); // exact value: 1494; should not be more than 10% worse
    }

    std::cout << "Stats" << std::endl;
    for (auto &stat : sched.stats()) {
        std::cout << stat.first <<  ": " << stat.second << std::endl;
    }
}


TEST_CASE("DVFS Aware Scheduler Test (one core per island, no GPU)", "[solver][fls][energy-aware][independent]") {
    config_t conf;
    SystemModel tg;
    Schedule sched(&tg, &conf);
    ScheduleValidationAnalyzer validator;
    setup_config(conf, tg, "eFLS_independent.xml"); // eFFLS only supports independent islands
    setup_tey(conf, tg, "energy_DAG.tey");
    setup_nfp(conf, tg, "energy_nfp.nfp");

    // Remove GPU -- (e)FFLS does not support that
    tg.coprocessors().clear();

    SECTION("DVFS FFLS Scheduler") { //TODO: change when scheduling is deterministic
        ForwardListScheduling fls;
        fls.type("DVFS_eFLS");
        fls.timeout(21600);

        for (CompilerPassPtr &p: conf.passes.passes())
            REQUIRE_NOTHROW(p->run(&tg, &conf, &sched));

        fls.run(&tg, &conf, &sched);

        REQUIRE_NOTHROW(validator.run(&tg, &conf, &sched));

        REQUIRE(sched.energy() <= 980000); // exact value: 965439; should not be more than 10% worse
        REQUIRE(sched.makespan() <= 2500); // exact value: 2361; should not be more than 10% worse
    }

    SECTION("eFFLS Scheduler no deadline") {
        // This scheduler produces a slightly worse schedule due to its sorting
        // But it is unclear why this is, as LLF _should_ produce a better sorting

        EnergyFFLS fls;

        for (CompilerPassPtr &p: conf.passes.passes())
            REQUIRE_NOTHROW(p->run(&tg, &conf, &sched));

        fls.run(&tg, &conf, &sched);

        REQUIRE_NOTHROW(validator.run(&tg, &conf, &sched));

        REQUIRE(sched.energy() <= 980000); // exact value: 973667; should not be more than 10% worse
        REQUIRE(sched.makespan() <= 2500); // exact value: 2404; should not be more than 10% worse
    }

    SECTION("eFFLS Scheduler deadline") {
        // Force it to compromise to meet deadline
        tg.global_deadline(2000); // deciseconds, = 200 seconds

        EnergyFFLS fls;
        for (CompilerPassPtr &p: conf.passes.passes())
            REQUIRE_NOTHROW(p->run(&tg, &conf, &sched));

        fls.run(&tg, &conf, &sched);

        REQUIRE_NOTHROW(validator.run(&tg, &conf, &sched));
        REQUIRE(sched.energy() <= 1200000); // exact value: 1151170
    }

    SECTION("eFFLS Scheduler strict deadline") {
        // Force it to focus fully on performance to meet deadline
        tg.global_deadline(1890); // deciseconds

        EnergyFFLS fls;
        for (CompilerPassPtr &p: conf.passes.passes())
            REQUIRE_NOTHROW(p->run(&tg, &conf, &sched));

        fls.run(&tg, &conf, &sched);

        REQUIRE_NOTHROW(validator.run(&tg, &conf, &sched));
        REQUIRE(sched.energy() <= 1400000); // exact value: 1357447
    }
}
/*
 * Copyright (C) 2022 Benjamin Rouxel <benjamin.rouxel@unimore.it>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef CPLEX
#warning "Need CPLEX to pass the ILP tests as GLPK or LPSolve are not good enough"
#else 

#include "config.hpp"
#include "Utils.hpp"
#include "InputParser/InputParser.hpp"
#include "Solvers/Solver.hpp"
#include "SystemModel.hpp"
#include "Schedule.hpp"

#include "test_utils.hpp"
#include "Analyse/ScheduleValidationAnalyzer.hpp"
#include <catch.hpp>

#include <csignal>
#include <string>
#include <Solvers/Solver.hpp>

using namespace std;

#ifdef CPLEX
static bool has_cplex = true;
#else
static bool has_cplex = false;
#endif


TEST_CASE("ILP ideal communication", "[solver][ilp][comm-aware][ilp-comm-ideal]") {
    Utils::Level oldl = Utils::getLevel();
    Utils::setLevel(Utils::Level::DEBUG); //necessary to be in debug mode as test results have been generated in debug mode
    CommandLineProperties prop;
    prop.insert({"solver", (has_cplex) ? "cplex" : "lp_solve"});
    config_t conf;
    SystemModel tg;
    Schedule sched(&tg, &conf);
    setup_config(conf, tg, "ilp_ideal.xml", prop);
    setup_tey(conf, tg, "sched_test.tey");
    
    Utils::setLevel(oldl);
    for(CompilerPass *p : conf.passes)
        REQUIRE_NOTHROW(p->run(&tg, &conf, &sched));
 
    if(has_cplex) {
        Schedule check_sched(&tg, &conf);
        ResXMLSaxParser * rp = setup_xmlchecker(conf, tg, &check_sched, "ilp_ideal.xml");
        CHECK(*rp->schedule() == sched);
        delete rp;
    }
    else {
        WARN("=====> For a better test, compile CECILE with CPLEX support <====");
        ScheduleValidationAnalyzer anal;
        REQUIRE_NOTHROW(anal.run(&tg, &conf, &sched));
    }
}

TEST_CASE("ILP worst_concurrency communication", "[solver][ilp][comm-aware][ilp-comm-worst]") {
    Utils::Level oldl = Utils::getLevel();
    Utils::setLevel(Utils::Level::DEBUG); //necessary to be in debug mode as test results have been generated in debug mode
    CommandLineProperties prop;
    prop.insert({"solver", (has_cplex) ? "cplex" : "lp_solve"});
    config_t conf;
    SystemModel tg;
    Schedule sched(&tg, &conf);
    setup_config(conf, tg, "ilp_worst.xml", prop);
    setup_tey(conf, tg, "sched_test.tey");

    Utils::setLevel(oldl);
    for(CompilerPass *p : conf.passes)
        REQUIRE_NOTHROW(p->run(&tg, &conf, &sched));
 
    if(has_cplex) {
        Schedule check_sched(&tg, &conf);
        ResXMLSaxParser * rp = setup_xmlchecker(conf, tg, &check_sched, "ilp_worst.xml");
        CHECK(*rp->schedule() == sched);
        delete rp;
    }
    else {
        WARN("=====> For a better test, compile CECILE with CPLEX support <====");
        ScheduleValidationAnalyzer anal;
        REQUIRE_NOTHROW(anal.run(&tg, &conf, &sched));
    }
}

TEST_CASE("ILP contention aware communication", "[solver][ilp][comm-aware][ilp-comm-contaw]") {
    Utils::Level oldl = Utils::getLevel();
    Utils::setLevel(Utils::Level::DEBUG); //necessary to be in debug mode as test results have been generated in debug mode
    CommandLineProperties prop;
    prop.insert({"solver", (has_cplex) ? "cplex" : "lp_solve"});
    config_t conf;
    SystemModel tg;
    Schedule sched(&tg, &conf);
    setup_config(conf, tg, "ilp_contentionaware.xml", prop);
    setup_tey(conf, tg, "sched_test.tey");

    Utils::setLevel(oldl);
    if(has_cplex) {
        for(CompilerPass *p : conf.passes) {
            REQUIRE_NOTHROW(p->run(&tg, &conf, &sched));
        }
    }
    else {
        try {
            for(CompilerPass *p : conf.passes)
                p->run(&tg, &conf, &sched);
        }
        catch(exception &e) {}
    }
    if(sched.status() != ScheduleProperties::sched_statue_e::SCHED_UNSCHEDULABLE) {
        ScheduleValidationAnalyzer anal;
        REQUIRE_NOTHROW(anal.run(&tg, &conf, &sched));
    }
    //lp_solve/glpk => timeout
}

TEST_CASE("ILP synchronized communication", "[solver][ilp][comm-aware][ilp-comm-sync]") {
    Utils::Level oldl = Utils::getLevel();
    Utils::setLevel(Utils::Level::DEBUG); //necessary to be in debug mode as test results have been generated in debug mode
    CommandLineProperties prop;
    prop.insert({"solver", (has_cplex) ? "cplex" : "lp_solve"});
    config_t conf;
    SystemModel tg;
    Schedule sched(&tg, &conf);
    setup_config(conf, tg, "ilp_synchronized.xml", prop);
    setup_tey(conf, tg, "sched_test.tey");

    Utils::setLevel(oldl);
    if(has_cplex) {
        for(CompilerPass *p : conf.passes)
            REQUIRE_NOTHROW(p->run(&tg, &conf, &sched));
    }
    else {
        WARN("=====> For a better test, compile CECILE with CPLEX support <====");
        try {
            for(CompilerPass *p : conf.passes)
                p->run(&tg, &conf, &sched);
        }
        catch(exception &e) {}
        //sometimes it works, sometimes it doesn't ...
    }
    if(sched.status() != ScheduleProperties::sched_statue_e::SCHED_UNSCHEDULABLE) {
        ScheduleValidationAnalyzer anal;
        REQUIRE_NOTHROW(anal.run(&tg, &conf, &sched));
    }
}

TEST_CASE("ILP worst_concurrency non-blocking communication", "[solver][ilp][comm-aware][ilp-comm-worst][ilp-nbl-comm-worst]") {
    Utils::Level oldl = Utils::getLevel();
    Utils::setLevel(Utils::Level::DEBUG); //necessary to be in debug mode as test results have been generated in debug mode
    CommandLineProperties prop;
    prop.insert({"solver", (has_cplex) ? "cplex" : "lp_solve"});
    config_t conf;
    SystemModel tg;
    Schedule sched(&tg, &conf);
    setup_config(conf, tg, "ilp_worst_nbl.xml", prop);
    setup_tey(conf, tg, "sched_test.tey");

    Utils::setLevel(oldl);
    if(has_cplex) {
        for(CompilerPass *p : conf.passes)
            REQUIRE_NOTHROW(p->run(&tg, &conf, &sched));
    }
    else {
        WARN("=====> For a better test, compile CECILE with CPLEX support <====");
        try {
            for(CompilerPass *p : conf.passes)
                p->run(&tg, &conf, &sched);
        }
        catch(exception &e) {}
        //sometimes it works, sometimes it doesn't ...
    }
    if(sched.status() != ScheduleProperties::sched_statue_e::SCHED_UNSCHEDULABLE) {
        ScheduleValidationAnalyzer anal;
        REQUIRE_NOTHROW(anal.run(&tg, &conf, &sched));
    }
}

TEST_CASE("ILP contention aware non-blocking communication", "[solver][ilp][comm-aware][ilp-nbl-comm-contaw]") {
    Utils::Level oldl = Utils::getLevel();
    Utils::setLevel(Utils::Level::DEBUG); //necessary to be in debug mode as test results have been generated in debug mode
    CommandLineProperties prop;
    prop.insert({"solver", (has_cplex) ? "cplex" : "lp_solve"});
    config_t conf;
    SystemModel tg;
    Schedule sched(&tg, &conf);
    setup_config(conf, tg, "ilp_contentionaware_nbl.xml", prop);
    setup_tey(conf, tg, "sched_test.tey");

    Utils::setLevel(oldl);
    if(has_cplex) {
        for(CompilerPass *p : conf.passes)
            REQUIRE_NOTHROW(p->run(&tg, &conf, &sched));
    }
    else {
        WARN("=====> For a better test, compile CECILE with CPLEX support <====");
        try {
            for(CompilerPass *p : conf.passes)
                p->run(&tg, &conf, &sched);
        }
        catch(exception &e) {}
        //sometimes it works, sometimes it doesn't ...
    }
    if(sched.status() != ScheduleProperties::sched_statue_e::SCHED_UNSCHEDULABLE) {
        ScheduleValidationAnalyzer anal;
        REQUIRE_NOTHROW(anal.run(&tg, &conf, &sched));
    }
}

TEST_CASE("ILP synchronized non-blocking communication", "[solver][ilp][comm-aware][ilp-nbl-comm-sync]") {
    Utils::Level oldl = Utils::getLevel();
    Utils::setLevel(Utils::Level::DEBUG); //necessary to be in debug mode as test results have been generated in debug mode
    CommandLineProperties prop;
    prop.insert({"solver", (has_cplex) ? "cplex" : "lp_solve"});
    config_t conf;
    SystemModel tg;
    Schedule sched(&tg, &conf);
    setup_config(conf, tg, "ilp_synchronized_nbl.xml", prop);
    setup_tey(conf, tg, "sched_test.tey");

    Utils::setLevel(oldl);
    if(has_cplex) {
        for(CompilerPass *p : conf.passes)
            REQUIRE_NOTHROW(p->run(&tg, &conf, &sched));
    }
    else {
        WARN("=====> For a better test, compile CECILE with CPLEX support <====");
        try {
            for(CompilerPass *p : conf.passes)
                p->run(&tg, &conf, &sched);
        }
        catch(exception &e) {}
        //sometimes it works, sometimes it doesn't ...
    }
    if(sched.status() != ScheduleProperties::sched_statue_e::SCHED_UNSCHEDULABLE) {
        ScheduleValidationAnalyzer anal;
        REQUIRE_NOTHROW(anal.run(&tg, &conf, &sched));
    }
}

TEST_CASE("ILP worst_concurrency shared communication", "[solver][ilp][comm-aware][ilp-comm-worst][ilp-sha-comm-worst]") {
    Utils::Level oldl = Utils::getLevel();
    Utils::setLevel(Utils::Level::DEBUG); //necessary to be in debug mode as test results have been generated in debug mode
    CommandLineProperties prop;
    prop.insert({"solver", (has_cplex) ? "cplex" : "lp_solve"});
    config_t conf;
    SystemModel tg;
    Schedule sched(&tg, &conf);
    setup_config(conf, tg, "ilp_worst_sha.xml", prop);
    setup_tey(conf, tg, "sched_test.tey");

    Utils::setLevel(oldl);
    if(has_cplex) {
        for(CompilerPass *p : conf.passes)
            REQUIRE_NOTHROW(p->run(&tg, &conf, &sched));
    }
    else {
        WARN("=====> For a better test, compile CECILE with CPLEX support <====");
        try {
            for(CompilerPass *p : conf.passes)
                p->run(&tg, &conf, &sched);
        }
        catch(exception &e) {}
        //sometimes it works, sometimes it doesn't ...
    }
    if(sched.status() != ScheduleProperties::sched_statue_e::SCHED_UNSCHEDULABLE) {
        ScheduleValidationAnalyzer anal;
        REQUIRE_NOTHROW(anal.run(&tg, &conf, &sched));
    }
}

TEST_CASE("ILP contention aware shared communication", "[solver][ilp][comm-aware][ilp-sha-comm-contaw]") {
    Utils::Level oldl = Utils::getLevel();
    Utils::setLevel(Utils::Level::DEBUG); //necessary to be in debug mode as test results have been generated in debug mode
    CommandLineProperties prop;
    prop.insert({"solver", (has_cplex) ? "cplex" : "lp_solve"});
    config_t conf;
    SystemModel tg;
    Schedule sched(&tg, &conf);
    setup_config(conf, tg, "ilp_contentionaware_sha.xml", prop);
    setup_tey(conf, tg, "sched_test.tey");

    Utils::setLevel(oldl);
    if(has_cplex) {
        for(CompilerPass *p : conf.passes)
            REQUIRE_NOTHROW(p->run(&tg, &conf, &sched));
    }
    else {
        WARN("=====> For a better test, compile CECILE with CPLEX support <====");
        try {
            for(CompilerPass *p : conf.passes)
                p->run(&tg, &conf, &sched);
        }
        catch(exception &e) {}
        //sometimes it works, sometimes it doesn't ...
    }
    if(sched.status() != ScheduleProperties::sched_statue_e::SCHED_UNSCHEDULABLE) {
        ScheduleValidationAnalyzer anal;
        REQUIRE_NOTHROW(anal.run(&tg, &conf, &sched));
    }
}

TEST_CASE("ILP synchronized shared communication", "[solver][ilp][comm-aware][ilp-sha-comm-sync]") {
    Utils::Level oldl = Utils::getLevel();
    Utils::setLevel(Utils::Level::DEBUG); //necessary to be in debug mode as test results have been generated in debug mode
    CommandLineProperties prop;
    prop.insert({"solver", (has_cplex) ? "cplex" : "lp_solve"});
    config_t conf;
    SystemModel tg;
    Schedule sched(&tg, &conf);
    setup_config(conf, tg, "ilp_synchronized_sha.xml", prop);
    setup_tey(conf, tg, "sched_test.tey");

    Utils::setLevel(oldl);
    if(has_cplex) {
        for(CompilerPass *p : conf.passes)
            REQUIRE_NOTHROW(p->run(&tg, &conf, &sched));
    }
    else {
        WARN("=====> For a better test, compile CECILE with CPLEX support <====");
        try {
            for(CompilerPass *p : conf.passes)
                p->run(&tg, &conf, &sched);
        }
        catch(exception &e) {}
        //sometimes it works, sometimes it doesn't ...
    }
    if(sched.status() != ScheduleProperties::sched_statue_e::SCHED_UNSCHEDULABLE) {
        ScheduleValidationAnalyzer anal;
        REQUIRE_NOTHROW(anal.run(&tg, &conf, &sched));
    }
}

TEST_CASE("ILP worst_concurrency non-blocking shared communication", "[solver][ilp][comm-aware][ilp-comm-worst][ilp-nbl-sha-comm-worst]") {
    Utils::Level oldl = Utils::getLevel();
    Utils::setLevel(Utils::Level::DEBUG); //necessary to be in debug mode as test results have been generated in debug mode
    CommandLineProperties prop;
    prop.insert({"solver", (has_cplex) ? "cplex" : "lp_solve"});
    config_t conf;
    SystemModel tg;
    Schedule sched(&tg, &conf);
    setup_config(conf, tg, "ilp_worst_nbl_sha.xml", prop);
    setup_tey(conf, tg, "sched_test.tey");

    Utils::setLevel(oldl);
    if(has_cplex) {
        for(CompilerPass *p : conf.passes)
            REQUIRE_NOTHROW(p->run(&tg, &conf, &sched));
    }
    else {
        WARN("=====> For a better test, compile CECILE with CPLEX support <====");
        try {
            for(CompilerPass *p : conf.passes)
                p->run(&tg, &conf, &sched);
        }
        catch(exception &e) {}
        //sometimes it works, sometimes it doesn't ...
    }
    if(sched.status() != ScheduleProperties::sched_statue_e::SCHED_UNSCHEDULABLE) {
        ScheduleValidationAnalyzer anal;
        REQUIRE_NOTHROW(anal.run(&tg, &conf, &sched));
    }
}

TEST_CASE("ILP contention aware non-blocking shared communication", "[solver][ilp][comm-aware][ilp-nbl-sha-comm-contaw]") {
    Utils::Level oldl = Utils::getLevel();
    Utils::setLevel(Utils::Level::DEBUG); //necessary to be in debug mode as test results have been generated in debug mode
    CommandLineProperties prop;
    prop.insert({"solver", (has_cplex) ? "cplex" : "lp_solve"});
    config_t conf;
    SystemModel tg;
    Schedule sched(&tg, &conf);
    setup_config(conf, tg, "ilp_contentionaware_nbl_sha.xml", prop);
    setup_tey(conf, tg, "sched_test.tey");

    Utils::setLevel(oldl);
    if(has_cplex) {
        for(CompilerPass *p : conf.passes)
            REQUIRE_NOTHROW(p->run(&tg, &conf, &sched));
    }
    else {
        WARN("=====> For a better test, compile CECILE with CPLEX support <====");
        try {
            for(CompilerPass *p : conf.passes)
                p->run(&tg, &conf, &sched);
        }
        catch(exception &e) {}
        //sometimes it works, sometimes it doesn't ...
    }
    if(sched.status() != ScheduleProperties::sched_statue_e::SCHED_UNSCHEDULABLE) {
        ScheduleValidationAnalyzer anal;
        REQUIRE_NOTHROW(anal.run(&tg, &conf, &sched));
    }
}

TEST_CASE("ILP synchronized non-blocking shared communication", "[solver][ilp][comm-aware][ilp-nbl-sha-comm-sync]") {
    Utils::Level oldl = Utils::getLevel();
    Utils::setLevel(Utils::Level::DEBUG); //necessary to be in debug mode as test results have been generated in debug mode
    CommandLineProperties prop;
    prop.insert({"solver", (has_cplex) ? "cplex" : "lp_solve"});
    config_t conf;
    SystemModel tg;
    Schedule sched(&tg, &conf);
    setup_config(conf, tg, "ilp_synchronized_nbl_sha.xml", prop);
    setup_tey(conf, tg, "sched_test.tey");

    Utils::setLevel(oldl);
    if(has_cplex) {
        for(CompilerPass *p : conf.passes)
            REQUIRE_NOTHROW(p->run(&tg, &conf, &sched));
    }
    else {
        WARN("=====> For a better test, compile CECILE with CPLEX support <====");
        try {
            for(CompilerPass *p : conf.passes)
                p->run(&tg, &conf, &sched);
        }
        catch(exception &e) {}
        //sometimes it works, sometimes it doesn't ...
    }
    if(sched.status() != ScheduleProperties::sched_statue_e::SCHED_UNSCHEDULABLE) {
        ScheduleValidationAnalyzer anal;
        REQUIRE_NOTHROW(anal.run(&tg, &conf, &sched));
    }
}
TEST_CASE("ILP worst_concurrency burst communication", "[solver][ilp][comm-aware][ilp-comm-worst][ilp-comm-worst-burstedge]") {
    Utils::Level oldl = Utils::getLevel();
    Utils::setLevel(Utils::Level::DEBUG); //necessary to be in debug mode as test results have been generated in debug mode
    CommandLineProperties prop;
    prop.insert({"solver", (has_cplex) ? "cplex" : "lp_solve"});
    config_t conf;
    SystemModel tg;
    Schedule sched1(&tg, &conf);
    setup_config(conf, tg, "ilp_worst_burstedge.xml", prop);
    setup_tey(conf, tg, "sched_test.tey");

    Utils::setLevel(oldl);
    for(CompilerPass *p : conf.passes) {
        if(p->get_uniqid_rtti().find("solver") != string::npos)
            REQUIRE_THROWS(p->run(&tg, &conf, &sched1));
        else
            REQUIRE_NOTHROW(p->run(&tg, &conf, &sched1));
    }
}

TEST_CASE("ILP contention aware burst communication", "[solver][ilp][comm-aware][ilp-comm-contaw-burstedge]") {
    Utils::Level oldl = Utils::getLevel();
    Utils::setLevel(Utils::Level::DEBUG); //necessary to be in debug mode as test results have been generated in debug mode
    CommandLineProperties prop;
    prop.insert({"solver", (has_cplex) ? "cplex" : "lp_solve"});
    config_t conf;
    SystemModel tg;
    Schedule sched(&tg, &conf);
    setup_config(conf, tg, "ilp_contentionaware_burstedge.xml", prop);
    setup_tey(conf, tg, "sched_test.tey");

    Utils::setLevel(oldl);
    for(CompilerPass *p : conf.passes) {
        if(p->get_uniqid_rtti().find("solver") != string::npos)
            REQUIRE_THROWS(p->run(&tg, &conf, &sched));
        else
            REQUIRE_NOTHROW(p->run(&tg, &conf, &sched));
    }
}

TEST_CASE("ILP synchronized burst communication", "[solver][ilp][comm-aware][ilp-comm-sync-burstedge]") {
    Utils::Level oldl = Utils::getLevel();
    Utils::setLevel(Utils::Level::DEBUG); //necessary to be in debug mode as test results have been generated in debug mode
    CommandLineProperties prop;
    prop.insert({"solver", (has_cplex) ? "cplex" : "lp_solve"});
    config_t conf;
    SystemModel tg;
    Schedule sched(&tg, &conf);
    setup_config(conf, tg, "ilp_synchronized_burstedge.xml", prop);
    setup_tey(conf, tg, "sched_test.tey");

    Utils::setLevel(oldl);
    if(has_cplex) {
        for(CompilerPass *p : conf.passes)
            REQUIRE_NOTHROW(p->run(&tg, &conf, &sched));
    }
    else {
        WARN("=====> For a better test, compile CECILE with CPLEX support <====");
        try {
            for(CompilerPass *p : conf.passes)
                p->run(&tg, &conf, &sched);
        }
        catch(exception &e) {}
        //sometimes it works, sometimes it doesn't ...
    }
    if(sched.status() != ScheduleProperties::sched_statue_e::SCHED_UNSCHEDULABLE) {
        ScheduleValidationAnalyzer anal;
        REQUIRE_NOTHROW(anal.run(&tg, &conf, &sched));
    }
}
TEST_CASE("ILP worst_concurrency non-blocking burst communication", "[solver][ilp][comm-aware][ilp-comm-worst][ilp-nbl-comm-worst-burstedge]") {
    Utils::Level oldl = Utils::getLevel();
    Utils::setLevel(Utils::Level::DEBUG); //necessary to be in debug mode as test results have been generated in debug mode
    CommandLineProperties prop;
    prop.insert({"solver", (has_cplex) ? "cplex" : "lp_solve"});
    config_t conf;
    SystemModel tg;
    Schedule sched(&tg, &conf);
    setup_config(conf, tg, "ilp_worst_nbl_burstedge.xml", prop);
    setup_tey(conf, tg, "sched_test.tey");

    Utils::setLevel(oldl);
    for(CompilerPass *p : conf.passes) {
        if(p->get_uniqid_rtti().find("solver") != string::npos)
            REQUIRE_THROWS(p->run(&tg, &conf, &sched));
        else
            REQUIRE_NOTHROW(p->run(&tg, &conf, &sched));
    }
}

TEST_CASE("ILP contention aware non-blocking burst communication", "[solver][ilp][comm-aware][ilp-nbl-comm-contaw-burstedge]") {
    Utils::Level oldl = Utils::getLevel();
    Utils::setLevel(Utils::Level::DEBUG); //necessary to be in debug mode as test results have been generated in debug mode
    CommandLineProperties prop;
    prop.insert({"solver", (has_cplex) ? "cplex" : "lp_solve"});
    config_t conf;
    SystemModel tg;
    Schedule sched(&tg, &conf);
    setup_config(conf, tg, "ilp_contentionaware_nbl_burstedge.xml", prop);
    setup_tey(conf, tg, "sched_test.tey");

    Utils::setLevel(oldl);
    for(CompilerPass *p : conf.passes) {
        if(p->get_uniqid_rtti().find("solver") != string::npos)
            REQUIRE_THROWS(p->run(&tg, &conf, &sched));
        else
            REQUIRE_NOTHROW(p->run(&tg, &conf, &sched));
    }
}

TEST_CASE("ILP synchronized non-blocking burst communication", "[solver][ilp][comm-aware][ilp-nbl-comm-sync-burstedge]") {
    Utils::Level oldl = Utils::getLevel();
    Utils::setLevel(Utils::Level::DEBUG); //necessary to be in debug mode as test results have been generated in debug mode
    CommandLineProperties prop;
    prop.insert({"solver", (has_cplex) ? "cplex" : "lp_solve"});
    config_t conf;
    SystemModel tg;
    Schedule sched(&tg, &conf);
    setup_config(conf, tg, "ilp_synchronized_nbl_burstedge.xml", prop);
    setup_tey(conf, tg, "sched_test.tey");

    Utils::setLevel(oldl);
    if(has_cplex) {
        for(CompilerPass *p : conf.passes)
            REQUIRE_NOTHROW(p->run(&tg, &conf, &sched));
    }
    else {
        WARN("=====> For a better test, compile CECILE with CPLEX support <====");
        try {
            for(CompilerPass *p : conf.passes)
                p->run(&tg, &conf, &sched);
        }
        catch(exception &e) {}
        //sometimes it works, sometimes it doesn't ...
    }
    if(sched.status() != ScheduleProperties::sched_statue_e::SCHED_UNSCHEDULABLE) {
        ScheduleValidationAnalyzer anal;
        REQUIRE_NOTHROW(anal.run(&tg, &conf, &sched));
    }
}
TEST_CASE("ILP worst_concurrency non-blocking burst shared communication", "[solver][ilp][comm-aware][ilp-comm-worst][ilp-nbl-sha-comm-worst-burstedge]") {
    Utils::Level oldl = Utils::getLevel();
    Utils::setLevel(Utils::Level::DEBUG); //necessary to be in debug mode as test results have been generated in debug mode
    CommandLineProperties prop;
    prop.insert({"solver", (has_cplex) ? "cplex" : "lp_solve"});
    config_t conf;
    SystemModel tg;
    Schedule sched(&tg, &conf);
    setup_config(conf, tg, "ilp_worst_nbl_sha_burstedge.xml", prop);
    setup_tey(conf, tg, "sched_test.tey");

    Utils::setLevel(oldl);
    for(CompilerPass *p : conf.passes) {
        if(p->get_uniqid_rtti().find("solver") != string::npos)
            REQUIRE_THROWS(p->run(&tg, &conf, &sched));
        else
            REQUIRE_NOTHROW(p->run(&tg, &conf, &sched));
    }
}
TEST_CASE("ILP contention aware non-blocking burst shared communication", "[solver][ilp][comm-aware][ilp-nbl-sha-comm-contaw-burstedge]") {
    Utils::Level oldl = Utils::getLevel();
    Utils::setLevel(Utils::Level::DEBUG); //necessary to be in debug mode as test results have been generated in debug mode
    CommandLineProperties prop;
    prop.insert({"solver", (has_cplex) ? "cplex" : "lp_solve"});
    config_t conf;
    SystemModel tg;
    Schedule sched(&tg, &conf);
    setup_config(conf, tg, "ilp_contentionaware_nbl_sha_burstedge.xml", prop);
    setup_tey(conf, tg, "sched_test.tey");

    Utils::setLevel(oldl);
    for(CompilerPass *p : conf.passes) {
        if(p->get_uniqid_rtti().find("solver") != string::npos)
            REQUIRE_THROWS(p->run(&tg, &conf, &sched));
        else
            REQUIRE_NOTHROW(p->run(&tg, &conf, &sched));
    }
}
TEST_CASE("ILP synchronized non-blocking burst shared communication", "[solver][ilp][comm-aware][ilp-nbl-sha-comm-sync-burstedge]") {
    Utils::Level oldl = Utils::getLevel();
    Utils::setLevel(Utils::Level::DEBUG); //necessary to be in debug mode as test results have been generated in debug mode
    CommandLineProperties prop;
    prop.insert({"solver", (has_cplex) ? "cplex" : "lp_solve"});
    config_t conf;
    SystemModel tg;
    Schedule sched(&tg, &conf);
    setup_config(conf, tg, "ilp_synchronized_nbl_sha_burstedge.xml", prop);
    setup_tey(conf, tg, "sched_test.tey");

    Utils::setLevel(oldl);
    if(has_cplex) {
        for(CompilerPass *p : conf.passes)
            REQUIRE_NOTHROW(p->run(&tg, &conf, &sched));
    }
    else {
        WARN("=====> For a better test, compile CECILE with CPLEX support <====");
        try {
            for(CompilerPass *p : conf.passes)
                p->run(&tg, &conf, &sched);
        }
        catch(exception &e) {}
        //sometimes it works, sometimes it doesn't ...
    }
    if(sched.status() != ScheduleProperties::sched_statue_e::SCHED_UNSCHEDULABLE) {
        ScheduleValidationAnalyzer anal;
        REQUIRE_NOTHROW(anal.run(&tg, &conf, &sched));
    }
}

TEST_CASE("ILP SPM mapping -- local data", "[solver][ilp][comm-aware][ilp-spm-mapping][ilp-spm-mapping-ldata]") {
    Utils::Level oldl = Utils::getLevel();
    Utils::setLevel(Utils::Level::DEBUG); //necessary to be in debug mode as test results have been generated in debug mode
    CommandLineProperties prop;
    prop.insert({"solver", (has_cplex) ? "cplex" : "lp_solve"});
    config_t conf;
    SystemModel tg;
    Schedule sched(&tg, &conf);
    setup_config(conf, tg, "ilp_spm_mapping_ldata.xml", prop);
    setup_tey(conf, tg, "sched_test.tey");

    Utils::setLevel(oldl);
    if(has_cplex) {
        for(CompilerPass *p : conf.passes)
            REQUIRE_NOTHROW(p->run(&tg, &conf, &sched));
    }
    else {
        WARN("=====> For a better test, compile CECILE with CPLEX support <====");
        try {
            for(CompilerPass *p : conf.passes)
                p->run(&tg, &conf, &sched);
        }
        catch(exception &e) {}
        //sometimes it works, sometimes it doesn't ...
    }
    if(sched.status() != ScheduleProperties::sched_statue_e::SCHED_UNSCHEDULABLE) {
        ScheduleValidationAnalyzer anal;
        REQUIRE_NOTHROW(anal.run(&tg, &conf, &sched));
    }
}

TEST_CASE("ILP SPM mapping -- code+local data", "[solver][ilp][comm-aware][ilp-spm-mapping][ilp-spm-mapping-full]") {
    Utils::Level oldl = Utils::getLevel();
    Utils::setLevel(Utils::Level::DEBUG); //necessary to be in debug mode as test results have been generated in debug mode
    CommandLineProperties prop;
    prop.insert({"solver", (has_cplex) ? "cplex" : "lp_solve"});
    config_t conf;
    SystemModel tg;
    Schedule sched(&tg, &conf);
    setup_config(conf, tg, "ilp_spm_mapping_full.xml", prop);
    setup_tey(conf, tg, "sched_test.tey");

    Utils::setLevel(oldl);
    if(has_cplex) {
        for(CompilerPass *p : conf.passes)
            REQUIRE_NOTHROW(p->run(&tg, &conf, &sched));
    }
    else {
        WARN("=====> For a better test, compile CECILE with CPLEX support <====");
        try {
            for(CompilerPass *p : conf.passes)
                p->run(&tg, &conf, &sched);
        }
        catch(exception &e) {}
        //sometimes it works, sometimes it doesn't ...
    }
    if(sched.status() != ScheduleProperties::sched_statue_e::SCHED_UNSCHEDULABLE) {
        ScheduleValidationAnalyzer anal;
        REQUIRE_NOTHROW(anal.run(&tg, &conf, &sched));
    }
}

TEST_CASE("ILP SPM mapping -- code+local data prefetch code", "[solver][ilp][comm-aware][ilp-spm-mapping][ilp-spm-mapping-pref]") {
    Utils::Level oldl = Utils::getLevel();
    Utils::setLevel(Utils::Level::DEBUG); //necessary to be in debug mode as test results have been generated in debug mode
    CommandLineProperties prop;
    prop.insert({"solver", (has_cplex) ? "cplex" : "lp_solve"});
    config_t conf;
    SystemModel tg;
    Schedule sched(&tg, &conf);
    setup_config(conf, tg, "ilp_spm_mapping_pref.xml", prop);
    setup_tey(conf, tg, "sched_test.tey");

    Utils::setLevel(oldl);
    if(has_cplex) {
        for(CompilerPass *p : conf.passes)
            REQUIRE_NOTHROW(p->run(&tg, &conf, &sched));
    }
    else {
        WARN("=====> For a better test, compile CECILE with CPLEX support <====");
        try {
            for(CompilerPass *p : conf.passes)
                p->run(&tg, &conf, &sched);
        }
        catch(exception &e) {}
        //sometimes it works, sometimes it doesn't ...
    }
    if(sched.status() != ScheduleProperties::sched_statue_e::SCHED_UNSCHEDULABLE) {
        ScheduleValidationAnalyzer anal;
        REQUIRE_NOTHROW(anal.run(&tg, &conf, &sched));
    }
}
TEST_CASE("ILP SPM mapping -- code+local data prefetch code shared", "[solver][ilp][comm-aware][ilp-spm-mapping][ilp-spm-mapping-pref-sha]") {
    Utils::Level oldl = Utils::getLevel();
    Utils::setLevel(Utils::Level::DEBUG); //necessary to be in debug mode as test results have been generated in debug mode
    CommandLineProperties prop;
    prop.insert({"solver", (has_cplex) ? "cplex" : "lp_solve"});
    config_t conf;
    SystemModel tg;
    Schedule sched(&tg, &conf);
    setup_config(conf, tg, "ilp_spm_mapping_pref_sha.xml", prop);
    setup_tey(conf, tg, "sched_test.tey");

    Utils::setLevel(oldl);
    if(has_cplex) {
        for(CompilerPass *p : conf.passes)
            REQUIRE_NOTHROW(p->run(&tg, &conf, &sched));
    }
    else {
        WARN("=====> For a better test, compile CECILE with CPLEX support <====");
        try {
            for(CompilerPass *p : conf.passes)
                p->run(&tg, &conf, &sched);
        }
        catch(exception &e) {}
        //sometimes it works, sometimes it doesn't ...
    }
    if(sched.status() != ScheduleProperties::sched_statue_e::SCHED_UNSCHEDULABLE) {
        ScheduleValidationAnalyzer anal;
        REQUIRE_NOTHROW(anal.run(&tg, &conf, &sched));
    }
}

TEST_CASE("ILP SPM mapping -- failed", "[solver][ilp][comm-aware][ilp-spm-mapping][ilp-spm-mapping-failed]") {
    Utils::Level oldl = Utils::getLevel();
    Utils::setLevel(Utils::Level::DEBUG); //necessary to be in debug mode as test results have been generated in debug mode
    CommandLineProperties prop;
    prop.insert({"solver", (has_cplex) ? "cplex" : "lp_solve"});
    config_t conf;
    SystemModel tg;
    Schedule sched(&tg, &conf);
    setup_config(conf, tg, "ilp_spm_mapping_failed.xml", prop);
    setup_tey(conf, tg, "sched_test.tey");

    Utils::setLevel(oldl);
    for(CompilerPass *p : conf.passes) {
        if(Utils::isa<Solver*>(p))
            REQUIRE_THROWS(p->run(&tg, &conf, &sched));
        else
            REQUIRE_NOTHROW(p->run(&tg, &conf, &sched));
    }
}
#endif // CPLEX
/*
 * Copyright (C) 2021 Lukas Miedema <l.miedema@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#define CATCH_CONFIG_EXTERNAL_INTERFACES

#include "config.hpp"
#include "Utils.hpp"
#include "InputParser/InputParser.hpp"
#include "Solvers/Solver.hpp"
#include "SystemModel.hpp"
#include "Schedule.hpp"

#include "test_utils.hpp"
#include <catch.hpp>

#include <csignal>
#include <string>
#include <Solvers/ForwardListScheduling/ForwardListScheduling.hpp>
#include <Transform/PropagateProperties.hpp>
#include <ScheduleValidationAnalyzer.hpp>
#include <Solvers/FastFLS/FFLS.hpp>
#include <Solvers/HEFT/HEFT.hpp>


/**
 * Throw 50 task graphs at each of the schedulers and test basic validity of the schedule
 * Besides the assumption that each of the task graphs is schedulable, there are no other assumptions made (e.g. there
 * is no assertion that the makespan is a particular value other than <= deadline).
 *
 * The 50 task graphs have been generated with TGFF, including random WCET values. FLS "ideal" was used to determine
 * makespans for each of the graphs, to which 20% has been added to create global deadlines. Note that the goal of this
 * test is not to test the performance of the solver, rather the correctness of the schedule.
 *
 * NOTE: Currently only does single-version single-phase problems optimizing makespan.
 * Also, due to the memory leaks in FLS, this uses about 8GB of memory for "ideal".
 *
 *
 * DAGs are in test/resources/coord/50dags/dagN.tey
 * NFPs are in test/resources/nfp/, one file usually covers multiple DAGs
 * - DAG [ 0,19] are homogenous single-version (8H.xml, dags-homogenous.nfp)
 * - DAG [ 0,19] are homogenous multi-version (8H.xml, dags-homogenous.nfp + augmented with "E" versions in the test)
 * - DAG [20,29] are heterogeneous single-version (4H4E.xml, dags-heterogeneous.nfp, single version per architecture)
 * - DAG [20,29] are heterogeneous multi-version (4H4E.xml, dags-heterogeneous.nfp + augmented with "fast" versions in the test)
 */

/**
 * This test suite is really lame. Yes it does extansively test the scheduler, but
 * what if they cover the same cases, meaning they don't increase the code coverage..
 * Besides, when one test fails it is really hard to replay it on the side to
 * identify the cause of the error.
 */

#define DAGS_HOMOGENOUS_START 0
#define DAGS_HOMOGENOUS_END   20

#define DAGS_HETEROGENEOUS_START 20
#define DAGS_HETEROGENEOUS_END   29

// Global data (initialized by TestListener)
typedef std::unique_ptr<std::pair<SystemModel, config_t>> GraphPtr;
std::vector<GraphPtr> graphs;
NfpFileParser *nfp_8h_sv, *nfp_8h_mv, *nfp_4h4e_sv, *nfp_4h4e_mv;

struct TestListener : Catch::TestEventListenerBase {
    using TestEventListenerBase::TestEventListenerBase;

    static void loadTest(uint dag_id, const std::string &config) {
        graphs.push_back(std::make_unique<std::pair<SystemModel, config_t>>());

        std::pair<SystemModel, config_t> *pair = graphs.back().get();
        SystemModel &tg = pair->first;
        config_t &conf = pair->second;

        setup_config(conf, tg, config);
        setup_tey(conf, tg, "50dags/dag" + std::to_string(dag_id) + ".tey");
    }

    static NfpFileParser *withFastVersion(NfpFileParser *input) {
        // multi-version offers a second 10% faster version which should always be picked
        auto *input_mv = new NfpFileParser();
        for (Nfp& nfp : input->properties()) {
            input_mv->properties().push_back(nfp);
            Nfp fastVersion = nfp;
            fastVersion.wcet = std::to_string((long) (0.9 * (double) std::stol(fastVersion.wcet)));
            fastVersion.componentVersion = "fast" + fastVersion.componentVersion;
            input_mv->properties().push_back(fastVersion);
        }
        return input_mv;
    }

    void testRunStarting(const Catch::TestRunInfo &_testRunInfo) override {
        // Read all DAGs
        for (uint i = DAGS_HOMOGENOUS_START; i < DAGS_HOMOGENOUS_END; i++)
            loadTest(i, "8H.xml");
        for (uint i = DAGS_HETEROGENEOUS_START; i < DAGS_HETEROGENEOUS_END; i++)
            loadTest(i, "4H4E.xml");

        // Read all NFP
        nfp_8h_sv = read_nfp("dags-homogenous.nfp");
        nfp_8h_mv = withFastVersion(nfp_8h_sv);
        nfp_4h4e_sv = read_nfp("dags-heterogeneous.nfp");
        nfp_4h4e_mv = withFastVersion(nfp_4h4e_sv);
    }

    void testRunEnded(const Catch::TestRunStats &stats) override {
        delete nfp_8h_sv;
        delete nfp_8h_mv;
        delete nfp_4h4e_sv;
        delete nfp_4h4e_mv;
        graphs.clear();
    }
};

CATCH_REGISTER_LISTENER(TestListener);


void runTest(SystemModel &tg, config_t &conf, Solver& solver) {
    Schedule schedule(&tg, &conf);
    PropagateProperties().run(&tg, &conf, &schedule);

    // Run solver
    solver.timeout(1000 * 1000 * 1000);
    solver.run(&tg, &conf, &schedule);

    // Validate
//    SVGViewer svg;
//    svg.run(&tg, &conf, &schedule);

    ScheduleValidationAnalyzer sva;
    bool test_ok = true;
    sva.report_function([&conf, &test_ok](bool &all_ok, bool property_ok, const std::string &label) {
        INFO((property_ok ? "[OKAY] " : "[FAIL] ") << label << " for " << conf.files.coord_basename);
        REQUIRE(property_ok);
    });

    sva.run(&tg, &conf, &schedule);
}

void runTestSuite(GraphPtr &graph, NfpFileParser *nfpSource,
        const std::function<void(SystemModel&, config_t&)>& additionalSetup = [](SystemModel&, config_t&) {}) {
    SystemModel& tg = graph->first;
    config_t &config = graph->second;

    // validate the test
    REQUIRE_FALSE(tg.tasks().empty());
    REQUIRE(tg.global_deadline() > 0);

    nfpSource->apply(config, tg, false);
    additionalSetup(tg, config);

    // FLS logs so much that the log messages take about 400MB, which seems to crash Catch -> disable DEBUG logging
    Utils::setLevel(Utils::Level::INFO);

    SECTION("FLS/ideal") {
        ForwardListScheduling fls;
        fls.type("ideal");
        runTest(tg, config, fls);
    }

//    SECTION("FLS/worst_concurrency") {
//
//        ForwardListScheduling fls;
//        fls.type("worst_concurrency");
//        runTest(tg, config, fls);
//    }

    SECTION("FLS/singlephase") {
        ForwardListScheduling fls;
        fls.type("singlephase");
        runTest(tg, config, fls);
    }

    SECTION("FLS/multiphase") {
        ForwardListScheduling fls;
        fls.type("multiphase");
        runTest(tg, config, fls);
    }

//    SECTION("HEFT/simple_HEFT") {
//        HEFT heft;
//        heft.type("simple_HEFT");
//        runTest(tg, config, heft);
//    }

    SECTION("FFLS") {
        FastForwardListScheduling ffls;
        runTest(tg, config, ffls);
    }

    // TODO: insane spaghetti code, this really shouldn't be necessary
    // SystemModel has no overloaded assignment operator, so our "tg" shares Task pointers with pair.first
    // As such, to let the other test cases reuse the same task graph, we must undo the effects of applying the NFP
    // Ben: Basic rule of unit testing, 1 task graph + 1 configuration = 1 test
    //      You shouldn't have any reuse between tests.
    for (Task *task : tg.tasks_it())
        task->clear_versions();
}

TEST_CASE("Solver test", "[solver][carpetbomb]") {
    SECTION("Homogenous single-version") {
        auto i = GENERATE(range(DAGS_HOMOGENOUS_START, DAGS_HOMOGENOUS_END));
        SECTION("dag" + to_string(i) + ".tey") {
            runTestSuite(graphs[i], nfp_8h_sv);
        }
    }

    SECTION("Homogenous multi-version") {
        auto i = GENERATE(range(DAGS_HOMOGENOUS_START, DAGS_HOMOGENOUS_END));
        SECTION("dag" + to_string(i) + ".tey") {
            runTestSuite(graphs[i], nfp_8h_mv, [](SystemModel& tg, config_t&) {
                // Lower the deadline substantially such that only a multi-version solution is schedulable
                tg.global_deadline(tg.global_deadline() * 0.7);
            });
        }
    }

    SECTION("Heterogeneous single-version") {
        auto i = GENERATE(range(DAGS_HETEROGENEOUS_START, DAGS_HETEROGENEOUS_END));
        SECTION("dag" + to_string(i) + ".tey") {
            runTestSuite(graphs[i], nfp_4h4e_sv);
        }
    }

    SECTION("Heterogeneous multi-version") {
        auto i = GENERATE(range(DAGS_HETEROGENEOUS_START, DAGS_HETEROGENEOUS_END));
        SECTION("dag" + to_string(i) + ".tey") {
            runTestSuite(graphs[i], nfp_4h4e_mv, [](SystemModel& tg, config_t&) {
                // Lower the deadline substantially such that only a multi-version solution is schedulable
                tg.global_deadline(tg.global_deadline() * 0.7);
            });
        }
    }

    // TODO: add multi-phase test here
}

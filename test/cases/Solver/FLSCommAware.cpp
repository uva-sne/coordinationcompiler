/*
 * Copyright (C) 2022 Benjamin Rouxel <benjamin.rouxel@unimore.it>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "config.hpp"
#include "Utils.hpp"
#include "InputParser/InputParser.hpp"
#include "Solvers/Solver.hpp"
#include "SystemModel.hpp"
#include "Schedule.hpp"

#include "test_utils.hpp"
#include <catch.hpp>

#include <csignal>
#include <string>
#include <Solvers/ForwardListScheduling/ForwardListScheduling.hpp>

using namespace std;

TEST_CASE("FLS ideal communication", "[solver][fls][comm-aware][fls-comm-ideal]") {
    config_t conf;
    SystemModel tg;
    Schedule sched(&tg, &conf);
    setup_config(conf, tg, "fls_ideal.xml");
    setup_tey(conf, tg, "sched_test.tey");

    for(CompilerPassPtr &p : conf.passes)
        REQUIRE_NOTHROW(p->run(&tg, &conf, &sched));
 
    Schedule check_sched(&tg, &conf);
    ResXMLSaxParser * rp = setup_xmlchecker(conf, tg, &check_sched, "fls_ideal.xml");
    
    CHECK(*rp->schedule() == sched);
    delete rp;
}

TEST_CASE("FLS worst_concurrency communication", "[solver][fls][comm-aware][fls-comm-worst]") {
    config_t conf;
    SystemModel tg;
    Schedule sched(&tg, &conf);
    setup_config(conf, tg, "fls_worst.xml");
    setup_tey(conf, tg, "sched_test.tey");

    for(CompilerPassPtr &p : conf.passes)
        REQUIRE_NOTHROW(p->run(&tg, &conf, &sched));
 
    Schedule check_sched(&tg, &conf);
    ResXMLSaxParser * rp = setup_xmlchecker(conf, tg, &check_sched, "fls_worst.xml");
    
    CHECK(*rp->schedule() == sched);
    delete rp;
}

TEST_CASE("FLS contention aware communication", "[solver][fls][comm-aware][fls-comm-contaw]") {
    config_t conf;
    SystemModel tg;
    Schedule sched(&tg, &conf);
    setup_config(conf, tg, "fls_contentionaware.xml");
    setup_tey(conf, tg, "sched_test.tey");

    for(CompilerPassPtr &p : conf.passes)
        REQUIRE_NOTHROW(p->run(&tg, &conf, &sched));
 
    Schedule check_sched(&tg, &conf);
    ResXMLSaxParser * rp = setup_xmlchecker(conf, tg, &check_sched, "fls_contentionaware.xml");
    
    CHECK(*rp->schedule() == sched);
    delete rp;
}

TEST_CASE("FLS synchronized communication", "[solver][fls][comm-aware][fls-comm-sync]") {
    config_t conf;
    SystemModel tg;
    Schedule sched(&tg, &conf);
    setup_config(conf, tg, "fls_synchronized.xml");
    setup_tey(conf, tg, "sched_test.tey");

    for(CompilerPassPtr &p : conf.passes)
        REQUIRE_NOTHROW(p->run(&tg, &conf, &sched));
 
    Schedule check_sched(&tg, &conf);
    ResXMLSaxParser * rp = setup_xmlchecker(conf, tg, &check_sched, "fls_synchronized.xml");
    
    CHECK(*rp->schedule() == sched);
    delete rp;
}

TEST_CASE("FLS worst_concurrency non-blocking communication", "[solver][fls][comm-aware][fls-comm-worst][fls-nbl-comm-worst]") {
    config_t conf;
    SystemModel tg;
    Schedule sched(&tg, &conf);
    setup_config(conf, tg, "fls_worst_nbl.xml");
    setup_tey(conf, tg, "sched_test.tey");

    for(CompilerPassPtr &p : conf.passes)
        REQUIRE_NOTHROW(p->run(&tg, &conf, &sched));
 
    Schedule check_sched(&tg, &conf);
    ResXMLSaxParser * rp = setup_xmlchecker(conf, tg, &check_sched, "fls_worst_nbl.xml");
    
    CHECK(*rp->schedule() == sched);
    delete rp;
}

TEST_CASE("FLS contention aware non-blocking communication", "[solver][fls][comm-aware][fls-nbl-comm-contaw]") {
    config_t conf;
    SystemModel tg;
    Schedule sched(&tg, &conf);
    setup_config(conf, tg, "fls_contentionaware_nbl.xml");
    setup_tey(conf, tg, "sched_test.tey");

    for(CompilerPassPtr &p : conf.passes)
        REQUIRE_NOTHROW(p->run(&tg, &conf, &sched));
 
    Schedule check_sched(&tg, &conf);
    ResXMLSaxParser * rp = setup_xmlchecker(conf, tg, &check_sched, "fls_contentionaware_nbl.xml");
    
    CHECK(*rp->schedule() == sched);
    delete rp;
}

TEST_CASE("FLS synchronized non-blocking communication", "[solver][fls][comm-aware][fls-nbl-comm-sync]") {
    config_t conf;
    SystemModel tg;
    Schedule sched(&tg, &conf);
    setup_config(conf, tg, "fls_synchronized_nbl.xml");
    setup_tey(conf, tg, "sched_test.tey");

    for(CompilerPassPtr &p : conf.passes)
        REQUIRE_NOTHROW(p->run(&tg, &conf, &sched));
 
    Schedule check_sched(&tg, &conf);
    ResXMLSaxParser * rp = setup_xmlchecker(conf, tg, &check_sched, "fls_synchronized_nbl.xml");
    
    CHECK(*rp->schedule() == sched);
    delete rp;
}

TEST_CASE("FLS worst_concurrency shared communication", "[solver][fls][comm-aware][fls-comm-worst][fls-sha-comm-worst]") {
    config_t conf;
    SystemModel tg;
    Schedule sched(&tg, &conf);
    setup_config(conf, tg, "fls_worst_sha.xml");
    setup_tey(conf, tg, "sched_test.tey");

    for(CompilerPassPtr &p : conf.passes)
        REQUIRE_NOTHROW(p->run(&tg, &conf, &sched));
 
    Schedule check_sched(&tg, &conf);
    ResXMLSaxParser * rp = setup_xmlchecker(conf, tg, &check_sched, "fls_worst_sha.xml");
    
    CHECK(*rp->schedule() == sched);
    delete rp;
}

TEST_CASE("FLS contention aware shared communication", "[solver][fls][comm-aware][fls-sha-comm-contaw]") {
    config_t conf;
    SystemModel tg;
    Schedule sched(&tg, &conf);
    setup_config(conf, tg, "fls_contentionaware_sha.xml");
    setup_tey(conf, tg, "sched_test.tey");

    for(CompilerPassPtr &p : conf.passes)
        REQUIRE_NOTHROW(p->run(&tg, &conf, &sched));
 
    Schedule check_sched(&tg, &conf);
    ResXMLSaxParser * rp = setup_xmlchecker(conf, tg, &check_sched, "fls_contentionaware_sha.xml");
    
    CHECK(*rp->schedule() == sched);
    delete rp;
}

TEST_CASE("FLS synchronized shared communication", "[solver][fls][comm-aware][fls-sha-comm-sync]") {
    config_t conf;
    SystemModel tg;
    Schedule sched(&tg, &conf);
    setup_config(conf, tg, "fls_synchronized_sha.xml");
    setup_tey(conf, tg, "sched_test.tey");

    for(CompilerPassPtr &p : conf.passes)
        REQUIRE_NOTHROW(p->run(&tg, &conf, &sched));
 
    Schedule check_sched(&tg, &conf);
    ResXMLSaxParser * rp = setup_xmlchecker(conf, tg, &check_sched, "fls_synchronized_sha.xml");
    
    
    CHECK(*rp->schedule() == sched);
    delete rp;
}

TEST_CASE("FLS worst_concurrency non-blocking shared communication", "[solver][fls][comm-aware][fls-comm-worst][fls-nbl-sha-comm-worst]") {
    config_t conf;
    SystemModel tg;
    Schedule sched(&tg, &conf);
    setup_config(conf, tg, "fls_worst_nbl_sha.xml");
    setup_tey(conf, tg, "sched_test.tey");

    for(CompilerPassPtr &p : conf.passes)
        REQUIRE_NOTHROW(p->run(&tg, &conf, &sched));
 
    Schedule check_sched(&tg, &conf);
    ResXMLSaxParser * rp = setup_xmlchecker(conf, tg, &check_sched, "fls_worst_nbl_sha.xml");
    
    CHECK(*rp->schedule() == sched);
    delete rp;
}

TEST_CASE("FLS contention aware non-blocking shared communication", "[solver][fls][comm-aware][fls-nbl-sha-comm-contaw]") {
    config_t conf;
    SystemModel tg;
    Schedule sched(&tg, &conf);
    setup_config(conf, tg, "fls_contentionaware_nbl_sha.xml");
    setup_tey(conf, tg, "sched_test.tey");

    for(CompilerPassPtr &p : conf.passes)
        REQUIRE_NOTHROW(p->run(&tg, &conf, &sched));
 
    Schedule check_sched(&tg, &conf);
    ResXMLSaxParser * rp = setup_xmlchecker(conf, tg, &check_sched, "fls_contentionaware_nbl_sha.xml");
    
    CHECK(*rp->schedule() == sched);
    delete rp;
}

TEST_CASE("FLS synchronized non-blocking shared communication", "[solver][fls][comm-aware][fls-nbl-sha-comm-sync]") {
    config_t conf;
    SystemModel tg;
    Schedule sched(&tg, &conf);
    setup_config(conf, tg, "fls_synchronized_nbl_sha.xml");
    setup_tey(conf, tg, "sched_test.tey");

    for(CompilerPassPtr &p : conf.passes)
        REQUIRE_NOTHROW(p->run(&tg, &conf, &sched));
 
    Schedule check_sched(&tg, &conf);
    ResXMLSaxParser * rp = setup_xmlchecker(conf, tg, &check_sched, "fls_synchronized_nbl_sha.xml");
    
    CHECK(*rp->schedule() == sched);
    delete rp;
}
TEST_CASE("FLS worst_concurrency burst communication", "[solver][fls][comm-aware][fls-comm-worst][fls-comm-worst-burstedge]") {
    config_t conf;
    SystemModel tg;
    Schedule sched1(&tg, &conf);
    setup_config(conf, tg, "fls_worst_burstedge.xml");
    setup_tey(conf, tg, "sched_test.tey");

    for(CompilerPassPtr &p : conf.passes)
        REQUIRE_NOTHROW(p->run(&tg, &conf, &sched1));
 
    Schedule check_sched(&tg, &conf);
    ResXMLSaxParser * rp = setup_xmlchecker(conf, tg, &check_sched, "fls_worst_burstedge.xml");
    
    CHECK(*rp->schedule() == sched1);
    delete rp;
}

TEST_CASE("FLS contention aware burst communication", "[solver][fls][comm-aware][fls-comm-contaw-burstedge]") {
    config_t conf;
    SystemModel tg;
    Schedule sched(&tg, &conf);
    setup_config(conf, tg, "fls_contentionaware_burstedge.xml");
    setup_tey(conf, tg, "sched_test.tey");

    for(CompilerPassPtr &p : conf.passes)
        REQUIRE_NOTHROW(p->run(&tg, &conf, &sched));
 
    Schedule check_sched(&tg, &conf);
    ResXMLSaxParser * rp = setup_xmlchecker(conf, tg, &check_sched, "fls_contentionaware_burstedge.xml");
    
    CHECK(*rp->schedule() == sched);
    delete rp;
}

TEST_CASE("FLS synchronized burst communication", "[solver][fls][comm-aware][fls-comm-sync-burstedge]") {
    config_t conf;
    SystemModel tg;
    Schedule sched(&tg, &conf);
    setup_config(conf, tg, "fls_synchronized_burstedge.xml");
    setup_tey(conf, tg, "sched_test.tey");

    for(CompilerPassPtr &p : conf.passes)
        REQUIRE_NOTHROW(p->run(&tg, &conf, &sched));
 
    Schedule check_sched(&tg, &conf);
    ResXMLSaxParser * rp = setup_xmlchecker(conf, tg, &check_sched, "fls_synchronized_burstedge.xml");
    
    CHECK(*rp->schedule() == sched);
    delete rp;
}
TEST_CASE("FLS worst_concurrency non-blocking burst communication", "[solver][fls][comm-aware][fls-comm-worst][fls-nbl-comm-worst-burstedge]") {
    config_t conf;
    SystemModel tg;
    Schedule sched(&tg, &conf);
    setup_config(conf, tg, "fls_worst_nbl_burstedge.xml");
    setup_tey(conf, tg, "sched_test.tey");

    for(CompilerPassPtr &p : conf.passes)
        REQUIRE_NOTHROW(p->run(&tg, &conf, &sched));
 
    Schedule check_sched(&tg, &conf);
    ResXMLSaxParser * rp = setup_xmlchecker(conf, tg, &check_sched, "fls_worst_nbl_burstedge.xml");
    
    CHECK(*rp->schedule() == sched);
    delete rp;
}

TEST_CASE("FLS contention aware non-blocking burst communication", "[solver][fls][comm-aware][fls-nbl-comm-contaw-burstedge]") {
    config_t conf;
    SystemModel tg;
    Schedule sched(&tg, &conf);
    setup_config(conf, tg, "fls_contentionaware_nbl_burstedge.xml");
    setup_tey(conf, tg, "sched_test.tey");

    for(CompilerPassPtr &p : conf.passes)
        REQUIRE_NOTHROW(p->run(&tg, &conf, &sched));
 
    Schedule check_sched(&tg, &conf);
    ResXMLSaxParser * rp = setup_xmlchecker(conf, tg, &check_sched, "fls_contentionaware_nbl_burstedge.xml");
    
    CHECK(*rp->schedule() == sched);
    delete rp;
}

TEST_CASE("FLS synchronized non-blocking burst communication", "[solver][fls][comm-aware][fls-nbl-comm-sync-burstedge]") {
    config_t conf;
    SystemModel tg;
    Schedule sched(&tg, &conf);
    setup_config(conf, tg, "fls_synchronized_nbl_burstedge.xml");
    setup_tey(conf, tg, "sched_test.tey");

    for(CompilerPassPtr &p : conf.passes)
        REQUIRE_NOTHROW(p->run(&tg, &conf, &sched));
 
    Schedule check_sched(&tg, &conf);
    ResXMLSaxParser * rp = setup_xmlchecker(conf, tg, &check_sched, "fls_synchronized_nbl_burstedge.xml");
    
    
    CHECK(*rp->schedule() == sched);
    delete rp;
}
TEST_CASE("FLS worst_concurrency non-blocking burst shared communication", "[solver][fls][comm-aware][fls-comm-worst][fls-nbl-sha-comm-worst-burstedge]") {
    config_t conf;
    SystemModel tg;
    Schedule sched(&tg, &conf);
    setup_config(conf, tg, "fls_worst_nbl_sha_burstedge.xml");
    setup_tey(conf, tg, "sched_test.tey");

    for(CompilerPassPtr &p : conf.passes)
        REQUIRE_NOTHROW(p->run(&tg, &conf, &sched));
 
    Schedule check_sched(&tg, &conf);
    ResXMLSaxParser * rp = setup_xmlchecker(conf, tg, &check_sched, "fls_worst_nbl_sha_burstedge.xml");
    
    CHECK(*rp->schedule() == sched);
    delete rp;
}

TEST_CASE("FLS contention aware non-blocking burst shared communication", "[solver][fls][comm-aware][fls-nbl-sha-comm-contaw-burstedge]") {
    config_t conf;
    SystemModel tg;
    Schedule sched(&tg, &conf);
    setup_config(conf, tg, "fls_contentionaware_nbl_sha_burstedge.xml");
    setup_tey(conf, tg, "sched_test.tey");

    for(CompilerPassPtr &p : conf.passes)
        REQUIRE_NOTHROW(p->run(&tg, &conf, &sched));
 
    Schedule check_sched(&tg, &conf);
    ResXMLSaxParser * rp = setup_xmlchecker(conf, tg, &check_sched, "fls_contentionaware_nbl_sha_burstedge.xml");
    
    CHECK(*rp->schedule() == sched);
    delete rp;
}
TEST_CASE("FLS synchronized non-blocking burst shared communication", "[solver][fls][comm-aware][fls-nbl-sha-comm-sync-burstedge]") {
    config_t conf;
    SystemModel tg;
    Schedule sched(&tg, &conf);
    setup_config(conf, tg, "fls_synchronized_nbl_sha_burstedge.xml");
    setup_tey(conf, tg, "sched_test.tey");

    for(CompilerPassPtr &p : conf.passes)
        REQUIRE_NOTHROW(p->run(&tg, &conf, &sched));
 
    Schedule check_sched(&tg, &conf);
    ResXMLSaxParser * rp = setup_xmlchecker(conf, tg, &check_sched, "fls_synchronized_nbl_sha_burstedge.xml");
    
    CHECK(*rp->schedule() == sched);
    delete rp;
}

TEST_CASE("FLS SPM mapping -- local data", "[solver][fls][comm-aware][fls-spm-mapping][fls-spm-mapping-ldata]") {
    config_t conf;
    SystemModel tg;
    Schedule sched(&tg, &conf);
    setup_config(conf, tg, "fls_spm_mapping_ldata.xml");
    setup_tey(conf, tg, "sched_test.tey");

    for(CompilerPassPtr &p : conf.passes)
        REQUIRE_NOTHROW(p->run(&tg, &conf, &sched));
 
    Schedule check_sched(&tg, &conf);
    ResXMLSaxParser * rp = setup_xmlchecker(conf, tg, &check_sched, "fls_spm_mapping_ldata.xml");
    
    CHECK(*rp->schedule() == sched);
    delete rp;
}

TEST_CASE("FLS SPM mapping -- code+local data", "[solver][fls][comm-aware][fls-spm-mapping][fls-spm-mapping-full]") {
    config_t conf;
    SystemModel tg;
    Schedule sched(&tg, &conf);
    setup_config(conf, tg, "fls_spm_mapping_full.xml");
    setup_tey(conf, tg, "sched_test.tey");

    for(CompilerPassPtr &p : conf.passes)
        REQUIRE_NOTHROW(p->run(&tg, &conf, &sched));
 
    Schedule check_sched(&tg, &conf);
    ResXMLSaxParser * rp = setup_xmlchecker(conf, tg, &check_sched, "fls_spm_mapping_full.xml");
    
    CHECK(*rp->schedule() == sched);
    delete rp;
}

TEST_CASE("FLS SPM mapping -- code+local data, prefetch code", "[solver][fls][comm-aware][fls-spm-mapping][fls-spm-mapping-pref]") {
    config_t conf;
    SystemModel tg;
    Schedule sched(&tg, &conf);
    setup_config(conf, tg, "fls_spm_mapping_pref.xml");
    setup_tey(conf, tg, "sched_test.tey");

    for(CompilerPassPtr &p : conf.passes)
        REQUIRE_NOTHROW(p->run(&tg, &conf, &sched));
 
    Schedule check_sched(&tg, &conf);
    ResXMLSaxParser * rp = setup_xmlchecker(conf, tg, &check_sched, "fls_spm_mapping_pref.xml");
    
    CHECK(*rp->schedule() == sched);
    delete rp;
}
TEST_CASE("FLS SPM mapping -- code+local data, prefetch code, shared", "[solver][fls][comm-aware][fls-spm-mapping][fls-spm-mapping-pref-sha]") {
    config_t conf;
    SystemModel tg;
    Schedule sched(&tg, &conf);
    setup_config(conf, tg, "fls_spm_mapping_pref_sha.xml");
    setup_tey(conf, tg, "sched_test.tey");

    for(CompilerPassPtr &p : conf.passes)
        REQUIRE_NOTHROW(p->run(&tg, &conf, &sched));
 
    Schedule check_sched(&tg, &conf);
    ResXMLSaxParser * rp = setup_xmlchecker(conf, tg, &check_sched, "fls_spm_mapping_pref_sha.xml");
    
    CHECK(*rp->schedule() == sched);
    delete rp;
}

TEST_CASE("FLS SPM mapping -- failed", "[solver][fls][comm-aware][fls-spm-mapping][fls-spm-mapping-failed]") {
    config_t conf;
    SystemModel tg;
    Schedule sched(&tg, &conf);
    setup_config(conf, tg, "fls_spm_mapping_failed.xml");
    setup_tey(conf, tg, "sched_test.tey");

    for(CompilerPassPtr &p : conf.passes) {
        if(Utils::isa<ForwardListScheduling*>(p.get()))
            REQUIRE_THROWS(p->run(&tg, &conf, &sched));
        else
            REQUIRE_NOTHROW(p->run(&tg, &conf, &sched));
    }
}
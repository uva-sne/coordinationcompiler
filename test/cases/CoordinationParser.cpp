/*
 * Copyright (C) 2020 Stephen Nicholas Swatman <s.n.swatman@uva.nl> 
 *                    Univeristy of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include "config.hpp"
#include "Utils.hpp"
#include "CompilerPass.hpp"
#include "InputParser/InputParser.hpp"
#include "SystemModel.hpp"
#include "Schedule.hpp"

#include "test_utils.hpp"

#include <csignal>
#include <stdexcept>
#include "catch.hpp"
#include <iostream>
#include <string>
#include "Utils.hpp"
#include <boost/algorithm/cxx11/any_of.hpp>


TEST_CASE("Coord simple parser", "[coord][input][parser][config]") {
    config_t conf;
    SystemModel tg;
    setup_trivial(conf, tg);
    setup_tey(conf, tg, "simple.tey");

    SECTION("Global parameters") {
        REQUIRE( tg.global_deadline() == 51000000000 );
        REQUIRE( tg.global_available_energy() == 811 );
        REQUIRE( tg.global_period() == 22000000000 );
    }

    SECTION("Tasks and edge count") {
        REQUIRE( tg.tasks().size() == 2 );
        REQUIRE( tg.nbedges() == 1 );
    }

    SECTION("Fetch tasks") {
        Task * t1 = tg.task("p1");
        Task * t2 = tg.task("p2");
        Task * t3 = tg.task("p3");

        REQUIRE( t1 != nullptr );
        REQUIRE( t2 != nullptr );
        REQUIRE( t3 == nullptr );
    }
}

TEST_CASE("Coord tasks parser", "[coord][input][parser][config]") {
    config_t conf;
    SystemModel tg;
    setup_trivial(conf, tg);
    setup_tey(conf, tg, "tasks.tey");

    REQUIRE( tg.tasks().size() == 4 );
    REQUIRE( tg.nbedges() == 0 );

    Task * t1 = tg.task("p1");
    Task * t2 = tg.task("p2");
    Task * t3 = tg.task("p3");
    Task * t4 = tg.task("p4");
    Task * t5 = tg.task("p5");

    REQUIRE( t1 != nullptr );
    REQUIRE( t2 != nullptr );
    REQUIRE( t3 != nullptr );
    REQUIRE( t4 != nullptr );
    REQUIRE( t5 == nullptr );

    CHECK( t1->id() == "p1" );
    CHECK( t2->id() == "p2" );
    CHECK( t3->id() == "p3" );
    CHECK( t4->id() == "p4" );

    CHECK( t1->predecessor_tasks().size() == 0 );
    CHECK( t2->predecessor_tasks().size() == 0 );
    CHECK( t3->predecessor_tasks().size() == 0 );
    CHECK( t4->predecessor_tasks().size() == 0 );

    CHECK( t1->successor_tasks().size() == 0 );
    CHECK( t2->successor_tasks().size() == 0 );
    CHECK( t3->successor_tasks().size() == 0 );
    CHECK( t4->successor_tasks().size() == 0 );

    CHECK( t1->inputs().size() == 0 );
    CHECK( t2->inputs().size() == 1 );
    CHECK( t3->inputs().size() == 2 );
    CHECK( t4->inputs().size() == 1 );

    CHECK( t1->outputs().size() == 1 );
    CHECK( t2->outputs().size() == 2 );
    CHECK( t3->outputs().size() == 0 );
    CHECK( t4->outputs().size() == 0 );

    CHECK( t1->D() == 0 );
    CHECK( t2->D() == 150000000000 );

    CHECK( t1->T() == 0 );
    CHECK( t2->T() == 120000000000 );

    CHECK( t1->versions().size() == 1 );
    CHECK( t2->versions().size() == 2 );

    CHECK( t1->version("") != nullptr );
    CHECK( t1->version("v") == nullptr );
    CHECK( t2->version("") == nullptr );
    CHECK( t2->version("v") != nullptr );
    CHECK( t2->version("w") != nullptr );
    CHECK( t2->version("x") == nullptr );
}

TEST_CASE("Coord parser - versions", "[coord][input][parser][config]") {
    config_t conf;
    SystemModel tg;
    setup_trivial(conf, tg);
    setup_tey(conf, tg, "tasks.tey");

    REQUIRE( tg.tasks().size() == 4 );
    REQUIRE( tg.nbedges() == 0 );

    Task * t1 = tg.task("p1");
    Task * t2 = tg.task("p2");
    Task * t3 = tg.task("p3");
    Task * t4 = tg.task("p4");

    REQUIRE( t1 != nullptr );
    REQUIRE( t2 != nullptr );
    REQUIRE( t3 != nullptr );
    REQUIRE( t4 != nullptr );

    Version * v1 = t1->version("");
    Version * v2 = t2->version("v");
    Version * v3 = t2->version("w");

    REQUIRE( v1 != nullptr );
    REQUIRE( v2 != nullptr );
    REQUIRE( v3 != nullptr );

    CHECK( v1->task() == t1 );
    CHECK( v2->task() == t2 );
    CHECK( v3->task() == t2 );

    CHECK( v1->id() == "" );
    CHECK( v2->id() == "v" );
    CHECK( v3->id() == "w" );
}

TEST_CASE("Coord parser - and", "[coord][input][parser][config]") {
    config_t conf;
    SystemModel tg;
    setup_trivial(conf, tg);
    setup_tey(conf, tg, "and.tey");

    // One more than you would expect, because we need an additional
    // intermediate task.
    REQUIRE( tg.tasks().size() == 3 );
    REQUIRE( tg.nbedges() == 2 );

    Task * t1 = tg.task("p1");
    Task * t2 = tg.task("p2");
    Task * t3 = tg.task("p3");

    REQUIRE( t1 != nullptr );
    REQUIRE( t2 != nullptr );
    REQUIRE( t3 != nullptr );

    CHECK( t1->successor_tasks().size() == 2 );

    bool found_t2 = false;
    bool found_t3 = false;

    for (Task::dep_t i : t1->successor_tasks()) {
        if (i.first == t3) {
            found_t3 = true;
        }

        if (i.first == t2) {
            found_t2 = true;
        }
    }

    CHECK( found_t2 );
    CHECK( found_t3 );

    CHECK( t2->predecessor_tasks().size() == 1 );
    CHECK_NOTHROW( t2->predecessor_tasks().at(t1) );

    CHECK( t3->predecessor_tasks().size() == 1 );
    CHECK_NOTHROW( t2->predecessor_tasks().at(t1) );
}


TEST_CASE("Coord profiles", "[coord][input][parser][config]") {
    config_t conf;
    SystemModel tg;
    setup_trivial(conf, tg);
    setup_tey(conf, tg, "profiles.tey");

    SECTION("Tasks and edge count") {
        REQUIRE( tg.tasks().size() == 4 );
    }

    SECTION("Fetch tasks") {
        Task * t1 = tg.task("p1");
        Task * t2 = tg.task("p2");
        Task * t3 = tg.task("p3");
        Task * t4 = tg.task("p4");

        REQUIRE( t1 != nullptr );
        REQUIRE( t2 != nullptr );
        REQUIRE( t3 != nullptr );
        REQUIRE( t4 != nullptr );

        // Propagating properties is a bit weird, this should ensure that the global period/deadline
        // But tests fail, we should look into this.
        // CHECK( Utils::stringtime_to_ns(t1->profile().get("period")) == Utils::stringtime_to_ns("1s") );

        // CHECK( Utils::stringtime_to_ns(t1->profile().get("deadline")) == Utils::stringtime_to_ns("1s") );

        CHECK( t2->D() == 50 );
        
        // Overwritten once
        CHECK( t1->profile().get("nModular_replicas") == "4" );

        // Normal fault-tolerance setting
        CHECK( t1->profile().get("nModular_votingReplicas") == "3" );

        CHECK( t2->profile().get("nVersion") == "true" );
        CHECK( t2->profile().get("nVersion_versions_v") == "2" );
        CHECK( t2->profile().get("nVersion_versions_w") == "1" );

        CHECK( t3->profile().get("standby") == "true" );
        CHECK( t3->profile().get("standby_replicas") == "3" );
        CHECK( t3->profile().get("standby_synchronization") == "hot" );

        CHECK( t4->profile().get("checkpoint") == "true" );
        CHECK( t4->profile().getModifier("checkpoint") == vital );

        CHECK (t4->profile().get("checkpoint_shrinking") == "true");
        CHECK (t4->profile().getModifier("checkpoint_shrinking") == vital);
    }
}

TEST_CASE("Coord subgraph complex", "[coord][input][parser][config]") {
    config_t conf;
    SystemModel tg;
    setup_trivial(conf, tg);
    setup_tey(conf, tg, "duplicateEdgeSubgraph.tey");

    SECTION("Tasks and edge count") {
        REQUIRE( tg.tasks().size() == 8 );
    }

    SECTION("Subgraphs and profile checks") {

        /* Fetch tasks */
        Task * addtest = tg.task("add_addtest");
        Task * sink = tg.task("add_sink");
        Task * gen_1 = tg.task("gen_1");
        Task * gen_2 = tg.task("gen_2");
        Task * gen_3 = tg.task("gen_3");
        Task * add_2 = tg.task("add_2");
        Task * mul = tg.task("mul");
        Task * print = tg.task("print");
        REQUIRE( addtest != nullptr );
        REQUIRE( sink != nullptr );
        REQUIRE( gen_1 != nullptr );
        REQUIRE( add_2 != nullptr );
        REQUIRE (mul != nullptr);
        REQUIRE (print != nullptr);

        // Profile checks
        CHECK( addtest->profile().get("nModular_replicas") == "3" );
        CHECK( addtest->profile().get("nModular") == "true" );
        
        // Is vital, so cannot be changed
        CHECK( sink->profile().get("nModular_replicas") == "3" );

        // Overwritten once
        CHECK( sink->profile().get("nModular_votingReplicas") == "3" );

        // Subgraph input checks
        CHECK_FALSE( addtest->predecessor_tasks()[gen_1].empty() );
        CHECK_FALSE( addtest->predecessor_tasks()[gen_2].empty() );

        CHECK_FALSE( sink->predecessor_tasks()[gen_1].empty() );

        CHECK_FALSE( add_2->predecessor_tasks()[gen_1].empty() );
        CHECK_FALSE( add_2->predecessor_tasks()[gen_2].empty() );

        // Subgraph output checks
        CHECK_FALSE( print->predecessor_tasks()[addtest].empty() );
        CHECK_FALSE( print->predecessor_tasks()[mul].empty() );

        CHECK_FALSE( mul->predecessor_tasks()[addtest].empty() );
        CHECK_FALSE( mul->predecessor_tasks()[gen_3].empty() );
    }
}

TEST_CASE("Coord WCET WCEC", "[coord][input][parser][config]") {
    config_t conf;
    SystemModel tg;
    setup_trivial(conf, tg);
    setup_tey(conf, tg, "wcet_wcec.tey");

    REQUIRE( tg.tasks().size() == 3 );
    
    SECTION("Fetch tasks") {
        Task * t1 = tg.task("p1");
        Task * t2 = tg.task("p2");
        Task * t3 = tg.task("p3");
    
        REQUIRE( t1 != nullptr );
        REQUIRE( t2 != nullptr );
        REQUIRE( t3 != nullptr );

        Version * v1 = t1->version("v1");
        Version * v2 = t2->version("v1");
        Version * v3 = t3->version("v1");

        REQUIRE( v1 != nullptr );
        REQUIRE( v2 != nullptr );
        REQUIRE( v3 != nullptr );

        CHECK( v1->C()   == 15000000 );
        CHECK( v1->C_E() == 0 );

        CHECK( v2->C()   == 1000000000 );
        CHECK( v2->C_E() == 600 );

        CHECK( v3->C()   == 0 );
        CHECK( v3->C_E() == 720000000 );
    }
}

TEST_CASE("Coord subgraph", "[coord][input][parser][config]") {
    config_t conf;
    SystemModel tg;
    setup_trivial(conf, tg);
    setup_tey(conf, tg, "subGraph.tey");

    SECTION("Tasks and edge count") {
        REQUIRE( tg.tasks().size() == 5 );
    }

    SECTION("Fetch tasks") {
        Task *t1 = tg.task("p1");
        Task *t2 = tg.task("p2_p2");
        Task *t3 = tg.task("p3");
        Task *t4 = tg.task("p4");

        REQUIRE( t1 != nullptr );
        REQUIRE( t2 != nullptr );
        REQUIRE( t3 != nullptr );
        REQUIRE( t4 != nullptr );

        CHECK_FALSE( t2->predecessor_tasks()[t1].empty() );

        CHECK_FALSE( t4->predecessor_tasks()[t2].empty() );

        CHECK_FALSE( t3->predecessor_tasks()[t2].empty() );
        
        CHECK_FALSE( t3->predecessor_tasks()[t1].empty() );

        // Removed
        CHECK( t2->profile().get("nModular_replicas") == "" );

        CHECK( t2->profile().get("nModular") == "" );

        // Removed
        CHECK( t2->profile().get("nModular_votingReplicas") == "" );
    }
}

TEST_CASE("Coord edges", "[coord][input][parser][config]") {
    config_t conf;
    SystemModel tg;
    setup_trivial(conf, tg);

    SECTION("Reject mismatched token counts") {
        REQUIRE_THROWS(setup_tey(conf, tg, "invalid5.tey"));
    }

    SECTION("Accept multiple edges") {
        setup_tey(conf, tg, "simple_duplicate.tey");
        Task* p1 = tg.task("p1");
        Task* p2 = tg.task("p2");
        CHECK( p1->successor_tasks()[p2].size() == 2 );
        CHECK( p2->predecessor_tasks()[p1].size() == 2 );
        CHECK( p1->successor_tasks()[p2] == p2->predecessor_tasks()[p1] ); // share the same connections
    }
}
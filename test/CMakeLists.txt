# Copyright (C) 2020  Stephen Nicholas Swatman <s.n.swatman@uva.nl> 
#                     Univeristy of Amsterdam
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

option(CECILE_BUILD_TESTS, "Build the tests")
if(CECILE_BUILD_TESTS)
    if(COMMAND cmake_policy)
      cmake_policy(SET CMP0003 NEW)
    endif(COMMAND cmake_policy)
    
# Define the test binary.
file(GLOB_RECURSE CECILE_TEST_SOURCES *.cpp)
add_executable(cecile_test ${CECILE_TEST_SOURCES})
target_include_directories(cecile_test PRIVATE .)
target_link_libraries(cecile_test PRIVATE cecile_core)
target_compile_definitions(cecile_test PRIVATE -DTEST_RESOURCE_DIR=${CMAKE_CURRENT_SOURCE_DIR})

# Code coverage testing
if (CMAKE_BUILD_TYPE STREQUAL "Coverage")
  if(CMAKE_COMPILER_IS_GNUCXX)
    if(DEFINED CPLEX_ROOT_DIR)
      set(EXCL_CPLEX_ROOT_DIR "${CPLEX_ROOT_DIR}")
    else()
      set(EXCL_CPLEX_ROOT_DIR "/.___hopefully_this_doesnt_exist/")
    endif()

    include(CodeCoverage)
    setup_target_for_coverage_lcov(
      NAME cecile_coverage
      EXECUTABLE "cecile_test"
      DEPENDENCIES cecile_test
      EXCLUDE 
      "/usr/*" "${PROJECT_SOURCE_DIR}/test/*" "*.tpl"
      "${PROJECT_SOURCE_DIR}/src-gen/*" "${EXCL_CPLEX_ROOT_DIR}/*"
    )
    message("-- Building tests for CECILE, invoke with 'cecile_test`")
  else()
    message(FATAL_ERROR "Test coverage build type selected, but compiler is not GCC.")
  endif()
endif()
else()
message("-- Tests will not be built --- to enable them: -DCECILE_BUILD_TESTS=On")
endif()